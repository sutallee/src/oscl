/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdlib.h>

#include "part.h"
#include "constants.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/aes128/cmac.h"
#include "oscl/entropy/rand.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/key/persist/iterator/composer.h"

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI
#define ENABLE_BT_MESH_NET_DYNAMIC_NETMESSAGEMEM

using namespace Oscl::BT::Mesh::Network;

static uint8_t	logConvert(uint16_t value) noexcept{

	if(value == 0xFFFF){
		return 0xFF;
		}

	for(unsigned i=0;i<16;++i){
		if(value & (0x0001 << (15-i))){
			return 16-i;
			}
		}

	return 0;
	}

Part::Part(
	ContextApi&							context,
	Oscl::BT::Mesh::Node::Api&			nodeApi,
	Oscl::BT::Mesh::Bearer::TX::Api&	bearerApi,
	Oscl::Pdu::FWD::Api&				beaconBearerApi,
	Oscl::Timer::Factory::Api&			timerFactoryApi,
	Oscl::BT::Mesh::
    Network::Persist::Api&				networkPersistApi,
	Oscl::BT::Mesh::Key::
	Persist::Creator::Api&				keyPersistApi,
	Oscl::BT::Mesh::
	Subscription::Register::RecordMem*	subscriptionRecordMem,
	unsigned							nSubscriptionRecords,
	const uint8_t						netKey[16],
	const uint8_t						newKey[16],
	uint16_t							keyIndex,
	uint16_t							unicastAddress,
	uint8_t								nElements,
	uint8_t								defaultTTL,
	uint8_t								nodeIdentityState,
	uint8_t								friendState,
	uint8_t								networkKeyPhase,
	uint16_t							heartbeatSubscriptionSource,
	uint16_t							heartbeatSubscriptionDestination,
	uint8_t								heartbeatSubscriptionPeriodLog,
	uint8_t								networkTransmitCount,
	uint8_t								networkTransmitIntervalSteps,
	bool								relayEnabled,
	uint8_t								relayRetransmitCount,
	uint8_t								relayRetransmitIntervalSteps,
	uint16_t							heartbeatPublishDestination,
	uint16_t							heartbeatPublishCount,
	uint8_t								heartbeatPublishPeriodLog,
	uint8_t								heartbeatPublishTtl,
	uint16_t							heartbeatPublishFeatures,
	Oscl::Pdu::Memory::Part&			freeStore,
	AccessReaperMem*					accessReaperMem,
	unsigned							nAccessReaperMem,
	ControlReaperMem*					controlReaperMem,
	unsigned							nControlReaperMem,
	Oscl::BT::Mesh::Key::RecordMem*		appKeyRecordMem,
	unsigned							nAppKeyRecordMem,
	Cache::EntryMem*					cacheEntryMem,
	unsigned							nCacheEntries,
	NetMessageMem*						relayMessageMem,
	unsigned							nNetMessages
	) noexcept:
		_context(context),
		_nodeApi(nodeApi),
		_bearerApi(bearerApi),
		_beaconBearerApi(beaconBearerApi),
		_timerFactoryApi(timerFactoryApi),
		_networkPersistApi(networkPersistApi),
		_keyPersistApi(keyPersistApi),
		_keyIndex(keyIndex),
		_unicastAddress(unicastAddress),
		_nElements(nElements),
		_keyRefreshState(
			*this,
			(networkKeyPhase==0)?
				&Oscl::BT::Mesh::Network::KeyRefresh::StateVar::getPhase0():
			(networkKeyPhase==1)?
				&Oscl::BT::Mesh::Network::KeyRefresh::StateVar::getPhase1():
			(networkKeyPhase==2)?
				&Oscl::BT::Mesh::Network::KeyRefresh::StateVar::getPhase2():
			(networkKeyPhase==3)?
				&Oscl::BT::Mesh::Network::KeyRefresh::StateVar::getPhase3():
				&Oscl::BT::Mesh::Network::KeyRefresh::StateVar::getPhase0()
			),
		_transportUpperRX(
			*this,	// Oscl::BT::Mesh::Transport::Upper::RX::Context::Api
			*this	// Oscl::BT::Mesh::Key::Api
			),
		_activeNetKey(0),
		_newNetKey(0),
		_newNetKeyCandidate(0),
		_configNetKeyUpdateFailed(false),
		_lastRxBeaconNetKey(0),
		_lastRxBeaconKeyRefreshFlag(false),
		_friendshipApi(0),
		_subscriptionRegister(
			subscriptionRecordMem,
			nSubscriptionRecords
			),
		_rxMeshMessageItem(
			*this,
			&Part::rxMeshMessage
			),
		_rxSecureNetworkBeaconItem(
			*this,
			&Part::rxSecureNetworkBeacon
			),
		_relayTxTimerExpired(
			*this,
			&Part::relayTxTimerExpired
			),
		_relayTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_netTxTimerExpired(
			*this,
			&Part::netTxTimerExpired
			),
		_netTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_netReTxTimerExpired(
			*this,
			&Part::netReTxTimerExpired
			),
		_netReTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_heartbeatTxTimerExpired(
			*this,
			&Part::heartbeatTxTimerExpired
			),
		_heartbeatTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_heartbeatSubTimerExpired(
			*this,
			&Part::heartbeatSubTimerExpired
			),
		_heartbeatSubTimerApi(
			*timerFactoryApi.allocate()
			),
		_allFreeNotification(
			*this,
			&Part::allFreeNotification
			),
		_freeStore(freeStore),
		_upperTransportControlRxComposer(
			*this,
			&Part::rxUpperTransportControlPDU
			),
		_loopLocked(false),
		_cache(
			cacheEntryMem,
			nCacheEntries
			),
		_nDynamicNetMessageMem(0),
		_nOutstandingNetMessageMem(0),
		_defaultTTL(defaultTTL),
		_relayEnabled(relayEnabled),
		_relayRetransmitCount(relayRetransmitCount),
		_relayRetransmitIntervalSteps(relayRetransmitIntervalSteps),
		_nodeIdentityState(nodeIdentityState),
		_friendState(friendState),	// Not currently supported
		_networkKeyPhase(networkKeyPhase),	// Normal operation
		_secureNetworkBeaconApi(0),
		_configHeartbeatPublishDestination(heartbeatPublishDestination),
		_configHeartbeatPublishCount(heartbeatPublishCount),
		_configHeartbeatPublishPeriodLog(heartbeatPublishPeriodLog),
		_configHeartbeatPublishTtl(heartbeatPublishTtl),
		_configHeartbeatPublishFeatures(heartbeatPublishFeatures),
		_configHeartbeatSubscriptionSource(heartbeatSubscriptionDestination),
		_configHeartbeatSubscriptionDestination(heartbeatSubscriptionDestination),
		_configHeartbeatSubscriptionPeriod(heartbeatSubscriptionPeriodLog),
		_configHeartbeatSubscriptionCount(0),
		_configHeartbeatSubscriptionMinHops(0x7F),
		_configHeartbeatSubscriptionMaxHops(0x00),
		_networkTransmitCount(networkTransmitCount),
		_networkTransmitIntervalSteps(networkTransmitIntervalSteps),
		_netTxCount(0),
		_rssi(127),	// RSSI is not available
		_stopping(false)
		{

	_relayTxTimerApi.setExpirationCallback(_relayTxTimerExpired);
	_netTxTimerApi.setExpirationCallback(_netTxTimerExpired);
	_netReTxTimerApi.setExpirationCallback(_netReTxTimerExpired);
	_heartbeatTxTimerApi.setExpirationCallback(_heartbeatTxTimerExpired);
	_heartbeatSubTimerApi.setExpirationCallback(_heartbeatSubTimerExpired);

	for(unsigned i=0;i<nAccessReaperMem;++i){
		// Pre-allocate timers
		Oscl::Timer::Api*
		timer	= _timerFactoryApi.allocate();

		accessReaperMem[i].mem.ackTimer	= timer;

		timer	= _timerFactoryApi.allocate();
		accessReaperMem[i].mem.incompleteTimer	= timer;

		_freeAccessReaperMem.put(&accessReaperMem[i]);
		}

	for(unsigned i=0;i<nControlReaperMem;++i){
		// Pre-allocate timers
		Oscl::Timer::Api*
		timer	= _timerFactoryApi.allocate();

		controlReaperMem[i].mem.ackTimer	= timer;

		timer	= _timerFactoryApi.allocate();
		controlReaperMem[i].mem.incompleteTimer	= timer;

		_freeControlReaperMem.put(&controlReaperMem[i]);
		}

	for(unsigned i=0;i<nAppKeyRecordMem;++i){
		_freeAppKeyRecordMem.put(&appKeyRecordMem[i]);
		}

	for(unsigned i=0;i<nNetMessages;++i){
		_freeNetMessageMem.put(&relayMessageMem[i]);
		}

	Oscl::BT::Mesh::Key::
	Persist::Iterator::Composer<Part>
		constructorKeyPersistIterator(
			*this,
			&Part::constructorNextKeyItem
			);

	keyPersistApi.iterate(constructorKeyPersistIterator);

	for(unsigned i=0;i<nNetKeyMem;++i){
		_freeNetKeyMem.put(&_netKeyMem[i]);
		}

	NetKeyMem*
	activeNetKeyMem	= _freeNetKeyMem.get();

	if(!activeNetKeyMem){
		/** This is a programming error. */
		Oscl::ErrorFatal::logAndExit(
			"Cannot allocate activeNetKeyMem.\n"
			);
		return;
		}

	_activeNetKey	= new (activeNetKeyMem) NetKey(
						netKey
						);

	_netKeys.put(_activeNetKey);

	Oscl::Error::Info::log(
		"_networkKeyPhase: %u\n",
		_networkKeyPhase
		);

	if( _networkKeyPhase == 0 ){
		_newNetKey	= 0;
		return;
		}

	NetKeyMem*
	newNetKeyMem	= _freeNetKeyMem.get();

	if(!newNetKeyMem){
		/** This is a programming error. */
		Oscl::ErrorFatal::logAndExit(
			"Cannot allocate newNetKeyMem."
			);
		return;
		}

	_newNetKey	= new (newNetKeyMem) NetKey(
						newKey
						);

	_netKeys.put(_newNetKey);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_relayTxTimerApi);
	_timerFactoryApi.free(_netTxTimerApi);
	_timerFactoryApi.free(_netReTxTimerApi);
	_timerFactoryApi.free(_heartbeatTxTimerApi);
	_timerFactoryApi.free(_heartbeatSubTimerApi);

	AccessReaperMem* accessMem;
	while((accessMem = _freeAccessReaperMem.get())){
		_timerFactoryApi.free(*accessMem->mem.ackTimer);
		_timerFactoryApi.free(*accessMem->mem.incompleteTimer);
		}

	ControlReaperMem* controlMem;
	while((controlMem = _freeControlReaperMem.get())){
		_timerFactoryApi.free(*controlMem->mem.ackTimer);
		_timerFactoryApi.free(*controlMem->mem.incompleteTimer);
		}
	}

Oscl::Frame::RF::RX::Item&	Part::getRxMeshMessageItem() noexcept{
	return _rxMeshMessageItem;
	}

Oscl::Frame::FWD::Item&	Part::getRxSecureNetworkBeaconItem() noexcept{
	return _rxSecureNetworkBeaconItem;
	}

void	Part::setFriendshipApi(
			Oscl::BT::Mesh::Network::Friendship::Api& api
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_friendshipApi	= &api;
	}

Oscl::BT::Mesh::Subscription::Register::IterationApi&
Part::getSubscriptionIterationApi() noexcept{
	return _subscriptionRegister;
	}

Oscl::Pdu::Memory::Api&	Part::getFreeStoreApi() noexcept{
	return _freeStore;
	}

bool	Part::nidMatch(uint8_t nid) const noexcept{
	for(
		NetKey*
		netKey	= _netKeys.first();
		netKey;
		netKey	= _netKeys.next(netKey)
		) {

		if(netKey->credentials()._nid == nid){
			return true;
			}

		}

	return false;
	}

void	Part::setDefaultTTL(uint8_t defaultTTL) noexcept{
	_defaultTTL	= defaultTTL;
	_networkPersistApi.setDefaultTTL(defaultTTL);
	}

void	Part::setRelay(
			uint8_t relay,
			uint8_t retransmitCount,
			uint8_t retransmitIntervalSteps
			) noexcept{

	_relayEnabled	= (relay)?true:false;
	_relayRetransmitCount	= retransmitCount;
	_relayRetransmitIntervalSteps	= retransmitIntervalSteps;

	_networkPersistApi.setRelay(
		relay,
		retransmitCount,
		retransmitIntervalSteps
		);
	}

void	Part::getConfigHeartbeatPublicationStatus(
			uint16_t&	destination,
			uint8_t&	countLog,
			uint8_t&	periodLog,
			uint8_t&	ttl,
			uint16_t&	features
			) noexcept{

	destination	= _configHeartbeatPublishDestination;

	if(Oscl::BT::Mesh::Address::unassigned == destination){
		countLog	= 0;
		periodLog	= 0;
		ttl			= 0;
		features	= 0;
		return;
		}

	countLog	= logConvert(_configHeartbeatPublishCount);
	periodLog	= _configHeartbeatPublishPeriodLog;
	ttl			= _configHeartbeatPublishTtl;
	features	= _configHeartbeatPublishFeatures;
	}

bool	Part::setConfigHeartbeatPublication(
			uint16_t	destination,
			uint8_t		countLog,
			uint8_t		periodLog,
			uint8_t		ttl,
			uint16_t	features
			) noexcept{

	if(countLog){
		if(countLog == 0xFF){
			// Indefinitely
			_configHeartbeatPublishCount		= 0xFFFF;
			}
		else {
			// Valid range: 0x01:0x11
			// not range checked
			// NOTE: It seems to me that
			// a value of 0x11 (17) will
			// overflow a 16-bit counter
			// giving a surprising result (0)!
			_configHeartbeatPublishCount		= (1<<(countLog-1));
			}
		}
	else {
		// Not ever sent / disabled
		_configHeartbeatPublishCount		= 0;
		}

	_configHeartbeatPublishDestination	= destination;
	_configHeartbeatPublishPeriodLog	= periodLog;
	_configHeartbeatPublishTtl			= ttl;
	_configHeartbeatPublishFeatures		= features;

	if(destination){
		// Send immediately... possibly just once
		// if periodLog or countLog is zero.
		publishHeartbeat();
		}
	else {
		_heartbeatTxTimerApi.stop();
		}

	return false;
	}

void	Part::allFreeNotification() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(!_stopping){
		return;
		}

	_context.stopped(*this);
	}

Oscl::BT::Mesh::Key::Record* Part::findAppKey(unsigned index) noexcept{

	for(
		Oscl::BT::Mesh::Key::Record*
		record	= _appKeys.first();
		record;
		record	= _appKeys.next(record)
		){
		if(record->index() == index){
			return record;
			}
		}

	return 0;
	}

bool	Part::addressWantedLocally(uint16_t dst) const noexcept{

	Oscl::BT::Mesh::Element::Symbiont::Api* element;

	for(
		element	= _elementList.first();
		element;
		element	= _elementList.next(element)
		){
		if(element->wantsDestination(dst)){
			return true;
			}
		}

	if(dst == _configHeartbeatSubscriptionDestination){
		return true;
		}

	if(_keyIndex == 0x0000){
		// Primary network
		if(dst == Oscl::BT::Mesh::Address::allNodes){
			return true;
			}
		}

	if(_friendshipApi){
		if(_friendshipApi->wantsDestination(dst)){
			return true;
			}
		}

	return false;
	}

const Oscl::BT::Mesh::Key::Result::Var::State*
Part::add(
	unsigned index,
	const uint8_t	key[16]
	) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" index: 0x%4.4X\n"
		" key: %p\n"
		"",
		__PRETTY_FUNCTION__,
		index,
		key
		);
	#endif

	if(findAppKey(index)){
		return &Oscl::BT::Mesh::Key::Result::Var::getIndexAlreadyExists();
		}

	unsigned char
	aid	= Oscl::BT::Crypto::Keys::k4(key);

	bool	updating	= false;

	switch(_networkKeyPhase){
		case 1:
		case 2:
			updating	= true;
		}

	bool
	failed	= createKey(
				index,
				key,
				aid,
				updating,
				key,
				aid
				);

	if(failed){
		return &Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
		}

	Oscl::BT::Mesh::Key::Persist::Api*
	persistApi	= _keyPersistApi.create(
					index,
					key,
					aid
					);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: Unable to create persistence.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return 0;
	}

const Oscl::BT::Mesh::Key::Result::Var::State*
Part::remove(unsigned index) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" index: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		index
		);
	#endif

	Oscl::BT::Mesh::Key::Record*
	appKey	= findAppKey(index);

	if(!appKey){
		return &Oscl::BT::Mesh::Key::Result::Var::getNoSuchIndex();
		}

	_appKeys.remove(appKey);

	appKey->~Record();

	_freeAppKeyRecordMem.put((Oscl::BT::Mesh::Key::RecordMem*)appKey);

	Oscl::BT::Mesh::Key::Persist::Api*
	persistApi	= findKeyPersistApi(index);

	if(persistApi){
		_keyPersistApi.destroy(*persistApi);
		}
	else {
		Oscl::Error::Info::log(
			"%s: no persistent keyIndex 0x%4.4X.\n",
			OSCL_PRETTY_FUNCTION,
			index
			);
		}

	return 0;
	}

const Oscl::BT::Mesh::Key::Result::Var::State*
Part::update(
				unsigned index,
				const uint8_t	key[16]
				) noexcept{
	/*
		The Config AppKey Update message shall generate an error
		when:
			-	node is in normal operation, Phase 2, or Phase 3
	 */
	switch(_networkKeyPhase){
		case 0:
		case 2:
		case 3:
			return &Oscl::BT::Mesh::Key::Result::Var::getInvalidState();
		default:
			break;
		}

	Oscl::BT::Mesh::Key::Record*
	appKey	= findAppKey(index);

	if(!appKey){
		return &Oscl::BT::Mesh::Key::Result::Var::getNoSuchIndex();
		}

	/*
		The Config AppKey Update message shall generate an error
		when:
			-	in Phase 1 when the Config AppKey Update message
				on a valid AppKeyIndex when the AppKey value is different.

		This doesn't make any sense.
		Shouldn't Phase1 be the *only* phase where a different
		AppKey value is allowed?

		Apparently, like the NetKey, each AppKeyIndex must support
		an "old"/active AppKey and a new AppKey, using the appropriate
		key to transmit/receive based on the phase.
	 */
	if(_networkKeyPhase == 1){
		if(appKey->updating()){
			int
			different	= memcmp(
							key,
							appKey->newKey(),
							16
							);
			if(different){
				return &Oscl::BT::Mesh::Key::Result::Var::getInvalidState();
				}
			}
		}

	unsigned char
	aid	= Oscl::BT::Crypto::Keys::k4(key);

	appKey->setNewKey(
		key,
		aid
		);

	/*	Update persistence by destroying
		the old persistence record and
		creating a new one.
	 */

	Oscl::BT::Mesh::Key::Persist::Api*
	persistKey	= findKeyPersistApi(appKey->index());

	if(persistKey){
		_keyPersistApi.destroy(*persistKey);
		}

	_keyPersistApi.create(
		appKey->index(),
		appKey->key(),
		appKey->id(),
		appKey->newKey(),
		appKey->newID()
		);

	return 0;
	}

bool Part::iterateTX(Oscl::BT::Mesh::Key::Iterator::Api& iterator) noexcept{

	for(
		Oscl::BT::Mesh::Key::Record*
		record	= _appKeys.first();
		record;
		record	= _appKeys.next(record)
		){
		/* FIXME:
			- _networkKeyPhase 0
				- TX with active key
			- _networkKeyPhase 1
				- TX with old key
			- _networkKeyPhase 2
				- TX with new key
			- _networkKeyPhase 3
				- TX with new/active key
		 */

		const uint8_t*	key	= 0;
		uint8_t			id	= 0;

		if(record->updating()){

			switch(_networkKeyPhase) {
				case 2:
				case 3:
					key	= record->newKey();
					id	= record->newID();
					break;
				case 0:
				case 1:
				default:
					key	= record->key();
					id	= record->id();
					break;
				}

			}
		else {
			key	= record->key();
			id	= record->id();
			}

		bool
		stop	= iterator.item(
					record->index(),
					key,
					id
					);
		if(stop){
			return true;
			}
		}

	return false;
	}

bool Part::iterateRX(Oscl::BT::Mesh::Key::Iterator::Api& iterator) noexcept{

	for(
		Oscl::BT::Mesh::Key::Record*
		record	= _appKeys.first();
		record;
		record	= _appKeys.next(record)
		){
		/* FIXME:
			- _networkKeyPhase 0
				- RX with active key
			- _networkKeyPhase 1
				- RX with both keys
			- _networkKeyPhase 2
				- RX with both keys
			- _networkKeyPhase 3
				- RX with new/active key
		 */

		if(record->updating()){

			switch(_networkKeyPhase) {
				case 1:
				case 2: {
					bool
					stop	= iterator.item(
								record->index(),
								record->newKey(),
								record->newID()
								);
					if(stop){
						return true;
						}
					}
					break;
				case 3: {
					bool
					stop	= iterator.item(
								record->index(),
								record->newKey(),
								record->newID()
								);
					if(stop){
						return true;
						}
					}
					return false;
				case 0:
				default:
					break;
				}
			}

		bool
		stop	= iterator.item(
					record->index(),
					record->key(),
					record->id()
					);
		if(stop){
			return true;
			}
		}

	return false;
	}

void	Part::attach(Oscl::BT::Mesh::Beacon::Secure::Network::Api& secureNetworkBeaconApi) noexcept{
	_secureNetworkBeaconApi	= &secureNetworkBeaconApi;
	}

void	Part::start() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::log(
		"\tKey Index: (0x%4.4X) %u\n"
		"\tUnicast Address: 0x%4.4X\n"
		"\tNID: 0x%2.2X\n"
		"",
		_keyIndex,
		_keyIndex,
		_unicastAddress,
		_activeNetKey->credentials()._nid
		);
	#endif

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tEncryption Key:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->credentials()._encryptionKey,
		sizeof(_activeNetKey->credentials()._encryptionKey)
		);
	Oscl::Error::Info::log(
		"\tPrivacy Key:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->credentials()._privacyKey,
		sizeof(_activeNetKey->credentials()._privacyKey)
		);
	Oscl::Error::Info::log(
		"\tActive Net Key:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->netKey(),
		16
		);
	if(_newNetKey){
		Oscl::Error::Info::log(
			"\tNew Net Key:\n"
			);
		Oscl::Error::Info::hexDump(
			_newNetKey->netKey(),
			16
			);
		}
	Oscl::Error::Info::log(
		"\tBeacon Key:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->beaconKey(),
		16
		);
	Oscl::Error::Info::log(
		"\tNetwork ID:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->networkID(),
		8
		);
	Oscl::Error::Info::log(
		"\tIdentity Key:\n"
		);
	Oscl::Error::Info::hexDump(
		_activeNetKey->identityKey(),
		16
		);
	#endif

	if(_secureNetworkBeaconApi){
		_secureNetworkBeaconApi->start();
		}

	publishHeartbeat();
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		Oscl::ErrorFatal::logAndExit(
			"%s: Double stop()!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	_stopping	= true;

	if(_secureNetworkBeaconApi){
		_secureNetworkBeaconApi->stop();
		}

	for(
		Oscl::BT::Mesh::Element::Symbiont::Api*
		element	= _elementList.first();
		element;
		element	= _elementList.next(element)
		){
		Oscl::Error::Info::log(
			"%s: element: %p\n",
			__PRETTY_FUNCTION__,
			element
			);
		element->stop();
		}

	stopAllTimers();

	stopReassmeblers();

	// Flush _relayQ (NetMessage)

	flushRelayQ();

	// Flush _netQ (NetMessage)
	flushNetQ();

	// Flush _netRetransmitQ (NetMessage)
	flushRetransmitQ();

	// Flush _loopQ (Oscl::Pdu::Pdu)
	flushLoopQ();

	// Flush _freeNetMessageMem
	//	- Need to free() ENABLE_BT_MESH_NET_DYNAMIC_NETMESSAGEMEM mallocs or eliminate functionality.
	//		- possibly have a separate queue for these?

	// Wait or allFreeNotification.
	if(!_freeStore.allMemoryIsFree()){
		Oscl::Error::Info::log(
			"%s: !_freeStore.allMemoryIsFree()\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	_context.stopped(*this);
	}

void	Part::stopAllTimers() noexcept{
	_relayTxTimerApi.stop();
	_netTxTimerApi.stop();
	_netReTxTimerApi.stop();
	_heartbeatTxTimerApi.stop();
	_heartbeatSubTimerApi.stop();
	}

void	Part::stopAccessReassmeblers() noexcept{
	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*	reaper;
	while((reaper = _activeAccessReapers.get())){
		reaper->stop();
		}
	}

void	Part::stopControlReassmeblers() noexcept{
	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Control::Reaper*	reaper;
	while((reaper = _activeControlReapers.get())){
		reaper->stop();
		}
	}
void	Part::stopReassmeblers() noexcept{
	stopAccessReassmeblers();
	stopControlReassmeblers();
	}

void	Part::flushRelayQ() noexcept{
	NetMessage*	netMsg;

	while((netMsg=_relayQ.get())){
		freeNetMessage(netMsg);
		}
	}

void	Part::flushNetQ() noexcept{
	NetMessage*	netMsg;

	while((netMsg=_netQ.get())){
		freeNetMessage(netMsg);
		}
	}

void	Part::flushRetransmitQ() noexcept{
	NetMessage*	netMsg;

	while((netMsg=_netRetransmitQ.get())){
		freeNetMessage(netMsg);
		}
	}

void	Part::flushLoopQ() noexcept{
	Oscl::Handle<Oscl::Pdu::Pdu>	pdu;

	while((pdu = _loopQ.get())){
		pdu	= 0;	// Release referenced pdu
		}
	}

void	Part::dumpStatus(Oscl::Print::Api& printApi) noexcept{
	printApi.print(
		"Network status:\n"
		" _netTxCount: %u\n"
		" _relayQ empty: %s\n"
		" _netQ empty: %s\n"
		" _netRetransmitQ empty: %s\n"
		" _loopQ empty: %s\n"
		" _freeNetMessageMem empty: %s\n"
		"",
		_netTxCount,
		_relayQ.first()?"false":"true",
		_netQ.first()?"false":"true",
		_netRetransmitQ.first()?"false":"true",
		_loopQ.first()?"false":"true",
		_freeNetMessageMem.first()?"false":"true"
		);
	}


const void*	Part::getNetKey() const noexcept{

	const void*	key	= getTxNetKey().netKey();

	return key;
	}

const void*	Part::getBeaconKey() const noexcept{

	const void*	key	= getTxNetKey().beaconKey();

	return key;
	}

uint16_t	Part::getKeyIndex() const noexcept{
	return _keyIndex;
	}

uint8_t		Part::getFlags() const noexcept{

	uint32_t	ivIndex;
	bool		ivUpdateFlag;
	bool		keyRefreshFlag;

	_nodeApi.getIvIndexState(
		ivIndex,
		ivUpdateFlag,
		keyRefreshFlag
		);

	uint8_t	ivUpdateFlagMask	= ivUpdateFlag?(1<<1):(0<<1);
	uint8_t	keyRefreshFlagMask	= keyRefreshFlag?(1<<0):(0<<0);

	uint8_t
	flags	= ivUpdateFlagMask | keyRefreshFlagMask;

	return flags;
	}

uint32_t	Part::getTxIvIndex() const noexcept{
	return _nodeApi.getTxIvIndex();
	}

uint16_t	Part::getUnicastAddress() const noexcept{
	return _unicastAddress;
	}

const void*	Part::getNetworkID() const noexcept{
	return getTxNetKey().networkID();
	}

const void*	Part::getIdentityKey() const noexcept{

	const void*	key	= getTxNetKey().identityKey();

	return key;
	}

Oscl::BT::Mesh::Key::Api&	Part::getAppKeyApi() noexcept{
	return *this;
	}

uint8_t	Part::getNodeIdentityState() const noexcept{
	return _nodeIdentityState;
	}

void	Part::setNodeIdentityState(uint8_t state) noexcept{
	_nodeIdentityState	= state;

	_networkPersistApi.setNodeIdentityState(state);
	}

Oscl::BT::Mesh::Element::Api*	Part::findElement(uint16_t unicastAddress) noexcept{

	for(
		Oscl::BT::Mesh::Element::Symbiont::Api*
		element	= _elementList.first();
		element;
		element	= _elementList.next(element)
		){
		bool
		match	= element->match(unicastAddress);
		if(match){
			Oscl::BT::Mesh::Element::Api&
			elementApi	= element->getElementApi();
			return &elementApi;
			}
		}

	return 0;
	}

bool	Part::updateKey(const uint8_t key[16]) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	This is invoked by Config NetKey Update.
		This causes us to transition to Key Refresh Phase 1.
	 */

	_newNetKeyCandidate	= key;

	_keyRefreshState.configNetKeyUpdate();

	return _configNetKeyUpdateFailed;
	}

bool	Part::deleteNetwork() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s: NEED IMPLEMENTATION\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// TODO:

	return true;
	}

bool	Part::setFriendState(uint8_t state) noexcept{

	if(_friendState == 0x02){
		// Not supported
		return true;
		}

	_friendState	= state;

	_networkPersistApi.setFriendState(state);

	switch(_friendState){
		case 0x00:
			_context.disableFriendFeature();
			break;
		case 0x01:
			_context.enableFriendFeature();
			break;
		default:
			break;
		}

	return false;
	}

uint8_t	Part::getFriendState() noexcept{
	return _friendState;
	}

void	Part::setNetworkKeyPhase(uint8_t transition) noexcept{

	switch(transition){
		case 2:
			_keyRefreshState.configKeyRefreshPhaseSet2();
			break;
		case 3:
			_keyRefreshState.configKeyRefreshPhaseSet3();
			break;
		default:
			break;
		}
	}

uint8_t	Part::getNetworkKeyPhase() noexcept{
	return _networkKeyPhase;
	}

void	Part::getConfigHeartbeatSubscriptionStatus(
			uint16_t&	source,
			uint16_t&	destination,
			uint8_t&	periodLog,
			uint8_t&	countLog,
			uint8_t&	minHops,
			uint8_t&	maxHops
			) noexcept{

	source		= _configHeartbeatSubscriptionSource;
	destination	= _configHeartbeatSubscriptionDestination;
	periodLog	= logConvert(_configHeartbeatSubscriptionPeriod);
	countLog	= logConvert(_configHeartbeatSubscriptionCount);
	minHops		= _configHeartbeatSubscriptionMinHops;
	maxHops		= _configHeartbeatSubscriptionMaxHops;
	}

bool	Part::setConfigHeartbeatSubscription(
			uint16_t	source,
			uint16_t	destination,
			uint8_t		periodLog
			) noexcept{

	/*
		When an element receives a Config Heartbeat Subscription Set
		message, it shall update the identified Heartbeat Subscription
		state to the corresponding field values (defined in Table 4.122)
		and respond with a Config Heartbeat Subscription Status message,
		setting the Status field to Success and setting the Source,
		Destination, PeriodLog, CountLog, MinHops, and MaxHops fields
		with the current values of the corresponding fields of the
		Heartbeat Subscription state.

		If the Source or the Destination field is set to the unassigned address,
		or the PeriodLog field is set to 0x00, then the processing of received
		Heartbeat messages shall be disabled, the Heartbeat Subscription Source
		state shall be set to the unassigned address, the Heartbeat Subscription
		Destination state shall be set to the unassigned address, the
		Heartbeat Subscription MinHops state shall be unchanged,
		the Heartbeat Subscription MaxHops state shall be unchanged,
		and the Heartbeat Subscription Count state shall be unchanged.

	 */
	bool
	disabled	= (!source || !destination || !periodLog);

	if(disabled){
		_configHeartbeatSubscriptionSource		= 0;
		_configHeartbeatSubscriptionDestination	= 0;
		_configHeartbeatSubscriptionPeriod		= 0;
		_configHeartbeatSubscriptionCount		= 0;
		_configHeartbeatSubscriptionMinHops		= 0x7F;
		_configHeartbeatSubscriptionMaxHops		= 0;

		_networkPersistApi.setHeartbeatSubscription(
			0,	// source
			0,	// destination
			0	// periodLog
			);
		return false;
		}

	/*
		If the Source field is set to a unicast address, and the Destination
		field is set to a unicast address or group address, and the PeriodLog
		field is set to a non-zero value, then the processing of received
		Heartbeat messages shall be enabled, the Heartbeat Subscription MinHops
		state shall be set to 0x7F, the Heartbeat Subscription MaxHops state
		shall be set to 0x00, and the Heartbeat Subscription Count state
		shall be set to 0x0000.
	 */

	bool
	sourceIsValid	= Oscl::BT::Mesh::Address::isUnicastAddress(source);

	bool
	destinationIsValid	=
			Oscl::BT::Mesh::Address::isUnicastAddress(destination)
		||	Oscl::BT::Mesh::Address::isGroupAddress(destination)
		;

	if(!sourceIsValid || !destinationIsValid){
		// Invalid address
		return true;
		}

	_networkPersistApi.setHeartbeatSubscription(
		source,
		destination,
		periodLog
		);

	_heartbeatSubTimerApi.stop();

	_configHeartbeatSubscriptionSource		= source;
	_configHeartbeatSubscriptionDestination	= destination;
	_configHeartbeatSubscriptionPeriod		= (1<<periodLog);
	_configHeartbeatSubscriptionCount		= 0x0000;
	_configHeartbeatSubscriptionMinHops		= 0x7F;
	_configHeartbeatSubscriptionMaxHops		= 0x00;

	_heartbeatSubTimerApi.start(1000);

	return false;
	}

void	Part::getLowPowerNodePollTimeout(
			uint16_t	lpnAddress,
			uint32_t&	pollTimeout
			) noexcept{

	/*
		The LPNAddress field shall contain the primary unicast
		address of the Low Power node.

		The PollTimeout field shall contain the current value
		of the PollTimeout timer of the Low Power node
		within a Friend node, or 0x000000 if the node is not
		a Friend node for the Low Power node identified by
		LPNAddress.
	 */

	// FIXME: Implement when Friend feature is
	// implemented and present.

	pollTimeout	= 0x000000;
	}

void	Part::getNetworkTransmit(
			uint8_t&	networkTransmitCount,
			uint8_t&	networkTransmitIntervalSteps
			) noexcept{

	networkTransmitCount			= _networkTransmitCount;
	networkTransmitIntervalSteps	= _networkTransmitIntervalSteps;
	}

void	Part::setNetworkTransmit(
			uint8_t	networkTransmitCount,
			uint8_t	networkTransmitIntervalSteps
			) noexcept{

	/*	4.2.19.1 Network Transmit Count
		The Network Transmit Count field is a 3-bit value
		that controls the number of message transmissions of
		the Network PDU **originating** from the node.
		The number of transmissions is the Transmit Count + 1.
	 */
	_networkTransmitCount			= networkTransmitCount;

	/*	4.2.19.2 Network Transmit Interval Steps
		The Network Transmit Interval Steps field is a 5-bit
		value representing the number of 10 millisecond steps
		that controls the interval between message transmissions
		of Network PDUs **originating** from the node.

		The transmission interval is calculated using the formula:
			transmission interval = (Network Retransmit Interval Steps + 1) * 10

		Each transmission should be perturbed by a random
		value between 0 to 10 milliseconds between each transmission.
	 */
	_networkTransmitIntervalSteps	= networkTransmitIntervalSteps;

	_networkPersistApi.setNetworkTransmit(
		networkTransmitCount,
		networkTransmitIntervalSteps
		);
	}

void	Part::attach(
			Oscl::BT::Mesh::Element::Symbiont::Api&	symbiont
			) noexcept{

	symbiont.start();

	_elementList.put(&symbiont);
	}

void	Part::detach(	
			Oscl::BT::Mesh::Element::Symbiont::Api&	symbiont
			) noexcept{

	if(_elementList.remove(&symbiont)){
		symbiont.stop();
		}
	}

void	Part::transmitNetwork(
			Oscl::Pdu::Pdu* pdu,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			uint8_t			ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"",
		__PRETTY_FUNCTION__,
		ttl
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= pdu;

	uint8_t	payload[128];

	unsigned
	payloadLen	= handle->read(
					payload,
					sizeof(payload),
					0	// offset
					);

	// TODO: implement
	if(ttl > 0x80){
		// Use default network TTL
		ttl	= _defaultTTL;
		}
	else if(ttl == 1){
		Oscl::Error::Info::log(
			"%s: Specified TTL (1) is prohibited. Using 2 instead.\n",
			OSCL_PRETTY_FUNCTION
			);
		ttl	= 2;
		}

	transmitNetwork(
		false,	// ctl
		ttl,
		src,
		dst,
		seq,
		ivIndex,
		payload,
		payloadLen
		);
	}

void	Part::transmitControl(
			Oscl::Pdu::Pdu* pdu,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			uint8_t			ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	uint8_t	buffer[12];
	unsigned
	len	= pdu->read(
			buffer,
			sizeof(buffer),
			0	// offset
			);
	Oscl::Error::Info::hexDump(
		buffer,
		len
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= pdu;

	uint8_t	payload[128];

	unsigned
	payloadLen	= handle->read(
					payload,
					sizeof(payload),
					0	// offset
					);

	// TODO: implement
	if(ttl > 0x80){
		// Use default network TTL
		ttl	= _defaultTTL;
		}
	else if(ttl == 1){
		Oscl::Error::Info::log(
			"%s: Specified TTL (1) is prohibited. Using 2 instead.\n",
			OSCL_PRETTY_FUNCTION
			);
		ttl	= 2;
		}

	transmitNetwork(
		true,	// ctl
		ttl,
		src,
		dst,
		seq,
		ivIndex,
		payload,
		payloadLen
		);
	}

const Oscl::BT::Mesh::Network::Crypto::Credentials&	Part::getTxCredentials() const noexcept{

	switch(_networkKeyPhase) {
		case 0:
		case 1:
			return _activeNetKey->credentials();
		case 2:
		case 3:
			return _newNetKey->credentials();
		default:
			for(;;);
			Oscl::ErrorFatal::logAndExit(
				"Invalid Network Key Phase\n"
				);
			/*	Make the compiler happy.
				Oscl::ErrorFatal::logAndExit() never returns.
			 */
			return *(Oscl::BT::Mesh::Network::Crypto::Credentials*)0;
		}
	}

const NetKey&	Part::getTxNetKey() const noexcept{

	const NetKey*	key = 0;

	switch(_networkKeyPhase) {
		case 0:
		case 1:
			key	= _activeNetKey;
			break;
		case 2:
		case 3:
			key	= _newNetKey;
			break;
		default:
			Oscl::ErrorFatal::logAndExit(
				"Invalid Network Key Phase\n"
				);
			/*	Make the compiler happy.
				Oscl::ErrorFatal::logAndExit() never returns.
			 */
			return *(NetKey*)0;
		}

	if(!key){
		Oscl::Error::Info::log(
			"%s: key == 0\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return *key;
	}

void	Part::transmitNetwork(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint16_t	dst,
			uint32_t	seq,
			uint32_t	ivIndex,
			const void*	transportPDU,
			unsigned	tranportPduLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"\tctl: %s\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"\tivIndex: 0x%8.8X\n"
		"\ttransportPDU:\n"
		"",
		__PRETTY_FUNCTION__,
		ttl,
		ctl?"true":"false",
		src,
		dst,
		seq,
		ivIndex
		);
	Oscl::Error::Info::hexDump(
		transportPDU,
		tranportPduLength
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			ctl,
			ttl,
			src,
			seq,
			ivIndex,
			transportPDU,
			tranportPduLength
			);

	if(!pdu){
		return;
		}

	/*	It is possible for an element
		to send a network message to
		an element (or elements) on this
		same node.

		If we decide to send the message
		to the loop bearer interface, there
		is no need to send it multiple times
		as the local loop bearer is reasonably
		reliable.
	 */
	if(Oscl::BT::Mesh::Address::isUnicastAddress(dst)){
		if(addressWantedLocally(dst)){
			/*
				If the destination is a unicast
				address on this node, then we
				send the message to the loop
				bearer and there is no need to
				send the message on any other
				network interface.
			 */
			loopBearerTx(
				pdu,
				dst
				);
			return;
			}
		}
	else if(addressWantedLocally(dst)){
		// At this point, the destination
		// address is not a unicast address.

		/*
			If the destination is a group
			or virtual address that is wanted
			by this node, then we send the
			message to the loop bearer as
			well as all network interfaces.
		 */
		loopBearerTx(
			pdu,
			dst
			);
		}

	transmitNetworkPDU(pdu,dst);
	}

void	Part::transmitNetworkPDU(
			Oscl::Pdu::Pdu* pdu,
			uint16_t		dst
			) noexcept{

	// At this point, we are ready to send
	// the message to all network bearer
	// interfaces, perhaps some number of
	// times.

	NetMessageMem*
	mem	= allocNetMessageMem();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of net message memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	NetMessage*
	message	= new(mem)
				NetMessage(
					pdu,
					dst,
					_networkTransmitCount
					);

	// Transmit the first one NOW! That
	// is ahead of any pending re-transmissions.

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: queue (%p,0x%4.4X)\n",
		__PRETTY_FUNCTION__,
		&*message->_pdu,
		message->_dst
		);
	#endif

	_netQ.put(message);

	if(_netTxTimerApi.running()){
		return;
		}

	// At this point, we will start the network
	// transmission timer such "that a small
	// random delay is introduced between each
	// Network PDU.

	// retransmission interval = (Network Retransmit Interval Steps + 1) * 10

	unsigned	smallRandomIntervalInMs;

	Oscl::Entropy::Random::fetch(
        &smallRandomIntervalInMs,
        sizeof(smallRandomIntervalInMs)
        );

	smallRandomIntervalInMs	%= 10;	// is 10ms max enough?
	smallRandomIntervalInMs	+= 1;	// is 1ms min enough

	_netTxTimerApi.start(smallRandomIntervalInMs);
	}

Oscl::Pdu::Pdu*	Part::buildNetworkPDU(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex,
			const void*	transportPDU,
			unsigned	tranportPduLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"\tctl: %s\n"
		"\ttransportPDU:\n"
		"",
		__PRETTY_FUNCTION__,
		ttl,
		ctl?"true":"false"
		);
	Oscl::Error::Info::hexDump(
		transportPDU,
		tranportPduLength
		);
	#endif

	// At this point, we are ready
	// to build a new network PDU
	// (with a new obfuscated header)
	// and forward it to all bearers.

	unsigned	bufferSize;

	void*
	mem	= _freeStore.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned
	length	= Oscl::BT::Mesh::Network::Crypto::encrypt(
			getTxCredentials(),
			ctl,
			ttl,
			src,
			seq,
			ivIndex,
			transportPDU,
			tranportPduLength,
			mem,
			bufferSize
			);

	if(!length){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Pdu::Pdu*
	fixed	= _freeStore.allocFixed(
				mem,
				length
				);

	if(!fixed){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	return fixed;
	}

void	Part::relayMessage(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint16_t	dst,
			uint32_t	seq,
			uint32_t	ivIndex,
			const void*	transportPDU,
			unsigned	tranportPduLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\t_relayEnabled: %s\n"
		"\tttl: %u\n"
		"",
		__PRETTY_FUNCTION__,
		_relayEnabled?"true":"false",
		ttl
		);
	#endif

	if(_loopLocked){
		// We don't want to relay
		// messages that we received
		// via the loopback interface.
		return;
		}

	if(!_relayEnabled){
		return;
		}

	if(ttl < 2){
		// Time-to-live has expired.
		return;
		}

	--ttl;

	// At this point, we are ready
	// to build a new network PDU
	// (with a new obfuscated header)
	// and forward it to all bearers.

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			ctl,
			ttl,
			src,
			seq,
			ivIndex,
			transportPDU,
			tranportPduLength
			);

	if(!pdu){
		return;
		}

	/*	3.4.6.3 Receiving a Network PDU

		If the message delivered from the advertising bearer
		is processed by the lower transport layer, the Relay
		feature is supported and enabled, the TTL field has a
		value of 2 or greater, and the destination is not a
		unicast address of an element on this node, then the
		TTL field value shall be decremented by 1, the
		Network PDU shall be tagged as relay, and the Network
		PDU shall be retransmitted to all network interfaces
		connected to the advertising bearer. It is recommended
		that a small random delay is introduced between receiving
		a Network PDU and relaying a Network PDU to avoid
		collisions between multiple relays that have received
		the Network PDU at the same time.

		If the message delivered from the GATT bearer is
		processed by the lower transport layer, and the Proxy
		feature is supported and enabled, and the TTL field
		has a value of 2 or greater, and the destination is not
		a unicast address of an element on this node, then the
		TTL field value shall be decremented by 1, and the
		Network PDU shall be retransmitted to all network
		interfaces.

		If the message delivered from the advertising bearer
		is processed by the lower transport layer, and the
		Proxy feature is supported and enabled, and the TTL
		field has a value of 2 or greater, and the destination
		is not a unicast address of an element on this node,
		then the TTL field shall be decremented by 1 and the
		Network PDU shall be retransmitted to all network
		interfaces connected to the GATT bearer.
	 */

	NetMessageMem*
	mem	= allocNetMessageMem();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of relay message memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	NetMessage*
	message	= new(mem)
				NetMessage(
					pdu,
					dst,
					_relayRetransmitCount+1
					);

	_relayQ.put(message);

	if(_relayTxTimerApi.running()){
		// At this point, we assume that the
		// relay retransmit timer is already
		// running.
		return;
		}

	// At this point, we will start the relay
	// retransmission timer such "that a small
	// random delay is introduced between receiving
	// a Network PDU and relaying a Network PDU to avoid
	// collisions between multiple relays that have received
	// the Network PDU at the same time

	// retransmission interval = (Relay Retransmit Interval Steps + 1) * 10

	unsigned	smallRandomIntervalInMs;

	Oscl::Entropy::Random::fetch(
        &smallRandomIntervalInMs,
        sizeof(smallRandomIntervalInMs)
        );

	smallRandomIntervalInMs	%= 20;	// is 21ms max enough?
	smallRandomIntervalInMs	+= 1;	// is 1ms min enough

	_relayTxTimerApi.start(smallRandomIntervalInMs);
	}

bool	Part::rxMeshMessage(
			const void*		frame,
			unsigned 		length,
			signed			rssi
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_rssi	= rssi;

	bool
	result	= rxNetwork(
				frame,
				length
				);

	_rssi	= 127;	// RSSI is not available

	return result;
	}

bool	Part::rxNetwork(
			const void*					frame,
			unsigned 					length,
			Oscl::BT::Mesh::
			Network::Interface::Api*	ifApi
			) noexcept{

	// <<Mesh Message>>

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s <<Mesh Message>>\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		plaintext[128];
	uint32_t	ivIndex;
	uint8_t		ctl;
	uint8_t		ttl;
	uint32_t	seq;
	uint16_t	src;

	unsigned	len	= 0;

	for(
		NetKey*
		netKey	= _netKeys.first();
		netKey;
		netKey	= _netKeys.next(netKey)
		) {
		len	= Oscl::BT::Mesh::Network::Crypto::decrypt(
				_nodeApi,
				netKey->credentials(),
				frame,
				length,
				plaintext,
				ivIndex,
				ctl,
				ttl,
				seq,
				src
				);
		if(len){
			break;
			}
		}

	if(!len){

		if(!_friendshipApi){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: decrypt failed. no friend\n",
				__PRETTY_FUNCTION__
				);
			#endif
			return false;
			}

		bool
		failed	= _friendshipApi->rxNetwork(frame,length);

		if(failed){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: decrypt failed. unwanted by friends.\n",
				__PRETTY_FUNCTION__
				);
			#endif
			}

		return failed;
		}

	// At this point, we have received a valid
	// Network PDU for this network.

	uint16_t	dst;
	uint8_t		segOpcode;	// Needed for heartbeat test
	unsigned	remaining;
	unsigned	lowerTransportPduOffset;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			plaintext,
			len
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		decoder.decode(dst);

		remaining	= decoder.remaining();

		decoder.decode(segOpcode);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			// Return true, since there's something
			// wrong with this PDU (or the preceding
			// software, and we don't want to relay
			// this PDU.
			return true;
			}

		lowerTransportPduOffset	= len - remaining;
		}

	if(ifApi){
		ifApi->netPacketInfo(src);

		/*	In the current implementation, only the
			GATT Proxy provides an ifApi, which is
			used to maintain a white-list filter.
		 */

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s from GATT Proxy :"
			" ctl: (%u) \"%s\","
			" ttl: (0x%2.2X) %u,"
			" seq: (0x%6.6X) %u,"
			" src: (0x%4.4X)"
			" dst: (0x%4.4X)"
			" ivIndex: (0x%8.8X)"
			"\n",
			__PRETTY_FUNCTION__,
			ctl,
			ctl?"control":"network",
			ttl,
			ttl,
			seq,
			seq,
			src,
			dst,
			ivIndex
			);
		#endif
		}

	/*	At this point, before we do cache processing,
		we're going to do special processing for
		heartbeat messages.
	 */
	if(ctl){
		bool
		wantsDestination	= addressWantedLocally(dst);

		/*	segOpcode contains both the opcode and
			the segmented flag. Heartbeats are always
			unsegmented.
		 */
		if(segOpcode == 0x0A && wantsDestination){
			processUnsegmentedHeartbeat(
				src,
				dst,
				seq,
				ttl,
				&plaintext[lowerTransportPduOffset],	// lowerTransportPDU
				remaining
				);
			return true;
			}
		}

	bool
	duplicate	= _cache.messageInCache(
					src,
					seq,
					ivIndex
					);

	if(duplicate){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"Duplicate network message (src:0x%4.4X,seq:0x%6.6X,ivIndex:0x%8.8X) in cache. Dropped.\n",
			src,
			seq,
			ivIndex
			);
		#endif
		return true;
		}

	_cache.addToCache(
		src,
		seq,
		ivIndex
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ctl: (%u) \"%s\","
		" ttl: (0x%2.2X) %u,"
		" seq: (0x%6.6X) %u,"
		" src: (0x%4.4X)"
		" dst: (0x%4.4X)"
		" ivIndex: (0x%8.8X)"
		"\n",
		__PRETTY_FUNCTION__,
		ctl,
		ctl?"control":"network",
		ttl,
		ttl,
		seq,
		seq,
		src,
		dst,
		ivIndex
		);
	#endif

	// FIXME: _friendshipApi.rxLpnPDU() gets
	// called twice. Here and below!
	// It think that is wrong and wasteful.
	if(_friendshipApi){
		_friendshipApi->rxLpnPDU(
			&plaintext[lowerTransportPduOffset],
			remaining,
			ivIndex,
			seq,
			src,
			dst,
			ctl,
			ttl
			);
		}

	bool
	locallyDestined	= rxLocalNetworkPDU(
						&plaintext[lowerTransportPduOffset],
						remaining,
						ivIndex,
						seq,
						src,
						dst,
						ctl,
						ttl
						);

	if(locallyDestined){
		return true;
		}

	if(_friendshipApi){
		locallyDestined	= _friendshipApi->rxLpnPDU(
							&plaintext[lowerTransportPduOffset],
							remaining,
							ivIndex,
							seq,
							src,
							dst,
							ctl,
							ttl
							);

		if(locallyDestined){
			return true;
			}
		}

	//	At this point, the destination address was
	//	not specifically for destined for this node.
	//	Therefore, we will perform relay processing.

	relayMessage(
		ctl,
		ttl,
		src,
		dst,
		seq,
		ivIndex,
		plaintext,
		len
		);

	return true;
	}

void	Part::encryptProxy(
			Oscl::Pdu::FWD::Api&	fwdApi,
			Oscl::Pdu::Pdu* 		pdu
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= pdu;

	uint8_t	payload[128];

	static const uint16_t	dstAddress = 0x0000; // Always unassigned for Proxy Configuration PDU
		{
		Oscl::Encoder::BE::Base
		dstAddrEncoderBae(
			payload,
			sizeof(dstAddress)
			);

		Oscl::Encoder::Api&	dstAddrEncoder	= dstAddrEncoderBae;

		dstAddrEncoder.encode(dstAddress);
		}

	unsigned
	payloadLen	= handle->read(
					&payload[2],
					sizeof(payload)-sizeof(dstAddress),
					0	// offset
					);

	payloadLen	+= sizeof(dstAddress);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		payload,
		payloadLen
		);
	#endif

	// Proxy PDUs are required to have a TTL of zero.
	constexpr uint8_t	ttl	= 0x00;

	unsigned	bufferSize;

	void*
	mem	= _freeStore.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	//////
	uint32_t	seqNumber	= _nodeApi.allocateSequenceNumber();

	constexpr uint8_t		ctl	= 1;	// Proxy PDU is always control

	static const uint8_t	ctlTTL	= (
									(ctl<<7)
								|	(ttl<<0)
								);

	uint8_t	header[dstOffset - sequenceNumberOffset];
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			header,
			sizeof(header)
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		encoder.encode24Bit(seqNumber);
		encoder.encode(_unicastAddress);
		}

	/* Proxy Nonce
		offset
		[0]		Nonce Type		0x00
		[1]		0x00
		[2:4]	SEQ
		[5:6]	SRC
		[7:8]	Pad				0x0000
		[9:12]	IV Index
	 */

	const uint32_t	ivIndex	= _nodeApi.getTxIvIndex();

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	nonceType	= 0x03;	// Proxy Nonce
		static const uint8_t	pad[]	= {0x00,0x00};

		nonceEnc.encode(nonceType);
		nonceEnc.encode(pad[0]);
		nonceEnc.copyIn(
			header,
			dstOffset - sequenceNumberOffset
			);

		nonceEnc.copyIn(
			pad,
			sizeof(pad)
			);

		nonceEnc.encode(ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	uint8_t*	cipherText	= (uint8_t*)mem;

	cipherText	= &cipherText[dstOffset];

	const Oscl::BT::Mesh::Network::Crypto::Credentials&
	credentials	= getTxCredentials();

	unsigned
	length	= Oscl::AES128::Ccm::encrypt(
				credentials._encryptionKey,
				nonce,
				0,	// adata
				0,	// alen
				payload,	// payload at destination address
				payloadLen,	// length
				ctl?ctlMicLength:netMicLength,
				cipherText
				);

	if(!length){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: payloadLen: %u, cipherTextLen: %u, cipherText:\n",
		__PRETTY_FUNCTION__,
		payloadLen,
		length
		);
	Oscl::Error::Info::hexDump(
		cipherText,
		length
		);
	#endif

	/*	Network PDU
		[0]		[ IVI[7] | NID[6:0] ]	clear
		[1]		[ CTL[7] | TTL[6:0] ]	obfuscated
		[2:4]	[ SEQ ]					obfuscated
		[5:6]	[ SRC ]					obfuscated
		[7:8]	[ DST ]					encrypted
		[9...]	[ Transport PDU ]		encrypted
		[...]	[ NetMIC ]
	 */

	uint8_t*	p	= (uint8_t*)mem;
		{
		Oscl::Encoder::BE::Base
		beHeaderEncoder(
			p,
			7
			);

		Oscl::Encoder::Api&	headerEncoder	= beHeaderEncoder;

		bool
		ivi	= (ivIndex & 0x00000001)?true:false;

		uint8_t
		iviNID	= (ivi)?0x80:0x00;
		iviNID	|= credentials._nid;

		headerEncoder.encode(iviNID);
		headerEncoder.encode(ctlTTL);
		headerEncoder.encode24Bit(seqNumber);
		headerEncoder.encode(_unicastAddress);
		}

	// Obfuscation
	uint8_t	pecbData[16];
		{
		Oscl::AES128::Cipher	pecb(credentials._privacyKey);

		Oscl::Encoder::BE::Base
		bePecbEncoder(
			pecbData,
			sizeof(pecbData)
			);

		Oscl::Encoder::Api&	pecbEncoder	= bePecbEncoder;

		static const uint8_t	zeros[5] = {0x00,0x00,0x00,0x00,0x00};

		constexpr unsigned privacyRandomLength	= 7;

		pecbEncoder.copyIn(
			zeros,
			sizeof(zeros)
			);

		pecbEncoder.encode(ivIndex);

		pecbEncoder.copyIn(
			cipherText,
			privacyRandomLength
			);

		pecb.cipher(pecbData);

		pecb.xorBlock(
			&p[obfuscatedDataOffset],
			&p[obfuscatedDataOffset],
			obfuscatedDataLength
			);
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	fixed	= _freeStore.allocFixed(
				mem,
				obfuscatedDataOffset + obfuscatedDataLength + length
				);

	if(!fixed){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	fwdApi.transfer(fixed);
	}

void	Part::segmentReceived(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{
	// This operation is not used in this context.
	}

void	Part::stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_activeAccessReapers.remove(&part);

	part.~Reaper();

	_freeAccessReaperMem.put((AccessReaperMem*)&part);
	}

void	Part::stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Control::Reaper& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_activeControlReapers.remove(&part);

	part.~Reaper();

	_freeControlReaperMem.put((ControlReaperMem*)&part);
	}

void	Part::rxLowerTransport(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" srcAddress: 0x%4.4X,"
		" dstAddress: 0x%4.4X,"
		" ivIndex: 0x%8.8X,"
		" seq: 0x%6.6X,"
		" ttl: 0x%2.2X"
		"\n",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		ivIndex,
		seq,
		ttl
		);
	#endif

	// At this point, we have determined that
	// we have at least one Element that wants
	// to receive this packet. We will now
	// proceed to perform Lower Transport
	// reassembly if required.

	// At this point, we have a Lower Transport PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6] - AKF Field
	//			0 - Device
	//			1 - Application
	//		[6:0] - AID Field

	uint8_t		segAkfAid;
	unsigned	remaining;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		decoder.decode(segAkfAid);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		remaining	= decoder.remaining();
		}

	bool
	segmented	= (segAkfAid >> 7)?true:false;

	bool
	akf	= ((segAkfAid >> 6) & 0x01)?true:false;

	uint8_t
	aid	= (segAkfAid & 0x3F);

	const uint8_t*	p = (const uint8_t*)lowerTransportPDU;

	unsigned
	upperTransportAccessPduOffset	= length - remaining;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: segmented: %s\n",
		__PRETTY_FUNCTION__,
		segmented?"true":"false"
		);
	#endif

	if(!segmented){

		_transportUpperRX.receive(
			&p[upperTransportAccessPduOffset],
			remaining,
			ivIndex,
			seq,
			srcAddress,
			dstAddress,
			aid,
			akf,
			false
			);
		return;
		}

	// At this point, we must reassemble the PDU

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: need reassbly\n"
		"\tSegment:\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		lowerTransportPDU,
		length
		);
	#endif

	bool	handled	= false;

	for(
			Oscl::BT::Mesh::Transport::
			Lower::Reassemble::Access::Reaper*
			reaper	= _activeAccessReapers.first();
			reaper;
			reaper	= _activeAccessReapers.next(reaper)
		){
		handled	= reaper->receive(
			srcAddress,
			dstAddress,
			seq,
			lowerTransportPDU,
			length
			);
		}

	if(handled){
		return;
		}

	AccessReaperMem*
	mem	= _freeAccessReaperMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of reaper memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*
	reaper	= new(&mem->mem.reaper)
				Oscl::BT::Mesh::Transport::
				Lower::Reassemble::Access::Reaper(
					*this,	// context,
					_freeStore,
					*mem->mem.ackTimer,
					*mem->mem.incompleteTimer,
					*this,
					_transportUpperRX,
					ivIndex,
					seq,
					srcAddress,
					dstAddress,
					ttl,
					aid,
					akf
					);

	_activeAccessReapers.put(reaper);

	reaper->start();

	reaper->receive(
		srcAddress,
		dstAddress,
		seq,
		lowerTransportPDU,
		length
		);

	return;
	}

void	Part::forwardUpperTransportPduToAllElements(
			const void*	upperTransportAccessPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tivIndex: 0x%8.8X\n"
		"\tseq: 0x%8.8X\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\taid: 0x%2.2X\n"
		"\takf: %s\n"
		"\tsegmented: %s\n"
		"",
		__PRETTY_FUNCTION__,
		ivIndex,
		seq,
		src,
		dst,
		aid,
		akf?"true":"false",
		segmented?"true":"false"
		);
	#endif

	_transportUpperRX.receive(
		upperTransportAccessPdu,
		pduLength,
		ivIndex,
		seq,
		src,
		dst,
		aid,
		akf,
		segmented
		);
	}

void	Part::rxUpperTransportControlPDU(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		ttl,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tivIndex: 0x%8.8X\n"
		"\tseq: 0x%8.8X\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\tttl: 0x%2.2X\n"
		"\topcode: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		ivIndex,
		seq,
		src,
		dst,
		ttl,
		opcode
		);
	#endif

	// NOTE: A friendship should only receive
	// Lower Transport PDUs, and must perform
	// reassembly itself... I think.
	}

void	Part::forwardLowerTransportAckPduToAllElements(
			const void*	lowerTransportAckPDU,
			unsigned	pduLength,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	for(
		Oscl::BT::Mesh::Element::Symbiont::Api*
		element	= _elementList.first();
		element;
		element	= _elementList.next(element)
		){
		element->rxLowerTransportACK(
			lowerTransportAckPDU,
			pduLength,
			seq,
			src,
			dst
			);
		}
	}

bool	Part::rxLocalNetworkPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ctl: %s"
		" ttl: %u"
		" src: 0x%4.4X"
		" dst: 0x%4.4X"
		" ivIndex: 0x%8.8X"
		" seq: 0x%6.6X"
		"\n",
		__PRETTY_FUNCTION__,
		ctl?"true":"false",
		ttl,
		src,
		dst,
		ivIndex,
		seq
		);

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif
	#endif

	bool	isUnicastDestAddress	= (dst & 0x8000)?false:true;

	bool
	wantsDestination	= addressWantedLocally(dst);

	bool
	locallyDestined	= (isUnicastDestAddress && wantsDestination);

	if(!locallyDestined && !wantsDestination){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (!locallyDestined (%d) && !wantsDestination (%d))\n",
			OSCL_PRETTY_FUNCTION,
			locallyDestined,
			wantsDestination
			);
		#endif
		return false;
		}

	if(ctl){
		// Transport Control Message
		processTransportControlMessage(
			src,
			dst,
			seq,
			ivIndex,
			ttl,
			frame,
			length
			);
		}
	else {
		rxLowerTransport(
			src,
			dst,
			ivIndex,
			seq,
			ttl,
			frame,
			length
			);
		}

	return locallyDestined;
	}

void	Part::sendHeartbeat() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(Oscl::BT::Mesh::Address::unassigned == _configHeartbeatPublishDestination){
		return;
		}

	uint8_t		payload[32];
	unsigned	length;
	{
		Oscl::Encoder::BE::Base
		beEncoder(
			payload,
			sizeof(payload)
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		// Unsegmented Control Message
		// [0]		opcode
		// [1:n]	parameters

		// Heartbeat
		//	[0]	initTTL
		//	[1:2] Features
		// 		0 - Relay
		// 		1 - Proxy
		// 		2 - Friend
		// 		3 - Low Power
		// 		4:15 - RFU
		uint8_t	opcode	= 0x0A;

		encoder.encode(_configHeartbeatPublishDestination);
		encoder.encode(opcode);
		encoder.encode(_configHeartbeatPublishTtl);
		encoder.encode(_configHeartbeatPublishFeatures);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		length	= encoder.length();
		}

	transmitNetwork(
		true,	// ctl
		_configHeartbeatPublishTtl,
		_unicastAddress,
		_configHeartbeatPublishDestination,
		allocSequenceNumber(),
		_nodeApi.getTxIvIndex(),
		payload,
		length
		);
	}

void	Part::processSegmentedTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length,
			uint8_t		opcode
			) noexcept{
	bool	handled	= false;

	for(
			Oscl::BT::Mesh::Transport::
			Lower::Reassemble::Control::Reaper*
			reaper	= _activeControlReapers.first();
			reaper;
			reaper	= _activeControlReapers.next(reaper)
		){
		handled	= reaper->receive(
			srcAddress,
			dstAddress,
			seq,
			lowerTransportPDU,
			length
			);
		}

	if(handled){
		return;
		}

	ControlReaperMem*
	mem	= _freeControlReaperMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of reaper memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Control::Reaper*
	reaper	= new(&mem->mem.reaper)
				Oscl::BT::Mesh::Transport::
				Lower::Reassemble::Control::Reaper(
					*this,	// context,
					_freeStore,
					*mem->mem.ackTimer,
					*mem->mem.incompleteTimer,
					*this,
					_upperTransportControlRxComposer,
					ivIndex,
					seq,
					srcAddress,
					dstAddress,
					ttl,
					opcode
					);

	_activeControlReapers.put(reaper);

	reaper->start();

	reaper->receive(
		srcAddress,
		dstAddress,
		seq,
		lowerTransportPDU,
		length
		);

	}

void	Part::processTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq
		);
	#endif

	// At this point, we have a Lower Transport Control PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6:0] - Opcode

	uint8_t	segOpcode;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		decoder.decode(segOpcode);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	bool
	segmented	= (segOpcode >> 7)?true:false;

	uint8_t
	opcode	= segOpcode & 0x7F;

	if(segmented){
		processSegmentedTransportControlMessage(
			srcAddress,
			dstAddress,
			seq,
			ivIndex,
			ttl,
			lowerTransportPDU,
			length,
			opcode
			);
		return;
		}

	// At this point, we have an unsegmented
	// control message.

	if(opcode){
		// Not an ACK
		if(opcode == 0x0A){
			/*
				Ignore heartbeat here since it has
				already been processed.
			 */
			return;
			}
		if(_friendshipApi){
			_friendshipApi->rxControl(
				lowerTransportPDU,
				length,
				ivIndex,
				seq,
				srcAddress,
				dstAddress,
				ttl,
				opcode
				);
			}
		return;
		}

	// At this point, we have a segment acknowledge
	// control message.

	const uint8_t*
	lowerTransportAckPdu	= (const uint8_t*)lowerTransportPDU;

	forwardLowerTransportAckPduToAllElements(
		lowerTransportAckPdu,
		length,
		seq,
		srcAddress,
		dstAddress
		);

	return;
	}

void	Part::processUnsegmentedHeartbeat(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"\tttl: %u\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq,
		ttl
		);
	Oscl::Error::Info::hexDump(
		lowerTransportPDU,
		length
		);
	#endif

	uint8_t		segOpcode;
	uint8_t		initTTL;
	uint16_t	features;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		// Heartbeat
		//	[0]	initTTL
		//	[1:2] Features
		// 		0 - Relay
		// 		1 - Proxy
		// 		2 - Friend
		// 		3 - Low Power
		// 		4:15 - RFU

		decoder.decode(segOpcode);
		decoder.decode(initTTL);
		decoder.decode(features);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	_context.recordHeartbeat(
		_keyIndex,
		srcAddress,
		dstAddress,
		ttl,
		initTTL,
		features,
		_rssi
		);

	if(_configHeartbeatSubscriptionDestination != dstAddress){
		return;
		}

	if(_configHeartbeatSubscriptionSource != srcAddress){
		return;
		}

	if(!_configHeartbeatSubscriptionPeriod){
		return;
		}

	/*	According to the Heartbeat message format defined
		in 3.6.5.10, the InitTTL field is 7 bits and the
		other bit in the octet is RFU. *However*, it is unclear
		whether the RFU bit is the MSB or the LSB. Here, we *assume*
		that the RFU bit is the MSB and we therefore clear it.
	 */
	initTTL	&= 0x7F;

	/*
		Upon receiving the Heartbeat message, a hops value
		shall be calculated using the InitTTL value from the
		message, and the received Network PDU TTL field value,
		known as RxTTL, as follows:
			hops = InitTTL - RxTTL +1
	 */
	uint8_t
	hops	= (initTTL - ttl) + 1;

	if(hops > _configHeartbeatSubscriptionMaxHops){
		_configHeartbeatSubscriptionMaxHops	= hops;
		}

	if(hops < _configHeartbeatSubscriptionMinHops){
		_configHeartbeatSubscriptionMinHops = hops;
		}

	if(_configHeartbeatSubscriptionCount < 0xFFFF){
		++_configHeartbeatSubscriptionCount;
		}
	}

bool	Part::decryptProxy(
			Oscl::Frame::FWD::Api&	fwdApi,
			const void*				frame,
			unsigned int			length
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	Network PDU
		[0]		[ IVI[7] | NID[6:0] ]	clear
		[1]		[ CTL[7] | TTL[6:0] ]	obfuscated
		[2:4]	[ SEQ ]					obfuscated
		[5:6]	[ SRC ]					obfuscated
		[7:8]	[ DST ]					encrypted
		[9...]	[ Transport PDU ]		encrypted
		[...]	[ NetMIC ]
	 */

	constexpr unsigned	maxDecryptedMessageSize	= 512;	// FIXME: need *real* value
	constexpr unsigned	iviNidOffset		= 0;

	uint8_t	plainTextPDU[maxDecryptedMessageSize];

	const uint8_t*	p	= (const uint8_t*)frame;

	plainTextPDU[iviNidOffset]	= p[0];

	uint8_t	iviNID	= p[0];

	uint8_t	nid		= iviNID & 0x7F;

	uint8_t	ivi		= iviNID >> 7;

	NetKey*	netKey;

	for(
		netKey	= _netKeys.first();
		netKey;
		netKey	= _netKeys.next(netKey)
		) {
		if(nid == netKey->credentials()._nid){
			break;
			}
		}

	if(!netKey){
		// Not destined for this network
		return false;
		}

	const Oscl::BT::Mesh::Network::Crypto::Credentials&
	credentials	= netKey->credentials();

	uint32_t	ivIndex	= _nodeApi.getRxIvIndex(ivi);

	if(length > maxDecryptedMessageSize){
		Oscl::Error::Info::log(
			"%s: Frame too long for buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	const uint8_t*	privacyRandom	= &p[dstOffset];

	Oscl::AES128::Cipher	pecb(credentials._privacyKey);

	uint8_t	pecbData[16];
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			pecbData,
			sizeof(pecbData)
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		static const uint8_t	zeros[5] = {0x00,0x00,0x00,0x00,0x00};

		constexpr unsigned privacyRandomLength	= 7;

		encoder.copyIn(zeros,sizeof(zeros));
		encoder.encode(ivIndex);
		encoder.copyIn(privacyRandom,privacyRandomLength);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: pecb overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return false;
			}
		}

	pecb.cipher(pecbData);

	pecb.xorBlock(
		&p[obfuscatedDataOffset],
		&plainTextPDU[obfuscatedDataOffset],
		obfuscatedDataLength
		);

	uint8_t		ctlTTL;
	uint32_t	seq;
	uint16_t	src;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			&plainTextPDU[obfuscatedDataOffset],
			sizeof(plainTextPDU)
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		decoder.decode(ctlTTL);
		decoder.decode24Bit(seq);
		decoder.decode(src);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: header underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return false;
			}
		}

	static const uint8_t	ctl	= ctlTTL >> 7;

	#ifdef DEBUG_TRACE
	uint8_t	ttl	= ctlTTL & 0x7F;

	Oscl::Error::Info::log(
		"%s:"
		" ctl: (%u) \"%s\","
		" ttl: (0x%2.2X) %u,"
		" seq: (0x%6.6X) %u,"
		" src: (0x%4.4X)"
		"\n",
		__PRETTY_FUNCTION__,
		ctl,
		ctl?"control":"network",
		ttl,
		ttl,
		seq,
		seq,
		src
		);
	Oscl::Error::Info::hexDump(
		plainTextPDU,
		dstOffset
		);
	#endif

	/* Proxy Nonce
		offset
		[0]		Nonce Type		0x00
		[1]		0x00
		[2:4]	SEQ
		[5:6]	SRC
		[7:8]	Pad				0x0000
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	nonceType	= 0x03;	// Proxy Nonce
		static const uint8_t	pad[]	= {0x00,0x00};
		nonceEnc.encode(nonceType);
		nonceEnc.encode(pad[0]);
		nonceEnc.copyIn(
			&plainTextPDU[sequenceNumberOffset],
			dstOffset - sequenceNumberOffset
			);
		nonceEnc.copyIn(
			pad,
			sizeof(pad)
			);
		nonceEnc.encode(ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	const unsigned	encryptedLength	= length - dstOffset;

	unsigned
	len	= Oscl::AES128::Ccm::decrypt(
			credentials._encryptionKey,
			nonce,
			0,
			0,
			&p[dstOffset],
			encryptedLength,
			ctl?ctlMicLength:netMicLength,
			&plainTextPDU[dstOffset]
			);

	if(!len){
		Oscl::Error::Info::log(
			"%s: decrypt failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	fwdApi.forward(
		plainTextPDU,
		len+dstOffset
		);

	return true;
	}

uint32_t	Part::allocSequenceNumber() noexcept{
	return _nodeApi.allocateSequenceNumber();
	}

uint8_t		Part::getDefaultTTL() const noexcept{
	return _defaultTTL;
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	for(
		Oscl::BT::Mesh::Element::Symbiont::Api*
		element	= _elementList.first();
		element;
		element	= _elementList.next(element)
		){
		if(!element->wantsDestination(dst)){
			continue;
			}

		element->processAccessMessage(
			frame,
			length,
			src,
			dst,
			appKeyIndex
			);
		}
	}

const void*	Part::getDeviceKey() const noexcept{
	return _nodeApi.getDeviceKey();
	}

Oscl::BT::Mesh::Subscription::Register::Api&
Part::getSubscriptionRegisterApi() noexcept{
	return _subscriptionRegister;
	}

void	Part::netTxTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Queue<NetMessage>	_tmpQ;

	NetMessage*
	msg;

	while((msg = _netQ.get())){
		if(msg->_txCount){
			--msg->_txCount;
			}

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _bearerApi.transmit(%p,0x%4.4X)\n",
			__PRETTY_FUNCTION__,
			&*msg->_pdu,
			msg->_dst
			);
		#endif

		_bearerApi.transmit(
			msg->_pdu,
			msg->_dst
			);

		++_netTxCount;

		if(!msg->_txCount){
			freeNetMessage(msg);
			continue;
			}

		if(Oscl::BT::Mesh::Address::isUnicastAddress(msg->_dst)){
			if(_nodeApi.isDestinedForProxy(msg->_dst)){
				// We don't perform retransmission
				// if the destination is for the
				// GATT Proxy.
				freeNetMessage(msg);
				continue;
				}
			}

		_tmpQ.put(msg);
		}

	_netRetransmitQ	= _tmpQ;

	if(!_netRetransmitQ.first()){
		// Empty tx queue
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Empty retransmit queue \n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(_netReTxTimerApi.running()){
		return;
		}

	/*	4.2.19.2 Network Transmit Interval Steps
		The Network Transmit Interval Steps field is a 5-bit value
		representing the number of 10 millisecond steps that controls
		the interval between message transmissions of Network PDUs
		originating from the node.

		The transmission interval is calculated using the formula:

			transmission interval = (Network Retransmit Interval Steps + 1) * 10

		Each transmission should be perturbed by a random value between
		0 to 10 milliseconds between each transmission.
	 */
	unsigned
	retransmitIntervalInMs	= (_networkTransmitIntervalSteps + 1) * 10;

	uint8_t	smallRandomIntervalInMs;

	Oscl::Entropy::Random::fetch(
        &smallRandomIntervalInMs,
        sizeof(smallRandomIntervalInMs)
        );

	smallRandomIntervalInMs	%= 10;	// 10ms max

	retransmitIntervalInMs	+= smallRandomIntervalInMs;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _netReTxTimerApi.start(%u ms)\n",
		__PRETTY_FUNCTION__,
		retransmitIntervalInMs
		);
	#endif

	_netReTxTimerApi.start(retransmitIntervalInMs);
	}

void	Part::netReTxTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\t_networkTransmitCount: %u\n"
		"\t_networkTransmitIntervalSteps: %u\n"
		"",
		_networkTransmitCount,
		_networkTransmitIntervalSteps
		);
	#endif

	Oscl::Queue<NetMessage>	tmpQ;

	NetMessage*	msg;

	while((msg = _netRetransmitQ.get())){
		if(!msg->_txCount){
			freeNetMessage(msg);
			continue;
			}

		--msg->_txCount;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _bearerApi.transmit(%p,0x%4.4X)\n",
			__PRETTY_FUNCTION__,
			&*msg->_pdu,
			msg->_dst
			);
		#endif

		_bearerApi.transmit(
			msg->_pdu,
			msg->_dst
			);

		++_netTxCount;

		if(!msg->_txCount){
			freeNetMessage(msg);
			continue;
			}

		tmpQ.put(msg);
		}

	_netRetransmitQ	= tmpQ;

	if(!_netRetransmitQ.first()){
		// Empty tx queue
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Empty retransmit queue \n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	/*	4.2.19.2 Network Transmit Interval Steps
		The Network Transmit Interval Steps field is a 5-bit value
		representing the number of 10 millisecond steps that controls
		the interval between message transmissions of Network PDUs
		originating from the node.

		The transmission interval is calculated using the formula:

			transmission interval = (Network Retransmit Interval Steps + 1) * 10

		Each transmission should be perturbed by a random value between
		0 to 10 milliseconds between each transmission.
	 */
	unsigned
	retransmitIntervalInMs	= (_networkTransmitIntervalSteps + 1) * 10;

	uint8_t	smallRandomIntervalInMs;

	Oscl::Entropy::Random::fetch(
        &smallRandomIntervalInMs,
        sizeof(smallRandomIntervalInMs)
        );

	smallRandomIntervalInMs	%= 10;	// 10ms max

	retransmitIntervalInMs	+= smallRandomIntervalInMs;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _netReTxTimerApi.start(%u ms)\n",
		__PRETTY_FUNCTION__,
		retransmitIntervalInMs
		);
	#endif

	_netReTxTimerApi.start(retransmitIntervalInMs);
	}

void	Part::publishHeartbeat() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendHeartbeat();

	/*
		The Heartbeat Publication Count state is a 16-bit value
		that controls the number of periodical Heartbeat transport
		control messages to be sent.

		When set to 0xFFFF, it is not decremented after sending each
		Heartbeat message.

		When set to 0x0000, Heartbeat messages are not sent.

		When set to a value greater than or equal to 0x0001 or less
		than or equal to 0xFFFE, it is decremented after sending each
		Heartbeat message. The values for this state are defined in
		Table 4.24.
	*/

	if(_configHeartbeatPublishCount < 0xFFFF){
		if(_configHeartbeatPublishCount) {
			--_configHeartbeatPublishCount;
			}
		}

	if(!_configHeartbeatPublishCount) {
		return;
		}

	uint8_t
	configHeartbeatPublishPeriodLog	= _configHeartbeatPublishPeriodLog;

	if(configHeartbeatPublishPeriodLog){

		if(configHeartbeatPublishPeriodLog > 0x11){
			// Prohibited. This is a programming
			// error somewhere.
			configHeartbeatPublishPeriodLog	= 0x11;
			}

		unsigned
		seconds	=  1<<(configHeartbeatPublishPeriodLog-1);

		unsigned
		milliseconds	=  seconds * 1000;

		_heartbeatTxTimerApi.start(
			milliseconds
			);
		}
	}

void	Part::heartbeatTxTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	publishHeartbeat();
	}

void	Part::heartbeatSubTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\t_configHeartbeatSubscriptionPeriod: %u\n"
		"",
		_configHeartbeatSubscriptionPeriod
		);
	#endif

	if(!_configHeartbeatSubscriptionPeriod){
		return;
		}

	--_configHeartbeatSubscriptionPeriod;

	_heartbeatSubTimerApi.start(1000);
	}

void	Part::relayTxTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	NetMessage*	msg;

	Oscl::Queue<NetMessage>	tmpQ;

	while((msg = _relayQ.get())){

		if(msg->_txCount){
			--msg->_txCount;
			}

		_bearerApi.transmit(
			msg->_pdu,
			msg->_dst
			);

		++_netTxCount;

		if(!msg->_txCount){
			freeNetMessage(msg);
			continue;
			}

		tmpQ.put(msg);
		}

	_relayQ	= tmpQ;

	if(!_relayQ.first()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Empty relay queue \n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	unsigned
	retransmitIntervalInMs	= (_relayRetransmitIntervalSteps + 1) * 10;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _relayTxTimerApi.start(%u ms)\n",
		__PRETTY_FUNCTION__,
		retransmitIntervalInMs
		);
	#endif

	_relayTxTimerApi.start(retransmitIntervalInMs);
	}

NetMessage::NetMessage(
	Oscl::Pdu::Pdu*	pdu,
	uint16_t		dst,
	uint8_t			txCount
	) noexcept:
		_pdu(pdu),
		_dst(dst),
		_txCount(txCount)
		{
	}

void	Part::loopBearerTx(
			Oscl::Pdu::Pdu*	pdu,
			uint16_t		dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tdst: 0x%4.4X\n"
		,
		__PRETTY_FUNCTION__,
		dst
		);
	#endif

	bool
	isUnicastAddress	= Oscl::BT::Mesh::Address::isUnicastAddress(dst);

	if(isUnicastAddress){
		if(!addressWantedLocally(dst)){
			return;
			}
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	wrapper	= _freeStore.allocCompositeWrapper(
					*pdu
					);

	if(!wrapper){
		Oscl::Error::Info::log(
			"%s: out of memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_loopQ.put(wrapper);

	processLoopQ();
	}

void	Part::processLoopQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _loopLocked: %s\n",
		__PRETTY_FUNCTION__,
		_loopLocked?"true":"false"
		);
	#endif
	
	if(_loopLocked){
		return;
		}

	_loopLocked	= true;

	Oscl::Handle<Oscl::Pdu::Pdu>	pdu;

	while((pdu = _loopQ.get())){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: NEXT.\n",
			__PRETTY_FUNCTION__
			);
		#endif

		uint8_t	buffer[128];	// FIXME: size?

		unsigned
		len	= pdu->read(
				buffer,
				sizeof(buffer),
				0
				);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: rxMeshMessage\n"
			"",
			__PRETTY_FUNCTION__
			);
		#endif

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::hexDump(
			buffer,
			len
			);
		#endif

		rxMeshMessage(
			buffer,
			len,
			127	// rssi
			);
		}

	_loopLocked	= false;
	}

Part::NetMessageMem*	Part::allocNetMessageMem() noexcept{

	NetMessageMem*
	mem	= _freeNetMessageMem.get();

	#ifdef ENABLE_BT_MESH_NET_DYNAMIC_NETMESSAGEMEM

	if(!mem){

		mem	= (NetMessageMem*)malloc(sizeof(NetMessageMem));

		if(!mem){
			Oscl::Error::Info::log(
				"%s: network: %p, malloc() failed.\n",
				OSCL_PRETTY_FUNCTION,
				this
				);
			return 0;
			}

		++_nDynamicNetMessageMem;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%p] network: %p, _nDynamicNetMessageMem: %u\n",
			__PRETTY_FUNCTION__,
			mem,
			this,
			_nDynamicNetMessageMem
			);
		#endif

		}
	else {
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%p]\n",
			__PRETTY_FUNCTION__,
			mem
			);
		#endif
		}

	#endif

	if(mem){

		if(!_nOutstandingNetMessageMem){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: first.\n",
				__PRETTY_FUNCTION__
				);
			#endif
			}

		++_nOutstandingNetMessageMem;
		}

	return mem;
	}


void	Part::freeNetMessage(NetMessage* msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: [%p]\n",
		__PRETTY_FUNCTION__,
		msg
		);
	#endif

	msg->~NetMessage();
	_freeNetMessageMem.put((NetMessageMem*)msg);

	--_nOutstandingNetMessageMem;

	if(!_nOutstandingNetMessageMem){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: all free.\n",
			__PRETTY_FUNCTION__
			);
		#endif
		}
	}

bool	Part::createKey(
			uint16_t		keyIndex,
			const uint8_t	key[16],
			uint8_t			aid,
			bool			updating,
			const uint8_t	newKey[16],
			uint8_t			newAID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Key::RecordMem*
	mem	= _freeAppKeyRecordMem.get();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of AppKeyRecordMem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Record*
	record	= new(mem)
				Oscl::BT::Mesh::Key::Record(
					keyIndex,
					key,
					aid
					);

	_appKeys.put(record);

	return false;
	}

bool	Part::constructorNextKeyItem(
			Oscl::BT::Mesh::
			Key::Persist::Api&	api,
			uint16_t			keyIndex,
			const uint8_t		key[16],
			uint8_t				aid,
			bool				updating,
			const uint8_t		newKey[16],
			uint8_t				newAID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" keyIndex: 0x%4.4X\n"
		" key: %p\n"
		" aid: 0x%2.2X\n"
		" newKey: %p\n"
		" newAID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		keyIndex,
		key,
		aid,
		updating?"true":"false",
		newKey,
		newAID
		);
	#endif

	bool
	failed	= createKey(
				keyIndex,
				key,
				aid,
				updating,
				newKey,
				newAID
				);

	bool
	haltIteration	= failed?true:false;

	return haltIteration;
	}

Oscl::BT::Mesh::Key::Persist::Api*
	Part::findKeyPersistApi(uint16_t keyIndex) noexcept{

	_findKeyIndex	= keyIndex;
	_findKeyPersistApi	= 0;

	Oscl::BT::Mesh::Key::
	Persist::Iterator::Composer<Part>
		findKeyPersistIterator(
			*this,
			&Part::findNextKeyItem
			);

	_keyPersistApi.iterate(findKeyPersistIterator);

	return _findKeyPersistApi;
	}

bool	Part::findNextKeyItem(
			Oscl::BT::Mesh::
			Key::Persist::Api&	api,
			uint16_t			keyIndex,
			const uint8_t		key[16],
			uint8_t				aid,
			bool				updating,
			const uint8_t		newKey[16],
			uint8_t				newAID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" keyIndex: 0x%4.4X\n"
		" key: %p\n"
		" aid: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		keyIndex,
		key,
		aid
		);
	#endif

	if(_findKeyIndex == keyIndex){
		_findKeyPersistApi	= &api;
		return true;
		}

	return false;
	}

bool	Part::rxSecureNetworkBeacon(
			const void*		frame,
			unsigned 		length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	uint8_t		flags;
	uint8_t		networkID[8];

	decoder.decode(flags);
	decoder.copyOut(networkID,sizeof(networkID));

	uint32_t	ivIndex;
	uint8_t		authenticationValue[8];

	decoder.decode(ivIndex);
	decoder.copyOut(
		authenticationValue,
		sizeof(authenticationValue)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	const uint8_t*
	ivIndexStart	= (const uint8_t*)frame;

	ivIndexStart	= &ivIndexStart[
							1	// size of flags
						+	8	// size of network ID
						];

	NetKey*	netKey;
	bool	networkIdMatch	= false;

	for(
		netKey	= _netKeys.first();
		netKey;
		netKey	= _netKeys.next(netKey)
		) {

		networkIdMatch	= !memcmp(
							networkID,
							netKey->networkID(),
							sizeof(networkID)
							);

		if(!networkIdMatch){
			continue;
			}

		/*
			Authentication Value = AES-CMACBeaconKey (Flags || Network ID || IV Index) [0–7]
		*/
		Oscl::AES128::CMAC	cmac(netKey->beaconKey());

		cmac.accumulate(
			&flags,
			1
			);

		cmac.accumulate(
			netKey->networkID(),
			8
			);

		cmac.accumulate(
			ivIndexStart,
			4
			);

		cmac.finalize();

		uint8_t		calculatedAuthenticationValue[8];

		cmac.copyOut(
			calculatedAuthenticationValue,
			sizeof(calculatedAuthenticationValue)
			);

		bool
		authenticated	= !memcmp(
							calculatedAuthenticationValue,
							authenticationValue,
							sizeof(calculatedAuthenticationValue)
							);

		if(authenticated){
			break;
			}
		}

	if(!networkIdMatch){
		// From an some other network.
		return false;
		}

	if(!netKey){
		Oscl::Error::Info::log(
			"%s: Authentication failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	_lastRxBeaconNetKey	= netKey;

	// At this point, we have a valid
	// secure network beacon for this
	// network.

	bool	keyRefreshFlag	= (flags & (1<<0))?true:false;
	bool	ivUpdateFlag	= (flags & (1<<1))?true:false;

	_lastRxBeaconKeyRefreshFlag	= keyRefreshFlag;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ivIndex: 0x%8.8X"
		" flags: 0x%2.2X,"
		" ivUpdateFlag: %s,"
		" keyRefreshFlag: %s,"
		" networkID: %2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X,"
		"\n",
		OSCL_PRETTY_FUNCTION,
		ivIndex,
		flags,
		ivUpdateFlag?"true":"false",
		keyRefreshFlag?"true":"false",
		networkID[0],
		networkID[1],
		networkID[2],
		networkID[3],
		networkID[4],
		networkID[5],
		networkID[6],
		networkID[7]
		);
	#endif

	_keyRefreshState.secureNetworkBeaconReceived();

	if(_friendshipApi){
		_friendshipApi->rxBeacon(
			ivIndex,
			keyRefreshFlag,
			ivUpdateFlag
			);
		}

	// FIXME: Need to remove keyRefreshFlag from _nodeApi
	// FIXME: Need to update Key Refresh state in this Part.

	_nodeApi.secureNetworkBeaconReceived(
		ivIndex,
		keyRefreshFlag,
		ivUpdateFlag
		);

	if(_secureNetworkBeaconApi){
		_secureNetworkBeaconApi->beaconReceived();
		}

	return true;
	}

void	Part::sendSecureNetworkBeacon() noexcept{

	uint32_t	ivIndex;
	bool		ivUpdateFlag;
	bool		keyRefreshFlag;

	_nodeApi.getIvIndexState(
		ivIndex,
		ivUpdateFlag,
		keyRefreshFlag
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ivIndex: 0x%6.6X, keyRefreshFlag: %u, ivUpdateFlag: %u\n",
		OSCL_PRETTY_FUNCTION,
		ivIndex,
		keyRefreshFlag,
		ivUpdateFlag
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			bufferMem,
			bufferSize
			);

		const uint8_t*
		ivIndexStart	= (const uint8_t*)bufferMem;

		ivIndexStart	= &ivIndexStart[
								1	// type
							+	1	// size of flags
							+	8	// size of network ID
							];

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const uint8_t		type	=	0x01; // Secure Network Beacon

		uint8_t		flags	=
				(keyRefreshFlag)?1<<0:0<<0	// Key Refresh Flag
			|	(ivUpdateFlag)  ?1<<1:0<<1	// IV Update Flag
			;

		const NetKey&	netKey	= getTxNetKey();

		encoder.encode(type);
		encoder.encode(flags);
		encoder.copyIn(
			netKey.networkID(),
			8
			);

		encoder.encode(ivIndex);

		if(encoder.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}

		uint8_t		authenticationValue[8];

		/*
			Authentication Value = AES-CMACBeaconKey (Flags || Network ID || IV Index) [0–7]
		*/
		Oscl::AES128::CMAC	cmac(netKey.beaconKey());

		cmac.accumulate(
			&flags,
			1
			);

		cmac.accumulate(
			netKey.networkID(),
			8
			);

		cmac.accumulate(
			ivIndexStart,
			4
			);

		cmac.finalize();

		cmac.copyOut(
			authenticationValue,
			sizeof(authenticationValue)
			);

		encoder.copyIn(
			authenticationValue,
			sizeof(authenticationValue)
			);

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);
		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_beaconBearerApi.transfer(fixed);
	}

void	Part::getBeaconState(
			uint32_t&	ivIndex,
			bool&		ivUpdateFlag,
			bool&		keyRefreshFlag
			) const noexcept{

	_nodeApi.getIvIndexState(
		ivIndex,
		ivUpdateFlag,
		keyRefreshFlag
		);
	}

void	Part::ivIndexStateChanged() noexcept{
	sendSecureNetworkBeacon();
	_nodeApi.networkSecurityChanged();
	}

void	Part::revokeOldNetKey() noexcept{
	/*
		Update NetKeys
			_netKeys.remove(_activeNetKey);
			_activeNetKey	= _newNetKey
			~_newNetKey()
			_freeNetKeyMem.put((NetKeyMem*)_newNetKey);
			_newNetKey	= 0;
			_networkPersistApi
				setNetworkKeyPhase()
				setNetKey()
				?setNewKey()

	 */

	if(!_newNetKey){
		// This is a programming error.
		Oscl::Error::Info::log(
			"%s: _newNetKey is zero\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_netKeys.remove(_activeNetKey);
	_activeNetKey->~NetKey();
	_freeNetKeyMem.put((NetKeyMem*)_activeNetKey);

	_activeNetKey	= _newNetKey;
	_newNetKey		= 0;

	_networkPersistApi.setNetKey(
		_activeNetKey->netKey()
		);
	}

void	Part::revokeUpdatingAppKeys() noexcept{

	Oscl::Queue<Oscl::BT::Mesh::Key::Record>	tempAppKeys;
	tempAppKeys	= _appKeys;

	Oscl::BT::Mesh::Key::Record*	appKey;

	while((appKey = tempAppKeys.get())){

		if(!appKey->updating()){
			_appKeys.put(appKey);
			continue;
			}

		appKey->revokeOldKey();

		_appKeys.put(appKey);

		/*	Update persistence by destroying the
			old persitence record and creating a
			new one.
		 */
		Oscl::BT::Mesh::Key::Persist::Api*
		persistKey	= findKeyPersistApi(appKey->index());

		if(persistKey){
			_keyPersistApi.destroy(*persistKey);
			}

		_keyPersistApi.create(
			appKey->index(),
			appKey->key(),
			appKey->id()
			);
		}
	}

bool	Part::secureNetworkBeaconReceivedWithNewKey() noexcept{
	// This is NOT valid for Key Refresh Phase 0
	return (_lastRxBeaconNetKey == _newNetKey);
	}

bool	Part::isKeyRefreshFlagSetInLastBeacon() noexcept{
	// This is NOT valid for Key Refresh Phase 0
	return _lastRxBeaconKeyRefreshFlag;
	}

bool	Part::isUpdateKeyEqualToTheNewKey() noexcept{

	if(!_newNetKey){
		Oscl::Error::Info::log(
			"%s: _newNetKey is 0\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	int
	different	= memcmp(
					_newNetKey->netKey(),
					_newNetKeyCandidate,
					16
					);

	return !different;
	}

bool	Part::isUpdateKeyEqualToTheOldKey() noexcept{

	if(!_activeNetKey){
		Oscl::Error::Info::log(
			"%s: _activeNetKey is 0\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	if(!_newNetKeyCandidate){
		Oscl::Error::Info::log(
			"%s: _newNetKeyCandidate is 0\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	int
	different	= memcmp(
					_activeNetKey->netKey(),
					_newNetKeyCandidate,
					16
					);

	return !different;
	}

void	Part::revokeOldKeys() noexcept{
	/*	This is invoked after entering Key Refresh Phase 3.
		preconditions:
			_networkKeyPhase is 3
	 */
	if(_networkKeyPhase != 3){
		// This indicates a programming error!
		Oscl::Error::Info::log(
			"%s: _networkKeyPhase(%u) is not 3.\n",
			OSCL_PRETTY_FUNCTION,
			_networkKeyPhase
			);
		}

	/*
		- Update AppKeys
		- Update NetKeys
		- _keyRefresh.revokeOldKeysComplete();
	 */
	revokeUpdatingAppKeys();
	revokeOldNetKey();

	_keyRefreshState.revokeOldKeysComplete();
	}


void	Part::initializeNewNetKey() noexcept{

	if(!_newNetKeyCandidate){
		// This indicates a programming error!
		Oscl::Error::Info::log(
			"%s: _newNetKeyCandidate is 0.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(_newNetKey){
		// This indicates a programming error!
		Oscl::Error::Info::log(
			"%s: _newNetKey is NOT 0.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	NetKeyMem*
	mem	= _freeNetKeyMem.get();

	if(!mem){
		// This indicates a programming error!
		Oscl::Error::Info::log(
			"%s: Out of NetKeyMem!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_newNetKey	= new (mem) NetKey(
					_newNetKeyCandidate
					);

	_netKeys.put(_newNetKey);

	_networkPersistApi.setNewKey(_newNetKey->netKey());

	_newNetKeyCandidate	= 0;
	}

void	Part::configNetKeyUpdateFailed() noexcept{
	_configNetKeyUpdateFailed	= true;
	}

void	Part::configNetKeyUpdateSucceeded() noexcept{
	_configNetKeyUpdateFailed	= false;
	}

void	Part::enteredPhase0() noexcept{

	_networkKeyPhase	= 0;

	_networkPersistApi.setNetworkKeyPhase(_networkKeyPhase);
	}

void	Part::enteredPhase1() noexcept{

	_networkKeyPhase	= 1;

	_networkPersistApi.setNetworkKeyPhase(_networkKeyPhase);
	}

void	Part::enteredPhase2() noexcept{

	_networkKeyPhase	= 2;

	_networkPersistApi.setNetworkKeyPhase(_networkKeyPhase);
	}

void	Part::enteredPhase3() noexcept{

	_networkKeyPhase	= 3;

	_networkPersistApi.setNetworkKeyPhase(_networkKeyPhase);

	}

