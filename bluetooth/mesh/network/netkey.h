/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_netkeyh_
#define _oscl_bluetooth_mesh_network_netkeyh_

#include "oscl/queue/queueitem.h"
#include "crypto.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {

/** */
class NetKey: public Oscl::QueueItem {
	private:
		/** */
		uint8_t					_netKey[16];

		/** */
		Oscl::BT::Mesh::Network::
		Crypto::Credentials		_credentials;
		
		/** 3.8.6.3.2 Network ID

			The Network ID is derived from the network key
			such that each network key generates one Network ID.
		 */
		uint8_t		_networkID[8];

		/**	Section 3.8.6.3.3 IdentityKey

			The IdentityKey is derived from the network key
			such that each network key generates one IdentityKey.

			salt = s1(“nkik”)
			P = “id128” || 0x01
			IdentityKey = k1 (NetKey, salt, P)
		 */
		uint8_t		_identityKey[16];

		/** */
		uint8_t		_beaconKey[16];

	public:
		/** */
		NetKey(
			const uint8_t	netKey[16]
			) noexcept;

		/** */
		const Oscl::BT::Mesh::Network::Crypto::Credentials& credentials() const noexcept{
			return _credentials;
			}

		const uint8_t*	netKey() const noexcept{
			return _netKey;
			}

		const uint8_t*	networkID() const noexcept{
			return _networkID;
			}

		const uint8_t*	identityKey() const noexcept{
			return _identityKey;
			}

		const uint8_t*	beaconKey() const noexcept{
			return _beaconKey;
			}

	private:
		/** */
		void	generateMasterSecurityMaterial() noexcept;

		/** */
		void	generateNetworkID() noexcept;

		/** */
		void	generateIdentityKey() noexcept;

		/** */
		void	generateBeaconKey() noexcept;
	};

/** */
typedef Oscl::Memory::AlignedBlock< sizeof(NetKey) >	NetKeyMem;

}
}
}
}

#endif
