/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_provisioner_network_apih_
#define _oscl_bluetooth_mesh_provisioner_network_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Provisioner {
/** */
namespace Network {

/** */
class Api {
	public:
		/** */
		virtual const void*	getNetKey() const noexcept=0;

		/** */
		virtual uint16_t	getKeyIndex() const noexcept=0;

		/** */
		virtual uint8_t		getFlags() const noexcept=0;

		/** */
		virtual uint32_t	getTxIvIndex() const noexcept=0;

		/**	Allocate a contiguous range of n unicast addresses.
			RETURN:
				zero on failure.
				first unicast address in range on success
		 */
		virtual uint16_t	allocateAddresses(uint8_t n) noexcept=0;
	};

}
}
}
}
}

#endif
