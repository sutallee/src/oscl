/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_apih_
#define _oscl_bluetooth_mesh_node_apih_

#include <stdint.h>
#include "oscl/bluetooth/mesh/network/iteration/api.h"
#include "oscl/inhibitor/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {

/** */
class Api {
	public:
		/** */
		virtual const void*	getDeviceKey() const noexcept=0;

		/** The IvIndex is specified when a new Node is
			provisioned and not when a new sub-network
			is created. Therefore, it is a function of
			the Node and is shared among all sub-networks
			within a node.
		 */
		virtual uint32_t	getTxIvIndex() const noexcept=0;

		/** The IvIndex is specified when a new Node is
			provisioned and not when a new sub-network
			is created. Therefore, it is a function of
			the Node and is shared among all sub-networks
			within a node.
		 */
		virtual uint32_t	getRxIvIndex(bool ivi) const noexcept=0;

		/** */
		virtual void	getIvIndexState(
							uint32_t&	ivIndex,
							bool&		ivUpdateFlag,
							bool&		keyRefreshFlag
							) const noexcept=0;

#if 1
		// FIXME: The sequence number is really belongs
		// in the Network::Api. While it *can* be shared
		// by all sub-networks within a node, it is only
		// required to be unique per element/source address
		// supported by a node.
		//
		// Moving the sequence number to the Network or
		// Element has the benefit or reducing the
		// frequency of IV Update procedures.
		//
		// However, the IV Update procedure is initiated/performed
		// using a Secure Network Beacon, which is a function
		// of each sub-network. Therefore,
		/*
			When a Secure Network beacon is received on a known subnet,
			the node shall monitor for IV Index updates (see Section 3.10.5)
			and Key Refresh procedures (see Section 3.10.4).

			A Secure Network beacon may be sent for each subnet that a node
			is a member of to identify the subnet and inform about IV Index
			updates (see Section 3.10.5) and Key Refresh procedures
			(see Section 3.10.4).
		 */

		/*
			The IV Update procedure is initiated by any node that is a member of a
			primary subnet. This may be done when the node believes it is at risk
			of exhausting its sequence numbers, or it determines another node is
			close to exhausting its sequence numbers. The node changes its IV Index
			and sends an indication to other nodes in the mesh that the IV Index is
			being updated. This is then followed by a change back to normal operation
			by the same or some other node in the mesh.

			At least one node within a connected subnet with a key index different
			from 0x000 must also be on the primary subnet.

			During the Normal Operation state, the IV Update Flag in the Secure Network
			beacon and in the Friend Update message shall be set to 0. When this state
			is active, a node shall transmit using the current IV Index and shall process
			messages from the current IV Index and also the current IV Index - 1.

			During the IV Update in Progress state, the IV Update Flag in the Secure Network
			beacon and in the Friend Update message shall be set to 1. When this state
			is active, a node shall transmit using the current IV Index - 1 and shall
			process messages from the current IV Index - 1 and also the current IV Index.

			After at least 96 hours and before 144 hours of operating in IV Update in
			Progress state, the node shall transition back to the IV Normal Operation
			state and not change the IV Index. At the point of transition, the node shall
			reset the sequence number to 0x000000.

			A node that is in the IV Update in Progress state that receives and accepts a
			Secure Network beacon with the IV Update Flag set to 0 (indicating the
			Normal Operation state) should transition into the Normal Operation state
			as soon as possible.

			A node shall defer state change from IV Update in Progress to Normal Operation,
			as defined by this procedure, when the node has transmitted a Segmented Access
			message or a Segmented Control message without receiving the corresponding
			Segment Acknowledgment messages. The deferred change of the state shall be
			executed when the appropriate Segment Acknowledgment message is received or
			timeout for the delivery of this message is reached.

				Note: This requirement is necessary because upon completing the IV Update
				procedure the sequence number is reset to 0x000000 and the SeqAuth value
				would not be valid.
		 */
		/** */
		virtual uint32_t	allocateSequenceNumber() noexcept=0;
#endif

		/** */
		virtual Oscl::Inhibitor::Api&	getIvUpdateInhibitorApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Network::Iteration::Api&
				getNetworkIterationApi() noexcept=0;

		/** */
		virtual void	setConfigSecureNetworkBeacon(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getConfigSecureNetworkBeacon() noexcept=0;

		/** */
		virtual void	setConfigDefaultTtl(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getConfigDefaultTtl() noexcept=0;

		/** */
		virtual void	setConfigGattProxy(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getConfigGattProxy() noexcept=0;

		/** */
		virtual void	setConfigRelay(
							uint8_t relay,
							uint8_t retransmitCount,
							uint8_t retransmitIntervalSteps
							) noexcept=0;

		/** */
		virtual uint8_t	getConfigRelayState() noexcept=0;

		/** */
		virtual uint8_t	getConfigRelayRetransmitCount() noexcept=0;

		/** */
		virtual uint8_t	getConfigRelayRetransmitIntervalSteps() noexcept=0;

		/** RETURN: true on failure (out-of-resources) */
		virtual bool	addNetworkKey(
							uint16_t		keyIndex,
							const uint8_t	key[16]
							) noexcept=0;

		/** */
		virtual void	getConfigHeartbeatPublicationStatus(
							uint16_t&	destination,
							uint8_t&	countLog,
							uint8_t&	periodLog,
							uint8_t&	ttl,
							uint16_t&	features,
							uint16_t&	netKeyIndex
							) noexcept=0;

		/** RETURN: true on failure (invalid netKeyIndex) */
		virtual bool	setConfigHeartbeatPublication(
							uint16_t	destination,
							uint8_t		countLog,
							uint8_t		periodLog,
							uint8_t		ttl,
							uint16_t	features,
							uint16_t	netKeyIndex
							) noexcept=0;

		/** RETURN: true if the dst address is known
			to be on the GATT proxy.
		 */
		virtual bool	isDestinedForProxy(uint16_t	address) noexcept=0;

		/** */
		virtual	Oscl::Inhibitor::Api&   getRxDisableInhibitorApi() noexcept=0;

		/** */
		virtual	void	nodeReset() noexcept=0;

		/** */
		virtual void	secureNetworkBeaconReceived(
							uint32_t	ivIndex,
							bool		keyRefreshFlag,
							bool		ivUpdateFlag
							) noexcept=0;

		/** This operation is invoked when a network
			experiences a change in its network key
			or its ivIndex state. This is the result
			in the change in state of the IV Update
			or the the Key Refresh.
			The motivation is to notify the GATT
			Proxy Server such that it can send
			Secure Network Beacons to the GATT
			Proxy Client.
		 */
		virtual void	networkSecurityChanged() noexcept=0;
	};

}
}
}
}

#endif
