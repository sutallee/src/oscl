/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_parth_
#define _oscl_bluetooth_mesh_node_parth_

#include <stdint.h>
#include <string.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "api.h"
#include "context.h"
#include "oscl/bluetooth/mesh/node/persist/api.h"
#include "oscl/bluetooth/mesh/network/persist/api.h"
#include "oscl/bluetooth/mesh/network/persist/creator/api.h"
#include "oscl/bluetooth/mesh/ivupdate/part.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {

/** */
class Part :
	public Oscl::QueueItem,
	public Oscl::BT::Mesh::Node::Api,
	private Oscl::BT::Mesh::IvUpdate::ContextApi
	{
	private:
		/** */
		Oscl::BT::Mesh::Node::ContextApi&	_nodeContextApi;

		/** */
		Oscl::BT::Mesh::
	    Node::Persist::Api&					_nodePersistApi;

		/** */
		Oscl::BT::Mesh::Network::
		Persist::Creator::Api&				_networkPersistCreatorApi;

		/** */
		Oscl::BT::Mesh::Network::
		Iteration::Api&						_networkIterationApi;

		/** */
		uint8_t								_deviceKey[16];

		/** */
		uint32_t							_ivIndex;

		/** */
		const uint16_t						_baseUnicastAddress;

		/** */
		uint8_t								_flags;

		/** */
		uint8_t								_secureNetworkBeaconState;

		/** */
		uint8_t								_defaultTTL;

		/** */
		uint8_t								_proxyState;

		/** */
		uint8_t								_relayState;

		/** */
		uint8_t								_relayRetransmitCount;

		/** */
		uint8_t								_relayRetransmitIntervalSteps;

		/** */
		uint16_t							_heartbeatPublishNetKeyIndex;

		/** */
		uint32_t							_firstSequenceNumber;

		/** */
		uint32_t							_sequenceNumber;

	private:
		/** */
		Oscl::BT::Mesh::IvUpdate::Part		_ivUpdatePart;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::Node::ContextApi&	nodeContextApi,
			Oscl::Timer::Factory::Api&			timerFactoryApi,
			Oscl::BT::Mesh::Node::Persist::Api&	nodePersistApi,
			Oscl::BT::Mesh::Network::
			Persist::Creator::Api&				networkPersistCreatorApi,
			Oscl::BT::Mesh::Network::
			Iteration::Api&						networkIterationApi,
			const uint8_t						deviceKey[16],
			uint32_t							ivIndex,
			uint16_t							baseUnicastAddress,
			uint8_t								flags,
			uint8_t								secureNetworkBeaconState,
			uint8_t								defaultTTL,
			uint8_t								proxyState,
			uint8_t								relayState,
			uint8_t								relayRetransmitCount,
			uint8_t								relayRetransmitIntervalSteps,
			uint16_t							heartbeatPublishNetKeyIndex,
			uint32_t							hoursSinceIvUpdateChange,
			uint32_t							sequenceNumber
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		Oscl::BT::Mesh::Node::Persist::Api&	getNodePersistApi() noexcept;

		/** */
		bool	proxyIsEnabled() const noexcept;

		/** */
		void	forceIvUpdateNormalState() noexcept;

		/** */
		void	forceIvUpdateInProgressState() noexcept;

	private: // Oscl::BT::Mesh::IvUpdate::ContextApi
		/** This operation is used to indicate to
			context that it should update the persitence
			for the IV Index and its state.

            param [in] ivIndex				The current IV Index
            param [in] ivUpdateInProgress	The IV Index Update flag
            param [in] hoursSinceChange  	The number of hours that have
											elapsed since the change began.
		 */
		void	storeIvIndexState(
					uint32_t	ivIndex,
					bool		ivUpdateInProgress,
					uint32_t	hoursSinceChange
					) noexcept;

		/** This operation is used to reset the value
			of the Sequence Number counter to zero.
		 */
		void	resetSequenceNumber() noexcept;

		/** This operation is used to notify the context
			that the IV index state has changed. This
			is generally used to cause a secure network
			beacon to be transmitted.
		 */
		void	ivIndexStateChanged() noexcept;

	public: // Oscl::BT::Mesh::Node::Api
		/** */
		const void*	getDeviceKey() const noexcept;

		/** */
		uint32_t	getTxIvIndex() const noexcept;

		/** */
		uint32_t	getRxIvIndex(bool ivi) const noexcept;

		/** */
		void	getIvIndexState(
					uint32_t&	ivIndex,
					bool&		ivUpdateFlag,
					bool&		keyRefreshFlag
					) const noexcept;

		/** */
		uint32_t	allocateSequenceNumber() noexcept;

		/** */
		Oscl::Inhibitor::Api&	getIvUpdateInhibitorApi() noexcept;

		/** */
		Oscl::BT::Mesh::Network::Iteration::Api&
				getNetworkIterationApi() noexcept;

		/** */
		void	setConfigSecureNetworkBeacon(uint8_t state) noexcept;

		/** */
		uint8_t	getConfigSecureNetworkBeacon() noexcept;

		/** */
		void	setConfigDefaultTtl(uint8_t state) noexcept;

		/** */
		uint8_t	getConfigDefaultTtl() noexcept;

		/** */
		void	setConfigGattProxy(uint8_t state) noexcept;

		/** */
		uint8_t	getConfigGattProxy() noexcept;

		/** */
		void	setConfigRelay(
					uint8_t relay,
					uint8_t retransmitCount,
					uint8_t retransmitIntervalSteps
					) noexcept;

		/** */
		uint8_t	getConfigRelayState() noexcept;

		/** */
		uint8_t	getConfigRelayRetransmitCount() noexcept;

		/** */
		uint8_t	getConfigRelayRetransmitIntervalSteps() noexcept;

		/** RETURN: true on failure (out-of-resources) */
		bool	addNetworkKey(
							uint16_t		keyIndex,
							const uint8_t	key[16]
							) noexcept;

		/** */
		void	getConfigHeartbeatPublicationStatus(
							uint16_t&	destination,
							uint8_t&	countLog,
							uint8_t&	periodLog,
							uint8_t&	ttl,
							uint16_t&	features,
							uint16_t&	netKeyIndex
							) noexcept;

		/** RETURN: true on failure (invalid netKeyIndex) */
		bool	setConfigHeartbeatPublication(
							uint16_t	destination,
							uint8_t		countLog,
							uint8_t		periodLog,
							uint8_t		ttl,
							uint16_t	features,
							uint16_t	netKeyIndex
							) noexcept;

		/** RETURN: true if the dst address is known
			to be on the GATT proxy.
		 */
		bool	isDestinedForProxy(uint16_t	address) noexcept;

		/** */
		Oscl::Inhibitor::Api&   getRxDisableInhibitorApi() noexcept;

		/** */
		void	nodeReset() noexcept;

		/** */
		void	secureNetworkBeaconReceived(
					uint32_t	ivIndex,
					bool		keyRefreshFlag,
					bool		ivUpdateFlag
					) noexcept;

		/** This operation is invoked when a network
			experiences a change in its network key
			or its ivIndex state. This is the result
			in the change in state of the IV Update
			or the the Key Refresh.
			The motivation is to notify the GATT
			Proxy Server such that it can send
			Secure Network Beacons to the GATT
			Proxy Client.
		 */
		void	networkSecurityChanged() noexcept;

	private: // Oscl::BT::Mesh::Network::Persist::Iterator::Composer
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	constructorNextNetworkItem(
					Oscl::BT::Mesh::
					Network::Persist::Api&	api,
					const uint8_t			netKey[16],
					const uint8_t			newKey[16],
					uint16_t				keyIndex,
					uint8_t					nNodeIdentityState,
					uint8_t					friendState,
					uint8_t					networkKeyPhase,
					uint16_t				heartbeatSubscriptionSource,
					uint16_t				heartbeatSubscriptionDestination,
					uint8_t					heartbeatSubscriptionPeriodLog,
					uint8_t					networkTransmitCount,
					uint8_t					networkTransmitIntervalSteps,
					uint8_t					defaultTTL,
					bool					relayEnabled,
					uint8_t					relayRetransmitCount,
					uint8_t					relayRetransmitIntervalSteps,
					uint16_t				heartbeatPublishDestination,
					uint16_t				heartbeatPublishCount,
					uint8_t					heartbeatPublishPeriodLog,
					uint8_t					heartbeatPublishTtl,
					uint16_t				heartbeatPublishFeatures
					) noexcept;

		/**	This operation creates a Network *without*
			the associated persistence.

			RETURN: true for failure.
			*/
		bool	createNetwork(
					Oscl::BT::Mesh::
					Network::Persist::Api&	networkPersistApi,
					const uint8_t			netKey[16],
					const uint8_t			newKey[16],
					uint16_t				keyIndex,
					uint8_t					nodeIdentityState,
					uint8_t					friendState,
					uint8_t					networkKeyPhase,
					uint16_t				heartbeatSubscriptionSource,
					uint16_t				heartbeatSubscriptionDestination,
					uint8_t					heartbeatSubscriptionPeriodLog,
					uint8_t					networkTransmitCount,
					uint8_t					networkTransmitIntervalSteps,
					bool					relayEnabled,
					uint8_t					relayRetransmitCount,
					uint8_t					relayRetransmitIntervalSteps,
					uint16_t				heartbeatPublishDestination,
					uint16_t				heartbeatPublishCount,
					uint8_t					heartbeatPublishPeriodLog,
					uint8_t					heartbeatPublishTtl,
					uint16_t				heartbeatPublishFeatures
					) noexcept;
	};

}
}
}
}

#endif
