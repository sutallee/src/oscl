/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_contexth_
#define _oscl_bluetooth_mesh_node_contexth_

#include <stdint.h>
#include "oscl/bluetooth/mesh/network/part.h"
#include "oscl/inhibitor/api.h"
#include "oscl/bluetooth/mesh/node/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {

/** */
class ContextApi {
	public:
		/** This operation instantiates a new network/node.
			This operation does NOT create persistence
			for the node/network.
		 */
		virtual Oscl::BT::Mesh::Network::Part*
			createNetwork(
				Oscl::BT::Mesh::
				Network::Persist::Api&	networkPersistApi,
				const uint8_t			netKey[16],
				const uint8_t			newKey[16],
				uint16_t				keyIndex,
				uint8_t					flags,
				uint32_t				ivIndex,
				uint16_t				unicastAddress,
				uint32_t				seqNumber,
				uint8_t					defaultTTL,
				uint8_t					nodeIdentityState,
				uint8_t					friendState,
				uint8_t					networkKeyPhase,
				uint16_t				heartbeatSubscriptionSource,
				uint16_t				heartbeatSubscriptionDestination,
				uint8_t					heartbeatSubscriptionPeriodLog,
				uint8_t					networkTransmitCount,
				uint8_t					networkTransmitIntervalSteps,
				bool					relayEnabled,
				uint8_t					relayRetransmitCount,
				uint8_t					relayRetransmitIntervalSteps,
				uint16_t				heartbeatPublishDestination,
				uint16_t				heartbeatPublishCount,
				uint8_t					heartbeatPublishPeriodLog,
				uint8_t					heartbeatPublishTTTL,
				uint16_t				heartbeatPublishFeatures
				) noexcept=0;

		/** RETURN: true if the dst address is known
			to be on the GATT proxy.
		 */
		virtual bool	isDestinedForProxy(uint16_t	address) noexcept=0;

		/** */
		virtual	Oscl::Inhibitor::Api&   getRxDisableInhibitorApi() noexcept=0;

		/** */
		virtual void	destroy(Oscl::BT::Mesh::Network::Part& network) noexcept=0;

		/** */
		virtual void	stopped(Oscl::BT::Mesh::Node::Api& node) noexcept=0;

		/** */
		virtual void	reset(Oscl::BT::Mesh::Node::Api& node) noexcept=0;

		/** This operation is invoked when a network
			experiences a change in its network key
			or its ivIndex state. This is the result
			in the change in state of the IV Update
			or the the Key Refresh.
			The motivation is to notify the GATT
			Proxy Server such that it can send
			Secure Network Beacons to the GATT
			Proxy Client.
		 */
		virtual void	networkSecurityChanged() noexcept=0;
	};

}
}
}
}

#endif
