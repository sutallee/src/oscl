/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/bluetooth/mesh/address/values.h"

#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/network/persist/iterator/composer.h"

//#define DEBUG_TRACE

using namespace Oscl::BT::Mesh::Node;

constexpr unsigned	nReservedSequenceNumbers	= 0x80;

constexpr unsigned ivUpdateFlagBit		= 1;
constexpr unsigned keyRefreshFlagBit	= 0;

Part::Part(
	Oscl::BT::Mesh::Node::ContextApi&	nodeContextApi,
	Oscl::Timer::Factory::Api&			timerFactoryApi,
	Oscl::BT::Mesh::Node::Persist::Api&	nodePersistApi,
	Oscl::BT::Mesh::Network::
	Persist::Creator::Api&				networkPersistCreatorApi,
	Oscl::BT::Mesh::Network::
	Iteration::Api&						networkIterationApi,
	const uint8_t						deviceKey[16],
	uint32_t							ivIndex,
	uint16_t							baseUnicastAddress,
	uint8_t								flags,
	uint8_t								secureNetworkBeaconState,
	uint8_t								defaultTTL,
	uint8_t								proxyState,
	uint8_t								relayState,
	uint8_t								relayRetransmitCount,
	uint8_t								relayRetransmitIntervalSteps,
	uint16_t							heartbeatPublishNetKeyIndex,
	uint32_t							hoursSinceIvUpdateChange,
	uint32_t							sequenceNumber
	) noexcept:
		_nodeContextApi(nodeContextApi),
		_nodePersistApi(nodePersistApi),
		_networkPersistCreatorApi(networkPersistCreatorApi),
		_networkIterationApi(networkIterationApi),
		_ivIndex(ivIndex),
		_baseUnicastAddress(baseUnicastAddress),
		_flags(flags), // FIXME these should not be stored here only in _ivUpdatePart, etc.
		_secureNetworkBeaconState(secureNetworkBeaconState),
		_defaultTTL(defaultTTL),
		_proxyState(proxyState),
		_relayState(relayState),
		_relayRetransmitCount(relayRetransmitCount),
		_relayRetransmitIntervalSteps(relayRetransmitIntervalSteps),
		_heartbeatPublishNetKeyIndex(heartbeatPublishNetKeyIndex),
		_firstSequenceNumber(sequenceNumber),
		_sequenceNumber(sequenceNumber),
		_ivUpdatePart(
			*this,	// context
            timerFactoryApi,
            ivIndex,
            hoursSinceIvUpdateChange,
            (flags & (1<<1))?true:false	// ivUpdateInProgress
			)
		{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	memcpy(
		_deviceKey,
		deviceKey,
		16
		);

	}

Part::~Part() noexcept{
	}

void	Part::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Network::
	Persist::Iterator::Composer<Part>
		constructorNetworkPersistIterator(
			*this,
			&Part::constructorNextNetworkItem
			);

	_networkPersistCreatorApi.iterate(constructorNetworkPersistIterator);

	_ivUpdatePart.start();
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// There is no callback for this.
	_ivUpdatePart.stop();

	_nodeContextApi.stopped(*this);
	}

bool	Part::proxyIsEnabled() const noexcept{
	// FIXME
	return (_proxyState == 0x01);
	}

void	Part::forceIvUpdateNormalState() noexcept{
	_ivUpdatePart.testNormalState();
	}

void	Part::forceIvUpdateInProgressState() noexcept{
	_ivUpdatePart.testInProgressState();
	}

void	Part::storeIvIndexState(
			uint32_t	ivIndex,
			bool		ivUpdateInProgress,
			uint32_t	hoursSinceChange
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ivIndex: 0x%8.8X, ivUpdateInProgress: %s, hoursSinceChange: %u\n",
		__PRETTY_FUNCTION__,
		ivIndex,
		ivUpdateInProgress?"true":"false",
		hoursSinceChange
		);
	#endif

	_nodePersistApi.setIvIndexState(
		ivIndex,
		ivUpdateInProgress,
		hoursSinceChange
		);

	}

void	Part::resetSequenceNumber() noexcept{
	_sequenceNumber	= 0;

	_firstSequenceNumber = nReservedSequenceNumbers;

	_nodePersistApi.setSequenceNumber(_firstSequenceNumber);
	}

void	Part::ivIndexStateChanged() noexcept{
	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterationApi.first();
		network;
		network	= _networkIterationApi.next(network)
		){
		network->ivIndexStateChanged();
		}
	}

const void*	Part::getDeviceKey() const noexcept{
	return _deviceKey;
	}

uint32_t	Part::getTxIvIndex() const noexcept{

	uint32_t
	ivIndex	= _ivUpdatePart.getTxIvIndex();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ivIndex: 0x%8.8X\n",
		OSCL_PRETTY_FUNCTION,
		ivIndex
		);
	#endif

	return ivIndex;
	}

uint32_t	Part::getRxIvIndex(bool ivi) const noexcept{

	uint32_t
	ivIndex	= _ivUpdatePart.getRxIvIndex(ivi);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ivi: %s, ivIndex: 0x%8.8X\n",
		OSCL_PRETTY_FUNCTION,
		ivi?"true":"false",
		ivIndex
		);
	#endif

	return ivIndex;
	}

void	Part::getIvIndexState(
			uint32_t&	ivIndex,
			bool&		ivUpdateFlag,
			bool&		keyRefreshFlag
			) const noexcept{

	// FIXME: update when keyRefresh mechanism is implemented.
	keyRefreshFlag	= (_flags & (1<<keyRefreshFlagBit))?true:false;

	_ivUpdatePart.getIvIndexState(
		ivIndex,
		ivUpdateFlag
		);
	}

uint32_t	Part::allocateSequenceNumber() noexcept{
	uint32_t	seq	= _sequenceNumber;

	++_sequenceNumber;

	// Revisit when we refactor for Node.
	// We shouldn't update the sequence number
	// persistence *every* time we increment it
	// this is just for testing persistence.

	if(_sequenceNumber > _firstSequenceNumber){
		_firstSequenceNumber	= _sequenceNumber + nReservedSequenceNumbers;
		// FIXME: hack!
		// We're using the Network object to
		// store the NodePersistApi.
		// Ultimately, we will refactor the Network
		// object into both a Node object and a Network
		// object. At that time we will change this
		// operation to use the "singleton" node object.
		_nodePersistApi.setSequenceNumber(_firstSequenceNumber);
		}

	// FIXME/TODO:
	// We need to consider initiating the IvIndex
	// update procedure when the sequence number
	// is "nearing" 0x00FFFFFF.

	return seq;
	}

Oscl::Inhibitor::Api&	Part::getIvUpdateInhibitorApi() noexcept{
	return _ivUpdatePart.getIvUpdateInhibitorApi();
	}

Oscl::BT::Mesh::Network::Iteration::Api&
Part::getNetworkIterationApi() noexcept{
	return _networkIterationApi;
	}

void	Part::setConfigSecureNetworkBeacon(uint8_t state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// FIXME: This needs to be propagated to the
	// Secure Network Beacon sub-system.

	_secureNetworkBeaconState	= state;
	}

uint8_t	Part::getConfigSecureNetworkBeacon() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	return _secureNetworkBeaconState;
	}

void	Part::setConfigDefaultTtl(uint8_t state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_defaultTTL	= state;

	_nodePersistApi.setDefaultTTL(state);

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterationApi.first();
		network;
		network	= _networkIterationApi.next(network)
		){
			network->setDefaultTTL(state);
		}
	}

uint8_t	Part::getConfigDefaultTtl() noexcept{
	// In theory, all networks have the same
	// defaultTTL?
	return _defaultTTL;
	}

void	Part::setConfigGattProxy(uint8_t state) noexcept{
	// FIXME
	_proxyState	= state;
	_nodePersistApi.setProxyState(state);
	}

uint8_t	Part::getConfigGattProxy() noexcept{

	return _proxyState;
	}

void	Part::setConfigRelay(
					uint8_t relay,
					uint8_t retransmitCount,
					uint8_t retransmitIntervalSteps
					) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\trelay: %u\n"
		"\tretransmitCount: %u\n"
		"\tretransmitIntervalSteps: %u\n"
		"",
		__PRETTY_FUNCTION__,
		relay,
		retransmitCount,
		retransmitIntervalSteps
		);
	#endif

	_relayState						= relay;
	_relayRetransmitCount			= retransmitCount;
	_relayRetransmitIntervalSteps	= retransmitIntervalSteps;

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterationApi.first();
		network;
		network	= _networkIterationApi.next(network)
		){
		network->setRelay(
			relay,
			retransmitCount,
			retransmitIntervalSteps
			);
		}
	}

uint8_t	Part::getConfigRelayState() noexcept{
	return _relayState;
	}

uint8_t	Part::getConfigRelayRetransmitCount() noexcept{
	return _relayRetransmitCount;
	}

uint8_t	Part::getConfigRelayRetransmitIntervalSteps() noexcept{
	return _relayRetransmitIntervalSteps;
	}

bool	Part::addNetworkKey(
			uint16_t		keyIndex,
			const uint8_t	key[16]
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NEED IMPLEMENTATION\n",
		__PRETTY_FUNCTION__
		);

	/*	The implementation needs to create
		a new Sub-Network object.
		Sub-Networks are NOT currently supported.
	 */

	return true;	// failed
	}

void	Part::getConfigHeartbeatPublicationStatus(
			uint16_t&	destination,
			uint8_t&	countLog,
			uint8_t&	periodLog,
			uint8_t&	ttl,
			uint16_t&	features,
			uint16_t&	netKeyIndex
			) noexcept{

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterationApi.first();
		network;
		network	= _networkIterationApi.next(network)
		){
		if(_heartbeatPublishNetKeyIndex == network->getKeyIndex()){
			network->getConfigHeartbeatPublicationStatus(
					destination,
					countLog,
					periodLog,
					ttl,
					features
					);
			netKeyIndex	= _heartbeatPublishNetKeyIndex;
			return;
			}
		}

	destination	= Oscl::BT::Mesh::Address::unassigned;
	countLog	= 0;
	periodLog	= 0;
	ttl			= 0;
	features	= 0;
	netKeyIndex	= _heartbeatPublishNetKeyIndex;
	}

bool	Part::setConfigHeartbeatPublication(
			uint16_t	destination,
			uint8_t		countLog,
			uint8_t		periodLog,
			uint8_t		ttl,
			uint16_t	features,
			uint16_t	netKeyIndex
			) noexcept{
	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterationApi.first();
		network;
		network	= _networkIterationApi.next(network)
		){
		if(netKeyIndex == network->getKeyIndex()){

			bool
			failed	= network->setConfigHeartbeatPublication(
						destination,
						countLog,
						periodLog,
						ttl,
						features
						);

			if(!failed){
				_heartbeatPublishNetKeyIndex	= netKeyIndex;
				}

			return failed;
			}
		}

	return true; // failed
	}

bool	Part::isDestinedForProxy(uint16_t address) noexcept{
	return _nodeContextApi.isDestinedForProxy(address);
	}

Oscl::Inhibitor::Api&   Part::getRxDisableInhibitorApi() noexcept{
	return _nodeContextApi.getRxDisableInhibitorApi();
	}

void	Part::nodeReset() noexcept{
	_nodeContextApi.reset(*this);
	}

void	Part::secureNetworkBeaconReceived(
			uint32_t	ivIndex,
			bool		keyRefreshFlag,
			bool		ivUpdateFlag
			) noexcept{

	// TODO: add network key refresh
	// implementation.

	_ivUpdatePart.beaconReceived(
		ivIndex,
		ivUpdateFlag
		);
	}

void	Part::networkSecurityChanged() noexcept{
	_nodeContextApi.networkSecurityChanged();
	}

bool	Part::createNetwork(
			Oscl::BT::Mesh::
			Network::Persist::Api&	networkPersistApi,
			const uint8_t			netKey[16],
			const uint8_t			newKey[16],
			uint16_t				keyIndex,
			uint8_t					nodeIdentityState,
			uint8_t					friendState,
			uint8_t					networkKeyPhase,
			uint16_t				heartbeatSubscriptionSource,
			uint16_t				heartbeatSubscriptionDestination,
			uint8_t					heartbeatSubscriptionPeriodLog,
			uint8_t					networkTransmitCount,
			uint8_t					networkTransmitIntervalSteps,
			bool					relayEnabled,
			uint8_t					relayRetransmitCount,
			uint8_t					relayRetransmitIntervalSteps,
			uint16_t				heartbeatPublishDestination,
			uint16_t				heartbeatPublishCount,
			uint8_t					heartbeatPublishPeriodLog,
			uint8_t					heartbeatPublishTtl,
			uint16_t				heartbeatPublishFeatures
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" netKey: %p\n"
		" newKey: %p\n"
		" keyIndex: 0x%4.4X\n"
		" nodeIdentityState: 0x%2.2X\n"
		" friendState: 0x%2.2X\n"
		" networkKeyPhase: 0x%2.2X\n"
		" heartbeatSubscriptionSource: 0x%4.4X\n"
		" heartbeatSubscriptionDestination: 0x%4.4X\n"
		" heartbeatSubscriptionPeriodLog: 0x%2.2X\n"
		" networkTransmitCount: 0x%2.2X\n"
		" networkTransmitIntervalSteps: 0x%2.2X\n"
		" relayEnabled: 0x%2.2X\n"
		" relayRetransmitCount: 0x%2.2X\n"
		" relayRetransmitIntervalSteps: 0x%2.2X\n"
		" heartbeatPublishDestination: 0x%4.4X\n"
		" heartbeatPublishCount: 0x%4.4X\n"
		" heartbeatPublishPeriodLog: 0x%2.2X\n"
		" heartbeatPublishTtl: 0x%2.2X\n"
		" heartbeatPublishFeatures: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		netKey,
		newKey,
		keyIndex,
		nodeIdentityState,
		friendState,
		networkKeyPhase,
		heartbeatSubscriptionSource,
		heartbeatSubscriptionDestination,
		heartbeatSubscriptionPeriodLog,
		networkTransmitCount,
		networkTransmitIntervalSteps,
		relayEnabled,
		relayRetransmitCount,
		relayRetransmitIntervalSteps,
		heartbeatPublishDestination,
		heartbeatPublishCount,
		heartbeatPublishPeriodLog,
		heartbeatPublishTtl,
		heartbeatPublishFeatures
		);
	#endif

	Oscl::BT::Mesh::Network::Part*
	network	= _nodeContextApi.createNetwork(
				networkPersistApi,
				netKey,
				newKey,
				keyIndex,
				_flags,	// FIXME these need to come from _ivUpdatePart, etc.
				_ivIndex,
				_baseUnicastAddress,
				_sequenceNumber,
				_defaultTTL,
				nodeIdentityState,
				friendState,
				networkKeyPhase,
				heartbeatSubscriptionSource,
				heartbeatSubscriptionDestination,
				heartbeatSubscriptionPeriodLog,
				networkTransmitCount,
				networkTransmitIntervalSteps,
				relayEnabled,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishDestination,
				heartbeatPublishCount,
				heartbeatPublishPeriodLog,
				heartbeatPublishTtl,
				heartbeatPublishFeatures
				);

	if(!network){
		return true;
		}

	return false;
	}

bool	Part::constructorNextNetworkItem(
			Oscl::BT::Mesh::
			Network::Persist::Api&	api,
			const uint8_t			netKey[16],
			const uint8_t			newKey[16],
			uint16_t				keyIndex,
			uint8_t					nodeIdentityState,
			uint8_t					friendState,
			uint8_t					networkKeyPhase,
			uint16_t				heartbeatSubscriptionSource,
			uint16_t				heartbeatSubscriptionDestination,
			uint8_t					heartbeatSubscriptionPeriodLog,
			uint8_t					networkTransmitCount,
			uint8_t					networkTransmitIntervalSteps,
			uint8_t					defaultTTL,
			bool					relayEnabled,
			uint8_t					relayRetransmitCount,
			uint8_t					relayRetransmitIntervalSteps,
			uint16_t				heartbeatPublishDestination,
			uint16_t				heartbeatPublishCount,
			uint8_t					heartbeatPublishPeriodLog,
			uint8_t					heartbeatPublishTtl,
			uint16_t				heartbeatPublishFeatures
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" netKey: %p\n"
		" newKey: %p\n"
		" nodeIdentityState: 0x%2.2X\n"
		" friendState: 0x%2.2X\n"
		" networkKeyPhase: 0x%2.2X\n"
		" heartbeatSubscriptionSource: 0x%4.4X\n"
		" heartbeatSubscriptionDestination: 0x%4.4X\n"
		" heartbeatSubscriptionPeriodLog: 0x%2.2X\n"
		" networkTransmitCount: 0x%2.2X\n"
		" networkTransmitIntervalSteps: 0x%2.2X\n"
		" defaultTTL: 0x%2.2X\n"
		" relayEnabled: 0x%2.2X\n"
		" relayRetransmitCount: 0x%2.2X\n"
		" relayRetransmitIntervalSteps: 0x%2.2X\n"
		" heartbeatPublishDestination: 0x%4.4X\n"
		" heartbeatPublishCount: 0x%4.4X\n"
		" heartbeatPublishPeriodLog: 0x%2.2X\n"
		" heartbeatPublishTtl: 0x%2.2X\n"
		" heartbeatPublishFeatures: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		netKey,
		newKey,
		nodeIdentityState,
		friendState,
		networkKeyPhase,
		heartbeatSubscriptionSource,
		heartbeatSubscriptionDestination,
		heartbeatSubscriptionPeriodLog,
		networkTransmitCount,
		networkTransmitIntervalSteps,
		defaultTTL,
		relayEnabled,
		relayRetransmitCount,
		relayRetransmitIntervalSteps,
		heartbeatPublishDestination,
		heartbeatPublishCount,
		heartbeatPublishPeriodLog,
		heartbeatPublishTtl,
		heartbeatPublishFeatures
		);
	#endif

	bool
	failed	= createNetwork(
				api,
				netKey,
				newKey,
				keyIndex,
				nodeIdentityState,
				friendState,
				networkKeyPhase,
				heartbeatSubscriptionSource,
				heartbeatSubscriptionDestination,
				heartbeatSubscriptionPeriodLog,
				networkTransmitCount,
				networkTransmitIntervalSteps,
				relayEnabled,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishDestination,
				heartbeatPublishCount,
				heartbeatPublishPeriodLog,
				heartbeatPublishTtl,
				heartbeatPublishFeatures
				);

	bool
	haltIteration	= failed?true:false;

	return haltIteration;
	}

