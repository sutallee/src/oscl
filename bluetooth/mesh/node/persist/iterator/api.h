/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_persist_iterator_apih_
#define _oscl_bluetooth_mesh_node_persist_iterator_apih_

#include "oscl/bluetooth/mesh/node/persist/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {
/** */
namespace Persist {
/** */
namespace Iterator {

/** */
class Api {
	public:
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		virtual bool	item(
							Oscl::BT::Mesh::
							Node::Persist::Api&		api,
							const uint8_t	deviceKey[16],
							uint16_t		keyIndex,
							uint8_t			flags,
							uint32_t		ivIndex,
							uint16_t		unicastAddress,
							uint32_t		seqNumber,
							uint8_t			secureNetworkBeaconState,
							uint8_t			defaultTTL,
							uint8_t			proxyState,
							uint8_t			relayState,
							uint8_t			relayRetransmitCount,
							uint8_t			relayRetransmitIntervalSteps,
							uint16_t		heartbeatPublishNetKeyIndex,
							uint32_t		hoursSinceIvUpdateChange
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
