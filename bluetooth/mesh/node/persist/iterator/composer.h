/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_persist_iterator_composerh_
#define _oscl_bluetooth_mesh_node_persist_iterator_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {
/** */
namespace Persist {
/** */
namespace Iterator {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**
		 */
		bool	(Context::*_item)(
					Oscl::BT::Mesh::
					Node::Persist::Api&	api,
					const uint8_t		deviceKey[16],
					uint16_t			keyIndex,
					uint8_t				flags,
					uint32_t			ivIndex,
					uint16_t			unicastAddress,
					uint32_t			seqNumber,
					uint8_t				secureNetworkBeaconState,
					uint8_t				defaultTTL,
					uint8_t				proxyState,
					uint8_t				relayState,
					uint8_t				relayRetransmitCount,
					uint8_t				relayRetransmitIntervalSteps,
					uint16_t			heartbeatPublishNetKeyIndex,
					uint32_t			hoursSinceIvUpdateChange
					);

	public:
		/** */
		Composer(
			Context&		context,
			bool	(Context::*item)(
							Oscl::BT::Mesh::
							Node::Persist::Api&	api,
							const uint8_t	deviceKey[16],
							uint16_t		keyIndex,
							uint8_t			flags,
							uint32_t		ivIndex,
							uint16_t		unicastAddress,
							uint32_t		seqNumber,
							uint8_t			secureNetworkBeaconState,
							uint8_t			defaultTTL,
							uint8_t			proxyState,
							uint8_t			relayState,
							uint8_t			relayRetransmitCount,
							uint8_t			relayRetransmitIntervalSteps,
							uint16_t		heartbeatPublishNetKeyIndex,
							uint32_t		hoursSinceIvUpdateChange
							)
			) noexcept;

	private:
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	item(
					Oscl::BT::Mesh::
					Node::Persist::Api&	api,
					const uint8_t		deviceKey[16],
					uint16_t			keyIndex,
					uint8_t				flags,
					uint32_t			ivIndex,
					uint16_t			unicastAddress,
					uint32_t			seqNumber,
					uint8_t				secureNetworkBeaconState,
					uint8_t				defaultTTL,
					uint8_t				proxyState,
					uint8_t				relayState,
					uint8_t				relayRetransmitCount,
					uint8_t				relayRetransmitIntervalSteps,
					uint16_t			heartbeatPublishNetKeyIndex,
					uint32_t			hoursSinceIvUpdateChange
					) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&	context,
			bool		(Context::*item)(
							Oscl::BT::Mesh::
							Node::Persist::Api&	api,
							const uint8_t		deviceKey[16],
							uint16_t			keyIndex,
							uint8_t				flags,
							uint32_t			ivIndex,
							uint16_t			unicastAddress,
							uint32_t			seqNumber,
							uint8_t				secureNetworkBeaconState,
							uint8_t				defaultTTL,
							uint8_t				proxyState,
							uint8_t				relayState,
							uint8_t				relayRetransmitCount,
							uint8_t				relayRetransmitIntervalSteps,
							uint16_t			heartbeatPublishNetKeyIndex,
							uint32_t			hoursSinceIvUpdateChange
							)
			) noexcept:
		_context(context),
		_item(item)
		{
	}

template <class Context>
bool	Composer<Context>::item(
			Oscl::BT::Mesh::
			Node::Persist::Api&	api,
			const uint8_t		deviceKey[16],
			uint16_t			keyIndex,
			uint8_t				flags,
			uint32_t			ivIndex,
			uint16_t			unicastAddress,
			uint32_t			seqNumber,
			uint8_t				secureNetworkBeaconState,
			uint8_t				defaultTTL,
			uint8_t				proxyState,
			uint8_t				relayState,
			uint8_t				relayRetransmitCount,
			uint8_t				relayRetransmitIntervalSteps,
			uint16_t			heartbeatPublishNetKeyIndex,
			uint32_t			hoursSinceIvUpdateChange
			) noexcept{

	return (_context.*_item)(
				api,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress,
				seqNumber,
				secureNetworkBeaconState,
				defaultTTL,
				proxyState,
				relayState,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishNetKeyIndex,
				hoursSinceIvUpdateChange
				);
	}

}
}
}
}
}
}

#endif
