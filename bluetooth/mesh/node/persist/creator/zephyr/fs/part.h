/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_bluetooth_mesh_node_persist_creator_zephyr_fs_parth_
#define _oscl_bluetooth_mesh_node_persist_creator_zephyr_fs_parth_

#include "oscl/bluetooth/mesh/node/persist/creator/api.h"
#include "oscl/bluetooth/mesh/node/persist/api.h"
#include "oscl/bluetooth/mesh/node/persist/iterator/api.h"

#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/persist/parent/zephyr/fs/part.h"
#include "oscl/fs/file/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {
/** */
namespace Persist {
/** */
namespace Creator {

/** */
class File :
		public Oscl::QueueItem,
		public Oscl::BT::Mesh::Node::Persist::Api
		{
	friend class Part;
	private:
		/** */
		Oscl::BT::Mesh::
		Node::Persist::
		Creator::Api&		_creatorApi;

	public:
		/** nodeXXXXXX len = 10 */
		char			_fileName[10+1];

		/** */
		uint8_t			_deviceKey[16];

		/** */
		uint16_t		_keyIndex;

		/** */
		uint8_t			_flags;

		/** */
		uint32_t		_ivIndex;

		/** */
		uint16_t		_unicastAddress;

		/** */
		uint32_t		_seqNumber;

		/** */
		uint8_t			_secureNetworkBeaconState;

		/** */
		uint8_t			_defaultTTL;

		/** */
		uint8_t			_proxyState;

		/** */
		uint8_t			_relayState;

		/** */
		uint8_t			_relayRetransmitCount;

		/** */
		uint8_t			_relayRetransmitIntervalSteps;

		/** */
		uint16_t		_heartbeatPublishNetKeyIndex;

		/** */
		uint32_t		_hoursSinceIvUpdateChange;

	public:
		/** */
		File(
			Oscl::BT::Mesh::
			Node::Persist::
			Creator::Api&		creatorApi,
			const char*			fileName,
			const uint8_t		deviceKey[16],
			uint16_t			keyIndex,
			uint8_t				flags,
			uint32_t			ivIndex,
			uint16_t			unicastAddress,
			uint32_t			seqNumber,
			uint8_t				secureNetworkBeaconState,
			uint8_t				defaultTTL,
			uint8_t				proxyState,
			uint8_t				relayState,
			uint8_t				relayRetransmitCount,
			uint8_t				relayRetransmitIntervalSteps,
			uint16_t			heartbeatPublishNetKeyIndex,
			uint32_t			hoursSinceIvUpdateChange
			) noexcept;

		/** */
		void	setSequenceNumber(uint32_t seq) noexcept;

		/** */
		void	setIvIndexState(
					uint32_t	ivIndex,
					bool		ivUpdateInProgress,
					uint32_t	hoursSinceIvUpdateChange
					) noexcept;

		/** */
		void	setSecureNetworkBeaconState(uint8_t state) noexcept;

		/** */
		void	setDefaultTTL(uint8_t ttl) noexcept;

		/** */
		void	setProxyState(
					uint8_t	proxyState
					) noexcept;

		/** */
		void	setRelayState(
					uint8_t	relayState,
					uint8_t	relayRetransmitCount,
					uint8_t	relayRetransmitIntervalSteps
					) noexcept;

		/** */
		void	setHeartbeatPublishNetKeyIndex(uint16_t keyIndex) noexcept;

		/** */
		uint32_t	getSeqNumber() const noexcept;

		/** */
		uint8_t		getSecureNetworkBeaconState() const noexcept;

		/** */
		uint8_t		getDefaultTTL() const noexcept;

		/** */
		uint8_t		getProxyState() const noexcept;

		/** */
		uint8_t		getRelayState() const noexcept;

		/** */
		uint8_t		getRelayRetransmitCount() const noexcept;

		/** */
		uint8_t		getRelayRetransmitIntervalSteps() const noexcept;

		/** */
		uint16_t	getHeartbeatPublishNetKeyIndex() const noexcept;

		/** */
		uint32_t	getHoursSinceIvUpdateChange() const noexcept;

	private:
		/** */
		void	update() noexcept;

		/** */
		void	write(Oscl::FS::File::Api& file) noexcept;

	};

union FileMem {
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock<sizeof(File)>	mem;
	};


/** */
class Part : public Oscl::BT::Mesh::Node::Persist::Creator::Api {

	private:
		/** */
		Oscl::Persist::
		Parent::Zephyr::FS::Part&			_parent;

	private:
		/** */
		static constexpr unsigned	nFileMem	= 10;

		/** */
		FileMem							_fileMem[nFileMem];

		/** */
		Oscl::Queue<
			FileMem
			>							_freeFileMem;

		/** */
		Oscl::Queue<
			File
			>							_fileList;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::Node::Persist::Creator::Api
		/**	Create a new/initialized persistence record.
			RETURN: 0 if out-of-resources or other failure.
		 */
		Oscl::BT::Mesh::Node::Persist::Api*
			create(
				const uint8_t	deviceKey[16],
				uint16_t		keyIndex,
				uint8_t			flags,
				uint32_t		ivIndex,
				uint16_t		unicastAddress,
				uint32_t		seqNumber
				) noexcept;

		/** Destroy the persistence associated.
		 */
		void destroy(Oscl::BT::Mesh::Node::Persist::Api& p) noexcept;

		/** Iterate the existing persistence.
		 */
		void	iterate(
					Oscl::BT::Mesh::Node::
					Persist::Iterator::Api&			iterator
					) noexcept;

	private:
		/** */
		void	createNode(
					const char*		fileName,
					const uint8_t	deviceKey[16],
					uint16_t		keyIndex,
					uint8_t			flags,
					uint32_t		ivIndex,
					uint16_t		unicastAddress,
					uint32_t		seqNumber,
					uint8_t			secureNetworkBeaconState,
					uint8_t			defaultTTL,
					uint8_t			proxyState,
					uint8_t			relayState,
					uint8_t			relayRetransmitCount,
					uint8_t			relayRetransmitIntervalSteps,
					uint16_t		heartbeatPublishNetKeyIndex,
					uint32_t		hoursSinceIvUpdateChange
					) noexcept;
	
		/** */
		void	createNodeFromFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	destroyNodeFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	createNodes() noexcept;

		/** */
		void	destroyNodeFiles() noexcept;

		/** */
		void	write(
					Oscl::BT::Mesh::
					Node::Persist::Api&	api
					) noexcept;

		/** */
		Oscl::BT::Mesh::Node::Persist::Creator::File*
			find(Oscl::BT::Mesh::Node::Persist::Api& api) noexcept;
	};

}
}
}
}
}
}

#endif
