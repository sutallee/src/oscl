/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <new>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/base.h"
#include "oscl/strings/fixed.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"

using namespace Oscl::BT::Mesh::Node::Persist::Creator;

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		_parent(parent)
		{

	for(unsigned i=0;i<nFileMem;++i){
		_freeFileMem.put(&_fileMem[i]);
		}

	createNodes();
	}

Part::~Part() noexcept{
	destroyNodeFiles();
	}

static void	convertBinaryToHexAsciiLabel(
				const uint8_t*	label,
				char*			asciiHex
				) noexcept{
	for(unsigned i=0;i<16;++i){
		Oscl::Strings::octetToLowerCaseHexAscii(
			label[i],
			asciiHex[(i*2)],	// msn
			asciiHex[(i*2)+1]	// lsn
			);
		}
	}

Oscl::BT::Mesh::Node::Persist::Api*
	Part::create(
			const uint8_t	deviceKey[16],
			uint16_t		keyIndex,
			uint8_t			flags,
			uint32_t		ivIndex,
			uint16_t		unicastAddress,
			uint32_t		seqNumber
			) noexcept{

	constexpr uint8_t	secureNetworkBeaconState		= 0;	// Not broadcasting
	constexpr uint8_t	defaultTTL						= 0x7F;	// Max TTL
	constexpr uint8_t	relayState						= 0;	// Disabled
	constexpr uint8_t	proxyState						= 0x01;	// Enabled
	constexpr uint8_t	relayRetransmitCount			= 0;	// Disabled
	constexpr uint8_t	relayRetransmitIntervalSteps	= 0;	// Disabled
	constexpr uint16_t	heartbeatPublishNetKeyIndex		= 0;	// NONE
	constexpr uint32_t	hoursSinceIvUpdateChange		= 0;	// 0

	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		return 0;
		}

	File*
	persist	= new(mem) File(
				*this,
				0,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress,
				seqNumber,
				secureNetworkBeaconState,
				defaultTTL,
				proxyState,
				relayState,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishNetKeyIndex,
				hoursSinceIvUpdateChange
				);

	char	deviceKeyArray[33];

	deviceKeyArray[0]	= '\0';
	deviceKeyArray[sizeof(deviceKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		deviceKey,
		deviceKeyArray
		);

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	unsigned
	offset	= path.length();
	if(offset){
		}

	path += "nodeXXXXXX";

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	Oscl::Zephyr::FS::File::Scope	f;

	bool
	failed	= f.mkstemp(buffer);

	if(failed){
		Oscl::Error::Info::log(
			"%s: mkstemp(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			buffer,
			f.openResult()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	strncpy(
		persist->_fileName,
		&buffer[offset],
		sizeof(persist->_fileName)
		);

	persist->_fileName[sizeof(persist->_fileName)-1]	= '\0';

	persist->write(f);

	_fileList.put(persist);

	return persist;
	}

void	Part::destroy(Oscl::BT::Mesh::Node::Persist::Api& p) noexcept{

	Oscl::BT::Mesh::Node::
	Persist::Creator::File*	persistApi	= find(p);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: unknown persistence API.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	path	+= persistApi->_fileName;

	int
	result	= fs_unlink(path.getString());

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	_fileList.remove(persistApi);

	persistApi->~File();

	_freeFileMem.put((FileMem*)persistApi);
	}

void	Part::iterate(
			Oscl::BT::Mesh::Node::Persist::Iterator::Api&	iterator
			) noexcept{

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		bool
		haltIteration	= iterator.item(
							*persist,
							persist->_deviceKey,
							persist->_keyIndex,
							persist->_flags,
							persist->_ivIndex,
							persist->_unicastAddress,
							persist->_seqNumber,
							persist->_secureNetworkBeaconState,
							persist->_defaultTTL,
							persist->_proxyState,
							persist->_relayState,
							persist->_relayRetransmitCount,
							persist->_relayRetransmitIntervalSteps,
							persist->_heartbeatPublishNetKeyIndex,
							persist->_hoursSinceIvUpdateChange
							);
		if(haltIteration){
			return;
			}
		}
	}

void	Part::createNode(
			const char*		fileName,
			const uint8_t	deviceKey[16],
			uint16_t		keyIndex,
			uint8_t			flags,
			uint32_t		ivIndex,
			uint16_t		unicastAddress,
			uint32_t		seqNumber,
			uint8_t			secureNetworkBeaconState,
			uint8_t			defaultTTL,
			uint8_t			proxyState,
			uint8_t			relayState,
			uint8_t			relayRetransmitCount,
			uint8_t			relayRetransmitIntervalSteps,
			uint16_t		heartbeatPublishNetKeyIndex,
			uint32_t		hoursSinceIvUpdateChange
			) noexcept{

	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of FileMem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	File*
	persist	= new(mem) File(
				*this,
				fileName,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress,
				seqNumber,
				secureNetworkBeaconState,
				defaultTTL,
				proxyState,
				relayState,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishNetKeyIndex,
				hoursSinceIvUpdateChange
				);

	_fileList.put(persist);
	}

void	Part::createNodeFromFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	static const char	prefix[] = {"node"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	Oscl::Zephyr::FS::File::Scope
		f(	filePath,
			false	// create
			);

	if(f.openResult()) {
		Oscl::Error::Info::log(
			"%s\topen() failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			f.openResult()
			);
		return;
		}

	uint8_t			deviceKey[16];
	bool			deviceKeyIsValid	= false;

	uint16_t		keyIndex	= 0;
	bool			keyIndexIsValid	= false;

	uint8_t			flags	= 0;
	bool			flagsAreValid	= false;

	uint32_t		ivIndex	= 0;
	bool			ivIndexIsValid	= false;

	uint16_t		unicastAddress	= 0;
	bool			unicastAddressIsValid	= false;

	uint32_t		seqNumber	= 0;
	bool			seqNumberIsValid	= false;

	uint8_t			secureNetworkBeaconState	= 0;
	bool			secureNetworkBeaconStateIsValid	= false;

	uint8_t			defaultTTL	= 0;
	bool			defaultTtlIsValid	= false;

	uint8_t			proxyState	= 0;
	bool			proxyStateIsValid	= false;

	uint8_t			relayState	= 0;
	bool			relayStateIsValid	= false;

	uint8_t			relayRetransmitCount	= 0;
	bool			relayRetransmitCountIsValid	= false;

	uint8_t			relayRetransmitIntervalSteps	= 0;
	bool			relayRetransmitIntervalStepsIsValid	= false;

	uint16_t		heartbeatPublishNetKeyIndex	= 0;
	bool			heartbeatPublishNetKeyIndexIsValid	= false;

	uint32_t		hoursSinceIvUpdateChange	= 0;
	bool			hoursSinceIvUpdateChangeIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"keyIndex")){
			keyIndex		= (uint16_t)strtoul(value,0,0);
			keyIndexIsValid	= true;
			}
		else if(!strcmp(line,"flags")){
			flags			= (uint8_t)strtoul(value,0,0);
			flagsAreValid	= true;
			}
		else if(!strcmp(line,"ivIndex")){
			ivIndex			= (uint32_t)strtoul(value,0,0);
			ivIndexIsValid	= true;
			}
		else if(!strcmp(line,"unicastAddress")){
			unicastAddress			= (uint16_t)strtoul(value,0,0);
			unicastAddressIsValid	= true;
			}
		else if(!strcmp(line,"seqNumber")){
			seqNumber			= (uint32_t)strtoul(value,0,0);
			seqNumberIsValid	= true;
			}
		else if(!strcmp(line,"secureNetworkBeaconState")){
			secureNetworkBeaconState	= (uint8_t)strtoul(value,0,0);
			secureNetworkBeaconStateIsValid	= true;
			}
		else if(!strcmp(line,"defaultTTL")){
			defaultTTL	= (uint8_t)strtoul(value,0,0);
			defaultTtlIsValid	= true;
			}
		else if(!strcmp(line,"proxyState")){
			proxyState	= (uint8_t)strtoul(value,0,0);
			proxyStateIsValid	= true;
			}
		else if(!strcmp(line,"relayState")){
			relayState	= (uint8_t)strtoul(value,0,0);
			relayStateIsValid	= true;
			}
		else if(!strcmp(line,"relayRetransmitCount")){
			relayRetransmitCount	= (uint8_t)strtoul(value,0,0);
			relayRetransmitCountIsValid	= true;
			}
		else if(!strcmp(line,"relayRetransmitIntervalSteps")){
			relayRetransmitIntervalSteps	= (uint8_t)strtoul(value,0,0);
			relayRetransmitIntervalStepsIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishNetKeyIndex")){
			heartbeatPublishNetKeyIndex	= (uint16_t)strtoul(value,0,0);
			heartbeatPublishNetKeyIndexIsValid	= true;
			}
		else if(!strcmp(line,"hoursSinceIvUpdateChange")){
			hoursSinceIvUpdateChange	= (uint32_t)strtoul(value,0,0);
			hoursSinceIvUpdateChangeIsValid	= true;
			}
		else if(!strcmp(line,"deviceKey")){
			
			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					deviceKey,
					sizeof(deviceKey)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				continue;
				}

			deviceKeyIsValid	= true;
			}
		}

	if(!deviceKeyIsValid){
		Oscl::Error::Info::log(
			"%s: deviceKey is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!keyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: keyIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!flagsAreValid){
		Oscl::Error::Info::log(
			"%s: flags are missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!ivIndexIsValid){
		Oscl::Error::Info::log(
			"%s: ivIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!unicastAddressIsValid){
		Oscl::Error::Info::log(
			"%s: unicastAddress is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!seqNumberIsValid){
		Oscl::Error::Info::log(
			"%s: seqNumber is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!secureNetworkBeaconStateIsValid){
		Oscl::Error::Info::log(
			"%s: secureNetworkBeaconState is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!defaultTtlIsValid){
		Oscl::Error::Info::log(
			"%s: defaultTTL is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!proxyStateIsValid){
		Oscl::Error::Info::log(
			"%s: proxyState is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayStateIsValid){
		Oscl::Error::Info::log(
			"%s: relayState is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayRetransmitCountIsValid){
		Oscl::Error::Info::log(
			"%s: relayRetransmitCount is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayRetransmitIntervalStepsIsValid){
		Oscl::Error::Info::log(
			"%s: relayRetransmitIntervalSteps is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishNetKeyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishNetKeyIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!hoursSinceIvUpdateChangeIsValid){
		// If hoursSinceIvUpdateChange is not valid,
		// we accept the default 0
		}

	createNode(
		fileName,
		deviceKey,
		keyIndex,
		flags,
		ivIndex,
		unicastAddress,
		seqNumber,
		secureNetworkBeaconState,
		defaultTTL,
		proxyState,
		relayState,
		relayRetransmitCount,
		relayRetransmitIntervalSteps,
		heartbeatPublishNetKeyIndex,
		hoursSinceIvUpdateChange
		);
	}

void	Part::destroyNodeFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		__PRETTY_FUNCTION__,
		filePath
		);
	#endif

	static const char	prefix[] = {"node"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	int
	result	= fs_unlink(filePath);

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

void	Part::createNodes() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::createNodeFromFile
			);

	_parent.walkDir(iterator);

	}

void	Part::destroyNodeFiles() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::destroyNodeFile
			);

	_parent.walkDir(iterator);
	}

Oscl::BT::Mesh::Node::Persist::Creator::File*
	Part::find(Oscl::BT::Mesh::Node::Persist::Api&	api) noexcept{

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		Oscl::BT::Mesh::Node::Persist::Api*
		persistApi	= persist;
		if(persistApi == &api){
			return persist;
			}
		}
	return 0;
	}

void	Part::write(
			Oscl::BT::Mesh::
			Node::Persist::Api&	api
			) noexcept{

	Oscl::BT::Mesh::Node::
	Persist::Creator::File*	persistApi	= find(api);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: unknown persistence API.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= persistApi->_fileName;

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	Oscl::Zephyr::FS::File::Scope
		f(	path.getString(),
			true	// create
			);
	int
	result	= f.openResult();

	if(result){
		Oscl::Error::Info::log(
			"%s: open() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		return;
		}

	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	persistApi->write(f);
	}

File::File(
	Oscl::BT::Mesh::
	Node::Persist::
	Creator::Api&	creatorApi,
	const char*		fileName,
	const uint8_t	deviceKey[16],
	uint16_t		keyIndex,
	uint8_t			flags,
	uint32_t		ivIndex,
	uint16_t		unicastAddress,
	uint32_t		seqNumber,
	uint8_t			secureNetworkBeaconState,
	uint8_t			defaultTTL,
	uint8_t			proxyState,
	uint8_t			relayState,
	uint8_t			relayRetransmitCount,
	uint8_t			relayRetransmitIntervalSteps,
	uint16_t		heartbeatPublishNetKeyIndex,
	uint32_t		hoursSinceIvUpdateChange
	) noexcept:
		_creatorApi(creatorApi),
		_keyIndex(keyIndex),
		_flags(flags),
		_ivIndex(ivIndex),
		_unicastAddress(unicastAddress),
		_seqNumber(seqNumber),
		_secureNetworkBeaconState(secureNetworkBeaconState),
		_defaultTTL(defaultTTL),
		_proxyState(proxyState),
		_relayState(relayState),
		_relayRetransmitCount(relayRetransmitCount),
		_relayRetransmitIntervalSteps(relayRetransmitIntervalSteps),
		_heartbeatPublishNetKeyIndex(heartbeatPublishNetKeyIndex),
		_hoursSinceIvUpdateChange(hoursSinceIvUpdateChange)
		{

	memcpy(
		_deviceKey,
		deviceKey,
		sizeof(_deviceKey)
		);

	if(fileName){
		strncpy(
			_fileName,
			fileName,
			sizeof(_fileName)
			);
		}
	}

void	File::update() noexcept{
	_creatorApi.write(
		*this
		);
	}

void	File::write(Oscl::FS::File::Api& f) noexcept{

	char	deviceKeyArray[33];

	deviceKeyArray[0]	= '\0';
	deviceKeyArray[sizeof(deviceKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		_deviceKey,
		deviceKeyArray
		);

	int
	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	char	buffer[1024];

	unsigned
	n	= snprintf(
			buffer,
			sizeof(buffer),
			"deviceKey=%s\n"
			"keyIndex=0x%4.4X\n"
			"flags=0x%2.2X\n"
			"ivIndex=0x%8.8X\n"
			"unicastAddress=0x%4.4X\n"
			"seqNumber=0x%6.6X\n"
			"secureNetworkBeaconState=0x%2.2X\n"
			"defaultTTL=0x%2.2X\n"
			"proxyState=0x%2.2X\n"
			"relayState=0x%2.2X\n"
			"relayRetransmitCount=0x%2.2X\n"
			"relayRetransmitIntervalSteps=0x%2.2X\n"
			"heartbeatPublishNetKeyIndex=0x%4.4X\n"
			"hoursSinceIvUpdateChange=0x%8.8X\n"
			"",
			deviceKeyArray,
			_keyIndex,
			_flags,
			_ivIndex,
			_unicastAddress,
			_seqNumber,
			_secureNetworkBeaconState,
			_defaultTTL,
			_proxyState,
			_relayState,
			_relayRetransmitCount,
			_relayRetransmitIntervalSteps,
			_heartbeatPublishNetKeyIndex,
			_hoursSinceIvUpdateChange
			);

	buffer[sizeof(buffer)-1]	= '\0';

	if(n >= sizeof(buffer)){
		// truncated
		Oscl::Error::Info::log(
			"%s: snprintf() failed buffer too small.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	ssize_t
	writeResult	= f.write(
					buffer,
					n
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		}
	else {
		unsigned long
		nWritten	= writeResult;

		if(nWritten < n){
			Oscl::Error::Info::log(
				"%s: write() failed. nWritten: %d, expected: %d\n",
				OSCL_PRETTY_FUNCTION,
				nWritten,
				n
				);
			}
		}
	}

void	File::setSequenceNumber(uint32_t seq) noexcept{

	_seqNumber	= seq;
	update();
	}

void	File::setIvIndexState(
			uint32_t	ivIndex,
			bool		ivUpdateInProgress,
			uint32_t	hoursSinceIvUpdateChange
			) noexcept{

	_flags &= ~(1<<1);
	_flags	|= (ivUpdateInProgress)?(1<<1):(0<<1);

	_ivIndex	= ivIndex;

	_hoursSinceIvUpdateChange	= hoursSinceIvUpdateChange;

	update();
	}

void	File::setSecureNetworkBeaconState(uint8_t state) noexcept{

	_secureNetworkBeaconState	= state;

	update();
	}

void	File::setDefaultTTL(uint8_t ttl) noexcept{

	_defaultTTL	= ttl;

	update();
	}

void	File::setProxyState(
			uint8_t	proxyState
			) noexcept{

	_proxyState						= proxyState;

	update();
	}

void	File::setRelayState(
			uint8_t	relayState,
			uint8_t	relayRetransmitCount,
			uint8_t	relayRetransmitIntervalSteps
			) noexcept{

	_relayState						= relayState;
	_relayRetransmitCount			= relayRetransmitCount;
	_relayRetransmitIntervalSteps	= relayRetransmitIntervalSteps;

	update();
	}

void	File::setHeartbeatPublishNetKeyIndex(uint16_t keyIndex) noexcept{

	_heartbeatPublishNetKeyIndex	= keyIndex;

	update();
	}

uint32_t	File::getSeqNumber() const noexcept{
	return _seqNumber;
	}

uint8_t		File::getSecureNetworkBeaconState() const noexcept{
	return _secureNetworkBeaconState;
	}

uint8_t		File::getDefaultTTL() const noexcept{
	return _defaultTTL;
	}

uint8_t		File::getProxyState() const noexcept{
	return _proxyState;
	}

uint8_t		File::getRelayState() const noexcept{
	return _relayState;
	}

uint8_t		File::getRelayRetransmitCount() const noexcept{
	return _relayRetransmitCount;
	}

uint8_t		File::getRelayRetransmitIntervalSteps() const noexcept{
	return _relayRetransmitIntervalSteps;
	}

uint16_t	File::getHeartbeatPublishNetKeyIndex() const noexcept{
	return _heartbeatPublishNetKeyIndex;
	}

uint32_t	File::getHoursSinceIvUpdateChange() const noexcept{
	return _hoursSinceIvUpdateChange;
	}

