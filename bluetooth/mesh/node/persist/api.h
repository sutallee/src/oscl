/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_persist_apih_
#define _oscl_bluetooth_mesh_node_persist_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {
/** */
namespace Persist {

/** */
class Api {
	public:
		/** */
		virtual void	setSequenceNumber(uint32_t seq) noexcept=0;

		/** */
		virtual void	setIvIndexState(
							uint32_t	ivIndex,
							bool		ivUpdateInProgress,
							uint32_t	hoursSinceIvUpdateChange
							) noexcept=0;

		/** */
		virtual void	setSecureNetworkBeaconState(uint8_t state) noexcept=0;

		/** */
		virtual void	setDefaultTTL(uint8_t ttl) noexcept=0;

		/** */
		virtual void	setProxyState(
							uint8_t	proxyState
							) noexcept=0;

		/** */
		virtual void	setRelayState(
							uint8_t	relayState,
							uint8_t	relayRetransmitCount,
							uint8_t	relayRetransmitIntervalSteps
							) noexcept=0;

		/** */
		virtual void	setHeartbeatPublishNetKeyIndex(uint16_t keyIndex) noexcept=0;

		/** */
		virtual uint32_t	getSeqNumber() const noexcept=0;

		/** */
		virtual uint8_t		getSecureNetworkBeaconState() const noexcept=0;

		/** */
		virtual uint8_t		getDefaultTTL() const noexcept=0;

		/** */
		virtual uint8_t		getProxyState() const noexcept=0;

		/** */
		virtual uint8_t		getRelayState() const noexcept=0;

		/** */
		virtual uint8_t		getRelayRetransmitCount() const noexcept=0;

		/** */
		virtual uint8_t		getRelayRetransmitIntervalSteps() const noexcept=0;

		/** */
		virtual uint16_t	getHeartbeatPublishNetKeyIndex() const noexcept=0;

		/** */
		virtual uint32_t	getHoursSinceIvUpdateChange() const noexcept=0;
	};

}
}
}
}
}

#endif
