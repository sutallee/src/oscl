/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_node_creator_apih_
#define _oscl_bluetooth_mesh_node_creator_apih_

#include <stdint.h>
/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Node {
/** */
namespace Creator {

/** */
class Api {
	public:
		/** This operation is called by the context to
			create a new network.
			RETURN: true for failure (e.g. out-of-resources)
		 */
		virtual bool	createNewNode(
							const uint8_t	netKey[16],
							const uint8_t	deviceKey[16],
							uint16_t		keyIndex,
							uint8_t			flags,
							uint32_t		ivIndex,
							uint16_t		unicastAddress
							) noexcept=0;

		/** */
		virtual uint8_t	getNumElements() const noexcept=0;

		/** */
		virtual bool	isProvisioned() const noexcept=0;

		/** */
		virtual bool	hasEnabledProxy() const noexcept=0;

		/** */
		virtual bool	gattConnectionIsActive() const noexcept=0;
	};

}
}
}
}
}

#endif
