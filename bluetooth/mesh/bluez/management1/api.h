/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_management1_apih_
#define _oscl_bluetooth_mesh_bluez_management1_apih_

#include <stdint.h>
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Management1 {

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotAuthorized
		org.bluez.mesh.Error.Busy

 */
class UnprovisionedScanResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotAuthorized(const char* reason) noexcept=0;
		/** */
		virtual void	failedBusy(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotAuthorized
 */
class  UnprovisionedScanCancelResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotAuthorized(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotAuthorized

 */
class  AddNodeResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotAuthorized(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.AlreadyExists
 */
class  CreateSubnetResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.AlreadyExists

 */
class  ImportSubnetResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.DoesNotExist
		org.bluez.mesh.Error.Busy
 */
class  UpdateSubnetResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failedBusy(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
 */
class  DeleteSubnetResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.DoesNotExist
 */
class  SetKeyPhaseResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.AlreadyExists
		org.bluez.mesh.Error.DoesNotExist
 */
class  CreateAppKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.AlreadyExists
		org.bluez.mesh.Error.DoesNotExist
 */
class  ImportAppKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.DoesNotExist
		org.bluez.mesh.Error.InProgress
 */
class  UpdateAppKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failedInProgress(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
 */
class  DeleteAppKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.Failed
		org.bluez.mesh.Error.InvalidArguments
 */
class  ImportRemoteNodeResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
 */
class  DeleteRemoteNodeResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/** */
class Api {
	public:
		/**
    		QDBusPendingReply<> UnprovisionedScan(const QVariantMap &options)

			This method is used by the application that supports
			org.bluez.mesh.Provisioner1 interface to start listening
			(scanning) for unprovisioned devices in the area.

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotAuthorized
				org.bluez.mesh.Error.Busy

			RETURN: true if busy with a previous request.
		 */
		virtual bool unprovisionedScan(
						uint16_t					seconds,
						UnprovisionedScanResultApi&	resultApi
						) noexcept=0;

		/**
    		inline QDBusPendingReply<> UnprovisionedScanCancel()

			This method is used by the application that supports
			org.bluez.mesh.Provisioner1 interface to stop listening
			(scanning) for unprovisioned devices in the area.

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotAuthorized

			RETURN: true if busy with a previous request.
		 */
		virtual bool unprovisionedScanCancel(
						UnprovisionedScanCancelResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> AddNode(const QByteArray &uuid, const QVariantMap &options)

			This method is used by the application that supports
			org.bluez.mesh.Provisioner1 interface to add the
			unprovisioned device specified by uuid, to the Network.

			The options parameter is a dictionary that may contain
			additional configuration info (currently an empty placeholder
			for forward compatibility).

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotAuthorized

			RETURN: true if busy with a previous request.
		 */
		virtual bool addNode(
						const char			uuid[16],
						AddNodeResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> CreateSubnet(ushort net_index)

			This method is used by the application to generate and add a new
			network subnet key.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.AlreadyExists

			RETURN: true if busy with a previous request.
		 */
		virtual bool createSubnet(
						uint16_t					net_index,
						CreateSubnetResultApi&		resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> ImportSubnet(ushort net_index, const QByteArray &net_key)

			This method is used by the application to add a network subnet
			key, that was originally generated by a remote Config Client.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.AlreadyExists

			RETURN: true if busy with a previous request.
		 */
		virtual bool importSubnet(
						uint16_t				net_index,
						const uint8_t			net_key[16],
						ImportSubnetResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> UpdateSubnet(ushort net_index)

			This method is used by the application to generate a new network
			subnet key, and set it's key refresh state to Phase 1.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.DoesNotExist
				org.bluez.mesh.Error.Busy

			RETURN: true if busy with a previous request.
		 */
		virtual bool updateSubnet(
						uint16_t				net_index,
						UpdateSubnetResultApi&	resultApi
				) noexcept=0;

		/**
    		QDBusPendingReply<> DeleteSubnet(ushort net_index)

			This method is used by the application that to delete a subnet.

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments

			RETURN: true if busy with a previous request.
		 */
		virtual bool deleteSubnet(
						uint16_t				net_index,
						DeleteSubnetResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> SetKeyPhase(ushort net_index, uchar phase)

			This method is used to set the master key update phase of the
			given subnet. When finalizing the procedure, it is important
			to CompleteAppKeyUpdate() on all app keys that have been
			updated during the procedure prior to setting phase 3.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.DoesNotExist

			RETURN: true if busy with a previous request.
		 */
		virtual bool setKeyPhase(
						uint16_t				net_index,
						uint8_t					phase,
						SetKeyPhaseResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> CreateAppKey(ushort net_index, ushort app_index)

			This method is used by the application to generate and add a new
			application key.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.AlreadyExists
				org.bluez.mesh.Error.DoesNotExist

			RETURN: true if busy with a previous request.
		 */
		virtual bool createAppKey(
						uint16_t				net_index,
						uint16_t				app_index,
						CreateAppKeyResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> ImportAppKey(ushort net_index, ushort app_index, const QByteArray &app_key)

			This method is used by the application to add an application
			key, that was originally generated by a remote Config Client.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.AlreadyExists
				org.bluez.mesh.Error.DoesNotExist

			RETURN: true if busy with a previous request.
		 */
		virtual bool importAppKey(
						uint16_t				net_index,
						uint16_t				app_index,
						const uint8_t			app_key[16],
						ImportAppKeyResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> UpdateAppKey(ushort app_index)

			This method is used by the application to generate a new
			application key.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.DoesNotExist
				org.bluez.mesh.Error.InProgress

			RETURN: true if busy with a previous request.
		 */
		virtual bool updateAppKey(
						uint16_t				unicastAddress,
						UpdateAppKeyResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> DeleteAppKey(ushort app_index)

			This method is used by the application to delete an application
			key.

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments

			RETURN: true if busy with a previous request.
		 */
		virtual bool deleteAppKey(
						uint16_t				app_index,
						DeleteAppKeyResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> ImportRemoteNode(ushort primary, uchar count, const QByteArray &device_key)

			This method is used by the application to import a remote node
			that has been provisioned by an external process.

			PossibleErrors:
				org.bluez.mesh.Error.Failed
				org.bluez.mesh.Error.InvalidArguments

			RETURN: true if busy with a previous request.
		 */
		virtual bool importRemoteNode(
						uint16_t					primary,
						uint8_t						count,
						const uint8_t				device_key[16],
						ImportRemoteNodeResultApi&	resultApi
						) noexcept=0;

		/**
    		QDBusPendingReply<> DeleteRemoteNode(ushort primary, uchar count)

			This method is used by the application to delete a remote node
			from the local device key database.

			PossibleErrors:
				org.bluez.mesh.Error.InvalidArguments

			RETURN: true if busy with a previous request.
		 */
		virtual bool deleteRemoteNode(
						uint16_t				primary,
						uint8_t					count,
						DeleteRemoteNodeResultApi&		resultApi
						) noexcept=0;

	};

}
}
}
}
}

#endif

