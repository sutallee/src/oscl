/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_management1_composersh_
#define _oscl_bluetooth_mesh_bluez_management1_composersh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Management1 {

/** */
template <class Context>
class UnprovisionedScanResultComposer: public UnprovisionedScanResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotAuthorized)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedBusy)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		UnprovisionedScanResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotAuthorized)(
						const char*	reason
						),
			void	(Context::*failedBusy)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotAuthorized(failedNotAuthorized),
				_failedBusy(failedBusy),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedNotAuthorized(const char* reason) noexcept{
					(_context.*_failedNotAuthorized)(
						reason
						);
					}
		/** */
		void	failedBusy(const char* reason) noexcept{
					(_context.*_failedBusy)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};

/** */
template <class Context>
class UnprovisionedScanCancelResultComposer: public UnprovisionedScanCancelResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);
		/** */
		void	(Context::*const _failedNotAuthorized)(
					const char*	reason
					);
		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		UnprovisionedScanCancelResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotAuthorized)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotAuthorized(failedNotAuthorized),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedNotAuthorized(const char* reason) noexcept{
					return (_context.*_failedNotAuthorized)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class AddNodeResultComposer: public AddNodeResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)();

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotAuthorized)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		AddNodeResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotAuthorized)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotAuthorized(failedNotAuthorized),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedNotAuthorized(const char* reason) noexcept{
					return (_context.*_failedNotAuthorized)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class CreateSubnetResultComposer: public CreateSubnetResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)();

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		CreateSubnetResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class ImportSubnetResultComposer: public ImportSubnetResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		ImportSubnetResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class UpdateSubnetResultComposer: public UpdateSubnetResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedBusy)(
					const char*	reason
					);
		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		UpdateSubnetResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failedBusy)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedDoesNotExist(failedDoesNotExist),
				_failedBusy(failedBusy),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					return (_context.*_failedDoesNotExist)(
							reason
							);
					}
		/** */
		void	failedBusy(const char* reason) noexcept{
					return (_context.*_failedBusy)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class DeleteSubnetResultComposer: public DeleteSubnetResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		DeleteSubnetResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class SetKeyPhaseResultComposer: public SetKeyPhaseResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		SetKeyPhaseResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedDoesNotExist(failedDoesNotExist),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					return (_context.*_failedDoesNotExist)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class CreateAppKeyResultComposer: public CreateAppKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		CreateAppKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failedDoesNotExist(failedDoesNotExist),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					return (_context.*_failedDoesNotExist)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class ImportAppKeyResultComposer: public ImportAppKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		ImportAppKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failedDoesNotExist(failedDoesNotExist),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					return (_context.*_failedDoesNotExist)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class UpdateAppKeyResultComposer: public UpdateAppKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedInProgress)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		UpdateAppKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failedInProgress)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedDoesNotExist(failedDoesNotExist),
				_failedInProgress(failedInProgress),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					return (_context.*_failedDoesNotExist)(
							reason
							);
					}
		/** */
		void	failedInProgress(const char* reason) noexcept{
					return (_context.*_failedInProgress)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class DeleteAppKeyResultComposer: public DeleteAppKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		DeleteAppKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class ImportRemoteNodeResultComposer: public ImportRemoteNodeResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		ImportRemoteNodeResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class DeleteRemoteNodeResultComposer: public DeleteRemoteNodeResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		DeleteRemoteNodeResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};
}
}
}
}
}

#endif

