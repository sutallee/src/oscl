/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_network1_apih_
#define _oscl_bluetooth_mesh_bluez_network1_apih_

#include <stdint.h>
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Network1 {

/**
	org.bluez.mesh.Error.InvalidArguments
	org.bluez.mesh.Error.AlreadyExists,
 */
class JoinResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
 */
class CancelResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

struct SigAddress {
	/** */
	void*	__qitemlink;

	/** */
	uint16_t	address;
	};

struct VendorAddress {
	/** */
	void*	__qitemlink;

	/** */
	uint8_t	label[16];
	};

struct Index {
	/** */
	void*	__qitemlink;

	/** */
	uint16_t	index;
	};

/** */
struct Model {
	/** */
	void*	__qitemlink;

	/** */
	uint16_t					_modelID;
	/** */
	bool						_isVendorModel;
	/** */
	uint16_t					_companyID;
	/** */
	uint32_t					_publicationPeriodInMs;
	/** */
	Oscl::Queue<Index>			_bindings;
	/** */
	Oscl::Queue<SigAddress>		_sigSubscriptions;
	/** */
	Oscl::Queue<VendorAddress>	_vendorSubscriptions;

	/** */
	Model() noexcept:
		_modelID(0),
		_isVendorModel(false),
		_companyID(0),
		_publicationPeriodInMs(0)
		{}

	/** */
	Model(
		uint16_t	modelID,
		bool		isVendorModel,
		uint16_t	companyID = 0
		) noexcept:
		_modelID(modelID),
		_isVendorModel(isVendorModel),
		_companyID(companyID),
		_publicationPeriodInMs(0)
		{}
	};

/** */
struct Element {
	/** */
	void*	__qitemlink;

	/** The offset of the element unicast
		address from the base unicast address
		of the node.
	 */
	uint8_t				elementIndex;

	/** */
	Oscl::Queue<Model>	models;
	};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound,
		org.bluez.mesh.Error.AlreadyExists,
		org.bluez.mesh.Error.Busy,
		org.bluez.mesh.Error.Failed
 */
class AttachResultApi {
	public:
		/** */
		virtual void	success(
							const char*					nodePath,
							const Oscl::Queue<Element>&	elements
							) noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failedBusy(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound
		org.bluez.mesh.Error.Busy
 */
class LeaveResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failedBusy(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.AlreadyExists
 */
class CreateNetworkResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	PossibleErrors:
		org.bluez.mesh.Error.InvalidArguments,
		org.bluez.mesh.Error.AlreadyExists,
		org.bluez.mesh.Error.NotSupported,
		org.bluez.mesh.Error.Failed
 */
class ImportResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedAlreadyExists(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotSupported(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/** */
class Api {
	public:
		/**	This is the first method that an application has to call to
			become a provisioned node on a mesh network. The call will
			initiate broadcasting of Unprovisioned Device Beacon.

			RETURN: true if busy with a previous request.
		 */
		virtual bool join(
						const char*		appRootPath,
						const uint8_t	deviceUUID[16],
						JoinResultApi&	resultApi
						) noexcept=0;

		/**	Cancels an outstanding provisioning request
			initiated by join() method.

			RETURN: true if busy with a previous request.
		 */
		virtual bool cancel(
						CancelResultApi&	resultApi
						) noexcept=0;

		/**	This is the first method that an application must call
			to get access to mesh node functionalities.

			RETURN: true if busy with a previous request.
		 */
		virtual bool attach(
						const char*			appRootPath,
						const uint64_t		token,
						AttachResultApi&	resultApi
						) noexcept=0;

		/**	This removes the configuration information about the mesh node
			identified by the 64-bit token parameter. The token parameter
			has been obtained as a result of successful join() method call.

			RETURN: true if busy with a previous request.
		 */
		virtual bool leave(
						const uint64_t		token,
						LeaveResultApi&		resultApi
						) noexcept=0;

		/**	This is the first method that an application calls to become
			a Provisioner node, and a Configuration Client on a newly
			created Mesh Network.

			RETURN: true if busy with a previous request.
		 */
		virtual bool createNetwork(
						const char*				appRootPath,
						const uint8_t			deviceUUID[16],
						CreateNetworkResultApi&	resultApi
						) noexcept=0;

		/**	This method creates a local mesh node based on node
			configuration that has been generated outside bluetooth-meshd.

			RETURN: true if busy with a previous request.
		 */
		virtual bool import(
						const char*				appRootPath,
						const uint8_t			deviceUUID[16],
						const uint8_t			deviceKey[16],
						const uint8_t			networkKey[16],
						uint16_t				netKeyIndex,
						bool					ivUpdate,
						bool					keyRefresh,
						uint32_t				ivIndex,
						uint16_t				unicastAddress,
						ImportResultApi&		resultApi
				) noexcept=0;

	};

}
}
}
}
}

#endif

