/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_network1_composersh_
#define _oscl_bluetooth_mesh_bluez_network1_composersh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Network1 {

/** */
template <class Context>
class JoinResultComposer: public JoinResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		JoinResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					(_context.*_failedAlreadyExists)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};

/** */
template <class Context>
class CancelResultComposer: public CancelResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		CancelResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}

		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class AttachResultComposer: public AttachResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					const char*					nodePath,
					const Oscl::Queue<Element>&	elements
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedBusy)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		AttachResultComposer(
			Context&	context,
			void	(Context::*success)(
						const char*					nodePath,
						const Oscl::Queue<Element>&	elements
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failedBusy)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotFound(failedNotFound),
				_failedAlreadyExists(failedAlreadyExists),
				_failedBusy(failedBusy),
				_failed(failed)
				{
			}

		/** */
		void	success(
					const char*					nodePath,
					const Oscl::Queue<Element>&	elements
					) noexcept{
					return (_context.*_success)(
							nodePath,
							elements
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					return (_context.*_failedNotFound)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failedBusy(const char* reason) noexcept{
					return (_context.*_failedBusy)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class LeaveResultComposer: public LeaveResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedBusy)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		LeaveResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failedBusy)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotFound(failedNotFound),
				_failedBusy(failedBusy),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					return (_context.*_failedNotFound)(
							reason
							);
					}
		/** */
		void	failedBusy(const char* reason) noexcept{
					return (_context.*_failedBusy)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};

/** */
template <class Context>
class CreateNetworkResultComposer: public CreateNetworkResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		CreateNetworkResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};
/** */
template <class Context>
class ImportResultComposer: public ImportResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedAlreadyExists)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotSupported)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		ImportResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedAlreadyExists)(
						const char*	reason
						),
			void	(Context::*failedNotSupported)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedAlreadyExists(failedAlreadyExists),
				_failedNotSupported(failedNotSupported),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					return (_context.*_success)(
							);
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					return (_context.*_failedInvalidArguments)(
							reason
							);
					}
		/** */
		void	failedAlreadyExists(const char* reason) noexcept{
					return (_context.*_failedAlreadyExists)(
							reason
							);
					}
		/** */
		void	failedNotSupported(const char* reason) noexcept{
					return (_context.*_failedNotSupported)(
							reason
							);
					}
		/** */
		void	failed(const char* reason) noexcept{
					return (_context.*_failed)(
							reason
							);
					}
		};
}
}
}
}
}

#endif

