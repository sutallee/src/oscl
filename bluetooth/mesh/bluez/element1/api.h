/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bluez_element1_apih_
#define _oscl_bluetooth_mesh_bluez_element1_apih_

#include <stdint.h>
#include "oscl/qt/dbus/bluez/mesh/element1/contextapi.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Element1 {

/** */
class MessageObserver {
	public:
		void*	__qitemlink;

	public:
		/** */
		virtual	void	devKeyMessageReceived(
							uint16_t	source,
							bool		remote,
							uint16_t	net_index,
							const void*	message,
							unsigned	messageLength
							) noexcept=0;

		/** */
		virtual void	messageReceived(
							uint16_t	source,
							uint16_t	key_index,
							uint16_t	address,
							const void*	message,
							unsigned	messageLength
							) noexcept=0;

		/**
			The destination is a virtual-address label,
			which is a 16-octet value. The 16-bit virtual address
			hash can be reconstructed using the function:
				uint16_t
				Oscl::BT::Mesh::Address::
					makeVirtualAddress(const uint8_t labelUUID[16]) noexcept;
		 */
		virtual void	messageReceived(
							uint16_t		source,
							uint16_t		key_index,
							const uint8_t	destination[16],
							const void*		message,
							unsigned		messageLength
							) noexcept=0;
	};

/** */
template <class Context>
class MessageObserverComposer : public MessageObserver {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _devKeyMessageReceived)(
					uint16_t	source,
					bool		remote,
					uint16_t	net_index,
					const void*	message,
					unsigned	messageLength
					);

		/** */
		void	(Context::*const _messageReceived)(
					uint16_t	source,
					uint16_t	key_index,
					uint16_t	address,
					const void*	message,
					unsigned	messageLength
					);

		/** */
		void	(Context::*const _messageReceivedVirtual)(
					uint16_t		source,
					uint16_t		key_index,
					const uint8_t	destination[16],
					const void*		message,
					unsigned		messageLength
					);
	public:
		/** */
		MessageObserverComposer(
			Context&	context,
			void	(Context::*devKeyMessageReceived)(
					uint16_t	source,
					bool		remote,
					uint16_t	net_index,
					const void*	message,
					unsigned	messageLength
					),
			void	(Context::*messageReceived)(
					uint16_t	source,
					uint16_t	key_index,
					uint16_t	address,
					const void*	message,
					unsigned	messageLength
					),
			void	(Context::*messageReceivedVirtual)(
					uint16_t		source,
					uint16_t		key_index,
					const uint8_t	destination[16],
					const void*		message,
					unsigned		messageLength
					)
			) noexcept:
				_context(context),
				_devKeyMessageReceived(devKeyMessageReceived),
				_messageReceived(messageReceived),
				_messageReceivedVirtual(messageReceivedVirtual)
				{
			}
			
	private:
		/** */
		void	devKeyMessageReceived(
					uint16_t	source,
					bool		remote,
					uint16_t	net_index,
					const void*	message,
					unsigned	messageLength
					) noexcept {
						(_context.*_devKeyMessageReceived)(
							source,
							remote,
							net_index,
							message,
							messageLength
							);
						}

		/** */
		void	messageReceived(
					uint16_t	source,
					uint16_t	key_index,
					uint16_t	address,
					const void*	message,
					unsigned	messageLength
					) noexcept {
						(_context.*_messageReceived)(
							source,
							key_index,
							address,
							message,
							messageLength
							);
						}

		/** */
		void	messageReceived(
					uint16_t		source,
					uint16_t		key_index,
					const uint8_t	destination[16],
					const void*		message,
					unsigned		messageLength
					) noexcept {
						(_context.*_messageReceivedVirtual)(
							source,
							key_index,
							destination,
							message,
							messageLength
							);
						}
	};

/** */
class ModelUpdateObserver {
	public:
		void*	__qitemlink;

	public:
		/** */
		virtual void	updateModelConfiguration(
							uint16_t				modelID,
							uint16_t				companyID,
							bool					isVendorModel,
							uint32_t				publicationPeriod,
							const uint16_t*			appKeys,
							unsigned				nAppKeys,
							const uint16_t*			subscriptions,
							unsigned				nSubscriptions,
							const Oscl::Qt::DBus::
							BlueZ::Mesh::Element1::
							VirtualLabel*			virtualSubscriptions,
							unsigned				nVirtualSubscriptions
							) noexcept = 0;
	};

/** */
template <class Context>
class ModelUpdateObserverComposer : public ModelUpdateObserver {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _updateModelConfiguration)(
					uint16_t				modelID,
					uint16_t				companyID,
					bool					isVendorModel,
					uint32_t				publicationPeriod,
					const uint16_t*			appKeys,
					unsigned				nAppKeys,
					const uint16_t*			subscriptions,
					unsigned				nSubscriptions,
					const Oscl::Qt::DBus::
					BlueZ::Mesh::Element1::
					VirtualLabel*			virtualSubscriptions,
					unsigned				nVirtualSubscriptions
					);
	public:
		/** */
		ModelUpdateObserverComposer(
			Context&	context,
			void	(Context::*const updateModelConfiguration)(
					uint16_t				modelID,
					uint16_t				companyID,
					bool					isVendorModel,
					uint32_t				publicationPeriod,
					const uint16_t*			appKeys,
					unsigned				nAppKeys,
					const uint16_t*			subscriptions,
					unsigned				nSubscriptions,
					const Oscl::Qt::DBus::
					BlueZ::Mesh::Element1::
					VirtualLabel*			virtualSubscriptions,
					unsigned				nVirtualSubscriptions
					)
			) noexcept:
				_context(context),
				_updateModelConfiguration(updateModelConfiguration)
				{
			}
			
	private:
		/** */
		void	updateModelConfiguration(
					uint16_t				modelID,
					uint16_t				companyID,
					bool					isVendorModel,
					uint32_t				publicationPeriod,
					const uint16_t*			appKeys,
					unsigned				nAppKeys,
					const uint16_t*			subscriptions,
					unsigned				nSubscriptions,
					const Oscl::Qt::DBus::
					BlueZ::Mesh::Element1::
					VirtualLabel*			virtualSubscriptions,
					unsigned				nVirtualSubscriptions
					) noexcept{
						(_context.*_updateModelConfiguration)(
							modelID,
							companyID,
							isVendorModel,
							publicationPeriod,
							appKeys,
							nAppKeys,
							subscriptions,
							nSubscriptions,
							virtualSubscriptions,
							nVirtualSubscriptions
							);
						}
	};

////////

/** */
class Api {
	public:
		/** */
		virtual void	attach(MessageObserver& observer) noexcept=0;

		/** */
		virtual void	detach(MessageObserver& observer) noexcept=0;

		/** */
		virtual void	attach(ModelUpdateObserver& observer) noexcept=0;

		/** */
		virtual void	detach(ModelUpdateObserver& observer) noexcept=0;
	};

}
}
}
}
}

#endif
