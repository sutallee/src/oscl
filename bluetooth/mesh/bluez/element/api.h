/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bluez_element_apih_
#define _oscl_bluetooth_mesh_bluez_element_apih_

#include "oscl/bluetooth/mesh/bluez/element1/api.h"
#include "oscl/bluetooth/mesh/bluez/node1/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Element {

/** */
class Api {
	public:
		/**	*/
		virtual bool send(
						uint16_t				destination,
						uint16_t				appKeyIndex,
						bool					forceSegmented,
						const void*				packet,
						unsigned				packetLength,
						Oscl::BT::Mesh::BlueZ::
						Node1::SendResultApi&	resultApi
						) noexcept=0;

		/**	*/
		virtual bool devKeySend(
						uint16_t					destination,
						bool						remote,
						uint16_t					netKeyIndex,
						bool						forceSegmented,
						const void*					packet,
						unsigned					packetLength,
						Oscl::BT::Mesh::BlueZ::
						Node1::DevKeySendResultApi&	resultApi
						) noexcept=0;
		/** */
		virtual bool addNetKey(
						uint16_t					destination,
						uint16_t					subNetIndex,
						uint16_t					netIndex,
						bool						update,
						Oscl::BT::Mesh::BlueZ::
						Node1::AddNetKeyResultApi&	resultApi
						) noexcept=0;
		/** */
		virtual bool addAppKey(
						uint16_t					destination,
						uint16_t					appIndex,
						uint16_t					netIndex,
						bool						update,
						Oscl::BT::Mesh::BlueZ::
						Node1::AddAppKeyResultApi&	resultApi
						) noexcept=0;
		/** */
		virtual bool publish(
						uint16_t					model,
						bool						forceSegmented,
						bool						isVendorModel,
						uint16_t					companyID,
						const void*					packet,
						unsigned					packetLength,
						Oscl::BT::Mesh::BlueZ::
						Node1::PublishResultApi&	resultApi
						) noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::BlueZ::Element1::Api&	getElement1Api() noexcept=0;
	};

}
}
}
}
}

#endif
