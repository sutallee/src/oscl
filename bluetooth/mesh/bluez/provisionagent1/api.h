/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bluez_provisionagent1_apih_
#define _oscl_bluetooth_mesh_bluez_provisionagent1_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace ProvisionAgent1 {

// /home/mike/root/src/oscl/qt/dbus/bluez/mesh/provisionagent1/contextapi.h
/** */
class PromptObserver {
	public:
		void*	__qitemlink;

	public:
		/** This operation is invoked when the user is being
			prompted to take some action as described by the
			string value.

			FIXME: The documentation does not specified how/when
			the prompt is removed.
		 */
		virtual void	displayString(
							const char*	value
							) noexcept=0;

		/** This operation is invoked when the user is being
			prompted to enter a numeric value on the joining
			device.

			The type indicates the way in which the joining
			device must be manipulated to enter the number.

				"push" - Request pushes on remote button
				"twist" - Request twists on remote knob

				FIXME: These are available in the mesh-api.txt document,
				but do not make sense for this operation.
				"blink" - Locally blink LED
				"beep" - Locally make a noise
				"vibrate" - Locally vibrate
				"out-numeric" - Display value to enter remotely

			The prompt should include the type string.

			FIXME: The documentation does not specified how/when
			the prompt is removed.
		 */
		virtual void	displayNumeric(
							const char*	type,
							uint32_t	number
							) noexcept=0;

		/**
			The client prompts the user for a (32-bit) decimal number
			between 1-99999999 as indicated by the joining device
			using the type of interface.

				"blink" - Enter times remote LED blinked
				"beep" - Enter times remote device beeped
				"vibrate" - Enter times remote device vibrated
				"in-numeric" - Enter remotely displayed value

				FIXME: These are available in the mesh-api.txt document,
				but do not make sense for this operation.
				"push" - Push local button remotely requested times
				"twist" - Twist local knob remotely requested times

			The prompt should include the type string.

			The resulting 32-bit value is copied into the result
			unless the user cancels the operation.

			RETURN: true if the user cancels the operation.
		 */
		virtual bool	promptNumeric(
							const char*				type,
							uint32_t&				result
							) noexcept=0;

		/**
			The client prompts the user for a 16-octet number.
			The resulting 16-octets are copied into the result array.

			RETURN: zero if the user cancels the operation. Otherwise,
			a pointer to the result array is returned.
		 */
		virtual const uint8_t*	promptStaticOob(
									uint8_t	result[16]
									) noexcept=0;

		/**
			The client prompts the user for an alpha-numeric string
			that is being indicated by the joining device.

			The maximum length of the alpha-numeric string is 16 characters.

			The alph-numeric characters are copied into the result array
			and zero padded such that a full 16-octets are copied into
			the result array.

			RETURN: zero if the user cancels the operation. Otherwise,
			a pointer to the result array is returned.
		 */
		virtual const uint8_t*	promptStaticAlphaNumeric(
									uint8_t	result[16]
									) noexcept=0;
	};

/** */
template <class Context>
class PromptObserverComposer : public PromptObserver {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _displayString)(
					const char*	value
					);

		/** */
		void	(Context::*const _displayNumeric)(
					const char*	type,
					uint32_t	number
					);

		/** */
		bool	(Context::*const _promptNumeric)(
					const char*				type,
					uint32_t&				result
					);

		/** */
		const uint8_t*	(Context::*const _promptStaticOob)(
							uint8_t	result[16]
							);

		/** */
		const uint8_t*	(Context::*const _promptStaticAlphaNumeric)(
							uint8_t	result[16]
							);

	public:
		/** */
		PromptObserverComposer(
			Context&	context,
			void	(Context::*displayString)(
						const char*	value
						),
			void	(Context::*displayNumeric)(
						const char*	type,
						uint32_t	number
						),
			bool	(Context::*promptNumeric)(
						const char*				type,
						uint32_t&				result
						),
			const uint8_t*	(Context::*promptStaticOob)(
								uint8_t	result[16]
								),
			const uint8_t*	(Context::*promptStaticAlphaNumeric)(
								uint8_t	result[16]
								)
			) noexcept:
				_context(context),
				_displayString(displayString),
				_displayNumeric(displayNumeric),
				_promptNumeric(promptNumeric),
				_promptStaticOob(promptStaticOob),
				_promptStaticAlphaNumeric(promptStaticAlphaNumeric)
				{
			}
			
	private:
		/** */
		void	displayString(
					const char*	value
					) noexcept {
						(_context.*_displayString)(
							value
							);
						}

		/** */
		void	displayNumeric(
					const char*	type,
					uint32_t	number
					) noexcept{
						(_context.*_displayNumeric)(
							type,
							number
							);
						}

		/** */
		bool	promptNumeric(
					const char*				type,
					uint32_t&				result
					) noexcept{
						return (_context.*_promptNumeric)(
									type,
									result
									);
						}

		/** */
		const uint8_t*	promptStaticOob(
							uint8_t	result[16]
							) noexcept {
								return (_context.*_promptStaticOob)(
											result
											);
								}

		/** */
		const uint8_t*	promptStaticAlphaNumeric(
							uint8_t	result[16]
							) noexcept{
								return (_context.*_promptStaticAlphaNumeric)(
											result
											);
								}
	};

/** */
class Api {
	public:
		/** */
		virtual void	attach(PromptObserver& observer) noexcept=0;

		/** */
		virtual void	detach(PromptObserver& observer) noexcept=0;
	};

}
}
}
}
}

#endif
