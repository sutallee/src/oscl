/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_node1_composersh_
#define _oscl_bluetooth_mesh_bluez_node1_composersh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Node1 {

/** */
template <class Context>
class SendResultComposer: public SendResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotAuthorized)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		SendResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotAuthorized)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotAuthorized(failedNotAuthorized),
				_failedNotFound(failedNotFound),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedNotAuthorized(const char* reason) noexcept{
					(_context.*_failedNotAuthorized)(
						reason
						);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					(_context.*_failedNotFound)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};

/** */
template <class Context>
class DevKeySendResultComposer: public DevKeySendResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		DevKeySendResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotFound(failedNotFound),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					(_context.*_failedNotFound)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};

/** */
template <class Context>
class PublishResultComposer: public PublishResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedDoesNotExist)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		PublishResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedDoesNotExist)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedDoesNotExist(failedDoesNotExist),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedDoesNotExist(const char* reason) noexcept{
					(_context.*_failedDoesNotExist)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};

/** */
template <class Context>
class AddNetKeyResultComposer: public AddNetKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		AddNetKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotFound(failedNotFound),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					(_context.*_failedNotFound)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};
/** */
template <class Context>
class AddAppKeyResultComposer: public AddAppKeyResultApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _success)(
					);

		/** */
		void	(Context::*const _failedInvalidArguments)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failedNotFound)(
					const char*	reason
					);

		/** */
		void	(Context::*const _failed)(
					const char*	reason
					);
	public:
		/** */
		AddAppKeyResultComposer(
			Context&	context,
			void	(Context::*success)(
						),
			void	(Context::*failedInvalidArguments)(
						const char*	reason
						),
			void	(Context::*failedNotFound)(
						const char*	reason
						),
			void	(Context::*failed)(
						const char*	reason
						)
			) noexcept:
				_context(context),
				_success(success),
				_failedInvalidArguments(failedInvalidArguments),
				_failedNotFound(failedNotFound),
				_failed(failed)
				{
			}

		/** */
		void	success() noexcept{
					(_context.*_success)();
					}
		/** */
		void	failedInvalidArguments(const char* reason) noexcept{
					(_context.*_failedInvalidArguments)(
						reason
						);
					}
		/** */
		void	failedNotFound(const char* reason) noexcept{
					(_context.*_failedNotFound)(
						reason
						);
					}
		/** */
		void	failed(const char* reason) noexcept{
					(_context.*_failed)(
						reason
						);
					}
		};
}
}
}
}
}

#endif

