/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_bluetooth_mesh_bluez_node1_apih_
#define _oscl_bluetooth_mesh_bluez_node1_apih_

#include <stdint.h>
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Node1 {

/**
	Possible errors:
		org.bluez.mesh.Error.NotAuthorized
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound
 */
class SendResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotAuthorized(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	Possible errors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound
 */
class DevKeySendResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	Possible errors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound
 */
class AddNetKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	Possible errors:
		org.bluez.mesh.Error.InvalidArguments
		org.bluez.mesh.Error.NotFound
 */
class AddAppKeyResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedNotFound(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
	Possible errors:
		org.bluez.mesh.Error.DoesNotExist
		org.bluez.mesh.Error.InvalidArguments
 */
class PublishResultApi {
	public:
		/** */
		virtual void	success() noexcept=0;
		/** */
		virtual void	failedInvalidArguments(const char* reason) noexcept=0;
		/** */
		virtual void	failedDoesNotExist(const char* reason) noexcept=0;
		/** */
		virtual void	failed(const char* reason) noexcept=0;
		};

/**
Methods:
	void Send(object element_path, uint16 destination, uint16 key_index,
						dict options, array{byte} data)

		This method is used to send a message originated by a local
		model.

		The element_path parameter is the object path of an element from
		a collection of the application elements (see Mesh Application
		Hierarchy section).

		The destination parameter contains the destination address. This
		destination must be a uint16 to a unicast address, or a well
		known group address.

		The key_index parameter determines which application key to use
		for encrypting the message. The key_index must be valid for that
		element, i.e., the application key must be bound to a model on
		this element. Otherwise, org.bluez.mesh.Error.NotAuthorized will
		be returned.

		The options parameter is a dictionary with the following keys
		defined:

			bool ForceSegmented
				Specifies whether to force sending of a short
				message as one-segment payload. If not present,
				the default setting is "false".

		The data parameter is an outgoing message to be encypted by the
		bluetooth-meshd daemon and sent on.

		Possible errors:
			org.bluez.mesh.Error.NotAuthorized
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound

	void DevKeySend(object element_path, uint16 destination, boolean remote,
			uint16 net_index, dict options, array{byte} data)

		This method is used to send a message originated by a local
		model encoded with the device key of the remote node.

		The element_path parameter is the object path of an element from
		a collection of the application elements (see Mesh Application
		Hierarchy section).

		The destination parameter contains the destination address. This
		destination must be a uint16 to a unicast address, or a well
		known group address.

		The remote parameter, if true, looks up the device key by the
		destination address in the key database to encrypt the message.
		If remote is true, but requested key does not exist, a NotFound
		error will be returned. If set to false, the local node's
		device key is used.

		The net_index parameter is the subnet index of the network on
		which the message is to be sent.

		The options parameter is a dictionary with the following keys
		defined:

			bool ForceSegmented
				Specifies whether to force sending of a short
				message as one-segment payload. If not present,
				the default setting is "false".

		The data parameter is an outgoing message to be encypted by the
		meshd daemon and sent on.

		Possible errors:
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound

	void AddNetKey(object element_path, uint16 destination,
			uint16 subnet_index, uint16 net_index, boolean update)

		This method is used to send add or update network key originated
		by the local configuration client to a remote configuration
		server.

		The element_path parameter is the object path of an element from
		a collection of the application elements (see Mesh Application
		Hierarchy section).

		The destination parameter contains the destination address. This
		destination must be a uint16 to a nodes primary unicast address.

		The subnet_index parameter refers to the subnet index of the
		network that is being added or updated. This key must exist in
		the local key database.

		The net_index parameter is the subnet index of the network on
		which the message is to be sent.

		The update parameter indicates if this is an addition or an
		update. If true, the subnet key must be in the phase 1 state of
		the key update procedure.

		Possible errors:
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound

	void AddAppKey(object element_path, uint16 destination,
			uint16 app_index, uint16 net_index, boolean update)

		This method is used to send add or update network key originated
		by the local configuration client to a remote configuration
		server.

		The element_path parameter is the object path of an element from
		a collection of the application elements (see Mesh Application
		Hierarchy section).

		The destination parameter contains the destination address. This
		destination must be a uint16 to a nodes primary unicast address.

		The app_index parameter refers to the application key which is
		being added or updated. This key must exist in the local key
		database.

		The net_index parameter is the subnet index of the network on
		which the message is to be sent.

		The update parameter indicates if this is an addition or an
		update. If true, the subnet key must be in the phase 1 state of
		the key update procedure.

		Possible errors:
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound

	void Publish(object element_path, uint16 model, dict options,
							array{byte} data)

		This method is used to send a publication originated by a local
		model. If the model does not exist, or it has no publication
		record, the method returns org.bluez.mesh.Error.DoesNotExist
		error.

		The element_path parameter is the object path of an element from
		a collection of the application elements (see Mesh Application
		Hierarchy section).

		The model parameter contains a model ID, as defined by the
		Bluetooth SIG. If the options dictionary contains a "Vendor"
		key, then this ID is defined by the specified vendor.

		The options parameter is a dictionary with the following keys
		defined:

			bool ForceSegmented
				Specifies whether to force sending of a short
				message as one-segment payload. If not present,
				the default setting is "false".

			uint16 Vendor
				A 16-bit Company ID as defined by the
				Bluetooth SIG. This key should only exist when
				publishing on a Vendor defined model.

		The data parameter is an outgoing message to be encypted by the
		meshd daemon and sent on.

		Since only one Publish record may exist per element-model, the
		destination and key_index are obtained from the Publication
		record cached by the daemon.

		Possible errors:
			org.bluez.mesh.Error.DoesNotExist
			org.bluez.mesh.Error.InvalidArguments


Properties:
	dict Features [read-only]

		The dictionary that contains information about feature support.
		The following keys are defined:

		boolean Friend

			Indicates the ability to establish a friendship with a
			Low Power node

		boolean LowPower

			Indicates support for operating in Low Power node mode

		boolean Proxy

			Indicates support for GATT proxy

		boolean Relay
			Indicates support for relaying messages

	If a key is absent from the dictionary, the feature is not supported.
	Otherwise, true means that the feature is enabled and false means that
	the feature is disabled.

	boolean Beacon [read-only]

		This property indicates whether the periodic beaconing is
		enabled (true) or disabled (false).

	boolean IvUpdate [read-only]

		When true, indicates that the network is in the middle of IV
		Index Update procedure. This information is only useful for
		provisioning.

	uint32 IvIndex [read-only]

		This property may be read at any time to determine the IV_Index
		that the current network is on. This information is only useful
		for provisioning.

	uint32 SecondsSinceLastHeard [read-only]

		This property may be read at any time to determine the number of
		seconds since mesh network layer traffic was last detected on
		this node's network.

	array{uint16} Addresses [read-only]

		This property contains unicast addresses of node's elements.

	uint32 SequenceNumber [read-only]

		This property may be read at any time to determine the
		sequence number.

 */
class Api {
	public:
		/**	
			void Send(
				object		element_path,
				uint16		destination,
				uint16		key_index,
				dict		options,
				array{byte}	data
				)

			This method is used to send a message originated by a local
			model.

			Possible errors:
				org.bluez.mesh.Error.NotAuthorized
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotFound

			RETURN: true if busy with a previous request.
		 */
		virtual bool send(
						const char*		elementPath,
						uint16_t		destination,
						uint16_t		appKeyIndex,
						bool			forceSegmented,
						const void*		packet,
						unsigned		packetLength,
						SendResultApi&	resultApi
						) noexcept=0;

		/**	
			void DevKeySend(
				object		element_path,
				uint16		destination,
				boolean		remote,
				uint16		net_index,
				dict		options,
				array{byte}		data
				)

			This method is used to send a message originated by a local
			model encoded with the device key of the remote node.

			NOTE: If I understand correctly, the remote parameter is
			true when sending a request from a Foundation Model Client
			to a destination Foundation Model Server, and false
			when sending a response to a destination on a Foundation
			Model Server.

			Possible errors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotFound

			RETURN: true if busy with a previous request.
		 */
		virtual bool devKeySend(
						const char*				elementPath,
						uint16_t				destination,
						bool					remote,
						uint16_t				netKeyIndex,
						bool					forceSegmented,
						const void*				packet,
						unsigned				packetLength,
						DevKeySendResultApi&	resultApi
						) noexcept=0;
		/**
			void AddNetKey(
					object element_path,
					uint16 destination,
					uint16 subnet_index,
					uint16 net_index,
					boolean update
					)

			This method is used to send add or update network key originated
			by the local configuration client to a remote configuration
			server.

			Possible errors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotFound

			RETURN: true if busy with a previous request.
		 */
		virtual bool addNetKey(
						const char*				elementPath,
						uint16_t				destination,
						uint16_t				subNetIndex,
						uint16_t				netIndex,
						bool					update,
						AddNetKeyResultApi&		resultApi
						) noexcept=0;
		/**
			void AddAppKey(
					object element_path,
					uint16 destination,
					uint16 app_index,
					uint16 net_index,
					boolean update
					)

			This method is used to send add or update network key originated
			by the local configuration client to a remote configuration
			server.

			Possible errors:
				org.bluez.mesh.Error.InvalidArguments
				org.bluez.mesh.Error.NotFound

			RETURN: true if busy with a previous request.
		 */
		virtual bool addAppKey(
						const char*				elementPath,
						uint16_t				destination,
						uint16_t				appIndex,
						uint16_t				netIndex,
						bool					update,
						AddAppKeyResultApi&		resultApi
						) noexcept=0;
		/**
			void Publish(
					object element_path,
					uint16 model,
					dict options,
					array{byte} data
					)

			This method is used to send a publication originated by a local
			model. If the model does not exist, or it has no publication
			record, the method returns org.bluez.mesh.Error.DoesNotExist
			error.

			The options parameter is a dictionary with the following keys
			defined:

					bool ForceSegmented
						Specifies whether to force sending of a short
						message as one-segment payload. If not present,
						the default setting is "false".

					uint16 Vendor
						A 16-bit Company ID as defined by the
						Bluetooth SIG. This key should only exist when
						publishing on a Vendor defined model.

			Possible errors:
				org.bluez.mesh.Error.DoesNotExist
				org.bluez.mesh.Error.InvalidArguments

			RETURN: true if busy with a previous request.
		 */
		virtual bool publish(
						const char*				elementPath,
						uint16_t				model,
						bool					forceSegmented,
						bool					isVendorModel,
						uint16_t				companyID,
						const void*				packet,
						unsigned				packetLength,
						PublishResultApi&		resultApi
						) noexcept=0;
		/**
			dict Features [read-only]

				The dictionary that contains information about feature support.
				The following keys are defined:

				boolean Friend

					Indicates the ability to establish a friendship with a
					Low Power node

				boolean LowPower

					Indicates support for operating in Low Power node mode

				boolean Proxy

					Indicates support for GATT proxy

				boolean Relay
					Indicates support for relaying messages

			If a key is absent from the dictionary, the feature is not supported.
			Otherwise, true means that the feature is enabled and false means that
			the feature is disabled.

			RETURN: true if failed.
		 */
		virtual bool	features(
							bool&	friendSupported,
							bool&	friendEnabled,
							bool&	lowPowerSupported,
							bool&	lowPowerEnabled,
							bool&	proxySupported,
							bool&	proxyEnabled,
							bool&	relaySupported,
							bool&	relayEnabled
							) noexcept=0;
		/**
			boolean Beacon [read-only]

				This property indicates whether the periodic beaconing is
				enabled (true) or disabled (false).

			RETURN: true if failed.
		 */
		virtual bool	beacon(
							bool&	enabled
							) noexcept=0;
		/**
			boolean IvUpdate [read-only]

				When true, indicates that the network is in the middle of IV
				Index Update procedure. This information is only useful for
				provisioning.

			RETURN: true if failed.
		 */
		virtual bool	ivUpdate(
							bool&	ivUpdateActive
							) noexcept=0;
		/**
			uint32 IvIndex [read-only]

				This property may be read at any time to determine the IV_Index
				that the current network is on. This information is only useful
				for provisioning.

			RETURN: true if failed.
		 */
		virtual bool	ivIndex(
							uint32_t&	ivIndex
							) noexcept=0;
		/**
			uint32 SecondsSinceLastHeard [read-only]

				This property may be read at any time to determine the number of
				seconds since mesh network layer traffic was last detected on
				this node's network.

			RETURN: true if failed.
		 */
		virtual bool	secondsSinceLastHeard(
							uint32_t&	seconds
							) noexcept=0;
		/**
			array{uint16} Addresses [read-only]

				This property contains unicast addresses of node's elements.

			RETURN: true if failed.
		 */
		virtual bool	addresses(
							uint16_t	addresses[],
							unsigned	maxAddresses
							) noexcept=0;
		/**
			uint32 SequenceNumber [read-only]

				This property may be read at any time to determine the
				sequence number.

			RETURN: true if failed.
		 */
		virtual bool	sequenceNumber(
							uint32_t&	sequenceNumber
							) noexcept=0;

	};

}
}
}
}
}

#endif

