/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bluez_provisioner1_apih_
#define _oscl_bluetooth_mesh_bluez_provisioner1_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace BlueZ {
/** */
namespace Provisioner1 {

/** */
class AddNodeResultObserver {
	public:
		void*	__qitemlink;

	public:
		/** */
		virtual void	addNodeComplete(
							const uint8_t		uuid[16],
							uint16_t			unicast,
							uint8_t				count
							) noexcept=0;

		/** */
		virtual void	addNodeFailed(
							const uint8_t		uuid[16],
							const char*			reason
							) noexcept=0;
	};

/** */
template <class Context>
class AddNodeResultObserverComposer : public AddNodeResultObserver {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _addNodeComplete)(
					const uint8_t	uuid[16],
					uint16_t		unicast,
					uint8_t			count
					);

		/** */
		void	(Context::*const _addNodeFailed)(
					const uint8_t	uuid[16],
					const char*		reason
					);

	public:
		/** */
		AddNodeResultObserverComposer(
			Context&	context,
			void		(Context::*addNodeComplete)(
							const uint8_t	uuid[16],
							uint16_t		unicast,
							uint8_t			count
							),
			void		(Context::*addNodeFailed)(
							const uint8_t	uuid[16],
							const char*		reason
							)
			) noexcept:
				_context(context),
				_addNodeComplete(addNodeComplete),
				_addNodeFailed(addNodeFailed)
				{
			}
			
	private:
		/** */
		void	addNodeComplete(
					const uint8_t	uuid[16],
					uint16_t		unicast,
					uint8_t			count
					) noexcept{
						(_context.*_addNodeComplete)(
							uuid,
							unicast,
							count
							);
						}

		/** */
		void	addNodeFailed(
					const uint8_t	uuid[16],
					const char*		reason
					) noexcept{
						(_context.*_addNodeFailed)(
							uuid,
							reason
							);
						}
	};

/** */
class ScanResultObserver {
	public:
		void*	__qitemlink;

	public:
		/** */
		virtual void	scanResult(
							int16_t             rssi,
							const uint8_t		uuid[16],
							uint16_t			oobInformation,
							bool				uriHashIsValid,
							uint32_t			uriHash
							) noexcept=0;
		
	};

/** */
template <class Context>
class ScanResultObserverComposer : public ScanResultObserver {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*_scanResult)(
					int16_t             rssi,
					const uint8_t		uuid[16],
					uint16_t			oobInformation,
					bool				uriHashIsValid,
					uint32_t			uriHash
					);

	public:
		/** */
		ScanResultObserverComposer(
			Context&	context,
			void		(Context::*scanResult)(
							int16_t             rssi,
							const uint8_t		uuid[16],
							uint16_t			oobInformation,
							bool				uriHashIsValid,
							uint32_t			uriHash
							)
			) noexcept:
				_context(context),
				_scanResult(scanResult)
				{
			}
			
	private:
		/** */
		void	scanResult(
					int16_t             rssi,
					const uint8_t		uuid[16],
					uint16_t			oobInformation,
					bool				uriHashIsValid,
					uint32_t			uriHash
					) noexcept{
						(_context.*_scanResult)(
							rssi,
							uuid,
							oobInformation,
							uriHashIsValid,
							uriHash
							);
						}
		
	};

/** */
class Api {
	public:
		/** */
		virtual void	attach(ScanResultObserver& observer) noexcept=0;

		/** */
		virtual void	detach(ScanResultObserver& observer) noexcept=0;

		/** */
		virtual void	attach(AddNodeResultObserver& observer) noexcept=0;

		/** */
		virtual void	detach(AddNodeResultObserver& observer) noexcept=0;
	};

}
}
}
}
}

#endif
