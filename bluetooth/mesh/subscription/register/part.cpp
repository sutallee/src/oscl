/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/bluetooth/mesh/address/values.h"

using namespace Oscl::BT::Mesh::Subscription::Register;

Part::Part(
	RecordMem*		recordMem,
	unsigned		nRecordMem
	) noexcept{

	for(unsigned i=0;i<nRecordMem;++i){
		_freeRecordMem.put(&recordMem[i]);
		}

	}

const Oscl::BT::Mesh::Subscription::Register::Record*
	Part::registerAddress(
		uint16_t	address,
		const void*	labelUUID
		) noexcept{

	Oscl::BT::Mesh::Subscription::Register::Record*
	r	= find(
			address,
			labelUUID
			);

	if(r){
		r->incrementReferenceCount();
		return r;
		}

	RecordMem*
	mem	= _freeRecordMem.get();

	if(!mem){
		return 0;
		}

	r	= new(mem) Record(
					address,
					labelUUID
					);

	_subscriptionList.put(r);

	_subject.changed();

	return r;
	}

void	Part::unregisterAddress(
			const Oscl::BT::Mesh::
			Subscription::Register::Record&	record
			) noexcept{

	Oscl::BT::Mesh::Subscription::Register::Record*
	r	= find(&record);

	if(!r){
		return;
		}

	r->decrementReferenceCount();

	if(r->unreferenced()){

		_subscriptionList.remove(r);

		r->~Record();

		_freeRecordMem.put((RecordMem*)r);

		_subject.changed();
		}
	}

Oscl::Change::Subject::Api&	Part::getSubjectApi() noexcept{
	return _subject;
	}

void	Part::iterate(std::function<bool (uint16_t address, const void* labelUUID)> callback) const noexcept{
	for(
		Record*
		record	= _subscriptionList.first();
		record;
		record	= _subscriptionList.next(record)
		){
		if(callback(record->address(),record->labelUUID())){
			return;
			}
		}
	}

Record*	Part::find(
			uint16_t	address,
			const void*	labelUUID
			) noexcept{

	for(
			Record*	r	= _subscriptionList.first();
			r;
			r	= _subscriptionList.next(r)
		){
		if(r->match(address)){
			if(r->match(labelUUID)){
				return r;
				}
			}
		}

	return 0;
	}

Record*	Part::find(const Record* record) noexcept{

	for(
			Record*	r	= _subscriptionList.first();
			r;
			r	= _subscriptionList.next(r)
		){
		if(record == r){
			return r;
			}
		}

	return 0;
	}

