/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_subscription_register_recordh_
#define _oscl_bluetooth_mesh_subscription_register_recordh_

#include <stdint.h>
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Subscription {
/** */
namespace Register {

/** */
class Record : public Oscl::QueueItem {
	private:
		/** */
		const uint16_t	_address;

		/** */
		unsigned		_refCount;

		/** The labelUUID is only valid for
			virtual addresses.
			Where: (_address & 0xC000) == 0x8000
		 */
		uint8_t			_labelUUID[16];

	public:
		/** */
		Record(
			uint16_t		address,
			const void*		labelUUID = 0
			) noexcept;

		/** */
		bool	match(uint16_t address) const noexcept{
					return (address == _address);
					}

		/** */
		bool	match(const void* labelUUID) const noexcept;

		/** */
		inline uint16_t	address() const noexcept{
						return _address;
						};

		/** */
		const uint8_t*	labelUUID() const noexcept;

		/** */
		bool	unreferenced() const noexcept{
					return (!_refCount);
					}

		/** */
		inline void	incrementReferenceCount() noexcept{
						++_refCount;
						};

		/** */
		inline void	decrementReferenceCount() noexcept{
						--_refCount;
						};
	};

/** */
union RecordMem{
	/** */
	void*	__qitemlink;

	/** */
	Oscl::Memory::AlignedBlock<
		sizeof(Oscl::BT::Mesh::Subscription::Register::Record)
		>			record;
	};

}
}
}
}
}

#endif
