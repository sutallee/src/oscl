/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "record.h"
#include "oscl/bluetooth/mesh/address/types.h"

using namespace Oscl::BT::Mesh::Subscription::Register;

Record::Record(
	uint16_t		address,
	const void*		labelUUID
	) noexcept:
		_address(address),
		_refCount(1)
		{

	if(!labelUUID){
		memset(
			_labelUUID,
			0,
			sizeof(_labelUUID)
			);
		return;
		}

	memcpy(
		_labelUUID,
		labelUUID,
		sizeof(_labelUUID)
		);
	}

bool	Record::match(const void* labelUUID) const noexcept{

	if(Oscl::BT::Mesh::Address::isVirtualAddress(_address)){
		if(!labelUUID){
			return false;
			}
		return !memcmp(labelUUID,_labelUUID,16);
		}

	return labelUUID?false:true;
	}

const uint8_t*	Record::labelUUID() const noexcept{

	if(!Oscl::BT::Mesh::Address::isVirtualAddress(_address)){
		return 0;
		}

	return _labelUUID;
	};

