/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/subscription/persist/iterator/composer.h"
#include "oscl/bluetooth/mesh/model/key/persist/iterator/composer.h"

using namespace Oscl::BT::Mesh::Subscription::Server;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::
	Subscription::Persist::Creator::Api&	subscriptionPersistApi,
	Oscl::BT::Mesh::
	Subscription::Register::Api&			subscriptionRegisterApi
	) noexcept:
		_subscriptionPersistApi(subscriptionPersistApi),
		_subscriptionRegisterApi(subscriptionRegisterApi)
		{

	for(unsigned i=0;i<nSubscriptionRecords;++i){
		_freeSubscriptionRecords.put(&_subscriptionRecordMem[i]);
		}

	Oscl::BT::Mesh::Subscription::
	Persist::Iterator::Composer<Part>
		constructorSubscriptionPersistIterator(
			*this,
			&Part::constructorNextSubscriptionItem
			);

	subscriptionPersistApi.iterate(constructorSubscriptionPersistIterator);
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

bool	Part::subscriptionMatch(uint16_t dst) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		dst
		);
	#endif

	if(findFirst(dst)){
		return true;
		}

	return false;
	}

void	Part::iterate(
			std::function<
				bool (
					uint16_t	address,
					const void*	labelUUID
					)
			>		callback
			) const noexcept{
	for(
		Oscl::BT::Mesh::Subscription::Record*
		record	= _subscriptions.first();
		record;
		record	= _subscriptions.next(record)
		){
		if(callback(record->address(),record->labelUUID())){
			return;
			}
		}
	}

Oscl::BT::Mesh::Subscription::Record*	Part::findFirst(uint16_t address) noexcept{

	for(
		Oscl::BT::Mesh::Subscription::Record*
		record	= _subscriptions.first();
		record;
		record	= _subscriptions.next(record)
		){
		if(record->address() == address){
			return record;
			}
		}

	return 0;
	}

Oscl::BT::Mesh::Subscription::Record*	Part::findNext(Oscl::BT::Mesh::Subscription::Record* prev) noexcept{

	if(!prev){
		// This shouldn't happen
		return 0;
		}

	Oscl::BT::Mesh::Subscription::Record* record;

	for(
		record	= _subscriptions.first();
		record;
		record	= _subscriptions.next(record)
		){
		if(record == prev){
			break;
			}
		}

	if(!record){
		// This shouldn't happen either
		return 0;
		}

	for(
		record	= _subscriptions.next(record);
		record;
		record	= _subscriptions.next(record)
		){
		if(record->address() == prev->address()){
			break;
			}
		}

	return 0;
	}

bool	Part::addSubscription(
			uint16_t	address,
			const void*	labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(Oscl::BT::Mesh::Address::isVirtualAddress(address)){
		if(!labelUUID){
			Oscl::Error::Info::log(
				"%s: virtual address must have labelUUID.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}
	else {
		if(labelUUID){
			Oscl::Error::Info::log(
				"%s: labelUUID must have virtual address.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	for(
		Oscl::BT::Mesh::Subscription::Record*
		record	= findFirst(address);
		record;
		record	= findNext(record)
		){
		if(record->match(labelUUID)){
			// We've already got one!
			return true;
			}
		}

	/*	If we get here, then there is no
		matching subscription already in
		the subscription list.
	 */

	bool
	failed	= createSubscription(
				address,
				labelUUID
				);

	if(failed){
		return true;
		}

	Oscl::BT::Mesh::Subscription::Persist::Api*
	persistApi	= _subscriptionPersistApi.create(
					address,
					(const uint8_t*)labelUUID
					);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: Unable to create persistence.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return false;
	}

bool	Part::createSubscription(
			uint16_t	address,
			const void*	labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %p, address: 0x%4.4x, labelUUID: %p\n",
		OSCL_PRETTY_FUNCTION,
		this,
		address,
		labelUUID
		);
	#endif

	const Oscl::BT::Mesh::
	Subscription::Register::Record*
	r	= _subscriptionRegisterApi.registerAddress(
			address,
			labelUUID
			);

	if(!r){
		Oscl::Error::Info::log(
			"%s: out of registration memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::BT::Mesh::Subscription::RecordMem*
	mem	= _freeSubscriptionRecords.get();

	if(!mem){

		Oscl::Error::Info::log(
			"%s: out of subscription memory.\n",
			OSCL_PRETTY_FUNCTION
			);

		_subscriptionRegisterApi.unregisterAddress(*r);

		return true;
		}

	Oscl::BT::Mesh::Subscription::Record*
	record	= new(mem)
				Oscl::BT::Mesh::Subscription::Record(
					*r
					);

	_subscriptions.put(record);

	return false;
	}

bool	Part::deleteSubscription(
			uint16_t	address,
			const void*	labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(Oscl::BT::Mesh::Address::isVirtualAddress(address)){
		if(!labelUUID){
			Oscl::Error::Info::log(
				"%s: virtual address must have labelUUID.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}
	else {
		if(labelUUID){
			Oscl::Error::Info::log(
				"%s: labelUUID must have virtual address.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	Oscl::BT::Mesh::Subscription::Record*	record;

	for(
		record	= findFirst(address);
		record;
		record	= findNext(record)
		){
		/*	At this point, we know the address matches.
		 */
		if(record->match(labelUUID)){
			// We have a match
			break;
			}
		}

	if(!record){
		// No match found
		Oscl::Error::Info::log(
			"%s: No match found for address: 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			address
			);
		return true;
		}

	_subscriptions.remove(record);

	_subscriptionRegisterApi.unregisterAddress(record->subRecord());

	record->~Record();

	_freeSubscriptionRecords.put((Oscl::BT::Mesh::Subscription::RecordMem*)record);

	Oscl::BT::Mesh::Subscription::Persist::Api*
	persistApi	= findSubscriptionPersistApi(
					address,
					labelUUID
					);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: persistApi: %p\n",
		OSCL_PRETTY_FUNCTION,
		persistApi
		);
	#endif

	if(persistApi){
		_subscriptionPersistApi.destroy(*persistApi);
		}
	else {
		Oscl::Error::Info::log(
			"%s: no persistent subscription address: 0x%4.4X.\n",
			OSCL_PRETTY_FUNCTION,
			address
			);
		}

	return false; // success
	}

bool	Part::deleteAllSubscriptions() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Subscription::Record*	record;

	while((record = _subscriptions.first())){
		deleteSubscription(
			record->address(),
			record->labelUUID()
			);
		}

	return false; // success
	}

bool	Part::overwriteSubscription(
			uint16_t	address,
			const void*	labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(Oscl::BT::Mesh::Address::isVirtualAddress(address)){
		if(!labelUUID){
			Oscl::Error::Info::log(
				"%s: virtual address must have labelUUID.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}
	else {
		if(labelUUID){
			Oscl::Error::Info::log(
				"%s: labelUUID must have virtual address.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	deleteAllSubscriptions();

	return addSubscription(
				address,
				labelUUID
				);
	}

unsigned	Part::getSubscriptionList(
				uint16_t*	addressList,
				unsigned	maxAddresses
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(!addressList){
		return 0;
		}

	unsigned	nCopied	= 0;

	for(
		Oscl::BT::Mesh::Subscription::Record*
		record	= _subscriptions.first();
		record && (nCopied < maxAddresses);
		record	= _subscriptions.next(record),++nCopied
		){
		addressList[nCopied]	= record->address();
		}

	return nCopied;
	}

bool	Part::constructorNextSubscriptionItem(
			Oscl::BT::Mesh::
			Subscription::Persist::Api&	api,
			uint16_t					address,
			const uint8_t*				labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" address: 0x%4.4X\n"
		" labelUUID: %p\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		address,
		labelUUID
		);
	#endif

	bool
	failed	= createSubscription(
				address,
				labelUUID
				);

	bool
	haltIteration	= failed?true:false;

	return haltIteration;
	}

Oscl::BT::Mesh::Subscription::Persist::Api*
	Part::findSubscriptionPersistApi(
		uint16_t	address,
		const void*	labelUUID
		) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X, labelUUID: %p\n",
		__PRETTY_FUNCTION__,
		address,
		labelUUID
		);
	#endif

	_findSubscriptionAddress	= address;
	_findSubscriptionLabelUUID	= labelUUID;
	_findSubscriptionPersistApi	= 0;

	Oscl::BT::Mesh::Subscription::
	Persist::Iterator::Composer<Part>
		findSubscriptionPersistIterator(
			*this,
			&Part::findNextSubscriptionItem
			);

	_subscriptionPersistApi.iterate(findSubscriptionPersistIterator);

	return _findSubscriptionPersistApi;
	}

bool	Part::findNextSubscriptionItem(
			Oscl::BT::Mesh::
			Subscription::Persist::Api&	api,
			uint16_t					address,
			const uint8_t*				labelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" address: 0x%4.4X\n"
		" labelUUID: %p\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		address,
		labelUUID
		);
	#endif

	if(_findSubscriptionAddress != address){
		return false;
		}

	if(_findSubscriptionLabelUUID){
		if(!labelUUID){
			return false;
			}
		}

	if(!_findSubscriptionLabelUUID){
		if(labelUUID){
			return false;
			}
		}

	if(!_findSubscriptionLabelUUID){
		_findSubscriptionPersistApi	= &api;
		return true;
		}

	bool
	noMatch	= memcmp(
				labelUUID,
				_findSubscriptionLabelUUID,
				16
				);

	if(noMatch){
		return false;
		}

	_findSubscriptionPersistApi	= &api;

	return true;	// halt
	}

