/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_subscription_server_parth_
#define _oscl_bluetooth_mesh_subscription_server_parth_

#include "api.h"
#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/subscription/register/api.h"
#include "oscl/bluetooth/mesh/subscription/record.h"
#include "oscl/bluetooth/mesh/subscription/persist/creator/api.h"
#include "oscl/bluetooth/mesh/subscription/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Subscription {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Foundation model.
 */
class Part :
	public Oscl::BT::Mesh::Subscription::Api,
	public Oscl::BT::Mesh::Subscription::Server::Api
	{
	private:
		/** */
		Oscl::BT::Mesh::
		Subscription::Persist::
		Creator::Api&					_subscriptionPersistApi;

		/** */
		Oscl::BT::Mesh::
		Subscription::Register::Api&	_subscriptionRegisterApi;

	private:
		/** */
		static constexpr unsigned		nSubscriptionRecords = 3;

		/** */
		Oscl::BT::Mesh::
		Subscription::RecordMem			_subscriptionRecordMem[nSubscriptionRecords];

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Subscription::RecordMem
			>							_freeSubscriptionRecords;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Subscription::Record
			>							_subscriptions;

	private:
		/** */
		uint16_t							_findSubscriptionAddress;

		/** */
		const void*							_findSubscriptionLabelUUID;

		/** */
		Oscl::BT::Mesh::Subscription::
		Persist::Api*						_findSubscriptionPersistApi;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::
			Subscription::Persist::Creator::Api&	subscriptionPersistApi,
			Oscl::BT::Mesh::
			Subscription::Register::Api&			subscriptionRegisterApi
			) noexcept;

		/**	*/
		void	stop() noexcept;

	public: // Oscl::BT::Mesh::Subscription::Server::Api
		/** */
		void	iterate(
					std::function<
						bool (
							uint16_t	address,
							const void*	labelUUID
							)
					>		callback
					) const noexcept;

		/** RETURN: true if the dst address matches a
			subscription address.
		 */
		bool	subscriptionMatch(uint16_t dst) noexcept;

	private:
		/** */
		Oscl::BT::Mesh::Subscription::Record*	findFirst(uint16_t address) noexcept;

		/** */
		Oscl::BT::Mesh::Subscription::Record*	findNext(Oscl::BT::Mesh::Subscription::Record* prev) noexcept;

		/** RETURN: 0 if there is no matching index. */
		Oscl::BT::Mesh::Subscription::Persist::Api*
			findSubscriptionPersistApi(
				uint16_t	address,
				const void*	labelUUID
				) noexcept;

	private: // Oscl::BT::Mesh::Subscription::Api

		/** RETURN: true if un-supported or failed. */
		bool	addSubscription(
					uint16_t	address,
					const void*	labelUUID
					) noexcept;

		/** RETURN: true if un-supported or failed. */
		bool	deleteSubscription(
					uint16_t	address,
					const void*	labelUUID
					) noexcept;

		/** RETURN: true if un-supported or failed. */
		bool	deleteAllSubscriptions() noexcept;

		/** RETURN: true if un-supported or failed. */
		bool	overwriteSubscription(
					uint16_t	address,
					const void*	labelUUID
					) noexcept;

		/** RETURN: number of addresses written to addressList */
		unsigned	getSubscriptionList(
						uint16_t*	addressList,
						unsigned	maxAddresses
						) noexcept;

	private:
		/** RETURN: true if un-supported or failed. */
		bool	createSubscription(
					uint16_t	address,
					const void*	labelUUID
					) noexcept;

	private: // Oscl::BT::Mesh::Subscription::Persist::Iterator::Composer

		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	constructorNextSubscriptionItem(
					Oscl::BT::Mesh::
					Subscription::Persist::Api&	api,
					uint16_t					address,
					const uint8_t*				labelUUID
					) noexcept;

	private: // Oscl::BT::Mesh::Subscription::Persist::Iterator::Composer	_findSubscriptionPersistIterator;

		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	findNextSubscriptionItem(
					Oscl::BT::Mesh::
					Subscription::Persist::Api&	api,
					uint16_t					address,
					const uint8_t*				labelUUID
					) noexcept;
	};

}
}
}
}
}

#endif
