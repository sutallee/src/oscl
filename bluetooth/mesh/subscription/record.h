/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_subscription_recordh_
#define _oscl_bluetooth_mesh_subscription_recordh_

#include <stdint.h>
#include <string.h>
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"
#include "oscl/bluetooth/mesh/subscription/register/record.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Subscription {

/** */
class Record : public Oscl::QueueItem {
	private:
		/** */
		const Oscl::BT::Mesh::
		Subscription::Register::Record&	_rec;

	public:
		/** */
		Record(
			const Oscl::BT::Mesh::
			Subscription::Register::Record&	rec
			) noexcept:
				_rec(rec)
				{
			}

		/** */
		inline bool	match(uint16_t address) const noexcept{
						return _rec.match(address);
						};

		/** */
		inline bool	match(const void* labelUUID) const noexcept{
						return _rec.match(labelUUID);
						};

		/** */
		inline uint16_t	address() const noexcept{
						return _rec.address();
						};

		/** */
		inline const uint8_t*	labelUUID() const noexcept{
						return _rec.labelUUID();
						};

		const Oscl::BT::Mesh::
		Subscription::Register::Record&	subRecord() const noexcept{
			return _rec;
			}
	};

/** */
union RecordMem{
	/** */
	void*	__qitemlink;

	/** */
	Oscl::Memory::AlignedBlock<
		sizeof(Oscl::BT::Mesh::Subscription::Record)
		>			record;
	};

}
}
}
}

#endif
