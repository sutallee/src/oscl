/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sigmodelidsh_
#define _oscl_bluetooth_mesh_model_sigmodelidsh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace SigModelID {

constexpr uint16_t	configurationServer					= 0x0000;
constexpr uint16_t	configurationClient					= 0x0001;
constexpr uint16_t	healthServer						= 0x0002;
constexpr uint16_t	healthClient						= 0x0003;

constexpr uint16_t	genericOnOffServer					= 0x1000;
constexpr uint16_t	genericOnOffClient					= 0x1001;
constexpr uint16_t	genericLevelServer					= 0x1002;
constexpr uint16_t	genericLevelClient					= 0x1003;
constexpr uint16_t	genericDefaultTransitionTimeServer	= 0x1004;
constexpr uint16_t	genericDefaultTransitionTimeClient	= 0x1005;
constexpr uint16_t	genericPowerOnOffServer				= 0x1006;
constexpr uint16_t	genericPowerOnOffSetupServer		= 0x1007;
constexpr uint16_t	genericPowerOnOffClient				= 0x1008;
constexpr uint16_t	genericPowerLevelServer				= 0x1009;
constexpr uint16_t	genericPowerLevelSetupServer		= 0x100A;
constexpr uint16_t	genericPowerLevelClient				= 0x100B;
constexpr uint16_t	genericBatteryServer				= 0x100C;
constexpr uint16_t	genericBatteryClient				= 0x100D;
constexpr uint16_t	genericLocationServer				= 0x100E;
constexpr uint16_t	genericLocationSetupServer			= 0x100F;
constexpr uint16_t	genericLocationClient				= 0x1010;
constexpr uint16_t	genericAdminPropertyServer			= 0x1011;
constexpr uint16_t	genericManufacturerPropertyServer	= 0x1012;
constexpr uint16_t	genericUserPropertyServer			= 0x1013;
constexpr uint16_t	genericClientPropertyServer			= 0x1014;
constexpr uint16_t	genericPropertyClient				= 0x1015;
constexpr uint16_t	sensorServer						= 0x1100;
constexpr uint16_t	sensorSetupServer					= 0x1101;
constexpr uint16_t	sensorClient						= 0x1102;
constexpr uint16_t	timeServer							= 0x1200;
constexpr uint16_t	timeSetupServer						= 0x1201;
constexpr uint16_t	timeClient							= 0x1202;
constexpr uint16_t	sceneServer							= 0x1203;
constexpr uint16_t	sceneSetupServer					= 0x1204;
constexpr uint16_t	sceneClient							= 0x1205;
constexpr uint16_t	schedulerServer						= 0x1206;
constexpr uint16_t	schedulerSetupServer				= 0x1207;
constexpr uint16_t	schedulerClient						= 0x1208;
constexpr uint16_t	lightLightnessServer				= 0x1300;
constexpr uint16_t	lightLightnessSetupServer			= 0x1301;
constexpr uint16_t	lightLightnessClient				= 0x1302;
constexpr uint16_t	lightCTLServer						= 0x1303;
constexpr uint16_t	lightCTLSetupServer					= 0x1304;
constexpr uint16_t	lightCTLClient						= 0x1305;
constexpr uint16_t	lightCTLTemperatureServer			= 0x1306;
constexpr uint16_t	lightHSLServer						= 0x1307;
constexpr uint16_t	lightHSLSetupServer					= 0x1308;
constexpr uint16_t	lightHSLClient						= 0x1309;
constexpr uint16_t	lightHSLHueServer					= 0x130A;
constexpr uint16_t	lightHSLSaturationServer			= 0x130B;
constexpr uint16_t	lightxyLServer						= 0x130C;
constexpr uint16_t	lightxyLSetupServer					= 0x130D;
constexpr uint16_t	lightxyLClient						= 0x130E;
constexpr uint16_t	lightLCServer						= 0x130F;
constexpr uint16_t	lightLCSetupServer					= 0x1310;
constexpr uint16_t	lightLCClient						= 0x1311;

}
}
}
}

#endif
