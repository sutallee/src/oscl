/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_key_recordh_
#define _oscl_bluetooth_mesh_key_recordh_

#include <stdint.h>
#include <string.h>
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Key {

/** */
class Record : public Oscl::QueueItem {
	private:
		/** */
		const uint16_t	_index;

		/** */
		uint8_t			_key[16];

		/** */
		uint8_t			_id;

		/** */
		bool			_updating;

		/** */
		uint8_t			_newKey[16];

		/** */
		uint8_t			_newID;

	public:
		/** */
		Record(
			uint16_t		index,
			const uint8_t	key[16],
			uint8_t			id,
			const uint8_t*	newKey	= 0,
			uint8_t			newID	= 0
			) noexcept;

		/** */
		inline uint16_t	index() const noexcept{
						return _index;
						};

		/** */
		inline const uint8_t*	key() const noexcept{
						return _key;
						};

		/** */
		inline uint8_t	id() const noexcept{
						return _id;
						};

		/** */
		inline bool	updating() const noexcept{
						return _updating;
						};

		/** */
		inline const uint8_t*	newKey() const noexcept{
						return _newKey;
						};

		/** */
		inline uint8_t	newID() const noexcept{
						return _newID;
						};

		/** */
		void	setNewKey(
					const uint8_t*	newKey,
					uint8_t			newID
					) noexcept;

		/** */
		void	revokeOldKey() noexcept;
	};

/** */
union RecordMem{
	/** */
	void*	__qitemlink;

	/** */
	Oscl::Memory::AlignedBlock<
		sizeof(Oscl::BT::Mesh::Key::Record)
		>			record;
	};

}
}
}
}

#endif
