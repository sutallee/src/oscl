/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_key_result_varh_
#define _oscl_bt_mesh_key_result_varh_

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Key {

/** */
namespace Result {

/**	This Var class implements an EMORF. The Flyweight Pattern
	is used in the implementation of the state subclasses
	allowing a single set of concrete state classes to be shared
	between any number of instances of this EMORF.
 */
/**	
 */
class Var {
	public:
		/**	This interface a kind of GOF visitor
			that allows the state of the variable
			to be determined/queried.
		 */
		class Query {
			public:
				/** Make the compiler happy. */
				virtual ~Query() {}
				/**	This operation is invoked by the variable
					when the variable is in the Success state.
				 */
				virtual void	success() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidIndex state.
				 */
				virtual void	invalidIndex() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the NoSuchIndex state.
				 */
				virtual void	noSuchIndex() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the IndexAlreadyExists state.
				 */
				virtual void	indexAlreadyExists() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the OutOfResources state.
				 */
				virtual void	outOfResources() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidState state.
				 */
				virtual void	invalidState() noexcept=0;

			};
	public:
		/**	This interface defines the operations that are
			supported by all of the states that the variable
			can assume. Sub-classes of this variable are to
			have no state (member variables) of their own.
			This allows a GOF fly-weight pattern to be used
			such that many instances of the Var can share the
			same concrete state instances.
		 */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}

				/**	Compare this state with another state for equality.
				 */
				virtual bool	operator ==(const State& other) const noexcept=0;

				/**	Compare this state with another state for inequality.
				 */
				virtual bool	operator !=(const State& other) const noexcept=0;

				/** Allow the concrete state to be queried to
					determine the actual state.
				 */
				virtual void	accept(Query& q) const noexcept=0;

			};
	private:
		/**	This member determines the current state of the EMORF.
		 */
		const State*	_state;

	public:
		/**	The constructor requires a reference to its context.
		 */
		Var(const State& initial=getSuccess()) noexcept;

		/** Be happy compiler! */
		virtual ~Var() {}

	public:
		/**	This operation is invoked to determine the current
			state of the variable via visitation.
		 */
		void	accept(Query& q) noexcept;

		/**	Compare the state of this Var with another for equality.
		 */
		bool	operator == (const Var& other) const noexcept;

		/**	Compare the state of this Var with another for inequality.
		 */
		bool	operator != (const Var& other) const noexcept;

		/**	Assign the state of another Var to this var.
		 */
		void	operator=(const Var& newState) noexcept;

		/**	Assign the specified state to this Var.
		 */
		void	operator=(const State& newState) noexcept;

		/**	Returns a reference to the current state.
		 */
		const State&	getCurrentState() const noexcept;

	public:
		/** This operation is invoked to change the EMORF
			to the Success state.
		 */
		void success() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidIndex state.
		 */
		void invalidIndex() noexcept;

		/** This operation is invoked to change the EMORF
			to the NoSuchIndex state.
		 */
		void noSuchIndex() noexcept;

		/** This operation is invoked to change the EMORF
			to the IndexAlreadyExists state.
		 */
		void indexAlreadyExists() noexcept;

		/** This operation is invoked to change the EMORF
			to the OutOfResources state.
		 */
		void outOfResources() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidState state.
		 */
		void invalidState() noexcept;

	public:
		/** This operation returns an EMORF state reference
			to the Success state.
		 */
		static const State& getSuccess() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidIndex state.
		 */
		static const State& getInvalidIndex() noexcept;

		/** This operation returns an EMORF state reference
			to the NoSuchIndex state.
		 */
		static const State& getNoSuchIndex() noexcept;

		/** This operation returns an EMORF state reference
			to the IndexAlreadyExists state.
		 */
		static const State& getIndexAlreadyExists() noexcept;

		/** This operation returns an EMORF state reference
			to the OutOfResources state.
		 */
		static const State& getOutOfResources() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidState state.
		 */
		static const State& getInvalidState() noexcept;

	};
}
}
}
}
}
#endif
