/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_key_persist_creator_dummy_parth_
#define _oscl_bluetooth_mesh_key_persist_creator_dummy_parth_

#include "oscl/bluetooth/mesh/key/persist/creator/api.h"
#include "oscl/bluetooth/mesh/key/persist/api.h"
#include "oscl/bluetooth/mesh/key/persist/iterator/api.h"

#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/persist/parent/posix/part.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Key {
/** */
namespace Persist {
/** */
namespace Creator {

/** */
class File :
		public Oscl::QueueItem,
		public Oscl::BT::Mesh::Key::Persist::Api
		{
	public:
		/** keyXXXXXX len = 9 */
		char			_fileName[9+1];

		/** */
		const uint16_t	_keyIndex;

		/** */
		uint8_t			_key[16];

		/** */
		const uint8_t	_aid;

		/** */
		bool			_updating;

		/** */
		uint8_t			_newKey[16];

		/** */
		const uint8_t	_newAID;

	public:
		/** */
		File(
			const char*		fileName,
			uint16_t		keyIndex,
			const uint8_t	key[16],
			uint8_t			aid,
			const uint8_t*	newKey	= 0,
			uint8_t			newAID	= 0
			) noexcept;
	};

union FileMem {
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock<sizeof(File)>	mem;
	};


/** */
class Part : public Oscl::BT::Mesh::Key::Persist::Creator::Api {

	private:
		/** */
		Oscl::Persist::
		Parent::Posix::Part&			_parent;

	private:
		/** */
		static constexpr unsigned	nFileMem	= 10;

		/** */
		FileMem							_fileMem[nFileMem];

		/** */
		Oscl::Queue<
			FileMem
			>							_freeFileMem;

		/** */
		Oscl::Queue<
			File
			>							_fileList;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Posix::Part&	parent
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::Key::Persist::Creator::Api
		/**	Create a new/initialized persistence record.
			RETURN: 0 if out-of-resources or other failure.
		 */
		Oscl::BT::Mesh::Key::Persist::Api*
			create(
				uint16_t		keyIndex,
				const uint8_t	key[16],
				uint8_t			aid,
				const uint8_t*	newKey	= 0,
				uint8_t			newAID	= 0
				) noexcept;

		/** Destroy the persistence associated.
		 */
		void destroy(Oscl::BT::Mesh::Key::Persist::Api& p) noexcept;

		/** Iterate the existing persistence.
		 */
		void	iterate(
					Oscl::BT::Mesh::Key::
					Persist::Iterator::Api&			iterator
					) noexcept;

	private:
		/** */
		void	createKey(
					const char*		fileName,
					uint16_t		keyIndex,
					const uint8_t	key[16],
					uint8_t			aid,
					const uint8_t*	newKey	= 0,
					uint8_t			newAID	= 0
					) noexcept;
	
		/** */
		void	createKeyFromFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	destroyKeyFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	destroyKeyFiles() noexcept;

		/** */
		void	createKeys() noexcept;
	};

}
}
}
}
}
}

#endif
