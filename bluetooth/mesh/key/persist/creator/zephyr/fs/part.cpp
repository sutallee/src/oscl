/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/base.h"
#include "oscl/strings/fixed.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"

using namespace Oscl::BT::Mesh::Key::Persist::Creator;

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		_parent(parent)
		{

	for(unsigned i=0;i<nFileMem;++i){
		_freeFileMem.put(&_fileMem[i]);
		}

	createKeys();
	}

Part::~Part() noexcept{
	destroyKeyFiles();
	}

static void	convertBinaryToHexAsciiLabel(
				const uint8_t*	label,
				char*			asciiHex
				) noexcept{
	for(unsigned i=0;i<16;++i){
		Oscl::Strings::octetToLowerCaseHexAscii(
			label[i],
			asciiHex[(i*2)],	// msn
			asciiHex[(i*2)+1]	// lsn
			);
		}
	}

Oscl::BT::Mesh::Key::Persist::Api*
	Part::create(
				uint16_t		keyIndex,
				const uint8_t	key[16],
				uint8_t			aid,
				const uint8_t*	newKey,
				uint8_t			newAID
				) noexcept{
	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		return 0;
		}

	File*
	persist	= new(mem) File(
				0,
				keyIndex,
				key,
				aid
				);

	char	keyArray[33];

	keyArray[0]	= '\0';
	keyArray[sizeof(keyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		key,
		keyArray
		);

	char	newKeyArray[33];

	newKeyArray[0]	= '\0';
	newKeyArray[sizeof(newKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		key,
		newKeyArray
		);

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	unsigned
	offset	= path.length();
	if(offset){
		}

	path += "keyXXXXXX";

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: file path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	Oscl::Zephyr::FS::File::Scope	f;

	bool
	failed	= f.mkstemp(buffer);

	if(failed){
		Oscl::Error::Info::log(
			"%s: mkstemp(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			buffer,
			f.openResult()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	strncpy(
		persist->_fileName,
		&buffer[offset],
		sizeof(persist->_fileName)
		);

	persist->_fileName[sizeof(persist->_fileName)-1]	= '\0';

	persist->write(f);

	_fileList.put(persist);

	return persist;
	}

void	Part::destroy(Oscl::BT::Mesh::Key::Persist::Api& p) noexcept{

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: %p\n",
			__PRETTY_FUNCTION__,
			persist
			);
		#endif

		Oscl::BT::Mesh::Key::Persist::Api*
		persistApi	= persist;
		if(persistApi == &p){

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: match (%s)\n",
				__PRETTY_FUNCTION__,
				persist->_fileName
				);
			#endif

			_fileList.remove(persist);

			path	+= persist->_fileName;

			if(path.truncated()){
				Oscl::Error::Info::log(
					"%s: trunacated path: %s\n",
					OSCL_PRETTY_FUNCTION,
					path.getString()
					);
				return;
				}

			int
			result	= fs_unlink(path.getString());

			if(result){
				Oscl::Error::Info::log(
					"%s fs_unlink() failed: %d\n",
					OSCL_PRETTY_FUNCTION,
					result
					);
				}

			_fileList.remove(persist);

			persist->~File();

			_freeFileMem.put((FileMem*)persist);

			break;
			}
		}
	}

void	Part::iterate(
			Oscl::BT::Mesh::Key::Persist::Iterator::Api&	iterator
			) noexcept{

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		bool
		haltIteration	= iterator.item(
							*persist,
							persist->_keyIndex,
							persist->_key,
							persist->_aid,
							persist->_updating,
							persist->_newKey,
							persist->_newAID
							);
		if(haltIteration){
			return;
			}
		}
	}

void	Part::createKey(
			const char*		fileName,
			uint16_t		keyIndex,
			const uint8_t	key[16],
			uint8_t			aid,
			const uint8_t*	newKey,
			uint8_t			newAID
			) noexcept{

	#ifdef DEBUG_TRACE 
	Oscl::Error::Info::log(
		"%s:\n"
		" fileName: \"%s\"\n"
		" keyIndex: 0x%4.4X\n"
		" key: %p\n"
		" aid: 0x%2.2X\n"
		" newKey: %p\n"
		" newAID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		fileName,
		keyIndex,
		key,
		aid,
		newKey,
		newAID
		);
	#endif

	FileMem*
	mem	= _freeFileMem.get();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of FileMem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	File*
	persist	= new(mem) File(
				fileName,
				keyIndex,
				key,
				aid
				);

	_fileList.put(persist);
	}

void	Part::createKeyFromFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" filePath: \"%s\"\n"
		" fileName: \"%s\"\n"
		"",
		__PRETTY_FUNCTION__,
		filePath,
		fileName
		);
	#endif

	static const char	prefix[] = {"key"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	Oscl::Zephyr::FS::File::Scope
		f(	filePath,
			false	// create
			);

	if(f.openResult()) {
		Oscl::Error::Info::log(
			"%s\topen() failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			f.openResult()
			);
		return;
		}

	uint16_t	keyIndex	= 0;
	bool		keyIndexIsValid	= false;

	uint8_t		keyArray[16];
	bool		keyIsValid		= false;

	uint16_t	aid	= 0;
	bool		aidIsValid	= false;

	uint8_t		newKeyArray[16];
	bool		newKeyIsValid	= false;

	uint16_t	newAID	= 0;
	bool		newAidIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"keyIndex")){
			keyIndex			= (uint16_t)strtoul(value,0,0);
			keyIndexIsValid	= true;
			}
		else if(!strcmp(line,"aid")){
			aid			= (uint8_t)strtoul(value,0,0);
			aidIsValid	= true;
			}
		else if(!strcmp(line,"key")){

			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					keyArray,
					sizeof(keyArray)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				}

			keyIsValid	= true;
			}
		else if(!strcmp(line,"newAID")){
			newAID			= (uint8_t)strtoul(value,0,0);
			newAidIsValid	= true;
			}
		else if(!strcmp(line,"newKey")){

			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					newKeyArray,
					sizeof(newKeyArray)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				}

			newKeyIsValid	= true;
			}
		}

	if(!keyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: keyIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!keyIsValid){
		Oscl::Error::Info::log(
			"%s: key is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!aidIsValid){
		Oscl::Error::Info::log(
			"%s: aid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	createKey(
		fileName,
		keyIndex,
		keyArray,
		aid,
		(newKeyIsValid && newAidIsValid)?
			newKeyArray:
			0
			,
		newAID
		);
	}

void	Part::destroyKeyFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		__PRETTY_FUNCTION__,
		filePath
		);
	#endif

	static const char	prefix[] = {"key"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	int
	result	= fs_unlink(filePath);

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

void	Part::createKeys() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::createKeyFromFile
			);

	_parent.walkDir(iterator);
	}

void	Part::destroyKeyFiles() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::destroyKeyFile
			);

	_parent.walkDir(iterator);
	}


File::File(
	const char*		fileName,
	uint16_t		keyIndex,
	const uint8_t	key[16],
	uint8_t			aid,
	const uint8_t*	newKey,
	uint8_t			newAID
	) noexcept:
		_keyIndex(keyIndex),
		_aid(aid),
		_updating(newKey?true:false),
		_newAID(newAID)
		{

	memcpy(
		_key,
		key,
		sizeof(_key)
		);

	if(newKey){
		memcpy(
			_newKey,
			newKey,
			sizeof(_newKey)
			);
		}

	if(fileName){
		strncpy(
			_fileName,
			fileName,
			sizeof(_fileName)
			);
		}
	}

void	File::write(Oscl::FS::File::Api& f) noexcept{
	int
	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	char	keyArray[33];

	keyArray[0]	= '\0';
	keyArray[sizeof(keyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		_key,
		keyArray
		);

	char	newKeyArray[33];

	newKeyArray[0]	= '\0';
	newKeyArray[sizeof(newKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		_newKey,
		newKeyArray
		);

	char	buffer[128];

	unsigned
	n	= snprintf(
			buffer,
			sizeof(buffer),
			"keyIndex=0x%4.4X\n"
			"key=%s\n"
			"aid=0x%2.2X\n"
			"newKey=%s\n"
			"newAID=0x%2.2X\n"
			"",
			_keyIndex,
			keyArray,
			_aid,
			newKeyArray,
			_newAID
			);

	buffer[sizeof(buffer)-1]	= '\0';

	if(n >= sizeof(buffer)){
		// truncated
		Oscl::Error::Info::log(
			"%s: snprintf() failed buffer too small.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	ssize_t
	writeResult	= f.write(
					buffer,
					n
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		}
	else {
		unsigned long
		nWritten	= writeResult;

		if(nWritten < n){
			Oscl::Error::Info::log(
				"%s: write() failed. n: %d\n",
				OSCL_PRETTY_FUNCTION,
				n
				);
			}
		}
	}
