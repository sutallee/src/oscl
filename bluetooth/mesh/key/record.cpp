/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "record.h"

using namespace Oscl::BT::Mesh::Key;

Record::Record(
	uint16_t		index,
	const uint8_t	key[16],
	uint8_t			id,
	const uint8_t*	newKey,
	uint8_t			newID
	) noexcept:
		_index(index),
		_id(id),
		_updating(newKey?true:false),
		_newID(newID)
		{

	memcpy(
		_key,
		key,
		sizeof(_key)
		);

	if(newKey){
		memcpy(
			_newKey,
			newKey,
			sizeof(_newKey)
			);
		}
	}

void	Record::setNewKey(
			const uint8_t*	newKey,
			uint8_t			newID
			) noexcept{
	memcpy(
		_newKey,
		newKey,
		sizeof(_newKey)
		);

	_newID	= newID;

	_updating	= true;
	}

void	Record::revokeOldKey() noexcept{

	if(!_updating){
		return;
		}

	memcpy(
		_key,
		_newKey,
		16
		);

	_id	= _newID;

	_updating	= false;
	}

