/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_key_pack_encoderh_
#define _oscl_bluetooth_mesh_key_pack_encoderh_

#include "oscl/encoder/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Key {
/** */
namespace Pack {

/*
	Some model messages contain multiple
	key-indicies. 
	Some messages include one, two or multiple key indexes.
	To enable efficient packing, two key indexes are packed
	into three octets.
	Where an odd number of key indexes need to be packed,
	all but the last key index are packed into sequences
	of three octets (see Figure 4.3), and the last key
	index is packed into two octets (see Figure 4.4).
	Where an even number of key indexes need to be packed,
	they are all packed into sequences of three octets.
 */

/** */
class Encoder {
	private:
		/** */
		Oscl::Encoder::Api& _encoder;

		/** */
		unsigned	_count;

		/** */
		uint16_t	_previousKey;

		/** */
		bool		_overflow;

	public:
		/** */
		Encoder(Oscl::Encoder::Api& encoder) noexcept;

		/** RETURN: true for overflow */
		bool	encode(uint16_t key) noexcept;

		/** RETURN: true for overflow */
		bool	finalize() noexcept;

		/** */
		bool	overflow() const noexcept;

		/** After finialize() this returns the number
			of keys successfully encoded.
		 */
		unsigned	count() const noexcept;

	private:
		/**
			If this operation overflows, the
			_previousKey is still valid such that
			finalize may write its value in 
			possibly without overlow.

			RETURN: true for overflow
		 */
		bool	encodePair(
					uint16_t	key0,
					uint16_t	key1
					) noexcept;

		/** */
		void	encodeLast(
					uint16_t	key
					) noexcept;
	};

}
}
}
}
}


#endif
