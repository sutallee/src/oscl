/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "decoder.h"

using namespace Oscl::BT::Mesh::Key::Pack;

Decoder::Decoder(
	Oscl::Decoder::Api& decoder
	) noexcept:
		_decoder(decoder),
		_overflow(false)
		{
	}

void	Decoder::iterate(std::function<void (uint16_t key)> callback) const noexcept{

	for(;;){

		uint8_t	buffer[3];

		_decoder.copyOut(
			&buffer[0],
			2
			);

		if(_decoder.underflow()){
			return;
			}

		uint16_t
		key	= (buffer[1] & 0x0F);
		key	<<= 8;
		key	|= buffer[0];

		callback(key);

		_decoder.copyOut(
			&buffer[2],
			1
			);

		if(_decoder.underflow()){
			return;
			}

		key	= buffer[2];
		key <<= 4;
		key	|= (buffer[1] & 0xF0) >> 4;

		callback(key);

		}
	}
