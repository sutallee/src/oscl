/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "encoder.h"

using namespace Oscl::BT::Mesh::Key::Pack;

static void	packEven(
			uint16_t	keyIndex,
			uint8_t		packedOutput[2]
			) noexcept{

	keyIndex	&= 0x0FFF;	// only 12-bits are "significant"

	packedOutput[0]	= keyIndex & 0x00FF;
	packedOutput[1]	= keyIndex >> 8;
	}

static void	packOdd(
			uint16_t	keyIndex,
			uint8_t		packedOutput[2]
			) noexcept{

	keyIndex	&= 0x0FFF;	// only 12-bits are "significant"

	packedOutput[0]	|= (keyIndex & 0x000F) << 4;
	packedOutput[1]	= keyIndex >> 4;
	}

Encoder::Encoder(
	Oscl::Encoder::Api& encoder
	) noexcept:
		_encoder(encoder),
		_count(0),
		_previousKey(0),
		_overflow(false)
		{
	}

bool	Encoder::encode(uint16_t key) noexcept{

	if(_overflow){
		return true;
		}

	if(_count & 0x0001){
		bool
		overflow	= encodePair(
						_previousKey,
						key
						);

		if(!overflow){
			++_count;
			return false;
			}

		_overflow	= true;

		return _overflow;
		}

	_previousKey	= key;

	++_count;

	return false;
	}

bool	Encoder::finalize() noexcept{

	if(_count & 0x0001){
		encodeLast(_previousKey);
		}

	return _overflow;
	}

bool	Encoder::overflow() const noexcept{
	return _overflow;
	}

bool	Encoder::encodePair(
			uint16_t	key0,
			uint16_t	key1
			) noexcept{

	if(_overflow){
		return false;
		}

	uint8_t	buffer[3];

	packEven(
		key0,
		&buffer[0]
		);

	packOdd(
		key1,
		&buffer[1]
		);

	Oscl::Encoder::State	state;
	_encoder.push(state);

	_encoder.copyIn(buffer,3);

	_overflow	= _encoder.overflow();

	_encoder.pop();

	if(!_overflow){
		_encoder.skip(3);
		}

	return _overflow;
	}

void	Encoder::encodeLast(
			uint16_t	key
			) noexcept{

	uint8_t	buffer[2];

	packEven(
		key,
		&buffer[0]
		);

	_encoder.copyIn(buffer,2);

	bool
	overflow	= _encoder.overflow();

	if(overflow){
		--_count;
		_overflow	= overflow;
		}
	}

unsigned	Encoder::count() const noexcept{
	return _count;
	}
