/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_key_iterator_composerh_
#define _oscl_bluetooth_mesh_key_iterator_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Key {
/** */
namespace Iterator {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context, which contains
			the done callback member function.
		 */
		Context&			_context;

	private:
		/** */
		bool	(Context::*_item)(
					unsigned		index,
					const uint8_t	key[16],
					uint8_t			id
					);

	public:
		/** */
		Composer(
			Context&	context,
			bool		(Context::*item)(
							unsigned		index,
							const uint8_t	key[16],
							uint8_t			id
							)
			) noexcept;

	private: // Api
		/** */
		bool	item(
						unsigned		index,
						const uint8_t	key[16],
						uint8_t			id
						) noexcept;
	};

template <class Context>
Composer<Context>::
	Composer(
			Context&	context,
			bool		(Context::*item)(
							unsigned		index,
							const uint8_t	key[16],
							uint8_t			id
							)
			) noexcept:
		_context(context),
		_item(item)
		{
	}

template <class Context>
bool	Composer<Context>::item(
			unsigned		index,
			const uint8_t	key[16],
			uint8_t			id
			) noexcept{
	return (_context.*_item)(
		index,
		key,
		id
		);
	}

}
}
}
}
}


#endif
