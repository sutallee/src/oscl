/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_consth_
#define _oscl_bluetooth_att_consth_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace Size {

/** */
constexpr unsigned	mtuMinimum = 23;

/**	Mesh Profile ATT_MTU 7.1.2.2.2
	The Provisioning Server should support an ATT_MTU size
	equal to or larger than 69 octets to accommodate the
	longest known Provisioning message (see Section 5.3).
 */
constexpr unsigned	meshProvMtuMinimum = 69;

/** Mesh Profile ATT_MTU 7.2.2.2.4
	The [Proxy] server should support an ATT_MTU size
	equal to or larger than 33 octets to be able to pass the
	content of a full Proxy PDU (see Section 6.5).
 */
constexpr unsigned	meshProxyMtuMinimum = 33;

/** */
constexpr unsigned	attributeValueLengthMaximum = 512;

}
}
}
}

#endif
