/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_opcodeh_
#define _oscl_bluetooth_att_opcodeh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace Opcode {
/** */
constexpr uint8_t	errorResponse			= 0x01;
/** */
constexpr uint8_t	exchangeMtuRequest		= 0x02;
/** */
constexpr uint8_t	exchangeMtuResponse		= 0x03;
/** */
constexpr uint8_t	findInformationRequest	= 0x04;
/** */
constexpr uint8_t	findInformationResponse	= 0x05;
/** */
constexpr uint8_t	findByTypeValueRequest	= 0x06;
/** */
constexpr uint8_t	findByTypeValueResponse	= 0x07;
/** */
constexpr uint8_t	readByTypeRequest		= 0x08;
/** */
constexpr uint8_t	readByTypeResponse		= 0x09;
/** */
constexpr uint8_t	readRequest				= 0x0A;
/** */
constexpr uint8_t	readResponse			= 0x0B;
/** */
constexpr uint8_t	readBlobRequest			= 0x0C;
/** */
constexpr uint8_t	readBlobResponse		= 0x0D;
/** */
constexpr uint8_t	readMultipleRequest		= 0x0E;
/** */
constexpr uint8_t	readMultipleResponse	= 0x0F;
/** */
constexpr uint8_t	readByGroupTypeRequest	= 0x10;
/** */
constexpr uint8_t	readByGroupTypeResponse	= 0x11;
/** */
constexpr uint8_t	writeRequest			= 0x12;
/** */
constexpr uint8_t	writeResponse			= 0x13;
/** */
constexpr uint8_t	prepareWriteRequest		= 0x16;
/** */
constexpr uint8_t	prepareWriteResponse	= 0x17;
/** */
constexpr uint8_t	executeWriteRequest		= 0x18;
/** */
constexpr uint8_t	executeWriteResponse	= 0x19;
/** */
constexpr uint8_t	handleValueNotification	= 0x1B;
/** */
constexpr uint8_t	handleValueIndication	= 0x1D;
/** */
constexpr uint8_t	handleValueConfirmation	= 0x1E;
/** */
constexpr uint8_t	writeCommand			= 0x52;
/** */
constexpr uint8_t	signedWriteCommand		= 0x53;

}
}
}
}

#endif
