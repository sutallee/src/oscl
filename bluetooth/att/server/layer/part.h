/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_server_layer_apih_
#define _oscl_bluetooth_att_server_layer_apih_

#include <stdint.h>

#include "oscl/bluetooth/att/server/tx/api.h"
#include "oscl/bluetooth/att/server/rx/api.h"
#include "oscl/bluetooth/l2cap/channel/tx/api.h"
#include "oscl/bluetooth/l2cap/channel/rx/itemcomp.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/endian/decoder/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace Server {
/** */
namespace Layer {

/** */
class Part :
	public Oscl::BT::ATT::Server::TX::Api
	{
	private:
		/** */
		Oscl::BT::ATT::Server::RX::Api&		_upperLayerApi;

		/** */
		Oscl::BT::L2CAP::Channel::TX::Api&	_lowerLayerApi;

		/** */
		Oscl::Pdu::Memory::Api&				_freeStore;

		/** */
		const uint16_t						_mtu;

	private:
		/** */
		Oscl::BT::L2CAP::
		Channel::RX::ItemComposer<Part>		_receiver;

		/** */
		uint16_t&							_attMTU;

	public:
		/** */
		Part(
			Oscl::BT::ATT::Server::RX::Api&		upperLayerApi,
			Oscl::BT::L2CAP::Channel::TX::Api&	lowerLayerApi,
			Oscl::Pdu::Memory::Api&				freeStore,
			uint16_t&							attMTU
			) noexcept;

		/** */
		Oscl::BT::L2CAP::Channel::RX::Item&	getRxItem() noexcept;

	private: // Oscl::BT::ATT::Server::TX::Api
		/**
			uint8_t	opcode	= 0x01;
			RETURN: true on resource failure.
		 */
		bool	sendErrorResponse(
					uint8_t		requestOpcodeInError,
					uint16_t	attributeHandleInError,
					uint8_t		errorCode
					) noexcept;

		/**
			uint8_t	opcode	= 0x02;
			RETURN: true on resource failure.
		 */
		bool	sendExchangeMtuResponse(uint16_t serverRxMtu) noexcept;

		/**
			uint8_t opcode = 0x05,
			uint8_t format = 0x01
			RETURN: true on resource failure.
		 */
		bool	sendFindInformationResponse16bitUUIDs(
					unsigned							nInformationDataRecords,
					const Oscl::BT::ATT::
					Server::TX::InformationData16bit	informationData[]
					) noexcept;

		/**
			uint8_t opcode = 0x05,
			uint8_t format = 0x02
			RETURN: true on resource failure.
		 */
		bool	sendFindInformationResponse128BitUUIDs(
					unsigned							nInformationDataRecords,
					const Oscl::BT::ATT::
					Server::TX::InformationData128bit	informationData[]
					) noexcept;

		/**
			uint8_t opcode = 0x07,
			RETURN: true on resource failure.
		 */
		bool	sendFindByTypeValueResponse(
					uint8_t							nHandles,
					const Oscl::BT::ATT::
					Server::TX::HandleInformation	handleInformation[]
					) noexcept;

		/**
			uint8_t opcode = 0x09,
			length: Size of each attribute/handle-value pair <handle>+<data>
			RETURN: true on resource failure.
		 */
		bool	sendReadByTypeResponse(
					uint8_t						length,
					const Oscl::BT::ATT::
					Server::TX::AttributeData	attributeData[],
					unsigned					nAttributeData
					) noexcept;

		/**
			uint8_t opcode = 0x0B,
			RETURN: true on resource failure.
		 */
		bool	sendReadResponse(
					const uint8_t*	value,
					unsigned		valueLength
					) noexcept;

		/**
			uint8_t opcode = 0x0D,
			RETURN: true on resource failure.
		 */
		bool	sendReadBlobResponse(
					const uint8_t*	partValue,
					unsigned		partValueLength
					) noexcept;

		/**
			uint8_t opcode = 0x0F,
			RETURN: true on resource failure.
		 */
		bool	sendReadMultipleResponse(
					const uint8_t*	setOfValues,
					unsigned		setOfValuesLen
					) noexcept;

		/**
			uint8_t opcode = 0x11,
			RETURN: true on resource failure.
		 */
		bool	sendReadByGroupTypeResponse(
					const Oscl::BT::ATT::
					Server::TX::GroupInformation16bit	attrubteDataList[],
					unsigned							nAttributeData
					) noexcept;

		/**
			uint8_t opcode = 0x13,
			RETURN: true on resource failure.
		 */
		bool	sendWriteResponse() noexcept;

		/**
			uint8_t opcode = 0x17,
			RETURN: true on resource failure.
		 */
		bool	sendPrepareWriteResponse(
					uint16_t		attributeHandle,
					uint16_t		valueOffset,
					const uint8_t	partAttributeValue[],
					unsigned		partAttributeValueLength
					) noexcept;

		/**
			uint8_t opcode = 0x19,
			RETURN: true on resource failure.
		 */
		bool	sendExecuteWriteResponse() noexcept;

		/**
			uint8_t opcode = 0x1B,
			RETURN: true on resource failure.
		 */
		bool	sendHandleValueNotification(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;

		/**
			uint8_t opcode = 0x1B,
			RETURN: true on resource failure.
		 */
		bool	sendHandleValueNotification(
					uint16_t		attributeHandle,
					Oscl::Pdu::Pdu*	attributeValue
					) noexcept;

		/**
			uint8_t opcode = 0x1D,
			RETURN: true on resource failure.
		 */
		bool	sendHandleValueIndication(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;

	private:
		/** */
		bool	processAttErrorResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttExchangeMtuRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttExchangeMtuResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttFindInformationRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttHandlesAnd16bitUUIDList(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttHandlesAnd128bitUUIDList(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttFindInformationResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttFindByTypeValueRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttFindByTypeValueResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadByTypeRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadByTypeResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadBlobRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadBlobResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadMultipleRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadMultipleResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadByGroupTypeRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttReadByGroupTypeResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttWriteRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttWriteResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttPrepareWriteRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttPrepareWriteResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttExecuteWriteRequest(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttExecuteWriteResponse(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttHandleValueNotification(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttHandleValueIndication(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttHandleValueConfirmation(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttWriteCommand(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processAttSignedWriteCommand(Oscl::Decoder::Api& decoder) noexcept;

	private: // Oscl::BT::L2CAP::Channel::RX::ItemComposer _receiver
		/** */
		uint16_t	getChannelID() const noexcept;

		/** */
		void	receive(
					const void* packet,
					unsigned	length
					) noexcept;
	};

}
}
}
}
}

#endif
