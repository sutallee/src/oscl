/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/uuid/string.h"
#include "oscl/bluetooth/uuid/base.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/bluetooth/att/error.h"

using namespace Oscl::BT::ATT::Server::Layer;

static constexpr uint16_t	attChannelID	= 0x0004;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::ATT::Server::RX::Api&		upperLayerApi,
	Oscl::BT::L2CAP::Channel::TX::Api&	lowerLayerApi,
	Oscl::Pdu::Memory::Api&				freeStore,
	uint16_t&							attMTU
	) noexcept:
		_upperLayerApi(upperLayerApi),
		_lowerLayerApi(lowerLayerApi),
		_freeStore(freeStore),
		_mtu(attMTU),
		_receiver(
			*this,
			&Part::getChannelID,
			&Part::receive
			),
		_attMTU(attMTU)
		{
	}

Oscl::BT::L2CAP::Channel::RX::Item&	Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::sendErrorResponse(
			uint8_t		requestOpcodeInError,
			uint16_t	attributeHandleInError,
			uint8_t		errorCode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	opcode	= 0x01;		// Error Response

		encoder.encode(opcode);
		encoder.encode(requestOpcodeInError);
		encoder.encode(attributeHandleInError);
		encoder.encode(errorCode);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendExchangeMtuResponse(uint16_t serverRxMtu) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: MTU %u\n",
		__PRETTY_FUNCTION__,
		serverRxMtu
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	opcode	= 0x03;		// Exchange Mtu Response

		encoder.encode(opcode);
		encoder.encode(serverRxMtu);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendFindInformationResponse16bitUUIDs(
			unsigned							nInformationDataRecords,
			const Oscl::BT::ATT::
			Server::TX::InformationData16bit	informationData[]
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	opcode	= Oscl::BT::ATT::Opcode::findInformationResponse;
		static const uint8_t	format	= 0x01;	// 16-bit UUIDs

		encoder.encode(opcode);
		encoder.encode(format);

		for(
			unsigned i=0;
			i < nInformationDataRecords;
			++i
			){
			encoder.encode(informationData[i].handle);
			encoder.encode(informationData[i].uuid);

			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					__PRETTY_FUNCTION__
					);
				// We'll just pretend this didn't happen.
				break;
				}

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tHandleUuid16Rec: handle: 0x%4.4X, uuid16: 0x%4.4X\n",
				informationData[i].handle,
				informationData[i].uuid
				);
			#endif
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendFindInformationResponse128BitUUIDs(
			unsigned							nInformationDataRecords,
			const Oscl::BT::ATT::
			Server::TX::InformationData128bit	informationData[]
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendFindByTypeValueResponse(
			uint8_t							nHandles,
			const Oscl::BT::ATT::
			Server::TX::HandleInformation	handleInformation[]
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendReadByTypeResponse(
			uint8_t						length,
			const Oscl::BT::ATT::
			Server::TX::AttributeData	attributeData[],
			unsigned					nAttributeData
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	opcode	= 0x09;		 // Read By Type Response

		encoder.encode(opcode);
		encoder.encode(length);

		/* A Characteristic Declaration Attribute Value contains:
			[0]		1-octet Characteristic Properties - A bitfield listing the  permitted operations on this characteristic
			[1:2]	2-octet Characteristic Value Handle - A handle to the attribute containing the characteristic value
			[3...]	2,4 or 16-octet Characteristic UUID - The UUID for this particular characteristic

			Notes:
			-	UUIDs (including 128-bit UUIDs) are encoded in Little Endian!
			-	An entry is either 7 octets (16-bit UUID) or 21 octets (128-bit UUID).
			-	Each Characteristic Attribute Value returned has:
				<Attribute Handle>
				<Characteristic Properties>
				<Characteristic Value Attribute Handle>
				<Characteristic UUID>
		 */

		for(unsigned i=0;i<nAttributeData;++i){
			encoder.copyIn(
				attributeData[i].data,
				length
				);

			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					__PRETTY_FUNCTION__
					);
				// We'll just pretend this didn't happen.
				break;
				}
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendReadResponse(
			const uint8_t*	value,
			unsigned		valueLength
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendReadBlobResponse(
			const uint8_t*	partValue,
			unsigned		partValueLength
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendReadMultipleResponse(
			const uint8_t*	setOfValues,
			unsigned		setOfValuesLen
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendReadByGroupTypeResponse(
			const Oscl::BT::ATT::
			Server::TX::GroupInformation16bit	attrubteDataList[],
			unsigned							nAttributeData
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

		Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	opcode	= 0x11; 	// Read By Group Type Response
		static const uint8_t	length	=
				sizeof(attrubteDataList[0].attributeHandle)
			+	sizeof(attrubteDataList[0].groupEndHandle)
			+	sizeof(attrubteDataList[0].attributeValue)
			;

		encoder.encode(opcode);
		encoder.encode(length);

		for(
			unsigned	i = 0;
			i < nAttributeData;
			++i
			){

			encoder.encode(attrubteDataList[i].attributeHandle);
			encoder.encode(attrubteDataList[i].groupEndHandle);
			encoder.encode(attrubteDataList[i].attributeValue);

			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					__PRETTY_FUNCTION__
					);
				// We'll just pretend this didn't happen.
				break;
				}
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendWriteResponse() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	static const uint8_t	opcode	= Oscl::BT::ATT::Opcode::writeResponse;

	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocConstFixed(
				&opcode,
				sizeof(opcode)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	return _lowerLayerApi.send(
			attChannelID,
			fixed
			);
	}

bool	Part::sendPrepareWriteResponse(
			uint16_t		attributeHandle,
			uint16_t		valueOffset,
			const uint8_t	partAttributeValue[],
			unsigned		partAttributeValueLength
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendExecuteWriteResponse() noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendHandleValueNotification(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::sendHandleValueNotification(
			uint16_t		attributeHandle,
			Oscl::Pdu::Pdu*	attributeValue
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t
		opcode	= Oscl::BT::ATT::Opcode::handleValueNotification;

		encoder.encode(opcode);
		encoder.encode(attributeHandle);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		header	= _freeStore.allocFragmentWrapper(
					buffer,
					encoder.length()
					);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composites.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*attributeValue
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	composite->append(header);
	composite->append(fragment);

	return _lowerLayerApi.send(
			attChannelID,
			composite
			);
	}

bool	Part::sendHandleValueIndication(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return false;
	}

bool	Part::processAttErrorResponse(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		requestOpcode;
	uint16_t	attributeHandle;
	uint8_t		errorCode;

	decoder.decode(requestOpcode);
	decoder.decode(attributeHandle);
	decoder.decode(errorCode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tRequest Opcode: (0x%2.2X) %s\n"
		"\tAttribute Handle: (0x%4.4X)\n"
		"\tError Code: (0x%2.2X) %s\n"
		"",
		__PRETTY_FUNCTION__,
		requestOpcode,
		Oscl::Bluetooth::Strings::attOpcode(requestOpcode),
		attributeHandle,
		errorCode,
		Oscl::Bluetooth::Strings::attErrorCode(errorCode)
		);
	#endif

	return true;
	}

bool	Part::processAttExchangeMtuRequest(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	decoder.decode(_attMTU);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	/* PB-ADV 5.2.1

		PB-ADV is a provisioning bearer used to provision a
		device using Generic Provisioning PDUs (see Section 5.3)
		over the advertising channels. The provisioning mechanism
		is session based. An unprovisioned device shall support
		only one session at a time. There is no such limitation
		for a Provisioner. A session is established using the
		Link Establishment procedure (see Section 5.3.2).

		The PB-ADV bearer is used for transmitting Generic
		Provisioning PDUs. The PB-ADV bearer MTU
		(Maximum Transmission Unit) size is 24 octets.

	 */

	/* PB-GATT 5.2.2

		If the negotiated ATT_MTU is less than a required Proxy
		PDU size, the transmission of the Mesh Provisioning Data In
		and Mesh Provisioning Out characteristics always needs to
		be fragmented and reassembled. Each PDU shall be fully
		reassembled before processing.

		The Mesh Provisioning Server shall be able to receive a
		Proxy PDU in one or several ATT PDUs. The Mesh Provision
		Server shall use one or several Handle Value Notification
		ATT PDUs to send a Proxy PDU to the Provision Client
		depending on the size of the message and negotiated ATT_MTU.
	 */

	/* 5.3.3 Generic Provisioning behavior
		Each bearer has its own constraints on the maximum size of
		a Generic Provisioning PDU that can be transmitted by that
		bearer. Each Generic Provisioning PDU shall be the length
		of the full MTU for that bearer, except for the last segment
		of a transaction.
	 */

	/* Proxy PDU 6.3
		A Proxy Client and a Proxy Server exchange Proxy PDUs.
		Proxy PDUs can contain Network PDUs, mesh beacons,
		proxy configuration messages or Provisioning PDUs.
		A single Proxy PDU can contain a full message or a
		segment of a message. The size of the Proxy PDU is
		determined by the user of the Proxy protocol.
		For example, the GATT bearer defines the size of
		the Proxy PDU based on the ATT_MTU.
	 */

	/* ATT_MTU 7.1.2.2.2
		The Provisioning Server should support an ATT_MTU size equal
		to or larger than 69 octets to accommodate the longest known
		Provisioning message (see Section 5.3).
	 */

	/* ATT_MTU 7.2.2.2.4
		The server should support an ATT_MTU size equal to or larger
		than 33 octets to be able to pass the content of a full Proxy
		PDU (see Section 6.5).
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tClient RX MTU: (0x%4.4X) %u octets\n"
		"",
		_attMTU,
		_attMTU
		);
	#endif

	// The Bluez Linux code has a bug that truncates
	// the upper octet of the MTU.
	// See: mesh/gatt.c: pipe_write() max_len calculation.
	// The max_len variable is declared as a uint8_t
	// causing the truncation from the uint16_t write_mtu.
	// The write_mtu is reduced by 4 (write_mtu - 3 -1)
	// leaving the max_len value as 1 when write_mtu MTU is 517.
	// max_len = (517 - 3 - 1) = 513 = 0x0201 = 0x01 after truncation.
	// As a result, I'm going to send an MTU such that the
	// calculation will truncate to 0xFF (255). Therefore,
	// I will send (255 + 3 +1) = 259
	// NOTE:
	//		In the "latest" Bluez (2010-09-26),
	//		this error has been propagated to a new location:
	//		tools/mesh/gatt.c: sock_write()
	//		Otherwise, the error is the same.

//	uint16_t	bluezSpecialMTU = 259;

	// The Linux meshctl seems to have a problem
	// sending the Public Key... truncating it
	// to 20 octets :-/
	// This is attempt to force it to segment.
	// FIXME: This attempt *did* indeed force
	// segmentation and allowed the provisioning
	// to continue. :-/
	// The defect is in Bluez meshctl.
	// 
#if 1
//	uint16_t
//	bluezSpecialMTU = 18; // Linux <<<<<<<
//	bluezSpecialMTU = (2*32)+8; // Big enough for Public-Key PDU?
//	bluezSpecialMTU = Oscl::BT::ATT::Size::mtuMinimum;
#endif

	sendExchangeMtuResponse(_mtu);

	return true;
	}

bool	Part::processAttExchangeMtuResponse(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	serverRxMTU;

	decoder.decode(serverRxMTU);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tServer RX MTU: (0x%4.4X) %u octets\n"
		"",
		serverRxMTU,
		serverRxMTU
		);
	#endif
	return true;
	}

bool	Part::processAttFindInformationRequest(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"",
		startingHandle,
		endingHandle
		);
	#endif

	bool
	processed	= _upperLayerApi.findInformationRequest(
					startingHandle,
					endingHandle
					);

	if(processed){
		return true;
		}

	sendErrorResponse(
		Oscl::BT::ATT::Opcode::findInformationRequest,
		startingHandle,
		Oscl::BT::ATT::ErrorCode::attributeNotFound
		);

	return false;
	}

bool	Part::processAttHandlesAnd16bitUUIDList(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned
	remaining	= decoder.remaining();

	unsigned
	nItems		= remaining/(sizeof(uint16_t)+sizeof(uint16_t));

	for(unsigned i=0;i<nItems;++i){
		uint16_t	handle;
		uint16_t	uuid;
		decoder.decode(handle);
		decoder.decode(uuid);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		Oscl::Error::Info::log(
			"Handle: 0x%4.4X, UUID: 0x%4.4X\n"
			"",
			handle,
			uuid
			);
		}
	return true;
	}

bool	Part::processAttHandlesAnd128bitUUIDList(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned
	remaining	= decoder.remaining();

	unsigned
	nItems		= remaining/(sizeof(uint16_t)+16);

	for(unsigned i=0;i<nItems;++i){
		uint16_t	handle;
		uint8_t		uuid[16];

		decoder.decode(handle);
		decoder.copyOut(
			uuid,
			16
			);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		char	uuidString[128];

		Oscl::UUID::String::uuidToString(
			uuid,
			uuidString,
			sizeof(uuidString)
			);

		uuidString[127]	= '\0';

		Oscl::Error::Info::log(
			"Handle: 0x%4.4X, "
			"UUID: %s"
			"",
			handle,
			uuidString
			);
		}
	return true;
	}

bool	Part::processAttFindInformationResponse(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	format;

	decoder.decode(format);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tFormat: (0x%4.4X) %s\n"
		"",
		format,
		Oscl::Bluetooth::Strings::attFormat(format)
		);
	#endif

	if(format == 0x01){
		return processAttHandlesAnd16bitUUIDList(decoder);
		}

	if(format == 0x02){
		return processAttHandlesAnd128bitUUIDList(decoder);
		}
	return true;
	}

bool	Part::processAttFindByTypeValueRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttFindByTypeValueResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadByTypeRequest(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"",
		startingHandle,
		endingHandle
		);
	#endif

	unsigned
	remaining	= decoder.remaining();

	if(remaining < 2){
		Oscl::Error::Info::log(
			"%s: invalid PDU UUID too short!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	if(remaining > 2){
		if(remaining < 16){
			Oscl::Error::Info::log(
				"%s: invalid PDU 128-bit UUID too short!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	uint8_t	uuid128[16];

	if(remaining < 3){
		uint16_t	type;
		decoder.decode(type);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		Oscl::BT::UUID::Base::make128bitUUID(uuid128,type);
		}
	else {
		decoder.copyOut(
			uuid128,
			sizeof(uuid128)
			);
		}

	char	uuidString[128];

	bool
	failed	= Oscl::UUID::String::uuidToString(
            uuid128,
			uuidString,
            sizeof(uuidString)
            );

	if(failed){
		Oscl::Error::Info::log(
			"%s: uuidToString() failed!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tAttribute Type: (%s) %s\n"
		"",
		uuidString,
		Oscl::Bluetooth::Strings::gattAttributeType(uuid128)
		);
	#endif

	bool
	processed	= _upperLayerApi.readByTypeRequest(
					startingHandle,
					endingHandle,
					uuid128
					);

	if(processed){
		return true;
		}

	sendErrorResponse(
		Oscl::BT::ATT::Opcode::readByTypeRequest,
		startingHandle,
		Oscl::BT::ATT::ErrorCode::attributeNotFound
		);

	return false;
	}

bool	Part::processAttReadByTypeResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadBlobRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadBlobResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadMultipleRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadMultipleResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttReadByGroupTypeRequest(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"",
		startingHandle,
		endingHandle
		);
	#endif

	unsigned
	remaining	= decoder.remaining();

	if(remaining < 2){
		Oscl::Error::Info::log(
			"%s: invalid PDU UUID too short!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	if(remaining > 2){
		if(remaining < 16){
			Oscl::Error::Info::log(
				"%s: invalid PDU 128-bit UUID too short!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	uint8_t	uuid128[16];

	if(remaining < 3){
		uint16_t	type;
		decoder.decode(type);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		Oscl::BT::UUID::Base::make128bitUUID(uuid128,type);
		}
	else {
		decoder.copyOut(
			uuid128,
			sizeof(uuid128)
			);
		}

	char	uuidString[128];

	bool
	failed	= Oscl::UUID::String::uuidToString(
            uuid128,
			uuidString,
            sizeof(uuidString)
            );

	if(failed){
		Oscl::Error::Info::log(
			"%s: uuidToString() failed!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tAttribute Group Type: (%s) %s\n"
		"",
		uuidString,
		Oscl::Bluetooth::Strings::gattAttributeType(uuid128)
		);
	#endif

	bool
	processed	= _upperLayerApi.readByGroupTypeRequest(
					startingHandle,
					endingHandle,
					uuid128
					);

	if(processed){
		return true;
		}

	sendErrorResponse(
		Oscl::BT::ATT::Opcode::readByGroupTypeRequest,
		startingHandle,
		Oscl::BT::ATT::ErrorCode::attributeNotFound
		);

	return false;
	}

bool	Part::processAttReadByGroupTypeResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttWriteRequest(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	handle;
	uint8_t		value[512];


	decoder.decode(handle);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tHandle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		__PRETTY_FUNCTION__,
		handle,
		remaining
		);
	#endif

	bool
	processed	= _upperLayerApi.writeRequest(
					handle,
					value,
					remaining
					);

	if(processed){
		return true;
		}

	sendErrorResponse(
		Oscl::BT::ATT::Opcode::writeRequest,
		handle,
		Oscl::BT::ATT::ErrorCode::attributeNotFound
		);

	return false;
	}

bool	Part::processAttWriteResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttPrepareWriteRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttPrepareWriteResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttExecuteWriteRequest(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttExecuteWriteResponse(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttHandleValueNotification(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttHandleValueIndication(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	handle;
	uint8_t		value[512];


	decoder.decode(handle);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Error::Info::log(
		"%s:  NOT HANDLED!\n"
		"\tHandle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"\tvalue:\n"
		"",
		__PRETTY_FUNCTION__,
		handle,
		remaining
		);

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);

	/*	3.4.7.2 Handle Value Indication

		The client shall send a Handle Value Confirmation in
		response to a Handle Value Indication. No further
		indications to this client shall occur until the
		confirmation has been received by the server.
	 */

	return true;
	}

bool	Part::processAttHandleValueConfirmation(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

bool	Part::processAttWriteCommand(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	handle;
	uint8_t		value[512];


	decoder.decode(handle);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tHandle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		__PRETTY_FUNCTION__,
		handle,
		remaining
		);
	#endif

	bool
	processed	= _upperLayerApi.writeCommand(
					handle,
					value,
					remaining
					);

	if(processed){
		return true;
		}

	sendErrorResponse(
		Oscl::BT::ATT::Opcode::writeCommand,
		handle,
		Oscl::BT::ATT::ErrorCode::attributeNotFound
		);

	return false;
	}

bool	Part::processAttSignedWriteCommand(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);

	return true;
	}

uint16_t	Part::getChannelID() const noexcept{
	return attChannelID;	// ATT
	}

void	Part::receive(
			const void* packet,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		packet,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		packet,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint8_t	opcode;
	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return;
		};

	switch(opcode){
		case Oscl::BT::ATT::Opcode::errorResponse:
			processAttErrorResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::exchangeMtuRequest:
			processAttExchangeMtuRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::exchangeMtuResponse:
			processAttExchangeMtuResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::findInformationRequest:
			processAttFindInformationRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::findInformationResponse:
			processAttFindInformationResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::findByTypeValueRequest:
			processAttFindByTypeValueRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::findByTypeValueResponse:
			processAttFindByTypeValueResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readByTypeRequest:
			processAttReadByTypeRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readByTypeResponse:
			processAttReadByTypeResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readRequest:
			processAttReadRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readResponse:
			processAttReadResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readBlobRequest:
			processAttReadBlobRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readBlobResponse:
			processAttReadBlobResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readMultipleRequest:
			processAttReadMultipleRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readMultipleResponse:
			processAttReadMultipleResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readByGroupTypeRequest:
			processAttReadByGroupTypeRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::readByGroupTypeResponse:
			processAttReadByGroupTypeResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::writeRequest:
			processAttWriteRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::writeResponse:
			processAttWriteResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::prepareWriteRequest:
			processAttPrepareWriteRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::prepareWriteResponse:
			processAttPrepareWriteResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::executeWriteRequest:
			processAttExecuteWriteRequest(decoder);
			return;
		case Oscl::BT::ATT::Opcode::executeWriteResponse:
			processAttExecuteWriteResponse(decoder);
			return;
		case Oscl::BT::ATT::Opcode::handleValueNotification:
			processAttHandleValueNotification(decoder);
			return;
		case Oscl::BT::ATT::Opcode::handleValueIndication:
			processAttHandleValueIndication(decoder);
			return;
		case Oscl::BT::ATT::Opcode::handleValueConfirmation:
			processAttHandleValueConfirmation(decoder);
			return;
		case Oscl::BT::ATT::Opcode::writeCommand:
			processAttWriteCommand(decoder);
			return;
		case Oscl::BT::ATT::Opcode::signedWriteCommand:
			processAttSignedWriteCommand(decoder);
			return;
		default:
			break;
		}

	Oscl::Error::Info::log(
		"%s: Unknown ATT opcode: 0x%2.2X\n",
		__PRETTY_FUNCTION__,
		opcode
		);
	}

