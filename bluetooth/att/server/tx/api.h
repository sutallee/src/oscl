/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_server_tx_apih_
#define _oscl_bluetooth_att_server_tx_apih_

#include <stdint.h>
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace Server {
/** */
namespace TX {

/** */
struct InformationData16bit {
	uint16_t	handle;
	uint16_t	uuid;
	};

/** */
struct InformationData128bit {
	uint16_t	handle;
	uint8_t		uuid[16];
	};

/** */
struct HandleInformation {
	uint16_t	attributeHandle;
	uint16_t	groupEndHandle;
	};

/** */
struct GroupInformation16bit {
	uint16_t		attributeHandle;
	uint16_t		groupEndHandle;
	uint16_t		attributeValue;
	};

/** */
struct AttributeData {
	const void*		data;
	};

/** This interface is used by an ATT Server client
	(GATT) to send ATT messages through the ATT layer.
 */
class Api {
	public:
		/**
			uint8_t	opcode	= 0x01;
			RETURN: true on resource failure.
		 */
		virtual bool	sendErrorResponse(
							uint8_t		requestOpcodeInError,
							uint16_t	attributeHandleInError,
							uint8_t		errorCode
							) noexcept=0;

		/**
			uint8_t	opcode	= 0x02;
			RETURN: true on resource failure.
		 */
		virtual bool	sendExchangeMtuResponse(
							uint16_t	serverRxMtu
							) noexcept=0;

		/**
			uint8_t opcode = 0x05,
			uint8_t format = 0x01
			RETURN: true on resource failure.
		 */
		virtual bool	sendFindInformationResponse16bitUUIDs(
							unsigned					nInformationDataRecords,
							const InformationData16bit	informationData[]
							) noexcept=0;

		/**
			uint8_t opcode = 0x05,
			uint8_t format = 0x02
			RETURN: true on resource failure.
		 */
		virtual bool	sendFindInformationResponse128BitUUIDs(
							unsigned					nInformationDataRecords,
							const InformationData128bit	informationData[]
							) noexcept=0;

		/**
			uint8_t opcode = 0x07,
			RETURN: true on resource failure.
		 */
		virtual	bool	sendFindByTypeValueResponse(
							uint8_t					nHandles,
							const HandleInformation	handleInformation[]
							) noexcept=0;

		/**
			uint8_t opcode = 0x09,
			length: Size of each attribute/handle-value pair <handle>+<data>
			RETURN: true on resource failure.
		 */
		virtual	bool	sendReadByTypeResponse(
							uint8_t				length,
							const AttributeData	attributeData[],
							unsigned			nAttributeData
							) noexcept=0;

		/**
			uint8_t opcode = 0x0B,
			RETURN: true on resource failure.
		 */
		virtual bool	sendReadResponse(
							const uint8_t*	value,
							unsigned		valueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x0D,
			RETURN: true on resource failure.
		 */
		virtual bool	sendReadBlobResponse(
							const uint8_t*	partValue,
							unsigned		partValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x0F,
			RETURN: true on resource failure.
		 */
		virtual bool	sendReadMultipleResponse(
							const uint8_t*	setOfValues,
							unsigned		setOfValuesLen
							) noexcept=0;

		/**
			uint8_t opcode = 0x11,
			RETURN: true on resource failure.
		 */
		virtual bool	sendReadByGroupTypeResponse(
							const GroupInformation16bit	attributeDataList[],
							unsigned					nAttributeData
							) noexcept=0;

		/**
			uint8_t opcode = 0x13,
			RETURN: true on resource failure.
		 */
		virtual bool	sendWriteResponse() noexcept=0;

		/**
			uint8_t opcode = 0x17,
			RETURN: true on resource failure.
		 */
		virtual bool	sendPrepareWriteResponse(
							uint16_t		attributeHandle,
							uint16_t		valueOffset,
							const uint8_t	partAttributeValue[],
							unsigned		partAttributeValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x19,
			RETURN: true on resource failure.
		 */
		virtual bool	sendExecuteWriteResponse() noexcept=0;

		/**
			uint8_t opcode = 0x1B,
			RETURN: true on resource failure.
		 */
		virtual bool	sendHandleValueNotification(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x1B,
			RETURN: true on resource failure.
		 */
		virtual bool	sendHandleValueNotification(
							uint16_t		attributeHandle,
							Oscl::Pdu::Pdu*	attributeValue
							) noexcept=0;

		/**
			uint8_t opcode = 0x1D,
			RETURN: true on resource failure.
		 */
		virtual bool	sendHandleValueIndication(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept=0;
	};
}
}
}
}
}

#endif
