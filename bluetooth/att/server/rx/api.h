/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_server_rx_apih_
#define _oscl_bluetooth_att_server_rx_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace Server {
/** */
namespace RX {

/** This interface is used by an ATT Server to forward
	ATT messages up to a client (GATT) layer.
 */
class Api {
	public:
		/**
			uint8_t opcode = 0x02
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	exchangeMtuRequest(uint16_t clientRxMTU) noexcept=0;

		/**
			uint8_t opcode = 0x04
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	findInformationRequest(
							uint16_t	startingHandle,
							uint16_t	endingHandle
							) noexcept=0;

		/**
			uint8_t opcode = 0x06
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	findByTypeValueRequest(
							uint16_t		startingHandle,
							uint16_t		endingHandle,
							uint16_t		attributeType,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x08
			For 128-bit attributeTypeUUID
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readByTypeRequest(
							uint16_t	startingHandle,
							uint16_t	endingHandle,
							uint8_t		attributeTypeUUID[16]
							) noexcept=0;

		/**
			uint8_t opcode = 0x0A
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readRequest(
							uint16_t	attributeHandle
							) noexcept=0;

		/**
			uint8_t opcode = 0x0C
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readBlobRequest(
							uint16_t	attributeHandle,
							uint16_t	valueOffset
							) noexcept=0;

		/**
			uint8_t opcode = 0x0E
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readMultipleRequest(
							const uint16_t	setOfAttributeHandles[],
							unsigned		nHandles
							) noexcept=0;

		/**
			uint8_t opcode = 0x10
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readByGroupTypeRequest(
							uint16_t	startingHandle,
							uint16_t	endingHandle,
							uint8_t		attributeGroupType[16]
							) noexcept=0;

		/**
			uint8_t opcode = 0x12
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	writeRequest(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0x52
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	writeCommand(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept=0;

		/**
			uint8_t opcode = 0xD2
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	signedWriteCommand(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength,
							const uint8_t	signature[12]
							) noexcept=0;

		/**
			uint8_t opcode = 0x16
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	prepareWriteRequest(
							uint16_t		attributeHandle,
							uint16_t		valueOffset,
							const uint8_t	partAttributeValue[],
							unsigned		partAttributeValueLength
						) noexcept=0;
	
		/**
			uint8_t opcode = 0x18
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	executeWriteRequestCancel() noexcept=0;

		/**
			uint8_t opcode = 0x18
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	executeWriteRequest() noexcept=0;

		/**
			uint8_t opcode = 0x1E
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	handleValueConfirmation() noexcept=0;
	};
}
}
}
}
}

#endif
