/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_att_errorh_
#define _oscl_bluetooth_att_errorh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace ATT {
/** */
namespace ErrorCode {

/** */
constexpr uint8_t	invalidHandle				= 0x01;
/** */
constexpr uint8_t	readNotPermitted			= 0x02;
/** */
constexpr uint8_t	writeNotPermitted			= 0x03;
/** */
constexpr uint8_t	invalidPDU					= 0x04;
/** */
constexpr uint8_t	insufficientAuthentication	= 0x05;
/** */
constexpr uint8_t	requestNotSupported			= 0x06;
/** */
constexpr uint8_t	invalidOffset				= 0x07;
/** */
constexpr uint8_t	insufficientAuthorization	= 0x08;
/** */
constexpr uint8_t	prepareQueueFull			= 0x09;
/** */
constexpr uint8_t	attributeNotFound			= 0x0A;
/** */
constexpr uint8_t	attributeNotLong			= 0x0B;
/** */
constexpr uint8_t	insufficientEncryptionKeySize	= 0x0C;
/** */
constexpr uint8_t	invalidAttributeValueLength		= 0x0D;
/** */
constexpr uint8_t	unlikelyError					= 0x0E;
/** */
constexpr uint8_t	insuffficientEncryption			= 0x0F;
/** */
constexpr uint8_t	unsupportedGroupType			= 0x10;
/** */
constexpr uint8_t	insufficientResources			= 0x11;

}
}
}
}

#endif
