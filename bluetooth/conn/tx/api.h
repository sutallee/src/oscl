/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_conn_tx_apih_
#define _oscl_bluetooth_conn_tx_apih_

#include <stdint.h>
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Conn {
/** */
namespace TX {

/** This interface is used by a Bluetooth Host
	to to send messages for a specific connection
	to a Bluetooth Controller interface (e.g. ACL Data).
 */
class Api {
	public:
		/**
			pbFlag : Packet boundary flag
				b00	= First non-automatically-flushable packet
				b01	= Continuing ragment
				b10	= First automatically flushable packet
				b11	= A complete L2CAP PDU.
			bcFlag : Broadcast flag
				b00	= Point-to-point
				b01	= Active Slave Broadcast

			RETURN: true if failed resource allocation.
		 */
		virtual bool	send(
							uint8_t			pbFlag,
							uint8_t			bcFlag,
							uint16_t		connHandle,
							uint16_t		dataTotalLength,
							Oscl::Pdu::Pdu*	pdu
							) noexcept=0;

		/** RETURN: the maximum 
		 */
		virtual unsigned	maxPduLen() noexcept=0;
	};
}
}
}
}

#endif
