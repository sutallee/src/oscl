/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/strings/module.h"

using namespace Oscl::BT::Conn::Layer;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Conn::TX::Api&	lowerLayerApi,
	Oscl::BT::L2CAP::RX::Api&	upperLayerApi,
	Oscl::Pdu::Memory::Api&		freeStore,
	uint16_t					connectionHandle
	) noexcept:
		_lowerLayerApi(lowerLayerApi),
		_upperLayerApi(upperLayerApi),
		_freeStore(freeStore),
		_connectionHandle(connectionHandle),
		_reasmEncoder(
			_reasmBuffer,
			sizeof(_reasmBuffer)
			),
		_receiver(
			*this,
			&Part::getConnectionHandle,
			&Part::receive
			)
		{
	}

Oscl::BT::Conn::RX::Item&	Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::send(Oscl::Pdu::Pdu* pdu) noexcept{
	/*	The PDU is an L2CAP packet formatted as
		as described in 

		This function encodes the pdu to the
		HCI ACL format described in the Bluetooth
		Specification 5.4.2 Figure 5.2.
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: pdu->length(): %u\n",
		OSCL_PRETTY_FUNCTION,
		pdu->length()
		);
	#endif

	const uint16_t	dataTotalLength	= pdu->length();

	unsigned
	maxPduLen	= _lowerLayerApi.maxPduLen();

	static const uint8_t	bcFlag	= 0x00; // Host to Controller No broadcast

	/*
		BLUETOOTH SPECIFICATION Version 5.0 | Vol 2, Part E page 733

		5.4.2 HCI ACL Data Packets

		Hosts and Controllers shall be able to accept
		HCI ACL Data Packets with up to 27 bytes of
		data excluding the HCI ACL Data Packet header on
		Connection_Handles associated with an LE-U logical
		link. The HCI ACL Data Packet header is the first 4
        octets of the packet.
	*/
	if(dataTotalLength <= maxPduLen){
		static const uint8_t	pbFlag	= 0x00;	// First non-automatically flushable packet
		// No need to fragment this PDU.
		return _lowerLayerApi.send(
				pbFlag,
				bcFlag,
				_connectionHandle,
				dataTotalLength,
				pdu
				);
		}

	Oscl::HQueue<Oscl::Pdu::Pdu>	pduList;

	// Here, we need to fragment the PDU

	for(
		unsigned
		remaining	= dataTotalLength;	// 28
		remaining;
		remaining	-= maxPduLen	// 27
		){

		Oscl::Handle<Oscl::Pdu::Composite>	fragment;

		fragment	= _freeStore.allocCompositeWrapper(*pdu);

		if(!fragment){

			Oscl::Error::Info::log(
				"%s: out of composite memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);

			Oscl::Handle<Oscl::Pdu::Pdu> p;

			while((p = pduList.get())) {}

			return true;
			}

		unsigned
		headerLength = dataTotalLength - remaining;

		fragment->stripHeader(headerLength);

		unsigned
		trailerLength	= (remaining > maxPduLen)?
							remaining - maxPduLen:
							0
							;

		fragment->stripTrailer(trailerLength);

		Oscl::Pdu::Pdu&	packet	= *fragment;

		pduList.put(&packet);

		if(!trailerLength){
			break;
			}
		}

	Oscl::Handle<Oscl::Pdu::Pdu> p;

	bool	failed	= false;

	// Now that we've created all of the
	// fragments, we'll send them down
	// the chain.

	for(
		bool first = true;
		(p = pduList.get());
		first	= false
		){
		static const uint8_t	pbFlagFirst			= 0x00;	// First non-automatically flushable packet
		static const uint8_t	pbFlagContinuing	= 0x01;	// Continuing fragment

		if(failed){
			// This effectively flushes the pduList.
			continue;
			}

		failed	= _lowerLayerApi.send(
					first?pbFlagFirst:pbFlagContinuing,
					bcFlag,
					_connectionHandle,
					p->length(),	// dataTotalLength,
					p
					);
		}

	return failed;
	}

uint16_t	Part::getConnectionHandle() const noexcept{
	return _connectionHandle;
	}

void	Part::receive(
			uint8_t		pbFlag,
			uint8_t		bcFlag,
			const void* packet,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: pbFlag: 0x%2.2X, bcFlag: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		pbFlag,
		bcFlag
		);
	Oscl::Error::Info::hexDump(
		packet,
		length
		);
	#endif

	// Do recombination as required.

	// Check for first fragment.

	if(pbFlag == 0x02){
		// First automatically flushable packet

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: First fragment.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		_reasmEncoder.reset();

		_reasmEncoder.copyIn(
			packet,
			length
			);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::hexDump(
			_reasmBuffer,
			_reasmEncoder.length()
			);
		#endif
		}
	else if(pbFlag == 0x01){
		// Continuing fragment
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Continuing fragment.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		_reasmEncoder.copyIn(
			packet,
			length
			);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::hexDump(
			_reasmBuffer,
			_reasmEncoder.length()
			);
		#endif

		return processL2CAP(
					_reasmBuffer,
					_reasmEncoder.length()
					);
		}

	return processL2CAP(
				_reasmBuffer,
				_reasmEncoder.length()
				);
	}

void	Part::processL2CAP(
			const uint8_t*	packet,
			unsigned		length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		_reasmBuffer,
		_reasmEncoder.length()
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	// Decode L2CAP header
	uint16_t	len;
	uint16_t	channelID;

	decoder.decode(len);
	decoder.decode(channelID);

	if(decoder.underflow()){
		// This may be normal if a 
		// short first fragment
		// comes through.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: normal underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	unsigned	remaining	= decoder.remaining();

	if(len > remaining){
		// This is an incomplete first
		// fragment. The layer below will
		// resend the PDU to us if/when a
		// Continuing fragment arrives.
		// This check avoids having the
		// higher layers process 
		// incomplete fragments.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (len(%u) > remaining(%u)) wait for next fragment.\n",
			OSCL_PRETTY_FUNCTION,
			len,
			remaining
			);
		#endif
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tLength: (0x%4.4X) %u\n"
		"\tChannel ID: (0x%4.4X) %s\n",
		OSCL_PRETTY_FUNCTION,
		len,
		len,
		channelID,
		Oscl::Bluetooth::Strings::l2capChannelID(channelID)
		);
	#endif

#if 0

	_upperLayerApi.receive(
		&packet[offset],
		len
		);
#else
	_upperLayerApi.receive(
		packet,
		length
		);
#endif
	}

