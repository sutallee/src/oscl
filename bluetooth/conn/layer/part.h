/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_conn_layer_parth_
#define _oscl_bluetooth_conn_layer_parth_

#include "oscl/bluetooth/l2cap/tx/api.h"
#include "oscl/bluetooth/l2cap/rx/api.h"
#include "oscl/bluetooth/l2cap/tx/api.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/conn/rx/itemcomp.h"
#include "oscl/bluetooth/conn/tx/api.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/api.h"
#include "oscl/bluetooth/l2cap/channel/rx/item.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Conn {
/** */
namespace Layer {

/** This class is responsible for a single connection.
	Its primary responsiblity is the recombination/reassembly
	of received L2CAP packets, and the fragmentation of
	transmitted L2CAP packets.
 */
class Part :
	public Oscl::BT::L2CAP::TX::Api
	{
	private:
		/** */
		Oscl::BT::Conn::TX::Api&			_lowerLayerApi;
	
		/** */
		Oscl::BT::L2CAP::RX::Api&			_upperLayerApi;

		/** */
		Oscl::Pdu::Memory::Api&				_freeStore;

		/** */
		uint16_t							_connectionHandle;

	private:
		/** */
		uint8_t								_reasmBuffer[512];

		/** */
		Oscl::Encoder::LE::Base				_reasmEncoder;

	private:
		/** */
		Oscl::BT::Conn::RX::ItemComposer<Part>	_receiver;

	public:
		/** */
		Part(
			Oscl::BT::Conn::TX::Api&			lowerLayerApi,
			Oscl::BT::L2CAP::RX::Api&			upperLayerApi,
			Oscl::Pdu::Memory::Api&				freeStore,
			uint16_t							connectionHandle
			) noexcept;

		/** */
		Oscl::BT::Conn::RX::Item&	getRxItem() noexcept;

	public: // Oscl::BT::L2CAP::TX::Api
		/** The PDU argument is a fully formed L2CAP PDU
			that includes a <length> field and a <channel ID>
			followed by an information payload as described
			in Section 3.1 CONNECTION-ORIENTED CHANNELS IN BASIC L2CAP MODE
			of the Bluetooth Specification.

			The implementation will use the underlying mechanism
			(such as HCI ACL) to send this packet.

			RETURN: true if resource allocation fails.
		 */
		bool	send(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::BT::Conn::RX::Api
		/** */
		uint16_t	getConnectionHandle() const noexcept;

		/**
			pbFlag : Packet boundary flag
				b00	= First non-automatically-flushable packet
				b01	= Continuing fragment
				b10	= First automatically flushable packet
				b11	= A complete L2CAP PDU.
			bcFlag : Broadcast flag
				b00	= Point-to-point
				b01	= Active Slave Broadcast
		 */
		void	receive(
					uint8_t		pbFlag,
					uint8_t		bcFlag,
					const void* packet,
					unsigned	length
					) noexcept;

	private:
		/** */
		void	processL2CAP(
					const uint8_t*	packet,
					unsigned		length
					) noexcept;
	};

}
}
}
}

#endif
