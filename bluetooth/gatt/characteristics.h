/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_gatt_characteristicsh_
#define _oscl_bluetooth_gatt_characteristicsh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace GATT {
/** */
namespace Characteristic {

/*	See: https://www.bluetooth.com/specifications/gatt/characteristics/
	Characteristics are defined attribute types that contain a single logical value.
 */


/*	For some unguessable reason, the following Mesh numbers are not
	contained in https://www.bluetooth.com/specifications/gatt/characteristics .
	*Possibly* because they dont contain a "single logical value"?
 */

/** Mesh Proxy Data In */
constexpr uint16_t	meshProxyDataIn			= 0x2ADD;

/** Mesh Proxy Data Out */
constexpr uint16_t	meshProxyDataOut		= 0x2ADE;

/** Mesh Provisioning Data In */
constexpr uint16_t	meshProvisioningDataIn	= 0x2ADB;

/** Mesh Provisioning Data Out */
constexpr uint16_t	meshProvisioningDataOut	= 0x2ADC;


}
}
}
}

#endif
