/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"
#include <string.h>
#include <new>
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/att/error.h"
#include "oscl/decoder/linear/le/base.h"
#include "oscl/decoder/linear/be/base.h"
#include "oscl/aes128/cmac.h"
#include "oscl/bluetooth/att/size.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/proxy/opcodes.h"
#include "oscl/bluetooth/mesh/proxy/sartx.h"
#include "oscl/bluetooth/gatt/services.h"
#include "oscl/bluetooth/gatt/declarations.h"
#include "oscl/bluetooth/gatt/descriptors.h"
#include "oscl/bluetooth/gatt/characteristics.h"

using namespace Oscl::BT::GATT::Server;

//#define DEBUG_TRACE

Base::Base() noexcept
		{
	}

const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
		Base::findServiceHandleRange(
			uint16_t	first,
			uint16_t	last,
			unsigned&	nIndicies
			) const noexcept{

	const Oscl::BT::ATT::Server::TX::GroupInformation16bit*	result = 0;

	unsigned	nServices;

	const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
	services	= getServices(nServices);

	for(unsigned i=0;i<nServices;++i){
		if(services[i].attributeHandle >= first){
			if(services[i].attributeHandle <= last){
				if(!result){
					nIndicies	= 1;
					result	= &services[i];
					}
				else {
					++nIndicies;
					}
				}
			}
		}

	return result;
	}

const CharacteristicDeclaration*
		Base::findCharacteristicHandleRange(
			uint16_t	first,
			uint16_t	last,
			unsigned&	nIndicies
			) const noexcept{
	unsigned	nCharacteristicDeclarations;

	const CharacteristicDeclaration*	characteristicDeclarations	= getCharacteristicDeclarations(nCharacteristicDeclarations);

	const CharacteristicDeclaration*	result = 0;

	for(unsigned i=0;i<nCharacteristicDeclarations;++i){
		if(characteristicDeclarations[i].handle >= first){
			if(characteristicDeclarations[i].handle <= last){
				if(!result){
					nIndicies	= 1;
					result	= &characteristicDeclarations[i];
					}
				else {
					++nIndicies;
					}
				}
			}
		}

	return result;
	}

/*
	This function returns a pointer to the
	first element of the handleUUID16[] array
	with a handle that is greater than or
	equal to the specified first handle or
	zero if there is no such element.

	If at least one element is found,
	nIndicies is set to the number of
	matching elemenet in the ordered array.
 */
const Oscl::BT::ATT::Server::TX::InformationData16bit*
		Base::findHandleRange(
			uint16_t	first,
			uint16_t	last,
			unsigned&	nIndicies
			) const noexcept{

	unsigned	nAttributes;

	const Oscl::BT::ATT::Server::TX::InformationData16bit*
	attributes	= getAttributes(nAttributes);

	const Oscl::BT::ATT::Server::TX::InformationData16bit*	result = 0;

	for(unsigned i=0;i<nAttributes;++i){
		if(attributes[i].handle >= first){
			if(attributes[i].handle <= last){
				if(!result){
					nIndicies	= 1;
					result	= &attributes[i];
					}
				else {
					++nIndicies;
					}
				}
			}
		}

	return result;
	}

/*
	4.4.1 Discover All Base Services
		This sub-procedure is used by a client to discover
		all the primary services on a server.
		The Attribute Protocol Read By Group Type Request
		shall be used with the Attribute Type parameter set
		to the UUID for «Base Service». The Starting Handle shall
		be set to 0x0001 and the Ending Handle shall be set to 0xFFFF.

		Two possible responses can be sent from the server for
		the Read By Group Type Request:
			Read By Group Type Response and
			Error Response.

		Error Response is returned if an error occurred on the server.

		Read By Group Type Response returns a list of Attribute Handle, End Group
		Handle, and Attribute Value tuples corresponding to the services supported by
		the server. Each Attribute Value contained in the response is the Service UUID
		of a service supported by the server. The Attribute Handle is the handle for the
		service declaration. The End Group Handle is the handle of the last attribute
		within the service definition. The End Group Handle of the last service in a
		device can be 0xFFFF. The Read By Group Type Request shall be called again
		with the Starting Handle set to one greater than the last End Group Handle in
		the Read By Group Type Response.

		This sub-procedure is complete when the Error Response is received and the
		Error Code is set to «Attribute Not Found» or when the End Group Handle in
		the Read by Type Group Response is 0xFFFF.

		It is permitted to end the sub-procedure early if a desired primary service is
		found prior to discovering all the primary services on the server.
		Note: The service declaration described in Section 3.1 specifies that the
		service declaration is readable and requires no authentication or authorization,
		therefore insufficient authentication or read not permitted errors shall not occur.
*/

void	Base::sendReadByGroupTypeResponse(
			uint16_t	startingHandle,
			uint16_t	endingHandle
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: startingHandle: 0x%4.4X, endingHandle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		startingHandle,
		endingHandle
		);
	#endif

	unsigned	nIndicies;

	const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
	descriptor	= findServiceHandleRange(
					startingHandle,
					endingHandle,
					nIndicies
					);

	if(!descriptor){
		getAttSendApi().sendErrorResponse(
			Oscl::BT::ATT::Opcode::readByGroupTypeRequest,
			startingHandle,
			Oscl::BT::ATT::ErrorCode::attributeNotFound
			);
		return;
		}

	getAttSendApi().sendReadByGroupTypeResponse(
            descriptor,
            nIndicies
            );
	}

void	Base::sendReadByTypeCharacteristicDeclarationResponse(
			uint16_t	startingHandle,
			uint16_t	endingHandle
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	unsigned	nCharacteristicDeclarations;
	getCharacteristicDeclarations(nCharacteristicDeclarations);

	/* The following arrays take advantage of the
		GCC "C Extensions" Variable Length arrays.
		WARNING: This causes dynamic stack expansion.
	 */
	Oscl::BT::ATT::Server::TX::AttributeData	attData[nCharacteristicDeclarations];
	CharacteristicDeclaration					buffers[nCharacteristicDeclarations];

	const uint8_t	length	= 2+1+2+2;	// Size of tuple <handle><properties><handle><uuid>

	unsigned		nIndicies;

	const CharacteristicDeclaration*
	rec	= findCharacteristicHandleRange(
			startingHandle,
			endingHandle,
			nIndicies
			);

	if(!rec){
		getAttSendApi().sendErrorResponse(
			Oscl::BT::ATT::Opcode::readByTypeRequest,
			startingHandle,
			Oscl::BT::ATT::ErrorCode::attributeNotFound
			);
		return;
		}

	for(unsigned i=0;i<nIndicies;++i){

		attData[i].data	= &buffers[i];

		Oscl::Encoder::LE::Base
		leEncoder(
			&buffers[i],
			sizeof(CharacteristicDeclaration)
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		encoder.encode(rec->handle);
		encoder.encode(rec->properties);
		encoder.encode(rec->attributeHandle);
		encoder.encode(rec->uuid16);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				__PRETTY_FUNCTION__
				);
			// We'll just pretend this didn't happen.
			break;
			}
		}

	getAttSendApi().sendReadByTypeResponse(
		length,
		attData,
		nIndicies
		);
	}

bool	Base::exchangeMtuRequest(uint16_t clientRxMTU) noexcept{
	return false;
	}

bool	Base::findInformationRequest(
			uint16_t	startingHandle,
			uint16_t	endingHandle
			) noexcept{

	unsigned	nIndicies;

	const Oscl::BT::ATT::Server::TX::InformationData16bit*
	rec	= findHandleRange(
			startingHandle,
			endingHandle,
			nIndicies
			);

	if(!rec){
		getAttSendApi().sendErrorResponse(
			Oscl::BT::ATT::Opcode::findInformationRequest,
			startingHandle,
			Oscl::BT::ATT::ErrorCode::attributeNotFound
			);
		return true;
		}

	getAttSendApi().sendFindInformationResponse16bitUUIDs(
			nIndicies,
			rec
			);

	return true;
	}

bool	Base::findByTypeValueRequest(
			uint16_t		startingHandle,
			uint16_t		endingHandle,
			uint16_t		attributeType,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{
	return false;
	}

bool	Base::readByTypeRequest(
			uint16_t	startingHandle,
			uint16_t	endingHandle,
			uint8_t		attributeTypeUUID[16]
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: startingHandle: 0x%4.4X, endingHandle: 0x%4.4X: attributeTypeUUID:\n",
		__PRETTY_FUNCTION__,
		startingHandle,
		endingHandle
		);
	Oscl::Error::Info::hexDump(
		attributeTypeUUID,
		16
		);
	#endif

	// Characteristic Declaration
	static uint8_t	characteristicServiceDeclarationUUID[16]	= {
		0x00, 0x00, 0x28, 0x03,
		0x00,0x00,
		0x10,0x00,
		0x80,0x00,
		0x00,0x80,0x5F,0x9B,0x34,0xFB
		};

	int
	different	= memcmp(
					attributeTypeUUID,
					characteristicServiceDeclarationUUID,
					sizeof(characteristicServiceDeclarationUUID)
					);

	if(!different){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: it's my characteristicServiceDeclarationUUID!\n",
			__PRETTY_FUNCTION__
			);
		#endif

		sendReadByTypeCharacteristicDeclarationResponse(
			startingHandle,
			endingHandle
			);
		return true;
		}

	return false;
	}

bool	Base::readRequest(
			uint16_t	attributeHandle
			) noexcept{
	return false;
	}

bool	Base::readBlobRequest(
			uint16_t	attributeHandle,
			uint16_t	valueOffset
			) noexcept{
	return false;
	}

bool	Base::readMultipleRequest(
			const uint16_t	setOfAttributeHandles[],
			unsigned		nHandles
			) noexcept{
	return false;
	}

bool	Base::readByGroupTypeRequest(
			uint16_t	startingHandle,
			uint16_t	endingHandle,
			uint8_t		attributeGroupType[16]
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	static uint8_t	primaryServiceUUID[16]	= {
		0x00, 0x00, 0x28, 0x00,
		0x00,0x00,
		0x10,0x00,
		0x80,0x00,
		0x00,0x80,0x5F,0x9B,0x34,0xFB
		};

	int
	different	= memcmp(
					attributeGroupType,
					primaryServiceUUID,
					sizeof(primaryServiceUUID)
					);

	if(different){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: it's MINE!\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendReadByGroupTypeResponse(
		startingHandle,
		endingHandle
		);

	return true;
	}

bool	Base::writeRequest(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	return false;
	}

bool	Base::writeCommand(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{
	return false;
	}

#if 0
bool	Base::writeCommand(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		"\tHandle: 0x%4.4X\n"
		"\tValue:\n",
		__PRETTY_FUNCTION__,
		attributeHandle
		);

	Oscl::Error::Info::hexDump(
		attributeValue,
		attributeValueLength
		);
	#endif

	if(!hasAttribute(attributeHandle)){
		return false;
		}

	// At this point, we have a command
	// destined for Not Mesh Provisioning Data In
	// The value is the command and it consists
	// of possibly segmented PDU that needs to
	// be reassembled.

	uint8_t	opcode	= attributeValue[0];
	uint8_t	sar		= opcode >> 6;
	opcode	&= 0x3F;

	Oscl::Encoder::Api&	encoder	= _reasmEncoder;

	switch(sar){
		case 0x00:
			// Complete Message
			processAssembledPDU(
				&attributeValue[0],
				attributeValueLength
				);
			break;
		case 0x01:
			// First Segment
			encoder.reset();
			encoder.encode(opcode);
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tFirst Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x02:
			// Continuation Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tContinuation Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x03:
			// Last Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					__PRETTY_FUNCTION__
					);
				}
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tReassembled packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			processAssembledPDU(
				_reasmBuffer,
				encoder.length()
				);
			break;
		}

	return true;
	}
#endif

bool	Base::signedWriteCommand(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength,
			const uint8_t	signature[12]
			) noexcept{
	return false;
	}

bool	Base::prepareWriteRequest(
			uint16_t		attributeHandle,
			uint16_t		valueOffset,
			const uint8_t	partAttributeValue[],
			unsigned		partAttributeValueLength
			) noexcept{
	return false;
	}

bool	Base::executeWriteRequestCancel() noexcept{
	return false;
	}

bool	Base::executeWriteRequest() noexcept{
	return false;
	}

bool	Base::handleValueConfirmation() noexcept{
	return false;
	}

