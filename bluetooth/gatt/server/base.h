/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_gatt_server_baseh_
#define _oscl_bluetooth_gatt_server_baseh_

#include <stdint.h>
#include "oscl/bluetooth/gatt/service/item.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/freestore/composers.h"
#include "oscl/done/operation.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/att/server/rx/api.h"
#include "oscl/bluetooth/att/server/tx/api.h"
#include "oscl/bluetooth/mesh/proxy/sar.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace GATT {
/** */
namespace Server {

/** */
struct CharacteristicDeclaration {
	/** */
	uint16_t	handle;
	/** */
	uint8_t		properties;
	/** */
	uint16_t	attributeHandle;
	/** */
	uint16_t	uuid16;
	};

/** */
class Base :
	public Oscl::BT::ATT::Server::RX::Api
	{
	public:
		/** */
		Base() noexcept;

		/** */
		virtual const Oscl::BT::ATT::Server::TX::GroupInformation16bit*	getServices(unsigned& nServices) const noexcept=0;

		/** */
		virtual const Oscl::BT::GATT::Server::CharacteristicDeclaration*	getCharacteristicDeclarations(unsigned& nDeclarations) const noexcept=0;

		/** */
		virtual const Oscl::BT::ATT::Server::TX::InformationData16bit*	getAttributes(unsigned& nAttributes) const noexcept=0;

		/** */
		virtual Oscl::BT::ATT::Server::TX::Api&	getAttSendApi() noexcept=0;

	private:
		/** */
		const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
			findServiceHandleRange(
				uint16_t	first,
				uint16_t	last,
				unsigned&	nIndicies
				) const noexcept;

		/** */
		const CharacteristicDeclaration*
			findCharacteristicHandleRange(
				uint16_t	first,
				uint16_t	last,
				unsigned&	nIndicies
				) const noexcept;

		/** */
		const Oscl::BT::ATT::Server::TX::InformationData16bit*
			findHandleRange(
				uint16_t	first,
				uint16_t	last,
				unsigned&	nIndicies
				) const noexcept;

#if 0
	private:
		/** */
		virtual void	processAssembledPDU(
							const void*	frame,
							unsigned	length
							) noexcept=0;
#endif

	private:
		/** */
		void	sendReadByGroupTypeResponse(
					uint16_t	startingHandle,
					uint16_t	endingHandle
					) noexcept;

		/** */
		void	sendReadByTypeCharacteristicDeclarationResponse(
					uint16_t	startingHandle,
					uint16_t	endingHandle
					) noexcept;

	public: // Oscl::BT::ATT::Server::RX::Api
		/**
			uint8_t opcode = 0x02
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	exchangeMtuRequest(uint16_t clientRxMTU) noexcept;

		/**
			Implemented here
			uint8_t opcode = 0x04
			RETURN: true if processed or false if not implemented.
		 */
		bool	findInformationRequest(
					uint16_t	startingHandle,
					uint16_t	endingHandle
					) noexcept;

		/**
			Not implemented here.
			uint8_t opcode = 0x06
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	findByTypeValueRequest(
							uint16_t		startingHandle,
							uint16_t		endingHandle,
							uint16_t		attributeType,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept;

		/**
			Implemented here.
			uint8_t opcode = 0x08
			For 128-bit attributeTypeUUID
			RETURN: true if processed or false if not implemented.
		 */
		bool	readByTypeRequest(
					uint16_t	startingHandle,
					uint16_t	endingHandle,
					uint8_t		attributeTypeUUID[16]
					) noexcept;

		/**
			uint8_t opcode = 0x0A
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readRequest(
							uint16_t	attributeHandle
							) noexcept;

		/**
			uint8_t opcode = 0x0C
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readBlobRequest(
							uint16_t	attributeHandle,
							uint16_t	valueOffset
							) noexcept;

		/**
			uint8_t opcode = 0x0E
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	readMultipleRequest(
							const uint16_t	setOfAttributeHandles[],
							unsigned		nHandles
							) noexcept;

		/**
			Implemented here.
			uint8_t opcode = 0x10
			RETURN: true if processed or false if not implemented.
		 */
		bool	readByGroupTypeRequest(
					uint16_t	startingHandle,
					uint16_t	endingHandle,
					uint8_t		attributeGroupType[16]
					) noexcept;

		/**
			uint8_t opcode = 0x12
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	writeRequest(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept;

		/**
			uint8_t opcode = 0x52
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	writeCommand(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength
							) noexcept;

		/**
			uint8_t opcode = 0xD2
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	signedWriteCommand(
							uint16_t		attributeHandle,
							const uint8_t	attributeValue[],
							unsigned		attributeValueLength,
							const uint8_t	signature[12]
							) noexcept;

		/**
			uint8_t opcode = 0x16
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	prepareWriteRequest(
							uint16_t		attributeHandle,
							uint16_t		valueOffset,
							const uint8_t	partAttributeValue[],
							unsigned		partAttributeValueLength
							) noexcept;
	
		/**
			uint8_t opcode = 0x18
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	executeWriteRequestCancel() noexcept;

		/**
			uint8_t opcode = 0x18
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	executeWriteRequest() noexcept;

		/**
			uint8_t opcode = 0x1E
			RETURN: true if processed or false if not implemented.
		 */
		virtual bool	handleValueConfirmation() noexcept;
	};

}
}
}
}

#endif
