/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "oscl/error/info.h"

#include "module.h"

using namespace Oscl::Bluetooth::Strings;

const char*	Oscl::Bluetooth::Strings::meshProxyFilterType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "White list filter";
		case 0x01:
			return "White list filter";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leAdvertisingType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Connectable and scannable undirected advertising (ADV_IND)";
		case 0x01:
			return "Connectable high duty cycle directed advertising (ADV_DIRECT_IND)";
		case 0x02:
			return "Scannable undirected advertising (ADV_SCAN_IND)";
		case 0x03:
			return "Non connectable undirected advertising (ADV_NONCONN_IND)";
		case 0x04:
			return "Connectable low duty cycle directed advertising (ADV_DIRECT_IND)";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::channelMask(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "37";
		case 1:
			return "38";
		case 2:
			return "39";
		default:
			return "Unknown";
		}
	}


const char*	Oscl::Bluetooth::Strings::gpcfLinkCloseReason(uint8_t reason) noexcept{
	switch(reason){
		case 0x00:
			return "Success";
		case 0x01:
			return "Timeout";
		case 0x02:
			return "Fail";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::gpcfBearerOpcode(uint8_t opcode) noexcept{
	switch(opcode){
		case 0x00:
			return "Open Link";
		case 0x01:
			return "Link ACK";
		case 0x02:
			return "Link Close";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::oobInformation(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "Other";
		case 1:
			return "Electronic / URI";
		case 2:
			return "2D machine-readable code";
		case 3:
			return "Bar code";
		case 4:
			return "Near Field Communication (NFC)";
		case 5:
			return "Number";
		case 6:
			return "String";
		case 11:
			return "On box";
		case 12:
			return "Inside box";
		case 13:
			return "On piece of paper";
		case 14:
			return "Inside manual";
		case 15:
			return "On device";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::meshBeaconType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Unprovisioned Device beacon";
		case 0x01:
			return "Secure Network beacon";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leScanType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Passive Scanning";
		case 0x01:
			return "Active Scanning";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leAddressType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Public Device Address";
		case 0x01:
			return "Random Device Address";
		case 0x02:
			return "Resolvable Private Address or public address";
		case 0x03:
			return "Resolvable Private Address or random address";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leAdvertisingFilterPolicy(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Process scan and connection requests from all devices (no white list)";
		case 0x01:
			return "Process connection requests ffrom all devices and only scan requests from devices that are in the White List.";
		case 0x02:
			return "Process scan requests from all devices and only connection requests from devices that are in the White List.";
		case 0x03:
			return "Process scan and connection requests only from devices in the White List.";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leScanningFilterPolicy(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Accept all advertising packets except directed not addressed";
		case 0x01:
			return "Accept only advertising packets in white list.";
		case 0x02:
			return "Accept all advertising packets except directed initiator unknown";
		case 0x03:
			return "Accept all advertising packets except not whitelist and not directed";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::voiceSettingInputCoding(uint16_t voiceSettings) noexcept{
	voiceSettings	>>= 8;
	voiceSettings	&= 0x03;

	switch(voiceSettings){
		case 0x00:
			return "Linear";
		case 0x01:
			return "u-Law";
		case 0x02:
			return "A-Law";
		case 0x03:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::voiceSettingInputDataFormat(uint16_t voiceSettings) noexcept{
	voiceSettings	>>= 6;
	voiceSettings	&= 0x03;

	switch(voiceSettings){
		case 0x00:
			return "1's complement";
		case 0x01:
			return "2's complement";
		case 0x02:
			return "Sign-Magnitude";
		case 0x03:
			return "Unsigned";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::voiceSettingSampleSize(uint16_t voiceSettings) noexcept{
	voiceSettings	>>= 5;
	voiceSettings	&= 0x01;

	switch(voiceSettings){
		case 0x00:
			return "8-bit";
		case 0x01:
			return "16-bit";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::voiceSettingLinearPcmBitPos(uint16_t voiceSettings) noexcept{
	voiceSettings	>>= 2;
	voiceSettings	&= 0x07;

	switch(voiceSettings){
		case 0x00:
			return "0";
		case 0x01:
			return "1";
		case 0x02:
			return "2";
		case 0x03:
			return "3";
		case 0x04:
			return "4";
		case 0x05:
			return "5";
		case 0x06:
			return "6";
		case 0x07:
			return "7";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::voiceSettingAirCodingFormat(uint16_t voiceSettings) noexcept{
	voiceSettings	>>= 0;
	voiceSettings	&= 0x02;

	switch(voiceSettings){
		case 0x00:
			return "CVSD";
		case 0x01:
			return "u-Law";
		case 0x02:
			return "A-Law";
		case 0x03:
			return "Transparent Data";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::classOfDevice(uint32_t classOfDevice) noexcept{
	switch(classOfDevice){
		case 0x00000000:
			return "Unknown Class of Device";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::linkType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "SCO Connection requested";
		case 0x01:
			return "ACL Connection requested";
		case 0x02:
			return "eSCO Connection requested";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::eirFlag(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "Limited";
		case 1:
			return "General";
		case 2:
			return "BR/EDR not Supported";
		case 3:
			return "Simultaneous LE & BR/EDR Controller";
		case 4:
			return "Simultaneous LE & BR/EDR Host";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::pageScanType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Standard Scan";
		case 0x01:
			return "Interlaced Scan";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::linkPolicy(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "Disable All LM Modes Default";
		case 1:
			return "Enable Role Switch";
		case 2:
			return "Enable Sniff Mode";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::lmpFeature(unsigned bit) noexcept{
	// page 528
	switch(bit){
		case 0:
			return "3 slot packets";
		case 1:
			return "5 slot packets";
		case 2:
			return "Encryption";
		case 3:
			return "Slot offset";
		case 4:
			return "Timing accuracy";
		case 5:
			return "Role switch";
		case 6:
			return "Hold mode";
		case 7:
			return "Sniff mode";
		case 9:
			return "Power control requests";
		case 10:
			return "Channel quality driven data rate (CQDDR)";
		case 11:
			return "SCO link";
		case 12:
			return "HV2 packets";
		case 13:
			return "HV3 packets";
		case 14:
			return "u-law log synchronous data";
		case 15:
			return "A-law log synchronous data";
		case 16:
			return "CVSD synchronous data";
		case 17:
			return "Paging parameter negotiation";
		case 18:
			return "Power control";
		case 19:
			return "Transparent synchronous data";
		case 20:
			return "Flow control lag (least significant bit)";
		case 21:
			return "Flow control lag (most significant bit)";
		case 22:
			return "Broadcast Encryption";
		case 25:
			return "Enhanced Data Rate ACL 2 Mb/s mode";
		case 26:
			return "Enhanced Data Rate ACL 3 Mb/s mode";
		case 27:
			return "Enhanced inquiry scan";
		case 28:
			return "Interlaced inquiry scan";
		case 29:
			return "Interlaced page scan";
		case 30:
			return "RSSI with inquiry results";
		case 31:
			return "Extended SCO link (EV3 packets)";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leLlFeature(unsigned bit) noexcept{
	// page 2654
	switch(bit){
		case 0:
			return "LE Encryption";
		case 1:
			return "Connection Parameters Request Procedure";
		case 2:
			return "Extended Reject Indication";
		case 3:
			return "Slave-initiated Features Exchange";
		case 4:
			return "LE Ping";
		case 5:
			return "LE Data Packet Length Extension";
		case 6:
			return "LL Privacy";
		case 7:
			return "Extended Scanner Filter Policies";
		case 8:
			return "LE 2M PHY";
		case 9:
			return "Stable Modulation Index - Transmitter";
		case 10:
			return "Stable Modulation Index - Receiver";
		case 11:
			return "LE Coded PHY";
		case 12:
			return "LE Extended Advertising";
		case 13:
			return "LE Periodic Advertising";
		case 14:
			return "Channel Selection Algorithm #2";
		case 15:
			return "LE Power Class 1";
		case 16:
			return "Minimum Number of Used Channels Procedure";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leLmpFeature(unsigned bit) noexcept{
	return lmpFeature(bit);
	}

const char*	Oscl::Bluetooth::Strings::featuresPage0(unsigned bit) noexcept{
	return lmpFeature(bit);
	}

const char*	Oscl::Bluetooth::Strings::featuresPage1(unsigned bit) noexcept{
	switch(bit){
		case 0:	// 64
			return "Secure Simple Pairing (Host Support)";
		case 1:	// 65
			return "LE Supported (Host)";
		case 2:	// 66
			return "Simultaneous LE and BR/EDR to Same Device Capable (Host)";
		case 3:	// 67
			return "Secure Connections (Host Support)";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::featuresPage2(unsigned bit) noexcept{
	switch(bit){
		case 0:	// 128
			return "Connectionless Slave Broadcast - Master Operation";
		case 1:	// 129
			return "Connectionless Slave Broadcast - Slave Operation";
		case 2:	// 130
			return "Synchronization Train";
		case 3:	// 131
			return "Synchronization Scan";
		case 4:	// 132
			return "Inquiry Response Notification Event";
		case 5:	// 133
			return "Generalized interlaced scan";
		case 6:	// 134
			return "Coarse Clock Adjustment";
		case 8:	// 136
			return "Secure Connections (Controller Support)";
		case 9:	// 137
			return "Ping";
		case 10:	// 138
			return "Slot Availability Mask";
		case 11:	// 139
			return "Train nudging";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::featureOnPage(unsigned page, unsigned bit){

	const char*	(*featureFunc)(unsigned bit);

	switch(page){
		case 0:
			featureFunc	= featuresPage0;
			break;
		case 1:
			featureFunc	= featuresPage1;
			break;
		case 2:
			featureFunc	= featuresPage2;
			break;
		default:
			featureFunc	= 0;
			break;
		}

	if(!featureFunc){
		return "Unknown Page";
		}

	return featureFunc(bit);
	}

const char*	Oscl::Bluetooth::Strings::hciOpcode(uint16_t opcode) noexcept{
	switch(opcode){
		case 0x0000:
			return "HCI_OP_NOP";
		case 0x0401:
			return "HCI_OP_INQUIRY";
		case 0x0402:
			return "HCI_OP_INQUIRY_CANCEL";
		case 0x0403:
			return "HCI_OP_PERIODIC_INQ";
		case 0x0404:
			return "HCI_OP_EXIT_PERIODIC_INQ";
		case 0x0405:
			return "HCI_OP_CREATE_CONN";
		case 0x0406:
			return "HCI_OP_DISCONNECT";
		case 0x0407:
			return "HCI_OP_ADD_SCO";
		case 0x0408:
			return "HCI_OP_CREATE_CONN_CANCEL";
		case 0x0409:
			return "HCI_OP_ACCEPT_CONN_REQ";
		case 0x040a:
			return "HCI_OP_REJECT_CONN_REQ";
		case 0x040b:
			return "HCI_OP_LINK_KEY_REPLY";
		case 0x040c:
			return "HCI_OP_LINK_KEY_NEG_REPLY";
		case 0x040d:
			return "HCI_OP_PIN_CODE_REPLY";
		case 0x040e:
			return "HCI_OP_PIN_CODE_NEG_REPLY";
		case 0x040f:
			return "HCI_OP_CHANGE_CONN_PTYPE";
		case 0x0411:
			return "HCI_OP_AUTH_REQUESTED";
		case 0x0413:
			return "HCI_OP_SET_CONN_ENCRYPT";
		case 0x0415:
			return "HCI_OP_CHANGE_CONN_LINK_KEY";
		case 0x0419:
			return "HCI_OP_REMOTE_NAME_REQ";
		case 0x041a:
			return "HCI_OP_REMOTE_NAME_REQ_CANCEL";
		case 0x041b:
			return "HCI_OP_READ_REMOTE_FEATURES";
		case 0x041c:
			return "HCI_OP_READ_REMOTE_EXT_FEATURES";
		case 0x041d:
			return "HCI_OP_READ_REMOTE_VERSION";
		case 0x041f:
			return "HCI_OP_READ_CLOCK_OFFSET";
		case 0x0428:
			return "HCI_OP_SETUP_SYNC_CONN";
		case 0x0429:
			return "HCI_OP_ACCEPT_SYNC_CONN_REQ";
		case 0x042a:
			return "HCI_OP_REJECT_SYNC_CONN_REQ";
		case 0x042b:
			return "HCI_OP_IO_CAPABILITY_REPLY";
		case 0x042c:
			return "HCI_OP_USER_CONFIRM_REPLY";
		case 0x042d:
			return "HCI_OP_USER_CONFIRM_NEG_REPLY";
		case 0x042e:
			return "HCI_OP_USER_PASSKEY_REPLY";
		case 0x042f:
			return "HCI_OP_USER_PASSKEY_NEG_REPLY";
		case 0x0430:
			return "HCI_OP_REMOTE_OOB_DATA_REPLY";
		case 0x0433:
			return "HCI_OP_REMOTE_OOB_DATA_NEG_REPLY";
		case 0x0434:
			return "HCI_OP_IO_CAPABILITY_NEG_REPLY";
		case 0x0435:
			return "HCI_OP_CREATE_PHY_LINK";
		case 0x0436:
			return "HCI_OP_ACCEPT_PHY_LINK";
		case 0x0437:
			return "HCI_OP_DISCONN_PHY_LINK";
		case 0x0438:
			return "HCI_OP_CREATE_LOGICAL_LINK";
		case 0x0439:
			return "HCI_OP_ACCEPT_LOGICAL_LINK";
		case 0x043a:
			return "HCI_OP_DISCONN_LOGICAL_LINK";
		case 0x043b:
			return "HCI_OP_LOGICAL_LINK_CANCEL";
		case 0x0441:
			return "HCI_OP_SET_CSB";
		case 0x0443:
			return "HCI_OP_START_SYNC_TRAIN";
		case 0x0803:
			return "HCI_OP_SNIFF_MODE";
		case 0x0804:
			return "HCI_OP_EXIT_SNIFF_MODE";
		case 0x0809:
			return "HCI_OP_ROLE_DISCOVERY";
		case 0x080b:
			return "HCI_OP_SWITCH_ROLE";
		case 0x080c:
			return "HCI_OP_READ_LINK_POLICY";
		case 0x080d:
			return "HCI_OP_WRITE_LINK_POLICY";
		case 0x080e:
			return "HCI_OP_READ_DEF_LINK_POLICY";
		case 0x080f:
			return "HCI_OP_WRITE_DEF_LINK_POLICY";
		case 0x0811:
			return "HCI_OP_SNIFF_SUBRATE";
		case 0x0c01:
			return "HCI_OP_SET_EVENT_MASK";
		case 0x0c03:
			return "HCI_OP_RESET";
		case 0x0c05:
			return "HCI_OP_SET_EVENT_FLT";
		case 0x0c0d:
			return "HCI_OP_READ_STORED_LINK_KEY";
		case 0x0c12:
			return "HCI_OP_DELETE_STORED_LINK_KEY";
		case 0x0c14:
			return "HCI_OP_READ_LOCAL_NAME";
		case 0x0c16:
			return "HCI_OP_WRITE_CA_TIMEOUT";
		case 0x0c18:
			return "HCI_OP_WRITE_PG_TIMEOUT";
		case 0x0c1a:
			return "HCI_OP_WRITE_SCAN_ENABLE";
		case 0x0c1f:
			return "HCI_OP_READ_AUTH_ENABLE";
		case 0x0c20:
			return "HCI_OP_WRITE_AUTH_ENABLE";
		case 0x0c21:
			return "HCI_OP_READ_ENCRYPT_MODE";
		case 0x0c22:
			return "HCI_OP_WRITE_ENCRYPT_MODE";
		case 0x0c23:
			return "HCI_OP_READ_CLASS_OF_DEV";
		case 0x0c24:
			return "HCI_OP_WRITE_CLASS_OF_DEV";
		case 0x0c25:
			return "HCI_OP_READ_VOICE_SETTING";
		case 0x0c26:
			return "HCI_OP_WRITE_VOICE_SETTING";
		case 0x0c33:
			return "HCI_OP_HOST_BUFFER_SIZE";
		case 0x0c38:
			return "HCI_OP_READ_NUM_SUPPORTED_IAC";
		case 0x0c39:
			return "HCI_OP_READ_CURRENT_IAC_LAP";
		case 0x0c3a:
			return "HCI_OP_WRITE_CURRENT_IAC_LAP";
		case 0x0c45:
			return "HCI_OP_WRITE_INQUIRY_MODE";
		case 240:
			return "HCI_MAX_EIR_LENGTH";
		case 0x0c52:
			return "HCI_OP_WRITE_EIR";
		case 0x0c55:
			return "HCI_OP_READ_SSP_MODE";
		case 0x0c56:
			return "HCI_OP_WRITE_SSP_MODE";
		case 0x0c57:
			return "HCI_OP_READ_LOCAL_OOB_DATA";
		case 0x0c58:
			return "HCI_OP_READ_INQ_RSP_TX_POWER";
		case 0x0c63:
			return "HCI_OP_SET_EVENT_MASK_PAGE_2";
		case 0x0c64:
			return "HCI_OP_READ_LOCATION_DATA";
		case 0x0c66:
			return "HCI_OP_READ_FLOW_CONTROL_MODE";
		case 0x0c6d:
			return "HCI_OP_WRITE_LE_HOST_SUPPORTED";
		case 0x0c74:
			return "HCI_OP_SET_RESERVED_LT_ADDR";
		case 0x0c75:
			return "HCI_OP_DELETE_RESERVED_LT_ADDR";
		case 0x0c76:
			return "HCI_OP_SET_CSB_DATA";
		case 0x0c77:
			return "HCI_OP_READ_SYNC_TRAIN_PARAMS";
		case 0x0c78:
			return "HCI_OP_WRITE_SYNC_TRAIN_PARAMS";
		case 0x0c79:
			return "HCI_OP_READ_SC_SUPPORT";
		case 0x0c7a:
			return "HCI_OP_WRITE_SC_SUPPORT";
		case 0x0c7d:
			return "HCI_OP_READ_LOCAL_OOB_EXT_DATA";
		case 0x1001:
			return "HCI_OP_READ_LOCAL_VERSION";
		case 0x1002:
			return "HCI_OP_READ_LOCAL_COMMANDS";
		case 0x1003:
			return "HCI_OP_READ_LOCAL_FEATURES";
		case 0x1004:
			return "HCI_OP_READ_LOCAL_EXT_FEATURES";
		case 0x1005:
			return "HCI_OP_READ_BUFFER_SIZE";
		case 0x1009:
			return "HCI_OP_READ_BD_ADDR";
		case 0x100a:
			return "HCI_OP_READ_DATA_BLOCK_SIZE";
		case 0x100b:
			return "HCI_OP_READ_LOCAL_CODECS";
		case 0x0c1b:
			return "HCI_OP_READ_PAGE_SCAN_ACTIVITY";
		case 0x0c1c:
			return "HCI_OP_WRITE_PAGE_SCAN_ACTIVITY";
		case 0x0c2d:
			return "HCI_OP_READ_TX_POWER";
		case 0x0c46:
			return "HCI_OP_READ_PAGE_SCAN_TYPE";
		case 0x0c47:
			return "HCI_OP_WRITE_PAGE_SCAN_TYPE";
		case 0x1405:
			return "HCI_OP_READ_RSSI";
		case 0x1407:
			return "HCI_OP_READ_CLOCK";
		case 0x1408:
			return "HCI_OP_READ_ENC_KEY_SIZE";
		case 0x1409:
			return "HCI_OP_READ_LOCAL_AMP_INFO";
		case 0x140a:
			return "HCI_OP_READ_LOCAL_AMP_ASSOC";
		case 0x140b:
			return "HCI_OP_WRITE_REMOTE_AMP_ASSOC";
		case 0x140c:
			return "HCI_OP_GET_MWS_TRANSPORT_CONFIG";
		case 0x1803:
			return "HCI_OP_ENABLE_DUT_MODE";
		case 0x1804:
			return "HCI_OP_WRITE_SSP_DEBUG_MODE";
		case 0x2001:
			return "HCI_OP_LE_SET_EVENT_MASK";
		case 0x2002:
			return "HCI_OP_LE_READ_BUFFER_SIZE";
		case 0x2003:
			return "HCI_OP_LE_READ_LOCAL_FEATURES";
		case 0x2005:
			return "HCI_OP_LE_SET_RANDOM_ADDR";
		case 0x2006:
			return "HCI_OP_LE_SET_ADV_PARAM";
		case 0x2007:
			return "HCI_OP_LE_READ_ADV_TX_POWER";
		case 31:
			return "HCI_MAX_AD_LENGTH";
		case 0x2008:
			return "HCI_OP_LE_SET_ADV_DATA";
		case 0x2009:
			return "HCI_OP_LE_SET_SCAN_RSP_DATA";
		case 0x200a:
			return "HCI_OP_LE_SET_ADV_ENABLE";
		case 0x200b:
			return "HCI_OP_LE_SET_SCAN_PARAM";
		case 0x200c:
			return "HCI_OP_LE_SET_SCAN_ENABLE";
		case 0x200d:
			return "HCI_OP_LE_CREATE_CONN";
		case 0x200e:
			return "HCI_OP_LE_CREATE_CONN_CANCEL";
		case 0x200f:
			return "HCI_OP_LE_READ_WHITE_LIST_SIZE";
		case 0x2010:
			return "HCI_OP_LE_CLEAR_WHITE_LIST";
		case 0x2011:
			return "HCI_OP_LE_ADD_TO_WHITE_LIST";
		case 0x2012:
			return "HCI_OP_LE_DEL_FROM_WHITE_LIST";
		case 0x2013:
			return "HCI_OP_LE_CONN_UPDATE";
		case 0x2016:
			return "HCI_OP_LE_READ_REMOTE_FEATURES";
		case 0x2019:
			return "HCI_OP_LE_START_ENC";
		case 0x201a:
			return "HCI_OP_LE_LTK_REPLY";
		case 0x201b:
			return "HCI_OP_LE_LTK_NEG_REPLY";
		case 0x201c:
			return "HCI_OP_LE_READ_SUPPORTED_STATES";
		case 0x2020:
			return "HCI_OP_LE_CONN_PARAM_REQ_REPLY";
		case 0x2021:
			return "HCI_OP_LE_CONN_PARAM_REQ_NEG_REPLY";
		case 0x2022:
			return "HCI_OP_LE_SET_DATA_LEN";
		case 0x2023:
			return "HCI_OP_LE_READ_DEF_DATA_LEN";
		case 0x2024:
			return "HCI_OP_LE_WRITE_DEF_DATA_LEN";
		case 0x202f:
			return "HCI_OP_LE_READ_MAX_DATA_LEN";
		case 0x2031:
			return "HCI_OP_LE_SET_DEFAULT_PHY";
		default:
			return "Unknown";
		};
	}

const char*	Oscl::Bluetooth::Strings::statusCode(uint8_t status) noexcept{
	switch(status){
		case 0x00:
			return "Success";
		case 0x01:
			return "Unknown HCI Command";
		case 0x02:
			return "Unknown Connection Identifier";
		case 0x03:
			return "Hardware Failure";
		case 0x04:
			return "Page Timeout";
		case 0x05:
			return "Authentication Failure";
		case 0x06:
			return "PIN or Key Missing";
		case 0x07:
			return "Memory Capacity Exceeded";
		case 0x08:
			return "Connection Timeout";
		case 0x09:
			return "Connection Limit Exceeded";
		case 0x0A:
			return "Synchronous Connection Limit To A Device Exceeded";
		case 0x0B:
			return "Connection Already Exists";
		case 0x0C:
			return "Command Disallowed";
		case 0x0D:
			return "Connection Rejected due to Limited Resources";
		case 0x0E:
			return "Connection Rejected Due To Security Reasons";
		case 0x0F:
			return "Connection Rejected due to Unacceptable BD_ADDR";
		case 0x10:
			return "Connection Accept Timeout Exceeded";
		case 0x11:
			return "Unsupported Feature or Parameter Value";
		case 0x12:
			return "Invalid HCI Command Parameters";
		case 0x13:
			return "Remote User Terminated Connection";
		case 0x14:
			return "Remote Device Terminated Connection due to Low Resources";
		case 0x15:
			return "Remote Device Terminated Connection due to Power Off";
		case 0x16:
			return "Connection Terminated By Local Host";
		case 0x17:
			return "Repeated Attempts";
		case 0x18:
			return "Pairing Not Allowed";
		case 0x19:
			return "Unknown LMP PDU";
		case 0x1A:
			return "Unsupported Remote Feature / Unsupported LMP Feature";
		case 0x1B:
			return "SCO Offset Rejected";
		case 0x1C:
			return "SCO Interval Rejected";
		case 0x1D:
			return "SCO Air Mode Rejected";
		case 0x1E:
			return "Invalid LMP Parameters / Invalid LL Parameters";
		case 0x1F:
			return "Unspecified Error";
		case 0x20:
			return "Unsupported LMP Parameter / Unsupported LL Parameter Value";
		case 0x21:
			return "Role Change Not Allowed";
		case 0x22:
			return "LMP Response Timeout / LL Response Timeout";
		case 0x23:
			return "LMP Error Transaction Collision / LL Procedure Collision";
		case 0x24:
			return "LMP PDU Not Allowed";
		case 0x25:
			return "Encryption Mode Not Acceptable";
		case 0x26:
			return "Link Key cannot be Changed";
		case 0x27:
			return "Requested QoS Not Supported";
		case 0x28:
			return "Instant Passed";
		case 0x29:
			return "Pairing With Unit Key Not Supported";
		case 0x2A:
			return "Different Transaction Collision";
		case 0x2C:
			return "QoS Unacceptable Parameter";
		case 0x2D:
			return "QoS Rejected";
		case 0x2E:
			return "Channel Classification Not Supported";
		case 0x2F:
			return "Insufficient Security";
		case 0x30:
			return "Parameter Out Of Mandantory Range";
		case 0x32:
			return "Role Switch Pending";
		case 0x34:
			return "Reserved Slot Violation";
		case 0x35:
			return "Role Switch Failed";
		case 0x36:
			return "Extended Inquiry Response Too Large";
		case 0x37:
			return "Secure Simple Pairing Not Supported By Host";
		case 0x38:
			return "Host Busy - Pairing";
		case 0x39:
			return "Connection Rejected due to No Suitable Channel Found";
		case 0x3A:
			return "Controller Busy";
		case 0x3B:
			return "Unacceptable Connection Parameters";
		case 0x3C:
			return "Advertising Timeout";
		case 0x3D:
			return "Connection Terminated due to MIC Failure";
		case 0x3E:
			return "Connection Failed to be Established";
		case 0x3F:
			return "MAC Connection Failed";
		case 0x40:
			return "Coarse Clock Adjustment Rejected but Will Try to Adjust Using Clock Dragging";
		case 0x41:
			return "Type0 Submap Not Defined";
		case 0x42:
			return "Unknown Advertising Identifier";
		case 0x43:
			return "Limit Reached";
		case 0x44:
			return "Operation Cancelled by Host";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leEventMask(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "Connection Complete";
		case 1:
			return "Advertising Report";
		case 2:
			return "Connection Update Complete";
		case 3:
			return "Read Remote Features Complete";
		case 4:
			return "Long Term Key Request";
		case 5:
			return "Remote Connection Parameter Request";
		case 6:
			return "Data Length Change";
		case 7:
			return "Read Local P-256 Public Key Complete";
		case 8:
			return "Generate DHKey Complete";
		case 9:
			return "Enhanced Connection Complete";
		case 10:
			return "Directed Advertising Report";
		case 11:
			return "PHY Update Complete";
		case 12:
			return "Extended Advertising Report";
		case 13:
			return "Periodic Advertising Sync Established";
		case 14:
			return "Periodic Advertising Report";
		case 15:
			return "Periodic Advertising Sync Lost";
		case 16:
			return "Extended Scan Timeout";
		case 17:
			return "Extended Advertising Set Terminated";
		case 18:
			return "Scan Request Received";
		case 19:
			return "Channel Selection Algorithm";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::leSubEvent(uint8_t type) noexcept{
	switch(type){
		case 0x01:
			return "LE Connection Complete";
		case 0x02:
			return "LE Advertising Reports";
		case 0x03:
			return "LE Connection Update Complete";
		case 0x04:
			return "LE Read Remote Features Complete";
		case 0x05:
			return "LE Long Term Key Request";
		case 0x06:
			return "LE Remote Connection Parameter Request";
		case 0x07:
			return "LE Data Length Change";
		case 0x08:
			return "LE Read Local P-256 Public Key Complete";
		case 0x09:
			return "LE Generate DHKey Complete";
		case 0x0A:
			return "LE Enhanced Connection Complete";
		case 0x0B:
			return "LE Directed Advertising Report";
		case 0x0C:
			return "LE PHY Update Complete";
		case 0x0D:
			return "LE Extended Advertising Report";
		case 0x0E:
			return "LE Periodic Advertising Sync Established";
		case 0x0F:
			return "LE Periodic Advertising Report";
		case 0x10:
			return "LE Periodic Advertising Sync Lost";
		case 0x11:
			return "LE Scan Timeout";
		case 0x12:
			return "LE Extended Advertising Set Terminated";
		case 0x13:
			return "LE Scan Request Received";
		case 0x14:
			return "LE Channel Selection Algorithm";
		case 0xFD:
			return "LE Stack Internal";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::inqueryMode(uint8_t mode) noexcept{
	switch(mode){
		case 0x01:
			return "Standard Inquiry Result";
		case 0x02:
			return "Inquiry Result format with RSSI";
		case 0x03:
			return "Inquiry Result with RSSI format or Extended Inquiry Result format";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand00(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Inquiry";
		case 1:
			return "Inquiry Cancel";
		case 2:
			return "Periodic Inquiry Mode";
		case 3:
			return "Exit Periodic Inquiry Mode";
		case 4:
			return "Create Connection";
		case 5:
			return "Disconnect";
		case 6:
			return "Add SCO Connection";	// deprecated
		case 7:
			return "Create Connection Cancel";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand01(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Accept Connection Request";
		case 1:
			return "Reject Connection Request";
		case 2:
			return "Link Key Request Reply";
		case 3:
			return "Link Key Request Negative Reply";
		case 4:
			return "PIN Code Request Reply";
		case 5:
			return "PIN Code Request Negative Reply";
		case 6:
			return "Change Connection Packet Type";
		case 7:
			return "Authentication Requested";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand02(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Set Connection Encryption";
		case 1:
			return "Change Connection Link Key";
		case 2:
			return "Master Link Key";
		case 3:
			return "Remote Name Request";
		case 4:
			return "Remote Name Request Cancel";
		case 5:
			return "Read Remote Supported Features";
		case 6:
			return "Read Remote Extended Features";
		case 7:
			return "Read Remote Version Information";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand03(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Clock Offset";
		case 1:
			return "Read LMP Handle";
		case 2:
			return "Reserved";
		case 3:
			return "Reserved";
		case 4:
			return "Reserved";
		case 5:
			return "Reserved";
		case 6:
			return "Reserved";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand04(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Reserved";
		case 1:
			return "Hold Mode";
		case 2:
			return "Sniff Mode";
		case 3:
			return "Exit Sniff Mode";
		case 4:
			return "Reserved";
		case 5:
			return "Reserved";
		case 6:
			return "QoS Setup";
		case 7:
			return "Role Discovery";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand05(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Switch Role";
		case 1:
			return "Read Link Policy Settings";
		case 2:
			return "Write Link Policy Settings";
		case 3:
			return "Read Default Link Policy Settings";
		case 4:
			return "Write Default Link Policy Settings";
		case 5:
			return "Flow Specification";
		case 6:
			return "Set Event Mask";
		case 7:
			return "Reset";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand06(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Set Event Filter";
		case 1:
			return "Flush";
		case 2:
			return "Read PIN Type";
		case 3:
			return "Write PIN Type";
		case 4:
			return "Create New Unit Key";
		case 5:
			return "Read Stored Link Key";
		case 6:
			return "Write Stored Link Key";
		case 7:
			return "Delete Stored Link Key";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand07(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Write Local Name";
		case 1:
			return "Read Local Name";
		case 2:
			return "Read Connection Accept Timeout";
		case 3:
			return "Write Connection Accept Timeout";
		case 4:
			return "Read Page Timeout";
		case 5:
			return "Write Page Timeout";
		case 6:
			return "Read Scan Enable";
		case 7:
			return "Write Scan Enable";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand08(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Page Scan Activity";
		case 1:
			return "Write Page Scan Activity";
		case 2:
			return "Read Inquiry Scan Activity";
		case 3:
			return "Write Inquiry Scan Activity";
		case 4:
			return "Read Authentication Enable";
		case 5:
			return "Write Authentication Enable";
		case 6:
			return "Read Encryption Mode"; // deprecated
		case 7:
			return "Write Encryption Mode"; // deprecated
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand09(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Class Of Device";
		case 1:
			return "Write Class Of Device";
		case 2:
			return "Read Voice Setting";
		case 3:
			return "Write Voice Setting";
		case 4:
			return "Read Automatic Flush Timeout";
		case 5:
			return "Write Automatic Flush Timeout";
		case 6:
			return "Read Num Broadcast Retransmissions";
		case 7:
			return "Write Num Broadcast Retransmissions";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand10(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Hold Mode Activity";
		case 1:
			return "Write Hold Mode Activity";
		case 2:
			return "Read Transmit Power Level";
		case 3:
			return "Read Synchronous Flow Control Enable";
		case 4:
			return "Write Synchronous Flow Control Enable";
		case 5:
			return "Set Controller To Host Flow Control";
		case 6:
			return "Host Buffer Size";
		case 7:
			return "Host Number Of Completed Packets";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand11(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Link Supervision Timeout";
		case 1:
			return "Write Link Supervision Timeout";
		case 2:
			return "Read Number of Supported IAC";
		case 3:
			return "Read Current IAC LAP";
		case 4:
			return "Write Current IAC LAP";
		case 5:
			return "Read Page Scan Mode Period"; // deprecated
		case 6:
			return "Write Page Scan Mode Period"; // deprecated
		case 7:
			return "Read Page Scan Mode"; // deprecated
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand12(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Write Page Scan Mode";	// deprecated
		case 1:
			return "Set AFH Host Channel Classification";
		case 2:
			return "Reserved";
		case 3:
			return "Reserved";
		case 4:
			return "Read Inquiry Scan Type";
		case 5:
			return "Write Inquiry Scan Type";
		case 6:
			return "Read Inquiry Mode";
		case 7:
			return "Write Inquiry Mode";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand13(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Page Scan Type";
		case 1:
			return "Write Page Scan Type";
		case 2:
			return "Read AFH Channel Assessment Mode";
		case 3:
			return "Write AFH Channel Assessment Mode";
		case 4:
			return "Reserved";
		case 5:
			return "Reserved";
		case 6:
			return "Reserved";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand14(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Reserved";
		case 1:
			return "Reserved";
		case 2:
			return "Reserved";
		case 3:
			return "Read Local Version Information";
		case 4:
			return "Reserved";
		case 5:
			return "Read Local Supported Features";
		case 6:
			return "Read Local Extended Features";
		case 7:
			return "Read Buffer Size";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand15(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Country Code"; // deprecated
		case 1:
			return "Read BD ADDR";
		case 2:
			return "Read Failed Contact Counter";
		case 3:
			return "Reset Failed Contact Counter";
		case 4:
			return "Read Link Quality";
		case 5:
			return "Read RSSI";
		case 6:
			return "Read AFH Channel Map";
		case 7:
			return "Read Clock";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand16(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Loopback Mode";
		case 1:
			return "Write Loopback Mode";
		case 2:
			return "Enable Device Under Test Mode";
		case 3:
			return "Setup Synchronous Connection Request";
		case 4:
			return "Accept Synchronous Connection Request";
		case 5:
			return "Reject Synchronous Connection Request";
		case 6:
			return "Reserved";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand17(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Extended Inquiry Response";
		case 1:
			return "Write Extended Inquiry Response";
		case 2:
			return "Refresh Encryption Key";
		case 3:
			return "Reserved";
		case 4:
			return "Sniff Subrating";
		case 5:
			return "Read Simple Pairing Mode";
		case 6:
			return "Write Simple Pairing Mode";
		case 7:
			return "Read Local OOB Data";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand18(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Inquiry Response Transmit Power Level";
		case 1:
			return "Write Inquiry Transmit Power Level";
		case 2:
			return "Read Default Erroneous Data Reporting";
		case 3:
			return "Write Default Erroneous Data Reporting";
		case 4:
			return "Reserved";
		case 5:
			return "Reserved";
		case 6:
			return "Reserved";
		case 7:
			return "IO Capability Request Reply";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand19(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "User Confirmation Request Reply";
		case 1:
			return "User Confirmation Request Negative Reply";
		case 2:
			return "User Passkey Request Reply";
		case 3:
			return "User Passkey Request Negative Reply";
		case 4:
			return "Remote OOB Data Request Reply";
		case 5:
			return "Write Simple Pairing Debug Mode";
		case 6:
			return "Enhanced Flush";
		case 7:
			return "Remote OOB Data Request Negative Reply";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand20(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Reserved";
		case 1:
			return "Reserved";
		case 2:
			return "Send Keypress Notification";
		case 3:
			return "IO Capability Request Negative Reply";
		case 4:
			return "Read Encryption Key Size";
		case 5:
			return "Reserved";
		case 6:
			return "Reserved";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand21(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Create Physical Link";
		case 1:
			return "Accept Physical Link";
		case 2:
			return "Disconnect Physical Link";
		case 3:
			return "Create Logical Link";
		case 4:
			return "Accept Logical Link";
		case 5:
			return "Disconnect Logical Link";
		case 6:
			return "Logical Link Cancel";
		case 7:
			return "Flow Spec Modify";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand22(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Logical Link Accept Timeout";
		case 1:
			return "Write Logical Link Accept Timeout";
		case 2:
			return "Set Event Mask Page 2";
		case 3:
			return "Read Location Data";
		case 4:
			return "Write Location Data";
		case 5:
			return "Read Local AMP Info";
		case 6:
			return "Read Local AMP_ASSOC";
		case 7:
			return "Write Remote AMP_ASSOC";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand23(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Flow Control Mode";
		case 1:
			return "Write Flow Control Mode";
		case 2:
			return "Read Data Block Size";
		case 3:
			return "Reserved";
		case 4:
			return "Reserved";
		case 5:
			return "Enable AMP Receiver Reports";
		case 6:
			return "AMP Test End";
		case 7:
			return "AMP Test";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand24(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Enhanced Transmit Power Level";
		case 1:
			return "Reserved";
		case 2:
			return "Read Best Effort Flush Timeout";
		case 3:
			return "Write Best Effort Flush Timeout";
		case 4:
			return "Short Range Mode";
		case 5:
			return "Read LE Host Support";
		case 6:
			return "Write LE Host Support";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand25(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Set Event Mask";
		case 1:
			return "LE Read Buffer Size";
		case 2:
			return "LE Read Local Supported Features";
		case 3:
			return "Reserved";
		case 4:
			return "LE Set Random Address";
		case 5:
			return "LE Set Advertising Parameters";
		case 6:
			return "LE Read Advertising Channel TX Power";
		case 7:
			return "LE Set Advertising Data";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand26(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Set Scan Response Data";
		case 1:
			return "LE Set Advertising Enable";
		case 2:
			return "LE Set Scan Parameters";
		case 3:
			return "LE Set Scan Enable";
		case 4:
			return "LE Create Connection";
		case 5:
			return "LE Create Connection Cancel";
		case 6:
			return "LE Read Write List Size";
		case 7:
			return "LE Clear White List";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand27(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Add Device To White List";
		case 1:
			return "LE Remove Device From White List";
		case 2:
			return "LE Connection Update";
		case 3:
			return "LE Set Host Channel Classification";
		case 4:
			return "LE Read Channel Map";
		case 5:
			return "LE Read Remote Features";
		case 6:
			return "LE Encrypt";
		case 7:
			return "LE Rand";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand28(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Start Encryption";
		case 1:
			return "LE Long Term Key Request Reply";
		case 2:
			return "LE Long Term Key Request Negative Reply";
		case 3:
			return "LE Read Supported States";
		case 4:
			return "LE Receiver Test";
		case 5:
			return "LE Transmitter Test";
		case 6:
			return "LE Test End";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand29(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Reserved";
		case 1:
			return "Reserved";
		case 2:
			return "Reserved";
		case 3:
			return "Ehnanced Setup Syncrhonous Connection";
		case 4:
			return "Ehnanced Accept Syncrhonous Connection";
		case 5:
			return "Read Local Supported Codecs";
		case 6:
			return "Set MWS Channel Parameters";
		case 7:
			return "Set External Frame Configuration";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand30(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Set MWS Signaling";
		case 1:
			return "Set MWS Transport Layer";
		case 2:
			return "Set MWS Scan Frequency Table";
		case 3:
			return "Get MWS Transport Layer Configuration";
		case 4:
			return "Set MWS PATTERN Configuration";
		case 5:
			return "Set Triggered Clock Capture";
		case 6:
			return "Truncated Page";
		case 7:
			return "Truncated Page Cancel";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand31(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Set Connectionless Slave Broadcast";
		case 1:
			return "Set Connectionless Slave Broadcast Receive";
		case 2:
			return "Start Synchronization Train";
		case 3:
			return "Receive Synchronization Train";
		case 4:
			return "Set Reserved LD_ADDR";
		case 5:
			return "Delete Reserved LD_ADDR";
		case 6:
			return "Set Connectionless Slave Broadcast Data";
		case 7:
			return "Read Synchronization Train Parameters";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand32(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Write Synchronization Train Parameters";
		case 1:
			return "Remote OOB Extended Data Request Reply";
		case 2:
			return "Read Secure Connections Host Support";
		case 3:
			return "Write Secure Connections Host Support";
		case 4:
			return "Read Authenticated Payload Timeout";
		case 5:
			return "Write Authenticated Payload Timeout";
		case 6:
			return "Read Local OOB Extended Data";
		case 7:
			return "Write Secure Connections Test Mode";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand33(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "Read Extended Page Timeout";
		case 1:
			return "Write Extended Page Timeout";
		case 2:
			return "Read Extended Inquiry Length";
		case 3:
			return "Write Extended Inquiry Length";
		case 4:
			return "LE Remote Connection Parameter Request Reply";
		case 5:
			return "LE Remote Connection Parameter Request Negative Reply";
		case 6:
			return "LE Set Data Length";
		case 7:
			return "LE Read Suggested Default Data Length";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand34(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Write Suggested Default Data Length";
		case 1:
			return "LE Read Local P-256 Public Key";
		case 2:
			return "LE Generate DH Key";
		case 3:
			return "LE Add Device To Resolving List";
		case 4:
			return "LE Remove Device From Resolving List";
		case 5:
			return "LE Clear Resolving List";
		case 6:
			return "LE Read Resolving List Size";
		case 7:
			return "LE Read Peer Resolvable Address";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand35(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Read Local Resolvable Address";
		case 1:
			return "LE Set Address Resolution Enable";
		case 2:
			return "LE Set Resolvable Private Address";
		case 3:
			return "LE Read Maximum Data Length";
		case 4:
			return "LE Read PHY Command";
		case 5:
			return "LE Set Default PHY Command";
		case 6:
			return "LE Set PHY Command";
		case 7:
			return "LE Enhanced Receiver Test Command";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand36(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Enhanced Transmitter Test Command";
		case 1:
			return "LE Set Advertising Set Random Address Command";
		case 2:
			return "LE Set Extended Advertising Parameters Command";
		case 3:
			return "LE Set Extended Advertising Data Comman";
		case 4:
			return "LE Set Extended Scan Response Data Command";
		case 5:
			return "LE Set Extended Advertising Enable Command";
		case 6:
			return "LE Read Maximum Advertising Data Length Command";
		case 7:
			return "LE Read Number of Supported Advertising Sets Command";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand37(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Remove Advertising Set Command";
		case 1:
			return "LE Clear Advertising Sets Command";
		case 2:
			return "LE Set Periodic Advertising Parameters Command";
		case 3:
			return "LE Set Periodic Advertising Data Command";
		case 4:
			return "LE Set Periodic Advertising Enable Command";
		case 5:
			return "LE Set Extended Scan Parameters Command";
		case 6:
			return "LE Set Extended Scan Enable Command";
		case 7:
			return "LE Extended Create Connection Command";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand38(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Periodic Advertising Create Sync Command";
		case 1:
			return "LE Periodic Advertising Create Sync Cancel Command";
		case 2:
			return "LE Periodic Advertising Terminate Sync Command";
		case 3:
			return "LE Add Device To Periodic Advertiser List Command";
		case 4:
			return "LE Remove Device From Periodic Advertiser List Command";
		case 5:
			return "LE Clear Periodic Advertister List Command";
		case 6:
			return "LE Read Periodic Advertiser List Size Command";
		case 7:
			return "LE Read Transmit Power Command";
		default:
			return "Unknown";
		}
	}

static const char*	hciCommand39(
				unsigned bit
				) noexcept{
	switch(bit){
		case 0:
			return "LE Read RF Path Compensation Command";
		case 1:
			return "LE Write RF Path Compensation Command";
		case 2:
			return "Reserved";
		case 3:
			return "Reserved";
		case 4:
			return "Reserved";
		case 5:
			return "Reserved";
		case 6:
			return "Reserved";
		case 7:
			return "Reserved";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::hciCommand(
				unsigned	octet,
				unsigned	bit
				) noexcept{
	switch(octet){
		case 0:
			return hciCommand00(bit);
		case 1:
			return hciCommand01(bit);
		case 2:
			return hciCommand02(bit);
		case 3:
			return hciCommand03(bit);
		case 4:
			return hciCommand04(bit);
		case 5:
			return hciCommand05(bit);
		case 6:
			return hciCommand06(bit);
		case 7:
			return hciCommand07(bit);
		case 8:
			return hciCommand08(bit);
		case 9:
			return hciCommand09(bit);
		case 10:
			return hciCommand10(bit);
		case 11:
			return hciCommand11(bit);
		case 12:
			return hciCommand12(bit);
		case 13:
			return hciCommand13(bit);
		case 14:
			return hciCommand14(bit);
		case 15:
			return hciCommand15(bit);
		case 16:
			return hciCommand16(bit);
		case 17:
			return hciCommand17(bit);
		case 18:
			return hciCommand18(bit);
		case 19:
			return hciCommand19(bit);
		case 20:
			return hciCommand20(bit);
		case 21:
			return hciCommand21(bit);
		case 22:
			return hciCommand22(bit);
		case 23:
			return hciCommand23(bit);
		case 24:
			return hciCommand24(bit);
		case 25:
			return hciCommand25(bit);
		case 26:
			return hciCommand26(bit);
		case 27:
			return hciCommand27(bit);
		case 28:
			return hciCommand28(bit);
		case 29:
			return hciCommand29(bit);
		case 30:
			return hciCommand30(bit);
		case 31:
			return hciCommand31(bit);
		case 32:
			return hciCommand32(bit);
		case 33:
			return hciCommand33(bit);
		case 34:
			return hciCommand34(bit);
		case 35:
			return hciCommand35(bit);
		case 36:
			return hciCommand36(bit);
		case 37:
			return hciCommand37(bit);
		case 38:
			return hciCommand38(bit);
		case 39:
			return hciCommand39(bit);
		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::eventMask(unsigned bit) noexcept{
	switch(bit){
		case 0:
			return "Inquiry Complete";
		case 1:
			return "Inquiry Result";
		case 2:
			return "Connection Complete";
		case 3:
			return "Connection Request";
		case 4:
			return "Disconnection Complete";
		case 5:
			return "Authentication Complete";
		case 6:
			return "Remote Name Request Complete";
		case 7:
			return "Encryption Change";
		case 8:
			return "Change Connection Link Key Complete";
		case 9:
			return "Master Link Key Complete";
		case 10:
			return "Read Remote Supported Features Complete";
		case 11:
			return "Read Remote Version Information Complete";
		case 12:
			return "QoS Setup Complete";
		case 15:
			return "Hardware Error";
		case 16:
			return "Flush Occurred";
		case 17:
			return "Role Change";
		case 19:
			return "Mode Change";
		case 20:
			return "Return Link Keys";
		case 21:
			return "PIN Code Request";
		case 22:
			return "Link Key Request";
		case 23:
			return "Link Key Notification";
		case 24:
			return "Loopback Command";
		case 25:
			return "Data Buffer Overflow";
		case 26:
			return "Max Slots Change";
		case 27:
			return "Read Clock Offset Complete";
		case 28:
			return "Connection Packet Type Changed";
		case 29:
			return "QoS Violation";
		case 30:
			return "Page Scan Mode Change";
		case 31:
			return "Page Scan Repitition Mode Change";
		case 32:
			return "Flow Specification Complete";
		case 33:
			return "Inquiry Result with RSSI";
		case 34:
			return "Read Remote Extended Features Complete";
		case 43:
			return "Synchronous Connection Complete";
		case 44:
			return "Synchronous Connection Changed";
		case 45:
			return "Sniff Subrating";
		case 46:
			return "Extended Inquiry Result";
		case 47:
			return "Encryption Key Refresh Complete";
		case 48:
			return "IO Capability Request";
		case 49:
			return "IO Capability Response";
		case 50:
			return "User Confirmation Request";
		case 51:
			return "User Passkey Request";
		case 52:
			return "Remote OOB Data Request";
		case 53:
			return "Simple Pairing Complete";
		case 55:
			return "Link Superviion Timeout Changed";
		case 56:
			return "Enhanced Flush Complete";
		case 58:
			return "User Passkey Notification";
		case 59:
			return "Keypress Notification";
		case 60:
			return "Remote Host Supported Features Notification";
		case 61:
			return "LE Meta";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leMasterClockAccuracy(uint8_t accuracy) noexcept{
	switch(accuracy){
		case 0x00:
			return "500 ppm";
		case 0x01:
			return "250 ppm";
		case 0x02:
			return "150 ppm";
		case 0x03:
			return "100 ppm";
		case 0x04:
			return "75 ppm";
		case 0x05:
			return "50 ppm";
		case 0x06:
			return "30 ppm";
		case 0x07:
			return "20 ppm";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::lePacketBoundaryFlag(uint8_t pbFlag) noexcept{
	switch(pbFlag){
		case 0x00:
			return "First non-automatically-flushable packet";
		case 0x01:
			return "Continuing fragment";
		case 0x02:
			return "First automatically flushable packet";
		case 0x03:
			return "Complete L2CAP PDU. Automatically flushable";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leHostToControllerBroadcastFlag(uint8_t bcFlag) noexcept{
	switch(bcFlag){
		case 0x00:
			return "No broadcast";
		case 0x01:
			return "Active Slave Broadcast";
		case 0x02:
		case 0x03:
			return "Reserved for future use";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leControllerToHostBroadcastFlag(uint8_t bcFlag) noexcept{
	switch(bcFlag){
		case 0x00:
			return "Point-to-point";
		case 0x01:
			return "Active Slave Broadcast";
		case 0x02:
		case 0x03:
			return "Reserved for future use";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leRole(uint8_t role) noexcept{
	switch(role){
		case 0x00:
			return "Master";
		case 0x01:
			return "Slave";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leEventType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "LE_ADV_IND";
		case 0x01:
			return "LE_ADV_DIRECT_IND";
		case 0x02:
			return "LE_ADV_SCAN_IND";
		case 0x03:
			return "LE_ADV_NONCONN_IND";
		case 0x04:
			return "LE_ADV_SCAN_RSP";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::leBdaddrType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "ADDR_LE_DEV_PUBLIC";
		case 0x01:
			return "ADDR_LE_DEV_RANDOM";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::attFormat(uint8_t format) noexcept{
	switch(format){
		case 0x01:
			return "Handle(s) and 16-bit Bluetooth UUID(s)";
		case 0x02:
			return "Handle(s) and 128-bit Bluetooth UUID(s)";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::attErrorCode(uint8_t error) noexcept{

	if(error >= 0x12){
		if(error <= 0x7F){
			return "Application Error";
			}
		}

	if(error >= 0xA0){
		if(error <= 0xDF){
			return "Reserved";
			}
		}

	if(error >= 0xE0){
		if(error <= 0xFB){
			return "Reserved";
			}
		}

	switch(error){
		case 0x01:
			return "Invalid Handle";
		case 0x02:
			return "Read Not Permitted";
		case 0x03:
			return "Write Not Permitted";
		case 0x04:
			return "Invalid PDU";
		case 0x05:
			return "Insufficient Authentication";
		case 0x06:
			return "Request Not Supported";
		case 0x07:
			return "Invalid Offset";
		case 0x08:
			return "Insufficient Authorization";
		case 0x09:
			return "Prepare Queue Full";
		case 0x0A:
			return "Attribute Not Found";
		case 0x0B:
			return "Attribute Not Long";
		case 0x0C:
			return "Insufficient Encryption Key Size";
		case 0x0D:
			return "Invalid Attribute Value Size";
		case 0x0E:
			return "Unlikely Error";
		case 0x0F:
			return "Insufficient Encryption";
		case 0x10:
			return "Unsupported Group Type";
		case 0x11:
			return "Insufficient Resources";
		case 0x12:
			return "Insufficient Resources";
		case 0xFC:
			return "Write Request Rejected";
		case 0xFD:
			return "Client Characteristic Configuration Descriptor Improperly Configured";
		case 0xFE:
			return "Procedure Already in Progress";
		case 0xFF:
			return "Out of Range";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::attOpcode(uint8_t opcode) noexcept{
	switch(opcode){
		case 0x01:
			return "Error Response";
		case 0x02:
			return "Exchange MTU Request";
		case 0x03:
			return "Exchange MTU Response";
		case 0x04:
			return "Find Information Request";
		case 0x05:
			return "Find Information Response";
		case 0x06:
			return "Find By Type Value Request";
		case 0x07:
			return "Find By Type Value Response";
		case 0x08:
			return "Read By Type Request";
		case 0x09:
			return "Read By Type Response";
		case 0x0A:
			return "Read Request";
		case 0x0B:
			return "Read Response";
		case 0x0C:
			return "Read Blob Request";
		case 0x0D:
			return "Read Blob Response";
		case 0x0E:
			return "Read Multiple Request";
		case 0x0F:
			return "Read Multiple Response";
		case 0x10:
			return "Read By Group Type Request";
		case 0x11:
			return "Read By Group Type Response";
		case 0x12:
			return "Write Request";
		case 0x13:
			return "Write Response";
		case 0x16:
			return "Prepare Write Request";
		case 0x17:
			return "Prepare Write Response";
		case 0x18:
			return "Execute Write Request";
		case 0x19:
			return "Execute Write Response";
		case 0x1B:
			return "Handle Value Notification";
		case 0x1D:
			return "Handle Value Indication";
		case 0x1E:
			return "Handle Value Confirmation";
		case 0x52:
			return "Write Command";
		case 0xD2:
			return "Signed Write Command";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::gattService(uint16_t serviceUUID) noexcept{
	switch(serviceUUID){
		case 0x1827:
			return "Mesh Provisioning Service";
		case 0x1828:
			return "Mesh Proxy Service";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::gattAttributeType(uint16_t type) noexcept{
	switch(type){
		case 0x2800:
			return "Primary Service";
		case 0x2801:
			return "Secondary Service";
		case 0x2802:
			return "Include";
		case 0x2803:
			return "Characteristic Declaration";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::gattAttributeType(const uint8_t type[16]) noexcept{

	static const uint8_t	bluetoothUuidBase[16-4] = {
		0x00,0x00,
		0x10,0x00,
		0x80,0x00,
		0x00,0x80,0x5F,0x9B,0x34,0xFB
		};

	int
	different	= memcmp(
					&type[4],
					&bluetoothUuidBase,
					sizeof(bluetoothUuidBase)
					);

	if(different){
		return "Unknown";
		}

	static const uint8_t	primaryServiceUUID[4]	= {
		0x00, 0x00, 0x28, 0x00
		};

	if(!memcmp(primaryServiceUUID,type,4)){
		return "Primary Service";
		}

	static const uint8_t	secondaryServiceUUID[4]	= {
		0x00, 0x00, 0x28, 0x01
		};

	if(!memcmp(secondaryServiceUUID,type,4)){
		return "Secondary Service";
		}

	static const uint8_t	includeUUID[4]	= {
		0x00, 0x00, 0x28, 0x02
		};

	if(!memcmp(includeUUID,type,4)){
		return "Include";
		}

	static const uint8_t	characteristicDeclarationUUID[4]	= {
		0x00, 0x00, 0x28, 0x03
		};

	if(!memcmp(characteristicDeclarationUUID,type,4)){
		return "Characteristic Declaration";
		}

	return "Unknown";
	}

const char* Oscl::Bluetooth::Strings::gattCharacteristicProperty(uint8_t bit) noexcept{
	switch(bit){
		case 0x00:
			return "Broadcast";
		case 0x01:
			return "Read";
		case 0x02:
			return "Write Without Response";
		case 0x03:
			return "Write";
		case 0x04:
			return "Notify";
		case 0x05:
			return "Indicate";
		case 0x06:
			return "Authenticated Signed Writes";
		case 0x07:
			return "Extended Properties";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::gattCharacteristic(uint16_t characteristic) noexcept{
	switch(characteristic){
		case 0x2ADB:
			return "Mesh Provisioning Data In";
		case 0x2ADC:
			return "Mesh Provisioning Data Out";
		case 0x2ADD:
			return "Mesh Proxy Data In";
		case 0x2ADE:
			return "Mesh Proxy Data Out";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::l2capChannelID(uint16_t channelID) noexcept{
	if(channelID >= 0x0020){
		if(channelID <=0x003E){
			return "Assigned Numbers";
			}
		}

	if(channelID >= 0x0040){
		if(channelID <= 0x007F){
			return "Dynamically Allocated";
			}
		}

	if(channelID >= 0x0007){
		if(channelID <= 0x001F){
			return "Reserved";
			}
		}

	switch(channelID){
		case 0x0000:
			return "Null identifier";
		case 0x0001:
		case 0x0002:
		case 0x0003:
			return "Reserved";
		case 0x0004:
			return "Attribute Protocol";
		case 0x0005:
			return "Low Energy L2CAP Signaling channel";
		case 0x0006:
			return "Security Manager Protocol";
		case 0x003F:
		default:
			return "Reserved";
		}
	}

const char* Oscl::Bluetooth::Strings::lmpOpcode(uint16_t opcode) noexcept{
	switch(opcode){
		case 0x0001:
			return "LMP_name_req";
		case 0x0002:
			return "LMP_name_res";
		case 0x0003:
			return "LMP_accepted";
		case 0x0004:
			return "LMP_not_accepted";
		case 0x0005:
			return "LMP_clkoffset_req";
		case 0x0006:
			return "LMP_clkoffset_res";
		case 0x0007:
			return "LMP_detach";
		case 0x0008:
			return "LMP_in_rand";
		case 0x0009:
			return "LMP_comb_key";
		case 0x000A:
			return "LMP_unit_key";
		case 0x000B:
			return "LMP_au_rand";
		case 0x000C:
			return "LMP_sres";
		case 0x000D:
			return "LMP_temp_rand";
		case 0x000E:
			return "LMP_temp_key";
		case 0x000F:
			return "LMP_encryption_mode_req";
		case 0x0010:
			return "LMP_encryption_key_size_req";
		case 0x0011:
			return "LMP_start_encryption_req";
		case 0x0012:
			return "LMP_stop_encryption_req";
		case 0x0013:
			return "LMP_switch_req";
		case 0x0014:
			return "LMP_hold";
		case 0x0015:
			return "LMP_hold_req";
		case 0x0017:
			return "LMP_sniff_req";
		case 0x0018:
			return "LMP_unsniff_req";
		case 0x001F:
			return "LMP_incr_power_req";
		case 0x0020:
			return "LMP_decr_power_req";
		case 0x0021:
			return "LMP_max_power";
		case 0x0022:
			return "LMP_min_power";
		case 0x0023:
			return "LMP_auto_rate";
		case 0x0024:
			return "LMP_preferred_rate";
		case 0x0025:
			return "LMP_version_req";
		case 0x0026:
			return "LMP_version_res";
		case 0x0027:
			return "LMP_features_req";
		case 0x0028:
			return "LMP_features_res";
		case 0x0029:
			return "LMP_quality_of_service";
		case 0x002A:
			return "LMP_quality_of_service_req";
		case 0x002B:
			return "LMP_SCO_link_req";
		case 0x002C:
			return "LMP_remove_SCO_link_req";
		case 0x002D:
			return "LMP_max_slot";
		case 0x002E:
			return "LMP_max_slot_req";
		case 0x002F:
			return "LMP_timing_accuracy_req";
		case 0x0030:
			return "LMP_timing_accuracy_res";
		case 0x0031:
			return "LMP_setup_complete";
		case 0x0032:
			return "LMP_use_semi_permanent_key";
		case 0x0033:
			return "LMP_host_connection_req";
		case 0x0034:
			return "LMP_slot_offset";
		case 0x0035:
			return "LMP_page_mode_req";
		case 0x0036:
			return "LMP_page_scan_mode_req";
		case 0x0037:
			return "LMP_supervision_timeout";
		case 0x0038:
			return "LMP_test_activate";
		case 0x0039:
			return "LMP_test_control";
		case 0x003A:
			return "LMP_encryption_key_size_mask_req";
		case 0x003B:
			return "LMP_encryption_key_size_mask_res";
		case 0x003C:
			return "LMP_set_AFH";
		case 0x003D:
			return "LMP_encapsulated_header";
		case 0x003E:
			return "LMP_encapsulated_payload";
		case 0x003F:
			return "LMP_Simple_Pairing_Confirm";
		case 0x0040:
			return "LMP_Simple_Pairing_Number";
		case 0x0041:
			return "LMP_DHkey_Check";
		case 0x0042:
			return "LMP_pause_encryption_aes_req";
		case 0x7F01:
			return "LMP_accepted_ext";
		case 0x7F02:
			return "LMP_not_accepted_ext";
		case 0x7F03:
			return "LMP_features_req_ext";
		case 0x7F04:
			return "LMP_features_res_ext";
		case 0x7F05:
			return "LMP_clk_adj";
		case 0x7F06:
			return "LMP_clk_adj_ack";
		case 0x7F0B:
			return "LMP_packet_type_table_req";
		case 0x7F0C:
			return "LMP_eSCO_link_req";
		case 0x7F0D:
			return "LMP_remove_eSCO_link_req";
		case 0x7F10:
			return "LMP_channel_classification_req";
		case 0x7F11:
			return "LMP_channel_classification";
		case 0x7F15:
			return "LMP_sniff_subrating_req";
		case 0x7F16:
			return "LMP_sniff_subrating_res";
		case 0x7F17:
			return "LMP_pause_encryption_req";
		case 0x7F18:
			return "LMP_resume_encryption_req";
		case 0x7F19:
			return "LMP_IO_Capability_req";
		case 0x7F1A:
			return "LMP_IO_Capability_res";
		case 0x7F1B:
			return "LMP_numeric_comparison_failed";
		case 0x7F1C:
			return "LMP_passkey_failed";
		case 0x7F1D:
			return "LMP_oob_failed";
		case 0x7F1E:
			return "LMP_keypress_notification";
		case 0x7F1F:
			return "LMP_power_control_req";
		case 0x7F20:
			return "LMP_power_control_res";
		case 0x7F21:
			return "LMP_ping_req";
		case 0x7F22:
			return "LMP_ping_res";
		case 0x7F23:
			return "LMP_SAM_sset_type0";
		case 0x7F24:
			return "LMP_SAM_define_map";
		case 0x7F25:
			return "LMP_SAM_switch";

		default:
			return "Unknown";
		}
	}

const char*	Oscl::Bluetooth::Strings::eirType(uint8_t type) noexcept{
	// https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile/
	switch(type){
		case 0x01:
			return "EIR_FLAGS";
		case 0x02:
			return "EIR_UUID16_SOME";
		case 0x03:
			return "EIR_UUID16_ALL";
		case 0x04:
			return "EIR_UUID32_SOME";
		case 0x05:
			return "EIR_UUID32_ALL";
		case 0x06:
			return "EIR_UUID128_SOME";
		case 0x07:
			return "EIR_UUID128_ALL";
		case 0x08:
			return "EIR_NAME_SHORT";
		case 0x09:
			return "EIR_NAME_COMPLETE";
		case 0x0A:
			return "EIR_TX_POWER";
		case 0x0D:
			return "EIR_CLASS_OF_DEV";
		case 0x0E:
			return "EIR_SSP_HASH_C192";
		case 0x0F:
			return "EIR_SSP_RAND_R192";
		case 0x10:
			return "EIR_DEVICE_ID";
		case 0x11:
			return "SecurityManagerOutOfBandFlags";
		case 0x12:
			return "SlaveConnectionIntervalRange";
		case 0x14:
			return "ListOf16BitServiceSoclicitationUUIDs";
		case 0x15:
			return "ListOf128BitServiceSoclicitationUUIDs";
		case 0x16:
			return "ServiceData";
		case 0x17:
			return "PublicTargetAddress";
		case 0x18:
			return "RandomTargetAddress";
		case 0x19:
			return "EIR_APPEARANCE";
		case 0x1A:
			return "AdvertisingInterval";
		case 0x1B:
			return "EIR_LE_BDADDR";
		case 0x1C:
			return "EIR_LE_ROLE";
		case 0x1D:
			return "EIR_SSP_HASH_C256";
		case 0x1F:
			return "ListOf32BitServiceSolicitationUUIDs";
		case 0x20:
			return "ServiceData32BitUUID";
		case 0x21:
			return "ServiceData128BitUUID";
		case 0x22:
			return "EIR_LE_SC_CONFIRM";
		case 0x23:
			return "EIR_LE_SC_RANDOM";
		case 0x24:
			return "URI";
		case 0x25:
			return "IndoorPositioning";
		case 0x26:
			return "TransportDiscoveryData";
		case 0x27:
			return "LeSupportedFeatures";
		case 0x28:
			return "ChannelMapUpdateIndication";
		case 0x29:
			return "PB-ADV";
		case 0x2A:
			return "MeshMessage";
		case 0x2B:
			return "MeshBeacon";
		case 0x3D:
			return "3DInformationData";
		case 0xFF:
			return "ManufacturerSpecificData";
		default:
			return "Unknown";
		}
	}

const char* Oscl::Bluetooth::Strings::sdpPduID(uint8_t pduID) noexcept{
	switch(pduID){
		case 0x00:
			return "Reserved";
		case 0x01:
			return "SDP_ErrorResponse";
		case 0x02:
			return "SDP_ServiceSearchRequest";
		case 0x03:
			return "SDP_ServiceSearchResponse";
		case 0x04:
			return "SDP_ServiceAttributeRequest";
		case 0x05:
			return "SDP_ServiceAttributeResponse";
		case 0x06:
			return "SDP_ServiceSearchAttributeRequest";
		case 0x07:
			return "SDP_ServiceSearchAttributeResponse";
		default:
			return "Reserved";
		}
	}

const char*	Oscl::Bluetooth::Strings::meshFoundationModelOpcode(uint16_t opcode) noexcept{
	switch(opcode){
		case 0x0000:
			return "Config AppKey Add";
		case 0x0001:
			return "Config AppKey Update";
		case 0x0002:
			return "Config Composition Data Status";
		case 0x0003:
			return "Config Model Publication Set";
		case 0x0005:
			return "Health Current Status";
		case 0x0006:
			return "Health Fault Status";
		case 0x8000:
			return "Config AppKey Delete";
		case 0x8001:
			return "Config AppKey Get";
		case 0x8002:
			return "Config AppKey List";
		case 0x8003:
			return "Config AppKey Status";
		case 0x8004:
			return "Health Attention Get";
		case 0x8005:
			return "Health Attention Set";
		case 0x8006:
			return "Health Attention Set Unacknowledged";
		case 0x8007:
			return "Health Attention Status";
		case 0x8008:
			return "Config Composition Data Get";
		case 0x8009:
			return "Config Beacon Get";
		case 0x800A:
			return "Config Beacon Set";
		case 0x800B:
			return "Config Beacon Status";
		case 0x800C:
			return "Config Default TTL Get";
		case 0x800D:
			return "Config Default TTL Set";
		case 0x800E:
			return "Config Default TTL Status";
		case 0x800F:
			return "Config Friened Get";
		case 0x8010:
			return "Config Friened Set";
		case 0x8011:
			return "Config Friened Status";
		case 0x8012:
			return "Config GATT Proxy Get";
		case 0x8013:
			return "Config GATT Proxy Set";
		case 0x8014:
			return "Config GATT Proxy Status";
		case 0x8015:
			return "Config Key Refresh Phase Get";
		case 0x8016:
			return "Config Key Refresh Phase Set";
		case 0x8017:
			return "Config Key Refresh Phase Status";
		case 0x8018:
			return "Config Model Publication Get";
		case 0x8019:
			return "Config Model Publication Status";
		case 0x801A:
			return "Config Model Publication Virtual Address Set";
		case 0x801B:
			return "Config Model Subscription Add";
		case 0x801C:
			return "Config Model Subscription Delete";
		case 0x801D:
			return "Config Model Subscription Delete All";
		case 0x801E:
			return "Config Model Subscription Overwrite";
		case 0x801F:
			return "Config Model Subscription Status";
		case 0x8020:
			return "Config Model Subscription Virtual Address Add";
		case 0x8021:
			return "Config Model Subscription Virtual Address Delete";
		case 0x8022:
			return "Config Model Subscription Virtual Address Overwrite";
		case 0x8023:
			return "Config Network Transmit Get";
		case 0x8024:
			return "Config Network Transmit Set";
		case 0x8025:
			return "Config Network Transmit Status";
		case 0x8026:
			return "Config Relay Get";
		case 0x8027:
			return "Config Relay Set";
		case 0x8028:
			return "Config Relay Status";
		case 0x8029:
			return "Config SIG Model Subscription Get";
		case 0x802A:
			return "Config SIG Model Subscription List";
		case 0x802B:
			return "Config Vendor Model Subscription Get";
		case 0x802C:
			return "Config Vendor Model Subscription List";
		case 0x802D:
			return "Config Low Power Node PollTimeout Get";
		case 0x802E:
			return "Config Low Power Node PollTimeout Status";
		case 0x802F:
			return "Health Fault Clear";
		case 0x8030:
			return "Health Fault Clear Unacknowledged";
		case 0x8031:
			return "Health Fault Get";
		case 0x8032:
			return "Health Fault Test";
		case 0x8033:
			return "Health Fault Test Unacknowledged";
		case 0x8034:
			return "Health Period Get";
		case 0x8035:
			return "Health Period Set";
		case 0x8036:
			return "Health Period Set Unacknowledged";
		case 0x8037:
			return "Health Period Status";
		case 0x8038:
			return "Config Heartbeat Publication Get";
		case 0x8039:
			return "Config Heartbeat Publication Set";
		case 0x803A:
			return "Config Heartbeat Subscription Get";
		case 0x803B:
			return "Config Heartbeat Subscription Set";
		case 0x803C:
			return "Config Heartbeat Subscription Status";
		case 0x803D:
			return "Config Model App Bind";
		case 0x803E:
			return "Config Model App Status";
		case 0x803F:
			return "Config Model App Unbind";
		case 0x8040:
			return "Config NetKey Add";
		case 0x8041:
			return "Config NetKey Delete";
		case 0x8042:
			return "Config NetKey Get";
		case 0x8043:
			return "Config NetKey Delete";
		case 0x8044:
			return "Config NetKey List";
		case 0x8045:
			return "Config NetKey Update";
		case 0x8046:
			return "Config Node Identity Get";
		case 0x8047:
			return "Config Node Identity Set";
		case 0x8048:
			return "Config Node Identity Status";
		case 0x8049:
			return "Config Node Reset";
		case 0x804A:
			return "Config Node Reset Status";
		case 0x804B:
			return "Config SIG Model App Get";
		case 0x804C:
			return "Config SIG Model App List";
		case 0x804D:
			return "Config Vendor Model App Get";
		case 0x804E:
			return "Config Vendor Model App List";
		default:
			return "Unknown";
			break;
		};
	}

const char*	Oscl::Bluetooth::Strings::hciEvent(uint8_t type) noexcept{
	switch(type){
		case 0x01:
			return "Inquiry Complete";
		case 0x02:
			return "Inquiry Result";
		case 0x03:
			return "Connection Complete";
		case 0x04:
			return "Connection Request";
		case 0x05:
			return "Disconnection Complete";
		case 0x06:
			return "Authentication Complete";
		case 0x07:
			return "Remote Name Request Complete";
		case 0x08:
			return "Encryption Change";
		case 0x09:
			return "Change Connection Link Key Complete";
		case 0x0A:
			return "Master Link Key Complete";
		case 0x0B:
			return "Read Remote Supported Features Complete";
		case 0x0C:
			return "Read Remote Version Information Complete";
		case 0x0D:
			return "QoS Setup Complete";
		case 0x0E:
			return "Command Complete";
		case 0x0F:
			return "Command Status";
		case 0x10:
			return "Hardware Error";
		case 0x11:
			return "Flush Occurred";
		case 0x12:
			return "Role Change";
		case 0x13:
			return "Number Of Completed Packets";
		case 0x14:
			return "Mode Change";
		case 0x15:
			return "Return Link Keys";
		case 0x16:
			return "PIN Code Request";
		case 0x17:
			return "Link Key Request";
		case 0x18:
			return "Link Key Notification";
		case 0x19:
			return "Loopback Command";
		case 0x1A:
			return "Data Buffer Overflow";
		case 0x1B:
			return "Max Slots Change";
		case 0x1C:
			return "Read Clock Offset Complete";
		case 0x1D:
			return "Connection Packet Type Changed";
		case 0x1E:
			return "QoS Violation";
		case 0x1F:
			return "Page Scan Mode Change";
		case 0x20:
			return "Page Scan Repitition Mode Change";
		case 0x21:
			return "Flow Specification Complete";
		case 0x22:
			return "Inquiry Result with RSSI";
		case 0x23:
			return "Read Remote Extended Features Complete";
		case 0x2C:
			return "Synchronous Connection Complete";
		case 0x2D:
			return "Synchronous Connection Changed";
		case 0x2E:
			return "Sniff Subrating";
		case 0x2F:
			return "Extended Inquiry Result";
		case 0x30:
			return "Encryption Key Refresh Complete";
		case 0x31:
			return "IO Capability Request";
		case 0x32:
			return "IO Capability Response";
		case 0x33:
			return "User Confirmation Request";
		case 0x34:
			return "User Passkey Request";
		case 0x35:
			return "Remote OOB Data Request";
		case 0x36:
			return "Simple Pairing Complete";
		case 0x38:
			return "Link Superviion Timeout Changed";
		case 0x39:
			return "Enhanced Flush Complete";
		case 0x3B:
			return "User Passkey Notification";
		case 0x3C:
			return "Keypress Notification";
		case 0x3D:
			return "Remote Host Supported Features Notification";
		case 0x3E:
			return "LE Meta";
		case 0x40:
			return "Physical Link Complete";
		case 0x41:
			return "Channel Selected";
		case 0x42:
			return "Disconnection Physical Link Complete";
		case 0x43:
			return "Physical Link Loss Early Warning";
		case 0x44:
			return "Physical Link Recovery";
		case 0x45:
			return "Logical Link Complete";
		case 0x46:
			return "Disconnection Logical Link Complete";
		case 0x47:
			return "Flow Spec Modify Complete";
		case 0x48:
			return "Number Of Completed Data Blocks";
		case 0x49:
			return "AMP Start Test";
		case 0x4A:
			return "AMP End Test";
		case 0x4B:
			return "AMP Receiver Report";
		case 0x4C:
			return "Short Range Mode Change Complete";
		case 0x4D:
			return "AMP Status Change Event";
		case 0x4E:
			return "Triggered Clock Capture";
		case 0x4F:
			return "Synchronization Train Complete";
		case 0x50:
			return "Synchronization Train Received";
		case 0x51:
			return "Connectionless Slave Broadcast Receive";
		case 0x52:
			return "Connectionless Slave Broadcast Timeout";
		case 0x53:
			return "Truncated Page Complete";
		case 0x54:
			return "Slave Page Response Timeout";
		case 0x55:
			return "Connectionless Slave Broadcast Channel Map Change";
		case 0x56:
			return "Inquiry Response Notification";
		case 0x57:
			return "Authenticated Payload Timeout Expired";
		case 0x58:
			return "SAM Status Change";
		default:
			return "Unknown";
		}
	}

