/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_strings_moduleh_
#define _oscl_bluetooth_strings_moduleh_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Bluetooth {

/** */
namespace Strings{

/** */
const char*	meshProxyFilterType(uint8_t type) noexcept;

/** */
const char*	leAdvertisingType(uint8_t type) noexcept;

/** */
const char*	channelMask(unsigned bit) noexcept;

/** */
const char*	gpcfLinkCloseReason(uint8_t reason) noexcept;

/** */
const char*	gpcfBearerOpcode(uint8_t opcode) noexcept;

/** */
const char*	oobInformation(unsigned bit) noexcept;

/** */
const char*	meshBeaconType(uint8_t type) noexcept;

/** */
const char*	leScanType(uint8_t type) noexcept;

/** */
const char*	leAddressType(uint8_t type) noexcept;

/** */
const char*	leAdvertisingFilterPolicy(uint8_t type) noexcept;

/** */
const char*	leScanningFilterPolicy(uint8_t type) noexcept;

/** */
const char*	voiceSettingInputCoding(uint16_t voiceSettings) noexcept;

/** */
const char*	voiceSettingInputDataFormat(uint16_t voiceSettings) noexcept;

/** */
const char*	voiceSettingSampleSize(uint16_t voiceSettings) noexcept;

/** */
const char*	voiceSettingLinearPcmBitPos(uint16_t voiceSettings) noexcept;

/** */
const char*	voiceSettingAirCodingFormat(uint16_t voiceSettings) noexcept;

/** */
const char*	classOfDevice(uint32_t classOfDevice) noexcept;

/** */
const char*	linkType(uint8_t type) noexcept;

/** */
const char*	eirFlag(unsigned bit) noexcept;

/** */
const char*	pageScanType(uint8_t type) noexcept;

/** */
const char*	linkPolicy(unsigned bit) noexcept;

/** */
const char*	lmpFeature(unsigned bit) noexcept;

/** */
const char*	leLlFeature(unsigned bit) noexcept;

/** */
const char*	leLmpFeature(unsigned bit) noexcept;

/** */
const char*	featuresPage0(unsigned bit) noexcept;

/** */
const char*	featuresPage1(unsigned bit) noexcept;

/** */
const char*	featuresPage2(unsigned bit) noexcept;

/** */
const char*	featureOnPage(unsigned page, unsigned bit);

/** */
const char*	hciOpcode(uint16_t opcode) noexcept;

/** */
const char*	statusCode(uint8_t status) noexcept;

/** */
const char*	leEventMask(unsigned bit) noexcept;

/** */
const char*	inqueryMode(uint8_t mode) noexcept;

/** */
const char*	eventMask(unsigned bit) noexcept;

/** */
const char* leMasterClockAccuracy(uint8_t accuracy) noexcept;

/** */
const char* lePacketBoundaryFlag(uint8_t pbFlag) noexcept;

/** */
const char* leHostToControllerBroadcastFlag(uint8_t bcFlag) noexcept;

/** */
const char* leControllerToHostBroadcastFlag(uint8_t bcFlag) noexcept;

/** */
const char* leRole(uint8_t role) noexcept;

/** */
const char* leEventType(uint8_t type) noexcept;

/** */
const char* leBdaddrType(uint8_t type) noexcept;

/** */
const char*	eirType(uint8_t type) noexcept;

/** */
const char*	hciCommand(
				unsigned	octet,
				unsigned	bit
				) noexcept;

/** */
const char* lmpOpcode(uint16_t opcode) noexcept;

/** */
const char* l2capChannelID(uint16_t channelID) noexcept;

/** */
const char* attErrorCode(uint8_t error) noexcept;

/** */
const char* attOpcode(uint8_t opcode) noexcept;

/** */
const char* attFormat(uint8_t format) noexcept;

/** */
const char* gattService(uint16_t serviceUUID) noexcept;

/** */
const char* gattAttributeType(uint16_t type) noexcept;

/** */
const char* gattAttributeType(const uint8_t type[16]) noexcept;

/** */
const char* gattCharacteristicProperty(uint8_t bit) noexcept;

/** */
const char* gattCharacteristic(uint16_t characteristic) noexcept;

/** */
const char* sdpPduID(uint8_t pduID) noexcept;

/** */
const char*	meshFoundationModelOpcode(uint16_t opcode) noexcept;

/** */
const char*	hciEvent(uint8_t type) noexcept;

/** */
const char*	leSubEvent(uint8_t type) noexcept;

}
}
}

#endif

