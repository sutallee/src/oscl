/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_l2cap_tx_apih_
#define _oscl_bluetooth_l2cap_tx_apih_

#include <stdint.h>
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace L2CAP {
/** */
namespace TX {

/** This interface is used by an L2CAP channel client
	to send messages through L2CAP.
 */
class Api {
	public:
		/** The PDU argument is a fully formed L2CAP PDU
			that includes a <length> field and a <channel ID>
			followed by an information payload as described
			in Section 3.1 CONNECTION-ORIENTED CHANNELS IN BASIC L2CAP MODE
			of the Bluetooth Specification.

			The implementation will use the underlying mechanism
			(such as HCI ACL) to send this packet.

			RETURN: true if resource allocation fails.
		 */
		virtual bool	send(Oscl::Pdu::Pdu* pdu) noexcept=0;
	};
}
}
}
}

#endif
