/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"

#include "oscl/bluetooth/strings/module.h"

using namespace Oscl::BT::L2CAP::Layer;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::L2CAP::TX::Api&			lowerLayerApi,
	Oscl::Pdu::Memory::Api&		freeStore
	) noexcept:
		_lowerLayerApi(lowerLayerApi),
		_freeStore(freeStore)
		{
	}

void	Part::attach(Oscl::BT::L2CAP::Channel::RX::Item& channel) noexcept{
	_channels.put(&channel);
	}

bool	Part::send(
			uint16_t		channelID,
			Oscl::Pdu::Pdu* pdu
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: pdu->length(): %u\n",
		OSCL_PRETTY_FUNCTION,
		pdu->length()
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		uint16_t				len	= pdu->length();

		encoder.encode(len);
		encoder.encode(channelID);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}

		header	= _freeStore.allocFragmentWrapper(
					buffer,
					encoder.length()
					);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of header memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	composite->append(header);
	composite->append(fragment);

	return _lowerLayerApi.send(composite);
	}

void	Part::receive(
			const void* packet,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		packet,
		length
		);
	#endif

	// Decode L2CAP header
	uint16_t	len;
	uint16_t	channelID;
	unsigned	remaining;
	{
		Oscl::Endian::Decoder::Linear::Part
		leDecoder(
			packet,
			length
			);
		Oscl::Decoder::Api&
		decoder	= leDecoder.le();

		decoder.decode(len);
		decoder.decode(channelID);

		if(decoder.underflow()){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif
			return;
			}

		remaining	= decoder.remaining();
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tLength: (0x%4.4X) %u\n"
		"\tChannel ID: (0x%4.4X) %s\n",
		OSCL_PRETTY_FUNCTION,
		len,
		len,
		channelID,
		Oscl::Bluetooth::Strings::l2capChannelID(channelID)
		);
	#endif

	Oscl::BT::L2CAP::Channel::RX::Item*	item;

	const uint8_t*	pdu	= (const uint8_t*)packet;

	unsigned	offset	= length - remaining;

	pdu	= &pdu[offset];

	for(
		item	= _channels.first();
		item;
		item	= _channels.next(item)
		){
		if(item->getChannelID() == channelID){
			item->receive(
				pdu,
				remaining
				);
			return;
			}
		}

	Oscl::Error::Info::log(
		"%s: Unknown channelID: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		channelID
		);
	}

