/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"

using namespace Oscl::BT::L2CAP::Channel::Layer;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::L2CAP::Channel::RX::Api&	upperLayerApi,
	Oscl::BT::L2CAP::TX::Api&			lowerLayerApi,
	Oscl::Pdu::Memory::Api&				freeStore,
	uint16_t							channel
	) noexcept:
		_upperLayerApi(upperLayerApi),
		_lowerLayerApi(lowerLayerApi),
		_freeStore(freeStore),
		_channel(channel),
		_receiver(
			*this,
			&Part::getChannelID,
			&Part::receive
			)
		{
	}

Oscl::BT::L2CAP::Channel::RX::Item&		Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::send(Oscl::Pdu::Pdu* pdu) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: pdu->length(): %u\n",
		__PRETTY_FUNCTION__,
		pdu->length()
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		uint16_t				len	= pdu->length();

		encoder.encode(len);
		encoder.encode(_channel);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}

		header	= _freeStore.allocFragmentWrapper(
					buffer,
					encoder.length()
					);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				__PRETTY_FUNCTION__
				);
			return true;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of header memory.\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	composite->append(header);
	composite->append(fragment);

	_lowerLayerApi.send(composite);

	return false;
	}

uint16_t	Part::getChannelID() const noexcept{
	return _channel;
	}

void	Part::receive(
			const void* packet,
			unsigned	length
			) noexcept{
	}

