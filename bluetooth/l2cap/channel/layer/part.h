/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_l2cap_channel_layer_parth_
#define _oscl_bluetooth_l2cap_channel_layer_parth_

#include "oscl/bluetooth/l2cap/channel/tx/api.h"
#include "oscl/bluetooth/l2cap/channel/rx/itemcomp.h"
#include "oscl/bluetooth/l2cap/tx/api.h"
#include "oscl/pdu/memory/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace L2CAP {
/** */
namespace Channel {
/** */
namespace Layer {

/** */
class Part : public Oscl::BT::L2CAP::Channel::TX::Api {
	public:
		/* */
		using Oscl::BT::L2CAP::Channel::TX::Api::send;

	private:
		/** */
		Oscl::BT::L2CAP::Channel::RX::Api&	_upperLayerApi;

		/** */
		Oscl::BT::L2CAP::TX::Api&			_lowerLayerApi;
	
		/** */
		Oscl::Pdu::Memory::Api&				_freeStore;

		/** */
		uint16_t	_channel;

	private:
		/** */
		Oscl::BT::L2CAP::Channel::
		RX::ItemComposer<Part>				_receiver;

	public:
		/** */
		Part(
			Oscl::BT::L2CAP::Channel::RX::Api&	upperLayerApi,
			Oscl::BT::L2CAP::TX::Api&			lowerLayerApi,
			Oscl::Pdu::Memory::Api&				freeStore,
			uint16_t							channel
			) noexcept;

		/** */
		Oscl::BT::L2CAP::Channel::RX::Item&		getRxItem() noexcept;

	public: // Oscl::BT::L2CAP::Channel::TX::Api
		/** */
		bool	send(Oscl::Pdu::Pdu* pdu) noexcept;

	public: // Oscl::BT::L2CAP::Channel::RX::ItemComposer

		/** */
		uint16_t	getChannelID() const noexcept;

		/** */
		void	receive(
					const void* packet,
					unsigned	length
					) noexcept;
	};

}
}
}
}
}

#endif
