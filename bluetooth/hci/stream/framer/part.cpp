/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
//#include <string.h>

#include "part.h"

#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

using namespace Oscl::BT::HCI::Stream::Framer;

Part::Part(
	Oscl::Frame::FWD::Api&		frameRxApi,
	Oscl::BT::HCI::Reset::Api&	hciResetApi,
	Oscl::Timer::Api&			hciFramerTimerApi
	) noexcept:
		_frameRxApi(frameRxApi),
		_hciResetApi(hciResetApi),
		_hciFramerTimerApi(hciFramerTimerApi),
		_rxFSM(*this),
		_hciFramerTimerExpired(
			*this,
			&Part::hciFramerTimerExpired
			),
		_currentOffset(0),
		_currentLength(0),
		_currentOctet(0),
		_prevOctet(0),
		_currentPacketType(0),
		_overflow(false)
		{
	_hciFramerTimerApi.setExpirationCallback(
		_hciFramerTimerExpired
		);
	}

void	Part::start() noexcept{
	_rxFSM.start();
	}

void	Part::input(const void* buffer,unsigned length) noexcept{
	const uint8_t*	p	= (const uint8_t*)buffer;

	for(unsigned i=0;i<length;++i){
		_prevOctet		= _currentOctet;
		_currentOctet	= p[i];
		_rxFSM.input();
		}
	}

bool	Part::isValidPacketType() noexcept{

	switch(_currentOctet){
		case 0x02:
			// HCI_ACLDATA_PKT
			break;
		case 0x03:
			// HCI_SCODATA_PKT
			break;
		case 0x04:
			// HCI_EVENT_PKT
			break;
		default:
			Oscl::Error::Info::log(
				"%s: INVALID\n",
				OSCL_PRETTY_FUNCTION
				);
			return false;
		}

	return true;
	}

bool	Part::isValidLength() noexcept{
	return (_currentOctet < 255)?true:false;
	}

bool	Part::isLengthZero() noexcept{
	return (_currentLength)?false:true;
	}

void	Part::sendReset() noexcept{
	_hciResetApi.sendReset();
	}

void	Part::restartTimer() noexcept{
	// The timer value is not particularly
	// critical. We need to know if we are
	// not receiving any responses from the
	// HCI serial port.
	// A single octet at 115.2Kbps requires
	// about 70 us to serialize.
	// The posix read may buffer many octets
	// before returning.
	// If we assume that a frame will be a
	// maximum of 2^16 octets, then we will
	// need no more than 4.588 s (4588 ms).
	// That, however, seems a bit extreme.
	// I may want to revisit this number.
	
	constexpr unsigned long	delayInMs	= 4588;

	_hciFramerTimerApi.restart(delayInMs);
	}

void	Part::savePacketType() noexcept{
	_currentPacketType	= _currentOctet;
	}

void	Part::initHeaderLength() noexcept{
	switch(_currentPacketType){
		case 0x02:
			// HCI_ACLDATA_PKT
		case 0x03:
			// HCI_SCODATA_PKT
			_currentLength	= 2;
			break;
		case 0x04:
			// HCI_EVENT_PKT
			_currentLength	= 1;
			break;
		default:
			Oscl::ErrorFatal::logAndExit(
				"%s: Invalid packet type (%u)\n",
				OSCL_PRETTY_FUNCTION,
				_currentPacketType
				);
		}
	}

void	Part::initLenLength() noexcept{
	switch(_currentPacketType){
		case 0x02:
			// HCI_ACLDATA_PKT
			_currentLength	= 2;
			break;
		case 0x03:
			// HCI_SCODATA_PKT
		case 0x04:
			// HCI_EVENT_PKT
			_currentLength	= 1;
			break;
		default:
			Oscl::ErrorFatal::logAndExit(
				"%s: Invalid packet type (%u)\n",
				OSCL_PRETTY_FUNCTION,
				_currentPacketType
				);
		}
	}

void	Part::saveLength() noexcept{
	switch(_currentPacketType){
		case 0x02:
			// HCI_ACLDATA_PKT
			// Little Endian
			_currentLength	= _currentOctet;
			_currentLength	<<= 8;
			_currentLength	|= _prevOctet;
			break;
		case 0x03:
			// HCI_SCODATA_PKT
		case 0x04:
			// HCI_EVENT_PKT
			_currentLength	= _currentOctet;
			break;
		default:
			Oscl::ErrorFatal::logAndExit(
				"%s: Invalid packet type (%u)\n",
				OSCL_PRETTY_FUNCTION,
				_currentPacketType
				);
		}
	}

void	Part::decrementLength() noexcept{
	--_currentLength;
	}

void	Part::openFrame() noexcept{
	_currentOffset	= 0;
	_overflow		= false;
	}

void	Part::appendToFrame() noexcept{

	if(_currentOffset >= (sizeof(_rxBuffer)-1)){
		Oscl::Error::Info::log(
			"%s: overflow\n",
			OSCL_PRETTY_FUNCTION
			);
		_overflow	= true;
		return;
		}

	_rxBuffer[_currentOffset]	= _currentOctet;
	++_currentOffset;
	}

void	Part::closeFrame() noexcept{
	if(_overflow){
		Oscl::Error::Info::log(
			"%s: overflow\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_frameRxApi.forward(_rxBuffer,_currentOffset);
	}

void	Part::abortFrame() noexcept{
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	}

void	Part::hciFramerTimerExpired() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	_rxFSM.timerExpired();
	}

