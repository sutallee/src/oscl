/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_hci_stream_framer_parth_
#define _oscl_bluetooth_hci_stream_framer_parth_

#include <stdint.h>
#include "oscl/stream/framer/rx/api.h"
#include "oscl/bluetooth/hci/reset/api.h"
#include "oscl/bluetooth/hci/uart/fsm.h"
#include "oscl/frame/fwd/api.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace HCI {
/** */
namespace Stream {
/** */
namespace Framer {

/** */
class Part :
	public Oscl::Stream::Framer::RX::Api,
	private Oscl::Bluetooth::HCI::UART::StateVar::ContextApi
	{
	private:
		/** */
		Oscl::Frame::FWD::Api&		_frameRxApi;

		/** */
		Oscl::BT::HCI::Reset::Api&	_hciResetApi;

		/** */
		Oscl::Timer::Api&			_hciFramerTimerApi;

	private:
		/** */
		Oscl::Bluetooth::HCI::UART::StateVar	_rxFSM;

		/** */
		Oscl::Done::Operation<Part>				_hciFramerTimerExpired;

	private:
		/** */
		uint8_t									_rxBuffer[256];

		/** */
		unsigned								_currentOffset;

		/** */
		uint16_t								_currentLength;

		/** */
		uint8_t									_currentOctet;

		/** */
		uint8_t									_prevOctet;

		/** */
		uint8_t									_currentPacketType;

		/** */
		bool									_overflow;

	public:
		/** */
		Part(
			Oscl::Frame::FWD::Api&		frameRxApi,
			Oscl::BT::HCI::Reset::Api&	hciResetApi,
			Oscl::Timer::Api&			hciFramerTimerApi
			) noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::Stream::Framer::RX::Api
		/** */
		void	input(const void* buffer,unsigned length) noexcept;

	private: // Oscl::Bluetooth::HCI::UART::StateVar::ContextApi
		/**	This guard returns true if the current octet is
			a valid HCI packet type.
		 */
		bool	isValidPacketType() noexcept;

		/**	This guard returns true if the current octet is
			a valid packet length value. Obviously, it cannot
			be greater than 255, but the implementation may
			only allow values that are less than some value
			that is less than 255.
		 */
		bool	isValidLength() noexcept;

		/**	This guard returns true if the value of the
			current length counter is zero. This is used by
			the FSM to delimit the end-of-frame.
		 */
		bool	isLengthZero() noexcept;

		/**	The implementation is expected to begin the
			process of restarting the HCI synchronization
			process. It is expected that the implementation
			will at *least* send an HCI RESET command and
			perhaps other HCI commands, which will result in
			one or more HCI packets to be received.
		 */
		void	sendReset() noexcept;

		/**	The implementation is expected to start a
			packet timer if it is not already running. If it
			is already running, when the timer expires, it
			is expected that the implementation will
			automatically restart the timer without generating
			a timerExpired event. If the timer expires without
			having received this restartTimer action, then
			it must result in a timerExpired event.
		 */
		void	restartTimer() noexcept;

		/**	The implementation is expected to copy the
			value of the current octet into a packet-type
			variable such that it can be referenced by
			initHeaderLength(), initLenLength(), and
			saveLength().
		 */
		void	savePacketType() noexcept;

		/**	Based on the saved packet type, the
			implementation must initialize the length counter
			variable to the number of expected octets remaining
			before the length field such that it can be used
			with the decrementLength() action and
			isLengthZero() guard.
		 */
		void	initHeaderLength() noexcept;

		/**	Based on the saved packet type, the
			implementation must initialize the length counter
			variable to the number of expected octets contained
			in the length field such that it can be used with
			the decrementLength() action and isLengthZero()
			guard.
		 */
		void	initLenLength() noexcept;

		/**	Based on the saved packet type, the
			implementation must initialize the length counter
			variable to the length of the payload such that
			it can be used with the decrementLength() action
			and isLengthZero() guard while the payload is being
			input.
		 */
		void	saveLength() noexcept;

		/**	The implementation is expected to decrement the
			value of its packet length variable.
		 */
		void	decrementLength() noexcept;

		/**	The implementation is expected to start a
			allocate a new frame buffer when this is received
			in preparation for that buffer being sequentially
			filled with octets by the appendToFrame() action.
		 */
		void	openFrame() noexcept;

		/**	The implementation is expected to append the
			current octet to the open frame. The implementation
			should silently discard the octet if the frame
			buffer is full and internally note that an overflow
			has happened such that the frame will be discarded
			when the closeFrame() action is received.
		 */
		void	appendToFrame() noexcept;

		/**	The implementation is expected to either
			forward the current frame buffer to the next layer
			unless it overflowed during an appendToFrame()
			action, in which case the frame should be
			discarded.
		 */
		void	closeFrame() noexcept;

		/**	The implementation is expected to discard the
			current frame.
		 */
		void	abortFrame() noexcept;

	private:	// Oscl::Done::Operation _hciFramerTimerExpired
		/** */
		void	hciFramerTimerExpired() noexcept;
	};

}
}
}
}
}

#endif
