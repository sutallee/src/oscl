/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_hci_stream_tx_parth_
#define _oscl_bt_hci_stream_tx_parth_

#include <stdint.h>
#include "oscl/done/operation.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/stream/write/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace HCI {
/** */
namespace Stream {
/** */
namespace TX {

/** */
class Part {
	private:
		/** */
		bool							_logTxPackets;

	private:
		/** */
		Oscl::Stream::Write::Api*		_streamWriteApi;

	private:
		/** */
		Oscl::Done::Operation<Part>		_txDone;

	private:
		/** */
		Oscl::Pdu::FWD::Composer<Part>	_hciTxComposer;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_txQ;

	private:
		/** */
		uint8_t							_txBuffer[1024];

		/** */
		unsigned						_len;

	private:
		/** */
		bool							_txBusy;

	public:
		/** */
		Part(
			bool						logTxPackets
			) noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getHciTxApi() noexcept;

		/** */
		Oscl::Done::Api&	getStreamWriteDoneApi() noexcept;

		/** */
		void	start(Oscl::Stream::Write::Api* streamWriteApi) noexcept;

	public:
		/** */
		void	dumpLast() noexcept;

		/** */
		void	logging(bool enable) noexcept;

	private:	// Oscl::Done::Operation _hciCmdComplete
		/** */
		void	hciCmdComplete() noexcept;

	private:
		/** */
		void	txDone() noexcept;

	private:
		/** */
		void	hciTx(Oscl::Pdu::Pdu* pdu) noexcept;

		/** */
		void	processTxQ() noexcept;
	};

}
}
}
}
}

#endif
