/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/debug/print/parse.h"

using namespace Oscl::BT::HCI::Stream::TX;

Part::Part(
	bool	logTxPackets
	) noexcept:
		_logTxPackets(logTxPackets),
		_streamWriteApi(0),
		_txDone(
			*this,
			&Part::txDone
			),
		_hciTxComposer(
			*this,
			&Part::hciTx
			),
		_len(0),
		_txBusy(false)
		{
	}

Oscl::Pdu::FWD::Api&	Part::getHciTxApi() noexcept{
	return _hciTxComposer;
	}

Oscl::Done::Api&	Part::getStreamWriteDoneApi() noexcept{
	return _txDone;
	}

void	Part::start(Oscl::Stream::Write::Api* streamWriteApi) noexcept{
	_streamWriteApi	= streamWriteApi;
	}

void	Part::txDone() noexcept{
	_txBusy	= false;

	processTxQ();
	}

void	Part::hciTx(Oscl::Pdu::Pdu* pdu) noexcept{
	_txQ.put(pdu);

	processTxQ();
	}

void	Part::processTxQ() noexcept{

	if(_txBusy){
		return;
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= _txQ.get();

	if(!handle){
		return;
		}

	_len	= handle->read(
				_txBuffer,
				sizeof(_txBuffer),
				0
				);

	bool
	busy	= _streamWriteApi->write(_txBuffer,_len);

	if(busy){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unexpected TX BUSY!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(_logTxPackets){
		Oscl::Bluetooth::Debug::Print::parseHciTxPacket(_txBuffer,_len);
		}

	_txBusy	= true;
	}

void	Part::dumpLast() noexcept{

	Oscl::Error::Info::log(
		"%s: _txBusy: %s, _txQ: %s\n",
		OSCL_PRETTY_FUNCTION,
		_txBusy?"true":"false",
		_txQ.first()?"NOT empty":"empty"
		);

	if(_len){
		Oscl::Bluetooth::Debug::Print::parseHciTxPacket(_txBuffer,_len);
		}

	_len	= 0;
	}

void	Part::logging(bool enable) noexcept{
	_logTxPackets	= enable;
	}

