/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
using namespace Oscl::Bluetooth::HCI::UART;

//--------------- Procedures ----------------
/**	This procedure executes a common set of actions
	and transitions to begin the synchronization
	process.
 */
void	StateVar::startReset(ContextApi& context) noexcept{
	context.sendReset();
	context.restartTimer();
	changeToHUNT();
	}

/**	This procedure executes a common set actions
	and transitions to begin receiving a packet after
	a valid packet type is received.
 */
void	StateVar::startPacket(ContextApi& context) noexcept{
	context.openFrame();
	context.appendToFrame();
	context.savePacketType();
	context.initHeaderLength();
	context.restartTimer();
	changeToHeader();
	}

//--------------- State ----------------
//--------------- INIT ----------------
StateVar::INIT::INIT() noexcept
		{
	}
void	StateVar::INIT::start(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.startReset(context);
	}
//--------------- HUNT ----------------
StateVar::HUNT::HUNT() noexcept
		{
	}

void	StateVar::HUNT::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	if(!context.isValidPacketType()){
		return;
		}

	stateVar.startPacket(context);
	}

void	StateVar::HUNT::timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.startReset(context);
	}
//--------------- Header ----------------
StateVar::Header::Header() noexcept
		{
	}
void	StateVar::Header::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	context.restartTimer();
	context.appendToFrame();
	context.decrementLength();
	if(!context.isLengthZero()){
		return;
		}

	context.initLenLength();
	stateVar.changeToLength();
	}

void	StateVar::Header::timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.startReset(context);
	}

//--------------- Length ----------------
StateVar::Length::Length() noexcept
		{
	}
void	StateVar::Length::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	context.restartTimer();
	context.appendToFrame();
	context.decrementLength();

	if(!context.isLengthZero()){
		return;
		}

	context.saveLength();

	if(!context.isValidLength()){
		context.abortFrame();
		stateVar.changeToHUNT();
		return;
		}

	if(context.isLengthZero()){
		context.closeFrame();
		stateVar.changeToSyncHunt();
		return;
		}

	stateVar.changeToPayload();
	}

void	StateVar::Length::timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.startReset(context);
	}

//--------------- Payload ----------------
StateVar::Payload::Payload() noexcept
		{
	}
void	StateVar::Payload::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	context.restartTimer();
	context.appendToFrame();
	context.decrementLength();

	if(!context.isLengthZero()){
		return;
		}

	context.closeFrame();
	stateVar.changeToSyncHunt();
	}

void	StateVar::Payload::timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.startReset(context);
	}

//--------------- SyncHunt ----------------
StateVar::SyncHunt::SyncHunt() noexcept
		{
	}

void	StateVar::SyncHunt::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	if(context.isValidPacketType()){
		stateVar.startPacket(context);
		}
	else {
		stateVar.startReset(context);
		}
	}

void	StateVar::SyncHunt::timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	// do nothing
	}
