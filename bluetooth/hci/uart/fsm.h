/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_hci_uart_fsmh_
#define _oscl_bluetooth_hci_uart_fsmh_

/** */
namespace Oscl {

/** */
namespace Bluetooth {

/** */
namespace HCI {

/** */
namespace UART {

/**	This StateVar class implements a Finite State Machine
	(FSM) by applying a refinement of the GOF State Pattern,
	which is relized by applying another GOF pattern named the
	Flyweight Pattern. The Flyweight Pattern is used in the
	implementation of the state subclasses allowing a single
	set of concrete state classes to be shared between any number
	of instances of this FSM.
 */
/**	This FSM implements the behavior of a RX framer for
	Bluetooth HCI interface over a UART.
 */
class StateVar {
	public:
		/**	This interface is implemented by the context whose behavior
			is controlled by the FSM implemented by this StateVar.
		 */
		class ContextApi {
			public:
				/** Make the compiler happy. */
				virtual ~ContextApi() {}
				/**	This guard returns true if the current octet is
					a valid HCI packet type.
				 */
				virtual bool	isValidPacketType() noexcept=0;

				/**	This guard returns true if the current octet is
					a valid packet length value. Obviously, it cannot
					be greater than 255, but the implementation may
					only allow values that are less than some value
					that is less than 255.
				 */
				virtual bool	isValidLength() noexcept=0;

				/**	This guard returns true if the current value of
					the length counter variable is zero. This is used
					by the FSM to delimit the end of a field
					(header,length,payload).
				 */
				virtual bool	isLengthZero() noexcept=0;

				/**	The implementation is expected to begin the
					process of restarting the HCI synchronization
					process. It is expected that the implementation
					will at *least* send an HCI RESET command and
					perhaps other HCI commands, which will result in
					one or more HCI packets to be received.
				 */
				virtual void	sendReset() noexcept=0;

				/**	The implementation is expected to start a
					packet timer if it is not already running. If it
					is already running, when the timer expires, it
					is expected that the implementation will
					automatically restart the timer without generating
					a timerExpired event. If the timer expires without
					having received this restartTimer action, then
					it must result in a timerExpired event.
				 */
				virtual void	restartTimer() noexcept=0;

				/**	The implementation is expected to copy the
					value of the current octet into a packet-type
					variable such that it can be referenced by
					initHeaderLength(), initLenLength(), and
					saveLength().
				 */
				virtual void	savePacketType() noexcept=0;

				/**	Based on the saved packet type, the
					implementation must initialize the length counter
					variable to the number of expected octets remaining
					before the length field such that it can be used
					with the decrementLength() action and
					isLengthZero() guard.
				 */
				virtual void	initHeaderLength() noexcept=0;

				/**	Based on the saved packet type, the
					implementation must initialize the length counter
					variable to the number of expected octets contained
					in the length field such that it can be used with
					the decrementLength() action and isLengthZero()
					guard.
				 */
				virtual void	initLenLength() noexcept=0;

				/**	Based on the saved packet type, the
					implementation must initialize the length counter
					variable to the length of the payload such that
					it can be used with the decrementLength() action
					and isLengthZero() guard while the payload is being
					input.
				 */
				virtual void	saveLength() noexcept=0;

				/**	The implementation is expected to decrement the
					value of its packet length variable.
				 */
				virtual void	decrementLength() noexcept=0;

				/**	The implementation is expected to start a
					allocate a new frame buffer when this is received
					in preparation for that buffer being sequentially
					filled with octets by the appendToFrame() action.
				 */
				virtual void	openFrame() noexcept=0;

				/**	The implementation is expected to append the
					current octet to the open frame. The implementation
					should silently discard the octet if the frame
					buffer is full and internally note that an overflow
					has happened such that the frame will be discarded
					when the closeFrame() action is received.
				 */
				virtual void	appendToFrame() noexcept=0;

				/**	The implementation is expected to either
					forward the current frame buffer to the next layer
					unless it overflowed during an appendToFrame()
					action, in which case the frame should be
					discarded.
				 */
				virtual void	closeFrame() noexcept=0;

				/**	The implementation is expected to discard the
					current frame.
				 */
				virtual void	abortFrame() noexcept=0;

			};
	private:
		/** */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}
				/**	When invoked by the context, the FSM
					is requested to begin operation.
				 */
				virtual void	start(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	
				 */
				virtual void	input(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	
				 */
				virtual void	timerExpired(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

			};
		/** */
		friend class State;

	private:
		/**	This concrete class represents the INIT state of the FSM.
		 */
		class INIT : public State {
			public:
				/** The cosntructor.
				 */
				INIT() noexcept;

			public:
				/**	When invoked by the context, the FSM is requested to
					begin operation.
				 */
				void	start(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class INIT;

	private:
		/**	
		 */
		class HUNT : public State {
			public:
				/** The cosntructor.
				 */
				HUNT() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	
				 */
				void	timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class HUNT;

	private:
		/**	
		 */
		class Header : public State {
			public:
				/** The cosntructor.
				 */
				Header() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	
				 */
				void	timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Header;

	private:
		/**	
		 */
		class Length : public State {
			public:
				/** The cosntructor.
				 */
				Length() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	
				 */
				void	timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Length;

	private:
		/**	
		 */
		class Payload : public State {
			public:
				/** The cosntructor.
				 */
				Payload() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	
				 */
				void	timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Payload;

	private:
		/**	
		 */
		class SyncHunt : public State {
			public:
				/** The cosntructor.
				 */
				SyncHunt() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	
				 */
				void	timerExpired(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class SyncHunt;

	private:
		/** This is the state-less shared instance (fly-weight)
			of the INIT state.
		 */
		const static INIT	_INIT;

		/** This is the state-less shared instance (fly-weight)
			of the HUNT state.
		 */
		const static HUNT	_HUNT;

		/** This is the state-less shared instance (fly-weight)
			of the Header state.
		 */
		const static Header	_Header;

		/** This is the state-less shared instance (fly-weight)
			of the Length state.
		 */
		const static Length	_Length;

		/** This is the state-less shared instance (fly-weight)
			of the Payload state.
		 */
		const static Payload	_Payload;

		/** This is the state-less shared instance (fly-weight)
			of the SyncHunt state.
		 */
		const static SyncHunt	_SyncHunt;

	private:
		/** This member references the context.
		 */
		ContextApi&		_context;

		/** This member determines the current state of the FSM.
		 */
		const State*	_state;

	public:
		/** The constructor requires a reference to its context.
		 */
		StateVar(ContextApi& context) noexcept;

		/** Be happy compiler! */
		virtual ~StateVar() {}

	public:
		/**	When invoked by the context, the FSM is requested to
			begin operation.
		 */
		void	start() noexcept;

		/**	
		 */
		void	input() noexcept;

		/**	
		 */
		void	timerExpired() noexcept;

	private:
		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the INIT state.
		 */
		void changeToINIT() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the HUNT state.
		 */
		void changeToHUNT() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Header state.
		 */
		void changeToHeader() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Length state.
		 */
		void changeToLength() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Payload state.
		 */
		void changeToPayload() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the SyncHunt state.
		 */
		void changeToSyncHunt() noexcept;

	private:
		/**	This procedure executes a common set of actions
			and transitions to begin the synchronization
			process.
		 */
		void	startReset(ContextApi& context) noexcept;

		/**	This procedure executes a common set actions
			and transitions to begin receiving a packet after
			a valid packet type is received.
		 */
		void	startPacket(ContextApi& context) noexcept;

	private:
		/** This operation is invoked when the FSM detects
			a protocol violation.
		 */
		void	protocolError() noexcept;
	};
}
}
}
}
#endif
