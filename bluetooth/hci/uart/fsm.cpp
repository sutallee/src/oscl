/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
using namespace Oscl::Bluetooth::HCI::UART;

//--------------- Concrete Fly-Weight States ----------------
const StateVar::INIT	StateVar::_INIT;
const StateVar::HUNT	StateVar::_HUNT;
const StateVar::Header	StateVar::_Header;
const StateVar::Length	StateVar::_Length;
const StateVar::Payload	StateVar::_Payload;
const StateVar::SyncHunt	StateVar::_SyncHunt;

//--------------- StateVar constructor ----------------

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&_INIT)
		{
	}

//--------------- StateVar events ----------------
void	StateVar::start() noexcept{
	_state->start(_context,*this);
	}

void	StateVar::input() noexcept{
	_state->input(_context,*this);
	}

void	StateVar::timerExpired() noexcept{
	_state->timerExpired(_context,*this);
	}

//--------------- StateVar state-change operations ----------------
void StateVar::changeToINIT() noexcept{
	_state	= &_INIT;
	}

void StateVar::changeToHUNT() noexcept{
	_state	= &_HUNT;
	}

void StateVar::changeToHeader() noexcept{
	_state	= &_Header;
	}

void StateVar::changeToLength() noexcept{
	_state	= &_Length;
	}

void StateVar::changeToPayload() noexcept{
	_state	= &_Payload;
	}

void StateVar::changeToSyncHunt() noexcept{
	_state	= &_SyncHunt;
	}

//--------------- State ----------------
void	StateVar::State::start(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{
	stateVar.protocolError();
	}

void	StateVar::State::input(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{
	stateVar.protocolError();
	}

void	StateVar::State::timerExpired(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{
	stateVar.protocolError();
	}

