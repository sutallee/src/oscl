/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_bluetooth_hci_base_parth_
#define _oscl_bluetooth_hci_base_parth_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/hci/stream/framer/part.h"
#include "oscl/bluetooth/hci/stream/tx/part.h"
#include "oscl/frame/fwd/item.h"
#include "oscl/bluetooth/mesh/conn/prov/part.h"
#include "oscl/bluetooth/mesh/conn/proxy/part.h"
#include "oscl/endian/decoder/api.h"
#include "oscl/pdu/fwd/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/itemcomp.h"
#include "oscl/timer/api.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/print/api.h"
#include "oscl/frame/fwd/host/composer.h"
#include "oscl/inhibitor/composer.h"

#include "oscl/frame/rf/rx/item.h"
#include "oscl/frame/rf/rx/host/composer.h"
#include "oscl/bluetooth/l2cap/tx/api.h"
#include "oscl/bluetooth/conn/tx/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace HCI {
/** */
namespace Base {

/** */
class Part :
	private Oscl::BT::HCI::Reset::Api,
	private Oscl::BT::Mesh::Conn::Prov::Part::ContextApi,
	private Oscl::BT::Mesh::Conn::Proxy::Part::ContextApi,
	private Oscl::BT::L2CAP::TX::Api,
	private Oscl::BT::Conn::TX::Api
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	setAttentionTimer(uint8_t nSeconds) noexcept=0;

				/** */
				virtual bool	oobOutputActionBlinkIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionBeepIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionVibrateIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionNumericIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionAlphaNumericIsSupported() const noexcept=0;

				/** */
				virtual void	oobOutputBlink(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputBeep(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputVibrate(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputNumeric(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputAlphaNumeric(const char* s) noexcept=0;

				/** */
				virtual void	provisioningStopped() noexcept=0;

				/** */
				virtual bool	isUnprovisioned() noexcept=0;

				/** The implementation is expected to encode the vendor-specific
					HCI command to set the board address starting with the
					OGF/OCF opcode field.
				 */
				virtual void	encodeBoardAddressHciCommand(Oscl::Encoder::Api& encoder) noexcept=0;
			};

	private:
		/** */
		ContextApi&					_context;

		/** */
		Oscl::BT::Mesh::Bearer::
		TX::Host::Api&				_bearerHostApi;

		/** */
		Oscl::BT::Mesh::
		Node::Creator::Api&			_nodeCreateApi;

		/** */
		Oscl::BT::Mesh::
	    Network::Iteration::Api&	_networkIterator;

		/** */
		Oscl::Frame::FWD::Api&		_provRxApi;

		/** */
		Oscl::Timer::Api&			_hciFramerTimerApi;

		/** */
		Oscl::Timer::Api&			_advTxTimerApi;

		/** */
		Oscl::Timer::Api&			_disconnectTimerApi;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::Pdu::FWD::Api&		_hciTxApi;

		/** */
		const uint8_t*	const		_boardAddress;

		/** */
		const char*	const			_deviceName;

		/** */
		bool						_logTxPackets;

		/** */
		bool						_logRxPackets;

		/** */
		const uint16_t				_attMTU;

		/** */
		const bool					_oobAvailable;

		/** */
		const uint8_t				_outputOobSizeInCharacters;

		/** */
		const bool					_alwaysOnDevice;

	private:
		/** */
		Oscl::Frame::RF::RX::
		Host::Composer<Part>		_meshMessageRxHostCommposer;
		
		/** */
		Oscl::Frame::FWD::
		Host::Composer<Part>		_secureNetworkBeaconRxHostComposer;
		
	private:
		/** */
		Oscl::BT::Mesh::Bearer::
		TX::ItemComposer<Part>		_netInterfaceComposer;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>		_hciCmdQ;

		/** */
		bool								_txBusy;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>		_hciAclTxQ;

		/** */
		uint16_t							_dataPacketLength;

		/** */
		uint8_t								_numLeDataPackets;

		/** */
		uint8_t								_numPendingLeDataPackets;

		/** */
		bool								_disconnectPending;

		/** */
		uint16_t							_disconnectHandle;

	private:
		/** */
		constexpr static unsigned	nL2CapConnections	= 1;

		/** */
		union L2CAPMem {
			/** */
			void*	__qitemlink;

			union {
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Conn::Prov::Part
						)
					>						prov;
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Conn::Proxy::Part
						)
					>						proxy;
				} mem;
			};

		/** */
		L2CAPMem	_l2capMem[nL2CapConnections];

		/** */
		Oscl::Queue<L2CAPMem>	_freeL2capMem;

	private:
		/** */
		Oscl::Done::Operation<Part>			_advTxTimerExpired;

		/** */
		Oscl::Done::Operation<Part>			_disconnectTimerExpired;

	private:
		/** */
		Oscl::Frame::FWD::Composer<Part>		_frameReceiver;

		/** */
		Oscl::Frame::FWD::Composer<Part>		_hciEventReceiver;

		/** */
		Oscl::Frame::FWD::Composer<Part>		_hciCmdCompleteReceiver;

		/** */
		Oscl::Frame::FWD::Composer<Part>		_hciLeMetaReceiver;

	private:
		/** */
		Oscl::Pdu::FWD::Composer<Part>			_advBearerTxProvComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>			_advBearerTxBeaconComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>			_advBearerTxMeshComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>			_advGattBearerTxComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>			_hciCmdTxPduComposer;

	private:
		/** */
		Oscl::Done::Operation<Part>				_allFreeNotification;

	private:
		/** */
		constexpr static unsigned	nBuffers	= 32;
		/** */
		constexpr static unsigned	nFixed		= 32;
		/** */
		constexpr static unsigned	nFragment	= 2*32;
		/** */
		constexpr static unsigned	nComposite	= 2*32;
		/** */
		constexpr static unsigned	bufferSize	= 32;

		/** */
		Oscl::Pdu::Memory::
		Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

		/** */
		typedef Oscl::Pdu::Memory::Config<
					bufferSize,
					nBuffers,
					nFixed,
					nFragment,
					nComposite
					>::BufferMem	BufferMem;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_advNonConnectableQ;

		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_advConnectableQ;

	private:
		/** */
		Oscl::Queue<Oscl::Frame::RF::RX::Item>	_meshMessageReceivers;

		/** */
		Oscl::Queue<Oscl::Frame::FWD::Item>	_secureNetworkBeaconReceivers;

	private:
		/** */
		Oscl::Queue<Oscl::Frame::FWD::Item>	_l2capConnections;

		/** */
		Oscl::Queue<Oscl::BT::Mesh::Conn::Prov::Part>	_activeProvConns;

		/** */
		Oscl::Queue<Oscl::BT::Mesh::Conn::Proxy::Part>	_activeProxyConns;

		/** */
		Oscl::Queue<Oscl::BT::Conn::RX::Item>	_activeConnections;

	private:
		/** */
		uint8_t									_txBuffer[1024];

	private:
		/** */
		bool									_advEnabled;

		/** */
		bool									_scanningEnabled;

		/** */
		uint8_t									_leType;

		/** */
		int8_t									_rssi;

	private:
		/** */
		enum{
			Invalid,
			Connectable,
			Unconnectable
			}									_advParams;

		/** */
		bool									_nonConnectableActive;

	private:
		/** */
		unsigned								_hciRxCount;

		/** */
		unsigned								_hciTxCount;

	private:
		/** */
		Oscl::Inhibitor::Composer<Part>			_hostControllerDisableInhibitor;

		/**	When this counter is non-zero the transition
			from normal operation to host controller
			disabled is deferred.
		 */
        unsigned    _inhibitTransitionToHostControllerDisabled;

		/** This member is true if the receiver
			has been disabled indicating that the
			host controller is asleep to save
			power.
		 */
		bool									_hostControllerDisabled;

	public:
		/** */
		Part(
			ContextApi&					contextApi,
			Oscl::BT::Mesh::Bearer::
			TX::Host::Api&				bearerHostApi,
			Oscl::BT::Mesh::
			Node::Creator::Api&			nodeCreateApi,
			Oscl::BT::Mesh::
		    Network::Iteration::Api&	networkIterator,
			Oscl::Frame::FWD::Api&		provRxApi,
			Oscl::Timer::Api&			hciFramerTimerApi,
			Oscl::Timer::Api&			advTxTimerApi,
			Oscl::Timer::Api&			disconnectTimerApi,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			Oscl::Pdu::FWD::Api&		hciTxApi,
			const uint8_t				boardAddress[6],
			const char*					deviceName,
			bool						logTxPackets,
			bool						logRxPackets,
			uint16_t					attMTU,
			bool						oobAvailable,
			uint8_t						outputOobSizeInCharacters,
			bool						alwaysOnDevice
			) noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getProvTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getBeaconTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getMeshTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getAdvGattBearerApi() noexcept;

		/** */
		Oscl::Frame::RF::RX::Host::Api&	getMeshMessageRxHostApi() noexcept;

		/** */
		Oscl::Frame::FWD::Host::Api&	getSecureNetworkBeaconRxHostApi() noexcept;

		/** This interface is used by clients to inhibit
			the process of shutting down the host controller
			to conserve power. This is primarily used when
			we are a LPN.
		 */
		Oscl::Inhibitor::Api&	getHostControllerDisableInhibitApi() noexcept;

		/** */
		Oscl::Frame::FWD::Api&	getFrameRxApi() noexcept;

		/** */
		Oscl::BT::HCI::Reset::Api&	getResetApi() noexcept;

		/** */
		void	start() noexcept;

		/** */
		bool	gattConnectionIsActive() const noexcept;

		/** */
		bool	isDestinedForProxy(uint16_t address) noexcept;

		/** */
		void	networkSecurityChanged() noexcept;

	private:
		/** */
		void	attachMeshMessageReceiver(Oscl::Frame::RF::RX::Item& receiver) noexcept;

		/** */
		void	detachMeshMessageReceiver(Oscl::Frame::RF::RX::Item& receiver) noexcept;

		/** */
		void	attachSecureNetworkBeaconReceiver(Oscl::Frame::FWD::Item& receiver) noexcept;

		/** */
		void	detachSecureNetworkBeaconReceiver(Oscl::Frame::FWD::Item& receiver) noexcept;

	public:
		/** */
		void	dumpStatus(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	hciTickle(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	disconnectL2CAP(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	logging(bool enable) noexcept;

	private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private: //Oscl::BT::Mesh::Conn::Proxy::Part::ContextApi
		/** */
		void	stopped(Oscl::BT::Mesh::Conn::Proxy::Part& connection) noexcept;

	private: // Oscl::BT::Mesh::Conn::Prov::Part::ContextApi
		/** */
		void	stopped(Oscl::BT::Mesh::Conn::Prov::Part& connection) noexcept;

		/** */
		void	setAttentionTimer(uint8_t nSeconds) noexcept;

		/** */
		bool	oobOutputActionBlinkIsSupported() const noexcept;

		/** */
		bool	oobOutputActionBeepIsSupported() const noexcept;

		/** */
		bool	oobOutputActionVibrateIsSupported() const noexcept;

		/** */
		bool	oobOutputActionNumericIsSupported() const noexcept;

		/** */
		bool	oobOutputActionAlphaNumericIsSupported() const noexcept;

		/** */
		void	oobOutputBlink(uint32_t n) noexcept;

		/** */
		void	oobOutputBeep(uint32_t n) noexcept;

		/** */
		void	oobOutputVibrate(uint32_t n) noexcept;

		/** */
		void	oobOutputNumeric(uint32_t n) noexcept;

		/** */
		void	oobOutputAlphaNumeric(const char* s) noexcept;

		/** */
		void	provisioningStopped() noexcept;

		/** */
		void	disconnect(
					uint16_t	handle,
					bool		force
					) noexcept;

	private:
		/** */
		void	txDone() noexcept;

	private:	// Oscl::BT::HCI::Reset::Api
		/**	*/
		void	sendReset() noexcept;

	private:
		/** */
		void	sendHciReset() noexcept;

		/** */
		void	sendLeReadBufferSizeCommand() noexcept;

		/** */
		void	sendHciSetBoardAddress() noexcept;

		/** */
		void	sendWriteLeHostSupported() noexcept;

		/** */
		void	sendLeSetRandomAddr() noexcept;

		/** */
		void	sendEventMask() noexcept;

		/** */
		void	sendLeEventMask() noexcept;

		/** */
		void	sendLeSetAdvParametersConnectableScanable() noexcept;

		/** */
		void	sendLeSetAdvParametersNonConnectable() noexcept;

		/** */
		void	sendLeSetScanParameters() noexcept;

		/** RETURN: true for failure. */
		bool	sendLeSetScanEnable(bool enable) noexcept;

		/** */
		void	sendLeSetScanEnable() noexcept;

		/** */
		void	sendLeSetScanDisable() noexcept;

		/** */
		void	sendDisconnectCommand(
					uint16_t	handle,
					uint8_t		reason
					) noexcept;

		/** */
		void	sendReadLocalSupportedCommands() noexcept;

		/** */
		void	sendScanResponseData() noexcept;

	private:
		/** */
		void	sendHciCmdLeSetAdvEnable() noexcept;

		/** */
		void	sendHciCmdLeSetAdvDisable() noexcept;

		/** */
		void	sendHciCmdLeSetScanResponseData(Oscl::Pdu::Pdu* pdu) noexcept;

		/** */
		void	sendConnectableAdv() noexcept;

		/** */
		void	sendNonConnectableAdv() noexcept;

		/** */
		void	sendLeRemoteConnectionParameterReply(
					uint16_t	handle,
					uint16_t	intervalMin,
					uint16_t	intervalMax,
					uint16_t	latency,
					uint16_t	timeout,
					uint16_t	minimumCeLength,
					uint16_t	maximumCeLength
					) noexcept;

	private:

		/** */
		void	parseLeReadBufferSizeStatus(Oscl::Endian::Decoder::Api& decoder) noexcept;

		/** */
		bool	parseUnprovisionedDeviceBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseSecureNetworkBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseMeshMessage(
					const void*		frame,
					uint8_t 		length,
					int8_t			rssi
					) noexcept;

		/** */
		bool	parseMeshBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseEirFields(
					uint8_t			bdaddrType,
					const uint8_t	bdaddr[6],
					uint8_t 		length,
					const void*		frame
					) noexcept;

		/** */
		bool	parseLeAdvInd(
					uint8_t			bdaddrType,
					const uint8_t	bdaddr[6],
					uint8_t 		length,
					const void*		frame
					) noexcept;

		/** */
		bool	parseLeAdvNonConnInd(
					uint8_t			bdaddrType,
					const uint8_t	bdaddr[6],
					uint8_t 		length,
					const void*		frame
					) noexcept;

		/** */
		bool	parseAdvertisingReport(
					const void*		frame,
					unsigned& 		length
					) noexcept;

		/** */
		bool	parseLeAdvertisingReports(
					const void*		frame,
					unsigned		length
					) noexcept;

		/** */
		bool	parseLeRemoteConnectionParameterRequest(
					const void*		frame,
					unsigned		length
					) noexcept;

		/** */
		bool	parseLeConnectionComplete(
					const void*		frame,
					unsigned		length
					) noexcept;

		/** */
		bool	parseDisconnectionComplete(
					const void*		frame,
					unsigned		length
					) noexcept;

		/** */
		bool	parseNumberOfCompletedPackets(
					const void*		frame,
					unsigned 		length
					) noexcept;

		/** */
		void	parseCommandStatus(
					const void*		frame,
					unsigned 		length
					) noexcept;

		/** */
		bool	parseLeConnectionUpdateComplete(
					const void*		frame,
					unsigned 		length
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _frameReceiver
		/** */
		bool	rxForward(	
					const void*		frame,
					unsigned int	length
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _hciEventReceiver
		/** */
		bool	hciEventForward(	
					const void*		frame,
					unsigned int	length
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _hciCmdCompleteReceiver
		/** */
		bool	hciCmdCompleteForward(	
					const void*		frame,
					unsigned int	length
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _hciLeMetaReceiver
		/** */
		bool	hciLeMetaForward(	
					const void*		frame,
					unsigned int	length
					) noexcept;

	private:
		/** */
		void	hciTxConnectableAdvPdu(Oscl::Pdu::Pdu* pdu) noexcept;

		/** */
		void	hciTxNonConnectableAdvPdu(Oscl::Pdu::Pdu* pdu) noexcept;

	private:// Oscl::PDU::FWD::Composer	_advBearerTxProvComposer;
		/** */
		void	advBearerTxProv(Oscl::Pdu::Pdu* pdu) noexcept;

	private:// Oscl::PDU::FWD::Composer	_advBearerTxBeaconComposer;
		/** */
		void	advBearerTxBeacon(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::BT::Mesh::Bearer::TX::ItemComposer _netInterfaceComposer
		/** */
		void	advBearerTx(
					Oscl::Pdu::Pdu* pdu,
					uint16_t		dst
					) noexcept;

	private:// Oscl::PDU::FWD::Composer	_advBearerTxMeshComposer;
		/** */
		void	advBearerTxMesh(Oscl::Pdu::Pdu* pdu) noexcept;

	private:// Oscl::PDU::FWD::Composer	_hciCmdTxPduComposer;
		/** */
		void	hciCmdTxPdu(Oscl::Pdu::Pdu* pdu) noexcept;

	private:
		/** */
		void	processHciCmdTxQ() noexcept;

		/** */
		void	processAdvTxQ() noexcept;

	private:	// Oscl::Done::Operation _advTxTimerExpired
		/** */
		void	advTxTimerExpired() noexcept;

	private:	// Oscl::Done::Operation _disconnectTimerExpired
		/** */
		void	disconnectTimerExpired() noexcept;

	private:
		/** RETURN: true for failure. */
		bool	createNewL2CAPConnection(
					uint16_t	connection_handle
					) noexcept;

		/** RETURN: true for failure. */
		bool	destroyL2CAPConnection(
					uint16_t	connection_handle
					) noexcept;

		/** RETURN: true for failure. */
		bool	processAclRX(
					const void*	frame,
					unsigned 	length
					) noexcept;

		/** */
		void	flushAclTxQueue() noexcept;

		/** */
		void	processAclTxQueue() noexcept;

	private:
		/** */
		void	processHostControllerState() noexcept;

	private: // Oscl::Inhibitor::Composer _hostControllerDisableInhibitor
		/** */
		void	preventTransitionToHcDisable() noexcept;

		/** */
		void	allowTransitionToHciDisable() noexcept;

	private: // Oscl::BT::L2CAP::TX::Api
		/** */
		bool	send(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::BT::Conn::TX::Api
		/**
			pbFlag : Packet boundary flag
				b00	= First non-automatically-flushable packet
				b01	= Continuing ragment
				b10	= First automatically flushable packet
				b11	= A complete L2CAP PDU.
			bcFlag : Broadcast flag
				b00	= Point-to-point
				b01	= Active Slave Broadcast

			RETURN: true if failed resource allocation.
		 */
		bool	send(
					uint8_t			pbFlag,
					uint8_t			bcFlag,
					uint16_t		connHandle,
					uint16_t		dataTotalLength,
					Oscl::Pdu::Pdu*	pdu
					) noexcept;

		/** RETURN: maximum size of PDU supported
			by the controller.
		 */
		unsigned	maxPduLen() noexcept;		
	};

}
}
}
}

#endif
