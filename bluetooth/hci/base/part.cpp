/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/debug/print/parse.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/bluetooth/hci/error/codes.h"

using namespace Oscl::BT::HCI::Base;

//#define DEBUG_TRACE
//#define EVENT_TRACE

Part::Part(
	ContextApi&					context,
	Oscl::BT::Mesh::Bearer::
	TX::Host::Api&				bearerHostApi,
	Oscl::BT::Mesh::
	Node::Creator::Api&			nodeCreateApi,
	Oscl::BT::Mesh::
    Network::Iteration::Api&	networkIterator,
	Oscl::Frame::FWD::Api&		provRxApi,
	Oscl::Timer::Api&			hciFramerTimerApi,
	Oscl::Timer::Api&			advTxTimerApi,
	Oscl::Timer::Api&			disconnectTimerApi,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	Oscl::Pdu::FWD::Api&		hciTxApi,
	const uint8_t				boardAddress[6],
	const char*					deviceName,
	bool						logTxPackets,
	bool						logRxPackets,
	uint16_t					attMTU,
	bool						oobAvailable,
	uint8_t						outputOobSizeInCharacters,
	bool						alwaysOnDevice
	) noexcept:
		_context(context),
		_bearerHostApi(bearerHostApi),
		_nodeCreateApi(nodeCreateApi),
		_networkIterator(networkIterator),
		_provRxApi(provRxApi),
		_hciFramerTimerApi(hciFramerTimerApi),
		_advTxTimerApi(advTxTimerApi),
		_disconnectTimerApi(disconnectTimerApi),
		_timerFactoryApi(timerFactoryApi),
		_hciTxApi(hciTxApi),
		_boardAddress(boardAddress),
		_deviceName(deviceName),
		_logTxPackets(logTxPackets),
		_logRxPackets(logRxPackets),
		_attMTU(attMTU),
		_oobAvailable(oobAvailable),
		_outputOobSizeInCharacters(outputOobSizeInCharacters),
		_alwaysOnDevice(alwaysOnDevice),
		_meshMessageRxHostCommposer(
			*this,
			&Part::attachMeshMessageReceiver,
			&Part::detachMeshMessageReceiver
			),
		_secureNetworkBeaconRxHostComposer(
			*this,
			&Part::attachSecureNetworkBeaconReceiver,
			&Part::detachSecureNetworkBeaconReceiver
			),
		_netInterfaceComposer(
			*this,
			&Part::advBearerTx
			),
		_txBusy(false),
		_dataPacketLength(27),
		_numLeDataPackets(0),
		_numPendingLeDataPackets(0),
		_disconnectPending(false),
		_disconnectHandle(0),
		_advTxTimerExpired(
			*this,
			&Part::advTxTimerExpired
			),
		_disconnectTimerExpired(
			*this,
			&Part::disconnectTimerExpired
			),
		_frameReceiver(
			*this,
			&Part::rxForward
			),
		_hciEventReceiver(
			*this,
			&Part::hciEventForward
			),
		_hciCmdCompleteReceiver(
			*this,
			&Part::hciCmdCompleteForward
			),
		_hciLeMetaReceiver(
			*this,
			&Part::hciLeMetaForward
			),
		_advBearerTxProvComposer(
			*this,
			&Part::advBearerTxProv
			),
		_advBearerTxBeaconComposer(
			*this,
			&Part::advBearerTxBeacon
			),
		_advBearerTxMeshComposer(
			*this,
			&Part::advBearerTxMesh
			),
		_advGattBearerTxComposer(
			*this,
			&Part::hciTxConnectableAdvPdu
			),
		_hciCmdTxPduComposer(
			*this,
			&Part::hciCmdTxPdu
			),
		_allFreeNotification(
			*this,
			&Part::allFreeNotification
			),
		_freeStore(
			"HCI",
			&_allFreeNotification
			),
		_advEnabled(false),
		_scanningEnabled(false),
		_advParams(Invalid),
		_nonConnectableActive(false),
		_hciRxCount(0),
		_hciTxCount(0),
		_hostControllerDisableInhibitor(
			*this,
			&Part::preventTransitionToHcDisable,
			&Part::allowTransitionToHciDisable
			),
		_inhibitTransitionToHostControllerDisabled(0),
		_hostControllerDisabled(false)
		{
	for(unsigned i=0;i<nL2CapConnections;++i){
		_freeL2capMem.put(&_l2capMem[i]);
		}
	_advTxTimerApi.setExpirationCallback(
		_advTxTimerExpired
		);
	_disconnectTimerApi.setExpirationCallback(
		_disconnectTimerExpired
		);
	}

Oscl::Pdu::FWD::Api&	Part::getProvTxBearerApi() noexcept{
	return _advBearerTxProvComposer;
	}

Oscl::Pdu::FWD::Api&	Part::getBeaconTxBearerApi() noexcept{
	return _advBearerTxBeaconComposer;
	}

Oscl::Pdu::FWD::Api&	Part::getMeshTxBearerApi() noexcept{
	return _advBearerTxMeshComposer;
	}

Oscl::Pdu::FWD::Api&	Part::getAdvGattBearerApi() noexcept{
	return _advGattBearerTxComposer;
	}

Oscl::Frame::RF::RX::Host::Api&	Part::getMeshMessageRxHostApi() noexcept{
	return _meshMessageRxHostCommposer;
	}

Oscl::Frame::FWD::Host::Api&	Part::getSecureNetworkBeaconRxHostApi() noexcept{
	return _secureNetworkBeaconRxHostComposer;
	}

void	Part::attachMeshMessageReceiver(Oscl::Frame::RF::RX::Item& receiver) noexcept{
	_meshMessageReceivers.put(&receiver);
	}

void	Part::detachMeshMessageReceiver(Oscl::Frame::RF::RX::Item& receiver) noexcept{
	_meshMessageReceivers.remove(&receiver);
	}

void	Part::attachSecureNetworkBeaconReceiver(Oscl::Frame::FWD::Item& receiver) noexcept{
	_secureNetworkBeaconReceivers.put(&receiver);
	}

void	Part::detachSecureNetworkBeaconReceiver(Oscl::Frame::FWD::Item& receiver) noexcept{
	_secureNetworkBeaconReceivers.remove(&receiver);
	}

Oscl::Inhibitor::Api&	Part::getHostControllerDisableInhibitApi() noexcept{
	return _hostControllerDisableInhibitor;
	}

Oscl::Frame::FWD::Api&	Part::getFrameRxApi() noexcept{
	return _frameReceiver;
	}

Oscl::BT::HCI::Reset::Api&	Part::getResetApi() noexcept{
	return *this;
	}

void	Part::start() noexcept{
	_bearerHostApi.attach(_netInterfaceComposer);
	}

void	Part::dumpStatus(Oscl::Print::Api& printApi) noexcept{
	printApi.print(
		"HCI status:\n"
		" _hciRxCount: %u\n"
		" _hciTxCount: %u\n"
		" _hciCmdQ empty: %s\n"
		" _disconnectPending: %s\n"
		" _numPendingLeDataPackets: %u\n"
		" _numLeDataPackets: %u\n"
		" _hciAclTxQ empty: %s\n"
		" _advNonConnectableQ empty: %s\n"
		" _advConnectableQ empty: %s\n"
		" _activeProvConns empty: %s\n"
		" _activeProxyConns empty: %s\n"
		"",
		_hciRxCount,
		_hciTxCount,
		_hciCmdQ.first()?"false":"true",
		_disconnectPending?"true":"false",
		_numPendingLeDataPackets,
		_numLeDataPackets,
		_hciAclTxQ.first()?"false":"true",
		_advNonConnectableQ.first()?"false":"true",
		_advConnectableQ.first()?"false":"true",
		_activeProvConns.first()?"false":"true",
		_activeProxyConns.first()?"false":"true"
		);
	}

void	Part::hciTickle(Oscl::Print::Api& printApi) noexcept{
	_txBusy	= false;

	processHciCmdTxQ();
	}

void	Part::disconnectL2CAP(Oscl::Print::Api& printApi) noexcept{

	Oscl::BT::Mesh::Conn::Prov::Part*
	prov	= _activeProvConns.first();

	if(prov){
		prov->forceDisconnect();
		printApi.print(
			"Disconnect sent.\n"
			);
		return;
		}

	Oscl::BT::Mesh::Conn::Proxy::Part*
	proxy	= _activeProxyConns.first();

	if(proxy){
		prov->forceDisconnect();
		printApi.print(
			"Disconnect sent.\n"
			);
		return;
		}

	printApi.print(
		"No L2CAP connection session.\n"
		);
	}

void	Part::logging(bool enable) noexcept{
	_logTxPackets	= enable;
	_logRxPackets	= enable;
	}

bool	Part::gattConnectionIsActive() const noexcept{
	return (
				_activeProxyConns.first()
			||	_activeProvConns.first()
			)?true:false;
	}

bool	Part::isDestinedForProxy(uint16_t address) noexcept{
	Oscl::BT::Mesh::Conn::Proxy::Part*
	proxy	= _activeProxyConns.first();

	if(!proxy){
		return false;
		}

	return proxy->isDestinedForProxy(address);
	}

void	Part::networkSecurityChanged() noexcept{
	Oscl::BT::Mesh::Conn::Proxy::Part*	connection;

	for(
		connection	= _activeProxyConns.first();
		connection;
		connection	= _activeProxyConns.next(connection)
		){
		connection->networkSecurityChanged();
		}
	}

void	Part::allFreeNotification() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif
	}

void	Part::sendReset() noexcept{

	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Handle<Oscl::Pdu::Pdu>	handle;
	while((handle=_hciCmdQ.get())){
		handle	= 0;
		}

	sendHciReset();
	sendReadLocalSupportedCommands();
	sendLeReadBufferSizeCommand();
	sendHciSetBoardAddress();
	sendWriteLeHostSupported();
	sendLeSetRandomAddr();
	sendEventMask();
	sendLeEventMask();
	sendLeSetAdvParametersConnectableScanable();
	sendLeSetScanParameters();
	sendScanResponseData();
	sendLeSetScanEnable();
	}

void	Part::sendHciReset() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x03,	// HCI_OP_RESET LSB
		0x0C,	// HCI_OP_RESET MSB
		0x00	// length
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendLeReadBufferSizeCommand() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x02,	// LE Read Buffer Size Command LSB
		0x20,	// LE Read Buffer Size Command MSB
		0x00	// length
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendHciSetBoardAddress() noexcept{
	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static constexpr uint8_t	hciCmd		= 0x01;	// HCI_COMMAND_PKT
		encoder.encode(hciCmd);

		_context.encodeBoardAddressHciCommand(encoder);

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fixed memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendReadLocalSupportedCommands() noexcept{
	// OGF: 0x04
	// OCF: 0x0002
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x02,	// Read Local Supported Commands LSB
		0x10,	// Read Local Supported Commands MSB (OGF << 2)
		0x00	// length
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendWriteLeHostSupported() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x6D,	// HCI_OP_WRITE_LE_HOST_SUPPORTED LSB
		0x0C,	// HCI_OP_WRITE_LE_HOST_SUPPORTED MSB
		0x02,	// length
		0x01,	// LE Supported Host enabled
		0x00	// Simulaneous LE and BR/EDR to Same Device Capable disabled.
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendLeSetRandomAddr() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x05,	// HCI_OP_LE_SET_RANDOM_ADDR LSB
		0x20,	// HCI_OP_LE_SET_RANDOM_ADDR MSB
		0x06,	// length
		0xF0,
		0x11,
		0x11,
		0x11,
		0x11,
		0xF0
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendEventMask() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x01,	// HCI_CMD_SET_EVENT_MASK LSB
		0x0C,	// HCI_CMD_SET_EVENT_MASK MSB
		0x08,	// length
				// [7:0]
			(1<<0)	// [0] Inquiry Complete
		|	(1<<1)	// [1] Inquiry Result
		|	(1<<2)	// [2] Connection Complete
		|	(1<<3)	// [3] Connection Request
		|	(1<<4)	// [4] Disconnection Complete
		|	(1<<5)	// [5] Authentication Complete
		|	(1<<6)	// [6] Remote Name Request Complete
		|	(1<<7)	// [7] Encryption Change
		,
			(1<<0)	// [8] Change Connection Link Key Complete
		|	(1<<1)	// [9] Master Link Key Complete
		|	(0<<2)	// [10] Read Remote Supported Features Complete
		|	(0<<3)	// [11] Read Remote Version Information Complete
		|	(1<<4)	// [12] QoS Setup Complete
		|	(0<<5)	// [13] Reserved
		|	(0<<6)	// [14] Reserved
		|	(1<<7)	// [15] Hardware Error
		,
			(1<<0)	// [16] Flush Occurred
		|	(1<<1)	// [17]	Role Change
		|	(0<<2)	// [18] Reserved
		|	(0<<3)	// [19] Mode Change
		|	(1<<4)	// [20] Return Link Keys
		|	(1<<5)	// [21] PIN Code Request
		|	(1<<6)	// [22] Link Key Request
		|	(1<<7)	// [23] Link Key Notification
		,
			(1<<0)	// [24] Loopback Command
		|	(1<<1)	// [25] Data Buffer Overflow
		|	(0<<2)	// [26] Max Slots Change
		|	(0<<3)	// [27] Read Clock Offset Complete
		|	(1<<4)	// [28] Connection Packet Type Changed
		|	(0<<5)	// [29] QoS Violation
		|	(0<<6)	// [30] Page Scan Mode Change
		|	(0<<7)	// [31] Page Scan Repetition Mode Change
		,
			(0<<0)	// [32] Flow Specification Complete
		|	(0<<1)	// [33] Inquiry Result with RSSI
		|	(1<<2)	// [34] Read Remote Extended Features Complete
		|	(0<<3)	// [35] Reserved
		|	(0<<4)	// [36] Reserved
		|	(0<<5)	// [37] Reserved
		|	(0<<6)	// [38] Reserved
		|	(0<<7)	// [39] Reserved
		,
			(0<<0)	// [40] Reserved
		|	(0<<1)	// [41] Reserved
		|	(0<<2)	// [42] Reserved
		|	(1<<3)	// [43] Synchronous Connection Complete
		|	(1<<4)	// [44] Synchronous Connection Changed
		|	(1<<5)	// [45] Sniff Subrating
		|	(1<<6)	// [46] Extended Inquiry Result
		|	(1<<7)	// [47] Encryption Key Refresh Complete
		,
			(1<<0)	// [48] IO Capability Request
		|	(1<<1)	// [49] IO Capability Response
		|	(1<<2)	// [50] User Confirmation Request
		|	(1<<3)	// [51] User Passkey Request
		|	(1<<4)	// [52] Remote OOB Data Request
		|	(1<<5)	// [53] Simple Pairing Complete
		|	(0<<6)	// [54] Reserved
		|	(0<<7)	// [55] Link Supervision Timeout Changed
		,
			(1<<0)	// [56] Enchanced Flush Complete
		|	(0<<1)	// [57] Reserved
		|	(1<<2)	// [58] User Passkey Notification
		|	(1<<3)	// [59] Keypress Notification
		|	(1<<4)	// [60] Remote Host Supported Features Notification
		|	(1<<5)	// [61] LE Meta
		|	(0<<6)	// [62] Reserved
		|	(0<<7)	// [63] Reserved
		};



	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendLeEventMask() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x01,	// HCI_CMD_LE_SET_EVENT_MASK LSB
		0x20,	// HCI_CMD_LE_SET_EVENT_MASK MSB
		0x08,	// length
			(1<<0)	// [0] Connection Complete
		|	(1<<1)	// [1] Advertising Report
		|	(1<<2)	// [2] Connection Update Complete
		|	(1<<3)	// [3] Read Remote Features Complete
		|	(1<<4)	// [4] Long Term Key Request
		|	(1<<5)	// [5] Remote Connection Parameter Request
		|	(1<<6)	// [6] Data Length Change
		|	(1<<7)	// [7] Read Local P-256 Public Key Complete
		,
			(1<<0)	// [8] Generate DHKey Complete
		|	(0<<1)	// [9] Enhanced Connection Complete
		|	(1<<2)	// [10] Directed Advertising Report
		|	(1<<3)	// [11] PHY Update Complete
		|	(1<<4)	// [12] Extended Advertising Report
		|	(1<<5)	// [13] Periodic Advertising Sync Established
		|	(1<<6)	// [14] Periodic Advertising Report
		|	(1<<7)	// [15] Periodic Advertising Sync Lost
		,
			(1<<0)	// [16] Extended Scan Timeout
		|	(1<<1)	// [17] Extended Adveretising Set Terminated
		|	(1<<2)	// [18] Scan Request Received
		|	(0<<3)	// [19] Channel Selection Algorithm
		,
		0x00,
		0x00,
		0x00,
		0x00,
		0x00
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
					cmd,
					sizeof(cmd)
					);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

/* [Proxy] Advertising 7.2.2.2.1
	The server shall use the GAP General Discoverable mode
	with connectable undirected advertising events in the
	format described in Table 7.6 below.
 */

/*	9.2.4 General Discoverable Mode
	9.2.4.1 Description
		Devices configured in general discoverable mode are
		intended to be discoverable by devices performing
		the general discovery procedure. The general discoverable
		mode is typically used when the device is intending to be
		discoverable for a long period of time.
	9.2.4.2 Conditions
		While a device is in the Peripheral role the device
		may support the general discoverable mode.
		While a device is only in the Broadcaster, Observer or
		Central role the device shall not support the general
		discoverable mode.
		A device in general discoverable mode shall send advertising
		events with the advertising data including the Flags AD data
		type as defined in [Core Specification Supplement], Part A,
		Section 1.3 with all the following flags set as described:

		• The LE Limited Discoverable Mode flag set to zero.
		• The LE General Discoverable Mode flag set to one.
		• For a device of the LE-only device type with all
			the following flags set as described:

		a) The ‘BR/EDR Not Supported’ flag set to one.
		b) The ‘Simultaneous LE and BR/EDR to Same Device Capable
			(Controller)’ flag set to zero.
		c) The ‘Simultaneous LE and BR/EDR to Same Device Capable
			(Host)’ flag set to zero.
		The advertising data should also include the following
		AD types to enable a faster connectivity experience:
		• TX Power Level AD type as defined in [Core Specification Supplement],
			Part A, Section 1.5.
		• Local Name AD type as defined in [Core Specification Supplement],
			Part A, Section 1.2.
		• Service UUIDs AD type as defined in [Core Specification Supplement],
			Part A, Section 1.1.
		• Slave Connection Interval Range AD type as defined in
			[Core Specification Supplement], Part A, Section 1.9.
		While a device is in general discoverable mode the Host configures
		the Controller as follows:
		• The Host shall set the advertising filter policy for all advertising
			sets to ‘process scan and connection requests from all devices’.
		• The Host should set the advertising intervals as defined in
			Section 9.3.11.
		The device shall remain in general discoverable mode until a connection
		is established or the Host terminates the mode.
 */

void	Part::sendLeSetAdvParametersConnectableScanable() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x06,	// HCI_CMD_LE_SET_ADV_PARAM LSB
		0x20,	// HCI_CMD_LE_SET_ADV_PARAM MSB
		0x0F,	// length
		0xA0,	// minInterval LSB 100ms
		0x00,	// minInterval MSB
		0xB0,	// maxInterval LSB 110ms
		0x00,	// maxInterval MSB
		/*
			A Peripheral device should follow the guidelines defined in Section 9.3.11. A
			Peripheral device shall send either 
			- connectable and scannable undirected advertising events or
			- connectable undirected advertising events.
		*/
		0x00,	// type - Connectable and scannable undirected advertising (ADV_IND)
		0x00,	// ownAddrType	Public Device Address
		0x00,	// directAddrType	Public Device Address
		0x00,0x00,0x00,0x00,0x00,0x00,	// directAddr
		0x07,	// channelMap chanels: 37,38,39
		0x00	// filterPolicy
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendLeSetAdvParametersNonConnectable() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x06,	// HCI_CMD_LE_SET_ADV_PARAM LSB
		0x20,	// HCI_CMD_LE_SET_ADV_PARAM MSB
		0x0F,	// length
		0xA0,	// minInterval LSB 100ms
		0x00,	// minInterval MSB
		0xB0,	// maxInterval LSB 110ms
		0x00,	// maxInterval MSB
		0x03,	// type - Non connectable undirected advertising
		0x00,	// ownAddrType	Public Device Address
		0x00,	// directAddrType	Public Device Address
		0x00,0x00,0x00,0x00,0x00,0x00,	// directAddr
		0x07,	// channelMap chanels: 37,38,39
		0x00	// filterPolicy
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

void	Part::sendLeSetScanParameters() noexcept{
	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x0B,	// HCI_CMD_LE_SET_SCAN_PARAM LSB
		0x20,	// HCI_CMD_LE_SET_SCAN_PARAM MSB
		0x07,	// length
		0x00,	// type - passive
		0x30,	// interval LSB 30ms
		0x00,	// interval MSB
		0x30,	// window LSB 30ms
		0x00,	// window MSB
		0x00,	// ownAddrType Public Device Address
		0x00	// filterPolicy Accept all advertising packets except directed not addressed
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	hciCmdTxPdu(fixed);
	}

bool	Part::sendLeSetScanEnable(bool enable) noexcept{
	// NOTE: This command is used to enable the reception
	// of advertising packets.
	// A low-power-node will disable this when not awake.
	// Similarly, a low-power-node will disable the transmitter
	// by setting LE_SET_ADV_ENABLE to false as well while not awake.
	static const uint8_t	header[] = {
		0x01,	// HCI_COMMAND_PKT
		0x0C,	// HCI_CMD_LE_SET_SCAN_ENABLE LSB
		0x20,	// HCI_CMD_LE_SET_SCAN_ENABLE MSB
		0x02	// length
		};

	static const uint8_t	enablePayload[] = {
		0x01,	// enable
		0x00	// filterDuplicates
		};
	
	static const uint8_t	disablePayload[] = {
		0x00,	// disable
		0x00	// filterDuplicates
		};

	const uint8_t*
	payload	= enable?enablePayload:disablePayload;

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocConstWrapper(
					header,
					sizeof(header)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	Oscl::Pdu::Fragment*
	fragment	= _freeStore.allocConstFragment(payload,sizeof(enablePayload));

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	composite->append(fragment);

	_scanningEnabled	= enable;

	hciCmdTxPdu(composite);

	return false;
	}

void	Part::sendLeSetScanEnable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	// NOTE: This command is used to enable the reception
	// of advertising packets.
	if(sendLeSetScanEnable(true)){
		// failed
		Oscl::Error::Info::log(
			"%s: FAILED\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	}

void	Part::sendLeSetScanDisable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	// NOTE: This command is used to disable the reception
	// of advertising packets.
	if(sendLeSetScanEnable(false)){
		Oscl::Error::Info::log(
			"%s: FAILED\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	}

void	Part::sendDisconnectCommand(
			uint16_t	handle,
			uint8_t		reason
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	cmd		= 0x01;		// HCI_COMMAND_PKT
		// OGF = 0x01
		static const uint16_t	opcode	= 0x0406;	// OGF 0x01 [gggg ggcc cccc cccc] OCF 0x0006 0000 0100 0000 0110
		static const uint8_t	length	= 0x03;		// sizeof(handle) + sizeof(reason)

		encoder.encode(cmd);
		encoder.encode(opcode);
		encoder.encode(length);
		encoder.encode(handle);
		encoder.encode(reason);

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fixed memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}


	hciCmdTxPdu(fixed);
	}

bool	Part::rxForward(	
			const void*		frame,
			unsigned int	length
			) noexcept{

	++_hciRxCount;

	const uint8_t*	p	= (const uint8_t*) frame;

	switch(p[0]){
		case 0x01:
			// HCI_COMMAND_PKT
			#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
			Oscl::Error::Info::log(
				"||||||||||||||||||||||||||||||||| RX HCI_COMMAND_PKT\n"
				);
			#endif
			return false;
		case 0x02:
			// HCI_ACLDATA_PKT
			#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
			Oscl::Error::Info::log(
				"||||||||||||||||||||||||||||||||| RX HCI_ACLDATA_PKT\n"
				);
			#endif
			if(_logRxPackets){
				Oscl::Bluetooth::Debug::Print::parseHCI_MON_ACL_RX_PKT(
					0,			// index (hci0)
					length-1,
					"hci0",		// devIndexName ("hci0")
					&p[1]
					);
				}
			return processAclRX(
					&p[1],
					length-1
					);
		case 0x03:
			// HCI_SCODATA_PKT
			#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
			Oscl::Error::Info::log(
				"||||||||||||||||||||||||||||||||| RX HCI_SCODATA_PKT\n"
				);
			#endif
			return false;
		case 0x04:
			{
			// HCI_EVENT_PKT
			#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
			Oscl::Error::Info::log(
				"||||||||||||||||||||||||||||||||| RX HCI_EVENT_PKT\n"
				);
			#endif
			if(_logRxPackets){
				Oscl::Bluetooth::Debug::Print::parseHCI_MON_EVENT_PKT(
					0,			// index (hci0)
					length-1,
					"hci0",		// devIndexName ("hci0")
					&p[1]
					);
				}
			Oscl::Frame::FWD::Api&
			api	= _hciEventReceiver;
			return api.forward(
					&p[1],
					length-1
					);
			}
		default:
//			#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
			#if 1
			Oscl::Error::Info::log(
				"||||||||||||||||||||||||||||||||| RX Unknown HCI Type: 0x%2.2X\n",
				p[0]
				);
			#endif
			return false;
		}

	return false;
	}

bool	Part::parseNumberOfCompletedPackets(
			const void*		frame,
			unsigned 		length
			) noexcept {

	/*
		The Number Of Completed Packets event is used by the Controller to indicate
		to the Host how many HCI Data Packets have been completed (transmitted or
		flushed) for each Connection_Handle since the previous Number Of Completed
		Packets event was sent to the Host.

		This means that the corresponding buffer space has been freed in the
		Controller.

		Based on this information, and the HC_Total_Num_ACL_Data_Packets and
		HC_Total_Num_Synchronous_Data_Packets return parameter of the
		Read_Buffer_Size command, the Host can determine for which
		Connection_Handles the following HCI Data Packets should be sent to
		the Controller.

		The Number Of Completed Packets event shall not specify a given
		Connection_Handle before the Connection Complete event for the
		corresponding connection or after an event indicating disconnection
		of the corresponding connection.

		While the Controller has HCI data packets in its buffer, it must keep
		sending the Number Of Completed Packets event to the Host at least
		periodically, until it finally reports that all the pending ACL Data
		Packets have been transmitted or flushed.

		The rate with which this event is sent is manufacturer specific.

		Note: Number Of Completed Packets events will not report on synchronous
		Connection_Handles if synchronous Flow Control is disabled. (See Section
		7.3.36 and Section 7.3.37.)
	*/

	/*	NOTE!
			The Host Controller will NOT necessarily send this
			event after all outstanding packets have been transmitted.
			Empirically, it is apparently only intended for flow-control
			to prevent Host Controller buffer overflows.

			As far as I can tell, there is no way to know when all
			outstanding packets have been sent/flushed. Only that
			there are buffers available.
	 */

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint8_t		number_of_handles;
	uint16_t	connection_handle;
	uint16_t	hc_num_of_completed_packets;

	decoder.decode(number_of_handles);
	decoder.decode(connection_handle);
	decoder.decode(hc_num_of_completed_packets);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" _numPendingLeDataPackets: %u,"
		" Number_Of_Handles: 0x%2.2X,"
		" Connection_Handle: 0x%4.4X,"
		" HC_Num_Of_Completed_Packets: %u\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		_numPendingLeDataPackets,
		number_of_handles,
		connection_handle,
		hc_num_of_completed_packets
		);
	#endif

	_numPendingLeDataPackets -= hc_num_of_completed_packets;

	if(!_activeConnections.first()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No connections, drop and flush.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		#endif
		// No connections
		// Flush the queue.
		flushAclTxQueue();
		return false;
		}

	if(!hc_num_of_completed_packets){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _numPendingLeDataPackets: %u, _disconnectPending: %s.\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		_numPendingLeDataPackets,
		_disconnectPending?"true":"false"
		);
	#endif

	processAclTxQueue();

	return false;
	}

bool	Part::hciEventForward(	
			const void*		frame,
			unsigned int	length
			) noexcept{

	if(length < 2){
		Oscl::Error::Info::log(
			"%s: frame too short.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	const uint8_t*	p	= (const uint8_t*) frame;

	unsigned	len	= p[1];

	const void*	payload	= &p[2];

	if(len > (length-1)){
		Oscl::Error::Info::log(
			"%s: embedded length longer than frame.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Event (0x%2.2X): \"%s\"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		p[0],
		Oscl::Bluetooth::Strings::hciEvent(p[0])
		);
	#endif

	switch(p[0]){
		case 0x3E:
			// LE Meta
			{
			Oscl::Frame::FWD::Api&
			api	= _hciLeMetaReceiver;
			return api.forward(
					payload,
					len
					);
			}
		case 0x0E:
			// Command Complete
			{
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::hciEvent(p[0])
				);
			#endif
			Oscl::Frame::FWD::Api&
			api	= _hciCmdCompleteReceiver;
			return api.forward(
					payload,
					len
					);
			}
		case 0x05:
			// Disconnection Complete
			return parseDisconnectionComplete(
					payload,
					length
					);
		case 0x0F:
			// Command Status
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::hciEvent(p[0])
				);
			#endif
			parseCommandStatus(
				payload,
				length
				);
			return false;
		case 0x13:
			// Number Of Completed Packets
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::hciEvent(p[0])
				);
			#endif
			return parseNumberOfCompletedPackets(
					payload,
					length
					);
		default:
//			#ifdef DEBUG_TRACE
			#if 1
			Oscl::Error::Info::log(
				"%s: Unhandled Event: (0x%2.2X) \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				p[0],
				Oscl::Bluetooth::Strings::hciEvent(p[0])
				);
			#endif
			return false;
		}

	return false;
	}

void	Part::parseLeReadBufferSizeStatus(Oscl::Endian::Decoder::Api& dec) noexcept{
	uint8_t		status;

	Oscl::Decoder::Api&	decoder	= dec.le();

	decoder.decode(status);
	decoder.decode(_dataPacketLength);
	decoder.decode(_numLeDataPackets);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tstatus: (0x%2.2X) %s\n"
		"\tHC_LE_Data_Packet_Length: (0x%4.4X) %u\n"
		"\tHC_Total_Num_LE_Data_Packets: (0x%2.2X) %u\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		status,
		Oscl::Bluetooth::Strings::statusCode(status),
		_dataPacketLength,
		_dataPacketLength,
		_numLeDataPackets,
		_numLeDataPackets
		);
	#endif
	}

bool	Part::hciCmdCompleteForward(	
			const void*		frame,
			unsigned int	length
			) noexcept{

	uint8_t		n;	// [0] N -  The Number of HCI command packets which are allowed to be sent to the Controller from the Host.
	uint16_t	opcode;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	decoder.decode(n);
	decoder.decode(opcode);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: n: %u, opcode: (0x%4.4X) %s\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		n,
		opcode,
		Oscl::Bluetooth::Strings::hciOpcode(opcode)
		);
	#endif

	switch(opcode){
		case 0x0000:
			break;
		case 0x2002:
			parseLeReadBufferSizeStatus(leDecoder);
			break;
		default:
			break;
		};

	_txBusy	= false;

	processHciCmdTxQ();

	return true;
	}

bool	Part::parseMeshMessage(
			const void*		frame,
			uint8_t 		length,
			int8_t			rssi
			) noexcept{
	// <<Mesh Message>>

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Frame::RF::RX::Item*	receiver;

	for(
		receiver	= _meshMessageReceivers.first();
		receiver;
		receiver	= _meshMessageReceivers.next(receiver)
		){
		bool
		accepted	= receiver->forward(frame,length,rssi);
		if(accepted){
			return true;
			}
		}

	return false;
	}

bool	Part::parseSecureNetworkBeacon(
			const void*		frame,
			uint8_t 		length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= beDecoder.be();

	uint8_t		flags;
	uint8_t		networkID[8];
	uint32_t	ivIndex;
	uint8_t		authenticationValue[8];

	decoder.decode(flags);
	decoder.copyOut(networkID,sizeof(networkID));
	decoder.decode(ivIndex);
	decoder.copyOut(authenticationValue,sizeof(authenticationValue));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" flags: 0x%2.2X,"
		" networkID: %2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X,"
		" ivIndex: 0x%8.8X,"
		" authenticationValue: %2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		flags,
		networkID[0],
		networkID[1],
		networkID[2],
		networkID[3],
		networkID[4],
		networkID[5],
		networkID[6],
		networkID[7],
		ivIndex,
		authenticationValue[0],
		authenticationValue[1],
		authenticationValue[2],
		authenticationValue[3],
		authenticationValue[4],
		authenticationValue[5],
		authenticationValue[6],
		authenticationValue[7]
		);
	#endif

	Oscl::Frame::FWD::Item*	receiver;

	for(
		receiver	= _secureNetworkBeaconReceivers.first();
		receiver;
		receiver	= _secureNetworkBeaconReceivers.next(receiver)
		){
		bool
		accepted	= receiver->forward(
						frame,
						length
						);
		if(accepted){
			return true;
			}
		}

	return false;
	}

bool	Part::parseUnprovisionedDeviceBeacon(
			const void*		frame,
			uint8_t 		length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= beDecoder.be();

	uint8_t		deviceUUID[16];
	uint16_t	oobInformation;
	uint32_t	uriHash	= 0;

	decoder.copyOut(deviceUUID,sizeof(deviceUUID));
	decoder.decode(oobInformation);

	if(decoder.remaining()){
		// The uriHash is optional!
		decoder.decode(uriHash);
		}

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	// 00000000-0000-0000-0000-000000ABCDEF
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Device UUID: "
		"%2.2X%2.2X%2.2X%2.2X-"
		"%2.2X%2.2X-%2.2X%2.2X-%2.2X%2.2X-"
		"%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X, "
		"URI Hash: 0x%8.8X, "
		"OOB Information: 0x%4.4X"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		deviceUUID[0],
		deviceUUID[1],
		deviceUUID[2],
		deviceUUID[3],
		deviceUUID[4],
		deviceUUID[5],
		deviceUUID[6],
		deviceUUID[7],
		deviceUUID[8],
		deviceUUID[9],
		deviceUUID[10],
		deviceUUID[11],
		deviceUUID[12],
		deviceUUID[13],
		deviceUUID[14],
		deviceUUID[15],
		uriHash,
		oobInformation
		);

	for(unsigned i=0;i<16;++i){
		if(!(oobInformation & (1<<i))){
			continue;
			}
		Oscl::Error::Info::log(
			"\t[%u] \"%s\"\n",
			i,
			Oscl::Bluetooth::Strings::oobInformation(i)
			);
		}
	#endif

	return true;
	}

bool	Part::parseMeshBeacon(
			const void*		frame,
			uint8_t 		length
			) noexcept{
	// <<Mesh Beacon>>

	const uint8_t*	p	= (const uint8_t*) frame;

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= beDecoder.be();

	uint8_t	beaconType;

	decoder.decode(beaconType);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: beaconType: 0x%2.2X, \"%s\"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		beaconType,
		Oscl::Bluetooth::Strings::meshBeaconType(beaconType)
		);
	#endif

	switch(beaconType){
		case 0x00:
			// Unprovisioned Device beacon
			return parseUnprovisionedDeviceBeacon(
				&p[sizeof(beaconType)],
				decoder.remaining()
				);
		case 0x01:
			// Secure Network beacon
			return parseSecureNetworkBeacon(
				&p[sizeof(beaconType)],
				decoder.remaining()
				);
		default:
			Oscl::Error::Info::hexDump(
				frame,
				decoder.remaining(),
				sizeof(beaconType)
				);
			return false;
		}

	return false;
	}

bool	Part::parseEirFields(
			uint8_t			bdaddrType,
			const uint8_t*	bdaddr,
			uint8_t 		length,
			const void*		frame
			) noexcept{

	const uint8_t*	p	= (const uint8_t*) frame;

	//	EIR Fields

	bool	processed	= false;

	for(unsigned i=0;length;++i){
		uint8_t	len		= p[0];
		if(!len){
			break;
			}

		uint8_t	type	= p[1];

		--len;

		switch(type){
			case 0x2A:
				// <<Mesh Message>>
				#ifdef DEBUG_TRACE
//				#if 1
				Oscl::Error::Info::log(
					"%s:<<Mesh Message>>:"
					"_leType: (0x%2.2X) \"%s\","
					"_rssi: %d"
					"\n",
					OSCL_PRETTY_FUNCTION_DETAIL,
					_leType,
					Oscl::Bluetooth::Strings::leEventType(_leType),
					_rssi
					);
				#endif
				processed	= (processed || parseMeshMessage(&p[2],len,_rssi))?true:false;
				break;
			case 0x29:
				#if 0
				// <<PB-ADV>>
				Oscl::Error::Info::log(
					"%s:<<PB-ADV>>:"
					"_leType: (0x%2.2X) \"%s\","
					"_rssi: %d"
					"\n",
					OSCL_PRETTY_FUNCTION_DETAIL,
					_leType,
					Oscl::Bluetooth::Strings::leEventType(_leType),
					_rssi
					);
				#endif
				processed	= (processed || _provRxApi.forward(&p[2],len))?true:false;
				break;
			case 0x2B:
				// <<Mesh Beacon>>
				#ifdef DEBUG_TRACE
//				#if 1
				Oscl::Error::Info::log(
					"%s:<<Mesh Beacon>>:"
					"_leType: (0x%2.2X) \"%s\","
					"_rssi: %d"
					"\n",
					OSCL_PRETTY_FUNCTION_DETAIL,
					_leType,
					Oscl::Bluetooth::Strings::leEventType(_leType),
					_rssi
					);
				#endif
				processed	= (processed || parseMeshBeacon(&p[2],len))?true:false;
				break;
			default:
				// Unused advertising types
				#if 0
				Oscl::Error::Info::log(
					"%s: Unused :"
					"_leType: (0x%2.2X) \"%s\","
					"_rssi: %d"
					"\n",
					OSCL_PRETTY_FUNCTION_DETAIL,
					_leType,
					Oscl::Bluetooth::Strings::leEventType(_leType),
					_rssi
					);
				#endif
				break;
			}

		p	+= (len+2);

		length	-= (len + 2);
		}

	return processed;
	}

bool	Part::parseLeAdvInd(
			uint8_t			bdaddrType,
			const uint8_t*	bdaddr,
			uint8_t 		length,
			const void*		frame
			) noexcept {

	return parseEirFields(
			bdaddrType,
			bdaddr,
			length,
			frame
			);
	}

bool	Part::parseLeAdvNonConnInd(
			uint8_t			bdaddrType,
			const uint8_t*	bdaddr,
			uint8_t 		length,
			const void*		frame
			) noexcept {

	return parseEirFields(
			bdaddrType,
			bdaddr,
			length,
			frame
			);
	}

bool	Part::parseAdvertisingReport(
			const void*		frame,
			unsigned& 		length
			) noexcept {

	const uint8_t*	p	= (const uint8_t*) frame;

	uint8_t		type		= p[0];
	uint8_t		bdaddrType	= p[1];
	uint8_t		bdaddr[6];
	uint8_t		len			= p[2+sizeof(bdaddr)];

	memcpy(bdaddr,&p[2],sizeof(bdaddr));

	unsigned
	headerLength	= 
			+	sizeof(type)
			+	sizeof(bdaddrType)
			+	sizeof(bdaddr)
			+	sizeof(len)
			;

	p	+=	headerLength;

	// TODO: The last octet in each advertising report
	// contains the RSSI field.
	// We *SHOULD* maintain a list of bdaddr/bdaddrType
	// and the RSSI statistics for that address.

	/*	Specification of the Bluetooth System v5.0
		7.7.65.2 LE Advertising Report Event
		RSSI[i]
			Value:
				N:
					Size: 1 uint8_t (signed integer)
					Range: -127 <= N <= +20
					Units: dBm
				127:
					RSSI is not available
				21:126
					Reserved for future use
	 */

	int8_t
	rssi	= (int8_t)p[len];

	if((rssi > 20) && (rssi < 127)){
		// Reserved or future use.
		/*	We could warn about this to indicate
			that either we've wor
			This could mean that the Host Controller is:
				1)	is faulty or
				2)	it implements a newer version
					of the Bluetooth Specification which
					has defined previously RFU values.
			We will simply assume that RSSI is not available
			in this case.
		 */
		_rssi	= 127;
		}
	else {
		_rssi	= rssi;
		}

	length	= len + headerLength + sizeof(_rssi);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\ttype: (0x%2.2X) \"%s\"\n"
		"\tbdaddrType: 0x%2.2X\n"
		"\tbdaddr: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tlen: %u\n"
		"\trssi: %d\n"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		type,
		Oscl::Bluetooth::Strings::leEventType(type),
		bdaddrType,
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		len,
		rssi
		);
	#endif

	_leType	= type;

	switch(type){
		case 0x03:
			// LE_ADV_NONCONN_IND
			return parseLeAdvNonConnInd(
					bdaddrType,
					bdaddr,
					len,
					p
					);
		case 0x00:
			// LE_ADV_IND
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: LE_ADV_IND\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			return false;
		case 0x01:
			// LE_ADV_DIRECT_IND
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: LE_ADV_DIRECT_IND\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			return false;
		case 0x02:
			// LE_ADV_SCAN_IND
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: LE_ADV_SCAN_IND\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			return false;
		case 0x04:
			// LE_ADV_SCAN_RSP
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: LE_ADV_SCAN_RSP\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			return false;
		default:
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Unknown\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			return false;
		};

	}

bool	Part::parseLeConnectionComplete(
			const void*		frame,
			unsigned 		length
			) noexcept {

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint8_t		status;
	uint16_t	connection_handle;
	uint8_t		role;
	uint8_t		peer_address_type;
	uint8_t		peer_address[6];
	uint16_t	conn_interval;
	uint16_t	conn_latency;
	uint16_t	supervision_timeout;
	uint8_t		master_clock_accuracy;

	decoder.decode(status);
	decoder.decode(connection_handle);
	decoder.decode(role);
	decoder.decode(peer_address_type);
	decoder.copyOut(peer_address,sizeof(peer_address));
	decoder.decode(conn_interval);
	decoder.decode(conn_latency);
	decoder.decode(supervision_timeout);
	decoder.decode(master_clock_accuracy);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	double
	interval	=  conn_interval * 1.25;
	double
	superTimeout	=  supervision_timeout * 10;

	Oscl::Error::Info::log(
		"%s:\n"
		"\tstatus: 0x%2.2X\n"
		"\tconnection_handle: 0x%4.4X\n"
		"\trole: 0x%2.2X\n"
		"\tpeer_address_type: 0x%2.2X\n"
		"\tconn_interval: (0x%4.4X) %f ms\n"
		"\tsupervision_timeout: (0x%4.4X) %f\n"
		"\tmaster_clock_accuracy: 0x%2.2X\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		status,
		connection_handle,
		role,
		peer_address_type,
		conn_interval,
		interval,
		supervision_timeout,
		superTimeout,
		master_clock_accuracy
		);
	#endif

	if(status != 0x00){
		Oscl::Error::Info::log(
			"%s: Connection failed. (0x%2.2X) \"%s\"\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			status,
			Oscl::Bluetooth::Strings::statusCode(status)
			);
		return false;
		}

	bool
	failed	= createNewL2CAPConnection(
				connection_handle
				);

	if(failed){

		sendDisconnectCommand(
			connection_handle,
			Oscl::BT::HCI::Error::connectionAlreadyExists
			);

		Oscl::Error::Info::log(
			"%s: createNewL2CAPConnection() failed.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);

		}

	return false;
	}

bool	Part::createNewL2CAPConnection(
			uint16_t	connection_handle
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: connection_handle: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		connection_handle
		);
	#endif

	L2CAPMem*
	mem	= _freeL2capMem.get();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of L2CAPMem.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	if(_context.isUnprovisioned()){
		Oscl::BT::Mesh::Conn::Prov::Part*
		connection	= new(mem)
						Oscl::BT::Mesh::Conn::Prov::Part(
							*this,
							_nodeCreateApi,
							*this, // Oscl::BT::Conn::TX::Api
							_timerFactoryApi,
							_hostControllerDisableInhibitor,
							connection_handle,
							_attMTU,
							_oobAvailable,
							_outputOobSizeInCharacters
							);

		connection->start();
		_activeProvConns.put(connection);
		_activeConnections.put(&connection->getRxConnItem());
		}
	else {
		Oscl::BT::Mesh::Conn::Proxy::Part*
		connection	= new(mem)
						Oscl::BT::Mesh::Conn::Proxy::Part(
							*this,
							_bearerHostApi,
							_networkIterator,
							*this, // Oscl::BT::Conn::TX::Api
							_timerFactoryApi,
							_hostControllerDisableInhibitor,
							connection_handle,
							_attMTU
							);

		connection->start();
		_activeProxyConns.put(connection);
		_activeConnections.put(&connection->getRxConnItem());
		}

	return false;
	}

bool	Part::parseDisconnectionComplete(
			const void*		frame,
			unsigned 		length
			) noexcept {

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint8_t		status;
	uint16_t	connection_handle;
	uint8_t		reason;

	decoder.decode(status);
	decoder.decode(connection_handle);
	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s:\n"
		"\tstatus: (0x%2.2X) \"%s\"\n"
		"\tconnection_handle: 0x%4.4X\n"
		"\treason: (0x%2.2X) \"%s\"\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		status,
		Oscl::Bluetooth::Strings::statusCode(status),
		connection_handle,
		reason,
		Oscl::Bluetooth::Strings::statusCode(reason)
		);
	#endif

	if(status != 0x00){
		Oscl::Error::Info::log(
			"%s: Disconnection failed. (0x%2.2X) \"%s\"\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			status,
			Oscl::Bluetooth::Strings::statusCode(status)
			);
		return false;
		}

	bool
	failed	= destroyL2CAPConnection(
				connection_handle
				);

	if(failed){
		Oscl::Error::Info::log(
			"%s: destroyL2CAPConnection() failed.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	flushAclTxQueue();

	_disconnectPending	= false;

	return false;
	}

bool	Part::destroyL2CAPConnection(
			uint16_t	connection_handle
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: connection_handle: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		connection_handle
		);
	#endif

	{
	Oscl::BT::Mesh::Conn::Prov::Part*	connection;
	for(
		connection	= _activeProvConns.first();
		connection;
		connection	= _activeProvConns.next(connection)
		){
		bool
		match	= connection->stop(connection_handle);
		if(match){
			_activeProvConns.remove(connection);
			_activeConnections.remove(&connection->getRxConnItem());
			return false;
			}
		}
	}

	{
	Oscl::BT::Mesh::Conn::Proxy::Part*	connection;
	for(
		connection	= _activeProxyConns.first();
		connection;
		connection	= _activeProxyConns.next(connection)
		){
		bool
		match	= connection->stop(connection_handle);
		if(match){
			_activeProxyConns.remove(connection);
			_activeConnections.remove(&connection->getRxConnItem());
			return false;
			}
		}
	}

	return true;
	}

void	Part::stopped(Oscl::BT::Mesh::Conn::Proxy::Part& connection) noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	L2CAPMem*	mem = (L2CAPMem*)&connection;

	connection.~Part();

	_disconnectPending			= false;
	_numPendingLeDataPackets	= 0;

	_freeL2capMem.put(mem);
	}

void	Part::stopped(Oscl::BT::Mesh::Conn::Prov::Part& connection) noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	L2CAPMem*	mem = (L2CAPMem*)&connection;

	connection.~Part();

	_disconnectPending			= false;
	_numPendingLeDataPackets	= 0;

	_freeL2capMem.put(mem);
	}

void	Part::setAttentionTimer(uint8_t nSeconds) noexcept{
	_context.setAttentionTimer(nSeconds);
	}

bool	Part::oobOutputActionBlinkIsSupported() const noexcept{
	return _context.oobOutputActionBlinkIsSupported();
	}

bool	Part::oobOutputActionBeepIsSupported() const noexcept{
	return _context.oobOutputActionBeepIsSupported();
	}

bool	Part::oobOutputActionVibrateIsSupported() const noexcept{
	return _context.oobOutputActionVibrateIsSupported();
	}

bool	Part::oobOutputActionNumericIsSupported() const noexcept{
	return _context.oobOutputActionNumericIsSupported();
	}

bool	Part::oobOutputActionAlphaNumericIsSupported() const noexcept{
	return _context.oobOutputActionAlphaNumericIsSupported();
	}

void	Part::oobOutputBlink(uint32_t n) noexcept{
	_context.oobOutputBlink(n);
	}

void	Part::oobOutputBeep(uint32_t n) noexcept{
	_context.oobOutputBeep(n);
	}

void	Part::oobOutputVibrate(uint32_t n) noexcept{
	_context.oobOutputVibrate(n);
	}

void	Part::oobOutputNumeric(uint32_t n) noexcept{
	_context.oobOutputNumeric(n);
	}

void	Part::oobOutputAlphaNumeric(const char* s) noexcept{
	_context.oobOutputAlphaNumeric(s);
	}

void	Part::provisioningStopped() noexcept{
	_context.provisioningStopped();
	}

void	Part::disconnect(
			uint16_t	handle,
			bool		force
			) noexcept{

	/*
		Apparently (empirically) it is *NOT* possible to
		know when all outstanding packets have been
		transmitted/completed/flushed on a given ACL
		link.

		Therefore, in an effort to ensure that all packets
		have been completed *BEFORE* disconnecting, we
		must use a timer to delay the actual transmission
		of the disconnect command unless we want to
		*force* the disconnection.
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle 0x%4.4X!\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		handle
		);
	#endif

	if(force){
		_disconnectPending	= false;
		_disconnectHandle	= handle;
		sendDisconnectCommand(
			handle,
			Oscl::BT::HCI::Error::remoteUserTerminatedConnection
			);
		return;
		}

	constexpr unsigned	disconnectTimeInMs	= 1000;

	_disconnectTimerApi.start(disconnectTimeInMs);

	_disconnectPending	= true;
	_disconnectHandle	= handle;

	}

void	Part::parseCommandStatus(
			const void*		frame,
			unsigned 		length
			) noexcept {

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint8_t		status;
	uint8_t		nHciCommandPackets;
	uint16_t	opcode;

	decoder.decode(status);
	decoder.decode(nHciCommandPackets);
	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tstatus: (0x%2.2X) \"%s\"\n"
		"\tnHciCommandPackets: %u\n"
		"\topcode: (0x%4.4X) \"%s\"\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		status,
		Oscl::Bluetooth::Strings::statusCode(status),
		nHciCommandPackets,
		opcode,
		Oscl::Bluetooth::Strings::hciOpcode(opcode)
		);
	#endif

	_txBusy	= false;

	processHciCmdTxQ();
	}

bool	Part::processAclRX(
			const void*	frame,
			unsigned 	length
			) noexcept{

	uint16_t	handleAndFlags;
	uint16_t	dataTotalLength;
	unsigned	remaining;
	{
		Oscl::Endian::Decoder::Linear::Part
		leDecoder(
			frame,
			length
			);

		Oscl::Decoder::Api&	decoder	= leDecoder.le();

		decoder.decode(handleAndFlags);
		decoder.decode(dataTotalLength);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return false;
			}

		remaining	= decoder.remaining();
		}

	if(dataTotalLength != remaining){
		Oscl::Error::Info::log(
			"%s: dataTotalLength(%u) != remaining(%u)\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			dataTotalLength,
			remaining
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\thandleAndFlags: 0x%4.4X\n"
		"\tdataTotalLength: 0x%4.4X\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		handleAndFlags,
		dataTotalLength
		);
	#endif

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t
	handle	= (handleAndFlags & 0x0FFF);

	const uint8_t	pbFlags	= (handleAndFlags >> 12) & 0x03;
	const uint8_t	bcFlags	= (handleAndFlags >> 14) & 0x03;

	const unsigned	offset	= length - remaining;

	length	-= offset;

	const uint8_t*	pdu	= (const uint8_t*)frame;

	pdu	= &pdu[offset];

	Oscl::BT::Conn::RX::Item*	item;

	for(
		item	= _activeConnections.first();
		item;
		item	= _activeConnections.next(item)
		){
		if(item->getConnectionHandle() == handle){
			item->receive(
				pbFlags,
				bcFlags,
				pdu,
				length	// dataTotalLength
				);
			return true;
			}
		}

	#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
	Oscl::Error::Info::log(
		"%s: RX HCI_ACLDATA_PKT no receiver.\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	return false;
	}

bool	Part::parseLeAdvertisingReports(
			const void*		frame,
			unsigned 		length
			) noexcept {
	const uint8_t*	p	= (const uint8_t*) frame;

	uint8_t		numReports	= p[0];

	++p;

	unsigned	len;
	bool		processed	= false;

	for(unsigned i=0;i<numReports;++i,p+=len){
		processed	= (processed || parseAdvertisingReport(p,len))?true:false;
		len	-= len;
		}

	return processed;
	}

void	Part::sendLeRemoteConnectionParameterReply(
			uint16_t	handle,
			uint16_t	intervalMin,
			uint16_t	intervalMax,
			uint16_t	latency,
			uint16_t	timeout,
			uint16_t	minimumCeLength,
			uint16_t	maximumCeLength
			) noexcept{

	#ifdef DEBUG_TRACE

	double
	minInterval	=  intervalMin * 1.25;
	double
	maxInterval	=  intervalMax * 1.25;
	double
	superTimeout	=  timeout * 10;

	Oscl::Error::Info::log(
		"%s:\n"
		"\tconnection_handle: 0x%4.4X\n"
		"\tintervalMin: (0x%4.4X) %f ms\n"
		"\tintervalMax: (0x%4.4X) %f ms\n"
		"\tlatency: (0x%4.4X) %u\n"
		"\ttimeout: (0x%4.4X) %f ms\n"
		"\tminimumCeLength: (0x%4.4X) %u\n"
		"\tmaximumCeLength: (0x%4.4X) %u\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		handle,
		intervalMin,
		minInterval,
		intervalMax,
		maxInterval,
		latency,
		latency,
		timeout,
		superTimeout,
		minimumCeLength,
		minimumCeLength,
		maximumCeLength,
		maximumCeLength
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	cmd	= 0x01;			// HCI_COMMAND_PKT
		/*
			opcode: OGF << 10 | OCF
			OGF: 0x08 << 10 = 0x2000
			OCF: 0x0020
			opcode: 0x2000 | 0x0020 = 0x2020 
		 */
		static const uint16_t	opcode	= 0x2020;	// LE Remote Connection Parameter Request Reply
		static const uint8_t	length	= 
										sizeof(handle)
									+	sizeof(intervalMin)
									+	sizeof(intervalMax)
									+	sizeof(latency)
									+	sizeof(timeout)
									+	sizeof(minimumCeLength)
									+	sizeof(maximumCeLength)
									;

		encoder.encode(cmd);
		encoder.encode(opcode);
		encoder.encode(length);
		encoder.encode(handle);
		encoder.encode(intervalMin);
		encoder.encode(intervalMax);
		encoder.encode(latency);
		encoder.encode(timeout);
		encoder.encode(minimumCeLength);
		encoder.encode(maximumCeLength);

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fixed memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	hciCmdTxPdu(fixed);
	}

bool	Part::parseLeRemoteConnectionParameterRequest(
			const void*		frame,
			unsigned 		length
			) noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: LE Remote Connection Parameter Request\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint16_t	connection_handle;
	uint16_t	intervalMin;
	uint16_t	intervalMax;
	uint16_t	latency;
	uint16_t	timeout;

	decoder.decode(connection_handle);
	decoder.decode(intervalMin);
	decoder.decode(intervalMax);
	decoder.decode(latency);
	decoder.decode(timeout);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	double
	minInterval	=  intervalMin * 1.25;
	double
	maxInterval	=  intervalMax * 1.25;
	double
	superTimeout	=  timeout * 10;

	Oscl::Error::Info::log(
		"\tconnection_handle: 0x%4.4X\n"
		"\tintervalMin: (0x%4.4X) %f ms\n"
		"\tintervalMax: (0x%4.4X) %f ms\n"
		"\tlatency: (0x%4.4X) %u\n"
		"\ttimeout: (0x%4.4X) %f ms\n"
		"",
		connection_handle,
		intervalMin,
		minInterval,
		intervalMax,
		maxInterval,
		latency,
		latency,
		timeout,
		superTimeout
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	// TODO: I'm uncertain about the
	// minimumCeLength and maximumCeLength
	// parameters.
	sendLeRemoteConnectionParameterReply(
		connection_handle,
		intervalMin,
		intervalMax,
		latency,
		timeout,
		0xFFFF,	// minimumCeLength 0 ms
		0xFFFF	// maximumCeLength 40.9 s
		);

	return true;
	}

bool	Part::parseLeConnectionUpdateComplete(
			const void*		frame,
			unsigned 		length
			) noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: LE Connection Update Complete\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= leDecoder.le();

	uint8_t		status;
	uint16_t	connection_handle;
	uint16_t	conn_interval;
	uint16_t	conn_latency;
	uint16_t	supervision_timeout;

	decoder.decode(status);
	decoder.decode(connection_handle);
	decoder.decode(conn_interval);
	decoder.decode(conn_latency);
	decoder.decode(supervision_timeout);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	double
	interval	=  conn_interval * 1.25;
	double
	superTimeout	=  supervision_timeout * 10;

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X: \"%s\"\n"
		"\tconnection_handle: 0x%4.4X\n"
		"\tconn_interval: (0x%4.4X) %f ms\n"
		"\tconn_latency: (0x%4.4X) %u\n"
		"\tsupervision_timeout: (0x%4.4X) %f ms\n"
		"",
		status,
		Oscl::Bluetooth::Strings::statusCode(status),
		connection_handle,
		conn_interval,
		interval,
		conn_latency,
		conn_latency,
		supervision_timeout,
		superTimeout
		);
	#endif

	return true;
	}

bool	Part::hciLeMetaForward(	
			const void*		frame,
			unsigned int	length
			) noexcept{

	const uint8_t*	p	= (const uint8_t*) frame;

	uint8_t	subevent	= p[0];

	--length;

	switch(subevent){
		case 0x02:
			// HCI_EV_LE_ADVERTISING_REPORTS
			// NOTE:
			// For Mesh without GATT Proxy, this
			// is the *only* event that is used.
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::leSubEvent(subevent)
				);
			#endif
			return parseLeAdvertisingReports(
					&p[1],
					length
					);
			break;
		case 0x01:
			// HCI_EV_LE_CONN_COMPLETE
			// LE Connection Complete
//			#ifdef DEBUG_TRACE
			#if 1
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::leSubEvent(subevent)
				);
			#endif
			return parseLeConnectionComplete(
					&p[1],
					length
					);
			break;
		case 0x06:
			// HCI_EV_LE_REMOTE_CONN_PARAM_REQ
			// LE Remote Connection Parameter Request
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::leSubEvent(subevent)
				);
			#endif
			return parseLeRemoteConnectionParameterRequest(
					&p[1],
					length
					);
		case 0x03:
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				Oscl::Bluetooth::Strings::leSubEvent(subevent)
				);
			#endif
			return parseLeConnectionUpdateComplete(
					&p[1],
					length
					);
			break;
		default:
			// LE Meta Event Unhandled
			Oscl::Error::Info::log(
				"%s: Unhandled LE Meta Subevent: (0x%2.2X): \"%s\"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				subevent,
				Oscl::Bluetooth::Strings::leSubEvent(subevent)
				);
			break;
		}

	return false;
	}

void	Part::hciTxNonConnectableAdvPdu(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	len;
	Oscl::Handle<Oscl::Pdu::Fragment>	headerFragment;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	cmd	= 0x01;			// HCI_COMMAND_PKT
		static const uint16_t	opcode	= 0x2008;	// HCI_CMD_LE_SET_ADV_DATA
		static uint8_t			plen	= 32;


		encoder.encode(cmd);
		encoder.encode(opcode);
		encoder.encode(plen);

		len	= pdu->length();

		encoder.encode(len);	// The number of significant octets in the Advertising Data

		headerFragment	= _freeStore.allocFragmentWrapper(
							buffer,
							encoder.length()
							);

		if(!headerFragment){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	static const uint8_t	zeros[31] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00
		};

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: sizeof(zeros): %zu, pdu->length(): %u, len: %u\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		sizeof(zeros),
		pdu->length(),
		len
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	trailerFragment	= _freeStore.allocConstFragment(
						zeros,
						31-(pdu->length())
						);

	if(!trailerFragment){
		Oscl::Error::Info::log(
			"%s: out of trailer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Pdu::Composite*
	composite	= _freeStore.allocCompositeWrapper(*pdu);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->prepend(headerFragment);
	composite->append(trailerFragment);

	_advNonConnectableQ.put(composite);

	if(!_advTxTimerApi.running()){
		processAdvTxQ();
		}
	}

void	Part::hciTxConnectableAdvPdu(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t len;
	Oscl::Handle<Oscl::Pdu::Fragment>	headerFragment;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const uint8_t	cmd	= 0x01;			// HCI_COMMAND_PKT
		static const uint16_t	opcode	= 0x2008;	// HCI_CMD_LE_SET_ADV_DATA
		static uint8_t			plen	= 32;

		encoder.encode(cmd);
		encoder.encode(opcode);
		encoder.encode(plen);

		len	= pdu->length();

		encoder.encode(len);	// The number of significant octets in the Advertising Data

		headerFragment	= _freeStore.allocFragmentWrapper(
							buffer,
							encoder.length()
							);

		if(!headerFragment){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	static const uint8_t	zeros[31] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00
		};

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: sizeof(zeros): %zu, pdu->length(): %u, len: %u\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		sizeof(zeros),
		pdu->length(),
		len
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	trailerFragment	= _freeStore.allocConstFragment(
						zeros,
						31-(pdu->length())
						);

	if(!trailerFragment){
		Oscl::Error::Info::log(
			"%s: out of trailer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Pdu::Composite*
	composite	= _freeStore.allocCompositeWrapper(*pdu);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->prepend(headerFragment);
	composite->append(trailerFragment);

	_advConnectableQ.put(composite);

	if(!_advTxTimerApi.running()){
		processAdvTxQ();
		}
	}

void	Part::processHciCmdTxQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _txBusy: %s\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		_txBusy?"true":"false"
		);
	#endif

	if(_txBusy){
		return;
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= _hciCmdQ.get();

	if(!handle){
		return;
		}

	_hciTxApi.transfer(handle);

	++_hciTxCount;

	_txBusy	= true;
	}


void	Part::hciCmdTxPdu(Oscl::Pdu::Pdu* pdu) noexcept{
	_hciCmdQ.put(pdu);
	processHciCmdTxQ();
	}

bool	Part::send(Oscl::Pdu::Pdu* pdu) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: pdu->length(): %u\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		pdu->length()
		);
	#endif

	// This just ensures that the pdu is released
	// if we take an early return.
	Oscl::Handle<Oscl::Pdu::Pdu>	handle(pdu);

	if(pdu->length() > _dataPacketLength){
		Oscl::Error::Info::log(
			"%s: ACL PDU length (%u) greater than _dataPacketLength (%u)\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			pdu->length(),
			_dataPacketLength
			);
		// For now, we're going to send it anyway.
		}

	if(!_activeConnections.first()){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No connections, drop and flush.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		#endif

		// No connections
		// Flush the queue.
		flushAclTxQueue();
		return true;
		}

	static const	uint8_t	cmd	= 0x02; // HCI_ACLDATA_PKT

	Oscl::Handle<Oscl::Pdu::Fragment>
	header	= _freeStore.allocConstFragment(
				&cmd,
				sizeof(cmd)
				);

	if(!header){
		Oscl::Error::Info::log(
			"%s: out of fragment memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragment memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	composite->append(header);
	composite->append(fragment);

	_hciAclTxQ.put(&*composite);

	processAclTxQueue();

	return false;
	}

void	Part::advBearerTxProv(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);


	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	fragment;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const uint8_t	type	= 0x29;		// <<PB-ADV>>
		uint8_t
		len	= pdu->length() + sizeof(type);

		encoder.encode(len);
		encoder.encode(type);

		fragment	= _freeStore.allocFragmentWrapper(
						buffer,
						encoder.length()
						);

		if(!fragment){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fragment memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocCompositeWrapper(*pdu);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->prepend(fragment);

	hciTxNonConnectableAdvPdu(composite);
	}

void	Part::advBearerTxBeacon(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const uint8_t	type	= 0x2B;		// <<Mesh Beacon>>
		uint8_t
		len	= pdu->length() + sizeof(type);

		encoder.encode(len);
		encoder.encode(type);

		header	= _freeStore.allocFragmentWrapper(
						buffer,
						encoder.length()
						);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fragment memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocCompositeWrapper(*pdu);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->prepend(header);

	hciTxNonConnectableAdvPdu(composite);
	}

void	Part::advBearerTx(
			Oscl::Pdu::Pdu* pdu,
			uint16_t		dst
			) noexcept{

	// FIXME: implement output filter

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"TX <<Mesh Message>>: dst: %4.4X\n",
		dst
		);
	#endif

	advBearerTxMesh(pdu);
	}

void	Part::advBearerTxMesh(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>	fragment;
		{
		Oscl::Encoder::BE::Base
			beEncoder(
				buffer,
				bufferSize
				);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const uint8_t	type	= 0x2A;		// <<Mesh Message>>
		uint8_t
		len	= pdu->length() + sizeof(type);

		encoder.encode(len);
		encoder.encode(type);

		fragment	= _freeStore.allocFragmentWrapper(
						buffer,
						encoder.length()
						);

		if(!fragment){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fragment memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocCompositeWrapper(*pdu);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->prepend(fragment);

	hciTxNonConnectableAdvPdu(composite);
	}

void	Part::advTxTimerExpired() noexcept{

	#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
	Oscl::Error::Info::log(
		"***************** %s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	processAdvTxQ();
	}

void	Part::disconnectTimerExpired() noexcept{

	#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
	Oscl::Error::Info::log(
		"***************** %s: _disconnectHandle: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		_disconnectHandle
		);
	#endif

	if(!_disconnectPending){
		return;
		}

	sendDisconnectCommand(
		_disconnectHandle,
		Oscl::BT::HCI::Error::remoteUserTerminatedConnection
		);

	_disconnectPending	= false;
	_disconnectHandle	= 0;
	}

void	Part::sendHciCmdLeSetAdvEnable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x0A,	// HCI_CMD_LE_SET_ADV_ENABLE LSB
		0x20,	// HCI_CMD_LE_SET_ADV_ENABLE MSB
		0x01,	// length
		0x01	// Enable
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	_advEnabled	= true;

	hciCmdTxPdu(fixed);
	}

/* 7.8.9 LE Set Advertising Enable Command

	Note: There is a possible race condition if the
	Advertising_Enable parameter is set to 0x00 (Disable)
	and the Advertising_Type parameter is 0x00, 0x01, or 0x04.
	The advertisements might not be stopped before a connection is
	created, and therefore both the Command Complete event and
	either an LE Connection Complete event or an LE Enhanced Connection
	Complete event could be generated. This can also occur when high
	duty cycle directed advertising is timed out and this command
	disables advertising.
 */

void	Part::sendHciCmdLeSetAdvDisable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x0A,	// HCI_CMD_LE_SET_ADV_ENABLE LSB
		0x20,	// HCI_CMD_LE_SET_ADV_ENABLE MSB
		0x01,	// length
		0x00	// Disable
		};

	Oscl::Pdu::Fixed*
	fixed	= _freeStore.allocConstFixed(
				cmd,
				sizeof(cmd)
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: out of fixed memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	_advEnabled	= false;

	hciCmdTxPdu(fixed);
	}

void	Part::sendScanResponseData() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	name;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		Oscl::Encoder::State	state;

		encoder.push(state);

		unsigned	nameLen	= strlen(_deviceName);
		static const uint8_t	type	= 0x09;		// <<Complete Local Name>>
		uint8_t
		len	= nameLen + sizeof(type);

		encoder.skip(1);

		encoder.encode(len);
		encoder.encode(type);
		encoder.copyIn(
			_deviceName,
			nameLen
			);

		uint8_t	plen	= encoder.length();

		encoder.pop();

		encoder.encode(plen);

		name	= _freeStore.allocFixed(
						buffer,
						plen
						);

		if(!name){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fragment memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	sendHciCmdLeSetScanResponseData(name);
	}

void	Part::sendHciCmdLeSetScanResponseData(Oscl::Pdu::Pdu* pdu) noexcept{

	static const uint8_t	cmd[] = {
		0x01,	// HCI_COMMAND_PKT
		0x09,	// LE Set Scan Response Data LSB
		0x20	// LE Set Scan Response Data MSB
		};

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	static const uint8_t	zeros[31] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00
		};

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		uint8_t
		len	= sizeof(zeros);

		encoder.copyIn(
			cmd,
			sizeof(cmd)
			);

		encoder.encode(len);

		header	= _freeStore.allocFragmentWrapper(
						buffer,
						encoder.length()
						);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of fragment memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	if(!header){
		Oscl::Error::Info::log(
			"%s: out of fragment memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	trailerFragment	= _freeStore.allocConstFragment(
						zeros,
						31-(pdu->length())
						);

	if(!trailerFragment){
		Oscl::Error::Info::log(
			"%s: out of trailer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragment memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite memory\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	composite->append(header);
	composite->append(fragment);
	composite->append(trailerFragment);

	hciCmdTxPdu(composite);
	}

void	Part::sendConnectableAdv() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= _advConnectableQ.get();

	if(!handle){
		// This should *NEVER* happen
		Oscl::Error::Info::log(
			"%s: Software logic ERROR!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	if(_advParams != Connectable){
		_advParams	= Connectable;
		sendLeSetAdvParametersConnectableScanable();
		}

	/*
		BLUETOOTH SPECIFICATIONVersion 5.0 | Vol 3, Part C page 2105
		Table A.1: Defined GAP timers

		TGAP(adv_fast_interval2)
			100 ms to 150 ms

			Minimum to maximum advertising interval in the
			following GAP Modes on the LE 1M PHY when user
			initiated and sending non-connectable advertising
			events:

			1. Non-Discoverable Mode
			2. Non-Connectable Mode
			3. Limited Discoverable Mode
			4. General Discoverable Mode

			Recommended value

		TGAP(adv_slow_interval)
			1 s to 1.2 s
			Minimum to maximum advertisement interval in any discoverable or con-
			nectable mode when back- ground advertising on the LE 1M PHY
			Recommended value
	 */

	static const unsigned long	connectableAdvWindowInMs	= 1000;

	_advTxTimerApi.restart(connectableAdvWindowInMs);

	hciCmdTxPdu(handle);
	}

void	Part::sendNonConnectableAdv() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	handle	= _advNonConnectableQ.get();

	if(!handle){
		// This should *NEVER* happen
		Oscl::Error::Info::log(
			"%s: Software logic ERROR!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	if(_advParams != Unconnectable){
		_advParams	= Unconnectable;
		sendLeSetAdvParametersNonConnectable();
		}

	/*
		BLUETOOTH SPECIFICATIONVersion 5.0 | Vol 3, Part C page 2105
		Table A.1: Defined GAP timers

		TGAP(adv_fast_interval2)
			100 ms to 150 ms

			Minimum to maximum advertising interval in the
			following GAP Modes on the LE 1M PHY when user
			initiated and sending non-connectable advertising
			events:

			1. Non-Discoverable Mode
			2. Non-Connectable Mode
			3. Limited Discoverable Mode
			4. General Discoverable Mode

			Recommended value

	 */
	static const unsigned long	advWindowInMs	= 150;

	_advTxTimerApi.restart(advWindowInMs);

	hciCmdTxPdu(handle);
	}

void	Part::processAdvTxQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	bool
	connectableReady	= _advConnectableQ.first();

	bool
	nonConnectableReady	= _advNonConnectableQ.first();

	if(_advEnabled){
		if(!(connectableReady || nonConnectableReady)){

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: nothing in either queue.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif

			if(_nonConnectableActive){
				sendHciCmdLeSetAdvDisable();
				}

			return;
			}

		sendHciCmdLeSetAdvDisable();
		}

	if(nonConnectableReady){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: nonConnectableReady\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		#endif

		sendNonConnectableAdv();
		sendHciCmdLeSetAdvEnable();
		_nonConnectableActive	= true;
		}
	else if(connectableReady){
		#ifdef EVENT_TRACE
		Oscl::Error::Info::log(
			"%s: connectableReady\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		#endif

		sendConnectableAdv();
		sendHciCmdLeSetAdvEnable();
		_nonConnectableActive	= false;
		}
	}

void	Part::flushAclTxQueue() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>	handle;
	while((handle = _hciAclTxQ.get())){
		handle	= 0;
		}
	}

static void	dumpPDU(Oscl::Pdu::Pdu* pdu) noexcept{
	uint8_t	buffer[256];

	unsigned
	len	= pdu->read(
			buffer,
			sizeof(buffer),
			0
			);
	Oscl::Error::Info::log(
		"HCI TX Raw:\n"
		);
	Oscl::Error::Info::hexDump(
		buffer,
		len
		);
	}

void	Part::processAclTxQueue() noexcept{

	while(_numPendingLeDataPackets < _numLeDataPackets){
		Oscl::Handle<Oscl::Pdu::Pdu>
		handle	= _hciAclTxQ.get();

		if(!handle){
			return;
			}

		++_numPendingLeDataPackets;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _numPendingLeDataPackets: %u.\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			_numPendingLeDataPackets
			);
		#endif

		_hciTxApi.transfer(handle);

		if(_logTxPackets){
			dumpPDU(handle);
			}

		++_hciTxCount;
		}
	}

void	Part::processHostControllerState() noexcept{

	if(_alwaysOnDevice){
		// This device does not sleep.
		// The HC is always enabled.
		return;
		}

	if(_hostControllerDisabled){
		// The host controller is currently disabled.
		if(_inhibitTransitionToHostControllerDisabled){
			_hostControllerDisabled	= false;
			// Here, we need to enable / restart the host controller.
			sendLeSetScanEnable();
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Enabling the host controller.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			}
		}
	else {
		// The host controller is currently enabled.
		if(!_inhibitTransitionToHostControllerDisabled){
			// Here, we need to disable / stop the host controller.
			sendLeSetScanDisable();
			_hostControllerDisabled	= true;
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Disabling the host controller.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			#endif
			}
		}
	}

void	Part::preventTransitionToHcDisable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	++_inhibitTransitionToHostControllerDisabled;

	processHostControllerState();
	}

void	Part::allowTransitionToHciDisable() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	#endif

	if(!_inhibitTransitionToHostControllerDisabled){
		Oscl::ErrorFatal::logAndExit(
			"%s: unbalanced inhibit!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	--_inhibitTransitionToHostControllerDisabled;

	processHostControllerState();
	}

// Oscl::BT::Conn::TX::Api::send()
bool	Part::send(
			uint8_t			pbFlag,
			uint8_t			bcFlag,
			uint16_t		connHandle,
			uint16_t		dataTotalLength,
			Oscl::Pdu::Pdu*	pdu
			) noexcept{
	/*	The pdu is an L2CAP fragment as described
		in the Bluetooth Specification Vol 3, Part A
		Section 3.1 Figure 3.1 .

		This operation encapsulates the L2CAP pdu
		as an ACL Data Packet described in
		Bluetooth Sepecification Vol 2, Part E
		Section 5.4.2 Figure 5.2.

		It also prepends the HCI ACL opcode and
		places the result in the ACL TX queue.
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _connHandle: 0x%4.4X, pdu->length(): %u\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		connHandle,
		pdu->length()
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of header memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	static const	uint8_t	cmd	= 0x02; // HCI_ACLDATA_PKT

	Oscl::Handle<Oscl::Pdu::Fragment>	header;
		{
		Oscl::Encoder::LE::Base
		leEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		uint16_t
		handleAndFlags	= bcFlag;
		handleAndFlags	<<= 2;
		handleAndFlags	|= pbFlag;
		handleAndFlags	<<= 12;
		handleAndFlags	|= connHandle;

		encoder.encode(cmd);

		encoder.encode(handleAndFlags);
		encoder.encode(dataTotalLength);

		header	= _freeStore.allocFragmentWrapper(
					buffer,
					encoder.length()
					);

		if(!header){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return true;
			}
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocComposite();

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of header memory.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return true;
		}

	composite->append(header);
	composite->append(fragment);

	/*
		BLUETOOTH SPECIFICATION Version 5.0 | Vol 2, Part E page 733

		5.4.2 HCI ACL Data Packets

		Hosts and Controllers shall be able to accept
		HCI ACL Data Packets with up to 27 bytes of
		data excluding the HCI ACL Data Packet header on
		Connection_Handles associated with an LE-U logical
		link.The HCI ACL Data Packet header is the first 4
		octets of the packet.
	 */
	if(fragment->getLength() > _dataPacketLength){
		Oscl::Error::Info::log(
			"%s: ACL PDU length (%u) greater than _dataPacketLength (%u)\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			pdu->length(),
			_dataPacketLength
			);
		// For now, we're going to send it anyway.
		}

	_hciAclTxQ.put(&*composite);

	processAclTxQueue();

	return false;
	}

unsigned	Part::maxPduLen() noexcept{
	return _dataPacketLength;
	}


