/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_hci_error_codesh_
#define _oscl_bluetooth_hci_error_codesh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace HCI {
/** */
namespace Error {

/** */
constexpr uint8_t	success	= 0x00;

/** */
constexpr uint8_t	unknownHciCommand	= 0x01;

/** */
constexpr uint8_t	unknownConnectionIdentifier	= 0x02;

/** */
constexpr uint8_t	hardwareFailure	= 0x03;

/** */
constexpr uint8_t	pageTimeout	= 0x04;

/** */
constexpr uint8_t	authenticationFailure	= 0x05;

/** */
constexpr uint8_t	pinOrKeyMissing	= 0x06;

/** */
constexpr uint8_t	memoryCapacityExceeded	= 0x07;

/** */
constexpr uint8_t	connectionTimeout	= 0x08;

/** */
constexpr uint8_t	connectionLimitExceeded	= 0x09;

/** */
constexpr uint8_t	synchronousConnectionLimitToADeviceExceeded	= 0x0A;

/** */
constexpr uint8_t	connectionAlreadyExists	= 0x0B;

/** */
constexpr uint8_t	commandDisallowed	= 0x0C;

/** */
constexpr uint8_t	connectionRejectedDueToLimitdResources	= 0x0D;

/** */
constexpr uint8_t	connectionRejectedDueToSecurityReasons	= 0x0E;

/** */
constexpr uint8_t	connectionRejectedDueToUnacceptableBD_ADDR	= 0x0F;

/** */
constexpr uint8_t	connectionAcceptTimeoutExceeded	= 0x10;

/** */
constexpr uint8_t	unsupportedFeatureOrParameterValue	= 0x11;

/** */
constexpr uint8_t	invalidHciCommandParameters	= 0x12;

/** */
constexpr uint8_t	remoteUserTerminatedConnection	= 0x13;

/** */
constexpr uint8_t	remoteDeviceTerminatedConnectionDueToLowResources	= 0x14;

/** */
constexpr uint8_t	remoteDeviceTerminatedConnectionDueToPowerOff	= 0x15;

/** */
constexpr uint8_t	connectionTerminatedByLocalHost	= 0x16;

/** */
constexpr uint8_t	repeatedAttempts	= 0x17;

/** */
constexpr uint8_t	pairingNotAllowed	= 0x18;

/** */
constexpr uint8_t	unknownLmpPDU	= 0x19;

/** */
constexpr uint8_t	unsupportedRemoteFeature	= 0x1A;

/** */
constexpr uint8_t	scoOffsetRejected	= 0x1B;

/** */
constexpr uint8_t	scoIntervalRejected	= 0x1C;

/** */
constexpr uint8_t	scoAirModeRejected	= 0x1D;

/** */
constexpr uint8_t	invalidLmpOrLlParameters	= 0x1E;

/** */
constexpr uint8_t	unspecifiedError	= 0x1F;

/** */
constexpr uint8_t	unsupportedLmpOrLlParameterValue	= 0x20;

/** */
constexpr uint8_t	roleChangeNotAllowed	= 0x21;

/** */
constexpr uint8_t	lmpOrLlResponseTimeout	= 0x22;

/** */
constexpr uint8_t	lmpErrorTransactionCollitionOrLlProcedureCollision	= 0x23;

/** */
constexpr uint8_t	lmpPduNotAllowed	= 0x24;

/** */
constexpr uint8_t	encryptionModeNotAcceptable	= 0x25;

/** */
constexpr uint8_t	linkKeyCannotBeChanged	= 0x26;

/** */
constexpr uint8_t	requestedQosNotSupported	= 0x27;

/** */
constexpr uint8_t	instantPassed	= 0x28;

/** */
constexpr uint8_t	pairingWithUnitKeyNotSupported	= 0x29;

/** */
constexpr uint8_t	differentTransactionCollision	= 0x2A;

/** */
constexpr uint8_t	qosUnacceptableParameter	= 0x2C;

/** */
constexpr uint8_t	qosRejected	= 0x2D;

/** */
constexpr uint8_t	channelClassificationNotSupported	= 0x2E;

/** */
constexpr uint8_t	insufficientSecurity	= 0x2F;

/** */
constexpr uint8_t	parameterOutOfMandatoryRange	= 0x30;

/** */
constexpr uint8_t	roleSwitchPending	= 0x32;

/** */
constexpr uint8_t	reservedSlotViolation	= 0x34;

/** */
constexpr uint8_t	roleSwitchFailed	= 0x35;

/** */
constexpr uint8_t	extendedInquiryResponseTooLarge	= 0x36;

/** */
constexpr uint8_t	secureSimplePairingNotSupportedByHost	= 0x37;

/** */
constexpr uint8_t	hostBusyPairing	= 0x38;

/** */
constexpr uint8_t	connectionRejectedDueToNoSuitableChannelFound	= 0x39;

/** */
constexpr uint8_t	controllerBusy	= 0x3A;

/** */
constexpr uint8_t	unacceptableConnectionParameters	= 0x3B;

/** */
constexpr uint8_t	advertisingTimeout	= 0x3C;

/** */
constexpr uint8_t	connectionTerminatedDueToMicFailure	= 0x3D;

/** */
constexpr uint8_t	connectionFailedToBeEstablished	= 0x3E;

/** */
constexpr uint8_t	macConnectionFailed	= 0x3F;

/** */
constexpr uint8_t	coarseClockAdjustmentRejectedButWillTryToAdjustUsingClockDragging	= 0x40;

/** */
constexpr uint8_t	type0SubmapNotDefined	= 0x41;

/** */
constexpr uint8_t	unknownAdvertisingIdentifier	= 0x42;

/** */
constexpr uint8_t	limitReached	= 0x43;

/** */
constexpr uint8_t	operationCancelledByHost	= 0x44;

}
}
}
}
#endif
