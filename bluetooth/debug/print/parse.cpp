/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "parse.h"
#include <stdio.h>
#include "oscl/error/info.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/uuid/string.h"
#include "oscl/bluetooth/uuid/base.h"
#include "oscl/bluetooth/att/size.h"
#include <inttypes.h>

using namespace Oscl::Bluetooth::Debug::Print;
using namespace Oscl::Bluetooth::Strings;

static bool	suppressAdvertisments	= false;

void	Oscl::Bluetooth::Debug::Print::suppressAdvertisements() noexcept{
	suppressAdvertisments	= true;
	}

static void	parseHCI_MON_NEW_INDEX(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{

	Oscl::Error::Info::log(
		"%s: HCI_MON_NEW_INDEX, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		payload,
		len
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	type;
	uint8_t	bus;
	uint8_t	bdaddr[6];
	char	name[9];

	decoder.decode(type);
	decoder.decode(bus);
	decoder.copyOut(bdaddr,6);
	decoder.copyOut(name,8);
	name[sizeof(name)-1]	= '\0';

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\ttype: 0x%2.2X\n"
		"\tbus: 0x%2.2X\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tname: \"%s\"\n"
		"\n",
		type,
		bus,
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		name
		);
	}

static void	parseHCI_MON_DEL_INDEX(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_DEL_INDEX, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseClearAllFiltersType(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\tflt_type: (0x00) Clear All Filters\n"
		""
		);
	}

static const char*	inquiryResultFilterConditionType(uint8_t cond_type) noexcept{
	switch(cond_type){
		case 0x00:
			return "All Devices";
		case 0x01:
			return "Specific Class of Device";
		case 0x02:
			return "Specific BD_ADDR";
		default:
			return "Unknown";
		}
	}

static void	parseInquiryResultFilterConditionTypeAllDevices(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 907
	// There is not condition octet for this

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x01) Inquiry Result Filter\n"
		"\t\tcond_type: (0x00) %s\n"
		"",
		inquiryResultFilterConditionType(0)
		);
	}

static void	parseInquiryResultFilterConditionTypeSpecificClassOfDevice(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 908
	uint32_t	class_of_device;
	uint32_t	class_of_device_mask;

	decoder.decode24Bit(class_of_device);
	decoder.decode24Bit(class_of_device_mask);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x01) Inquiry Result Filter\n"
		"\t\tcond_type: (0x01) %s\n"
		"\t\tclass of device: 0x%6.6X %s\n"
		"\t\tclass of device mask: 0x%6.6X\n"
		"",
		inquiryResultFilterConditionType(1),
		class_of_device,
		classOfDevice(class_of_device),
		class_of_device_mask
		);
	}

static void	parseInquiryResultFilterConditionTypeSpecificBD_ADDR(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 908

	uint8_t	bdaddr[6];

	decoder.copyOut(bdaddr,6);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x01) Inquiry Result Filter\n"
		"\t\tcond_type: (0x02) %s\n"
		"\tbd_addr: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"",
		inquiryResultFilterConditionType(2),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseInquiryResultFilterConditionType(
				Oscl::Decoder::Api&	decoder
				) noexcept{

	// There is no condition for Inquiry_Result_Filter_Condition_Type.
	uint8_t	cond_type;

	decoder.decode(cond_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	switch(cond_type){
		case 0x00:
			return parseInquiryResultFilterConditionTypeAllDevices(decoder);
		case 0x01:
			return parseInquiryResultFilterConditionTypeSpecificClassOfDevice(decoder);
		case 0x02:
			return parseInquiryResultFilterConditionTypeSpecificBD_ADDR(decoder);
		default:
			break;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x01) Inquiry Result Filter\n"
		"\t\tcond_type: (0x%2.2X) %s\n"
		"",
		cond_type,
		inquiryResultFilterConditionType(cond_type)
		);
	}

static const char*	connectionSetupFilterConditionType(uint8_t cond_type) noexcept{
	switch(cond_type){
		case 0x00:
			return "All Devices";
		case 0x01:
			return "Specific Class of Device";
		case 0x02:
			return "Specific BD_ADDR";
		default:
			return "Unknown";
		}
	}

static const char*	autoAcceptFlag(uint8_t flag) noexcept{
	switch(flag){
		case 0x00:
			return "Do NOT Auto accept the connection";
		case 0x01:
			return "Do Auto accept the connection with role switch disabled";
		case 0x02:
			return "Do Auto accept the connection with role switch enabled";
		default:
			return "Unknown";
		}
	}

static void	parseConnectionSetupFilterConditionTypeAllowAll(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 908
	// Connection_Setup_Filter_Condition_Type = 0x00

	uint8_t	auto_accept_flag;

	decoder.decode(auto_accept_flag);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x02) Connection Setup Filter Condition Type\n"
		"\t\tcond_type: (0x00) %s\n"
		"\t\tAuto_Accept_Flag: (0x%2.2X) %s\n"
		"",
		connectionSetupFilterConditionType(0),
		auto_accept_flag,
		autoAcceptFlag(auto_accept_flag)
		);
	}

static void	parseConnectionSetupFilterConditionTypeAllowSpecificClassOfDevice(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 909
	// Connection_Setup_Filter_Condition_Type = 0x01

	uint32_t	class_of_device;
	uint32_t	class_of_device_mask;
	uint8_t	auto_accept_flag;

	decoder.decode24Bit(class_of_device);
	decoder.decode24Bit(class_of_device_mask);
	decoder.decode(auto_accept_flag);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x02) Connection Setup Filter Condition Type\n"
		"\t\tcond_type: (0x01) %s\n"
		"\t\tClassOfDevice: (0x%6.6X) %s\n"
		"\t\tClassOfDeviceMask: (0x%6.6X)\n"
		"\t\tAuto_Accept_Flag: (0x%2.2X) %s\n"
		"",
		connectionSetupFilterConditionType(1),
		class_of_device,
		classOfDevice(class_of_device),
		class_of_device_mask,
		auto_accept_flag,
		autoAcceptFlag(auto_accept_flag)
		);
	}

static void	parseConnectionSetupFilterConditionTypeAllowSpecificBD_ADDR(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	// page 909
	// Connection_Setup_Filter_Condition_Type = 0x02

	uint8_t	bdaddr[6];
	uint8_t	auto_accept_flag;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(auto_accept_flag);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tflt_type: (0x02) Connection Setup Filter Condition Type\n"
		"\t\tcond_type: (0x02) %s\n"
		"\t\tbd_addr: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\t\tAuto_Accept_Flag: (0x%2.2X) %s\n"
		"",
		connectionSetupFilterConditionType(1),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		auto_accept_flag,
		autoAcceptFlag(auto_accept_flag)
		);
	}

static void	parseConnectionSetupFilterConditionType(
				Oscl::Decoder::Api&	decoder
				) noexcept{

	// page 907

	uint8_t	cond_type;

	decoder.decode(cond_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	switch(cond_type){
		case 0x00:
			return parseConnectionSetupFilterConditionTypeAllowAll(decoder);
		case 0x01:
			return parseConnectionSetupFilterConditionTypeAllowSpecificClassOfDevice(decoder);
		case 0x02:
			return parseConnectionSetupFilterConditionTypeAllowSpecificBD_ADDR(decoder);
		default:
			break;
		}

	Oscl::Error::Info::log(
		"%s: cond_type: (0x%2.2X) %s"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		cond_type,
		connectionSetupFilterConditionType(cond_type)
		);
	}

static void	parseHCI_OP_SET_EVENT_FLT(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_SET_EVENT_FLT\n"
		);

	// page 905

	uint8_t	flt_type;

	decoder.decode(flt_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	switch(flt_type){
		case 0x00:
			return parseClearAllFiltersType(decoder);
		case 0x01:
			return parseInquiryResultFilterConditionType(decoder);
		case 0x02:
			return parseConnectionSetupFilterConditionType(decoder);
		default:
			return;
		}

	Oscl::Error::Info::log(
		"%s: flt_type: (0x%2.2X) Unknown\n"
		"",
		OSCL_PRETTY_FUNCTION_DETAIL,
		flt_type
		);
	}

static void	parseHCI_OP_WRITE_CA_TIMEOUT(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_CA_TIMEOUT\n"
		);

	// Write Connection Accept Timeout Command
	// page 710
	// page 925

	uint16_t	timeout;

	decoder.decode(timeout);

	double
	timeInMilliSeconds	= 0.625 * timeout;

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\ttimeout: (0x%4.4X) %f s\n",
		timeout,
		timeInMilliSeconds*1000
		);
	}

static void	parseHCI_OP_LE_SET_ADV_ENABLE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_ADV_ENABLE\n"
		);
	// LE Set Advertising Enable
	// page 1259

	uint8_t		advertising_enable;

	decoder.decode(advertising_enable);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tadvertising_enable: (0x%2.2X) %s\n"
		"",
		advertising_enable,
		advertising_enable?"Advertising is enabled.":"Advertising is disabled."
		);
	}

static void	parseCmdHCI_OP_LE_SET_SCAN_PARAM(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_SCAN_PARAM\n"
		);
	// LE Set Scan Parameters
	// page 1261

	uint8_t		le_scan_type;
	uint16_t	le_scan_interval;
	uint16_t	le_scan_window;
	uint8_t		own_address_type;
	uint8_t		scanning_filter_policy;

	decoder.decode(le_scan_type);
	decoder.decode(le_scan_interval);
	decoder.decode(le_scan_window);
	decoder.decode(own_address_type);
	decoder.decode(scanning_filter_policy);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	double	intervalInSeconds	= le_scan_interval * 0.000625;
	double	windowInSeconds	= le_scan_window * 0.000625;

	Oscl::Error::Info::log(
		"\t\tle_scan_type: (0x%2.2X) %s\n"
		"\t\tle_scan_interval: (0x%4.4X) %f seconds\n"
		"\t\tle_scan_window: (0x%4.4X) %f seconds\n"
		"\t\town_address_type: (0x%2.2X) %s\n"
		"\t\tscanning_filter_policy: (0x%2.2X) %s\n"
		"",
		le_scan_type,
		leScanType(le_scan_type),
		le_scan_interval,
		intervalInSeconds,
		le_scan_window,
		windowInSeconds,
		own_address_type,
		leAddressType(own_address_type),
		scanning_filter_policy,
		leScanningFilterPolicy(scanning_filter_policy)
		);
	}

#if 0
static void	parseHCI_OP_LE_SET_SCAN_PARAM(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_SCAN_PARAM\n"
		);
	// LE Set Scan Parameters
	// page 1261

	uint8_t	status;

	decoder.decode(status);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"",
		status,
		statusCode(status)
		);
	}
#endif

static void	parseCmdHCI_OP_LE_SET_SCAN_ENABLE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_SCAN_ENABLE\n"
		);
	// LE Set Scan Enable
	// page 1264

	uint8_t	le_scan_enable;
	uint8_t	filter_duplicates;

	decoder.decode(le_scan_enable);
	decoder.decode(filter_duplicates);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tle_scan_enable: 0x%2.2X\n"
		"\t\tfilter_duplicates: 0x%2.2X\n"
		"",
		le_scan_enable,
		filter_duplicates
		);
	}

#if 0
static void	parseHCI_OP_LE_SET_SCAN_ENABLE(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_SCAN_ENABLE\n"
		);
	// LE Set Scan Enable
	// page 1264

	uint8_t	status;

	decoder.decode(status);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"",
		status,
		statusCode(status)
		);
	}
#endif

static void	parseHCI_OP_LE_READ_REMOTE_FEATURES(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_REMOTE_FEATURES\n"
		);
	// LE Read Remote Features
	// page 1285

	uint16_t	connection_handle;

	decoder.decode(connection_handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tconnection_handle: 0x%4.4X\n"
		"",
		connection_handle
		);
	}

static void	parseHCI_OP_LE_START_ENC(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_REMOTE_FEATURES\n"
		);
	}

static void	parseHCI_OP_LE_LTK_REPLY(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_LTK_REPLY\n"
		);
	}

static void	parseHCI_OP_LE_LTK_NEG_REPLY(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_LTK_NEG_REPLY\n"
		);
	}

static void	parseHCI_OP_LE_READ_SUPPORTED_STATES(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_SUPPORTED_STATES\n"
		);
	}

static void	parseHCI_OP_LE_CONN_PARAM_REQ_REPLY(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_CONN_PARAM_REQ_REPLY\n"
		);
	}

static void	parseHCI_OP_LE_CONN_PARAM_REQ_NEG_REPLY(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_CONN_PARAM_REQ_NEG_REPLY\n"
		);
	}

static void	parseHCI_OP_LE_SET_DATA_LEN(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_DATA_LEN\n"
		);
	}

static void	parseHCI_OP_LE_READ_DEF_DATA_LEN(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_DEF_DATA_LEN\n"
		);
	}

static void	parseHCI_OP_LE_WRITE_DEF_DATA_LEN(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_WRITE_DEF_DATA_LEN\n"
		);
	}

static void	parseHCI_OP_LE_READ_MAX_DATA_LEN(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_MAX_DATA_LEN\n"
		);
	}

static void	parseHCI_OP_LE_SET_DEFAULT_PHY(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_REMOTE_FEATURES\n"
		);
	}

static void	parseHCI_OP_DISCONNECT(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_DISCONNECT\n"
		);
	// Disconnect
	// page 777

	uint16_t	connection_handle;
	uint8_t		reason;

	decoder.decode(connection_handle);
	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tconnection_handle: 0x%4.4X\n"
		"\t\treason: (0x%4.4X) \"%s\"\n"
		"",
		connection_handle,
		reason,
		statusCode(reason)
		);
	}

static void	parseHCI_OP_WRITE_DEF_LINK_POLICY(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_DEF_LINK_POLICY\n"
		);

	// Disconnect
	// page 896

	uint16_t	default_link_policy_settings;

	decoder.decode(default_link_policy_settings);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tdefault_link_policy_settings: 0x%4.4X\n"
		"",
		default_link_policy_settings
		);

	for(unsigned i=0;i<16;++i){
		bool
		set	= default_link_policy_settings & (1<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			linkPolicy(i)
			);
		}
	}

static void	parseHCI_OP_READ_PAGE_SCAN_ACTIVITY(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_PAGE_SCAN_ACTIVITY\n"
		);
	// Disconnect
	// page 896

	uint8_t		status;
	uint16_t	page_scan_interval;
	uint16_t	page_scan_window;

	decoder.decode(status);
	decoder.decode(page_scan_interval);
	decoder.decode(page_scan_window);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\t\tpage_scan_interval: 0x%4.4X\n"
		"\t\tpage_scan_window: 0x%4.4X\n"
		"",
		status,
		statusCode(status),
		page_scan_interval,
		page_scan_window
		);
	}

static void	parseHCI_OP_READ_PAGE_SCAN_TYPE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_PAGE_SCAN_TYPE\n"
		);

	// Disconnect
	// page 974

	uint8_t		status;
	uint8_t		page_scan_type;

	decoder.decode(status);
	decoder.decode(page_scan_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\t\tpage_scan_type: (0x%2.2X) \"%s\"\n"
		"",
		status,
		statusCode(status),
		page_scan_type,
		pageScanType(page_scan_type)
		);
	}

static void	parseHCI_OP_LE_SET_EVENT_MASK(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_EVENT_MASK\n"
		);
	// page 1245

	uint64_t	mask;

	decoder.decode(mask);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tmask: 0x%16.16llX:\n",
		(long long unsigned int)mask
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= mask & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			leEventMask(i)
			);
		}
	}

static void	parseHCI_OP_LE_READ_BUFFER_SIZE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_BUFFER_SIZE\n"
		);
	}

static void	parseHCI_OP_LE_READ_LOCAL_FEATURES(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_LOCAL_FEATURES\n"
		);
	}

static void	parseHCI_OP_LE_SET_RANDOM_ADDR(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_RANDOM_ADDR\n"
		);
	}

static void	parseHCI_OP_LE_SET_ADV_PARAM(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_ADV_PARAM\n"
		);

	// page 1251

	uint16_t	advertising_interval_min;
	uint16_t	advertising_interval_max;
	uint8_t		advertising_type;
	uint8_t		own_address_type;
	uint8_t		peer_address_type;
	uint8_t		peer_address[6];
	uint8_t		advertising_channel_map;
	uint8_t		filter_policy;

	decoder.decode(advertising_interval_min);
	decoder.decode(advertising_interval_max);
	decoder.decode(advertising_type);
	decoder.decode(own_address_type);
	decoder.decode(peer_address_type);
	decoder.copyOut(peer_address,sizeof(peer_address));
	decoder.decode(advertising_channel_map);
	decoder.decode(filter_policy);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	double	advIntervalMinMs	= advertising_interval_min * 0.625;
	double	advIntervalMaxMs	= advertising_interval_max * 0.625;

	Oscl::Error::Info::log(
		"\t\tadvertising interval min: %f ms\n"
		"\t\tadvertising interval max: %f ms\n"
		"\t\tadvertising_type: (0x%2.2X) \"%s\"\n"
		"\t\town address type: (0x%2.2X) \"%s\"\n"
		"\t\tpeer address type: (0x%2.2X) \"%s\"\n"
		"\t\tpeer address: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\t\tfilter policy: (0x%2.2X) \"%s\"\n"
		"\t\tchannel map: (0x%2.2X):\n"
		"",
		advIntervalMinMs,
		advIntervalMaxMs,
		advertising_type,
		Oscl::Bluetooth::Strings::leAdvertisingType(advertising_type),
		own_address_type,
		Oscl::Bluetooth::Strings::leAddressType(own_address_type),
		peer_address_type,
		Oscl::Bluetooth::Strings::leAddressType(peer_address_type),
		peer_address[5],peer_address[4],peer_address[3],peer_address[2],peer_address[1],peer_address[0],
		filter_policy,
		Oscl::Bluetooth::Strings::leAdvertisingFilterPolicy(filter_policy),
		advertising_channel_map
		);

	for(unsigned i=0;i<8;++i){
		bool
		set	= advertising_channel_map & (1<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			Oscl::Bluetooth::Strings::channelMask(i)
			);
		}
	}

static void	processInvite(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tInvite:\n"
		);

	uint8_t	attentionDuration;
	decoder.decode(attentionDuration);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tattentionDuration: %u\n"
		"",
		attentionDuration
		);
	}

static void	processCapabilities(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tCapabilities:\n"
		);

	uint8_t		nElements;
	uint16_t	algorithms;
	uint8_t		publicKeyType;
	uint8_t		staticOobType;
	uint8_t		outputOobSize;
	uint16_t	outputOobAction;
	uint8_t		inputOobSize;
	uint16_t	inputOobAction;

	decoder.decode(nElements);
	decoder.decode(algorithms);
	decoder.decode(publicKeyType);
	decoder.decode(staticOobType);
	decoder.decode(outputOobSize);
	decoder.decode(outputOobAction);
	decoder.decode(inputOobSize);
	decoder.decode(inputOobAction);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tNumber of Elements: %u\n"
		"\t\t\t\t\tAlgorithms: 0x%4.4X\n"
		"\t\t\t\t\tPublic Key Type: 0x%2.2X\n"
		"\t\t\t\t\tStatic OOB Type: 0x%2.2X\n"
		"\t\t\t\t\tOutput OOB Size: 0x%2.2X\n"
		"\t\t\t\t\tOutput OOB Action: 0x%4.4X\n"
		"\t\t\t\t\tInput OOB Size: 0x%2.2X\n"
		"\t\t\t\t\tInput OOB Action: 0x%4.4X\n"
		"",
		nElements,
		algorithms,
		publicKeyType,
		staticOobType,
		outputOobSize,
		outputOobAction,
		inputOobSize,
		inputOobAction
		);
	}

static const char*	algorithmType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "IPS P-256 Elliptic Curve";
		default:
			return "Unknown";
		}
	}

static const char*	publicKeyType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "No OOB Public Key is used.";
		case 0x01:
			return "OOB Public Key is used.";
		default:
			return "Unknown";
		}
	}

static const char*	authenticationType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "No OOB authentication is used.";
		case 0x01:
			return "Static OOB authentication is used.";
		case 0x02:
			return "Output OOB authentication is used.";
		case 0x03:
			return "Input OOB authentication is used.";
		default:
			return "Unknown";
		}
	}

static const char*	outputOobActionType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Blink";
		case 0x01:
			return "Beep";
		case 0x02:
			return "Vibrate";
		case 0x03:
			return "Output Numeric";
		case 0x04:
			return "Output Alphanumeric";
		default:
			return "Unknown";
		}
	}

static const char*	inputOobActionType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Push";
		case 0x01:
			return "Twist";
		case 0x02:
			return "Input Numeric";
		case 0x03:
			return "Input Alphanumeric";
		default:
			return "Unknown";
		}
	}

static const char*	actionType(uint8_t methodType, uint8_t type) noexcept{
	switch(methodType){
		case 0x00:
		case 0x01:
			return "None";
		case 0x02:
			return outputOobActionType(type);
		case 0x03:
			return inputOobActionType(type);
		default:
			return "Unknown";
		}
	}

static void	processStart(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tStart:\n"
		);

	uint8_t	algorithm;
	uint8_t	pubKeyType;
	uint8_t	authenticationMethod;
	uint8_t	authenticationAction;
	uint8_t	authenticationSize;

	decoder.decode(algorithm);
	decoder.decode(pubKeyType);
	decoder.decode(authenticationMethod);
	decoder.decode(authenticationAction);
	decoder.decode(authenticationSize);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tAlgorithm: 0x%2.2X,\"%s\"\n"
		"\t\t\t\t\tPublic Key: 0x%2.2X,\"%s\"\n"
		"\t\t\t\t\tAuthentication Method: 0x%2.2X,\"%s\"\n"
		"\t\t\t\t\tAuthentication Action: 0x%2.2X,\"%s\"\n"
		"\t\t\t\t\tAuthentication Size: %u\n"
		"\n",
		algorithm,
		algorithmType(algorithm),
		pubKeyType,
		publicKeyType(pubKeyType),
		authenticationMethod,
		authenticationType(authenticationMethod),
		authenticationAction,
		actionType(authenticationMethod,authenticationAction),
		authenticationSize
		);

	}

static void	processPublicKey(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tlength %u\n"
		"\t\t\t\tPublicKey:\n"
		"",
		decoder.remaining()
		);

	uint8_t	buffer[32];

	decoder.copyOut(
		buffer,
		14
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		buffer,
		14
		);
	}

static void	processInputComplete(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tInput Complete:\n"
		);
	// No parameters
	}

static void	processConfirmation(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tConfirmation:\n"
		"\t\t\t\t\tData (partial):\n"
		);

	uint8_t	buffer[32];

	decoder.copyOut(
		buffer,
		14	// MTU for start
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		buffer,
		14	// MTU for start
		);
	}

static void	processRandom(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tRandom:\n"
		"\t\t\t\t\tData (partial):\n"
		);

	constexpr unsigned	randomLength	= 16;

	uint8_t	buffer[randomLength];

	decoder.copyOut(
		buffer,
		randomLength
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		buffer,
		randomLength
		);
	}

static void	processData(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tEncrypted Provisioning Data:\n"
		"\t\t\t\t\tData (partial):\n"
		);

	uint8_t	buffer[32];

	decoder.copyOut(
		buffer,
		14
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		buffer,
		14
		);
	}

static void	processComplete(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tComplete:\n"
		);

	// No parameters
	}

static const char*	errorCodeString(uint8_t errorCode) noexcept{

	switch(errorCode){
		case 0x00:
			return "Prohibited";
		case 0x01:
			return "Invalid PDU";
		case 0x02:
			return "Invalid Format";
		case 0x03:
			return "Unexpected PDU";
		case 0x04:
			return "Confirmation Failed";
		case 0x05:
			return "Out of Resources";
		case 0x06:
			return "Decryption Failed";
		case 0x07:
			return "Unexpected Error";
		case 0x08:
			return "Cannot Assign Addresses";
		default:
			return "RFU";
		}
	}

static void	processFailed(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tFailed:\n"
		);

	uint8_t	errorCode;
	decoder.decode(errorCode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tError Code: (0x%2.2X) \"%s\"\n"
		"",
		errorCode,
		errorCodeString(errorCode)
		);
	}

static void	processProvisioningPDU(
				Oscl::Decoder::Api& decoder
				) noexcept{

	uint8_t	octet;
	decoder.decode(octet);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	type	= octet & 0x3F;

	switch(type){
		case 0x00:
			// Invite
			return processInvite(decoder);
		case 0x01:
			// Capabilities
			return processCapabilities(decoder);
		case 0x02:
			// Start
			return processStart(decoder);
		case 0x03:
			// Public Key
			return processPublicKey(decoder);
		case 0x04:
			// Input Complete
			return processInputComplete(decoder);
		case 0x05:
			// Confirmation
			return processConfirmation(decoder);
		case 0x06:
			// Random
			return processRandom(decoder);
		case 0x07:
			// Data
			return processData(decoder);
		case 0x08:
			// Complete
			return processComplete(decoder);
		case 0x09:
			// Failed
			return processFailed(decoder);
		default:
			Oscl::Error::Info::log(
				"%s: Unrecognized type (0x%2.2X)!\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				type
				);
			break;
		}

	uint8_t	buffer[128];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		buffer,
		remaining
		);

	Oscl::Error::Info::hexDump(
		buffer,
		remaining
		);
	}

static const char*	proxyFilterType(uint8_t filterType) noexcept{
	switch(filterType){
		case 0x00:
			return "White list filter";
		case 0x01:
			return "White list filter";
		default:
			return "Unknown";
		}
	}

static void	processSetFilterType(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tSet Filter Type:\n"
		);

	uint8_t	filterType;
	decoder.decode(filterType);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tFilter Type: (0x%2.2X), %s\n"
		"",
		filterType,
		proxyFilterType(filterType)
		);
	}

static void	processAddAddressesToFilter(
				Oscl::Decoder::Api& decoder
				) noexcept{
	unsigned
	nAddresses	= decoder.remaining()/2;

	Oscl::Error::Info::log(
		"\t\t\t\tAdd Addresses To Filter:\n"
		"\t\t\t\t\tremaining: %u\n"
		"\t\t\t\t\tnAddresses: %u\n"
		"\t\t\t\t\tAddress Array:\n"
		"",
		decoder.remaining(),
		nAddresses
		);

	for(unsigned i=0;i<nAddresses;++i){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			"\t\t\t\t\t\t[%u]: 0x%4.4X\n"
			"",
			i,
			address
			);
		}
	}

static void	processRemoveAddressesFromFilter(
				Oscl::Decoder::Api& decoder
				) noexcept{
	unsigned
	nAddresses	= decoder.remaining()/2;

	Oscl::Error::Info::log(
		"\t\t\t\tRemove Addresses From Filter:\n"
		"\t\t\t\t\tremaining: %u\n"
		"\t\t\t\t\tnAddresses: %u\n"
		"\t\t\t\t\tAddress Array:\n"
		"",
		decoder.remaining(),
		nAddresses
		);

	for(unsigned i=0;i<nAddresses;++i){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			"\t\t\t\t\t\t[%u]: 0x%4.4X\n"
			"",
			i,
			address
			);
		}
	}

static void	processFilterStatus(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\t\t\t\tFilter Status:\n"
		);

	uint8_t		filterType;
	uint16_t	listSize;
	decoder.decode(filterType);
	decoder.decode(listSize);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\t\tFilterType: (0x%2.2X), %s\n"
		"\t\t\t\t\tListSize: (0x%4.4X), %u\n"
		"",
		filterType,
		proxyFilterType(filterType),
		listSize,
		listSize
		);
	}

static void	processProxyConfiguration(
				Oscl::Decoder::Api& decoder
				) noexcept{

	uint8_t	octet;
	decoder.decode(octet);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	type	= octet & 0x3F;

	switch(type){
		case 0x00:
			// Set Filter Type
			return processSetFilterType(decoder);
		case 0x01:
			// Add Addresses To Filter
			return processAddAddressesToFilter(decoder);
		case 0x02:
			// Remove Addresses From Filter
			return processRemoveAddressesFromFilter(decoder);
		case 0x03:
			// Filter Status
			return processFilterStatus(decoder);
		default:
			Oscl::Error::Info::log(
				"%s: Unrecognized type (0x%2.2X)!\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				type
				);
			break;
		}

	uint8_t	buffer[128];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		buffer,
		remaining
		);

	Oscl::Error::Info::log(
		"%s: Unrecognized Type dump:\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);

	Oscl::Error::Info::hexDump(
		buffer,
		remaining
		);
	}

static void	parseTransactionStart(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber,
				uint8_t			segN
				) noexcept{

	uint16_t	totalLength;
	uint8_t		fcs;
	decoder.decode(totalLength);
	decoder.decode(fcs);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	unsigned	remaining	= decoder.remaining();

	Oscl::Error::Info::log(
		"\n"
		"\t\t\tTransaction Start:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"\t\t\t\tsegN: 0x%2.2X\n"
		"\t\t\t\ttotalLength: %u, (0x%4.4X)\n"
		"\t\t\t\tfcs: 0x%2.2X\n"
		"\t\t\t\tremaining: %u\n"
		"",
		linkID,
		transactionNumber,
		segN,
		totalLength,
		totalLength,
		fcs,
		remaining
		);

	if(totalLength <= remaining){
		remaining	= totalLength;
		}

	if(!remaining){
		return;
		}

	processProvisioningPDU(decoder);
	}

static void	parseTransactionAcknowledgement(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber
				) noexcept{
	Oscl::Error::Info::log(
		"\n"
		"\t\t\tTransaction Acknowledgement:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"",
		linkID,
		transactionNumber
		);
	}

static void	parseTransactionContinuation(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber,
				uint8_t			seg
				) noexcept{
	Oscl::Error::Info::log(
		"\n"
		"\t\t\tTransaction Continuation:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"\t\t\t\tseg: 0x%2.2X\n"
		"",
		linkID,
		transactionNumber,
		seg
		);

	uint8_t	buffer[128];

	unsigned	remaining	= decoder.remaining();

	decoder.copyOut(buffer,remaining);

	Oscl::Error::Info::hexDump(
		buffer,
		remaining
		);
	}

static void	parseProvisioningBearerControlLinkOpen(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber
				) noexcept{

	unsigned
	remaining	= decoder.remaining();

	uint8_t	deviceUUID[16];
	decoder.copyOut(deviceUUID,sizeof(deviceUUID));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow, remaining; %u\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			remaining
			);
		return;
		}

	Oscl::Error::Info::log(
		"\n"
		"\t\t\tProvisioning Bearer Control Link Open:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"\t\t\t\tUUID: "
		"%2.2X%2.2X%2.2X%2.2X-"
		"%2.2X%2.2X-%2.2X%2.2X-%2.2X%2.2X-"
		"%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X"
		"\n",
		linkID,
		transactionNumber,
		deviceUUID[0],
		deviceUUID[1],
		deviceUUID[2],
		deviceUUID[3],
		deviceUUID[4],
		deviceUUID[5],
		deviceUUID[6],
		deviceUUID[7],
		deviceUUID[8],
		deviceUUID[9],
		deviceUUID[10],
		deviceUUID[11],
		deviceUUID[12],
		deviceUUID[13],
		deviceUUID[14],
		deviceUUID[15]
		);
	}

static void	parseProvisioningBearerControlLinkACK(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber
				) noexcept{
	Oscl::Error::Info::log(
		"\n"
		"\t\t\tProvisioning Bearer Control Link ACK:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"",
		linkID,
		transactionNumber
		);
	}

static void	parseProvisioningBearerControlLinkClose(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber
				) noexcept{
	uint8_t	reason;

	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\n"
		"\t\t\tProvisioning Bearer Control Link Close:\n"
		"\t\t\t\tlinkID: 0x%8.8X\n"
		"\t\t\t\ttransactionNumber: 0x%2.2X\n"
		"\t\t\t\treason: 0x%2.2X, \"%s\"\n"
		"",
		linkID,
		transactionNumber,
		reason,
		gpcfLinkCloseReason(reason)
		);
	}

static void	parseProvisioningBearerControl(
				Oscl::Decoder::Api& decoder,
				uint32_t		linkID,
				uint8_t			transactionNumber,
				uint8_t			bearerOpcode
				) noexcept{
	// /home/mike/root/src/mnm/btmesh/hci/part.cpp

	switch(bearerOpcode){
		case 0x00:
			// Link Open
			return parseProvisioningBearerControlLinkOpen(
					decoder,
					linkID,
					transactionNumber
					);
		case 0x01:
			// Link ACK
			return parseProvisioningBearerControlLinkACK(
					decoder,
					linkID,
					transactionNumber
					);
		case 0x02:
			// Link Close
			return parseProvisioningBearerControlLinkClose(
					decoder,
					linkID,
					transactionNumber
					);
		default:
			return;
		}
	}

static void	parseUnprovisionedDeviceBeacon(
				Oscl::Decoder::Api& decoder
				) noexcept{
	uint8_t		deviceUUID[16];
	uint16_t	oobInformation;
	uint32_t	uriHash	= 0;

	decoder.copyOut(deviceUUID,sizeof(deviceUUID));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow, deviceUUID.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	decoder.decode(oobInformation);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow, oobInformation.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	bool	hasUriHash	= false;

	if(decoder.remaining() > 1){
		// The uriHash is optional!
		decoder.decode(uriHash);
		hasUriHash	= true;
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow, uriHash.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			}
		}

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	// 00000000-0000-0000-0000-000000ABCDEF
	Oscl::Error::Info::log(
		"\n"
		"\t\t\tUnprovisioned Device Beacon:\n"
		"\t\t\t\tDevice UUID: "
		"%2.2X%2.2X%2.2X%2.2X-"
		"%2.2X%2.2X-%2.2X%2.2X-%2.2X%2.2X-"
		"%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X\n"
		"\t\t\t\tOOB Information: 0x%4.4X"
		"\n",
		deviceUUID[0],
		deviceUUID[1],
		deviceUUID[2],
		deviceUUID[3],
		deviceUUID[4],
		deviceUUID[5],
		deviceUUID[6],
		deviceUUID[7],
		deviceUUID[8],
		deviceUUID[9],
		deviceUUID[10],
		deviceUUID[11],
		deviceUUID[12],
		deviceUUID[13],
		deviceUUID[14],
		deviceUUID[15],
		oobInformation
		);

	for(unsigned i=0;i<16;++i){
		if(!(oobInformation & (1<<i))){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t\t\t[%u] \"%s\"\n",
			i,
			Oscl::Bluetooth::Strings::oobInformation(i)
			);
		}

	if(!hasUriHash){
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t\tURI Hash: 0x%8.8X\n"
		"\n",
		uriHash
		);

	}

static void	parseSecureNetworkBeacon(
				Oscl::Decoder::Api& decoder
				) noexcept{

	uint8_t		flags;
	uint8_t		networkID[8];
	uint32_t	ivIndex;
	uint8_t		authenticationValue[8];

	decoder.decode(flags);
	decoder.copyOut(networkID,sizeof(networkID));
	decoder.decode(ivIndex);
	decoder.copyOut(authenticationValue,sizeof(authenticationValue));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\n"
		"\t\t\tSecure Network Beacon: (0x21)\n"
		"\t\t\t\tflags: 0x%2.2X\n"
		"\t\t\t\tnetworkID: %2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X\n"
		"\t\t\t\tivIndex: 0x%8.8X\n"
		"\t\t\t\tauthenticationValue: %2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X-%2.2X"
		"\n",
		flags,
		networkID[0],
		networkID[1],
		networkID[2],
		networkID[3],
		networkID[4],
		networkID[5],
		networkID[6],
		networkID[7],
		ivIndex,
		authenticationValue[0],
		authenticationValue[1],
		authenticationValue[2],
		authenticationValue[3],
		authenticationValue[4],
		authenticationValue[5],
		authenticationValue[6],
		authenticationValue[7]
		);
	}

static void	parseEirFlags(uint8_t flags) noexcept {

	bool	needComma	= false;

	for(unsigned i=0;i<8;++i){
		bool
		set	= flags & (1<<i);

		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"%s%s",
			needComma?",":"",
			eirFlag(i)
			);
		needComma	= true;
		}
	}

static void	parseEIR_FLAGS(
				Oscl::Endian::Decoder::Api& dec,
				uint8_t				length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint8_t	data;
	decoder.decode(data);

	Oscl::Error::Info::log("\t\t\t");

	parseEirFlags(data);

	Oscl::Error::Info::log("\n");

	if(length > 1){
		// FIXME: WARNING!
		decoder.skip(length-1);
		}
	}

static void	parseIncompleteListOf16bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Incomplete List of 16-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseCompleteListOf16bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Complete List of 16-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseIncompleteListOf32bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Incomplete List of 32-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseCompleteListOf32bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Complete List of 32-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseIncompleteListOf128bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Incomplete List of 128-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseCompleteListOf128bitServiceClassUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Complete List of 128-bit Service Class UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseShortenedLocalName(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Shortened Local Name>>\n"
		"\t\t\t\t\""
		);

	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			"%c",
			data
			);
		}
	Oscl::Error::Info::log("\"\n");
	}

static void	parseCompleteLocalName(
				Oscl::Endian::Decoder::Api& dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Complete Local Name>>\n"
		"\t\t\t\t\""
		);

	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"%c",
			data
			);
		}
	Oscl::Error::Info::log("\"\n");
	}


static void	parseTxPowerLevel(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{
	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Tx Power Level>>\n"
		);

	int8_t	level;
	decoder.decode(level);

	Oscl::Error::Info::log(
		"\t\t\t\t%d dBm\n",
		level
		);
	}

static void	parseClassOfDevice(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint32_t	classOfDevice;
	decoder.decode(classOfDevice);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t<<Class of Device>>\n"
		"\t\t\t\t0x%8.8X \"%s\"\n",
		classOfDevice,
		Oscl::Bluetooth::Strings::classOfDevice(classOfDevice)
		);
	}

static void	parseSimplePairingHashC(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Simple Pairing Hash C>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSimplePairingHashR(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Simple Pairing Hash R>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSimplePairingRandomizerR192(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Simple Pairing Randomizer R-192>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseDeviceIdSecurityManagerTkValue(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	Oscl::Error::Info::log(
		"\t\t\t<<Device ID / Security Manager TK Value>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSecurityManagerOutOfBandFlags(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Security Manager Out of Band Flags>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSlaveConnectionIntervalRange(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint16_t	minConnectionInterval;
	uint16_t	maxConnectionInterval;

	decoder.decode(minConnectionInterval);
	decoder.decode(maxConnectionInterval);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\t\t<<Slave Connection Interval Range>>\n"
		"\t\t\t\tmin: 0x%4.4X\n"
		"\t\t\t\tmax: 0x%4.4X\n"
		"",
		minConnectionInterval,
		maxConnectionInterval
		);
	}

static void	parseListOf16bitServiceSolicitationUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<List of 16-bit Service Solicitation UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseListOf128bitServiceSolicitationUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<List of 128-bit Service Solicitation UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseServiceData16bitUUID(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Service Data 16-bit UUID>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parsePublicTargetAddress(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Public Target Address>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseRandomTargetAddress(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	}

static void	parseAppearance(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Random Target Address>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseAdvertisingInterval(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Advertising Interval>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseLeBluetoothDeviceAddress(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<LE Bluetooth Device Address>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseLeRole(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<LE Role>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSimplePairingHashC256(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Simple Pairing Hash C-256>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseSimplePairingRandomizerR256(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Simple Pairing Randomizer R-256>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseListOf32bitServiceSolicitationUUIDs(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<List of 32-bit Service Solicitation UUIDs>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseServiceData32bitUUID(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Service Data 32-bit UUID>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseServiceData128bitUUID(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Service Data 128-bit UUID>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseLeSecureConnectionsConfirmationValue(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<LE Secure Connections Confirmation Value>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseLeSecureConnectionsRandomValue(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<LE Secure Connections Random Value>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseURI(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<URI>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"%c",
			data
			);
		}
	Oscl::Error::Info::log("\"\n");
	}

static void	parseIndoorPositioning(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Indoor Positioning>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseTransportDiscoveryData(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Transport Discovery Data>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseLeSupportedFeatures(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);
	}

static void	parseChannelMapUpdateIndication(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Channel Map Update Indication>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseMeshProvisioning(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	// <<PB-ADV>>

	uint8_t		bePayload[32];

	unsigned
	len	= decoder.remaining() > sizeof(bePayload)?sizeof(bePayload):decoder.remaining();

	decoder.copyOut(bePayload,len);

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		bePayload,
		len
		);

	Oscl::Decoder::Api&
	beDecoderApi	= beDecoder.be();

	uint32_t	linkID;
	uint8_t		transactionNumber;

	beDecoderApi.decode(linkID);
	if(beDecoderApi.underflow()){
		Oscl::Error::Info::log(
			"%s: linkID underflow\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	beDecoderApi.decode(transactionNumber);

	if(beDecoderApi.underflow()){
		Oscl::Error::Info::log(
			"%s: transactionNumber underflow, linkID: 0x%8.8X\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			linkID
			);
		return;
		}

	// Generic Provisioning PDU

	//	Generic Provisioning Control
	uint8_t	typeSeg;
	beDecoderApi.decode(typeSeg);

	if(beDecoderApi.underflow()){
		Oscl::Error::Info::log(
			"%s: typeSeg underflow, linkID: 0x%8.8X, transactionNumber: 0x%2.2X\n",
			OSCL_PRETTY_FUNCTION_DETAIL,
			linkID,
			transactionNumber
			);
		return;
		}

	uint8_t	type	= (typeSeg & 0x3);
	uint8_t	seg		= (typeSeg >> 2);

	//	Generic Provisioning Payload
	switch(type){
		case 0x00:
			// Transaction Start
			return parseTransactionStart(
					beDecoderApi,
					linkID,
					transactionNumber,
					seg
					);
			break;
		case 0x01:
			// Transaction Acknowledgement
			return parseTransactionAcknowledgement(
					beDecoderApi,
					linkID,
					transactionNumber
					);
			break;
		case 0x02:
			// Transaction Continuation
			return parseTransactionContinuation(
					beDecoderApi,
					linkID,
					transactionNumber,
					seg
					);
		case 0x03:
			// Provisioning Bearer Control
			return parseProvisioningBearerControl(
					beDecoderApi,
					linkID,
					transactionNumber,
					seg
					);
		default:
			Oscl::Error::Info::log(
				"%s: Unknown type: 0x%2.2X\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				type
				);
			return;
		}
	}

static void	parseMeshMessage(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint8_t	nidIvi;
	decoder.decode(nidIvi);

	uint8_t	nid	= nidIvi & 0x7F;
	uint8_t	ivi	= nidIvi >> 7;

	Oscl::Error::Info::log(
		"\n"
		"\t\t\tMesh Message:\n"
		"\t\t\t\tNID: 0x%2.2X\n"
		"\t\t\t\tIVI: %u\n"
		"\t\t\t\tObfuscated header:"
		"",
		nid,
		ivi
		);

	for(unsigned i=0;i<7;++i){
		uint8_t	data;
		decoder.decode(data);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			" 0x%2.2X",
			data
			);
		}

	Oscl::Error::Info::log(
		"\n"
		"\t\t\t\tPayload:"
		);

	length	-= 8;

	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			" 0x%2.2X",
			data
			);
		}

	Oscl::Error::Info::log(
		"\n"
		);
	}

static void	parseMeshBeacon(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	/*	3.9.1 Endianness
		All multiple-octet numeric values in mesh beacons
		shall be sent in “big endian”, as described in Section 3.1.1.1.
	 */

	Oscl::Decoder::Api&
	decoder	= dec.be();

	uint8_t	beaconType;

	decoder.decode(beaconType);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	switch(beaconType){
		case 0x00:
			// Unprovisioned Device beacon
			parseUnprovisionedDeviceBeacon(
					decoder
					);
			break;
		case 0x01:
			// Secure Network beacon
			parseSecureNetworkBeacon(
				decoder
				);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: Unknown beacon type: 0x%2.2X!\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				beaconType
				);
			break;
		}

#if 0
	unsigned
	nRemaining		= decoder.remaining();

	unsigned
	nRead	= remaining - nRemaining;

	decoderApi.skip(nRead);
#endif
	}

static void	parse3DInformationData(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<3D Information Data>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseManufacturerSpecificData(
				Oscl::Endian::Decoder::Api&	dec,
				uint8_t						length
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\t\t\t<<Manufacturer Specific Data>>\n"
		"\t\t\t\t"
		);
	for(unsigned i=0;i<length;++i){
		uint8_t	data;
		decoder.decode(data);
		
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"\n%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"0x%2.2X ",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseEirField(
				Oscl::Endian::Decoder::Api& decoder,
				uint8_t						type,
				uint8_t						length
				) noexcept{

	Oscl::Error::Info::log(
		"\t\ttype: (0x%2.2X) %s\n"
		"\t\teir-data:\n"
		"",
		type,
		eirType(type)
		);

	switch(type){
		case 0x01:
			// <<Flags>>
			parseEIR_FLAGS(
				decoder,
				length
				);
			return;
		case 0x02:
			// <<Incomplete List of 16-bit Service Class UUIDs>>
			parseIncompleteListOf16bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x03:
			// <<Complete List of 16-bit Service Class UUIDs>>
			parseCompleteListOf16bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x04:
			// <<Incomplete List of 32-bit Service Class UUIDs>>
			parseIncompleteListOf32bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x05:
			// <<Complete List of 32-bit Service Class UUIDs>>
			parseCompleteListOf32bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x06:
			// <<Incomplete List of 128-bit Service Class UUIDs>>
			parseIncompleteListOf128bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x07:
			// <<Complete List of 128-bit Service Class UUIDs>>
			parseCompleteListOf128bitServiceClassUUIDs(
				decoder,
				length
				);
			return;
		case 0x08:
			// <<Shortened Local Name>>
			parseShortenedLocalName(
				decoder,
				length
				);
			return;
		case 0x09:
			// <<Complete Local Name>>
			parseCompleteLocalName(
				decoder,
				length
				);
			return;
		case 0x0A:
			// <<Tx Power Level>>
			parseTxPowerLevel(
				decoder,
				length
				);
			return;
		case 0x0B:
			// <<Class of Device>>
			parseClassOfDevice(
				decoder,
				length
				);
			return;
		case 0x0C:
			// <<Simple Pairing Hash C>>
			parseSimplePairingHashC(
				decoder,
				length
				);
			return;
		case 0x0D:
			// <<Simple Pairing Randomizer R>>
			parseSimplePairingHashR(
				decoder,
				length
				);
			return;
		case 0x0F:
			// <<Simple Pairing Randomizer R-192>>
			parseSimplePairingRandomizerR192(
				decoder,
				length
				);
			return;
		case 0x10:
			// <<Device ID>>
			// <<Security Manager TK Value>>
			parseDeviceIdSecurityManagerTkValue(
				decoder,
				length
				);
			return;
		case 0x11:
			// <<Security Manager Out of Band Flags>>
			parseSecurityManagerOutOfBandFlags(
				decoder,
				length
				);
			return;
		case 0x12:
			// <<Slave Connection Interval Range>>
			parseSlaveConnectionIntervalRange(
				decoder,
				length
				);
			return;
		case 0x14:
			// <<List of 16-bit Service Solicitation UUIDs>>
			parseListOf16bitServiceSolicitationUUIDs(
				decoder,
				length
				);
			return;
		case 0x15:
			// <<List of 128-bit Service Solicitation UUIDs>>
			parseListOf128bitServiceSolicitationUUIDs(
				decoder,
				length
				);
			return;
		case 0x16:
			// <<Service Data>>
			// <<Service Data - 16-bit UUID>>
			parseServiceData16bitUUID(
				decoder,
				length
				);
			return;
		case 0x17:
			// <<Public Target Address>>
			parsePublicTargetAddress(
				decoder,
				length
				);
			return;
		case 0x18:
			// <<Random Target Address>>
			parseRandomTargetAddress(
				decoder,
				length
				);
			return;
		case 0x19:
			// <<Appearance>>
			parseAppearance(
				decoder,
				length
				);
			return;
		case 0x1A:
			// <<Advertising Interval>>
			parseAdvertisingInterval(
				decoder,
				length
				);
			return;
		case 0x1B:
			// <<LE Bluetooth Device Address>>
			parseLeBluetoothDeviceAddress(
				decoder,
				length
				);
			return;
		case 0x1C:
			// <<LE Role>>
			parseLeRole(
				decoder,
				length
				);
			return;
		case 0x1D:
			// <<Simple Pairing Hash C-256>>
			parseSimplePairingHashC256(
				decoder,
				length
				);
			return;
		case 0x1E:
			// <<Simple Pairing Randomizer R-256>>
			parseSimplePairingRandomizerR256(
				decoder,
				length
				);
			return;
		case 0x1F:
			// <<List of 32-bit Service Solicitation UUIDs>>
			parseListOf32bitServiceSolicitationUUIDs(
				decoder,
				length
				);
			return;
		case 0x20:
			// <<Service Data - 32-bit UUID>>
			parseServiceData32bitUUID(
				decoder,
				length
				);
			return;
		case 0x21:
			// <<Service Data - 128-bit UUID>>
			parseServiceData128bitUUID(
				decoder,
				length
				);
			return;
		case 0x22:
			// <<LE Secure Connections Confirmation Value>>
			parseLeSecureConnectionsConfirmationValue(
				decoder,
				length
				);
			return;
		case 0x23:
			// <<LE Secure Connections Random Value>>
			parseLeSecureConnectionsRandomValue(
				decoder,
				length
				);
			return;
		case 0x24:
			// <<URI>>
			parseURI(
				decoder,
				length
				);
			return;
		case 0x25:
			// <<Indoor Positioning>>
			parseIndoorPositioning(
				decoder,
				length
				);
			return;
		case 0x26:
			// <<Transport Discovery Data>>
			parseTransportDiscoveryData(
				decoder,
				length
				);
			return;
		case 0x27:
			// <<LE Supported Features>>
			parseLeSupportedFeatures(
				decoder,
				length
				);
			return;
		case 0x28:
			// <<Channel Map Update Indication>>
			parseChannelMapUpdateIndication(
				decoder,
				length
				);
			return;
		case 0x29:
			// <<PB-ADV>>
			parseMeshProvisioning(
				decoder,
				length
				);
			return;
		case 0x2A:
			// <<Mesh Message>>
			parseMeshMessage(
				decoder,
				length
				);
			return;
		case 0x2B:
			// <<Mesh Beacon>>
			parseMeshBeacon(
				decoder,
				length
				);
			return;
		case 0x3D:
			// <<3D Information Data>>
			parse3DInformationData(
				decoder,
				length
				);
			return;
		case 0xFF:
			// <<Manufacturer Specific Data>>
			parseManufacturerSpecificData(
				decoder,
				length
				);
			return;
		default:
			break;
		}

	for(unsigned j=0;j<length;++j){
		uint8_t	data;
		decoder.le().decode(data);
		
		if(decoder.le().underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"%2.2X",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseEirFields(
				Oscl::Endian::Decoder::Api& dec,
				uint8_t						length
				) noexcept{
	/*
		The "eir" data consists of zero or more "segments"
		with the format <field_len><type><data>:

		u8	field_len
		u8	type(9)
		u8	data[field_len-1]

		The <field_len> is the length of the next
		field including the <type> and <data>.
	 */

	Oscl::Decoder::Api&
	decoder	= dec.le();

	for(unsigned i=0;i<length;){
		uint8_t	field_len;

		decoder.decode(field_len);

		Oscl::Error::Info::log(
			"\t\tfield_len: %u\n",
			field_len
			);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		if(!field_len){
			return;
			}

		uint8_t	type;

		decoder.decode(type);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		field_len	-= 1;

		parseEirField(
			dec,
			type,
			field_len
			);

		i += field_len + 2;
		}
	}

static void	parseHCI_OP_LE_READ_ADV_TX_POWER(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_ADV_TX_POWER\n"
		);
	}

static void	parseHCI_MAX_AD_LENGTH(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_MAX_AD_LENGTH\n"
		);
	}

static void	parseHCI_OP_LE_SET_ADV_DATA(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_ADV_DATA\n"
		);

	// page 1256

	uint8_t		length;

	decoder.decode(length);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tlength: %u\n"
		"\tremaining: %u\n"
		"",
		length,
		decoder.remaining()
		);

	parseEirFields(dec,decoder.remaining());
	}

static void	parseHCI_OP_LE_SET_SCAN_RSP_DATA(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_SET_SCAN_RSP_DATA\n"
		);

	uint8_t		length;

	decoder.decode(length);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	parseEirFields(dec,decoder.remaining());
	}

static void	parseHCI_OP_LE_READ_ADV_TX_POWER_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_ADV_TX_POWER\n"
		);
	// Disconnect
	// page 1255

	uint8_t		status;
	int8_t		transmit_power_level;

	decoder.decode(status);
	decoder.decode(transmit_power_level);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\t\ttransmit_power_level: %d dBm\n"
		"\n",
		status,
		statusCode(status),
		transmit_power_level
		);
	}

static void	parseHCI_OP_LE_READ_WHITE_LIST_SIZE_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_WHITE_LIST_SIZE\n"
		);
	// Disconnect
	// page 1272

	uint8_t		status;
	uint8_t		white_list_size;

	decoder.decode(status);
	decoder.decode(white_list_size);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\t\twhite_list_size: %u\n"
		"\n",
		status,
		statusCode(status),
		white_list_size
		);
	}

#if 0
static void	parseHCI_OP_LE_CLEAR_WHITE_LIST(
				Oscl::Decoder::Api& decoder
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_LE_CLEAR_WHITE_LIST\n"
		);
	// page 1273

	uint8_t		status;

	decoder.decode(status);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\n",
		status,
		statusCode(status)
		);
	}
#endif

static void	parseHCI_OP_READ_SSP_MODE(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_SSP_MODE\n"
		);
	}

static void	parseHCI_OP_WRITE_SSP_MODE(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_SSP_MODE\n"
		);
	}

static void	parseHCI_OP_READ_LOCAL_OOB_DATA(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_OOB_DATA\n"
		);
	}

static void	parseHCI_OP_READ_INQ_RSP_TX_POWER(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_INQ_RSP_TX_POWER\n"
		);
	}

static void	parseHCI_OP_SET_EVENT_MASK_PAGE_2(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_SET_EVENT_MASK_PAGE_2\n"
		);
	}

static void	parseHCI_OP_READ_LOCATION_DATA(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCATION_DATA\n"
		);
	}

static void	parseHCI_OP_READ_FLOW_CONTROL_MODE(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_FLOW_CONTROL_MODE\n"
		);
	}

static void	parseHCI_OP_WRITE_LE_HOST_SUPPORTED(Oscl::Endian::Decoder::Api& dec) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_LE_HOST_SUPPORTED\n"
		);
	uint8_t	le_supported_host;
	uint8_t	simultaneous_le_host;

	decoder.decode(le_supported_host);
	decoder.decode(simultaneous_le_host);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tle_supported_host: (0x%2.2X) %s\n"
		"\t\tsimultaneous_le_host: (0x%2.2X) %s\n"
		"\n",
		le_supported_host,
		le_supported_host?"LE Supported Host enabled":"LE Supported Host disabled",
		simultaneous_le_host,
		simultaneous_le_host?"Reserved":"Simultaneous LE and BR/EDR to Same Device Capable disabled"
		);
	}

static void	parseHCI_OP_SET_RESERVED_LT_ADDR(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_SET_RESERVED_LT_ADDR\n"
		);
	}

static void	parseHCI_OP_DELETE_RESERVED_LT_ADDR(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_DELETE_RESERVED_LT_ADDR\n"
		);
	}

static void	parseHCI_OP_SET_CSB_DATA(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_SET_CSB_DATA\n"
		);
	}

static void	parseHCI_OP_READ_SYNC_TRAIN_PARAMS(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_SYNC_TRAIN_PARAMS\n"
		);
	}

static void	parseHCI_OP_WRITE_SYNC_TRAIN_PARAMS(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_SYNC_TRAIN_PARAMS\n"
		);
	}

static void	parseHCI_OP_READ_SC_SUPPORT(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_SC_SUPPORT\n"
		);
	}

static void	parseHCI_OP_WRITE_SC_SUPPORT(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_SC_SUPPORT\n"
		);
	}

static void	parseHCI_OP_READ_LOCAL_OOB_EXT_DATA(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_OOB_EXT_DATA\n"
		);
	}

static void	parseHCI_OP_READ_LOCAL_VERSION(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_VERSION\n"
		);
	}

static void	parseHCI_OP_READ_LOCAL_COMMANDS(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_COMMANDS\n"
		);
	}

static void	parseHCI_OP_READ_LOCAL_FEATURES(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_FEATURES\n"
		);
	}

static void	parseCmdHCI_OP_READ_LOCAL_EXT_FEATURES(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_EXT_FEATURES\n"
		);
	// page 1052

	uint8_t	page_number;

	decoder.decode(page_number);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tpage_number: 0x%2.2X\n"
		"\n",
		page_number
		);
	}

static void	parseHCI_OP_LE_CREATE_CONN(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_CREATE_CONN\n"
		);
	// LE Create Connection
	// page 1266

	uint16_t	le_scan_interval;
	uint16_t	le_scan_window;
	uint8_t		initiator_filter_policy;
	uint8_t		peer_address_type;
	uint8_t		peer_address[6];	// bdaddr
	uint8_t		own_address_type;
	uint16_t	conn_interval_min;
	uint16_t	conn_interval_max;
	uint16_t	conn_latency;
	uint16_t	supervision_timeout;
	uint16_t	minimum_ce_length;
	uint16_t	maximum_ce_length;

	decoder.decode(le_scan_interval);
	decoder.decode(le_scan_window);
	decoder.decode(initiator_filter_policy);
	decoder.decode(peer_address_type);
	decoder.copyOut(peer_address,sizeof(peer_address));
	decoder.decode(own_address_type);
	decoder.decode(conn_interval_min);
	decoder.decode(conn_interval_max);
	decoder.decode(conn_latency);
	decoder.decode(supervision_timeout);
	decoder.decode(minimum_ce_length);
	decoder.decode(maximum_ce_length);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tle_scan_interval: 0x%4.4X\n"
		"\t\tle_scan_window: 0x%4.4X\n"
		"\t\tinitiator_filter_policy: 0x%2.2X\n"
		"\t\tpeer_address_type: (0x%2.2X) %s\n"
		"\t\tpeer_address: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\t\town_address_type: (0x%2.2X) %s\n"
		"\t\tconn_interval_min: 0x%4.4X\n"
		"\t\tconn_interval_max: 0x%4.4X\n"
		"\t\tconn_latency: 0x%4.4X\n"
		"\t\tsupervision_timeout: 0x%4.4X\n"
		"\t\tminimum_ce_length: 0x%4.4X\n"
		"\t\tmaximum_ce_length: 0x%4.4X\n"
		"",
		le_scan_interval,
		le_scan_window,
		initiator_filter_policy,
		peer_address_type,
		Oscl::Bluetooth::Strings::leAddressType(peer_address_type),
		peer_address[5],peer_address[4],peer_address[3],peer_address[2],peer_address[1],peer_address[0],
		own_address_type,
		Oscl::Bluetooth::Strings::leAddressType(own_address_type),
		conn_interval_min,
		conn_interval_max,
		conn_latency,
		supervision_timeout,
		minimum_ce_length,
		maximum_ce_length
		);
	}

static void	parseHCI_OP_LE_CREATE_CONN_CANCEL(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log("\tHCI_OP_LE_CREATE_CONN_CANCEL\n");
	}

static void	parseHCI_OP_LE_READ_WHITE_LIST_SIZE(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log("\tHCI_OP_LE_READ_WHITE_LIST_SIZE\n");
	}

static void	parseHCI_OP_LE_CLEAR_WHITE_LIST(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log("\tHCI_OP_LE_CLEAR_WHITE_LIST\n");
	}

static void	parseHCI_OP_LE_ADD_TO_WHITE_LIST(Oscl::Endian::Decoder::Api& dec) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log("\tHCI_OP_LE_ADD_TO_WHITE_LIST\n");

	uint8_t	address_type;
	uint8_t	addr[6];

	decoder.decode(address_type);
	decoder.copyOut(addr,sizeof(addr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tAddress_Type: (0x%2.2X) %s\n"
		"\tAddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"",
		address_type,
		Oscl::Bluetooth::Strings::leAddressType(address_type),
		addr[5],addr[4],addr[3],addr[2],addr[1],addr[0]
		);
	}

static void	parseHCI_OP_LE_DEL_FROM_WHITE_LIST(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log("\tHCI_OP_LE_DEL_FROM_WHITE_LIST\n");
	}

static void	parseHCI_OP_LE_CONN_UPDATE(Oscl::Endian::Decoder::Api& decoder) noexcept{
	Oscl::Error::Info::log("\tHCI_OP_LE_CONN_UPDATE\n");
	}

static void	parseHCI_OP_WRITE_EIR(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_EIR\n"
		);

	uint8_t	fec;

	decoder.decode(fec);

	uint8_t	data[240];

	unsigned
	remaining	= decoder.remaining();

	if(remaining > sizeof(data)){
		remaining	= sizeof(data);
		}

	decoder.copyOut(data,remaining);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tfec: 0x%2.2X\n"
		"\tdata[len:%u]:"
		"",
		fec,
		remaining
		);

	for(unsigned i=0;i<remaining;++i){
		if(!(i%16)){
			Oscl::Error::Info::log("\n\t\t");
			}
		Oscl::Error::Info::log(
			" %2.2X",
			data[i]
			);
		}

	Oscl::Error::Info::log("\n");

	parseEirFields(dec,remaining);
	}

static void	parseHCI_OP_WRITE_INQUIRY_MODE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_WRITE_INQUIRY_MODE\n"
		);

	// page 710

	uint8_t		mode;

	decoder.decode(mode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tmode 0x%2.2X, \"%s\"\n",
		mode,
		inqueryMode(mode)
		);
	}

static void	parseHCI_OP_SET_EVENT_MASK(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_SET_EVENT_MASK\n"
		);

	// page 705,

	uint64_t	mask;

	decoder.decode(mask);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

#if 0
	Oscl::Error::Info::log(
		"\t\tmask: 0x%" PRIXFAST64 ":\n",
		(uint64_t)mask
		);
#else
	uint32_t	high	= mask >> 32;
	uint32_t	low		= mask;
	Oscl::Error::Info::log(
		"\t\tmask: 0x%X%X:\n",
		high,
		low
		);
#endif

	for(unsigned i=0;i<64;++i){
		bool
		set	= mask & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			eventMask(i)
			);
		}
	}

static void	parseHCI_MON_COMMAND_PKT(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_COMMAND_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		payload,
		len
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	opcode;

	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	plen;

	decoder.decode(plen);

	Oscl::Error::Info::log(
		"\topcode: (0x%4.4X) %s\n"
		"\tplen: %u octets\n"
		"",
		opcode,
		hciOpcode(opcode),
		plen
		);

	switch(opcode){
#if 0
		case 0x0000:
			return parseHCI_OP_NOP(leDecoder);
		case 0x0401:
			return parseHCI_OP_INQUIRY(leDecoder);
		case 0x0402:
			return parseHCI_OP_INQUIRY_CANCEL(leDecoder);
		case 0x0403:
			return parseHCI_OP_PERIODIC_INQ(leDecoder);
		case 0x0404:
			return parseHCI_OP_EXIT_PERIODIC_INQ(leDecoder);
		case 0x0405:
			return parseHCI_OP_CREATE_CONN(leDecoder);
#endif
		case 0x0406:
			return parseHCI_OP_DISCONNECT(leDecoder);
#if 0
		case 0x0407:
			return parseHCI_OP_ADD_SCO(leDecoder);
		case 0x0408:
			return parseHCI_OP_CREATE_CONN_CANCEL(leDecoder);
		case 0x0409:
			return parseHCI_OP_ACCEPT_CONN_REQ(leDecoder);
		case 0x040a:
			return parseHCI_OP_REJECT_CONN_REQ(leDecoder);
		case 0x040b:
			return parseHCI_OP_LINK_KEY_REPLY(leDecoder);
		case 0x040c:
			return parseHCI_OP_LINK_KEY_NEG_REPLY(leDecoder);
		case 0x040d:
			return parseHCI_OP_PIN_CODE_REPLY(leDecoder);
		case 0x040e:
			return parseHCI_OP_PIN_CODE_NEG_REPLY(leDecoder);
		case 0x040f:
			return parseHCI_OP_CHANGE_CONN_PTYPE(leDecoder);
		case 0x0411:
			return parseHCI_OP_AUTH_REQUESTED(leDecoder);
		case 0x0413:
			return parseHCI_OP_SET_CONN_ENCRYPT(leDecoder);
		case 0x0415:
			return parseHCI_OP_CHANGE_CONN_LINK_KEY(leDecoder);
		case 0x0419:
			return parseHCI_OP_REMOTE_NAME_REQ(leDecoder);
		case 0x041a:
			return parseHCI_OP_REMOTE_NAME_REQ_CANCEL(leDecoder);
		case 0x041b:
			return parseHCI_OP_READ_REMOTE_FEATURES(leDecoder);
		case 0x041c:
			return parseHCI_OP_READ_REMOTE_EXT_FEATURES(leDecoder);
		case 0x041d:
			return parseHCI_OP_READ_REMOTE_VERSION(leDecoder);
		case 0x041f:
			return parseHCI_OP_READ_CLOCK_OFFSET(leDecoder);
		case 0x0428:
			return parseHCI_OP_SETUP_SYNC_CONN(leDecoder);
		case 0x0429:
			return parseHCI_OP_ACCEPT_SYNC_CONN_REQ(leDecoder);
		case 0x042a:
			return parseHCI_OP_REJECT_SYNC_CONN_REQ(leDecoder);
		case 0x042b:
			return parseHCI_OP_IO_CAPABILITY_REPLY(leDecoder);
		case 0x042c:
			return parseHCI_OP_USER_CONFIRM_REPLY(leDecoder);
		case 0x042d:
			return parseHCI_OP_USER_CONFIRM_NEG_REPLY(leDecoder);
		case 0x042e:
			return parseHCI_OP_USER_PASSKEY_REPLY(leDecoder);
		case 0x042f:
			return parseHCI_OP_USER_PASSKEY_NEG_REPLY(leDecoder);
		case 0x0430:
			return parseHCI_OP_REMOTE_OOB_DATA_REPLY(leDecoder);
		case 0x0433:
			return parseHCI_OP_REMOTE_OOB_DATA_NEG_REPLY(leDecoder);
		case 0x0434:
			return parseHCI_OP_IO_CAPABILITY_NEG_REPLY(leDecoder);
		case 0x0435:
			return parseHCI_OP_CREATE_PHY_LINK(leDecoder);
		case 0x0436:
			return parseHCI_OP_ACCEPT_PHY_LINK(leDecoder);
		case 0x0437:
			return parseHCI_OP_DISCONN_PHY_LINK(leDecoder);
		case 0x0438:
			return parseHCI_OP_CREATE_LOGICAL_LINK(leDecoder);
		case 0x0439:
			return parseHCI_OP_ACCEPT_LOGICAL_LINK(leDecoder);
		case 0x043a:
			return parseHCI_OP_DISCONN_LOGICAL_LINK(leDecoder);
		case 0x043b:
			return parseHCI_OP_LOGICAL_LINK_CANCEL(leDecoder);
		case 0x0441:
			return parseHCI_OP_SET_CSB(leDecoder);
		case 0x0443:
			return parseHCI_OP_START_SYNC_TRAIN(leDecoder);
		case 0x0803:
			return parseHCI_OP_SNIFF_MODE(leDecoder);
		case 0x0804:
			return parseHCI_OP_EXIT_SNIFF_MODE(leDecoder);
		case 0x0809:
			return parseHCI_OP_ROLE_DISCOVERY(leDecoder);
		case 0x080b:
			return parseHCI_OP_SWITCH_ROLE(leDecoder);
		case 0x080c:
			return parseHCI_OP_READ_LINK_POLICY(leDecoder);
		case 0x080d:
			return parseHCI_OP_WRITE_LINK_POLICY(leDecoder);
		case 0x080e:
			return parseHCI_OP_READ_DEF_LINK_POLICY(leDecoder);
#endif
		case 0x080f:
			return parseHCI_OP_WRITE_DEF_LINK_POLICY(leDecoder);
#if 0
		case 0x0811:
			return parseHCI_OP_SNIFF_SUBRATE(leDecoder);
#endif
		case 0x0c01:
			return parseHCI_OP_SET_EVENT_MASK(leDecoder);
#if 0
		case 0x0c03:
			return parseHCI_OP_RESET(leDecoder);
#endif
		case 0x0c05:
			return parseHCI_OP_SET_EVENT_FLT(leDecoder);
#if 0
		case 0x0c0d:
			return parseHCI_OP_READ_STORED_LINK_KEY(leDecoder);
		case 0x0c12:
			return parseHCI_OP_DELETE_STORED_LINK_KEY(leDecoder);
		case 0x0c14:
			return parseHCI_OP_READ_LOCAL_NAME(leDecoder);
#endif
		case 0x0c16:
			return parseHCI_OP_WRITE_CA_TIMEOUT(leDecoder);
#if 0
		case 0x0c18:
			return parseHCI_OP_WRITE_PG_TIMEOUT(leDecoder);
		case 0x0c1a:
			return parseHCI_OP_WRITE_SCAN_ENABLE(leDecoder);
		case 0x0c1f:
			return parseHCI_OP_READ_AUTH_ENABLE(leDecoder);
		case 0x0c20:
			return parseHCI_OP_WRITE_AUTH_ENABLE(leDecoder);
		case 0x0c21:
			return parseHCI_OP_READ_ENCRYPT_MODE(leDecoder);
		case 0x0c22:
			return parseHCI_OP_WRITE_ENCRYPT_MODE(leDecoder);
		case 0x0c23:
			return parseHCI_OP_READ_CLASS_OF_DEV(leDecoder);
		case 0x0c24:
			return parseHCI_OP_WRITE_CLASS_OF_DEV(leDecoder);
		case 0x0c25:
			return parseHCI_OP_READ_VOICE_SETTING(leDecoder);
		case 0x0c26:
			return parseHCI_OP_WRITE_VOICE_SETTING(leDecoder);
		case 0x0c33:
			return parseHCI_OP_HOST_BUFFER_SIZE(leDecoder);
		case 0x0c38:
			return parseHCI_OP_READ_NUM_SUPPORTED_IAC(leDecoder);
		case 0x0c39:
			return parseHCI_OP_READ_CURRENT_IAC_LAP(leDecoder);
		case 0x0c3a:
			return parseHCI_OP_WRITE_CURRENT_IAC_LAP(leDecoder);
#endif
		case 0x0c45:
			return parseHCI_OP_WRITE_INQUIRY_MODE(leDecoder);
#if 0
		case 240:
			return parseHCI_MAX_EIR_LENGTH(leDecoder);
#endif
		case 0x0c52:
			return parseHCI_OP_WRITE_EIR(leDecoder);
#if 1
		case 0x0c55:
			return parseHCI_OP_READ_SSP_MODE(leDecoder);
		case 0x0c56:
			return parseHCI_OP_WRITE_SSP_MODE(leDecoder);
		case 0x0c57:
			return parseHCI_OP_READ_LOCAL_OOB_DATA(leDecoder);
		case 0x0c58:
			return parseHCI_OP_READ_INQ_RSP_TX_POWER(leDecoder);
		case 0x0c63:
			return parseHCI_OP_SET_EVENT_MASK_PAGE_2(leDecoder);
		case 0x0c64:
			return parseHCI_OP_READ_LOCATION_DATA(leDecoder);
		case 0x0c66:
			return parseHCI_OP_READ_FLOW_CONTROL_MODE(leDecoder);
		case 0x0c6d:
			return parseHCI_OP_WRITE_LE_HOST_SUPPORTED(leDecoder);
		case 0x0c74:
			return parseHCI_OP_SET_RESERVED_LT_ADDR(leDecoder);
		case 0x0c75:
			return parseHCI_OP_DELETE_RESERVED_LT_ADDR(leDecoder);
		case 0x0c76:
			return parseHCI_OP_SET_CSB_DATA(leDecoder);
		case 0x0c77:
			return parseHCI_OP_READ_SYNC_TRAIN_PARAMS(leDecoder);
		case 0x0c78:
			return parseHCI_OP_WRITE_SYNC_TRAIN_PARAMS(leDecoder);
		case 0x0c79:
			return parseHCI_OP_READ_SC_SUPPORT(leDecoder);
		case 0x0c7a:
			return parseHCI_OP_WRITE_SC_SUPPORT(leDecoder);
		case 0x0c7d:
			return parseHCI_OP_READ_LOCAL_OOB_EXT_DATA(leDecoder);
		case 0x1001:
			return parseHCI_OP_READ_LOCAL_VERSION(leDecoder);
		case 0x1002:
			return parseHCI_OP_READ_LOCAL_COMMANDS(leDecoder);
		case 0x1003:
			return parseHCI_OP_READ_LOCAL_FEATURES(leDecoder);
#endif
		case 0x1004:
			return parseCmdHCI_OP_READ_LOCAL_EXT_FEATURES(leDecoder);
#if 0
		case 0x1005:
			return parseHCI_OP_READ_BUFFER_SIZE(leDecoder);
		case 0x1009:
			return parseHCI_OP_READ_BD_ADDR(leDecoder);
		case 0x100a:
			return parseHCI_OP_READ_DATA_BLOCK_SIZE(leDecoder);
		case 0x100b:
			return parseHCI_OP_READ_LOCAL_CODECS(leDecoder);
		case 0x0c1b:
			return parseHCI_OP_READ_PAGE_SCAN_ACTIVITY(leDecoder);
		case 0x0c1c:
			return parseHCI_OP_WRITE_PAGE_SCAN_ACTIVITY(leDecoder);
		case 0x0c2d:
			return parseHCI_OP_READ_TX_POWER(leDecoder);
		case 0x0c46:
			return parseHCI_OP_READ_PAGE_SCAN_TYPE(leDecoder);
		case 0x0c47:
			return parseHCI_OP_WRITE_PAGE_SCAN_TYPE(leDecoder);
		case 0x1405:
			return parseHCI_OP_READ_RSSI(leDecoder);
		case 0x1407:
			return parseHCI_OP_READ_CLOCK(leDecoder);
		case 0x1408:
			return parseHCI_OP_READ_ENC_KEY_SIZE(leDecoder);
		case 0x1409:
			return parseHCI_OP_READ_LOCAL_AMP_INFO(leDecoder);
		case 0x140a:
			return parseHCI_OP_READ_LOCAL_AMP_ASSOC(leDecoder);
		case 0x140b:
			return parseHCI_OP_WRITE_REMOTE_AMP_ASSOC(leDecoder);
		case 0x140c:
			return parseHCI_OP_GET_MWS_TRANSPORT_CONFIG(leDecoder);
		case 0x1803:
			return parseHCI_OP_ENABLE_DUT_MODE(leDecoder);
		case 0x1804:
			return parseHCI_OP_WRITE_SSP_DEBUG_MODE(leDecoder);
#endif
		case 0x2001:
			return parseHCI_OP_LE_SET_EVENT_MASK(leDecoder);

		case 0x2002:
			return parseHCI_OP_LE_READ_BUFFER_SIZE(leDecoder);
		case 0x2003:
			return parseHCI_OP_LE_READ_LOCAL_FEATURES(leDecoder);
		case 0x2005:
			return parseHCI_OP_LE_SET_RANDOM_ADDR(leDecoder);

		case 0x2006:
			return parseHCI_OP_LE_SET_ADV_PARAM(leDecoder);

		case 0x2007:
			return parseHCI_OP_LE_READ_ADV_TX_POWER(leDecoder);
		case 31:
			return parseHCI_MAX_AD_LENGTH(leDecoder);

		case 0x2008:
			return parseHCI_OP_LE_SET_ADV_DATA(leDecoder);

		case 0x2009:
			return parseHCI_OP_LE_SET_SCAN_RSP_DATA(leDecoder);

		case 0x200a:
			return parseHCI_OP_LE_SET_ADV_ENABLE(leDecoder);
		case 0x200b:
			return parseCmdHCI_OP_LE_SET_SCAN_PARAM(leDecoder);
		case 0x200c:
			return parseCmdHCI_OP_LE_SET_SCAN_ENABLE(leDecoder);
		case 0x200d:
			return parseHCI_OP_LE_CREATE_CONN(leDecoder);
		case 0x200e:
			return parseHCI_OP_LE_CREATE_CONN_CANCEL(leDecoder);
		case 0x200f:
			return parseHCI_OP_LE_READ_WHITE_LIST_SIZE(leDecoder);
		case 0x2010:
			return parseHCI_OP_LE_CLEAR_WHITE_LIST(leDecoder);
		case 0x2011:
			return parseHCI_OP_LE_ADD_TO_WHITE_LIST(leDecoder);
		case 0x2012:
			return parseHCI_OP_LE_DEL_FROM_WHITE_LIST(leDecoder);
		case 0x2013:
			return parseHCI_OP_LE_CONN_UPDATE(leDecoder);
		case 0x2016:
			return parseHCI_OP_LE_READ_REMOTE_FEATURES(leDecoder);
		case 0x2019:
			return parseHCI_OP_LE_START_ENC(leDecoder);
		case 0x201a:
			return parseHCI_OP_LE_LTK_REPLY(leDecoder);
		case 0x201b:
			return parseHCI_OP_LE_LTK_NEG_REPLY(leDecoder);
		case 0x201c:
			return parseHCI_OP_LE_READ_SUPPORTED_STATES(leDecoder);
		case 0x2020:
			return parseHCI_OP_LE_CONN_PARAM_REQ_REPLY(leDecoder);
		case 0x2021:
			return parseHCI_OP_LE_CONN_PARAM_REQ_NEG_REPLY(leDecoder);
		case 0x2022:
			return parseHCI_OP_LE_SET_DATA_LEN(leDecoder);
		case 0x2023:
			return parseHCI_OP_LE_READ_DEF_DATA_LEN(leDecoder);
		case 0x2024:
			return parseHCI_OP_LE_WRITE_DEF_DATA_LEN(leDecoder);
		case 0x202f:
			return parseHCI_OP_LE_READ_MAX_DATA_LEN(leDecoder);
		case 0x2031:
			return parseHCI_OP_LE_SET_DEFAULT_PHY(leDecoder);
		default:
			break;
		};

	const uint8_t*	p	= (const uint8_t*)payload;

	Oscl::Error::Info::hexDump(
		&p[len-decoder.remaining()],
		decoder.remaining()
		);
	}

static void	parseHCI_EV_INQUIRY_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_INQUIRY_COMPLETE\n"
		);
	}

static void	parseHCI_EV_INQUIRY_RESULT(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_INQUIRY_RESULT\n"
		);
	uint8_t		bdaddr[6];
	uint8_t		pscan_rep_mode;
	uint8_t		pscan_period_mode;
	uint8_t		pscan_mode;
	uint8_t		dev_class[3];
	uint16_t	clock_offset;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(pscan_rep_mode);
	decoder.decode(pscan_period_mode);
	decoder.decode(pscan_mode);
	decoder.copyOut(dev_class,sizeof(dev_class));
	decoder.decode(clock_offset);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint32_t	deviceClass;

	deviceClass	= dev_class[2];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[1];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[0];

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpscan_rep_mode: 0x%2.2X\n"
		"\tpscan_period_mode: 0x%2.2X\n"
		"\tpscan_mode: 0x%2.2X\n"
		"\tdev_class: (0x%6.6X) %s\n"
		"\tclock_offset: 0x%4.4X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		pscan_rep_mode,
		pscan_period_mode,
		pscan_mode,
		deviceClass,
		classOfDevice(deviceClass),
		clock_offset
		);
	}

static void	parseHCI_EV_CONN_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_CONN_COMPLETE\n"
		);
	}

static void	parseHCI_EV_CONN_REQUEST(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CONN_REQUEST\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		dev_class[3];
	uint8_t		link_type;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.copyOut(dev_class,sizeof(dev_class));
	decoder.decode(link_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint32_t	deviceClass;

	deviceClass	= dev_class[2];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[1];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[0];

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tdev_class: (0x%6.6X) %s\n"
		"\tlink_type: (0x%2.2X) %s\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		deviceClass,
		classOfDevice(deviceClass),
		link_type,
		linkType(link_type)
		);
	}

static void	parseHCI_EV_DISCONN_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_DISCONN_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		reason;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\treason: (0x%2.2X) \"%s\"\n"
		"\n",
		status,
		statusCode(status),
		handle,
		reason,
		statusCode(reason)
		);
	}

static void	parseHCI_EV_AUTH_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CONN_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;

	decoder.decode(status);
	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle
		);
	}

static void	parseHCI_EV_REMOTE_NAME(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_NAME\n"
		);

	uint8_t	status;
	uint8_t	bdaddr[6];
	char	name[248+1];

	decoder.decode(status);
	decoder.copyOut(bdaddr,6);
	decoder.copyOut(name,248);
	name[sizeof(name)-1]	= '\0';

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tname: \"%s\"\n"
		"\n",
		status,
		statusCode(status),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		name
		);
	}

static void	parseHCI_EV_ENCRYPT_CHANGE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_ENCRYPT_CHANGE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		encrypt;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(encrypt);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tencrypt: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		encrypt
		);
	}

static void	parseHCI_EV_CHANGE_LINK_KEY_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CHANGE_LINK_KEY_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;

	decoder.decode(status);
	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle
		);
	}

static void	parseHCI_EV_REMOTE_FEATURES(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_FEATURES\n"
		);
	// page 1117

	uint8_t		status;
	uint16_t	handle;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tfeatures: 0x%16.16llX"
		"\n",
		status,
		statusCode(status),
		handle,
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t[%u] %s\n",
			i,
			lmpFeature(i)
			);
		}
	}

static void	parseHCI_EV_REMOTE_VERSION(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_VERSION\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		lmp_ver;
	uint16_t	manufacturer;
	uint16_t	lmp_subver;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(lmp_ver);
	decoder.decode(manufacturer);
	decoder.decode(lmp_subver);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tlmp_ver: 0x%2.2X\n"
		"\tmanufacturer: 0x%4.4X\n"
		"\tlmp_subver: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		lmp_ver,
		manufacturer,
		lmp_subver
		);
	}

static void	parseHCI_EV_QOS_SETUP_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_QOS_SETUP_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		service_type;
	uint32_t	token_rate;
	uint32_t	peak_bandwidth;
	uint32_t	latency;
	uint32_t	delay_variation;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(service_type);
	decoder.decode(token_rate);
	decoder.decode(peak_bandwidth);
	decoder.decode(latency);
	decoder.decode(delay_variation);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tqos.service_type: 0x%2.2X\n"
		"\tqos.token_rate: 0x%8.8X\n"
		"\tqos.peak_bandwidth: 0x%8.8X\n"
		"\tqos.latency: 0x%8.8X\n"
		"\tqos.delay_variation: 0x%8.8X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		service_type,
		token_rate,
		peak_bandwidth,
		latency,
		delay_variation
		);
	}

static void	parseEVT_HCI_OP_READ_LOCAL_VERSION(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_VERSION\n"
		);

	uint8_t		status;
	uint8_t		hci_ver;
	uint16_t	hci_rev;
	uint8_t		lmp_ver;
	uint16_t	manufacturer;
	uint16_t	lmp_subver;

	decoder.decode(status);
	decoder.decode(hci_ver);
	decoder.decode(hci_rev);
	decoder.decode(lmp_ver);
	decoder.decode(manufacturer);
	decoder.decode(lmp_subver);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\thci_ver: 0x%2.2X\n"
		"\t\thci_rev: 0x%4.4X\n"
		"\t\tlmp_ver: 0x%2.2X\n"
		"\t\tmanufacturer: 0x%4.4X\n"
		"\t\tlmp_subver: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		hci_ver,
		hci_rev,
		lmp_ver,
		manufacturer,
		lmp_subver
		);
	}

static void	parseEVT_HCI_OP_READ_LOCAL_FEATURES(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_FEATURES\n"
		);

	uint8_t		status;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tfeatures: 0x%16.16llX"
		"\n",
		status,
		statusCode(status),
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			lmpFeature(i)
			);
		}
	}

static void	parseHCI_OP_READ_BUFFER_SIZE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_BUFFER_SIZE\n"
		);

	uint8_t		status;
	uint16_t	acl_mtu;
	uint8_t		sco_mtu;
	uint16_t	acl_max_pkt;
	uint16_t	sco_max_pkt;

	decoder.decode(status);
	decoder.decode(acl_mtu);
	decoder.decode(sco_mtu);
	decoder.decode(acl_max_pkt);
	decoder.decode(sco_max_pkt);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tacl_mtu: %u octets\n"
		"\t\tsco_mtu: %u octets\n"
		"\t\tacl_max_pkt: %u packets\n"
		"\t\tsco_max_pkt: %u packets\n"
		"\n",
		status,
		statusCode(status),
		acl_mtu,
		sco_mtu,
		acl_max_pkt,
		sco_max_pkt
		);
	}

static void	parseHCI_OP_READ_BD_ADDR(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_BD_ADDR\n"
		);

	uint8_t		status;
	uint8_t		bdaddr[6];

	decoder.decode(status);
	decoder.copyOut(bdaddr,6);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		status,
		statusCode(status),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_OP_LE_READ_BUFFER_SIZE_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_BUFFER_SIZE\n"
		);

	uint8_t		status;
	uint16_t	le_mtu;
	uint8_t		le_max_pkt;

	decoder.decode(status);
	decoder.decode(le_mtu);
	decoder.decode(le_max_pkt);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tle_mtu: 0x%4.4X\n"
		"\tle_max_pkt: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		le_mtu,
		le_max_pkt
		);
	}

static void	parseHCI_OP_LE_READ_LOCAL_FEATURES_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_LOCAL_FEATURES\n"
		);

	uint8_t		status;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tfeatures: 0x%16.16llX"
		"\n",
		status,
		statusCode(status),
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			lmpFeature(i)
			);
		}

	}

static void	parseHCI_OP_LE_READ_SUPPORTED_STATES_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_LE_READ_SUPPORTED_STATES\n"
		);

	uint8_t		status;
	uint8_t		le_states[8];

	decoder.decode(status);
	decoder.copyOut(le_states,sizeof(le_states));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tle_states: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		status,
		statusCode(status),
		le_states[0],
		le_states[1],
		le_states[2],
		le_states[3],
		le_states[4],
		le_states[5],
		le_states[6],
		le_states[7]
		);
	}

static void	parseEVT_HCI_OP_READ_LOCAL_COMMANDS(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_COMMANDS\n"
		);

	uint8_t		status;
	uint8_t		commands[64];

	decoder.decode(status);

	unsigned
	remaining	= decoder.remaining();

	if(remaining > sizeof(commands)){
		remaining	= sizeof(commands);
		}

	decoder.copyOut(commands,remaining);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}


	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tcommands[len:%u]:"
		"",
		status,
		statusCode(status),
		remaining
		);

	for(unsigned i=0;i<remaining;++i){
		if(!(i%16)){
			Oscl::Error::Info::log("\n\t\t");
			}
		Oscl::Error::Info::log(
			" %2.2X",
			commands[i]
			);
		}

	Oscl::Error::Info::log(
		"\n"
		"\tCommands Supported [Y/N]:\n"
		);

	for(unsigned i=0;i<remaining;++i){
		uint8_t	octet	= commands[i];
		for(unsigned j=0;j<8;++j){
			bool
			supported	= (octet & (1<<j))?true:false;
			Oscl::Error::Info::log(
				"\t\t[%2u,%u, (%s)] %s\n"
				"",
				i,
				j,
				supported?"Y":"N",
				Oscl::Bluetooth::Strings::hciCommand(i,j)
				);
			}
		}

	}

static void	parseHCI_OP_READ_CLASS_OF_DEV(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_CLASS_OF_DEV\n"
		);

	uint8_t		status;
	uint8_t		dev_class[3];

	decoder.decode(status);
	decoder.copyOut(dev_class,sizeof(dev_class));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint32_t	deviceClass;

	deviceClass	= dev_class[2];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[1];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[0];

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tdev_class: (0x%6.6X) %s\n"
		"\n",
		status,
		statusCode(status),
		deviceClass,
		classOfDevice(deviceClass)
		);
	}

static void	parseHCI_OP_READ_LOCAL_NAME(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_NAME\n"
		);

	uint8_t		status;
	char		name[248];

	decoder.decode(status);
	decoder.copyOut(name,sizeof(name));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tname: \"%s\"\n"
		"\n",
		status,
		statusCode(status),
		name
		);
	}

static void	parseHCI_OP_READ_VOICE_SETTING(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_VOICE_SETTING\n"
		);

	uint8_t		status;
	uint16_t	voice_setting;

	decoder.decode(status);
	decoder.decode(voice_setting);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tvoice_setting: 0x%4.4X\n"
		"\t\t\tInput Coding: %s\n"
		"\t\t\tInput Data Format: %s\n"
		"\t\t\tInput Sample Size: %s\n"
		"\t\t\tLinear PCM Bit Position: %s\n"
		"\t\t\tAir Coding Format: %s\n"
		"\n",
		status,
		statusCode(status),
		voice_setting,
		voiceSettingInputCoding(voice_setting),
		voiceSettingInputDataFormat(voice_setting),
		voiceSettingSampleSize(voice_setting),
		voiceSettingLinearPcmBitPos(voice_setting),
		voiceSettingAirCodingFormat(voice_setting)
		);
	}

static void	parseHCI_OP_READ_NUM_SUPPORTED_IAC(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_NUM_SUPPORTED_IAC\n"
		);


	uint8_t	status;
	uint8_t	num_iac;

	decoder.decode(status);
	decoder.decode(num_iac);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tnum_iac: %u simultaneous IACs\n"
		"\n",
		status,
		statusCode(status),
		num_iac
		);
	}

static void	parseHCI_OP_READ_CURRENT_IAC_LAP(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_CURRENT_IAC_LAP\n"
		);

	// page 965

	uint8_t	status;
	uint8_t	num_iac;

	decoder.decode(status);
	decoder.decode(num_iac);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\tnum_iac: %u IACs \n"
		"\n",
		status,
		statusCode(status),
		num_iac
		);

	for(unsigned i=0;i<num_iac;++i){
		uint8_t	iac[3];
		decoder.copyOut(iac,sizeof(iac));
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		uint32_t
		value	= iac[2];
		value	<<= 8;
		value	|= iac[1];
		value	<<= 8;
		value	|= iac[0];

		Oscl::Error::Info::log(
			"\t\t\t[i] 0x%6.6x\n",
			value
			);
		}
	}

static void	parseEVT_HCI_OP_READ_INQ_RSP_TX_POWER(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_INQ_RSP_TX_POWER\n"
		);

	uint8_t	status;
	int8_t	tx_power;

	decoder.decode(status);
	decoder.decode(tx_power);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\t\tstatus: 0x%2.2X \"%s\"\n"
		"\t\ttx_power: %d\n"
		"\n",
		status,
		statusCode(status),
		tx_power
		);
	}

static void	parseHCI_OP_READ_LOCAL_EXT_FEATURES(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_LOCAL_EXT_FEATURES\n"
		);

	uint8_t		status;
	uint8_t		page;
	uint8_t		max_page;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(page);
	decoder.decode(max_page);
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tpage: 0x%2.2X\n"
		"\tmax_page: 0x%2.2X\n"
		"\tfeatures: (0x%16.16llX)\n"
		"",
		status,
		statusCode(status),
		page,
		max_page,
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t[%u] %s\n",
			i,
			featureOnPage(page,i)
			);
		}
	}

static void	parseHCI_OP_READ_STORED_LINK_KEY(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_OP_READ_STORED_LINK_KEY\n"
		);

	uint8_t	status;
	uint8_t	max_keys;
	uint8_t	num_keys;

	decoder.decode(status);
	decoder.decode(max_keys);
	decoder.decode(num_keys);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tmax_keys: 0x%2.2X\n"
		"\tnum_keys: 0x%2.2X\n"
		"",
		status,
		statusCode(status),
		max_keys,
		num_keys
		);

	unsigned
	remaining	= decoder.remaining();

	Oscl::Error::Info::log(
		"\t\tdata (len:%u):",
		remaining
		);

	for(unsigned i=0;i<remaining;++i){
		uint8_t	data;
		decoder.decode(data);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			" %2.2x",
			data
			);
		}

	Oscl::Error::Info::log("\n");
	}

static void	parseHCI_EV_CMD_COMPLETE(
				Oscl::Endian::Decoder::Api&	decoder
				) noexcept{

	Oscl::Error::Info::log(
		"\tHCI_EV_CMD_COMPLETE\n"
		);
	uint8_t		ncmd;
	uint16_t	opcode;

	decoder.le().decode(ncmd);
	decoder.le().decode(opcode);

	if(decoder.le().underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		}

	switch(opcode){
#if 0
		case 0x0000:
			return parseHCI_OP_NOP(decoder);
		case 0x0401:
			return parseHCI_OP_INQUIRY(decoder);
		case 0x0402:
			return parseHCI_OP_INQUIRY_CANCEL(decoder);
		case 0x0403:
			return parseHCI_OP_PERIODIC_INQ(decoder);
		case 0x0404:
			return parseHCI_OP_EXIT_PERIODIC_INQ(decoder);
		case 0x0405:
			return parseHCI_OP_CREATE_CONN(decoder);
		case 0x0406:
			return parseHCI_OP_DISCONNECT(decoder);
		case 0x0407:
			return parseHCI_OP_ADD_SCO(decoder);
		case 0x0408:
			return parseHCI_OP_CREATE_CONN_CANCEL(decoder);
		case 0x0409:
			return parseHCI_OP_ACCEPT_CONN_REQ(decoder);
		case 0x040a:
			return parseHCI_OP_REJECT_CONN_REQ(decoder);
		case 0x040b:
			return parseHCI_OP_LINK_KEY_REPLY(decoder);
		case 0x040c:
			return parseHCI_OP_LINK_KEY_NEG_REPLY(decoder);
		case 0x040d:
			return parseHCI_OP_PIN_CODE_REPLY(decoder);
		case 0x040e:
			return parseHCI_OP_PIN_CODE_NEG_REPLY(decoder);
		case 0x040f:
			return parseHCI_OP_CHANGE_CONN_PTYPE(decoder);
		case 0x0411:
			return parseHCI_OP_AUTH_REQUESTED(decoder);
		case 0x0413:
			return parseHCI_OP_SET_CONN_ENCRYPT(decoder);
		case 0x0415:
			return parseHCI_OP_CHANGE_CONN_LINK_KEY(decoder);
		case 0x0419:
			return parseHCI_OP_REMOTE_NAME_REQ(decoder);
		case 0x041a:
			return parseHCI_OP_REMOTE_NAME_REQ_CANCEL(decoder);
		case 0x041b:
			return parseHCI_OP_READ_REMOTE_FEATURES(decoder);
		case 0x041c:
			return parseHCI_OP_READ_REMOTE_EXT_FEATURES(decoder);
		case 0x041d:
			return parseHCI_OP_READ_REMOTE_VERSION(decoder);
		case 0x041f:
			return parseHCI_OP_READ_CLOCK_OFFSET(decoder);
		case 0x0428:
			return parseHCI_OP_SETUP_SYNC_CONN(decoder);
		case 0x0429:
			return parseHCI_OP_ACCEPT_SYNC_CONN_REQ(decoder);
		case 0x042a:
			return parseHCI_OP_REJECT_SYNC_CONN_REQ(decoder);
		case 0x042b:
			return parseHCI_OP_IO_CAPABILITY_REPLY(decoder);
		case 0x042c:
			return parseHCI_OP_USER_CONFIRM_REPLY(decoder);
		case 0x042d:
			return parseHCI_OP_USER_CONFIRM_NEG_REPLY(decoder);
		case 0x042e:
			return parseHCI_OP_USER_PASSKEY_REPLY(decoder);
		case 0x042f:
			return parseHCI_OP_USER_PASSKEY_NEG_REPLY(decoder);
		case 0x0430:
			return parseHCI_OP_REMOTE_OOB_DATA_REPLY(decoder);
		case 0x0433:
			return parseHCI_OP_REMOTE_OOB_DATA_NEG_REPLY(decoder);
		case 0x0434:
			return parseHCI_OP_IO_CAPABILITY_NEG_REPLY(decoder);
		case 0x0435:
			return parseHCI_OP_CREATE_PHY_LINK(decoder);
		case 0x0436:
			return parseHCI_OP_ACCEPT_PHY_LINK(decoder);
		case 0x0437:
			return parseHCI_OP_DISCONN_PHY_LINK(decoder);
		case 0x0438:
			return parseHCI_OP_CREATE_LOGICAL_LINK(decoder);
		case 0x0439:
			return parseHCI_OP_ACCEPT_LOGICAL_LINK(decoder);
		case 0x043a:
			return parseHCI_OP_DISCONN_LOGICAL_LINK(decoder);
		case 0x043b:
			return parseHCI_OP_LOGICAL_LINK_CANCEL(decoder);
		case 0x0441:
			return parseHCI_OP_SET_CSB(decoder);
		case 0x0443:
			return parseHCI_OP_START_SYNC_TRAIN(decoder);
		case 0x0803:
			return parseHCI_OP_SNIFF_MODE(decoder);
		case 0x0804:
			return parseHCI_OP_EXIT_SNIFF_MODE(decoder);
		case 0x0809:
			return parseHCI_OP_ROLE_DISCOVERY(decoder);
		case 0x080b:
			return parseHCI_OP_SWITCH_ROLE(decoder);
		case 0x080c:
			return parseHCI_OP_READ_LINK_POLICY(decoder);
		case 0x080d:
			return parseHCI_OP_WRITE_LINK_POLICY(decoder);
		case 0x080e:
			return parseHCI_OP_READ_DEF_LINK_POLICY(decoder);
		case 0x080f:
			return parseHCI_OP_WRITE_DEF_LINK_POLICY(decoder);
		case 0x0811:
			return parseHCI_OP_SNIFF_SUBRATE(decoder);
#endif
#if 0
		case 0x0c01:
			return parseHCI_OP_SET_EVENT_MASK(decoder);
		case 0x0c03:
			return parseHCI_OP_RESET(decoder);
		case 0x0c05:
			return parseHCI_OP_SET_EVENT_FLT(decoder);
#endif
		case 0x0c0d:
			return parseHCI_OP_READ_STORED_LINK_KEY(decoder);
#if 0
		case 0x0c12:
			return parseHCI_OP_DELETE_STORED_LINK_KEY(decoder);
#endif
		case 0x0c14:
			return parseHCI_OP_READ_LOCAL_NAME(decoder);
#if 0
		case 0x0c16:
			return parseHCI_OP_WRITE_CA_TIMEOUT(decoder);
		case 0x0c18:
			return parseHCI_OP_WRITE_PG_TIMEOUT(decoder);
		case 0x0c1a:
			return parseHCI_OP_WRITE_SCAN_ENABLE(decoder);
		case 0x0c1f:
			return parseHCI_OP_READ_AUTH_ENABLE(decoder);
		case 0x0c20:
			return parseHCI_OP_WRITE_AUTH_ENABLE(decoder);
		case 0x0c21:
			return parseHCI_OP_READ_ENCRYPT_MODE(decoder);
		case 0x0c22:
			return parseHCI_OP_WRITE_ENCRYPT_MODE(decoder);
#endif
		case 0x0c23:
			return parseHCI_OP_READ_CLASS_OF_DEV(decoder);
#if 0
		case 0x0c24:
			return parseHCI_OP_WRITE_CLASS_OF_DEV(decoder);
#endif
		case 0x0c25:
			return parseHCI_OP_READ_VOICE_SETTING(decoder);
#if 0
		case 0x0c26:
			return parseHCI_OP_WRITE_VOICE_SETTING(decoder);
		case 0x0c33:
			return parseHCI_OP_HOST_BUFFER_SIZE(decoder);
#endif
		case 0x0c38:
			return parseHCI_OP_READ_NUM_SUPPORTED_IAC(decoder);
		case 0x0c39:
			return parseHCI_OP_READ_CURRENT_IAC_LAP(decoder);
#if 0
		case 0x0c3a:
			return parseHCI_OP_WRITE_CURRENT_IAC_LAP(decoder);
		case 0x0c45:
			return parseHCI_OP_WRITE_INQUIRY_MODE(decoder);
		case 240:
			return parseHCI_MAX_EIR_LENGTH(decoder);
		case 0x0c52:
			return parseHCI_OP_WRITE_EIR(decoder);
		case 0x0c55:
			return parseHCI_OP_READ_SSP_MODE(decoder);
		case 0x0c56:
			return parseHCI_OP_WRITE_SSP_MODE(decoder);
		case 0x0c57:
			return parseHCI_OP_READ_LOCAL_OOB_DATA(decoder);
#endif
		case 0x0c58:
			return parseEVT_HCI_OP_READ_INQ_RSP_TX_POWER(decoder);
#if 0
		case 0x0c63:
			return parseHCI_OP_SET_EVENT_MASK_PAGE_2(decoder);
		case 0x0c64:
			return parseHCI_OP_READ_LOCATION_DATA(decoder);
		case 0x0c66:
			return parseHCI_OP_READ_FLOW_CONTROL_MODE(decoder);
		case 0x0c6d:
			return parseHCI_OP_WRITE_LE_HOST_SUPPORTED(decoder);
		case 0x0c74:
			return parseHCI_OP_SET_RESERVED_LT_ADDR(decoder);
		case 0x0c75:
			return parseHCI_OP_DELETE_RESERVED_LT_ADDR(decoder);
		case 0x0c76:
			return parseHCI_OP_SET_CSB_DATA(decoder);
		case 0x0c77:
			return parseHCI_OP_READ_SYNC_TRAIN_PARAMS(decoder);
		case 0x0c78:
			return parseHCI_OP_WRITE_SYNC_TRAIN_PARAMS(decoder);
		case 0x0c79:
			return parseHCI_OP_READ_SC_SUPPORT(decoder);
		case 0x0c7a:
			return parseHCI_OP_WRITE_SC_SUPPORT(decoder);
		case 0x0c7d:
			return parseHCI_OP_READ_LOCAL_OOB_EXT_DATA(decoder);
#endif
		case 0x1001:
			return parseEVT_HCI_OP_READ_LOCAL_VERSION(decoder);
		case 0x1002:
			return parseEVT_HCI_OP_READ_LOCAL_COMMANDS(decoder);
		case 0x1003:
			return parseEVT_HCI_OP_READ_LOCAL_FEATURES(decoder);
		case 0x1004:
			return parseHCI_OP_READ_LOCAL_EXT_FEATURES(decoder);
		case 0x1005:
			return parseHCI_OP_READ_BUFFER_SIZE(decoder);
		case 0x1009:
			return parseHCI_OP_READ_BD_ADDR(decoder);
#if 0
		case 0x100a:
			return parseHCI_OP_READ_DATA_BLOCK_SIZE(decoder);
		case 0x100b:
			return parseHCI_OP_READ_LOCAL_CODECS(decoder);
#endif
		case 0x0c1b:
			return parseHCI_OP_READ_PAGE_SCAN_ACTIVITY(decoder);
#if 0
		case 0x0c1c:
			return parseHCI_OP_WRITE_PAGE_SCAN_ACTIVITY(decoder);
		case 0x0c2d:
			return parseHCI_OP_READ_TX_POWER(decoder);
#endif
		case 0x0c46:
			return parseHCI_OP_READ_PAGE_SCAN_TYPE(decoder);
#if 0
		case 0x0c47:
			return parseHCI_OP_WRITE_PAGE_SCAN_TYPE(decoder);
		case 0x1405:
			return parseHCI_OP_READ_RSSI(decoder);
		case 0x1407:
			return parseHCI_OP_READ_CLOCK(decoder);
		case 0x1408:
			return parseHCI_OP_READ_ENC_KEY_SIZE(decoder);
		case 0x1409:
			return parseHCI_OP_READ_LOCAL_AMP_INFO(decoder);
		case 0x140a:
			return parseHCI_OP_READ_LOCAL_AMP_ASSOC(decoder);
		case 0x140b:
			return parseHCI_OP_WRITE_REMOTE_AMP_ASSOC(decoder);
		case 0x140c:
			return parseHCI_OP_GET_MWS_TRANSPORT_CONFIG(decoder);
		case 0x1803:
			return parseHCI_OP_ENABLE_DUT_MODE(decoder);
		case 0x1804:
			return parseHCI_OP_WRITE_SSP_DEBUG_MODE(decoder);
		case 0x2001:
			return parseHCI_OP_LE_SET_EVENT_MASK(decoder);
#endif
		case 0x2002:
			return parseHCI_OP_LE_READ_BUFFER_SIZE_COMPLETE(decoder);
		case 0x2003:
			return parseHCI_OP_LE_READ_LOCAL_FEATURES_COMPLETE(decoder);
#if 0
		case 0x2005:
			return parseHCI_OP_LE_SET_RANDOM_ADDR(decoder);
		case 0x2006:
			return parseHCI_OP_LE_SET_ADV_PARAM(decoder);
#endif
		case 0x2007:
			return parseHCI_OP_LE_READ_ADV_TX_POWER_COMPLETE(decoder);
#if 0
		case 31:
			return parseHCI_MAX_AD_LENGTH(decoder);
		case 0x2008:
			return parseHCI_OP_LE_SET_ADV_DATA(decoder);
		case 0x2009:
			return parseHCI_OP_LE_SET_SCAN_RSP_DATA(decoder);
		case 0x200a:
			return parseHCI_OP_LE_SET_ADV_ENABLE(decoder);
#endif
#if 0
		case 0x200b:
			return parseHCI_OP_LE_SET_SCAN_PARAM(decoder);
		case 0x200c:
			return parseHCI_OP_LE_SET_SCAN_ENABLE(decoder);
#endif
#if 0
		case 0x200d:
			return parseHCI_OP_LE_CREATE_CONN(decoder);
		case 0x200e:
			return parseHCI_OP_LE_CREATE_CONN_CANCEL(decoder);
#endif
		case 0x200f:
			return parseHCI_OP_LE_READ_WHITE_LIST_SIZE_COMPLETE(decoder);
#if 0
		case 0x2010:
			return parseHCI_OP_LE_CLEAR_WHITE_LIST(decoder);
#endif
#if 0
		case 0x2011:
			return parseHCI_OP_LE_ADD_TO_WHITE_LIST(decoder);
		case 0x2012:
			return parseHCI_OP_LE_DEL_FROM_WHITE_LIST(decoder);
		case 0x2013:
			return parseHCI_OP_LE_CONN_UPDATE(decoder);
		case 0x2016:
			return parseHCI_OP_LE_READ_REMOTE_FEATURES(decoder);
		case 0x2019:
			return parseHCI_OP_LE_START_ENC(decoder);
		case 0x201a:
			return parseHCI_OP_LE_LTK_REPLY(decoder);
		case 0x201b:
			return parseHCI_OP_LE_LTK_NEG_REPLY(decoder);
#endif
		case 0x201c:
			return parseHCI_OP_LE_READ_SUPPORTED_STATES_COMPLETE(decoder);
#if 0
		case 0x2020:
			return parseHCI_OP_LE_CONN_PARAM_REQ_REPLY(decoder);
		case 0x2021:
			return parseHCI_OP_LE_CONN_PARAM_REQ_NEG_REPLY(decoder);
		case 0x2022:
			return parseHCI_OP_LE_SET_DATA_LEN(decoder);
		case 0x2023:
			return parseHCI_OP_LE_READ_DEF_DATA_LEN(decoder);
		case 0x2024:
			return parseHCI_OP_LE_WRITE_DEF_DATA_LEN(decoder);
		case 0x202f:
			return parseHCI_OP_LE_READ_MAX_DATA_LEN(decoder);
		case 0x2031:
			return parseHCI_OP_LE_SET_DEFAULT_PHY(decoder);
#endif
		default:
			break;
		};

	uint8_t	status;
	decoder.le().decode(status);

	unsigned	remaining	=  decoder.le().remaining();

	Oscl::Error::Info::log(
		"\tncmd: 0x%2.2X\n"
		"\topcode: (0x%4.4X) %s\n"
		"\tstatus: (0x%4.4X) %s\n"
		"\tdata (len: %u):",
		ncmd,
		opcode,
		hciOpcode(opcode),
		status,
		Oscl::Bluetooth::Strings::statusCode(status),
		remaining
		);

	for(unsigned i=0;i<remaining;++i){
		uint8_t	data;
		decoder.le().decode(data);
		if(decoder.le().underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			" %2.2x",
			data
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseHCI_EV_CMD_STATUS(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CMD_STATUS\n"
		);

	uint8_t		status;
	uint8_t		ncmd;
	uint16_t	opcode;

	decoder.decode(status);
	decoder.decode(ncmd);
	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tncmd: 0x%2.2X\n"
		"\topcode: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		ncmd,
		opcode
		);
	}

static void	parseHCI_EV_HARDWARE_ERROR(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_HARDWARE_ERROR\n"
		);

	uint8_t		code;

	decoder.decode(code);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tcode: 0x%2.2X\n"
		"\n",
		code
		);
	}

static void	parseHCI_EV_ROLE_CHANGE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_ROLE_CHANGE\n"
		);

	uint8_t		status;
	uint8_t		bdaddr[6];
	uint8_t		role;

	decoder.decode(status);
	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(role);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\trole: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		role
		);
	}

static void	parseHCI_EV_NUM_COMP_PKTS(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_NUM_COMP_PKTS\n"
		);

	uint8_t		num_hndl;

	decoder.decode(num_hndl);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tnum_hndl: 0x%4.4X\n"
		"\n",
		num_hndl
		);

	for(unsigned i=0;i<num_hndl;++i){
		uint16_t	handle;
		uint16_t	count;

		decoder.decode(handle);
		decoder.decode(count);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"\t\thandle: 0x%4.4X\n"
			"\t\tcount: 0x%4.4X\n"
			"\n",
			handle,
			count
			);

		}
	}

static void	parseHCI_EV_MODE_CHANGE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_MODE_CHANGE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		mode;
	uint16_t	interval;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(mode);
	decoder.decode(interval);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tmode: 0x%2.2X\n"
		"\tinterval: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		mode,
		interval
		);
	}

static void	parseHCI_EV_PIN_CODE_REQ(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_PIN_CODE_REQ\n"
		);

	uint8_t		bdaddr[6];

	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_LINK_KEY_REQ(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LINK_KEY_REQ\n"
		);

	uint8_t		bdaddr[6];

	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_LINK_KEY_NOTIFY(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LINK_KEY_NOTIFY\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		link_key[16];
	uint8_t		key_type;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.copyOut(link_key,sizeof(link_key));
	decoder.decode(key_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tlink_key: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X,%2.2X:%2.2X,"
		"%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X,%2.2X:%2.2X\n"
		"\tkey_type: 0x%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		link_key[0],
		link_key[1],
		link_key[2],
		link_key[3],
		link_key[4],
		link_key[5],
		link_key[6],
		link_key[7],
		link_key[8],
		link_key[9],
		link_key[10],
		link_key[11],
		link_key[12],
		link_key[13],
		link_key[14],
		link_key[15],
		key_type
		);
	}

static void	parseHCI_EV_CLOCK_OFFSET(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CLOCK_OFFSET\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint16_t	clock_offset;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(clock_offset);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tclock_offset: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		clock_offset
		);
	}

static void	parseHCI_EV_PKT_TYPE_CHANGE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_PKT_TYPE_CHANGE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint16_t	pkt_type;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(pkt_type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tpkt_type: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		pkt_type
		);
	}

static void	parseHCI_EV_PSCAN_REP_MODE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_PSCAN_REP_MODE\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		pscan_rep_mode;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(pscan_rep_mode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpscan_rep_mode: 0x%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		pscan_rep_mode
		);
	}

static void	parseHCI_EV_INQUIRY_RESULT_WITH_RSSI(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_INQUIRY_RESULT_WITH_RSSI\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		pscan_rep_mode;
	uint8_t		pscan_period_mode;
	uint8_t		pscan_mode;
	uint8_t		dev_class[3];
	uint16_t	clock_offset;
	int8_t		rssi;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(pscan_rep_mode);
	decoder.decode(pscan_period_mode);
	decoder.decode(pscan_mode);
	decoder.copyOut(dev_class,sizeof(dev_class));
	decoder.decode(clock_offset);
	decoder.decode(rssi);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint32_t	deviceClass;

	deviceClass	= dev_class[2];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[1];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[0];

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpscan_rep_mode: 0x%2.2X\n"
		"\tpscan_period_mode: 0x%2.2X\n"
		"\tpscan_mode: 0x%2.2X\n"
		"\tdev_class: (0x%6.6X) %s\n"
		"\tclock_offset: 0x%4.4X\n"
		"\trssi: %d\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		pscan_rep_mode,
		pscan_period_mode,
		pscan_mode,
		deviceClass,
		classOfDevice(deviceClass),
		clock_offset,
		rssi
		);
	}

static void	parseHCI_EV_REMOTE_EXT_FEATURES(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_EXT_FEATURES\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		page;
	uint8_t		max_page;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(page);
	decoder.decode(max_page);
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tpage: 0x%2.2X\n"
		"\tmax_page: 0x%2.2X\n"
		"\tfeatures: (0x%16.16llX):\n"
		"\n",
		status,
		statusCode(status),
		handle,
		page,
		max_page,
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t[%u] %s\n",
			i,
			featureOnPage(page,i)
			);
		}
	}

static void	parseHCI_EV_SYNC_CONN_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_SYNC_CONN_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		bdaddr[6];
	uint8_t		link_type;
	uint8_t		tx_interval;
	uint8_t		retrans_window;
	uint16_t	rx_pkt_len;
	uint16_t	tx_pkt_len;
	uint8_t		air_mode;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(link_type);
	decoder.decode(tx_interval);
	decoder.decode(retrans_window);
	decoder.decode(rx_pkt_len);
	decoder.decode(tx_pkt_len);
	decoder.decode(air_mode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tlink_type: 0x%2.2X\n"
		"\ttx_interval: 0x%2.2X\n"
		"\tretrans_window: 0x%2.2X\n"
		"\trx_pkt_len: 0x%4.4X\n"
		"\ttx_pkt_len: 0x%4.4X\n"
		"\tair_mode: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		link_type,
		tx_interval,
		retrans_window,
		rx_pkt_len,
		tx_pkt_len,
		air_mode
		);
	}

static void	parseHCI_EV_SYNC_CONN_CHANGED(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_SYNC_CONN_CHANGED\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		tx_interval;
	uint8_t		retrans_window;
	uint16_t	rx_pkt_len;
	uint16_t	tx_pkt_len;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(tx_interval);
	decoder.decode(retrans_window);
	decoder.decode(rx_pkt_len);
	decoder.decode(tx_pkt_len);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\ttx_interval: 0x%2.2X\n"
		"\tretrans_window: 0x%2.2X\n"
		"\trx_pkt_len: 0x%4.4X\n"
		"\ttx_pkt_len: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		tx_interval,
		retrans_window,
		rx_pkt_len,
		tx_pkt_len
		);
	}

static void	parseHCI_EV_SNIFF_SUBRATE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_SNIFF_SUBRATE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint16_t	max_tx_latency;
	uint16_t	max_rx_latency;
	uint16_t	max_remote_timeout;
	uint16_t	max_local_timeout;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(max_tx_latency);
	decoder.decode(max_rx_latency);
	decoder.decode(max_remote_timeout);
	decoder.decode(max_local_timeout);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tmax_tx_latency: 0x%4.4X\n"
		"\tmax_rx_latency: 0x%4.4X\n"
		"\tmax_remote_timeout: 0x%4.4X\n"
		"\tmax_local_timeout: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		max_tx_latency,
		max_rx_latency,
		max_remote_timeout,
		max_local_timeout
		);
	}

static void	parseHCI_EV_EXTENDED_INQUIRY_RESULT(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_EXTENDED_INQUIRY_RESULT\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		pscan_rep_mode;
	uint8_t		pscan_period_mode;
	uint8_t		dev_class[3];
	uint16_t	clock_offset;
	int8_t		rssi;
	uint8_t		data[240];

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(pscan_rep_mode);
	decoder.decode(pscan_period_mode);
	decoder.copyOut(dev_class,sizeof(dev_class));
	decoder.decode(clock_offset);
	decoder.decode(rssi);
	decoder.copyOut(data,sizeof(data));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint32_t	deviceClass;

	deviceClass	= dev_class[2];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[1];
	deviceClass	<<= 8;
	deviceClass	|= dev_class[0];

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpscan_rep_mode: 0x%2.2X\n"
		"\tpscan_period_mode: 0x%2.2X\n"
		"\tdev_class: (0x%6.6X) %s\n"
		"\tclock_offset: 0x%4.4X\n"
		"\trssi: %d\n"
		"\tdata[]:",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		pscan_rep_mode,
		pscan_period_mode,
		deviceClass,
		classOfDevice(deviceClass),
		clock_offset,
		rssi
		);
	for(unsigned i=0;i<sizeof(data);++i){
		if(!(i%8)){
			Oscl::Error::Info::log("\n\t\t");
			}
		Oscl::Error::Info::log(
			"%2.2X ",
			data[i]
			);
		}
	Oscl::Error::Info::log("\n");
	}

static void	parseHCI_EV_KEY_REFRESH_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_KEY_REFRESH_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;

	decoder.decode(status);
	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\n",
		status,
		statusCode(status),
		handle
		);
	}

static void	parseHCI_EV_IO_CAPA_REQUEST(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_IO_CAPA_REQUEST\n"
		);

	uint8_t		bdaddr[6];

	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_IO_CAPA_REPLY(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_IO_CAPA_REPLY\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		capability;
	uint8_t		oob_data;
	uint8_t		authentication;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(capability);
	decoder.decode(oob_data);
	decoder.decode(authentication);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tcapability: 0x%2.2X\n"
		"\toob_data: 0x%2.2X\n"
		"\tauthentication: 0x%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		capability,
		oob_data,
		authentication
		);
	}

static void	parseHCI_EV_USER_CONFIRM_REQUEST(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_USER_CONFIRM_REQUEST\n"
		);

	uint8_t		bdaddr[6];
	uint32_t	passkey;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(passkey);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpasskey: 0x%8.8X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		passkey
		);
	}

static void	parseHCI_EV_USER_PASSKEY_REQUEST(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_USER_PASSKEY_REQUEST\n"
		);

	uint8_t		bdaddr[6];

	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_REMOTE_OOB_DATA_REQUEST(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_OOB_DATA_REQUEST\n"
		);

	uint8_t		bdaddr[6];

	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_SIMPLE_PAIR_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_SIMPLE_PAIR_COMPLETE\n"
		);

	uint8_t		status;
	uint8_t		bdaddr[6];

	decoder.decode(status);
	decoder.copyOut(bdaddr,sizeof(bdaddr));

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\n",
		status,
		statusCode(status),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]
		);
	}

static void	parseHCI_EV_USER_PASSKEY_NOTIFY(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_USER_PASSKEY_NOTIFY\n"
		);

	uint8_t		bdaddr[6];
	uint8_t		passkey;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(passkey);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tpasskey: 0x%8.8X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		passkey
		);
	}

static void	parseHCI_EV_KEYPRESS_NOTIFY(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_KEYPRESS_NOTIFY\n"
		);
/*
#define HCI_KEYPRESS_STARTED        0
#define HCI_KEYPRESS_ENTERED        1
#define HCI_KEYPRESS_ERASED     2
#define HCI_KEYPRESS_CLEARED        3
#define HCI_KEYPRESS_COMPLETED      4
*/
	uint8_t		bdaddr[6];
	uint8_t		type;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(type);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\ttype: 0x%2.2X\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		type
		);
	}

static void	parseHCI_EV_REMOTE_HOST_FEATURES(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_REMOTE_HOST_FEATURES\n"
		);

	uint8_t		bdaddr[6];
	uint64_t	features;

	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(features);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tfeatures: (0x%16.16llX:"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t[%u] %s\n",
			i,
			leLmpFeature(i)
			);
		}
	}

static void	parseHCI_EV_LE_CONN_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LE_CONN_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		role;
	uint8_t		bdaddr_type;
	uint8_t		bdaddr[6];
	uint16_t	interval;
	uint16_t	latency;
	uint16_t	supervision_timeout;
	uint8_t		clk_accuracy;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(role);
	decoder.decode(bdaddr_type);
	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(interval);
	decoder.decode(latency);
	decoder.decode(supervision_timeout);
	decoder.decode(clk_accuracy);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\trole: (0x%2.2X) %s\n"
		"\tbdaddr_type: (0x%2.2X) %s\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tinterval: (0x%4.4X) %f ms\n"
		"\tlatency: (0x%4.4X) %u events\n"
		"\tsupervision_timeout: (0x%4.4X) %f ms\n"
		"\tclk_accuracy: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		role,
		Oscl::Bluetooth::Strings::leAddressType(role),
		bdaddr_type,
		Oscl::Bluetooth::Strings::leAddressType(bdaddr_type),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		interval,
		(interval * 1.35),
		latency,
		latency,
		supervision_timeout,
		(supervision_timeout * 10.0),
		clk_accuracy
		);
	}

static void	parseHCI_EV_LE_ADVERTISING_REPORT(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LE_ADVERTISING_REPORT\n"
		);

	uint8_t		evt_type;
	uint8_t		bdaddr_type;
	uint8_t		bdaddr[6];
	uint8_t		length;

	decoder.decode(evt_type);
	decoder.decode(bdaddr_type);
	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(length);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tevt_type: (0x%2.2X) %s\n"
		"\tbdaddr_type: (0x%2.2X) %s\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tlength: %u\n"
		"\tdata:"
		"\n",
		evt_type,
		leEventType(evt_type),
		bdaddr_type,
		leBdaddrType(bdaddr_type),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		length
		);

	parseEirFields(dec,length);
	}

static void	parseHCI_EV_LE_ADVERTISING_REPORTS(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	if(suppressAdvertisments){
		return;
		}

	Oscl::Error::Info::log(
		"\tHCI_EV_LE_ADVERTISING_REPORTS\n"
		);

	uint8_t		num_reports;

	decoder.decode(num_reports);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tnum_reports: %u\n"
		"\n",
		num_reports
		);

	for(unsigned i=0;i<num_reports;++i){
		parseHCI_EV_LE_ADVERTISING_REPORT(dec);
		}
	}

static void	parseHCI_EV_LE_CONN_UPDATE_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_LE_CONN_UPDATE_COMPLETE\n"
		);
	}

static void	parseHCI_EV_LE_REMOTE_FEAT_COMPLETE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LE_REMOTE_FEAT_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint64_t	features;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(features);

	Oscl::Error::Info::log(
		"\t\tstatus: (0x%2.2X) \"%s\"\n"
		"\t\thandle: (0x%4.4X)\n"
		"\t\tfeatures: (0x%16.16llX)\n"
		"",
		status,
		statusCode(status),
		handle,
		(long long unsigned int)features
		);

	for(unsigned i=0;i<64;++i){
		bool
		set	= features & (1ULL<<i);
		if(!set){
			continue;
			}
		Oscl::Error::Info::log(
			"\t\t\t[%u] %s\n",
			i,
			leLlFeature(i)
			);
		}
	}

static void	parseHCI_EV_LE_LTK_REQ(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_LE_LTK_REQ\n"
		);
	}

static void	parseHCI_EV_LE_REMOTE_CONN_PARAM_REQ(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_LE_REMOTE_CONN_PARAM_REQ\n"
		);
	}

static void	parseHCI_EV_LE_DATA_LEN_CHANGE(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_LE_DATA_LEN_CHANGE\n"
		);
	}

static void	parseHCI_EV_LE_DIRECT_ADV_REPORT(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LE_DIRECT_ADV_REPORT\n"
		);

	uint8_t		evt_type;
	uint8_t		bdaddr_type;
	uint8_t		bdaddr[6];
	uint8_t		direct_addr_type;
	uint8_t		direct_addr[6];
	int8_t		rssi;

	decoder.decode(evt_type);
	decoder.decode(bdaddr_type);
	decoder.copyOut(bdaddr,sizeof(bdaddr));
	decoder.decode(direct_addr_type);
	decoder.copyOut(direct_addr,sizeof(direct_addr));
	decoder.decode(rssi);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tevt_type: %s\n"
		"\tbdaddr_type: %s\n"
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tdirect_addr_type: 0x%2.2X\n"
		"\tdirect_addr: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\trssi: %d\n"
		"\tdata:"
		"\n",
		leEventType(evt_type),
		leBdaddrType(bdaddr_type),
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		direct_addr_type,
		direct_addr[5],direct_addr[4],direct_addr[3],direct_addr[2],direct_addr[1],direct_addr[0],
		rssi
		);

	}

static void	parseHCI_EV_STACK_INTERNAL(
				Oscl::Endian::Decoder::Api& dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_STACK_INTERNAL\n"
		);
	}

static void	parseHCI_EV_LE_META(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	// LE for "Low Energy"

	#if 0
	Oscl::Error::Info::log(
		"\tHCI_EV_LE_META\n"
		);
	#endif

	uint8_t		subevent;

	decoder.decode(subevent);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	switch(subevent){
		case 0x01:
			parseHCI_EV_LE_CONN_COMPLETE(
				dec
				);
			break;
		case 0x02:
			parseHCI_EV_LE_ADVERTISING_REPORTS(
				dec
				);
			break;
		case 0x03:
			parseHCI_EV_LE_CONN_UPDATE_COMPLETE(
				dec
				);
			break;
		case 0x04:
			parseHCI_EV_LE_REMOTE_FEAT_COMPLETE(
				dec
				);
			break;
		case 0x05:
			parseHCI_EV_LE_LTK_REQ(
				dec
				);
			break;
		case 0x06:
			parseHCI_EV_LE_REMOTE_CONN_PARAM_REQ(
				dec
				);
			break;
		case 0x07:
			parseHCI_EV_LE_DATA_LEN_CHANGE(
				dec
				);
			break;
		case 0x08:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_READ_LOCAL_P256_PUBLIC_KEY_COMPLETE\n"
				);
			break;
		case 0x09:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_GEN_DHKEY_COMPLETE\n"
				);
			break;
		case 0x0A:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_ENHANCED_CONN_COMPLETE\n"
				);
			break;
		case 0x0B:
			parseHCI_EV_LE_DIRECT_ADV_REPORT(
				dec
				);
			break;
		case 0x0C:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_PHY_UPDATE_COMPLETE\n"
				);
			break;
		case 0x0D:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_EXT_ADV_REPORT\n"
				);
			break;
		case 0x0E:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_PERIODIC_ADV_SYNC_ESTABLISHED\n"
				);
			break;
		case 0x0F:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_PERIODIC_ADV_REPORT\n"
				);
			break;
		case 0x10:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_PERIODIC_SYNC_LOST\n"
				);
			break;
		case 0x11:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_SCAN_TIMEOUT\n"
				);
			break;
		case 0x12:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_ADV_SET_TERMINATED\n"
				);
			break;
		case 0x13:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_SCAN_REQUEST_RECEIVED\n"
				);
			break;
		case 0x14:
			Oscl::Error::Info::log(
				"\tHCI_EV_LE_CHANNEL_SELECTION_ALGO\n"
				);
			break;
		case 0xfd:
			parseHCI_EV_STACK_INTERNAL(
				dec
				);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected\n"
				"\tsubevent: 0x%2.2X\n"
				"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				subevent
				);
			break;
		}
	}

static void	parseHCI_EV_PHY_LINK_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_PHY_LINK_COMPLETE\n"
		);

	uint8_t		status;
	uint8_t		phy_handle;

	decoder.decode(status);
	decoder.decode(phy_handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tphy_handle: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		phy_handle
		);
	}

static void	parseHCI_EV_CHANNEL_SELECTED(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_CHANNEL_SELECTED\n"
		);

	uint8_t		phy_handle;

	decoder.decode(phy_handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tphy_handle: 0x%2.2X\n"
		"\n",
		phy_handle
		);
	}

static void	parseHCI_EV_DISCONN_PHY_LINK_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_DISCONN_PHY_LINK_COMPLETE\n"
		);

	uint8_t		status;
	uint8_t		phy_handle;
	uint8_t		reason;

	decoder.decode(status);
	decoder.decode(phy_handle);
	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\tphy_handle: 0x%2.2X\n"
		"\treason: (0x%2.2X) \"%s\"\n"
		"\n",
		status,
		statusCode(status),
		phy_handle,
		reason,
		statusCode(reason)
		);
	}

static void	parseHCI_EV_LOGICAL_LINK_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_LOGICAL_LINK_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		phy_handle;
	uint8_t		flow_spec_id;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(phy_handle);
	decoder.decode(flow_spec_id);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\tphy_handle: 0x%2.2X\n"
		"\tflow_spec_id: 0x%2.2X\n"
		"\n",
		status,
		statusCode(status),
		handle,
		phy_handle,
		flow_spec_id
		);
	}

static void	parseHCI_EV_DISCONN_LOGICAL_LINK_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_DISCONN_LOGICAL_LINK_COMPLETE\n"
		);

	uint8_t		status;
	uint16_t	handle;
	uint8_t		reason;

	decoder.decode(status);
	decoder.decode(handle);
	decoder.decode(reason);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\thandle: 0x%4.4X\n"
		"\treason: (0x%2.2X) \"%s\"\n"
		"\n",
		status,
		statusCode(status),
		handle,
		reason,
		statusCode(reason)
		);
	}

static void	parseHCI_EV_NUM_COMP_BLOCKS(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_NUM_COMP_BLOCKS\n"
		);

	uint16_t	num_blocks;
	uint16_t	num_hndl;

	decoder.decode(num_blocks);
	decoder.decode(num_hndl);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tnum_block: 0x%4.4X\n"
		"\tnum_hndls: 0x%4.4X\n"
		"\n",
		num_blocks,
		num_hndl
		);
	for(unsigned i=0;i<num_hndl;++i){
		uint16_t	handle;
		uint16_t	pkts;
		uint16_t	blocks;

		decoder.decode(handle);
		decoder.decode(pkts);
		decoder.decode(blocks);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"\thandles[%u]\n"
			"\t\thandle: 0x%4.4X\n"
			"\t\tpkts: 0x%4.4X\n"
			"\t\tblocks: 0x%4.4X\n"
			"",
			i,
			handle,
			pkts,
			blocks
			);
		}
	}

static void	parseHCI_EV_SYNC_TRAIN_COMPLETE(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.le();

	Oscl::Error::Info::log(
		"\tHCI_EV_SYNC_TRAIN_COMPLETE\n"
		);

	uint8_t		status;

	decoder.decode(status);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tstatus: 0x%2.2X \"%s\"\n"
		"\n",
		status,
		statusCode(status)
		);
	}

static void	parseHCI_EV_SLAVE_PAGE_RESP_TIMEOUT(
				Oscl::Endian::Decoder::Api&	dec
				) noexcept{
	Oscl::Error::Info::log(
		"\tHCI_EV_SLAVE_PAGE_RESP_TIMEOUT\n"
		);
	}


void	Oscl::Bluetooth::Debug::Print::parseHCI_MON_EVENT_PKT(
			unsigned	index,
			unsigned	len,
			const char*	devIndexName,
			const void*	payload
			) noexcept{
	#if 0
	Oscl::Error::Info::log(
		"%s: HCI_MON_EVENT_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	decoder(
		payload,
		len
		);

	uint8_t	evt;
	uint8_t	plen;

	decoder.le().decode(evt);
	decoder.le().decode(plen);

	if(decoder.le().underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	#if 0
	Oscl::Error::Info::log(
		"\tevt: 0x%2.2X\n"
		"\tplen: %u octets\n"
		"\n",
		evt,
		plen
		);
	#endif

	switch(evt){
		case 0x01:
			parseHCI_EV_INQUIRY_COMPLETE(
				decoder
				);
			break;
		case 0x02:
			parseHCI_EV_INQUIRY_RESULT(
				decoder
				);
			break;
		case 0x03:
			parseHCI_EV_CONN_COMPLETE(
				decoder
				);
			break;
		case 0x04:
			parseHCI_EV_CONN_REQUEST(
				decoder
				);
			break;
		case 0x05:
			parseHCI_EV_DISCONN_COMPLETE(
				decoder
				);
			break;
		case 0x06:
			parseHCI_EV_AUTH_COMPLETE(
				decoder
				);
			break;
		case 0x07:
			parseHCI_EV_REMOTE_NAME(
				decoder
				);
			break;

		case 0x08:
			parseHCI_EV_ENCRYPT_CHANGE(
				decoder
				);
			break;
		case 0x09:
			parseHCI_EV_CHANGE_LINK_KEY_COMPLETE(
				decoder
				);
			break;
		case 0x0b:
			parseHCI_EV_REMOTE_FEATURES(
				decoder
				);
			break;
		case 0x0c:
			parseHCI_EV_REMOTE_VERSION(
				decoder
				);
			break;
		case 0x0d:
			parseHCI_EV_QOS_SETUP_COMPLETE(
				decoder
				);
			break;
		case 0x0e:
			parseHCI_EV_CMD_COMPLETE(
				decoder
				);
			break;
		case 0x0f:
			parseHCI_EV_CMD_STATUS(
				decoder
				);
			break;
		case 0x10:
			parseHCI_EV_HARDWARE_ERROR(
				decoder
				);
			break;
		case 0x12:
			parseHCI_EV_ROLE_CHANGE(
				decoder
				);
			break;
		case 0x13:
			parseHCI_EV_NUM_COMP_PKTS(
				decoder
				);
			break;
		case 0x14:
			parseHCI_EV_MODE_CHANGE(
				decoder
				);
			break;
		case 0x16:
			parseHCI_EV_PIN_CODE_REQ(
				decoder
				);
			break;
		case 0x17:
			parseHCI_EV_LINK_KEY_REQ(
				decoder
				);
			break;
		case 0x18:
			parseHCI_EV_LINK_KEY_NOTIFY(
				decoder
				);
			break;
		case 0x1c:
			parseHCI_EV_CLOCK_OFFSET(
				decoder
				);
			break;
		case 0x1d:
			parseHCI_EV_PKT_TYPE_CHANGE(
				decoder
				);
			break;
		case 0x20:
			parseHCI_EV_PSCAN_REP_MODE(
				decoder
				);
			break;
		case 0x22:
			parseHCI_EV_INQUIRY_RESULT_WITH_RSSI(
				decoder
				);
			break;
		case 0x23:
			parseHCI_EV_REMOTE_EXT_FEATURES(
				decoder
				);
			break;
		case 0x2c:
			parseHCI_EV_SYNC_CONN_COMPLETE(
				decoder
				);
			break;
		case 0x2d:
			parseHCI_EV_SYNC_CONN_CHANGED(
				decoder
				);
			break;
		case 0x2e:
			parseHCI_EV_SNIFF_SUBRATE(
				decoder
				);
			break;
		case 0x2f:
			parseHCI_EV_EXTENDED_INQUIRY_RESULT(
				decoder
				);
			break;
		case 0x30:
			parseHCI_EV_KEY_REFRESH_COMPLETE(
				decoder
				);
			break;
		case 0x31:
			parseHCI_EV_IO_CAPA_REQUEST(
				decoder
				);
			break;
		case 0x32:
			parseHCI_EV_IO_CAPA_REPLY(
				decoder
				);
			break;
		case 0x33:
			parseHCI_EV_USER_CONFIRM_REQUEST(
				decoder
				);
			break;
		case 0x34:
			parseHCI_EV_USER_PASSKEY_REQUEST(
				decoder
				);
			break;
		case 0x35:
			parseHCI_EV_REMOTE_OOB_DATA_REQUEST(
				decoder
				);
			break;
		case 0x36:
			parseHCI_EV_SIMPLE_PAIR_COMPLETE(
				decoder
				);
			break;
		case 0x3b:
			parseHCI_EV_USER_PASSKEY_NOTIFY(
				decoder
				);
			break;
		case 0x3c:
			parseHCI_EV_KEYPRESS_NOTIFY(
				decoder
				);
			break;
		case 0x3d:
			parseHCI_EV_REMOTE_HOST_FEATURES(
				decoder
				);
			break;
		case 0x3e:
			parseHCI_EV_LE_META(
				decoder
				);
			break;
		case 0x40:
			parseHCI_EV_PHY_LINK_COMPLETE(
				decoder
				);
			break;
		case 0x41:
			parseHCI_EV_CHANNEL_SELECTED(
				decoder
				);
			break;
		case 0x42:
			parseHCI_EV_DISCONN_PHY_LINK_COMPLETE(
				decoder
				);
			break;
		case 0x45:
			parseHCI_EV_LOGICAL_LINK_COMPLETE(
				decoder
				);
			break;
		case 0x46:
			parseHCI_EV_DISCONN_LOGICAL_LINK_COMPLETE(
				decoder
				);
			break;
		case 0x48:
			parseHCI_EV_NUM_COMP_BLOCKS(
				decoder
				);
			break;
		case 0x4F:
			parseHCI_EV_SYNC_TRAIN_COMPLETE(
				decoder
				);
			break;
		case 0x54:
			parseHCI_EV_SLAVE_PAGE_RESP_TIMEOUT(
				decoder
				);
			break;
		default:
			{
			Oscl::Error::Info::log(
				"%s: Unexpected HCI_MON_EVENT_PKT, "
				"index %s, "
				"len %u"
				"\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				devIndexName,
				len
				);
			Oscl::Error::Info::log(
				"\tevt: 0x%2.2X\n"
				"\tplen: %u octets\n"
				"\n",
				evt,
				plen
				);
			const uint8_t*	p = (const uint8_t*)payload;

			unsigned	remain	= decoder.le().remaining();
			unsigned	offset	= len-remain;

			Oscl::Error::Info::hexDump(
				&p[offset],
				remain
				);
			}
		}
	}

static void	parseAttErrorResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		requestOpcode;
	uint16_t	attributeHandle;
	uint8_t		errorCode;

	decoder.decode(requestOpcode);
	decoder.decode(attributeHandle);
	decoder.decode(errorCode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tRequest Opcode: (0x%2.2X) %s\n"
		"\tAttribute Handle: (0x%4.4X)\n"
		"\tError Code: (0x%2.2X) %s\n"
		"",
		requestOpcode,
		Oscl::Bluetooth::Strings::attOpcode(requestOpcode),
		attributeHandle,
		errorCode,
		Oscl::Bluetooth::Strings::attErrorCode(errorCode)
		);
	}

static void	parseAttExchangeMtuRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	clientRxMTU;

	decoder.decode(clientRxMTU);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tClient RX MTU: (0x%4.4X) %u octets\n"
		"",
		clientRxMTU,
		clientRxMTU
		);
	}

static void	parseAttExchangeMtuResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	serverRxMTU;

	decoder.decode(serverRxMTU);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tServer RX MTU: (0x%4.4X) %u octets\n"
		"",
		serverRxMTU,
		serverRxMTU
		);
	}

static void	parseAttFindInformationRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"",
		startingHandle,
		endingHandle
		);
	}

static void	parseAttHandlesAnd16bitUUIDList(Oscl::Decoder::Api& decoder) noexcept{
	unsigned
	remaining	= decoder.remaining();

	unsigned
	nItems		= remaining/(sizeof(uint16_t)+sizeof(uint16_t));

	for(unsigned i=0;i<nItems;++i){
		uint16_t	handle;
		uint16_t	uuid;
		decoder.decode(handle);
		decoder.decode(uuid);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		Oscl::Error::Info::log(
			"Handle: 0x%4.4X, UUID: 0x%4.4X\n"
			"",
			handle,
			uuid
			);
		}
	}

static void	parseAttHandlesAnd128bitUUIDList(Oscl::Decoder::Api& decoder) noexcept{
	unsigned
	remaining	= decoder.remaining();

	unsigned
	nItems		= remaining/(sizeof(uint16_t)+16);

	for(unsigned i=0;i<nItems;++i){
		uint16_t	handle;
		uint8_t		uuid[16];

		decoder.decode(handle);
		decoder.copyOut(
			uuid,
			16
			);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}

		char	uuidString[128];

		Oscl::UUID::String::uuidToString(
			uuid,
			uuidString,
			sizeof(uuidString)
			);

		uuidString[127]	= '\0';

		Oscl::Error::Info::log(
			"Handle: 0x%4.4X, "
			"UUID: %s"
			"",
			handle,
			uuidString
			);
		}
	}

static void	parseAttFindInformationResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t	format;

	decoder.decode(format);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tFormat: (0x%4.4X) %s\n"
		"",
		format,
		Oscl::Bluetooth::Strings::attFormat(format)
		);

	if(format == 0x01){
		return parseAttHandlesAnd16bitUUIDList(decoder);
		}

	if(format == 0x02){
		return parseAttHandlesAnd128bitUUIDList(decoder);
		}
	}

static void	parseAttFindByTypeValueRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	startingHandle;
	uint16_t	endingHandle;
	uint16_t	attributeType;

	uint8_t		value[512];

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);
	decoder.decode(attributeType);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"\tAttribute Type: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		startingHandle,
		endingHandle,
		attributeType,
		remaining
		);

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttFindByTypeValueResponse(Oscl::Decoder::Api& decoder) noexcept{

	uint8_t		value[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttReadByTypeRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		startingHandle,
		endingHandle,
		decoder.remaining()
		);

	unsigned
	remaining	= decoder.remaining();

	if(remaining < 2){
		Oscl::Error::Info::log(
			"%s: invalid PDU UUID too short!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	if(remaining > 2){
		if(remaining < 16){
			Oscl::Error::Info::log(
				"%s: invalid PDU 128-bit UUID too short!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	uint8_t	uuid128[16];

	if(remaining < 3){
		uint16_t	type;
		decoder.decode(type);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::BT::UUID::Base::make128bitUUID(uuid128,type);
		}
	else {
		decoder.copyOut(
			uuid128,
			sizeof(uuid128)
			);
		}

	char	uuidString[128];

	bool
	failed	= Oscl::UUID::String::uuidToString(
            uuid128,
			uuidString,
            sizeof(uuidString)
            );

	if(failed){
		Oscl::Error::Info::log(
			"%s: uuidToString() failed!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tAttribute Type: (%s) %s\n"
		"",
		uuidString,
		Oscl::Bluetooth::Strings::gattAttributeType(uuid128)
		);
	}

static void	parseAttReadByTypeResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t	length;

	decoder.decode(length);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	unsigned
	remaining	= decoder.remaining();

	unsigned
	nAdRecords	= 0;

	if(remaining){
		nAdRecords	= remaining/length;
		}

	Oscl::Error::Info::log(
		"\tlength: (0x%4.4X) %u\n"
		"\tremaining: %u\n"
		"\tn attribute data records: %u\n"
		"",
		length,
		length,
		remaining,
		nAdRecords
		);

	if(remaining < 2){
		Oscl::Error::Info::log(
			"%s: invalid PDU attribute data too short!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	for(unsigned i=0;i<nAdRecords;++i){
		uint8_t	adRecord[256];
		decoder.copyOut(
			adRecord,
			length
			);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::Error::Info::log(
			"[%u]\n",
			i
			);
		Oscl::Error::Info::hexDump(
			adRecord,
			length
			);
		}
	}

static void	parseAttReadRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;

	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"",
		handle
		);
	}

static void	parseAttReadResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		value[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttReadBlobRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;
	uint16_t	valueOffset;

	decoder.decode(handle);
	decoder.decode(valueOffset);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"\tValue Offset: (0x%4.4X) %u\n"
		"",
		handle,
		valueOffset,
		valueOffset
		);
	}

static void	parseAttReadBlobResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		value[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttReadMultipleRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		value[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttReadMultipleResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		value[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttReadByGroupTypeRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	startingHandle;
	uint16_t	endingHandle;

	decoder.decode(startingHandle);
	decoder.decode(endingHandle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tStarting Handle: (0x%4.4X)\n"
		"\tEnding Handle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		startingHandle,
		endingHandle,
		decoder.remaining()
		);

	unsigned
	remaining	= decoder.remaining();

	if(remaining < 2){
		Oscl::Error::Info::log(
			"%s: invalid PDU UUID too short!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	if(remaining > 2){
		if(remaining < 16){
			Oscl::Error::Info::log(
				"%s: invalid PDU 128-bit UUID too short!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		}

	uint8_t	uuid128[16];

	if(remaining < 3){
		uint16_t	type;
		decoder.decode(type);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION_DETAIL
				);
			return;
			}
		Oscl::BT::UUID::Base::make128bitUUID(uuid128,type);
		}
	else {
		decoder.copyOut(
			uuid128,
			sizeof(uuid128)
			);
		}

	char	uuidString[128];

	bool
	failed	= Oscl::UUID::String::uuidToString(
            uuid128,
			uuidString,
            sizeof(uuidString)
            );

	if(failed){
		Oscl::Error::Info::log(
			"%s: uuidToString() failed!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tAttribute Group Type: (%s) %s\n"
		"",
		uuidString,
		Oscl::Bluetooth::Strings::gattAttributeType(uuid128)
		);

	}

static void	parseAttReadByGroupTypeResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t	length;

	decoder.decode(length);

	uint8_t	buffer[512];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		buffer,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Error::Info::log(
		"\tLength: (0x%4.4X) %u\n"
		"\tremaining: %u\n"
		"",
		length,
		length,
		remaining
		);

	Oscl::Error::Info::hexDump(
		buffer,
		remaining
		);
	}

static void	parseAttWriteRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;
	uint8_t		value[512];

	decoder.decode(handle);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		handle,
		remaining
		);
	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttWriteResponse(Oscl::Decoder::Api& decoder) noexcept{
	// Nothing further
	}

static void	parseAttPrepareWriteRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;
	uint16_t	valueOffset;
	uint8_t		value[512];

	decoder.decode(handle);
	decoder.decode(valueOffset);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"\tValueOffset: (0x%4.4X) %u\n"
		"\tremaining: %u\n"
		"",
		handle,
		valueOffset,
		valueOffset,
		remaining
		);
	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttPrepareWriteResponse(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;
	uint16_t	valueOffset;
	uint8_t		value[512];

	decoder.decode(handle);
	decoder.decode(valueOffset);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"\tValueOffset: (0x%4.4X) %u\n"
		"\tremaining: %u\n"
		"",
		handle,
		valueOffset,
		valueOffset,
		remaining
		);
	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttExecuteWriteRequest(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t	flags;

	decoder.decode(flags);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}
	Oscl::Error::Info::log(
		"\tFlags: (0x%2.2X)\n"
		"",
		flags
		);
	}

static void	parseAttExecuteWriteResponse(Oscl::Decoder::Api& decoder) noexcept{
	// Nothing further
	}

static void	parseAttHandleValueNotification(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;

	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	unsigned
	remaining	= decoder.remaining();

	uint8_t	value[Oscl::BT::ATT::Size::attributeValueLengthMaximum];

	Oscl::Error::Info::log(
		"\t\tHandle (0x%4.4X)\n"
		"\t\tremaining: %u\n"
		"\t\tsizeof(value[]): %zu\n"
		"\t\tValue:\n"
		"",
		handle,
		remaining,
		sizeof(value)
		);

	if(remaining > sizeof(value)){
		remaining	= sizeof(value);
		}

	decoder.copyOut(
		value,
		remaining
		);

	// At this point, we will consider this as a
	// Mesh Provisioning Data Out or
	// Mesh Provisioning Data In PDU
	// even though that is not generally true
	// of a Handle Value Notification.

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		&value[0],
		remaining
		);

	Oscl::Decoder::Api&
	vDecoder	= leDecoder.le();

	uint8_t	opcode;
	vDecoder.decode(opcode);

	if(vDecoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	sar	= opcode >> 6;

	// Strip SAR
	opcode	&= 0x3F;

	Oscl::Error::Info::log(
		"\t\t\tMesh Provisioning/Proxy Data In/Out:\n"
		"\t\t\t\tOpcode: (0x%2.2X)\n"
		"\t\t\t\tSAR bits: (0x%2.2X)\n"
		"",
		opcode,
		sar
		);

	switch(opcode){
		case 0x00:
			// Network PDU
			// <<Mesh Proxy Service>>
			Oscl::Error::Info::log(
				"\t\t\t\tNetwork PDU\n"
				);
			break;
		case 0x01:
			// Mesh Beacon
			// <<Mesh Proxy Service>>
			Oscl::Error::Info::log(
				"\t\t\t\tMesh Beacon\n"
				);
			break;
		case 0x02:
			// Proxy Configuration
			// <<Mesh Proxy Service>>
			// - 0x00 Set Filter Type
			// - 0x01 Add Addresses To Filter
			// - 0x02 Remove Addresses From Filter
			// - 0x03 Filter Status
			Oscl::Error::Info::log(
				"\t\t\t\tProxy Configuration\n"
				);
			processProxyConfiguration(vDecoder);
			break;
		case 0x03:
			// Provisioning PDU
			// <<Mesh Proxy Service>>
			// <<Mesh Proviioning Service>>
			Oscl::Error::Info::log(
				"\t\t\t\tProvisioning PDU\n"
				);
			processProvisioningPDU(vDecoder);
			break;
		default:
			// Probably the SAR bits are set
			// if it is a Mesh Provisioning Data In/Out PDU.
			Oscl::Error::Info::log(
				"\t\t\t\tUnknown\n"
				);
			break;
		}

	Oscl::Error::Info::log(
		"\t\t\tRaw Value:\n"
		);
	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttHandleValueIndication(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;

	uint8_t		value[512];

	decoder.decode(handle);

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		value,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\tHandle: (0x%4.4X)\n"
		"\tremaining: %u\n"
		"",
		handle,
		remaining
		);

	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttHandleValueConfirmation(Oscl::Decoder::Api& decoder) noexcept{
	// Nothing further
	}

static void	parseAttWriteCommand(Oscl::Decoder::Api& decoder) noexcept{

	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION_DETAIL
		);

	uint16_t	handle;

	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	unsigned
	remaining	= decoder.remaining();

	uint8_t	value[Oscl::BT::ATT::Size::attributeValueLengthMaximum];

	Oscl::Error::Info::log(
		"\t\tHandle (0x%4.4X)\n"
		"\t\tremaining: %u\n"
		"\t\tsizeof(value[]): %zu\n"
		"\t\tValue:\n"
		"",
		handle,
		remaining,
		sizeof(value)
		);

	if(remaining > sizeof(value)){
		remaining	= sizeof(value);
		}

	decoder.copyOut(
		value,
		remaining
		);

	// At this point, we will consider this as a
	// Mesh Provisioning Data Out or
	// Mesh Provisioning Data In PDU
	// even though that is not generally true
	// of a Handle Value Notification.

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		&value[0],
		remaining
		);

	Oscl::Decoder::Api&
	vDecoder	= leDecoder.le();

	uint8_t	opcode;
	vDecoder.decode(opcode);

	if(vDecoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint8_t	sar	= opcode >> 6;

	// Strip SAR
	opcode	&= 0x3F;

	Oscl::Error::Info::log(
		"\t\t\tMesh Provisioning/Proxy Data In/Out:\n"
		"\t\t\t\tOpcode: (0x%2.2X)\n"
		"\t\t\t\tSAR bits: (0x%2.2X)\n"
		"",
		opcode,
		sar
		);

	switch(opcode){
		case 0x00:
			// Network PDU
			Oscl::Error::Info::log(
				"\t\t\t\tNetwork PDU\n"
				);
			break;
		case 0x01:
			// Mesh Beacon
			Oscl::Error::Info::log(
				"\t\t\t\tMesh Beacon\n"
				);
			break;
		case 0x02:
			// Proxy Configuration
			Oscl::Error::Info::log(
				"\t\t\t\tProxy Configuration\n"
				);
			processProxyConfiguration(vDecoder);
			break;
		case 0x03:
			// Provisioning PDU
			Oscl::Error::Info::log(
				"\t\t\t\tProvisioning PDU\n"
				);
			processProvisioningPDU(vDecoder);
			break;
		default:
			// Probably the SAR bits are set
			// if it is a Mesh Provisioning Data In/Out PDU.
			Oscl::Error::Info::log(
				"\t\t\t\tUnknown\n"
				);
			break;
		}

	Oscl::Error::Info::log(
		"\t\t\tRaw Value:\n"
		);
	Oscl::Error::Info::hexDump(
		value,
		remaining
		);
	}

static void	parseAttSignedWriteCommand(Oscl::Decoder::Api& decoder) noexcept{
	uint16_t	handle;

	decoder.decode(handle);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	unsigned
	remaining	= decoder.remaining();

	uint8_t	value[Oscl::BT::ATT::Size::attributeValueLengthMaximum];

	if(remaining > sizeof(value)){
		remaining	= sizeof(value);
		}

	Oscl::Error::Info::log(
		"\t\tHandle (0x%4.4X)\n"
		"\t\tremaining: %u\n"
		"\t\tValue:\n"
		"",
		handle,
		remaining
		);

	if(remaining < 12){
		Oscl::Error::Info::log(
			"%s: payload less than length of valid signature (12)!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::hexDump(
		&value[0],
		remaining-12
		);

	Oscl::Error::Info::log(
		"\t\tAuthentication Signature:\n"
		""
		);

	Oscl::Error::Info::hexDump(
		&value[remaining-12],
		12
		);
	}

static void	parseAttPDU(
				Oscl::Decoder::Api&	decoder
				) noexcept{
	uint8_t	opcode;
	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		};

	Oscl::Error::Info::log(
		"\tATT Opcode (0x%2.2X) %s\n"
		"",
		opcode,
		Oscl::Bluetooth::Strings::attOpcode(opcode)
		);

	switch(opcode){
		case 0x01:
			return parseAttErrorResponse(decoder);
		case 0x02:
			return parseAttExchangeMtuRequest(decoder);
		case 0x03:
			return parseAttExchangeMtuResponse(decoder);
		case 0x04:
			return parseAttFindInformationRequest(decoder);
		case 0x05:
			return parseAttFindInformationResponse(decoder);
		case 0x06:
			return parseAttFindByTypeValueRequest(decoder);
		case 0x07:
			return parseAttFindByTypeValueResponse(decoder);
		case 0x08:
			return parseAttReadByTypeRequest(decoder);
		case 0x09:
			return parseAttReadByTypeResponse(decoder);
		case 0x0A:
			return parseAttReadRequest(decoder);
		case 0x0B:
			return parseAttReadResponse(decoder);
		case 0x0C:
			return parseAttReadBlobRequest(decoder);
		case 0x0D:
			return parseAttReadBlobResponse(decoder);
		case 0x0E:
			return parseAttReadMultipleRequest(decoder);
		case 0x0F:
			return parseAttReadMultipleResponse(decoder);
		case 0x10:
			return parseAttReadByGroupTypeRequest(decoder);
		case 0x11:
			return parseAttReadByGroupTypeResponse(decoder);
		case 0x12:
			return parseAttWriteRequest(decoder);
		case 0x15:
			return parseAttWriteResponse(decoder);
		case 0x16:
			return parseAttPrepareWriteRequest(decoder);
		case 0x17:
			return parseAttPrepareWriteResponse(decoder);
		case 0x18:
			return parseAttExecuteWriteRequest(decoder);
		case 0x19:
			return parseAttExecuteWriteResponse(decoder);
		case 0x1B:
			return parseAttHandleValueNotification(decoder);
		case 0x1D:
			return parseAttHandleValueIndication(decoder);
		case 0x1E:
			return parseAttHandleValueConfirmation(decoder);
		case 0x52:
			return parseAttWriteCommand(decoder);
		case 0xD2:
			return parseAttSignedWriteCommand(decoder);
		default:
			break;
		}

	uint8_t	buffer[256];

	unsigned
	remaining	= decoder.remaining();

	decoder.copyOut(
		buffer,
		remaining
		);

	Oscl::Error::Info::hexDump(
		buffer,
		remaining
		);
	}

static void	parseHCI_MON_ACL_TX_PKT(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_ACL_TX_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	uint16_t	handleAndFlags;
	uint16_t	dataTotalLength;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		payload,
		len
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(handleAndFlags);
	decoder.decode(dataTotalLength);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint16_t
	handle	= (handleAndFlags & 0x0FFF);

	uint8_t
	pbFlag	= (handleAndFlags >> 12);
	pbFlag	&= 0x03;

	uint8_t
	bcFlag	= (handleAndFlags >> 14);
	bcFlag	&= 0x03;

	Oscl::Error::Info::log(
		"\tHandle: 0x%4.4X\n"
		"\tPacket_Boundary_Flag: (0x%2.2X) %s\n"
		"\tBroadcast_Flag: (0x%2.2X) %s\n"
		"\tData_Total_Length: (0x%4.4X) %u\n"
		"",
		handle,
		pbFlag,
		Oscl::Bluetooth::Strings::lePacketBoundaryFlag(pbFlag),
		bcFlag,
		Oscl::Bluetooth::Strings::leHostToControllerBroadcastFlag(bcFlag),
		dataTotalLength,
		dataTotalLength
		);

	switch(pbFlag){
		case 0x01:	// Continuing fragment.
			return;
		case 0x00:	// First non-automatically flushable.
			break;
		case 0x02:	// First automatically flushable.
			break;
		case 0x03:	// Complete PDU flushable
			break;
		default:
			return;
		}

	// Decode L2CAP header
	uint16_t	length;
	uint16_t	channelID;

	decoder.decode(length);
	decoder.decode(channelID);

	Oscl::Error::Info::log(
		"\tLength: (0x%4.4X) %u\n"
		"\tChannel ID: (0x%4.4X) %s\n",
		length,
		length,
		channelID,
		Oscl::Bluetooth::Strings::l2capChannelID(channelID)
		);

	const uint8_t*	p	= (const uint8_t*)payload;

	unsigned
	offset	= len - decoder.remaining();


	switch(channelID){
		case 0x0004:
			return parseAttPDU(
					decoder
					);
		default:
			break;
		}

	Oscl::Error::Info::hexDump(
		&p[offset],
		decoder.remaining()
		);
	}

void	Oscl::Bluetooth::Debug::Print::parseHCI_MON_ACL_RX_PKT(
				unsigned	index,
				unsigned	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_ACL_RX_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	uint16_t	handleAndFlags;
	uint16_t	dataTotalLength;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		payload,
		len
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(handleAndFlags);
	decoder.decode(dataTotalLength);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	uint16_t
	handle	= (handleAndFlags & 0x0FFF);

	uint8_t
	pbFlag	= (handleAndFlags >> 12);
	pbFlag	&= 0x03;

	uint8_t
	bcFlag	= (handleAndFlags >> 14);
	bcFlag	&= 0x03;

	Oscl::Error::Info::log(
		"\tHandle: 0x%4.4X\n"
		"\tPacket_Boundary_Flag: (0x%2.2X) %s\n"
		"\tBroadcast_Flag: (0x%2.2X) %s\n"
		"",
		handle,
		pbFlag,
		Oscl::Bluetooth::Strings::lePacketBoundaryFlag(pbFlag),
		bcFlag,
		Oscl::Bluetooth::Strings::leControllerToHostBroadcastFlag(bcFlag)
		);

	switch(pbFlag){
		case 0x01:	// Continuing fragment.
			return;
		case 0x00:	// First non-automatically flushable.
			break;
		case 0x02:	// First automatically flushable.
			break;
		case 0x03:	// Complete PDU flushable
			break;
		default:
			return;
		}

	// Decode L2CAP header
	uint16_t	length;
	uint16_t	channelID;

	decoder.decode(length);
	decoder.decode(channelID);

	Oscl::Error::Info::log(
		"\tLength: (0x%4.4X) %u\n"
		"\tChannel ID: (0x%4.4X) %s\n",
		length,
		length,
		channelID,
		Oscl::Bluetooth::Strings::l2capChannelID(channelID)
		);

	const uint8_t*	p	= (const uint8_t*)payload;

	unsigned
	offset	= len - decoder.remaining();


	switch(channelID){
		case 0x0004:
			return parseAttPDU(
					decoder
					);
		default:
			break;
		}

	Oscl::Error::Info::hexDump(
		&p[offset],
		decoder.remaining()
		);
	}

static void	parseHCI_MON_SCO_TX_PKT(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_SCO_TX_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_SCO_RX_PKT(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_SCO_RX_PKT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_OPEN_INDEX(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_OPEN_INDEX, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_CLOSE_INDEX(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_CLOSE_INDEX, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_INDEX_INFO(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_INDEX_INFO, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		payload,
		len
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	bdaddr[6];
	uint16_t	manufacturer;

	decoder.copyOut(bdaddr,6);
	decoder.decode(manufacturer);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	Oscl::Error::Info::log(
		"\taddress: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X\n"
		"\tmanufacturer: \"0x%4.4X\"\n"
		"\n",
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0],
		manufacturer
		);
	}

static void	parseHCI_MON_VENDOR_DIAG(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_VENDOR_DIAG, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_SYSTEM_NOTE(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	const char*	note	= (const char*)payload;

	Oscl::Error::Info::log(
		"%s: HCI_MON_SYSTEM_NOTE, "
		"index %s, "
		"len %u"
		"note: \"%s\""
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len,
		note
		);
	}

static void	parseHCI_MON_USER_LOGGING(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_USER_LOGGING, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_CTRL_OPEN(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_CTRL_OPEN, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_CTRL_CLOSE(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_CTRL_CLOSE, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_CTRL_COMMAND(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_CTRL_COMMAND, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

static void	parseHCI_MON_CTRL_EVENT(
				uint16_t	index,
				uint16_t	len,
				const char*	devIndexName,
				const void*	payload
				) noexcept{
	Oscl::Error::Info::log(
		"%s: HCI_MON_CTRL_EVENT, "
		"index %s, "
		"len %u"
		"\n",
		OSCL_PRETTY_FUNCTION_DETAIL,
		devIndexName,
		len
		);

	Oscl::Error::Info::hexDump(
		payload,
		len
		);
	}

void	Oscl::Bluetooth::Debug::Print::parseMonitorPacket(
			const void*	packet,
			unsigned	length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		packet,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	opcode;
	uint16_t	index; // #define HCI_DEV_NONE    0xffff
	uint16_t	len;

	decoder.decode(opcode);
	decoder.decode(index);
	decoder.decode(len);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION_DETAIL
			);
		return;
		}

	const uint8_t*	p	= (const uint8_t*)packet;

	unsigned
	offset	= length - decoder.remaining();

	const void*	payload	= &p[offset];

	char	devIndexString[32];

	snprintf(
		devIndexString,
		sizeof(devIndexString)-1,
		"hci%u",
		index
		);

	devIndexString[sizeof(devIndexString)-1]	= '\0';

	const char*	devIndexName	= (index == 0xffff)?"HCI_DEV_NONE":devIndexString;

	switch(opcode){
		case 0x0000: // HCI_MON_NEW_INDEX
			parseHCI_MON_NEW_INDEX(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0001: // HCI_MON_DEL_INDEX
			parseHCI_MON_DEL_INDEX(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0002: // HCI_MON_COMMAND_PKT
			parseHCI_MON_COMMAND_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0003: // HCI_MON_EVENT_PKT
			parseHCI_MON_EVENT_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0004: // HCI_MON_ACL_TX_PKT
			parseHCI_MON_ACL_TX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0005: // HCI_MON_ACL_RX_PKT
			parseHCI_MON_ACL_RX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0006: // HCI_MON_SCO_TX_PKT
			parseHCI_MON_SCO_TX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0007: // HCI_MON_SCO_RX_PKT
			parseHCI_MON_SCO_RX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0008: // HCI_MON_OPEN_INDEX
			parseHCI_MON_OPEN_INDEX(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0009: // HCI_MON_CLOSE_INDEX
			parseHCI_MON_CLOSE_INDEX(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000A: // HCI_MON_INDEX_INFO
			parseHCI_MON_INDEX_INFO(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000B: // HCI_MON_VENDOR_DIAG
			parseHCI_MON_VENDOR_DIAG(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000C: // HCI_MON_SYSTEM_NOTE
			parseHCI_MON_SYSTEM_NOTE(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000D: // HCI_MON_USER_LOGGING
			parseHCI_MON_USER_LOGGING(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000E: // HCI_MON_CTRL_OPEN
			parseHCI_MON_CTRL_OPEN(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x000F: // HCI_MON_CTRL_CLOSE
			parseHCI_MON_CTRL_CLOSE(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0010: // HCI_MON_CTRL_COMMAND
			parseHCI_MON_CTRL_COMMAND(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x0011: // HCI_MON_CTRL_EVENT
			parseHCI_MON_CTRL_EVENT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected opcode: %u(0x%4.4X)\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				opcode,
				opcode
				);
			Oscl::Error::Info::hexDump(
				payload,
				len
				);

			return;
		}
	}

void	Oscl::Bluetooth::Debug::Print::parseHciTxPacket(
			const void*	packet,
			unsigned	length
			) noexcept{

	static const uint16_t	index	= 0;
	uint16_t				len		= length;

	char	devIndexString[32];

	snprintf(
		devIndexString,
		sizeof(devIndexString)-1,
		"hci%u",
		index
		);

	devIndexString[sizeof(devIndexString)-1]	= '\0';

	const char*	devIndexName	= (index == 0xffff)?"HCI_DEV_NONE":devIndexString;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		packet,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	type;
	decoder.decode(type);

	const uint8_t*	payload	= (const uint8_t*)packet;

	++payload;

	--len;

	switch(type){
		case 0x01: // HCI_COMMAND_PKT
			parseHCI_MON_COMMAND_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x04: // HCI_EVENT_PKT
			parseHCI_MON_EVENT_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x02: // HCI_ACLDATA_PKT
			parseHCI_MON_ACL_TX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0x03: // HCI_SCODATA_PKT
			parseHCI_MON_SCO_RX_PKT(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		case 0xF0: // HCI_DIAG_PKT
			parseHCI_MON_VENDOR_DIAG(
				index,
				len,
				devIndexName,
				payload
				);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected type: %u(0x%2.2X)\n",
				OSCL_PRETTY_FUNCTION_DETAIL,
				type,
				type
				);
			Oscl::Error::Info::hexDump(
				payload,
				len
				);

			return;
		}
	}

void	Oscl::Bluetooth::Debug::Print::parseMeshProvisioningPDU(
			const void*	packet,
			unsigned	length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		packet,
		length
		);

	parseMeshProvisioning(
		beDecoder,
		length
		);
	}

void	Oscl::Bluetooth::Debug::Print::parseLeAdvertisingData(
			const void*	packet,
			unsigned	length
			) noexcept{
	Oscl::Error::Info::log(
		"\tLE ADVERTISING DATA\n"
		);

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		packet,
		length
		);

	parseEirFields(
		leDecoder,
		length
		);
	}

