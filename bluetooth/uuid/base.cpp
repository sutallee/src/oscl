/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"
#include <string.h>

void	Oscl::BT::UUID::Base::make128bitUUID(
			uint8_t		outputUUID[16],
			uint32_t	uuid
			) noexcept{
	static const uint8_t	bluetoothBaseUUID[] = {
		0x00,0x00,0x00,0x00,
		0x00,0x00,
		0x10,0x00,
		0x80,0x00,
		0x00,0x80,0x5F,0x9B,0x34,0xFB
		};

	memcpy(
		&outputUUID[4],
		&bluetoothBaseUUID[4],
		sizeof(bluetoothBaseUUID)-4
		);

	outputUUID[0]	= (uuid >> 24);
	outputUUID[1]	= (uuid >> 16);
	outputUUID[2]	= (uuid >> 8);
	outputUUID[3]	= (uuid >> 0);
	}

