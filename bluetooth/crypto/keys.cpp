/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <stdint.h>
#include "keys.h"
#include "salt.h"
#include "oscl/aes128/cmac.h"
#include "oscl/error/info.h"

void	Oscl::BT::Crypto::Keys::k1(
			const void*		n,
			unsigned		nLen,
			const void*		salt,
			const void*		p,
			unsigned		pLen,
			void*			out
			) noexcept{

	const uint8_t*	N		= (const uint8_t*)n;
	const uint8_t*	P		= (const uint8_t*)p;
	uint8_t*		output	= (uint8_t*)out;

	Oscl::AES128::CMAC	cmac(salt);

	cmac.accumulate(N,nLen);

	cmac.finalize();

	cmac.copyOut(output,16);

	Oscl::AES128::CMAC	cmac2(output);

	cmac2.accumulate(P,pLen);

	cmac2.finalize();

	cmac2.copyOut(output,16);
	}

void	Oscl::BT::Crypto::Keys::k2(
			const void*		n,
			const void*		p,
			unsigned		pLen,
			void*			out
			) noexcept{

	const uint8_t*	N	= (const uint8_t*)n;
	const uint8_t*	P	= (const uint8_t*)p;
	uint8_t*		output	= (uint8_t*)out;

	uint8_t	saltString[4] = {'s','m','k','2'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		saltString,
		sizeof(saltString),
		salt,
		sizeof(salt)
		);

	////
	uint8_t	T[16];

	Oscl::AES128::CMAC	cmacSALT(salt);

	cmacSALT.accumulate(N,16);

	cmacSALT.finalize();

	cmacSALT.copyOut(T,sizeof(T));

	////
	uint8_t*	T1	= &output[0];

	Oscl::AES128::CMAC	cmacT1(T);

	cmacT1.accumulate(P,pLen);

	static const uint8_t	one	= 0x01;

	cmacT1.accumulate(&one,sizeof(one));

	cmacT1.finalize();

	cmacT1.copyOut(T1,16);

	////
	uint8_t*	T2	= &output[16];

	Oscl::AES128::CMAC	cmacT2(T);

	cmacT2.accumulate(T1,16);

	cmacT2.accumulate(P,pLen);

	static const uint8_t	two	= 0x02;

	cmacT2.accumulate(&two,sizeof(two));

	cmacT2.finalize();

	cmacT2.copyOut(T2,16);

	////
	uint8_t*	T3	= &output[32];

	Oscl::AES128::CMAC	cmacT3(T);

	cmacT3.accumulate(T2,16);

	cmacT3.accumulate(P,pLen);

	static const uint8_t	three	= 0x03;

	cmacT3.accumulate(&three,sizeof(three));

	cmacT3.finalize();

	cmacT3.copyOut(T3,16);
	}

void	Oscl::BT::Crypto::Keys::k3(
			const void*	n,
			void*		output
			) noexcept{
	const uint8_t*	N	= (const uint8_t*)n;

	static const uint8_t	saltString[4] = {'s','m','k','3'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		saltString,
		sizeof(saltString),
		salt,
		sizeof(salt)
		);

	////
	uint8_t	T[16];

	Oscl::AES128::CMAC	cmacSALT(salt);

	cmacSALT.accumulate(N,16);

	cmacSALT.finalize();

	cmacSALT.copyOut(T,sizeof(T));

	////
	static const uint8_t	id64String[4] = {'i','d','6','4'};

	Oscl::AES128::CMAC	cmacT(T);

	cmacT.accumulate(id64String,sizeof(id64String));

	static const uint8_t	one	= 0x01;

	cmacT.accumulate(&one,sizeof(one));

	cmacT.finalize();

	cmacT.copyOut(output,8,8);
	}

unsigned char	Oscl::BT::Crypto::Keys::k4(const void* N) noexcept{
	static const uint8_t	saltString[4] = {'s','m','k','4'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		saltString,
		sizeof(saltString),
		salt,
		sizeof(salt)
		);

	////
	uint8_t	T[16];

	Oscl::AES128::CMAC	cmacSALT(salt);

	cmacSALT.accumulate(N,16);

	cmacSALT.finalize();

	cmacSALT.copyOut(T,sizeof(T));

	////
	static const uint8_t	id64String[4] = {'i','d','6',0x01};

	Oscl::AES128::CMAC	cmacT(T);

	cmacT.accumulate(id64String,sizeof(id64String));

	cmacT.finalize();

	uint8_t	out;

	cmacT.copyOut(&out,1,15);

	out	&=	0x3F;

	return out;
	}
