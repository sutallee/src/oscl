/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_crypto_salth_
#define _oscl_bluetooth_crypto_salth_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Crypto {
/** */
namespace Salt {

/**	This function uses AES-128 CBC-MAC as a hash
	function. 
	@param [in] input		pointer to input data
	@param [in] inputLen	length of input data
	@param [out] output		pointer output buffer
	@param [in] outputLen	maximum length of output buffer.

	The outputLen is used to prevent buffer overflow of
	the output buffer. The output of the CBC-MAC will never
	be more than 16 octets and, therefore, an outputLen larger
	than 16 is wasting space.
 */
void	s1(
			const void*		input,
			unsigned		inputLen,
			void*			output,
			unsigned		outputLen	= 16
			) noexcept;

}
}
}
}

#endif
