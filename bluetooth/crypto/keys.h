/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_crypto_keysh_
#define _oscl_bluetooth_crypto_keysh_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Crypto {
/** */
namespace Keys {

/**	This function uses AES-128 CBC-MAC as a hash
	function. 
	@param [in] input		pointer to input data
	@param [in] inputLen	length of input data
	@param [out] output		pointer output buffer
	@param [in] outputLen	maximum length of output buffer.

	The outputLen is used to prevent buffer overflow of
	the output buffer. The output of the CBC-MAC will never
	be more than 16 octets and, therefore, an outputLen larger
	than 16 is wasting space.
 */

/**	The network key material derivation function k1
	is used to generate instances of IdentityKey and BeaconKey.
	@param [in] N		    pointer to K data
	@param [in] nLen		number of octets at K
	@param [in] salt		pointer 16 octets of salt
	@param [in] P			pointer to P data
	@param [in] pLen		number of octets at P
	@param [out] output		pointer to 16 octet output/result buffer
 */
void	k1(
			const void*		N,
			unsigned		nLen,
			const void*		salt,
			const void*		P,
			unsigned		pLen,
			void*			output
			) noexcept;

/**	The network key material derivation function k2
	is used to generate instances of EncryptionKey,
	PrivacyKey, and NID for use as Master and
	Private Low Power node communication.
	@param [in] N		    pointer to 16 octet K data
	@param [in] P			pointer to P data
	@param [in] pLen		number of octets at P
	@param [out] output		pointer to 48 octet output/result buffer
 */
void	k2(
			const void*		N,
			const void*		P,
			unsigned		pLen,
			void*			output
			) noexcept;

/**	The derivation function k3 is used to generate a public
	value of 64 bits derived from a private key.
	@param [in] N		    pointer to 16 octet K data
	@param [out] output		pointer to 8 octet output/result buffer
 */
void	k3(
			const void*	N,
			void*		output
			) noexcept;

/**	The derivation function k4 is used to generate a public
	value of 6 bits derived from a private key.
	@param [in] N		    pointer to 16 octet K data
	RETURN: 6-bit result in LSb of octet
 */
unsigned char	k4(const void* N) noexcept;
}
}
}
}

#endif
