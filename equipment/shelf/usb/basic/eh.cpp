/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "eh.h"

using namespace Oscl::Equip::Shelf::USB::Basic;

EH::EH(
			// Dyn::Part
			Oscl::Mt::Itc::PostMsgApi&			ehPapi,
			Oscl::Mt::Itc::Dyn::Adv::
			Core<	Oscl::Equip::
					Holder::Dyn::Part
					>::ReleaseApi&				ehAdvRapi,
			Oscl::Mt::Itc::PostMsgApi&			ehAdvPapi,
			Oscl::Mt::Itc::PostMsgApi&			ehDynPapi,
			Oscl::Equip::Holder::Model::
			Part::RecognitionApi&				recognitionApi,
			bool								enabledByDefault,
			// This is the location of the new equipment holder
			const Oscl::ObjectID::RO::Api&		ehLocation,
			// Match
			// This is the location of the USB port corresponding
			// to this equipment holder.
			// This *may* be the same as the "location" above.
			// However, it may be a good implementation choice
			// to keep them independent and let the application
			// decide.
			const Oscl::ObjectID::RO::Api&		matchOID,
			// Presence
			Oscl::Mt::Itc::Dyn::
			Adv::Cli::Req::Api< Oscl::Usb::
								DynDevice
								>::SAP&			usbAdvSAP,
			Oscl::Mt::Itc::Dyn::Adv::
			Cli::SyncApi<Oscl::Usb::DynDevice>&	usbFindApi,
			Oscl::Mt::Itc::PostMsgApi&			presencePapi
			) noexcept:
		_matchOID(),
		_eh(	ehPapi,	// _equipHolderServer
				ehAdvRapi,	// _ehDynAdv
				ehAdvPapi,	// _ehAdvServer
				ehDynPapi,	// _ehDynServer
				recognitionApi,
				enabledByDefault,	// true
				&ehLocation
				),
		_match(_matchOID),
		_presence(	_eh._model.presenceApi(),
					usbAdvSAP,	// _dynDeviceAdv.getCliSAP()
					usbFindApi,	// _dynDeviceAdv
					presencePapi,	// _equipHolderPresenceServer
					_match
					)
		{
	static_cast<Oscl::ObjectID::Api&>(_matchOID)	= matchOID;
	}

void	EH::start() noexcept{
	_eh.syncOpen();
	_presence.syncOpen();
	}

void	EH::stop() noexcept{
	_presence.syncClose();
	_eh.deactivate();
	_eh.syncClose();
	}

