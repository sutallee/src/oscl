/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_shelf_usb_basic_eh_
#define _oscl_equipment_shelf_usb_basic_eh_
#include "oscl/equipment/holder/dyn/part.h"
#include "oscl/equipment/holder/oid/usb/match.h"
#include "oscl/equipment/holder/oid/dyn/part.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Shelf {
/** */
namespace USB {
/** */
namespace Basic {

/** This unit contains all of the items necessary
	for the implementation of each "slot" in the
	shelf. Each slot consists of an equipment holder
	and the associated presence controller.
	A shelf with 4 slots would contain 4 instances
	of this unit.
 */
struct EH : public Oscl::QueueItem {
	public:
		/** */
		enum{maxOidSize=32};
		/** */
		Oscl::ObjectID::Fixed<maxOidSize>		_matchOID;
		/** */
		Oscl::Equip::Holder::Dyn::Part			_eh;
		/** */
		Oscl::Equip::Holder::OID::USB::Match	_match;
		/** */
		Oscl::Equip::Holder::
		OID::Dyn::Part<	Oscl::Usb::
						DynDevice
						>						_presence;
	public:
		/** */
		EH(
			// Dyn::Part
			Oscl::Mt::Itc::PostMsgApi&			ehPapi,
			Oscl::Mt::Itc::Dyn::Adv::
			Core<	Oscl::Equip::
					Holder::Dyn::Part
					>::ReleaseApi&				ehAdvRapi,
			Oscl::Mt::Itc::PostMsgApi&			ehDdvPapi,
			Oscl::Mt::Itc::PostMsgApi&			ehDynPapi,
			Oscl::Equip::Holder::Model::
			Part::RecognitionApi&				recognitionApi,
			bool								enabledByDefault,
			// This is the location of the new equipment holder
			// This is copied locally during construction.
			const Oscl::ObjectID::RO::Api&		ehLocation,
			// Match
			// This is the location of the USB port corresponding
			// to this equipment holder.
			// This *may* be the same as the "location" above.
			// However, it may be a good implementation choice
			// to keep them independent and let the application
			// decide.
			const Oscl::ObjectID::RO::Api&		matchOID,
			// Presence
			Oscl::Mt::Itc::Dyn::
			Adv::Cli::Req::Api< Oscl::Usb::
								DynDevice
								>::SAP&			usbAdvSAP,
			Oscl::Mt::Itc::Dyn::Adv::
			Cli::SyncApi<Oscl::Usb::DynDevice>&	usbFindApi,
			Oscl::Mt::Itc::PostMsgApi&			presencePapi
			) noexcept;

		/** */
		void	start() noexcept;
		/** */
		void	stop() noexcept;
	};

}
}
}
}
}

#endif
