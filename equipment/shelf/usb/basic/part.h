/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_shelf_usb_basic_parth_
#define _oscl_equipment_shelf_usb_basic_parth_
#include "eh.h"
#include "oscl/equipment/cp/usb/hub/part.h"
#include "oscl/equipment/holder/dyn/part.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monvar.h"
#include "oscl/equipment/holder/oid/eh/match.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Shelf {
/** */
namespace USB {
/** */
namespace Basic {

/** This object implements a USB Hub as a Shelf.
	The implementation waits for an equipment holder
	with a particular location (its container) to
	become available, establishes a multi-port
	USB hub, and advertises an equipment holder
	for each slot in the shelf.
 */
class Part :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api
				{
	public:
		/** */
		struct EhMem {
			/** */
			Oscl::Memory::AlignedBlock<	sizeof(EH) >	eh;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Equip::CP::USB::HUB::Part::CancelMem	hubPart;
			};
	private:
		/** */
		CancelMem								_cancelMem;

		/** */
		Oscl::Equip::Holder::OID::EH::Match		_match;

		/** */
		Oscl::Mt::Itc::Dyn::
		Unit::MonitorVar<	Oscl::Equip::
							Holder::Dyn::Part,
							Part
							>					_monitor;

		/** */
		Oscl::Done::Operation<Part>				_hubCancelDoneOp;


		/** The USB hub.
		 */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Equip::
								CP::USB::
								HUB::Part
								)
						>						_hubCpMem;

		/** The USB hub.
		 */
		Oscl::Equip::CP::USB::HUB::Part*		_hubCP;

		/** */
		Oscl::Queue<EH>							_slots;

		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<	Oscl::Equip::
							Holder::Dyn::
							Part
							>&					_ehAdvCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		/** */
		SyncApi<Oscl::Usb::DynDevice>&			_usbFindApi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		/** */
		Req::Api<Oscl::Usb::DynDevice>::SAP&	_usbAdvSAP;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		/** */
		Creator::SyncApi<Oscl::Usb::DynDevice>&	_usbAdvCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		/** */
		Core<Oscl::Usb::DynDevice>::ReleaseApi&	_usbAdvRapi;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	_delayServiceSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_monPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_usbHubPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_usbDynPapi;
		/** */
		Oscl::Usb::Setup::Mem&					_setupMem;
		/** */
		Oscl::Usb::Hub::Creator::Part::
		/** */
		PacketMem&								_packetMem;
		/** */
		Oscl::Usb::Hub::Adaptor::
		/** */
		Driver::SetupMem&						_adaptorSetupMem;
		/** */
		Oscl::Usb::Hub::Adaptor::
		/** */
		Driver::PacketMem&						_adaptorPacketMem;
		/** */
		Oscl::Usb::Hub::Creator::Part::
		/** */
		PortDriverMem*	const					_portDriverMem;
		/** */
		Oscl::Usb::Hub::Creator::Part::
		/** */
		PortAdaptorMem*	const					_portAdaptorMem;
		/** */
		Oscl::Usb::Hub::Port::Adaptor::
		/** */
		Port::SetupMem*	const					_portSetupMem;
		/** */
		Oscl::Usb::Hub::Port::Adaptor::
		/** */
		Port::PacketMem*	const				_portPacketMem;
		/** */
		Oscl::Usb::Hub::Port::
		Driver::SetupPktMem*	const			_portDriverSetupMem;
		/** */
		Oscl::Usb::Hub::Port::
		Driver::PacketMem*	const				_portDriverPacketMem;
		/** */
		unsigned								_maxPorts;
		/** */
		unsigned								_oidType;
		/** FIXME: should this be a copy? */
		const Oscl::ObjectID::RO::Api&			_cpUsbHubLocation;

		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*				_openReq;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;



	public:
		/** */
		Part(
				Oscl::Mt::Itc::PostMsgApi&				equipmentPapi,
				Oscl::Mt::Itc::PostMsgApi&				ehPapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Equip::
							Holder::Dyn::Part
							>&		ehFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Equip::
							Holder::Dyn::Part
							>::SAP&						ehAdvSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<	Oscl::Equip::
									Holder::Dyn::
									Part
									>&					ehAdvCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<	Oscl::Equip::
						Holder::Dyn::Part
						>::ReleaseApi&					ehAdvRapi,
				Oscl::Mt::Itc::PostMsgApi&				ehAdvPapi,
				Oscl::Mt::Itc::PostMsgApi&				ehDynPapi,
				Oscl::Equip::Holder::Model::
				Part::RecognitionApi&					recognitionApi,
				bool									enabledByDefault,
				Oscl::Mt::Itc::PostMsgApi&				presencePapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<Oscl::Usb::DynDevice>&			usbFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Usb::DynDevice
							>::SAP&						usbAdvSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<Oscl::Usb::DynDevice>&	usbAdvCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Oscl::Usb::DynDevice>::ReleaseApi&	usbAdvRapi,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
				Oscl::Mt::Itc::PostMsgApi&				monPapi,
				Oscl::Mt::Itc::PostMsgApi&				usbHubPapi,
				Oscl::Mt::Itc::PostMsgApi&				usbDynPapi,
				Oscl::Usb::Setup::Mem&					setupMem,
				Oscl::Usb::Hub::Creator::Part::
				PacketMem&								packetMem,
				Oscl::Usb::Hub::Adaptor::
				Driver::SetupMem&						adaptorSetupMem,
				Oscl::Usb::Hub::Adaptor::
				Driver::PacketMem&						adaptorPacketMem,
				Oscl::Usb::Hub::Creator::Part::
				PortDriverMem*							portDriverMem,
				Oscl::Usb::Hub::Creator::Part::
				PortAdaptorMem*							portAdaptorMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::SetupMem*							portSetupMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::PacketMem*						portPacketMem,
				Oscl::Usb::Hub::Port::
				Driver::SetupPktMem*					portDriverSetupMem,
				Oscl::Usb::Hub::Port::
				Driver::PacketMem*						portDriverPacketMem,
				unsigned								maxPorts,
				unsigned								oidType,
				const Oscl::ObjectID::RO::Api&			cpUsbHubocation,
				EhMem									ehMem[],
				unsigned								nEquipmentHolders
				) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	private:
		/** */
		void	hubCancelDone() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	};

}
}
}
}
}

#endif
