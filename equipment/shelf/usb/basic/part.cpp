/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Equip::Shelf::USB::Basic;

Part::Part(
				Oscl::Mt::Itc::PostMsgApi&			equipmentPapi,
				Oscl::Mt::Itc::PostMsgApi&			ehPapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Equip::
							Holder::Dyn::Part
							>&						ehFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Equip::
							Holder::Dyn::Part
							>::SAP&					ehAdvSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<	Oscl::Equip::
									Holder::Dyn::
									Part
									>&				ehAdvCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<	Oscl::Equip::
						Holder::Dyn::Part
						>::ReleaseApi&				ehAdvRapi,
				Oscl::Mt::Itc::PostMsgApi&			ehAdvPapi,
				Oscl::Mt::Itc::PostMsgApi&			ehDynPapi,
				Oscl::Equip::Holder::Model::
				Part::RecognitionApi&				recognitionApi,
				bool								enabledByDefault,
				Oscl::Mt::Itc::PostMsgApi&			presencePapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<Oscl::Usb::DynDevice>&			usbFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Usb::DynDevice
							>::SAP&						usbAdvSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<Oscl::Usb::DynDevice>&	usbAdvCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Oscl::Usb::DynDevice>::ReleaseApi&	usbAdvRapi,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
				Oscl::Mt::Itc::PostMsgApi&				monPapi,	// _usbDevMonServer
				Oscl::Mt::Itc::PostMsgApi&				usbHubPapi,	// _usbHubServer
				Oscl::Mt::Itc::PostMsgApi&				usbDynPapi,	// _usbDynDeviceServer
				Oscl::Usb::Setup::Mem&					setupMem,
				Oscl::Usb::Hub::Creator::Part::
				PacketMem&								packetMem,
				Oscl::Usb::Hub::Adaptor::
				Driver::SetupMem&						adaptorSetupMem,
				Oscl::Usb::Hub::Adaptor::
				Driver::PacketMem&						adaptorPacketMem,
				Oscl::Usb::Hub::Creator::Part::
				PortDriverMem*							portDriverMem,
				Oscl::Usb::Hub::Creator::Part::
				PortAdaptorMem*							portAdaptorMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::SetupMem*							portSetupMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::PacketMem*						portPacketMem,
				Oscl::Usb::Hub::Port::
				Driver::SetupPktMem*					portDriverSetupMem,
				Oscl::Usb::Hub::Port::
				Driver::PacketMem*						portDriverPacketMem,
				unsigned								maxPorts,
				unsigned								oidType,
				const Oscl::ObjectID::RO::Api&			cpUsbHubLocation,
				EhMem									ehMem[],
				unsigned								nEquipmentHolders
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,equipmentPapi),
		_match(cpUsbHubLocation),
		_monitor(
					ehAdvSAP,
					ehFindApi,
					monPapi,
					_match,
					*this,
					&Part::activate,
					&Part::deactivate
					),
		_hubCancelDoneOp(*this,&Part::hubCancelDone),
		_hubCP(0),
		_slots(),
		_ehAdvCapi(ehAdvCapi),
		_usbFindApi(usbFindApi),
		_usbAdvSAP(usbAdvSAP),
		_usbAdvCapi(usbAdvCapi),
		_usbAdvRapi(usbAdvRapi),
		_delayServiceSAP(delayServiceSAP),
		_monPapi(monPapi),	
		_usbHubPapi(usbHubPapi),
		_usbDynPapi(usbDynPapi),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_adaptorSetupMem(adaptorSetupMem),
		_adaptorPacketMem(adaptorPacketMem),
		_portDriverMem(portDriverMem),
		_portAdaptorMem(portAdaptorMem),
		_portSetupMem(portSetupMem),
		_portPacketMem(portPacketMem),
		_portDriverSetupMem(portDriverSetupMem),
		_portDriverPacketMem(portDriverPacketMem),
		_maxPorts(maxPorts),
		_oidType(oidType),
		_cpUsbHubLocation(cpUsbHubLocation),
		_openReq(0),
		_closeReq(0)
		{
	for(unsigned i=0;i<nEquipmentHolders;++i){
		Oscl::ObjectID::Fixed<32>	oid;
		static_cast<Oscl::ObjectID::Api&>(oid)	= cpUsbHubLocation;
		oid	+= oidType;
		oid	+= i;
		EH*	eh	= new(&ehMem[i])
					EH(	ehPapi,
						ehAdvRapi,
						ehAdvPapi,
						ehDynPapi,
						recognitionApi,
						enabledByDefault,
						oid,
						oid,
						usbAdvSAP,
						usbFindApi,
						presencePapi
						);
		_slots.put(eh);
		}
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	for(EH* next=_slots.first();next;next=_slots.next(next)){
		next->start();
		_ehAdvCapi.add(next->_eh);
		}
	_openReq	= &msg;
	_monitor.syncOpen();
	msg.returnToSender();
	_openReq	= 0;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	_monitor.syncClose();
	for(EH* next=_slots.first();next;next=_slots.next(next)){
		next->stop();
		}
	msg.returnToSender();
	_closeReq	= 0;
	}


void	Part::hubCancelDone() noexcept{
	_monitor.deactivateDone();
	}

void	Part::activate() noexcept{
	Oscl::Equip::Holder::Dyn::Part&	dynSrv	= _monitor._handle->getDynSrv();
	_hubCP	= new(&_hubCpMem)
				Oscl::Equip::CP::USB::HUB::
				Part(	dynSrv._model.viewSAP(),
						dynSrv._model.criteriaApi(),
						dynSrv._model.provApi(),
						_usbFindApi,
						_usbAdvSAP,
						_usbAdvCapi,
						_usbAdvRapi,
						_delayServiceSAP,
						_monPapi,	// _usbDevMonServer
						_usbHubPapi,	// _usbHubServer
						_usbDynPapi,	// _usbDynDeviceServer
						_setupMem,
						_packetMem,
						_adaptorSetupMem,
						_adaptorPacketMem,
						_portDriverMem,
						_portAdaptorMem,
						_portSetupMem,
						_portPacketMem,
						_portDriverSetupMem,
						_portDriverPacketMem,
						_maxPorts,
						_oidType,
						&_cpUsbHubLocation
						);
	_hubCP->start();
	}

void	Part::deactivate() noexcept{
	_hubCP->stop(_cancelMem.hubPart,_hubCancelDoneOp);
	}

