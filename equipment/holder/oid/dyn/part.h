/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_oid_dyn_parth_
#define _oscl_equipment_holder_oid_dyn_parth_
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/equipment/holder/presence/api.h"
#include "oscl/mt/itc/dyn/adv/cli.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace OID {
/** */
namespace Dyn {

/** */
template <class dynType>
class Part :	public	Oscl::Mt::Itc::Dyn::Unit::
						Monitor<dynType>
				{
	private:
		/** */
		class Iterator :	public	Oscl::Mt::Itc::Dyn::Adv::
									Iterator<dynType>
							{
			public:
				/** */
				const Oscl::ObjectID::RO::Api&	_matchOID;
			public:
				/** */
				Iterator(const Oscl::ObjectID::RO::Api& matchOID) noexcept;
			private:
				/** */
				bool	next(dynType& dynSrv) noexcept;
			};
	private:
		/** */
		Oscl::Equip::Holder::Presence::Api&		_ehApi;

		/** */
		Oscl::Mt::Itc::Dyn::
		Adv::Iterator<dynType>&					_iterator;

	public:
		/** */
		Part(	Oscl::Equip::Holder::Presence::Api&	ehApi,
				typename Oscl::Mt::Itc::Dyn::
				Adv::Cli::Req::Api< dynType >::SAP&	advSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Cli::SyncApi<dynType>&				findApi,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::Dyn::
				Adv::Iterator<dynType>&				iterator
				) noexcept;

	private:	//Oscl::Mt::Itc::Dyn::Unit::Monitor
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	};

template <class dynType>
Part<dynType>::Iterator::Iterator(const Oscl::ObjectID::RO::Api& matchOID) noexcept:
		_matchOID(matchOID)
		{
	}

template <class dynType>
bool	Part<dynType>::Iterator::next(dynType& dynSrv) noexcept{
	const Oscl::ObjectID::RO::Api&	location	= dynSrv._location;
	if(location == _matchOID){
		return true;
		}
	return false;
	}

template <class dynType>
Part<dynType>::Part(	Oscl::Equip::Holder::Presence::Api&	ehApi,
						typename Oscl::Mt::Itc::Dyn::
						Adv::Cli::Req::Api< dynType >::SAP&	advSAP,
						Oscl::Mt::Itc::Dyn::Adv::
						Cli::SyncApi<dynType>&				findApi,
						Oscl::Mt::Itc::PostMsgApi&			myPapi,
						Oscl::Mt::Itc::Dyn::
						Adv::Iterator<dynType>&				iterator
						) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::
		Monitor<dynType>(	advSAP,
							findApi,
							myPapi,
							iterator
							),
		_ehApi(ehApi),
		_iterator(iterator)
		{
	}

template <class dynType>
void	Part<dynType>::activate() noexcept{
	_ehApi.insert();
	}

template <class dynType>
void	Part<dynType>::deactivate() noexcept{
	_ehApi.remove();
	this->deactivateDone();
	}

}
}
}
}
}

#endif
