/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_model_parth_
#define _oscl_equipment_holder_model_parth_
#include "oscl/equipment/holder/fsm/sv.h"
#include "oscl/equipment/holder/provision/itc/sync.h"
#include "oscl/equipment/holder/presence/itc/sync.h"
#include "oscl/equipment/holder/enable/itc/sync.h"
#include "oscl/equipment/holder/view/itc/reqapi.h"
#include "oscl/equipment/holder/criteria/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace Model {

/** This class provides the implementation of the model
	(as in model-view-controller) aspect of an equipment
	holder. The model has three controller interfaces
	and one view interface.
	The view interface is used by clients to receive
	equipment holder state change notifications.
	The three controller interfaces are the presence
	interface, the provisioning interface, and the enable
	interface.
	The presence interface is invoked by the client that
	determines whether or not a replaceable unit is
	present in the slot.
	The provisioning interface is invoked by the client
	that is responsible for provisioning the replaceable
	unit after it has been inserted into the equipment
	holder. This client typically listens to for equipment
	holder state changes, and begins provisioning the
	replaceable unit when the state changes to NEW.
	The enable interface provides a means for a system
	client to place the equipment holder in a state where
	it is NOT expecting any replaceable unit to be present.
	This enables the equipment holder to present the
	INVALID and MISSING states.
 */
class Part :	private Oscl::Equip::Holder::Provision::ITC::Req::Api,
				private Oscl::Equip::Holder::Presence::ITC::Req::Api,
				private Oscl::Equip::Holder::Enable::ITC::Req::Api,
				private Oscl::Equip::Holder::Criteria::ITC::Req::Api,
				private Oscl::Equip::Holder::View::ITC::Req::Api,
				private Oscl::Equip::Holder::FSM::StateVar::ContextApi
				{
	public:
		/** */
		class RecognitionApi {
			public:
				/** */
				virtual ~RecognitionApi() {}
				/** This operation must return TRUE if the
					circuit-pack in this slot is of a known
					type. Since obtaining the type is bus
					specific and the list of "known/recognized"
					types is system specific, the context
					must provide this information. Further,
					since the list of known types is static
					there is no need to make this run-time
					configurable.
				 */
				virtual bool	isRecognized() noexcept=0;
			};
	private:
		/** */
		Oscl::Queue<	Oscl::Equip::Holder::
						Criteria::Acceptability
						>							_acceptabilityStack;
		/** */
		Oscl::Queue<	Oscl::Equip::Holder::View::
						ITC::Req::Api::ChangeReq
						>							_observers;
		/** */
		RecognitionApi&								_recognitionApi;
		/** */
		Oscl::Equip::Holder::Provision::ITC::Sync	_provisionSync;
		/** */
		Oscl::Equip::Holder::Presence::ITC::Sync	_presenceSync;
		/** */
		Oscl::Equip::Holder::Enable::ITC::Sync		_enableSync;
		/** */
		Oscl::Equip::Holder::Criteria::ITC::Sync	_criteriaSync;
		/** */
		Oscl::Equip::Holder::View::
		ITC::Req::Api::ConcreteSAP					_viewSAP;
		/** */
		Oscl::Equip::Holder::FSM::StateVar			_state;
		/** */
		const Oscl::Equip::Holder::FSM::StateVar::State*	_prevState;
		/** */
		unsigned									_disableCount;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				RecognitionApi&				recognitionApi,
				bool						enabledByDefault
				) noexcept;

		/** */
		Oscl::Equip::Holder::
		Provision::ITC::Req::Api::SAP&	provSAP() noexcept;
		/** */
		Oscl::Equip::Holder::
		Presence::ITC::Req::Api::SAP&	presenceSAP() noexcept;
		/** */
		Oscl::Equip::Holder::
		Enable::ITC::Req::Api::SAP&		enableSAP() noexcept;
		/** */
		Oscl::Equip::Holder::
		Criteria::ITC::Req::Api::SAP&	criteriaSAP() noexcept;
		/** */
		Oscl::Equip::Holder::
		View::ITC::Req::Api::SAP&		viewSAP() noexcept;

		/** */
		Oscl::Equip::Holder::Provision::Api&		provApi() noexcept;
		/** */
		Oscl::Equip::Holder::Presence::Api&			presenceApi() noexcept;
		/** */
		Oscl::Equip::Holder::Enable::Api&			enableApi() noexcept;
		/** */
		Oscl::Equip::Holder::Criteria::Api&			criteriaApi() noexcept;

	private:
		/** */
		void	notify() noexcept;


	private: // Oscl::Equip::Holder::Provision::ITC::Req::Api
		/** */
		void	request(	Oscl::Equip::Holder::
							Provision::ITC::Req::Api::ProvisioningReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							Provision::ITC::Req::Api::ProvisionedReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							Provision::ITC::Req::Api::FailedReq&	msg
							) noexcept;
	private: // Oscl::Equip::Holder::Presence::ITC::Req::Api
		/** */
		void	request(	Oscl::Equip::Holder::
							Presence::ITC::Req::Api::InsertReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							Presence::ITC::Req::Api::RemoveReq&	msg
							) noexcept;
	private: // Oscl::Equip::Holder::Enable::ITC::Req::Api
		/** */
		void	request(	Oscl::Equip::Holder::
							Enable::ITC::Req::Api::EnableReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							Enable::ITC::Req::Api::DisableReq&	msg
							) noexcept;
	private: // Oscl::Equip::Holder::Criteria::ITC::Req::Api
		/** */
		void	request(	Oscl::Equip::Holder::
							Criteria::ITC::Req::Api::PushReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							Criteria::ITC::Req::Api::PopReq&	msg
							) noexcept;
	private: // Oscl::Equip::Holder::View::ITC::Req::Api
		/** */
		void	request(	Oscl::Equip::Holder::
							View::ITC::Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Equip::Holder::
							View::ITC::Req::Api::CancelReq&	msg
							) noexcept;
	private: // Oscl::Equip::Holder::FSM::StateVar::ContextApi
		/** */
		void	quietCP() noexcept;
		/** */
		void	incrementDisable() noexcept;
		/** */
		void	decrementDisable() noexcept;
		/** */
		bool	holderEnabled() const noexcept;
		/** */
		bool	isAcceptable() const noexcept;
		/** */
		bool	isUnrecognized() const noexcept;
	};

}
}
}
}

#endif
