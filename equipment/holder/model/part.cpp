/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Equip::Holder::Model;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			RecognitionApi&				recognitionApi,
			bool						enabledByDefault
			) noexcept:
		_recognitionApi(recognitionApi),
		_provisionSync(*this,myPapi),
		_presenceSync(*this,myPapi),
		_enableSync(*this,myPapi),
		_criteriaSync(*this,myPapi),
		_viewSAP(*this,myPapi),
		_state(*this),
		_prevState(0),
		_disableCount(0)
		{
	if(!enabledByDefault){
		_disableCount	= 1;
		_state.disable();
		}
	else {
		_state.enable();
		}
	}

Oscl::Equip::Holder::Provision::ITC::Req::Api::SAP&	Part::provSAP() noexcept{
	return _provisionSync.getSAP();
	}

Oscl::Equip::Holder::Presence::ITC::Req::Api::SAP&	Part::presenceSAP() noexcept{
	return _presenceSync.getSAP();
	}

Oscl::Equip::Holder::Enable::ITC::Req::Api::SAP&	Part::enableSAP() noexcept{
	return _enableSync.getSAP();
	}

Oscl::Equip::Holder::Criteria::ITC::Req::Api::SAP&	Part::criteriaSAP() noexcept{
	return _criteriaSync.getSAP();
	}

Oscl::Equip::Holder::View::ITC::Req::Api::SAP&	Part::viewSAP() noexcept{
	return _viewSAP;
	}

Oscl::Equip::Holder::Provision::Api&		Part::provApi() noexcept{
	return _provisionSync;
	}

Oscl::Equip::Holder::Presence::Api&			Part::presenceApi() noexcept{
	return _presenceSync;
	}

Oscl::Equip::Holder::Enable::Api&			Part::enableApi() noexcept{
	return _enableSync;
	}

Oscl::Equip::Holder::Criteria::Api&			Part::criteriaApi() noexcept{
	return _criteriaSync;
	}

void	Part::notify() noexcept{
	if(_prevState == &_state.state()){
		return;
		}
	Oscl::Equip::Holder::View::ITC::Req::Api::ChangeReq*	next;
	while((next=_observers.get())){
		next->_payload._state	= &_state.state();
		next->returnToSender();
		}
	_prevState	= &_state.state();
	}

void	Part::request(	Oscl::Equip::Holder::
						Provision::ITC::Req::Api::ProvisioningReq&	msg
						) noexcept{
	_state.provisioning();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Provision::ITC::Req::Api::ProvisionedReq&	msg
						) noexcept{
	_state.provisioned();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Provision::ITC::Req::Api::FailedReq&	msg
						) noexcept{
	_state.failed();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Presence::ITC::Req::Api::InsertReq&	msg
						) noexcept{
	_state.insert();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Presence::ITC::Req::Api::RemoveReq&	msg
						) noexcept{
	_state.remove();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Enable::ITC::Req::Api::EnableReq&	msg
						) noexcept{
	if(!_disableCount){
		// Already enabled, error?
		msg.returnToSender();
		return;
		}
	--_disableCount;
	if(_disableCount){
		// No change
		msg.returnToSender();
		return;
		}
	_state.enable();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Enable::ITC::Req::Api::DisableReq&	msg
						) noexcept{
	if(!_disableCount){
		// State enable state change
		++_disableCount;
		_state.disable();
		notify();
		msg.returnToSender();
		return;
		}
	// No state enable state change
	++_disableCount;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Criteria::ITC::Req::Api::PushReq&	msg
						) noexcept{
	_acceptabilityStack.push(&msg._payload._acceptability);
	_state.criteriaChanged();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						Criteria::ITC::Req::Api::PopReq&	msg
						) noexcept{
	_acceptabilityStack.pop();
	_state.criteriaChanged();
	notify();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Equip::Holder::
						View::ITC::Req::Api::ChangeReq&	msg
						) noexcept{
	if(&_state.state() != msg._payload._state){
		msg._payload._state	= &_state.state();
		msg.returnToSender();
		return;
		}
	_observers.put(&msg);
	}

void	Part::request(	Oscl::Equip::Holder::
						View::ITC::Req::Api::CancelReq&	msg
						) noexcept{
	if(_observers.remove(&msg._payload._msgToCancel)){
		msg._payload._msgToCancel.returnToSender();
		}
	msg.returnToSender();
	}

bool	Part::holderEnabled() const noexcept{
	return !_disableCount;
	}

bool	Part::isAcceptable() const noexcept{
	if(_acceptabilityStack.first()){
		return _acceptabilityStack.first()->isAcceptable();
		}
	return false;
	}

bool	Part::isUnrecognized() const noexcept{
	return !_recognitionApi.isRecognized();
	}

