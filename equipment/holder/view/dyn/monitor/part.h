/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_view_dyn_monitor_parth_
#define _oscl_equipment_holder_view_dyn_monitor_parth_
#include "oscl/equipment/holder/dyn/part.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/partvar.h"
#include "oscl/equipment/holder/oid/eh/match.h"
#include "oscl/equipment/holder/view/monitor/info.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace View {
/** */
namespace Dyn {
/** */
namespace Monitor {

/** This object implements a USB Hub as a Shelf.
	The implementation waits for an equipment holder
	with a particular location (its container) to
	become available, establishes a multi-port
	USB hub, and advertises an equipment holder
	for each slot in the shelf.
 */
class Part :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api
				{
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Equip::Holder::
								View::Monitor::Info
								)
						>						_infoMem;
		/** */
		Oscl::Equip::Holder::OID::EH::Match		_match;

		/** */
		Oscl::Mt::Itc::Dyn::
		Unit::PartVar<	Oscl::Equip::
						Holder::Dyn::Part,
						Part
						>						_monitor;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;

		/** */
		const char*	const						_prefix;

		/** */
		Oscl::Done::Operation<Part>				_infoCancelDoneOp;

		/** */
		Oscl::Done::Operation<Part>				_monitorStartDoneOp;

		/** */
		Oscl::Done::Operation<Part>				_monitorStopDoneOp;

		/** */
		Oscl::Equip::Holder::
		View::Monitor::Info*					_info;

		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*				_openReq;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

	public:
		/** */
		Part(
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Equip::
							Holder::Dyn::Part
							>&						ehFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Equip::
							Holder::Dyn::Part
							>::SAP&					ehAdvSAP,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				const Oscl::ObjectID::RO::Api&		location,
				const char*							prefix	= 0
				) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	private:
		/** */
		void	infoCancelDone() noexcept;
		/** */
		void	monitorStartDone() noexcept;
		/** */
		void	monitorStopDone() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	};

}
}
}
}
}
}

#endif
