/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Equip::Holder::View::Dyn::Monitor;

Part::Part(
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Equip::
							Holder::Dyn::Part
							>&						ehFindApi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Equip::
							Holder::Dyn::Part
							>::SAP&					ehAdvSAP,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				const Oscl::ObjectID::RO::Api&		location,
				const char*							prefix
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_match(location),
		_monitor(
					ehAdvSAP,
					ehFindApi,
					myPapi,
					_match,
					*this,
					&Part::activate,
					&Part::deactivate
					),
		_myPapi(myPapi),
		_prefix(prefix),
		_infoCancelDoneOp(*this,&Part::infoCancelDone),
		_monitorStartDoneOp(*this,&Part::monitorStartDone),
		_monitorStopDoneOp(*this,&Part::monitorStopDone),
		_info(0),
		_openReq(0),
		_closeReq(0)
		{
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	_monitor.start(_monitorStartDoneOp);
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	_monitor.stop(_monitorStopDoneOp);
	}


void	Part::infoCancelDone() noexcept{
	_monitor.deactivateDone();
	}

void	Part::monitorStartDone() noexcept{
	if(_openReq){
		_openReq->returnToSender();
		_openReq	= 0;
		}
	}

void	Part::monitorStopDone() noexcept{
	if(_closeReq){
		_closeReq->returnToSender();
		_closeReq	= 0;
		}
	}

void	Part::activate() noexcept{
	Oscl::Equip::Holder::Dyn::Part&	dynSrv	= _monitor._handle->getDynSrv();
	_info	= new(&_infoMem)
				Oscl::Equip::Holder::View::Monitor::
				Info(	dynSrv._model.viewSAP(),
						_myPapi,
						_prefix
						);
	_info->start();
	}

void	Part::deactivate() noexcept{
	_info->stop(_infoCancelDoneOp);
	}

