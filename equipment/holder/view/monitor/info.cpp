/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "info.h"
#include "oscl/error/info.h"

using namespace Oscl::Equip::Holder::View::Monitor;

Info::Info(	Oscl::Equip::Holder::View::
			ITC::Req::Api::SAP&				ehSAP,
			Oscl::Mt::Itc::PostMsgApi&		myPapi,
			const char*						prefix
			) noexcept:
		_part(	ehSAP,
				myPapi,
				*this,
				&Info::emptyAction,
				&Info::newAction,
				&Info::unrecognizedAction,
				&Info::unacceptableAction,
				&Info::invalidAction,
				&Info::missingAction,
				&Info::provisioningAction,
				&Info::provisionedAction,
				&Info::failedAction,
				&Info::stopCompleteAction
				),
		_myPapi(myPapi),
		_prefix(prefix),
		_stopDone(0)
		{
	}

void	Info::start() noexcept{
	_part.start();
	}

void	Info::stop(Oscl::Done::Api& stopDone) noexcept{
	_stopDone	= &stopDone;
	_part.stop(_cancelMem,_myPapi);
	}


void	Info::printPrefix() noexcept{
	if(_prefix){
		Oscl::Error::Info::log(_prefix);
		}
	}

void	Info::emptyAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("EMPTY\n");
	}

void	Info::newAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("NEW\n");
	}

void	Info::unrecognizedAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("UNRECOGNIZED\n");
	}

void	Info::unacceptableAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("UNACCEPTABLE\n");
	}

void	Info::invalidAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("INVALID\n");
	}

void	Info::missingAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("MISSING\n");
	}

void	Info::provisioningAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("PROVISIONING\n");
	}

void	Info::provisionedAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("PROVISIONED\n");
	}

void	Info::failedAction() noexcept{
	printPrefix();
	Oscl::Error::Info::log("FAILED\n");
	}

void	Info::stopCompleteAction() noexcept{
	_stopDone->done();
	_stopDone	= 0;
	}

