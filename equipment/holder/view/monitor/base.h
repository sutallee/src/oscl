/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_view_monitor_baseh_
#define _oscl_equipment_holder_view_monitor_baseh_
#include "oscl/equipment/holder/view/itc/respmem.h"
#include "oscl/equipment/holder/fsm/sv.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace View {
/** */
namespace Monitor {

/** */
class Base : public Oscl::Equip::Holder::View::ITC::Resp::Api {
	private:
		/** */
		Oscl::Equip::Holder::View::ITC::Req::Api::ChangePayload	_payload;
		/** */
		Oscl::Equip::Holder::View::ITC::Resp::Api::ChangeResp	_resp;
		/** */
		Oscl::Equip::Holder::View::ITC::Req::Api::SAP&			_ehSAP;
		/** */
		Oscl::Equip::Holder::FSM::StateVar::Handler&			_handler;
		/** */
		bool													_canceling;

	public:
		/** */
		Base(	Oscl::Equip::Holder::View::ITC::Req::Api::SAP&	ehSAP,
				Oscl::Equip::Holder::FSM::StateVar::Handler&	handler,
				Oscl::Mt::Itc::PostMsgApi&						myPapi
				) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop(	Oscl::Equip::Holder::View::
						ITC::Resp::CancelMem&		cancelMem,
						Oscl::Mt::Itc::PostMsgApi&	myPapi
						) noexcept;

	private:
		/** */
		void	response(ChangeResp& msg) noexcept;
		/** */
		void	response(CancelResp& msg) noexcept;

	protected:
		/** */
		virtual	void stopComplete() noexcept=0;
	};

}
}
}
}
}

#endif
