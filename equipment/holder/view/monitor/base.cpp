/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "base.h"

using namespace Oscl::Equip::Holder::View::Monitor;

Base::Base(	Oscl::Equip::Holder::View::ITC::Req::Api::SAP&	ehSAP,
			Oscl::Equip::Holder::FSM::StateVar::Handler&	handler,
			Oscl::Mt::Itc::PostMsgApi&						myPapi
			) noexcept:
		_payload(),
		_resp(	ehSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_ehSAP(ehSAP),
		_handler(handler),
		_canceling(false)
		{
	}

void	Base::start() noexcept{
	_ehSAP.post(_resp.getSrvMsg());
	}

void	Base::stop(	Oscl::Equip::Holder::View::
					ITC::Resp::CancelMem&		cancelMem,
					Oscl::Mt::Itc::PostMsgApi&	myPapi
					) noexcept{
	Oscl::Equip::Holder::View::ITC::Req::Api::CancelPayload*
	payload	= new(&cancelMem.payload)
				Oscl::Equip::Holder::View::
				ITC::Req::Api::CancelPayload(_resp.getSrvMsg());
	Oscl::Equip::Holder::View::ITC::Resp::Api::CancelResp*
	resp	= new(&cancelMem.resp)
				Oscl::Equip::Holder::View::
				ITC::Resp::Api::CancelResp(	_ehSAP.getReqApi(),
											*this,
											myPapi,
											*payload
											);
	_ehSAP.post(resp->getSrvMsg());
	_canceling	= true;
	}

void	Base::response(	Oscl::Equip::Holder::View::
						ITC::Resp::Api::ChangeResp&		msg
						) noexcept{
	if(_canceling) return;
	msg._payload._state->query(_handler);
	_ehSAP.post(msg.getSrvMsg());
	}

void	Base::response(	Oscl::Equip::Holder::View::
						ITC::Resp::Api::CancelResp&		msg
						) noexcept{
	_canceling	= false;
	stopComplete();
	}

