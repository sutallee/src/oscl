/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_view_monitor_parth_
#define _oscl_equipment_holder_view_monitor_parth_
#include "base.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace View {
/** */
namespace Monitor {

/** */
template <class Context>
class Part :	public Base,
				private Oscl::Equip::Holder::FSM::StateVar::Handler
				{
	public:
		/** */
		typedef void	(Context::* ActionFunc)();

	private:
		/** */
		Context&		_context;
		/** */
		ActionFunc		_emptyAction;
		/** */
		ActionFunc		_newAction;
		/** */
		ActionFunc		_unrecognizedAction;
		/** */
		ActionFunc		_unacceptableAction;
		/** */
		ActionFunc		_invalidAction;
		/** */
		ActionFunc		_missingAction;
		/** */
		ActionFunc		_provisioningAction;
		/** */
		ActionFunc		_provisionedAction;
		/** */
		ActionFunc		_failedAction;
		/** */
		ActionFunc		_stopCompleteAction;

	public:
		Part(	Oscl::Equip::Holder::View::
				ITC::Req::Api::SAP&				ehSAP,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Context&						context,
				ActionFunc						emptyAction,
				ActionFunc						newAction,
				ActionFunc						unrecognizedAction,
				ActionFunc						unacceptableAction,
				ActionFunc						invalidAction,
				ActionFunc						missingAction,
				ActionFunc						provisioningAction,
				ActionFunc						provisionedAction,
				ActionFunc						failedAction,
				ActionFunc						stopCompleteAction
				) noexcept;

	private:	// Base
		/** */
		void	stopComplete() noexcept;
		
	private:	// Oscl::Equip::Holder::FSM::StateVar::Handler
		/** */
		void	emptyState() noexcept;
		/** */
		void	missingState() noexcept;
		/** */
		void	invalidState() noexcept;
		/** */
		void	newState() noexcept;
		/** */
		void	provisioningState() noexcept;
		/** */
		void	provisionedState() noexcept;
		/** */
		void	failedState() noexcept;
		/** */
		void	unrecognizedState() noexcept;
		/** */
		void	unacceptableState() noexcept;
	};


/** */
template <class Context>
Part<Context>::Part(	Oscl::Equip::Holder::View::
						ITC::Req::Api::SAP&				ehSAP,
						Oscl::Mt::Itc::PostMsgApi&		myPapi,
						Context&						context,
						ActionFunc						emptyAction,
						ActionFunc						newAction,
						ActionFunc						unrecognizedAction,
						ActionFunc						unacceptableAction,
						ActionFunc						invalidAction,
						ActionFunc						missingAction,
						ActionFunc						provisioningAction,
						ActionFunc						provisionedAction,
						ActionFunc						failedAction,
						ActionFunc						stopCompleteAction
						) noexcept:
		Base(ehSAP,*this,myPapi),
		_context(context),
		_emptyAction(emptyAction),
		_newAction(newAction),
		_unrecognizedAction(unrecognizedAction),
		_unacceptableAction(unacceptableAction),
		_invalidAction(invalidAction),
		_missingAction(missingAction),
		_provisioningAction(provisioningAction),
		_provisionedAction(provisionedAction),
		_failedAction(failedAction),
		_stopCompleteAction(stopCompleteAction)
		{
	}

/** */
template <class Context>
void	Part<Context>::stopComplete() noexcept{
	(_context.*_stopCompleteAction)();
	}

/** */
template <class Context>
void	Part<Context>::emptyState() noexcept{
	(_context.*_emptyAction)();
	}

/** */
template <class Context>
void	Part<Context>::missingState() noexcept{
	(_context.*_missingAction)();
	}

/** */
template <class Context>
void	Part<Context>::invalidState() noexcept{
	(_context.*_invalidAction)();
	}

/** */
template <class Context>
void	Part<Context>::newState() noexcept{
	(_context.*_newAction)();
	}

/** */
template <class Context>
void	Part<Context>::provisioningState() noexcept{
	(_context.*_provisioningAction)();
	}

/** */
template <class Context>
void	Part<Context>::provisionedState() noexcept{
	(_context.*_provisionedAction)();
	}

/** */
template <class Context>
void	Part<Context>::failedState() noexcept{
	(_context.*_failedAction)();
	}

/** */
template <class Context>
void	Part<Context>::unrecognizedState() noexcept{
	(_context.*_unrecognizedAction)();
	}

/** */
template <class Context>
void	Part<Context>::unacceptableState() noexcept{
	(_context.*_unacceptableAction)();
	}

}
}
}
}
}

#endif
