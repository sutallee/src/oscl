/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_view_monitor_infoh_
#define _oscl_equipment_holder_view_monitor_infoh_
#include "part.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace View {
/** */
namespace Monitor {

/** */
class Info {
	private:
		/** */
		Oscl::Equip::Holder::View::
		ITC::Resp::CancelMem		_cancelMem;
		/** */
		Part<Info>					_part;
		/** */
		Oscl::Mt::Itc::PostMsgApi&	_myPapi;
		/** */
		const char*					_prefix;
		/** */
		Oscl::Done::Api*			_stopDone;

	public:
		/** */
		Info(	Oscl::Equip::Holder::View::
				ITC::Req::Api::SAP&				ehSAP,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				const char*						prefix=0
				) noexcept;
		/** */
		void	start() noexcept;
		/** */
		void	stop(Oscl::Done::Api& stopDone) noexcept;
		/** */
		void	printPrefix() noexcept;

	private:
		/** */
		void	emptyAction() noexcept;
		/** */
		void	newAction() noexcept;
		/** */
		void	unrecognizedAction() noexcept;
		/** */
		void	unacceptableAction() noexcept;
		/** */
		void	invalidAction() noexcept;
		/** */
		void	missingAction() noexcept;
		/** */
		void	provisioningAction() noexcept;
		/** */
		void	provisionedAction() noexcept;
		/** */
		void	failedAction() noexcept;
		/** */
		void	stopCompleteAction() noexcept;
	};

}
}
}
}
}

#endif
