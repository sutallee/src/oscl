/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_dyn_parth_
#define _oscl_equipment_holder_dyn_parth_
#include "oscl/mt/itc/dyn/adv/core.h"
#include "oscl/oid/fixed.h"
#include "oscl/equipment/holder/model/part.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace Dyn {

/** */
class Part :	public Oscl::Mt::Itc::Dyn::Adv::Core<Part> {
	public:
		/** */
		Oscl::Equip::Holder::Model::Part	_model;
		/** */
		enum{ObjectIDSize=32};	// FIXME
		/** */
		Oscl::ObjectID::Fixed<ObjectIDSize>	_location;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			ehPapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Part>::ReleaseApi&				ehAdvRapi,
				Oscl::Mt::Itc::PostMsgApi&			ehAdvPapi,
				Oscl::Mt::Itc::PostMsgApi&			ehDynPapi,
				Oscl::Equip::Holder::Model::
				Part::RecognitionApi&				recognitionApi,
				bool								enabledByDefault,
				const Oscl::ObjectID::RO::Api*		location=0
				) noexcept;

	private: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** */
		void	openDynamicService() noexcept;
		/** */
		void	closeDynamicService() noexcept;
		/** */
		Part&	getDynSrv() noexcept;
	};

}
}
}
}

#endif
