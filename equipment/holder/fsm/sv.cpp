/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sv.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Equip::Holder::FSM;

static const StateVar::Empty						empty;
static const StateVar::New							newState;
static const StateVar::Unrecognized					unrecognized;
static const StateVar::Unacceptable					unacceptable;
static const StateVar::Invalid						invalid;
static const StateVar::Missing						missing;
static const StateVar::Provisioning					provisioningState;
static const StateVar::Provisioned					provisionedState;
static const StateVar::Failed						failedState;

// StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&empty)
		{
	}

void	StateVar::insert() noexcept{
	_state->insert(_context,*this);
	}

void	StateVar::remove() noexcept{
	_state->remove(_context,*this);
	}

void	StateVar::enable() noexcept{
	_state->enable(_context,*this);
	}

void	StateVar::disable() noexcept{
	_state->disable(_context,*this);
	}

void	StateVar::criteriaChanged() noexcept{
	_state->criteriaChanged(_context,*this);
	}

void	StateVar::reset() noexcept{
	_state->reset(_context,*this);
	}

void	StateVar::failed() noexcept{
	_state->failed(_context,*this);
	}

void	StateVar::provisioned() noexcept{
	_state->provisioned(_context,*this);
	}

void	StateVar::provisioning() noexcept{
	_state->provisioning(_context,*this);
	}

void	StateVar::query(Handler& h) const noexcept{
	_state->query(h);
	}

// global actions
void	StateVar::protocolError(const char* state) noexcept{
	ErrorFatal::logAndExit(state);
	}

// state change operations
void	StateVar::changeToEmpty() noexcept{
	_state	= &empty;
	}

void	StateVar::changeToMissing() noexcept{
	_state	= &missing;
	}

void	StateVar::changeToInvalid() noexcept{
	_state	= &invalid;
	}

void	StateVar::changeToNew() noexcept{
	_state	= &newState;
	}

void	StateVar::changeToUnacceptable() noexcept{
	_state	= &unacceptable;
	}

void	StateVar::changeToUnrecognized() noexcept{
	_state	= &unrecognized;
	}

void	StateVar::changeToFailed() noexcept{
	_state	= &failedState;
	}

void	StateVar::changeToProvisioning() noexcept{
	_state	= &provisioningState;
	}

void	StateVar::changeToProvisioned() noexcept{
	_state	= &provisionedState;
	}

