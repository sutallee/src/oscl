/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sv.h"

using namespace Oscl::Equip::Holder::FSM;

void	StateVar::State::
insert(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
remove(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
enable(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
disable(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
criteriaChanged(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
reset(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
failed(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
provisioned(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
provisioning(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

///////////////////////////////////

void	StateVar::Unequipped::
insert(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	if(context.holderEnabled()){
		if(context.isAcceptable()){
			sv.changeToNew();
			}
		else if(context.isUnrecognized()){
			sv.changeToUnrecognized();
			}
		else {
			sv.changeToUnacceptable();
			}
		}
	else {
		sv.changeToInvalid();
		}
	}

void	StateVar::Unequipped::
criteriaChanged(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// Do nothing, since we don't have
	// and circuit-pack to worry about.
	}

///////////////////////////////////

void	StateVar::Empty::
enable(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToMissing();
	}

void		StateVar::Empty::query(Handler& h) const noexcept{
	h.emptyState();
	}

///////////////////////////////////

void	StateVar::Missing::
disable(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.changeToEmpty();
	}

void		StateVar::Missing::query(Handler& h) const noexcept{
	h.missingState();
	}

///////////////////////////////////

void	StateVar::Equipped::
remove(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	if(context.holderEnabled()){
		sv.changeToMissing();
		}
	else {
		sv.changeToEmpty();
		}
	}

///////////////////////////////////

void	StateVar::Valid::
disable(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.changeToInvalid();
	}

void	StateVar::Valid::
reset(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	if(context.holderEnabled()){
		if(context.isAcceptable()){
			sv.changeToNew();
			}
		else if(context.isUnrecognized()){
			sv.changeToUnrecognized();
			}
		else {
			sv.changeToUnacceptable();
			}
		}
	else{
		sv.changeToInvalid();
		}
	sv.changeToInvalid();
	}

///////////////////////////////////

void	StateVar::Invalid::
enable(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	if(context.isAcceptable()){
		sv.changeToNew();
		}
	else if(context.isUnrecognized()){
		sv.changeToUnrecognized();
		}
	else {
		sv.changeToUnacceptable();
		}
	}

void		StateVar::Invalid::query(Handler& h) const noexcept{
	h.invalidState();
	}

///////////////////////////////////

void	StateVar::Acceptable::
criteriaChanged(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.isUnrecognized()){
		sv.changeToUnrecognized();
		}
	else if(!context.isAcceptable()){
		sv.changeToUnacceptable();
		}
	}

///////////////////////////////////

void	StateVar::Unacceptable::
criteriaChanged(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.isAcceptable()){
		sv.changeToNew();
		}
	}

void		StateVar::Unacceptable::query(Handler& h) const noexcept{
	h.unacceptableState();
	}

///////////////////////////////////

void	StateVar::Unrecognized::
criteriaChanged(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.isAcceptable()){
		sv.changeToNew();
		}
	else if(!context.isAcceptable()){
		sv.changeToUnacceptable();
		}
	}

void		StateVar::Unrecognized::query(Handler& h) const noexcept{
	h.unrecognizedState();
	}

///////////////////////////////////

void	StateVar::New::
provisioning(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToProvisioning();
	}

void		StateVar::New::query(Handler& h) const noexcept{
	h.newState();
	}

///////////////////////////////////

void	StateVar::Provisioning::
provisioned(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToProvisioned();
	}

void	StateVar::Provisioning::
failed(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToFailed();
	}

void		StateVar::Provisioning::query(Handler& h) const noexcept{
	h.provisioningState();
	}

///////////////////////////////////

void		StateVar::Provisioned::query(Handler& h) const noexcept{
	h.provisionedState();
	}

///////////////////////////////////

void		StateVar::Failed::query(Handler& h) const noexcept{
	h.failedState();
	}

