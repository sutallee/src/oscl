/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_fsm_svh_
#define _oscl_equipment_holder_fsm_svh_

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace FSM {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:
				/** Make GCC happy */
				virtual ~ContextApi() {}
			public:	// request actions
				/** */
				virtual	bool	holderEnabled() const noexcept=0;
				/** */
				virtual	bool	isAcceptable() const noexcept=0;
				/** */
				virtual	bool	isUnrecognized() const noexcept=0;
			};
	public:
		/** */
		class Handler {
			public:
				/** Make GCC happy */
				virtual ~Handler() {}
			public:
				/** */
				virtual	void	emptyState() noexcept=0;
				/** */
				virtual	void	missingState() noexcept=0;
				/** */
				virtual	void	invalidState() noexcept=0;
				/** */
				virtual	void	newState() noexcept=0;
				/** */
				virtual	void	provisioningState() noexcept=0;
				/** */
				virtual	void	provisionedState() noexcept=0;
				/** */
				virtual	void	failedState() noexcept=0;
				/** */
				virtual	void	unrecognizedState() noexcept=0;
				/** */
				virtual	void	unacceptableState() noexcept=0;
			};
		/** */
		class State {
			public:
				/** Make GCC happy */
				virtual ~State() {}

			public:	// Admin Events
				/** */
				virtual void	insert(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	remove(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	enable(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	disable(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	criteriaChanged(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	reset(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	failed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	provisioned(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	provisioning(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			public:
				/** */
				virtual const char*	stateName() const noexcept=0;
				/** This operation invokes the appropriate
					handler operation according to the concrete
					type of the State.
				 */
				virtual	void		query(Handler& h) const noexcept=0;
			};

	public:
		/** */
		class Unequipped : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Unequipped";}
			private:
				/** */
				void	insert(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	criteriaChanged(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class Empty : public Unequipped {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Empty";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	enable(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Missing : public Unequipped {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Missing";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	disable(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class Equipped : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Equipped";}
			public:
				/** */
				void	remove(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Valid : public Equipped {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Valid";}
			public:
				/** */
				void	disable(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	reset(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Invalid : public Equipped {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Invalid";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	enable(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};

		/** */
		class Acceptable : public Valid {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Acceptable";}
			public:
				/** */
				void	criteriaChanged(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};

		/** */
		class Unacceptable : public Valid {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Unacceptable";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	criteriaChanged(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class Unrecognized : public Valid {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Unrecognized";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	criteriaChanged(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class New : public Acceptable {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "New";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	provisioning(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class Provision : public Acceptable {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Provision";}
			public:
			};
		/** */
		class Failed : public Provision {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Failed";}
				/** */
				void		query(Handler& h) const noexcept;
			};
		/** */
		class Provisioning : public Provision {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Provisioning";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
				/** */
				void	provisioned(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	failed(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Provisioned : public Provision {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Provisioned";}
				/** */
				void		query(Handler& h) const noexcept;
			public:
			};
	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	public:
		/** */
		StateVar(ContextApi& context) noexcept;

		/** */
		void	query(Handler& h) const noexcept;

		/** */
		inline const State&	state() const noexcept{ return *_state;}

	public:	// events
		/** */
		void	insert() noexcept;
		/** */
		void	remove() noexcept;
		/** */
		void	enable() noexcept;
		/** */
		void	disable() noexcept;
		/** */
		void	criteriaChanged() noexcept;
		/** */
		void	reset() noexcept;
		/** */
		void	failed() noexcept;
		/** */
		void	provisioned() noexcept;
		/** */
		void	provisioning() noexcept;

	private: // global actions and state change operations
		/** */
		friend class Unequipped;
		friend class Empty;
		friend class Missing;
		friend class Equipped;
		friend class Valid;
		friend class Invalid;
		friend class Acceptable;
		friend class Unacceptable;
		friend class Unrecognized;
		friend class Provision;
		friend class Failed;
		friend class Provisioning;
		friend class Provisioned;
		/** */
		void	protocolError(const char* state) noexcept;
		/** */
		void	changeToEmpty() noexcept;
		/** */
		void	changeToMissing() noexcept;
		/** */
		void	changeToInvalid() noexcept;
		/** */
		void	changeToNew() noexcept;
		/** */
		void	changeToUnacceptable() noexcept;
		/** */
		void	changeToUnrecognized() noexcept;
		/** */
		void	changeToFailed() noexcept;
		/** */
		void	changeToProvisioning() noexcept;
		/** */
		void	changeToProvisioned() noexcept;
	};

}
}
}
}

#endif
