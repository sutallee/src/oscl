/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_holder_provision_itc_reqapih_
#define _oscl_equipment_holder_provision_itc_reqapih_
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace Holder {
/** */
namespace Provision {
/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class ProvisioningPayload {};
		/** */
		class ProvisionedPayload {};
		/** */
		class FailedPayload {};

	public:
		/** */
		typedef SrvEvent<Api,ProvisioningPayload>	ProvisioningReq;
		/** */
		typedef SrvEvent<Api,ProvisionedPayload>	ProvisionedReq;
		/** */
		typedef SrvEvent<Api,FailedPayload>			FailedReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(ProvisioningReq& msg) noexcept=0;
		/** */
		virtual void	request(ProvisionedReq& msg) noexcept=0;
		/** */
		virtual void	request(FailedReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
