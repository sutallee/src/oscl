/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_cp_usb_hub_acceptabilityh_
#define _oscl_equipment_cp_usb_hub_acceptabilityh_
#include "oscl/equipment/holder/criteria/acceptability.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace CP {
/** */
namespace USB {
/** */
namespace HUB {

/** */
class Acceptability : public Oscl::Equip::Holder::Criteria::Acceptability {
	private:
		/** */
		class Part&	_context;

	public:
		/** */
		Acceptability(Part& context) noexcept;
	private:
		/** This operation is invoked from the equipment
			holder thread to determin if the circuit-pack
			currently inserted is of an acceptable type.
			Acceptability is a function of not only what
			the system is prepared to instantiate for this
			equipment-holder slot, but also one of whether
			the application configuration expects a circuit-
			pack of this type to be in this slot.
		 */
		bool	isAcceptable() noexcept;
	};


}
}
}
}
}

#endif

