/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"

using namespace Oscl::Equip::CP::USB::HUB;

Part::Part(
					Oscl::Equip::Holder::View::
					ITC::Req::Api::SAP&						ehSAP,
					Oscl::Equip::Holder::Criteria::Api&		ehCriteriaApi,
					Oscl::Equip::Holder::Provision::Api&	ehProvisionApi,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::Usb::DynDevice>&			advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				cpPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		_ehView(	ehSAP,
					cpPapi,
					*this,
					&Part::emptyAction,
					&Part::newAction,
					&Part::unrecognizedAction,
					&Part::unacceptableAction,
					&Part::invalidAction,
					&Part::missingAction,
					&Part::provisioningAction,
					&Part::provisionedAction,
					&Part::failedAction,
					&Part::stopCompleteAction
					),
		_ehCriteriaApi(ehCriteriaApi),
		_ehProvisionApi(ehProvisionApi),
		_acceptability(*this),
		_doneOp(*this,&Part::hubCreatorStopDone),
		_hubCreator(	advSAP,
						advCapi,
						advRapi,
						delayServiceSAP,
						cpPapi,
						hubPapi,
						dynSrvPapi,
						setupMem,
						packetMem,
						adaptorSetupMem,
						adaptorPacketMem,
						portDriverMem,
						portAdaptorMem,
						portSetupMem,
						portPacketMem,
						portDriverSetupMem,
						portDriverPacketMem,
						maxPorts,
						oidType
						),
		_advFind(advFind),
		_myPapi(cpPapi),
		_acceptMatch(location),
		_claimMatch(location),
		_stopDone(0),
		_open(false),
		_newState(false),
		_deactivating(false),
		_activated(false)
		{
	}

void	Part::start() noexcept{
	_ehView.start();
	_ehCriteriaApi.push(_acceptability);
	_open	= true;
	}

void	Part::stop(CancelMem& mem, Oscl::Done::Api& done) noexcept{
	_open		= false;
	_stopDone	= &done;
	_ehCriteriaApi.pop();
	_ehView.stop(mem.ehViewCancelMem,_myPapi);
	}

void	Part::emptyAction() noexcept{
	antiProvision();
	}

void	Part::newAction() noexcept{
	antiProvision();
	_newState	= true;
	// Provision the circuit-pack.
	provision();
	}

void	Part::unrecognizedAction() noexcept{
	antiProvision();
	}

void	Part::unacceptableAction() noexcept{
	antiProvision();
	}

void	Part::invalidAction() noexcept{
	antiProvision();
	}

void	Part::missingAction() noexcept{
	antiProvision();
	}

void	Part::provisioningAction() noexcept{
	}

void	Part::provisionedAction() noexcept{
	}

void	Part::failedAction() noexcept{
	antiProvision();
	}

void	Part::stopCompleteAction() noexcept{
	// The assumption here is that we have
	// received a CloseReq as that is the
	// only reason that I can think of to
	// stop the equipment holder monitor.
	if(_handle != 0){
		_hubCreator.stop(_doneOp);
		}
	}

void	Part::provision() noexcept{
	if(!_newState){
		return;
		}
	if(_handle == 0){
		_handle = _advFind.find(_claimMatch);
		if(_handle == 0){
			// Assume the circuit-pack has been removed?
			// Perhaps, the right thing to do is to send
			// the EH a reset. No, that might cause a
			// loop.
			return;
			}
		}

	// Indicate that we're provisioning
	_ehProvisionApi.provisioning();

	// Provision the circuit-pack.
	if(_hubCreator.start(_handle)){
		_ehProvisionApi.failed();
		return;
		}
	_ehProvisionApi.provisioned();
	_activated = true;
	}

void	Part::antiProvision() noexcept{
	_newState	= false;
	if(!_activated){
		return;
		}
	if(_deactivating){
		return;
		}
	_deactivating	= true;
	_activated		= false;
	_hubCreator.stop(_doneOp);
	}

void	Part::hubCreatorStopDone() noexcept{
	_handle	= 0;
	_deactivating	= false;
	if(_stopDone){
		_stopDone->done();
		}
	}

