/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_cp_usb_hub_serviceh_
#define _oscl_equipment_cp_usb_hub_serviceh_
#include "part.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace CP {
/** */
namespace USB {
/** */
namespace HUB {

/** This concrete class is a single USB HUB device monitor.
	It waits for a new USB hub to be created, claims the
	device, and then creates a new HUB driver for that device.
	When the device is removed from the USB hub device is removed
	from the USB bus, the resources for the HUB driver are reclaimed
	and the monitor waits for a new hub to be created.
	This class is responsible for creating exactly one USB HUB
	driver.
 */
class Service :
		public Oscl::Mt::Itc::Srv::CloseSync,
		public Oscl::Mt::Itc::Srv::Close::Req::Api
		{
	private:
		/** */
		union CancelMem {
			/** */
			Oscl::Equip::CP::USB::HUB::Part::CancelMem	hubPart;
			};

	private:
		/** */
		CancelMem								_cancelMem;

		/** */
		Oscl::Equip::CP::USB::HUB::Part			_part;

	private:
		/** */
		Oscl::Done::Operation<Service>			_doneOp;

		/** Set to the close request message
			when closing.
		 */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

		/** */
		bool									_open;
		/** */
		bool									_newState;
		/** */
		bool									_deactivating;
		/** */
		bool									_activated;
		
	public:
		/** */
		Service(
					Oscl::Equip::Holder::View::
					ITC::Req::Api::SAP&						ehSAP,
					Oscl::Equip::Holder::Criteria::Api&		ehCriteriaApi,
					Oscl::Equip::Holder::Provision::Api&	ehProvisionApi,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::Usb::DynDevice>&			advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				cpPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:
		/** */
		void	partStopDone() noexcept;
		
	};

}
}
}
}
}

#endif

