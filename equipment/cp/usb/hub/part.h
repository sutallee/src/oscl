/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_equipment_cp_usb_hub_parth_
#define _oscl_equipment_cp_usb_hub_parth_
#include "match.h"
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/alloc/power/respapi.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/driver/usb/hub/adaptor/driver.h"
#include "oscl/driver/usb/hub/port/driver/driver.h"
#include "oscl/driver/usb/desc/hub.h"
#include "oscl/mt/itc/delay/reqapi.h"
#include "oscl/equipment/holder/view/monitor/part.h"
#include "oscl/equipment/holder/criteria/api.h"
#include "oscl/equipment/holder/provision/api.h"
#include "acceptability.h"
#include "oscl/driver/usb/hub/creator/part.h"
#include "oscl/done/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace Equip {
/** */
namespace CP {
/** */
namespace USB {
/** */
namespace HUB {

/** This concrete class is a single USB HUB device monitor.
	It waits for a new USB hub to be created, claims the
	device, and then creates a new HUB driver for that device.
	When the device is removed from the USB hub device is removed
	from the USB bus, the resources for the HUB driver are reclaimed
	and the monitor waits for a new hub to be created.
	This class is responsible for creating exactly one USB HUB
	driver.
 */
class Part {
	/** */
	friend class Acceptability;
	public:
		/** */
		union CancelMem {
			/** */
			Oscl::Equip::Holder::View::
			ITC::Resp::CancelMem			ehViewCancelMem;
			};
	private:
		/** */
		Oscl::Equip::Holder::View::
		Monitor::Part<Part>						_ehView;
		/** */
		Oscl::Mt::Itc::Dyn::
		Handle<Oscl::Usb::DynDevice>			_handle;
		/** */
		Oscl::Equip::Holder::Criteria::Api&		_ehCriteriaApi;
		/** */
		Oscl::Equip::Holder::Provision::Api&	_ehProvisionApi;
		/** */
		Acceptability							_acceptability;
		/** FIXME: union with other cancelation
			memory and serialize (already serialized for
			simplicity).
		 */
		Oscl::Equip::Holder::View::
		ITC::Resp::CancelMem					_ehViewCancelMem;

	private:
		/** */
		Oscl::Done::Operation<Part>				_doneOp;
		/** */
		Oscl::Usb::Hub::Creator::Part			_hubCreator;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		SyncApi<Oscl::Usb::DynDevice>&			_advFind;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		AcceptMatch								_acceptMatch;
		/** */
		ClaimMatch								_claimMatch;
		/** */
		Oscl::Done::Api*						_stopDone;

		/** */
		bool									_open;
		/** */
		bool									_newState;
		/** */
		bool									_deactivating;
		/** */
		bool									_activated;
		
	public:
		/** */
		Part(
					Oscl::Equip::Holder::View::
					ITC::Req::Api::SAP&						ehSAP,
					Oscl::Equip::Holder::Criteria::Api&		ehCriteriaApi,
					Oscl::Equip::Holder::Provision::Api&	ehProvisionApi,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::Usb::DynDevice>&			advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				cpPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;

		/** */
		void	start() noexcept;
		/** */
		void	stop(CancelMem& mem, Oscl::Done::Api& done) noexcept;

	private:	// actions for Oscl::Equip::Holder::View::Monitor::Part
		/** */
		void	emptyAction() noexcept;
		/** */
		void	newAction() noexcept;
		/** */
		void	unrecognizedAction() noexcept;
		/** */
		void	unacceptableAction() noexcept;
		/** */
		void	invalidAction() noexcept;
		/** */
		void	missingAction() noexcept;
		/** */
		void	provisioningAction() noexcept;
		/** */
		void	provisionedAction() noexcept;
		/** */
		void	failedAction() noexcept;
		/** */
		void	stopCompleteAction() noexcept;

	private:
		/** */
		void	provision() noexcept;
		/** */
		void	antiProvision() noexcept;

	private:
		/** */
		void	hubCreatorStopDone() noexcept;
		
	};

}
}
}
}
}

#endif

