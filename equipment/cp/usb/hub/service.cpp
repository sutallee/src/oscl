/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"
#include "oscl/error/info.h"

using namespace Oscl::Equip::CP::USB::HUB;

Service::Service(
					Oscl::Equip::Holder::View::
					ITC::Req::Api::SAP&						ehSAP,
					Oscl::Equip::Holder::Criteria::Api&		ehCriteriaApi,
					Oscl::Equip::Holder::Provision::Api&	ehProvisionApi,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::Usb::DynDevice>&			advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				cpPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,cpPapi),
		_part(
					ehSAP,
					ehCriteriaApi,
					ehProvisionApi,
					advFind,
					advSAP,
					advCapi,
					advRapi,
					delayServiceSAP,
					cpPapi,
					hubPapi,
					dynSrvPapi,
					setupMem,
					packetMem,
					adaptorSetupMem,
					adaptorPacketMem,
					portDriverMem,
					portAdaptorMem,
					portSetupMem,
					portPacketMem,
					portDriverSetupMem,
					portDriverPacketMem,
					maxPorts,
					oidType,
					location
					),
		_doneOp(*this,&Service::partStopDone),
		_closeReq(0),
		_open(false)
		{
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	if(_closeReq){
		Oscl::Error::Info::log(	"Equip::CP::USB::HUB::Service: "
								"open programming error!"
								);
		return;
		}
	_part.start();
	_open	= true;
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	if(_closeReq){
		Oscl::Error::Info::log(	"Equip::CP::USB::HUB::Service: "
								"close programming error!"
								);
		return;
		}
	_closeReq	= &msg;
	_open		= false;
	_part.stop(_cancelMem.hubPart,_doneOp);
	}

void	Service::partStopDone() noexcept{
	if(_closeReq){
		_closeReq->returnToSender();
		_closeReq	= 0;
		}
	}

