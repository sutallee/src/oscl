/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ber_rxh_
#define _oscl_ber_rxh_
#include "oscl/oid/api.h"

/** This class is used to decode a BER extended value type.
	When constructed, the value is decoded and the number of octets
	it consumed in its encoded form are saved.
 */
class BerExtendedUnsigned {
	private:
		/** Contains the decoded value.
		 */
		unsigned	_value;

		/** Contains the number of octets occupied by the encoded value.
		 */
		unsigned	_length;

	public:
		/** During the execution of this constructor, the encoded value
			is decoded and placed in the _value field, and the number
			of octets occupied by the encoded value is placed in the
			_length field. This constructor requires a pointer to the
			octet buffer containing the encoding, and the length
			of the buffer. The buffer length is used to detect invalid
			encodings.
		 */
		BerExtendedUnsigned(const unsigned char* p,unsigned pLen) noexcept;

		/** This accessor returns the number of octets occupied in the
			origninal octet buffer by the encoded value.
		 */
		unsigned	getLength() const noexcept;

		/** This accessor returns the decoded integer value.
		 */
		unsigned	getValue() const noexcept;
	};

/** This class is used to decode a BER OID (Object Identifier).
	The decoded value is obtained using the iteration interface of
	this class.

	E.g.:
		void	foo(const void* content, unsigned contentLength) {
			BerOID	oid(content,contentLength);
			for(unsigned i=0;oid.more();oid.next(),++i){
				// Store next OID value.
				}
			}
 */
class BerOID {
	private:
		/** Used by the implementation during iteration to
			keep track of the current octet offset into the
			encoded buffer.
		 */
		unsigned					_index;

		/** Initialized by the constructor to the number of
			contiguous valid octets contained within the buffer.
			This value is used to validate encodings.
		 */
		const unsigned				_length;

		/** Points to the beginning of the octet buffer containing
			the encoded value as passed through the constructor.
		 */
		const unsigned char* const	_enc;

		/** True if an invalid encoding is detected. Reset
			to false at construction or the re-starting of iteration.
		 */
		bool						_err;

		/** Contains the value of the current sub OID value
			during iteration. This value is returned by the
			current() accessor operation.
		 */
		unsigned long			_currentSubIdValue;

		/** Contains the number of octets occupied by the
			_currentSubIdValue in its encoded form. It is
			used by the implementation to track the current
			location in the encoded buffer during iteration.
		 */
		unsigned long			_currentSubIdLength;

	public:
		/** The constructor requires a pointer to an octet
			buffer that contains the encoded BER OID, and
			the maximum valid length of the buffer in octets.
		 */
		BerOID(const void* content,unsigned len) noexcept;

		/** This operation returns true if an encoding
			error is encountered during construction
			or iteration.
		 */
		bool			error() const noexcept;

		/** This operation returns the value of the
			current OID sub value. This is invoked
			during iteration to get the current sub
			OID.
		 */
		unsigned long	current() const noexcept;

		/** This iterator operation is used at the beginning
			of the OID decoding iteration to reset decoder's
			internal state and decode the first sub OID value.
			The error state is also reset by this operation.
		 */
		void			first() noexcept;

		/** This iterator operation is used to decode the
			next sub OID. During this operation, the next
			sub OID is decoded, the end-of-OID is detected,
			or an encoding error may be detected.
		 */
		void			next() noexcept;

		/** This iterator operation is used after the first()
			or next() operation to determine if another sub
			OID has been successfully decoded and the value
			is available using the current() operation.
		 */
		bool			more() const noexcept;

	private:
		/** This internal operation is invoked to decode the
			next sub OID.
		 */
		void			decodeCurrentSubID() noexcept;
	};

/** The purpose of this class is to provide a means for decoding
	BER (ASN.1 Basic Encoding Rules ITU.X690 ) encoded values.
	BER is commonly used for propagating values across heterogeneous
	data communications networks. The SNMP (Simple Network Management Protocol)
	and many other standard data communications protocols make use of the
	encoding rules implemented by this class.

	This class is used by clients that wish to decode received
	BER values.

	This particular implementation currently only implements
	the most common BER encodings types:

		INTEGER
		OCTETSTRING
		OBJECTIDENTIFIER
		SEQUENCE

	Usage:
		1)	Construct a BerRxEncoding passing a buffer (pdu)
			and the length of the buffer with the constructor
			to initialize the decoder.

		2)	Repeat the following steps:

			2.1)	Invoke the encodingIsValid() operation to determine
					if the first encoding is valid. If this operation
					returns false, and no further data is required
					by the client, then proceed to step 3. If this
					operation returns false and the client expects
					more data, then a protocol error is present. If
					true is returned and the client does not expect
					more data, then a protocol error is present. Otherwise
					proceed.

			2.2)	Determine the type of the encoded value using the
					following operations:
						o currentIsINTEGER()
						o currentIsSEQUENCE()
						o currentIsOID()
						o currentIsOCTETSTRING()
						o currentIsEndOfContents()
			2.3)	Use the operation corresponding to the "currentIs"
					operation from step 2.2 above to retrieve the
					decoded value:
						o decodeINTEGER()
						o decodeSINTEGER()
						o decodeUINTEGER()
						o decodeOCTETSTRING()
						o decodeOID()

			2.4)	Continue at 2.1 as required by the client.

		3)	Destruct the BerRxEncoding to reclaim its memory.
 */
class BerRxEncoding {
	private:
		/** Points to the beginning of the octet buffer containing
			the encoded value(s) as passed through the constructor.
		 */
		const unsigned char*	_pdu;

		/** Initialized by the constructor to the maximum number of
			contiguous valid octets contained within the buffer.
			This value is used to validate encodings.
		 */
		const unsigned			_length;

		/** Used to track the current octet offset from the beginning
			of the encoding buffer (_pdu) as the decoding proceeds.
		 */
		unsigned				_offset;

		/** Contains the value of the current Tag (Type)
			as decoding proceeds.
		 */
		unsigned long			_currentTag;

		/** Contains the number of octets consumed by
			the current encoded value as decoding proceeds.
		 */
		unsigned				_currentLength;

		/** Tracks the octet offset to the location of the current
			Length field in the octet buffer.
		 */
		unsigned				_lengthOffset;

		/** Tracks the octet offset to the location of the current
			Value field in the octet buffer.
		 */
		unsigned				_valueOffset;

	public:
		/** This constructor requires a pointer to the octet buffer
			containing the encoded value(s) and the length of the
			buffer in octets.
		 */
		BerRxEncoding(const void* p,unsigned bufferLength) noexcept;

		/** Returns the value of the current identifer TAG (Type).
		 */
		unsigned long	currentIdentifierTag() const noexcept;

		/** Returns the value TLV Length value for the current
			encoding. This corresponds to the number of bytes
			consumed by the encoded value. This is typically used in
			conjunction with currentContent() to copy raw octet values
			from SEQUENCE and other contained types.
		 */
		unsigned long	currentContentLength() const noexcept;

		/** Returns a pointer to the Value field of the current TLV in the
			octet buffer. This is typically used in conjunction with
			currentContentLength() to copy raw octet values from SEQUENCE
			and other contained types.
		 */
		const void*		currentContent() const noexcept;

		/** Returns a pointer to the beginning of the current TLV encoding.
			This may be used in conjunction with currentSize() to copy
			the current raw TLV encoding.
		 */
		const void*		currentEncoding() const noexcept;

		/** Returns the length of the current TLV encoding.
			This corresponds to the number of bytes consumed by the
			TLV encoding. This is typically used in conjunction with
			currentSize() to copy entire TLV encodings.
		 */
		unsigned		currentSize() const noexcept;

		/**	Advances to the next TLV encoding in the octet buffer.
			The client uses this operation after it has decoded
			the current TLV encoding.
			Returns true if there are more octets in the buffer that
			could contain encodings. This is a function of the 
			number of valid octets in the buffer as supplied
			by in the constructor and the current decoding position.
		 */
		bool			nextEncoding() noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a primitive BER type.
		 */
		bool			currentTypeIsPrimitive() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER class type Universal.
		 */
		bool			currentClassIsUniversal() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER class type Application.
		 */
		bool			currentClassIsApplication() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER class type "Context Specific".
		 */
		bool			currentClassIsContextSpecific() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER class type "Context Private".
		 */
		bool			currentClassIsContextPrivate() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER type is an ASN.1 INTEGER.
		 */
		bool			currentIsINTEGER() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER type is an ASN.1 OCTETSTRING.
		 */
		bool			currentIsOCTETSTRING() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER type is an ASN.1 OID.
		 */
		bool			currentIsOID() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER type is an ASN.1 SEQUENCE.
		 */
		bool			currentIsSEQUENCE() const noexcept;

		/** Returns true if the current encoding in the octet
			buffer is a BER type is an ASN.1 "End Of Contents"
			indicator for nested types.
		 */
		bool			currentIsEndOfContents() const noexcept;

		/** Returns the integer value of the current encoding.
			The assumption is made that currentIsINTEGER() returns
			true, otherwise the return value is indeterminate.
		 */
		signed long		decodeINTEGER() const noexcept;

		/** Returns the signed integer value of the current encoding.
			The assumption is made that currentIsINTEGER() returns
			true, otherwise the return value is indeterminate.
		 */
		signed long		decodeSINTEGER() const noexcept;

		/** Returns the unsigned integer value of the current encoding.
			The assumption is made that currentIsINTEGER() returns
			true, otherwise the return value is indeterminate.
		 */
		unsigned long	decodeUINTEGER() const noexcept;

		/** Copies the OCTETSTRING value to the buffer pointed to
			by dest that is at least maxLength octets in length.
			Returns the actual number of octets copied to the
			destination buffer.
			The assumption is made that currentIsOCTETSTRING() returns
			true, otherwise the returned values are indeterminate.
		 */
		unsigned		decodeOCTETSTRING(	void*		dest,
											unsigned	maxLength
											) const noexcept;

		/** Copies the OID value to the OID object referenced by
			oid.  The assumption is made that currentIsOID() returns
			true, otherwise the returned values are indeterminate.
		 */
		void			decodeOID(Oscl::ObjectID::Api& oid) const noexcept;

		/** This operation is invoked to decode the current TLV encoding.
			The TLV Type and Length fields are decoded and the internal
			state is initialized to point to the various TLV fields.
			This operation MUST be invoked for each TLV encoding
			before any of the other operations are invoked.
		 */
		bool			encodingIsValid() noexcept;

	private:
		/** This operation is used internally to decode the TLV tag value.
		 */
		unsigned		decodeCurrentTag() noexcept;

		/** This operation is used inernally to decode the TLV Length field.
		 */
		unsigned		decodeCurrentLength() noexcept;

		/** This operation is used internally to determine the length of
			a TLV with an "Indefinite" length (essentially a null
			terminated style of encoding).
		 */
		unsigned		lengthOfIndefinite() const noexcept;

	};

#endif
