/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ber_txh_
#define _oscl_ber_txh_
#include "oscl/oid/roapi.h"

typedef enum{
	berClassTypeUniversal		= 0,
	berClassTypeApplication		= 1,
	berClassTypeContextSpecific	= 2,
	berClassTypePrivate			= 3
	} BerClassType;

/** The purpose of this class is to provide a means for creating
	BER (ASN.1 Basic Encoding Rules ITU.X690 ) encoded values
	in a buffer. BER is commonly used for propagating values
	across heterogeneous data communications networks. The SNMP
	(Simple Network Management Protocol) and many other
	standard data communications protocols make use of the
	encoding rules implemented by this class.

	This class is used by clients that wish to encode
	BER values for transmision.

	This particular implementation currently only implements
	the most common BER encodings types:

		INTEGER
		OCTETSTRING
		OBJECTIDENTIFIER
		SEQUENCE

	Usage:
		1)	Construct a BerTxEncoder passing a buffer (pdu)
			and the length of the buffer with the constructor
			to initialize the encoder.

		2)	Use the various BerTxEncoder operations to encode
			values sequentially into the buffer.

		3)	Invoke the tooBig() operation after encoding
			is complete to find wheter or not the buffer was long
			enough to contain all of the encoded values
			without overflow.

		4)	Destruct the BerTxEncoder to reclaim its memory.

		5)	Transmit the associated buffer.
 */
class BerTxEncoder {
	private:
		/** A pointer to the octet buffer managed by this encoder as
			passed through its constructor.
		 */
		unsigned char*		_pdu;

		/** The length in octets of the buffer managed by this encoder
			as passed through its constructor.
		 */
		const unsigned		_length;

		/** The offset in octets from the beginning of the octet buffer
			(_pdu) to the first octet not used for an encoding. This value
			is initialized to zero and is advanced as new encodings are
			appended to the buffer.
		 */
		unsigned			_offset;

		/** Initialized to false, and changed to true if the client
			attempts to encode more than the buffer can hold.
		 */
		bool				_overflow;

	public:
		/** The constructor requires a pointer to an octet buffer (pdu)
			that is intended to hold the BER value(s). The size of the
			buffer (number of octets) is specified by the length
			parameter and is used by the BerTxEncoder implementation
			to prevent and track overflowing the buffer when the client
			attempts to encode more information than the buffer can
			hold.
		 */
		BerTxEncoder(void* pdu,unsigned length) noexcept;

		/** It is expected that the client will invoke this operation
			to determine whether or not all values were encoded
			into the buffer without overflow.

			Returns true if there was an overflow error while
			encoding values into the buffer.
		 */
		bool	tooBig() const noexcept{return _overflow;}

		/** This operation is used when creating BER Constructed
			types whose length is unknown before encoding.

			BER uses a Type-Length-Value encoding sequence. When the
			"Length" is not known until the Value has been encoded
			the client:
				1)	invokes this operation to encode the "Type" field,
				2)	Saves a "pointer" to the "Length" field.
				3)	Places a dummy length into the buffer using the
					encodeDefiniteTwoOctetLength() operation.
				4)	Encodes the "Value" using appropriate operators.
				5)	Using the saved "pointer", the length of the
					value is encoded.

			E.g.:
				void	foo(BerTxEncoder& response) {
					// Encode the Constructed "Type"
					response.encodeConstructedIdentifier();
					// Save the "Length" "pointer".
					BerTxEncoder	lengthEnc(	response.currentBuffer(),
												response.remaining()
												);
					// Write dummy length
					response.encodeDefiniteTwoOctetLength(0);
					// Create a container for the SEQUENCE content.
					BerTxEncoder	resp(	response.currentBuffer(),
											response.remaining()
											);
					// Write whatever to the container.
					encodeSequenceStuff(resp);
					// Encode the real length of the SEQUENCE using the 
					// saved "Length pointer" and the length of the
					// values encoded in the SEQUENCE container.
					lengthEnc.encodeDefiniteTwoOctetLength(resp.length());

					// Tell the containing encoder that we have added
					// the container Value field.
					response.advance(resp.length());
					}
		 */
		void	encodeConstructedIdentifier(	BerClassType	type,
												unsigned char	tag
												) noexcept;
		/** This operation is used when creating BER SEQUENCE
			types whose length is unknown before encoding.

			BER uses a Type-Length-Value encoding sequence. When the
			"Length" is not known until the Value has been encoded
			the client:
				1)	invokes this operation to encode the "Type" field,
				2)	Saves a "pointer" to the "Length" field.
				3)	Places a dummy length into the buffer using the
					encodeDefiniteTwoOctetLength() operation.
				4)	Encodes the "Value" using appropriate operators.
				5)	Using the saved "pointer", the Length of the
					value is encoded.

			E.g.:
				void	foo(BerTxEncoder& response) {
					// Encode the SEQUENCE "Type"
					response.encodeSequenceIdentifier();
					// Save the "Length" "pointer".
					BerTxEncoder	lengthEnc(	response.currentBuffer(),
												response.remaining()
												);
					// Write dummy length
					response.encodeDefiniteTwoOctetLength(0);
					// Create a container for the SEQUENCE content.
					BerTxEncoder	resp(	response.currentBuffer(),
											response.remaining()
											);
					// Write whatever to the container.
					encodeSequenceStuff(resp);
					// Encode the real length of the SEQUENCE using the 
					// saved "Length pointer" and the length of the
					// values encoded in the SEQUENCE container.
					lengthEnc.encodeDefiniteTwoOctetLength(resp.length());

					// Tell the containing encoder that we have added
					// the container Value field.
					response.advance(resp.length());
					}
		 */
		void	encodeSequenceIdentifier() noexcept;

		/** This operation is used to encode a two octet length
			field at the current offset. This is typically used
			when building Constructed and SEQUENCE value types.
			See the usage in the comments for the encodeConstructedIdentifier()
			and encodeSequenceIdentifier() operations.
			The encoder's position offset is advanced past the encoding at
			the completion of this operation.
		 */
		void	encodeDefiniteTwoOctetLength(unsigned length) noexcept;

		/** This operation is used to directly encode the Type-Length-Value
			sequence for an ASN.1 INTEGER type at the current position
			of the encoder's buffer. The encoder's position offset is
			advanced past the encoding at the completion of this operation.
		 */
		void	encodeINTEGER(signed long value) noexcept;

		/** This operation is used to explicitly encode a signed integer
			as an ASN.1 INTEGER type at the current position of the encoder's
			buffer. The encoder's position offset is advanced past the
			encoding at the completion of this operation.
		 */
		void	encodeSINTEGER(signed long value) noexcept;

		/** This operation is used to explicitly encode an unsigned integer
			as an ASN.1 INTEGER type at the current position of the encoder's
			buffer. The encoder's position offset is advanced past the
			encoding at the completion of this operation.
		 */
		void	encodeUINTEGER(unsigned long value) noexcept;

		/** This operation is used to directly encode the Type-Length-Value
			sequence for an ASN.1 OID (Object ID) type at the current position
			of the encoder's buffer. The position offset is advanced past
			the encoding at the completion of this operation.
		 */
		void	encodeOID(const Oscl::ObjectID::RO::Api& oid) noexcept;

		/** This operation is used to directly encode the Type-Length-Value
			sequence for an ASN.1 OCTETSTRING type at the current position
			of the encoder's buffer. The position offset is advanced past
			the encoding at the completion of this operation.
		 */
		void	encodeOCTETSTRING(const void* s,unsigned length) noexcept;

		/** This operation returns a pointer to the
			octet at the current offset of the encoder,
			and is intended for building nested encodings.
		 */
		unsigned char*	currentBuffer() const noexcept;

		/** This operation returns the number of valid octets
			contained in the buffer buffer that is managed
			by this encoder. This is the same as the offset
			from the beginning of the buffer to the first unused octet.
		 */
		unsigned		length() const noexcept;

		/** This operation returns the number of octets that are
			unused at the end of the buffer managed by this encoder.
		 */
		unsigned		remaining() const noexcept;

		/** This operation is used to manually advance the encoding
			offset by the specified number of octets. It is intended
			to be used for creating nested encodings. See the comments
			for encodeSequenceIdentifier() and encodeConstructedIdentifier()
			for examples of its usage in this context.
		 */
		void	advance(unsigned amount) noexcept;

		/** This operation is used to append data to the buffer managed
			by the encoder. This may be used to copy perviously encoded
			values into the buffer, or to copy data for other nested
			encodings.
		 */
		void	append(const void* buffer,unsigned length) noexcept;

	private:
		/** Used by the implementation to encode a variable length
			length value according to BER.
		 */
		void		encodeLength(unsigned long) noexcept;

		/** Used by the implementation to encode a variable length
			unsigned integer value according to BER.
		 */
		void		encodeValue(unsigned long) noexcept;

		/** Returns the number of octets necessary to encode the
			specified unsigned integer value.
		 */
		unsigned	calcEncodedLength(unsigned long) noexcept;
	};

#endif
