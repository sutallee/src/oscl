/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "tx.h"

BerTxEncoder::BerTxEncoder(void* pdu,unsigned length) noexcept:
		_pdu((unsigned char*)pdu),
		_length(length),
		_offset(0),
		_overflow(false)
		{
	}

void	BerTxEncoder::encodeConstructedIdentifier(	BerClassType	type,
													unsigned char	tag
													) noexcept{
	_pdu[_offset]	= (((unsigned char)type) << 6) | (tag & 0x1F) | 0x20;
	_offset			+=	1;
	}

void	BerTxEncoder::encodeSequenceIdentifier() noexcept{
	_pdu[_offset]	=	0x30;
	_offset			+=	1;
	}

void	BerTxEncoder::encodeDefiniteTwoOctetLength(unsigned length) noexcept{
	_pdu[_offset]	= 0x82;
	_pdu[_offset+1]	= (unsigned char)((length >> 8) & 0x00FF);
	_pdu[_offset+2]	= (unsigned char)((length >> 0) & 0x00FF);
	_offset			+=	3;
	}

void		BerTxEncoder::encodeLength(unsigned long value) noexcept{
	const unsigned	bufferSize	= ((sizeof(value)*8)+6)/7;
	unsigned char	buffer[bufferSize];

	if(_offset+1 >= _length){
		_overflow	= true;
		return;
		}

	if(value == 0){
		_pdu[_offset]	= 0;
		_offset++;
		return;
		}

	for(unsigned i=0;i<bufferSize;++i){
		buffer[i]	= (unsigned char)((value >> (7*((bufferSize-1)-i))) & 0x0000007F);
		}

	bool	start	= false;
	for(unsigned i=0;i<(bufferSize-1);++i){
		if(buffer[i]){
			start	= true;
			}
		if(start){
			if(_offset >= _length){
				_overflow	= true;
				return;
				}
			_pdu[_offset]	= 0x80 | buffer[i];
			_offset			+= 1;
			}
		}
	if(_offset >= _length){
		_overflow	= true;
		return;
		}
	_pdu[_offset]	=	buffer[bufferSize-1];
	_offset			+=	1;
	return;
	}

void		BerTxEncoder::encodeValue(unsigned long value) noexcept{
	encodeLength(value);
	}

unsigned	BerTxEncoder::calcEncodedLength(unsigned long value) noexcept{
	const unsigned	bufferSize	= ((sizeof(value)*8)+6)/7;
	unsigned char	buffer[bufferSize];
	unsigned		length = 1;

	if(value == 0){
		return 1;
		}

	for(unsigned i=0;i<bufferSize;++i){
		buffer[i]	= (unsigned char)((value >> (7*i)) & 0x0000007F);
		}

	bool	start	= false;
	for(unsigned i=0;i<(bufferSize-1);++i){
		if(buffer[i]){
			start	= false;
			}
		if(start){
			length	+= 1;
			}
		}
	return length;
	}

void	BerTxEncoder::encodeINTEGER(signed long value) noexcept{
	const unsigned	nOctets	= sizeof(value);
	unsigned		len = 1;
	if(value < 0){
		// Do negative
		for(unsigned i=0;i<(nOctets);++i){
			unsigned char	v	= (unsigned char)(value >> (8*((nOctets-1)-i)));
			if((v & 0x80) == 0){
				len	+= nOctets-i;
				break;
				}
			else if(v != 0xFF){
				len	= nOctets-i;
				break;
				}
			}
		}
	else{
		// Do positive
		for(unsigned i=0;i<(nOctets);++i){
			unsigned char	v	= value >> (8*((nOctets-1)-i));
			if((v & 0x80) != 0){
				len	+= nOctets-i;
				break;
				}
			else if(v != 0){
				len = nOctets-i;
				break;
				}
			}
		}
	if((_offset+len+2) > _length){
		return;
		}
	_pdu[_offset]	= 0x02;
	_offset	+= 1;
	_pdu[_offset]	= (unsigned char)len;
	_offset	+= 1;
	for(unsigned i=0;i<len;++i){
		_pdu[_offset+(len-1-i)]	= (unsigned char)value;
		value	>>= 8;
		}
	_offset += len;
	}

void	BerTxEncoder::encodeSINTEGER(signed long value) noexcept{
	encodeINTEGER(value);
	}

void	BerTxEncoder::encodeUINTEGER(unsigned long value) noexcept{
	encodeSINTEGER((signed long)value);
	}

void	BerTxEncoder::encodeOID(const Oscl::ObjectID::RO::Api& oid) noexcept{
	Oscl::ObjectID::RO::Iterator	it(oid);
	unsigned char	first;
	unsigned		length;
	_pdu[_offset]	= 0x06;
	_offset++;
	if(!it.more()){
		encodeLength(0);	// advances _offset
		return;
		}
	first	= it.current() * 40;
	it.next();
	if(!it.more()){
		encodeLength(1);	// advances _offset
		_pdu[_offset]	= first;
		_offset			+= 1;
		return;
		}
	first	+= it.current();

	length	=	1;

	it.next();
	for(;it.more();it.next()){
		length	+= calcEncodedLength(it.current());
		}
	encodeLength(length);

	_pdu[_offset]	= first;
	_offset	+=	1;

	it.first();
	it.next();	// skip first subID
	it.next();	// skip second subID
	for(;it.more();it.next()){
		encodeValue(it.current());	// advances _offset
		}
	}

void	BerTxEncoder::encodeOCTETSTRING(const void* s,unsigned length) noexcept{
	const unsigned char*	src	= (const unsigned char*)s;
	_pdu[_offset]	=	0x04;
	_offset			+=	1;
	encodeLength(length);

	if(tooBig()){
		return;
		}

	if(length >= _length){
		_overflow	= true;
		return;
		}

	unsigned char*			dst = &_pdu[_offset];
	for(unsigned i=0;i<length;++i,++src,++dst){
		*dst	= *src;
		}
	_offset	+= length;
	}

unsigned char*	BerTxEncoder::currentBuffer() const noexcept{
	return &_pdu[_offset];
	}

unsigned		BerTxEncoder::length() const noexcept{
	return _offset;
	}

unsigned		BerTxEncoder::remaining() const noexcept{
	return _length-_offset;
	}

void	BerTxEncoder::advance(unsigned amount) noexcept{
	_offset	+= amount;
	}

void	BerTxEncoder::append(const void* buffer,unsigned length) noexcept{
	const unsigned char*	p	= (const unsigned char*)buffer;
	for(unsigned i=0;i<length;++i){
		_pdu[_offset]	= *p;
		++p;
		++_offset;
		}
	}

