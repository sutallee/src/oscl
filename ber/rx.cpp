/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "rx.h"

/////// BerExtendedUnsigned

BerExtendedUnsigned::BerExtendedUnsigned(const unsigned char* p,unsigned pLen) noexcept{
	unsigned long	v	= 0;
	for(unsigned i=0;(i<pLen) && (i<sizeof(unsigned long));++i,++p){
		v	<<= 7;
		v	|= (*p & 0x7F);
		if((*p & 0x80) == 0){
			_value	= v;
			_length	= i+1;
			return;
			}
		}
	_length	= 0;
	return;
	}

unsigned	BerExtendedUnsigned::getLength() const noexcept{	// zero if invalid or overflow
	return _length;
	}

unsigned	BerExtendedUnsigned::getValue() const noexcept{		// only valid if length is non-zero
	return _value;
	}

/////// BerOID

BerOID::BerOID(const void* content,unsigned len) noexcept:
	_index(0),
	_length(len),
	_enc((const unsigned char*)content),
	_err(false)
	{
	decodeCurrentSubID();
	}

void	BerOID::decodeCurrentSubID() noexcept{

	if(_index == 0){
		_currentSubIdValue	= _enc[0]/40;
		_currentSubIdLength	= 1;
		return;
		}

	if(_index == 1){
		_currentSubIdValue	= _enc[0]%40;
		_currentSubIdLength	= 1;
		return;
		}

	unsigned	offset=_index-1;
	if(offset >= _length){
		_currentSubIdLength	= 0;
		return;
		}

	BerExtendedUnsigned	xSubid(&_enc[offset],_length-offset);

	_currentSubIdLength	= xSubid.getLength();

	if(!_currentSubIdLength){
		_err		= true;
		return;
		}
	_currentSubIdValue	= xSubid.getValue();
	}

bool	BerOID::error() const noexcept{
	return _err;
	}

unsigned long	BerOID::current() const noexcept{
	return _currentSubIdValue;
	}

void	BerOID::first() noexcept{
	_index	= 0;
	_err	= false;
	decodeCurrentSubID();
	return ;
	}

void	BerOID::next() noexcept{
	if(_err) return;
	if(_currentSubIdLength == 0) return;
	_index	+= _currentSubIdLength;
	decodeCurrentSubID();
	}

bool	BerOID::more() const noexcept{
	return (_currentSubIdLength)?true:false;
	}

/////// BerRxEncoding

BerRxEncoding::BerRxEncoding(const void* p,unsigned bufferLength) noexcept:
		_pdu((const unsigned char*)p),
		_length(bufferLength),
		_offset(0)
		{
	}

unsigned long	BerRxEncoding::currentIdentifierTag() const noexcept{
	return _currentTag;
	}

unsigned long	BerRxEncoding::currentContentLength() const noexcept{	// returns content length
	return _currentLength;
	}

const void*		BerRxEncoding::currentContent() const noexcept{	// Returns pointer to content
	return &_pdu[_valueOffset];
	}

const void*		BerRxEncoding::currentEncoding() const noexcept{
	return &_pdu[_offset];
	}

unsigned		BerRxEncoding::currentSize() const noexcept{
	return _currentLength+(_valueOffset-_offset);
	}

bool			BerRxEncoding::nextEncoding() noexcept{	// Returns true if more encodings follow
	if(_currentLength == 0){
		return false;
		}
	_offset	= _valueOffset+_currentLength;
	if(_offset >= _length){
		return false;
		}
	else{
		return true;
		}
	}

bool	BerRxEncoding::currentTypeIsPrimitive() const noexcept{	// Otherwise constructed.
	return (_pdu[_offset] & 0x20) == 0x00;
	}

bool	BerRxEncoding::currentClassIsUniversal() const noexcept{
	return (_pdu[_offset] & 0xC0) == 0x00;
	}
bool	BerRxEncoding::currentClassIsApplication() const noexcept{
	return (_pdu[_offset] & 0xC0) == 0x40;
	}
bool	BerRxEncoding::currentClassIsContextSpecific() const noexcept{
	return (_pdu[_offset] & 0xC0) == 0x80;
	}
bool	BerRxEncoding::currentClassIsContextPrivate() const noexcept{
	return (_pdu[_offset] & 0xC0) == 0xC0;
	}
bool	BerRxEncoding::currentIsINTEGER() const noexcept{
	return _pdu[_offset] == 0x02;
	}
bool	BerRxEncoding::currentIsOCTETSTRING() const noexcept{
	return _pdu[_offset] == 0x04;
	}

bool	BerRxEncoding::currentIsOID() const noexcept{
	return _pdu[_offset] == 0x06;
	}

bool	BerRxEncoding::currentIsSEQUENCE() const noexcept{
	return _pdu[_offset] == 0x30;
	}

bool	BerRxEncoding::currentIsEndOfContents() const noexcept{
	return _pdu[_offset] == 0x00;
	}

unsigned	BerRxEncoding::decodeOCTETSTRING(void* dest,unsigned maxLength) const noexcept{
	unsigned	length	= (maxLength < _currentLength)?maxLength:_currentLength;
	memcpy(dest,&_pdu[_valueOffset],length);
	return length;
	}

signed long	BerRxEncoding::decodeINTEGER() const noexcept{
	const unsigned char*	p	= &_pdu[_valueOffset];
	signed long				value;
	if(_currentLength){
		// Sign extension
		if(*p & 0x80){
			value	= -1;
			}
		else {
			value	= 0;
			}
		}
	else{
		value	= 0;
		}
	for(unsigned i=0;i<_currentLength;++i,++p){
		value <<= 8;
		value	|= *p;
		}
	return value;
	}

signed long	BerRxEncoding::decodeSINTEGER() const noexcept{
	return decodeINTEGER();
	}

unsigned long	BerRxEncoding::decodeUINTEGER() const noexcept{
	return (unsigned long)decodeSINTEGER();
	}

void	BerRxEncoding::decodeOID(Oscl::ObjectID::Api& oid) const noexcept{
	BerOID	berOID(currentContent(),currentContentLength());
	oid	= "";
	for(unsigned i=0;berOID.more();berOID.next(),++i){
		oid	+= berOID.current();
		}
	}
unsigned BerRxEncoding::decodeCurrentTag() noexcept{
	unsigned long	tag;
	unsigned		index	= _offset;
	unsigned		len;
	unsigned		tagLen;

	if(index >= _length){
		return 0;
		}
	len		= _length - index;

	tag	= (_pdu[index] & 0x1F);

	if(tag != 0x1F){
		// Easy short tag
		_currentTag	= tag;
		return 1;
		}

	index	+= 1;
	len		-= 1;

	BerExtendedUnsigned	xTag(&_pdu[index],len);
	tagLen	= xTag.getLength();

	if(tagLen == 0){
		return 0;
		}

	_currentTag	= xTag.getValue();

	return tagLen;
}

unsigned BerRxEncoding::decodeCurrentLength() noexcept{
	unsigned long	contentLength;
	unsigned		index	= _lengthOffset;
	unsigned		len;

	if(index >= _length){
		return false;
		}
	len		= _length - index;

	contentLength	= _pdu[index];

	if(contentLength == 0xFF){
		// Reserved for extension
		return 0;
		}

	if(!(contentLength & 0x80)){
		// Easy short form
		_currentLength	= contentLength;
		return 1;
		}

	contentLength	&= ~0x80;

	if(contentLength == 0){
		// Indefinite form
		_valueOffset	= _lengthOffset+1;
		contentLength	= lengthOfIndefinite();
		_currentLength	= contentLength;
		if(!contentLength){
			return 0;
			}
		return 1;
		}

	index	+= 1;
	len		-= 1;

	if(len < contentLength){
		// Extends beyond buffer
		return 0;
		}

	if(contentLength > sizeof(unsigned long)){
		// Won't fit into unsigned long
		// System limitation
		return 0;
		}

	len				= contentLength;
	contentLength	= 0;

	const unsigned char*	p	= &_pdu[index];
	for(unsigned i=0; (i<len);++i,++p){
		contentLength	<<=	8;
		contentLength	|=	*p;
		}

	_currentLength	= contentLength;

	return len+1;
	}

unsigned		BerRxEncoding::lengthOfIndefinite() const noexcept{
	BerRxEncoding	enc(currentContent(),_length-_valueOffset);

	do{
		if(!enc.encodingIsValid()){
			return 0;
			}
		if(enc.currentIsEndOfContents()){
			return enc._valueOffset;
			}
		}while(enc.nextEncoding());

	return 0;
	}

bool	BerRxEncoding::encodingIsValid() noexcept{
	unsigned	len;
	if(_offset >= _length){
		return false;
		}
	len	= decodeCurrentTag();
	if(len == 0){
		return false;
		}

	_lengthOffset	= _offset+len;

	len	= decodeCurrentLength();
	if(len == 0){
		return false;
		}

	_valueOffset	= _lengthOffset+len;

	return true;
	}

