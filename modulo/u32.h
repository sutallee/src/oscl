/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_modulo_u32h_
#define _oscl_modulo_u32h_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Modulo {

/** */
class U32 {
	private:
		/** */
		uint32_t	_value;

	public:
		/** */
		inline U32() noexcept
				{
			}
		/** */
		inline U32(uint32_t value) noexcept:
				_value(value)
				{
			}
		/** */
		inline U32(const U32& other) noexcept:
				_value(other._value)
				{
			}

		/** */
		inline uint32_t	getValue() const noexcept{
			return _value;
			}

		/** */
//		inline operator uint32_t() const noexcept{
//			return _value;
//			}
		// Asignment operators
		/** */
		inline U32	operator=(uint32_t value) noexcept{
			_value	= value;
			return _value;
			}
		/** */
		inline U32	operator+=(uint32_t value) noexcept{
			_value	+= value;
			return _value;
			}
		/** */
		inline U32	operator+=(const U32& other) noexcept{
			_value	+= other._value;
			return _value;
			}
		/** */
		inline U32	operator-=(uint32_t value) noexcept{
			_value	-= value;
			return _value;
			}
		/** */
		inline U32	operator-=(const U32& other) noexcept{
			_value	-= other._value;
			return _value;
			}
		// Increment/Decrement operators
		/** prefix increment */
		inline U32	operator++() noexcept{
			return ++_value;
			}
		/** postfix increment */
		inline U32	operator++(int dummy) noexcept{
			return _value++;
			}
		/** prefix decrement */
		inline U32	operator--() noexcept{
			return --_value;
			}
		/** postfix decrement */
		inline U32	operator--(int dummy) noexcept{
			return _value--;
			}
		/** */
		inline U32	operator~() noexcept{
			_value	= ~_value;
			return _value;
			}
		// Comparison operators
		/** */
		inline bool	operator!() const noexcept{
			return !_value;
			}
		/** */
		inline bool	operator == (const U32& other) const noexcept{
			return _value == other._value;
			}
		/** */
		inline bool	operator == (unsigned long value) const noexcept{
			return _value == value;
			}
		/** */
		inline bool	operator != (const U32& other) const noexcept{
			return _value != other._value;
			}
		/** */
		inline bool	operator != (unsigned long value) const noexcept{
			return _value != value;
			}
		/** */
		inline bool	operator < (const U32& other) const noexcept{
			if(_value == other._value){
				return false;
				}
			if(_value < other._value){
				uint32_t	diff	= _value-other._value;
				if(diff > 0x80000000){
					return true;
					}
				}
			else {
				uint32_t	diff	= _value-other._value;
				if(diff > 0x80000000){
					return true;
					}
				}
			return false;
			}
		/** */
		inline bool	operator <= (const U32& other) const noexcept{
			if(_value == other._value){
				return true;
				}
			return operator < (other);
			}
		/** */
		inline bool	operator > (const U32& other) const noexcept{
			if(_value == other._value){
				return false;
				}
			if(_value > other._value){
				uint32_t	diff	= _value-other._value;
				if(diff < 0x80000000){
					return true;
					}
				}
			else {
				uint32_t	diff	= _value-other._value;
				if(diff < 0x80000000){
					return true;
					}
				}
			return false;
			}
		/** */
		inline bool	operator >= (const U32& other) const noexcept{
			if(_value == other._value){
				return true;
				}
			return operator > (other);
			}
	};

inline U32	operator + (U32 a,unsigned long b) {
	U32	r	= a;
	return r+= b;
	}

inline U32	operator - (U32 a,unsigned long b) {
	U32	r	= a;
	return r-= b;
	}

inline U32	operator + (U32 a,U32 b) {
	U32	r	= a;
	return r+= b;
	}

inline unsigned long	operator - (U32 a,U32 b) {
	unsigned long	result	= a.getValue()-b.getValue();
	if(result >= 0x80000000){
		result	= 0-result;
		}
	return result;
	}

inline bool	operator == (U32 a,unsigned long b) {
	return a.getValue() == b;
	}

inline bool	operator == (unsigned long a,U32 b) {
	return a == b.getValue();
	}

inline bool	operator != (U32 a,unsigned long b) {
	return a.getValue() != b;
	}

inline bool	operator != (unsigned long a,U32 b) {
	return a != b.getValue();
	}
}
}

#endif
