/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_rgb_frame_apih_
#define _oscl_rgb_frame_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace RGB {

/** */
namespace Frame {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	Update the frame based on the specified pixels. Pixels
			may be ARGB32 or RGBA32 encoded depending upon the
			implementation. Implementations *may* ignore the Alpha field.
		 */
		virtual void	update(	
							const uint32_t*	pixels,
							unsigned int	nPixels
							) noexcept=0;

	};

}
}
}
#endif
