/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/strings/fixed.h"
#include "oscl/posix/errno/errstring.h"

using namespace Oscl::LinuxOS::SysFs::PWM;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	const char*						pwmDirPath,
	unsigned						periodInNs
	) noexcept:
		_papi(papi),
		_pwmDirPath(pwmDirPath),
		_periodInNs(periodInNs),
		_reqComp(
			*this,
			&Part::request
			),
		_sync(
			papi,
			_reqComp
			)
		{
	}

void	Part::start() noexcept{

	Oscl::Posix::Errno::ErrString	err;

	Oscl::Strings::Fixed<1024>	path(_pwmDirPath);

	path	+= "duty_cycle";
	
	_duty_cycle_fd	= open(path.getString(),O_WRONLY);
	if(_duty_cycle_fd < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't open \"%s\", \"%s\"\n",
			__PRETTY_FUNCTION__,
			path.getString(),
			err.value()
			);
		return;
		}

	path	= _pwmDirPath;
	path	+= "period";

	int
	periodFD		= open(path.getString(),O_WRONLY);
	if(periodFD < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't open \"%s\", \"%s\"\n",
			__PRETTY_FUNCTION__,
			path.getString(),
			err.value()
			);
		return;
		}

	path	= _pwmDirPath;
	path	+= "enable";

	int
	enableFD		= open(path.getString(),O_WRONLY);
	if(enableFD < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't open \"%s\", \"%s\"\n",
			__PRETTY_FUNCTION__,
			path.getString(),
			err.value()
			);
		return;
		}

	Oscl::Strings::Fixed<1024>
	period(
		"%u",
		_periodInNs
		);

	ssize_t
	n	= pwrite(periodFD,period.getString(),period.length(),0);
	if(n < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't write \"period\" file, \"%s\"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	static const char	initialPulseWidth[] = {"0"};

	n	= pwrite(_duty_cycle_fd,initialPulseWidth,sizeof(initialPulseWidth)-1,0);
	if(n < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't write \"duty_cycle\" file, \"%s\"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	static const char	enableStr[] = {"1"};

	n	= pwrite(enableFD,enableStr,sizeof(enableStr)-1,0);
	if(n < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't write \"enable\" file, \"%s\"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}
	}

Oscl::PWM::Discrete::Api&	Part::getSyncApi() noexcept{
	return _sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sync.getSAP();
	}

void	Part::updatePulseWidth(unsigned count) noexcept{

	Oscl::Strings::Fixed<32>
	countStr(
		"%u",
		count
		);

#if 1
	ssize_t
	n	= pwrite(
			_duty_cycle_fd,
			countStr.getString(),
			countStr.length(),
			0	// offset
			);

	if(n < 0){
		Oscl::Posix::Errno::ErrString	err;
		Oscl::ErrorFatal::logAndExit(
			"%s: Can't write \"duty_cycle\" file, \"%s\"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}
#else
	Oscl::Error::Info::log(
		"%s: count: %u (\"%s\")\n",
		__PRETTY_FUNCTION__,
		count,
		countStr.getString()
		);
#endif

	}

void	Part::request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{
	Oscl::PWM::Discrete::Req::Api::SetPulseWidthPayload&
	payload	= msg.getPayload();

	updatePulseWidth(payload._count);

	msg.returnToSender();
	}

