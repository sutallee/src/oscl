/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_linux_sysfs_pwm_parth_
#define _oscl_linux_sysfs_pwm_parth_

#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/pwm/discrete/sync.h"
#include "oscl/pwm/discrete/reqcomp.h"

/** */
namespace Oscl {
/** */
namespace LinuxOS {
/** */
namespace SysFs {
/** */
namespace PWM {

/** */
class Part {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		/** */
		const char*	const								_pwmDirPath;

		/** */
		const unsigned									_periodInNs;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_reqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_sync;

	private:
		/** */
		int												_duty_cycle_fd;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			const char*					pwmDirPath,
			unsigned					periodInNs
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getSyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	updatePulseWidth(unsigned count) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _reqComp
		/** */
		void	request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	};

}
}
}
}

#endif
