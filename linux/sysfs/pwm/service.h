/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_linux_sysfs_pwm_serviceh_
#define _oscl_linux_sysfs_pwm_serviceh_

#include "part.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace LinuxOS {
/** */
namespace SysFs {
/** */
namespace PWM {

/** */
class Service : private Oscl::Mt::Itc::Srv::Close::Req::Api {
	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync		_closeSync;

		/** */
		Oscl::LinuxOS::SysFs::PWM::Part		_part;

	public:
		/** */
		Service(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			const char*					pwmDirPath,
			unsigned					periodInNs
			) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::CloseSyncApi&	getCloseSyncApi() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    getOpenSAP() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   getCloseSAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getSyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getSAP() noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept;

	};

}
}
}
}

#endif
