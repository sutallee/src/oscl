/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_linux_i2c_master_parth_
#define _oscl_linux_i2c_master_parth_

#include <stdint.h>
#include "oscl/posix/select/api.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/i2c/master/sevenbit/itc/reqapi.h"
#include "oscl/i2c/master/sevenbit/itc/sync.h"
#include "oscl/queue/queue.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace LinuxOS {
/** */
namespace I2C {
/** */
namespace Master {

/** */
class Part :
		private Oscl::I2C::Master::SevenBit::Req::Api
		{
	private:
		/** */
		Oscl::Posix::Select::Api&							_posixSelectApi;

		/** */
		const char*											_deviceName;

	private:
		/** */
		Oscl::I2C::Master::SevenBit::Sync					_sync;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::ReadReq*		_readMsg;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::WriteReq*	_writeMsg;

		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>					_pending;

		/** */
		unsigned char*										_currentData;

		/** */
		unsigned 											_currentRemaining;

		/** */
		bool												_read;

		/** */
		unsigned long										_count;

	private:
		/** */
		int													_fd;

		/** */
		uint8_t												_address;

	private:
		/** */
		Oscl::Done::Api*									_stopDone;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Posix::Select::Api&	posixSelectApi,
			const char*					deviceName
			) noexcept;

		/** */
		Oscl::I2C::Master::SevenBit::
		Req::Api::SAP&							getSevenBitSAP() noexcept;

		/** */
		Oscl::I2C::Master::SevenBit::Api&		getSevenBitSyncApi() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop(Oscl::Done::Api& done) noexcept;

	private:
		/** */
		bool	busy() noexcept;

		/** */
		void	processNext() noexcept;

		/** */
		bool	selectDevice(uint8_t address) noexcept;

		/** */
		void	startWrite() noexcept;

		/** */
		void	startRead() noexcept;

	private: // Oscl::I2C::Master::SevenBit::Req::Api
		/** */
		void	request(Oscl::I2C::Master::SevenBit::Req::Api::ReadReq& msg) noexcept;
		/** */
		void	request(Oscl::I2C::Master::SevenBit::Req::Api::CancelReadReq& msg) noexcept;
		/** */
		void	request(Oscl::I2C::Master::SevenBit::Req::Api::WriteReq& msg) noexcept;
		/** */
		void	request(Oscl::I2C::Master::SevenBit::Req::Api::CancelWriteReq& msg) noexcept;
	};

}
}
}
}

#endif
