/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_linux_i2c_master_serviceh_
#define _oscl_linux_i2c_master_serviceh_

#include "part.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/i2c/master/sevenbit/itc/reqapi.h"
#include "oscl/i2c/master/sevenbit/itc/sync.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace LinuxOS {
/** */
namespace I2C {
/** */
namespace Master {

/** */
class Service :
		public Oscl::Mt::Itc::Srv::Close::Req::Api
		{
	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync	_closeSync;

		/** */
		Part							_part;

	private:
		/** */
		Oscl::Done::Operation<Service>	_closeDone;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

	public:
		/** */
		Service(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Posix::Select::Api&	posixSelectApi,
			const char*					deviceName
			) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::CloseSyncApi&	getCloseSyncApi() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    getOpenSAP() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   getCloseSAP() noexcept;

		/** */
		Oscl::I2C::Master::SevenBit::
		Req::Api::SAP&						getSevenBitSAP() noexcept;

		/** */
		Oscl::I2C::Master::SevenBit::Api&	getSevenBitSyncApi() noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept;

	private:	// Oscl::Done::Operation<Service> _closeDone
		/** */
		void	closeDone() noexcept;

	};

}
}
}
}

#endif
