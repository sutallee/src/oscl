/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <unistd.h>
#include <errno.h>
#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/posix/errno/errstring.h"

using namespace Oscl::LinuxOS::I2C::Master;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Posix::Select::Api&	posixSelectApi,
	const char*					deviceName
	) noexcept:
		_posixSelectApi(posixSelectApi),
		_deviceName(deviceName),
		_sync(
			papi,
			*this
			),
		_readMsg(0),
		_writeMsg(0),
		_currentData(0),
		_currentRemaining(0),
		_read(true),
		_count(0),
		_fd(-1),
		_address(0xFF),
		_stopDone(0)
		{
	}

void Part::start() noexcept{

	_fd	= open(
			_deviceName,
			O_RDWR
			);

	if(_fd < 0){
		Oscl::Posix::Errno::ErrString	err;
		Oscl::ErrorFatal::logAndExit(
			"%s: open failed, \"%s\"!\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}
	}

void	Part::stop(Oscl::Done::Api& done) noexcept{
	if(_stopDone){
		Oscl::ErrorFatal::logAndExit(
			"%s: Protocol Error, already have stop pending!\n",
			__PRETTY_FUNCTION__
			);
		}

	_stopDone	= &done;

	// FIXME: implement correctly
	Oscl::Done::Api*
	stopDone	= _stopDone;
	_stopDone	= 0;
	stopDone->done();
	}

Oscl::I2C::Master::SevenBit::Req::Api::SAP& Part::getSevenBitSAP() noexcept{
	return _sync.getSAP();
	}

Oscl::I2C::Master::SevenBit::Api&	Part::getSevenBitSyncApi() noexcept{
	return _sync;
	}

bool	Part::busy() noexcept{
	return (_writeMsg || _readMsg);
	}

void	Part::processNext() noexcept{
	if(busy()){
		return;
		}

	Oscl::Mt::Itc::SrvMsg*
	msg	= _pending.get();

	if(!msg){
		return;
		}

	msg->process();
	}

bool	Part::selectDevice(uint8_t address) noexcept{

	if(address == _address){
		return false;
		}

#if 1
	if(address >= 0x80){
		int
		result	= ioctl(
					_fd,
					I2C_TENBIT
					);
		if (result < 0) {
			int	error	= errno;
			Oscl::Posix::Errno::ErrString	err;
			Oscl::Error::Info::log(
				"%s: ioctl (I2C_TENBIT,address: %u) failed, %d \"%s\"!\n",
				__PRETTY_FUNCTION__,
				address,
				error,
				err.value()
				);
		return true;
		}
		}
#endif

	int
	result	= ioctl(
				_fd,
				I2C_SLAVE,
				address
				);

	if (result < 0) {
		int	error	= errno;
		Oscl::Posix::Errno::ErrString	err;
		Oscl::Error::Info::log(
			"%s: ioctl (address: %u) failed, %d \"%s\"!\n",
			__PRETTY_FUNCTION__,
			address,
			error,
			err.value()
			);
		return true;
		}

	_address	= address;

	return false;
	}

void	Part::startWrite() noexcept{

	Oscl::I2C::Master::SevenBit::Req::Api::WritePayload&
	payload	= _writeMsg->getPayload();

	bool
	failed	= selectDevice(payload._address);
	if(failed){
		payload._failed	= true;
		_writeMsg->returnToSender();
		return;
		}

	errno	= 0;

	ssize_t
	result	= ::write(
				_fd,
				payload._data,
				payload._nDataBytes
				);
	if (result < 0) {
		int	error	= errno;
		Oscl::Posix::Errno::ErrString	err;
		Oscl::Error::Info::log(
			"%s: write(%d) failed, %d \"%s\"!\n",
			__PRETTY_FUNCTION__,
			_fd,
			error,
			err.value(error)
			);
		payload._failed	= true;
		_writeMsg->returnToSender();
		return;
		}

	payload._failed	= false;
	_writeMsg->returnToSender();

	_writeMsg	= 0;

	processNext();
	}

void	Part::startRead() noexcept{

	Oscl::I2C::Master::SevenBit::Req::Api::ReadPayload&
	payload	= _readMsg->getPayload();

	bool
	failed	= selectDevice(payload._address);
	if(failed){
		payload._failed	= true;
		_readMsg->returnToSender();
		return;
		}

	ssize_t
	result	= read(
				_fd,
				payload._dataBuffer,
				payload._nDataBytesToRead
				);
	if (result < 0) {
		Oscl::Posix::Errno::ErrString	err;
		Oscl::Error::Info::log(
			"%s: read failed, \"%s\"!\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		payload._failed	= true;
		_readMsg->returnToSender();
		return;
		}

	payload._failed	= false;
	_readMsg->returnToSender();

	_readMsg	= 0;

	processNext();
	}

void	Part::request(Oscl::I2C::Master::SevenBit::Req::Api::ReadReq& msg) noexcept{
	if(busy()){
		_pending.put(&msg);
		return;
		}

	_readMsg	= &msg;

	startRead();
	}

void	Part::request(Oscl::I2C::Master::SevenBit::Req::Api::CancelReadReq& msg) noexcept{
	}

void	Part::request(Oscl::I2C::Master::SevenBit::Req::Api::WriteReq& msg) noexcept{
	if(busy()){
		_pending.put(&msg);
		return;
		}

	_writeMsg	= &msg;

	startWrite();
	}

void	Part::request(Oscl::I2C::Master::SevenBit::Req::Api::CancelWriteReq& msg) noexcept{
	}
