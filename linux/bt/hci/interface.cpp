/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "interface.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/strings/fixed.h"
#include "oscl/posix/errno/errstring.h"

#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#include <stdlib.h>
#include <sys/ioctl.h>

#if 0
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#endif

using namespace Oscl::LinuxOS::BT::HCI;

Interface::Interface(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Posix::Select::Api&	posixSelectApi,
	int							devID
	) noexcept:
		_papi(papi),
		_posixSelectApi(posixSelectApi),
		_devID(devID),
		_readHandler(
			*this,
			&Interface::readStart,
			&Interface::readStop,
			&Interface::readHandleIO,
			&Interface::fdIsClosed
			),
		_fd(-1)
		{
	}

void	Interface::start() noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);

	Oscl::Posix::Errno::ErrString	err;

	_fd	= socket(
			AF_BLUETOOTH,
			SOCK_RAW,
			BTPROTO_HCI
			);

	if(_fd < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket() \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	struct sockaddr_hci	a;
	a.hci_family	= AF_BLUETOOTH;
	a.hci_dev		= _devID;

	int
	result	= bind(_fd,(struct sockaddr*)&a,sizeof(a));

	if(result < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: bind() \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	struct hci_filter	filter;

	hci_filter_clear(&filter);
	hci_filter_all_ptypes(&filter);
	hci_filter_all_events(&filter);

	result	= setsockopt(
				_fd,
				SOL_HCI,
				HCI_FILTER,
				&filter,
				sizeof(filter)
				);

	if(result < 0){
		int	error	= errno;
		Oscl::ErrorFatal::logAndExit(
			"%s: setsockopt(HCI_FILTER) errno: %d \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			error,
			err.value()
			);
		}

	_posixSelectApi.attach(_readHandler);
	Oscl::Error::Info::log("%s: TRACE OUT\n",__PRETTY_FUNCTION__);
	}

void	Interface::parse(unsigned nRead) noexcept{
	Oscl::Error::Info::log(
		"%s: Read %u octets.\n",
		__PRETTY_FUNCTION__,
		nRead
		);
	Oscl::Error::Info::hexDump(
		_buffer,
		nRead
		);
	}

void	Interface::readStart(Oscl::Posix::Select::FileDescSets& next) noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);
	FD_SET(_fd, &next._readfds);
	FD_SET(_fd, &next._exceptfds);
	}

void	Interface::readStop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);
	FD_CLR(_fd, &next._readfds);
	FD_CLR(_fd, &next._exceptfds);
#if 0
	if(_doneApi){
		_doneApi->done();
		}
#endif
	Oscl::Error::Info::log(
		"%s: Unexpected!\n",
		__PRETTY_FUNCTION__
		);
	}

bool	Interface::readHandleIO(
			const Oscl::Posix::Select::FileDescSets&	pending,
			Oscl::Posix::Select::FileDescSets&			next,
			time_t&										timestamp
			) noexcept{
	if(!FD_ISSET(_fd, &pending._readfds)){
		if(FD_ISSET(_fd, &pending._exceptfds)){
			Oscl::ErrorFatal::logAndExit(
				"%s: FD_ISSET(pending._exceptfds)"
				"\n",
				__PRETTY_FUNCTION__
				);
#if 0
			_doneApi	= &_failed;
#endif
			return true;
			}
		return false;
		}

	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);

	ssize_t
	nRead	= ::read(_fd,_buffer,sizeof(_buffer));

	if(nRead < 0 && (errno != EAGAIN)){
#if 0
		_doneApi	= &_failed;
#endif
		Oscl::Posix::Errno::ErrString	err;

		Oscl::ErrorFatal::logAndExit(
			"%s: read, \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		return true;
		}

	if(nRead < 0){
		// EAGAIN
		return false;
		}

	if(!nRead){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket closed!"
			"\n",
			__PRETTY_FUNCTION__
			);
#if 0
		_doneApi	= &_failed;
#endif
		return true;
		}

	parse(nRead);

	return false;
	}

bool    Interface::fdIsClosed() noexcept{
	fd_set			fds;

	FD_ZERO(&fds);
	FD_SET(_fd,&fds);

	struct timeval	timeout;
	timeout.tv_sec	= 0;
	timeout.tv_usec	= 0;

	int	result	= select(FD_SETSIZE,&fds,0,0, &timeout);

	if(result < 0){
		if(errno == EBADF){
#if 0
			_doneApi	= &_failed;
#endif
			return true;
			}
		}

	return false;
	}

