/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/strings/fixed.h"
#include "oscl/posix/errno/errstring.h"

#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#include <stdlib.h>
#include <sys/ioctl.h>

#if 0
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#endif

#include "monitor.h"

#include "oscl/decoder/le/base.h"
#include "oscl/bluetooth/debug/print/parse.h"

using namespace Oscl::LinuxOS::BT::HCI;

Monitor::Monitor(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Posix::Select::Api&	posixSelectApi
	) noexcept:
		_papi(papi),
		_posixSelectApi(posixSelectApi),
		_readHandler(
			*this,
			&Monitor::readStart,
			&Monitor::readStop,
			&Monitor::readHandleIO,
			&Monitor::fdIsClosed
			),
		_fd(-1)
		{
	}

void	Monitor::start() noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);

	Oscl::Posix::Errno::ErrString	err;

	_fd	= socket(
			AF_BLUETOOTH,
			SOCK_RAW,
			BTPROTO_HCI
			);

	if(_fd < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket() \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

#if 0
	int	hci_data_dir	= 1;	// WTF?
	int
	result	= setsockopt(
				fd,
				SOL_HCI,
				HCI_DATA_DIR,
				&hci_data_dir,
				sizeof(hci_data_dir)
				);

	if(result < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: setsockopt(SOL_HCI,HCI_DATA_DIR) \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	int	hci_time_stamp	= 1;

	result	= setsockopt(
				fd,
				SOL_HCI, HCI_TIME_STAMP,
				&hci_time_stamp,
				sizeof(hci_time_stamp)
				);

	if(result < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: setsockopt(SOL_HCI,HCI_TIME_STAMP) \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	struct hci_filter	filter;

	hci_filter_clear(&filter);
	hci_filter_all_ptypes(&filter);
	hci_filter_all_events(&filter);

	_posixSelectApi.attach(_readHandler);
	Oscl::Error::Info::log("%s: TRACE OUT\n",__PRETTY_FUNCTION__);

	constexpr unsigned	maxHciDevices	= 3;

	struct hci_dev_list_req*	dl	= 0;
	struct hci_dev_req*			dr	= 0;

	dl = (struct hci_dev_list_req*)malloc(maxHciDevices * sizeof(*dr) + sizeof(*dl));

	memset(dl, 0, maxHciDevices * sizeof(*dr) + sizeof(*dl));

	dl->dev_num = maxHciDevices;
	dr = dl->dev_req;

	result	= ioctl(fd, HCIGETDEVLIST, (void *) dl);

	if(result < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: ioctl(HCIGETDEVLIST) \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	for(unsigned i=0;i<dl->dev_num;++i){
		Oscl::Error::Info::log(
			"%p: dev_opt: 0x%2.2X, device ID: %u\n",
			&dr[i],
			dr[i].dev_opt,
			dr[i].dev_id
			);
		Interface*
		hci	= new Interface(
					_papi,
					_posixSelectApi,
					dr[i].dev_id
					);
		hci->start();
		}
#endif

	struct sockaddr_hci	a;
    a.hci_family	= AF_BLUETOOTH;
    a.hci_dev		= HCI_DEV_NONE;
    a.hci_channel	= HCI_CHANNEL_MONITOR;

	int
	result	= bind(_fd,(struct sockaddr*)&a,sizeof(a));

	if(result < 0){
		int	error	= errno;
		Oscl::ErrorFatal::logAndExit(
			"%s: bind() errno: %d, \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			error,
			err.value()
			);
		}

	_posixSelectApi.attach(_readHandler);
	Oscl::Error::Info::log("%s: TRACE OUT\n",__PRETTY_FUNCTION__);
	}

void	Monitor::parse(unsigned nRead) noexcept{
	Oscl::Bluetooth::Debug::Print::parseMonitorPacket(
		_buffer,
		nRead
		);
	}

void	Monitor::readStart(Oscl::Posix::Select::FileDescSets& next) noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);
	FD_SET(_fd, &next._readfds);
	FD_SET(_fd, &next._exceptfds);
	}

void	Monitor::readStop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);
	FD_CLR(_fd, &next._readfds);
	FD_CLR(_fd, &next._exceptfds);
#if 0
	if(_doneApi){
		_doneApi->done();
		}
#endif
	Oscl::Error::Info::log(
		"%s: Unexpected!\n",
		__PRETTY_FUNCTION__
		);
	}

bool	Monitor::readHandleIO(
			const Oscl::Posix::Select::FileDescSets&	pending,
			Oscl::Posix::Select::FileDescSets&			next,
			time_t&										timestamp
			) noexcept{
	if(!FD_ISSET(_fd, &pending._readfds)){
		if(FD_ISSET(_fd, &pending._exceptfds)){
			Oscl::ErrorFatal::logAndExit(
				"%s: FD_ISSET(pending._exceptfds)"
				"\n",
				__PRETTY_FUNCTION__
				);
#if 0
			_doneApi	= &_failed;
#endif
			return true;
			}
		return false;
		}

#if 0
	Oscl::Error::Info::log("%s\n",__PRETTY_FUNCTION__);
#endif

	ssize_t
	nRead	= ::read(_fd,_buffer,sizeof(_buffer));

	if(nRead < 0 && (errno != EAGAIN)){
#if 0
		_doneApi	= &_failed;
#endif
		Oscl::Posix::Errno::ErrString	err;

		Oscl::ErrorFatal::logAndExit(
			"%s: read, \"%s\""
			"\n",
			__PRETTY_FUNCTION__,
			err.value()
			);
		return true;
		}

	if(nRead < 0){
		// EAGAIN
		return false;
		}

	if(!nRead){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket closed!"
			"\n",
			__PRETTY_FUNCTION__
			);
#if 0
		_doneApi	= &_failed;
#endif
		return true;
		}

	parse(nRead);

	return false;
	}

bool    Monitor::fdIsClosed() noexcept{
	fd_set			fds;

	FD_ZERO(&fds);
	FD_SET(_fd,&fds);

	struct timeval	timeout;
	timeout.tv_sec	= 0;
	timeout.tv_usec	= 0;

	int	result	= select(FD_SETSIZE,&fds,0,0, &timeout);

	if(result < 0){
		if(errno == EBADF){
#if 0
			_doneApi	= &_failed;
#endif
			return true;
			}
		}

	return false;
	}

