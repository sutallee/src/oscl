/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_linux_bt_hci_interfaceh_
#define _oscl_linux_bt_hci_interfaceh_

#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/posix/select/api.h"
#include "oscl/posix/select/trans/read.h"
#include "oscl/posix/select/handler.h"

/** */
namespace Oscl {
/** */
namespace LinuxOS {
/** */
namespace BT {
/** */
namespace HCI {

/** */
class Interface {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_papi;

		/** */
		Oscl::Posix::Select::Api&		_posixSelectApi;

		/** */
		const int						_devID;

	private:
		/** */
		Oscl::Posix::Select::
		HandlerComposer<Interface>		_readHandler;

	private:
		/** */
		unsigned char					_buffer[1024];

		/** */
		int								_fd;

	public:
		/** */
		Interface(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Posix::Select::Api&	posixSelectApi,
			int							devID
			) noexcept;

		/** */
		void	start() noexcept;

	private:
		/** */
		void	parse(unsigned nRead) noexcept;

	private: // Oscl::Posix::Select::Handler
		/**	*/
		void	readStart(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	*/
		void	readStop(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	*/
		bool	readHandleIO(
					const Oscl::Posix::Select::FileDescSets&	pending,
					Oscl::Posix::Select::FileDescSets&			next,
					time_t&										timestamp
					) noexcept;

		/**	*/
		bool	fdIsClosed() noexcept;


	};

}
}
}
}

#endif
