#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <alloca.h>
#include <errno.h>
#include "oscl/middleman/app.h"
#include "oscl/posix/mkdir.h"

//using namespace Oscl::Linux::MiddleMan;

static void	printHelp(const char* progName) {
	printf(	"\nUsage: %s [options]\n\n"
			" Launches an application that operates in the middle of a tty\n"
			" data stream, allowing access to the data flowing in either\n"
			" direction between the application and the tty.\n"
			"\n"
			" Two stream server sockets (AF_UNIX or optionally AF_INET,)\n"
			" one for each direction (east and west), allow external\n"
			" applications access to the data streams. Access includes\n"
			" receiving a copy of any data comming from the endpoint\n"
			" (east or west), and the ability to inject data into that\n"
			" same endpoint.\n"
			"\n"
			" -r socket_directory_root\n"
			"    Specifies the path to the root directory that will contain\n"
			"    the AF_UNIX socket directories. The default is \"/tmp\".\n"
			" -u socket_directory\n"
			"    Specifies the name of a directory created in the root socket\n"
			"    directory that will contain the AF_UNIX socket files. The\n"
			"    default the name of the directory is \"sockets\". This name\n"
			"    is appended to the root socket directory specified by the\n"
			"    \"-p\" option.\n."
			" -e east_socket_name\n"
			"    Specifies the name of an AF_UNIX socket file for the east tap.\n"
			"    The name of the file is \"east\" by default.\n"
			" -w west_socket_name\n"
			"    Specifies the name of an AF_UNIX socket file for the west tap.\n"
			"    The name of the file is \"west\" by default.\n"
			" -t tty_path\n"
			"    Specifies the name of a tty device to be opened as the west tty.\n"
			"    This is typically a \"/dev/tts/\" device file for a serial port.\n"
			"    Left unspecified standard in and standard out are used, although\n"
			"    such a default is usually only used for testing.\n"
			" -c command\n"
			"    Specifies the fully qualified path to an executable that will\n"
			"    be forked and executed with its standard input and output being\n"
			"    the east pseudo-tty created and monitored by this application.\n"
			" -a east_port_number\n"
			"    Specifies an AF_INET port number to be used for the east tap\n"
			"    socket. When this value is specified, the system will use an\n"
			"    AF_INET socket instead of an AF_UNIX socket for the east tap.\n"
			"    This allows a remote monitoring of the eastern tap via TCP/IP.\n"
			"    The value of this number must be greater than zero and a valid\n"
			"    unused TCP port number.\n"
			"    WARNING: The AF_INET option provides unrestricted TCP/IP\n"
			"    to the application data stream. No authentication is required!\n"
			" -b west_port_number\n"
			"    Specifies an AF_INET port number to be used for the west tap\n"
			"    socket. When this value is specified, the system will use an\n"
			"    AF_INET socket instead of an AF_UNIX socket for the west tap.\n"
			"    This allows a remote monitoring of the western tap via TCP/IP.\n"
			"    The value of this number must be greater than zero and a valid\n"
			"    unused TCP port number.\n"
			"    WARNING: The AF_INET option provides unrestricted TCP/IP\n"
			"    to the application data stream. No authentication is required!\n"
			" -h\n"
			"    Displays this help message.\n"
			,
			progName
			);
	}

static void becomeChild(
				const char*	command,
				const char*	slave,
				int			masterFD,
				int			slaveFD
				) {
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	close(masterFD);
	close(slaveFD);

	setsid();

	// Open the slave tty and it will be assiged
	// to the STDIN_FILENO descriptor.
	int
	fd	= open(slave,O_RDWR);
	if(fd < 0) {
		exit(errno);
		}


	// Now duplicate STDIN for STDOUT and STDERR
	dup(fd);	// STDOUT
	dup(fd);	// STDERR

	execl(command,command,(const char*)0);

	exit(errno);
	}

// Contains the default TTY settings.
static struct termios		savedTtyTios;

// The West file descriptor
static int	ttyFD	= STDIN_FILENO;

static int childPID	= -1;

static bool	childDied	= false;

static void childDiedSignalHandler(int sig) {
	int	status = 1;

	// The reason for this check is that if the child dies
	// before the fork returns, we will hang on the wait().
	if(childPID < 0) {
		childDied	= true;
		printf("child died without fork completing.\n");
		return;
		}

	wait(&status);

	if(status) {
		printf("The child process died! %d %s\n\r",status,strerror(status));
		}

	// Restore the tty terminal settings before leaving.
	tcsetattr(ttyFD,TCSANOW,&savedTtyTios);

	exit(status);
	}

/** */
int main(int argc,char* argv[]) {
	int			opt;
	// Base location of TAP socket files for AF_UNIX.
	const char*	path			= "/tmp";
	// Default name of tap socket directory.
	const char*	sockName		= "default";
	// Default name of east tap socket.
	const char*	eastFileName	= "east";
	// Default name of east tap socket.
	const char*	westFileName	= "west";
	// tty device name
	const char*	tty				= 0;
	// pseudo client command
	const char*	command			= 0;
	// Port number for eastern AF_INET tap socket
	unsigned	eastTapPort		= 0;
	// Port number for western AF_INET tap socket
	unsigned	westTapPort		= 0;

	struct sigaction			action;
	struct sigaction			oldAction;

	action.sa_handler	= childDiedSignalHandler;
	sigemptyset(&action.sa_mask);
	action.sa_flags    = 0;
	sigaction(SIGCHLD,&action,&oldAction);

	// This loop takes care of standard Posix option processing.
	while((opt = getopt(argc,argv,"a:b:e:w:r:c:t:p:u:h")) != -1) {
		switch(opt) {
			case 'c':
				command			= optarg;
				break;
			case 'r':
				path			= optarg;
				break;
			case 'u':
				sockName		= optarg;
				break;
			case 't':
				tty				= optarg;
				break;
			case 'e':
				eastFileName	= optarg;
				break;
			case 'w':
				westFileName	= optarg;
				break;
			case 'a':
				eastTapPort		= (unsigned)strtoul(optarg,0,0);
				break;
			case 'b':
				westTapPort		= (unsigned)strtoul(optarg,0,0);
				break;
			case 'h':
				printHelp(argv[0]);
				exit(0);
				break;
			case ':':
				fprintf(stderr,"option %c requires an argument\n",optopt);
				exit(1);
			case '?':
				fprintf(stderr,"unknown option: %c\n",optopt);
				exit(1);
			}
		}

	char*
	eastTapSocketName	= (char*)alloca(		strlen(path)
											+	strlen(sockName)
											+	strlen(eastFileName)
											+2+1
											);

	strcpy(eastTapSocketName,path);
	strcat(eastTapSocketName,"/");
	strcat(eastTapSocketName,sockName);
	strcat(eastTapSocketName,"/");
	strcat(eastTapSocketName,eastFileName);

	if(OsclPosixMkdirFileLeaf(eastTapSocketName,0777)) {
		perror("Cant create east socket directory.\n");
		exit(1);
		}

	char*
	westTapSocketName	= (char*)alloca(		strlen(path)
											+	strlen(sockName)
											+	strlen(westFileName)
											+2+1
											);

	strcpy(westTapSocketName,path);
	strcat(westTapSocketName,"/");
	strcat(westTapSocketName,sockName);
	strcat(westTapSocketName,"/");
	strcat(westTapSocketName,westFileName);

	if(OsclPosixMkdirFileLeaf(westTapSocketName,0777)) {
		perror("Cant create west socket directory.\n");
		exit(1);
		}

	// Contains the RAW TTY settings sent
	// to both the slave and master PTY.
	struct termios		newTtyTios;

	// Open the special master pseudo tty device.
	int	masterFD	= open("/dev/ptmx",O_RDWR);
	if(masterFD < 0) {
		perror("open(\"/dev/ptmx\",O_RDWR)");
		exit(1);
		}

	// Change the mode and ownership of the slave pseudo-terminal
	// device associated with this master.
	int
	err	= grantpt(masterFD);
	if(err) {
		perror("grantpt(masterFD):");
		exit(1);
		}

	// Clears the lock flag associated with the slave pseudo-terminal
	// device, so that the slave pseudo-terminal device can be opened.
	err	= unlockpt(masterFD);
	if(err) {
		perror("unlockpt(masterFD):");
		exit(1);
		}

	// Obtain the name of the slave pseudo-terminal device.
	const char*	name	= ptsname(masterFD);
	if(!name) {
		fprintf(stderr,"ptsname(masterFD) failed");
		exit(1);
		}

	// Get the TTY attributes of the master pseudo-terminal device.
	err	= tcgetattr(masterFD,&savedTtyTios);
	if(err) {
		perror("tcgetattr(masterFD,&savedTtyTios):");
		exit(1);
		}

	// Use the saved terminal attributes to initialize
	// the new RAW terminal mode attributes.
	newTtyTios	= savedTtyTios;

	// Change the new terminal attributes to RAW.
	newTtyTios.c_lflag	= 0;
	newTtyTios.c_oflag	= 0;
	newTtyTios.c_iflag	= 0;
	newTtyTios.c_cc[VMIN]	= 0;
	newTtyTios.c_cc[VTIME]	= 0;

	// Change the master pseudo-terminal device attributes to RAW.
	err	= tcsetattr(masterFD,TCSANOW,&newTtyTios);
	if(err) {
		perror("tcsetattr(masterFD,TCSANOW,&newTtyTios):");
		exit(1);
		}

	// Open the slave pseudo-terminal device so that we can
	// change its terminal mode to RAW. NOTE: We keep the
	// slave pseudo-terminal device open to prevent a reset
	// of the attributes, which we set to RAW later.
	int
	slaveFD	= open(name,O_RDWR);
	if(slaveFD < 0) {
		perror("open(slaveName,O_RDWR):");
		exit(1);
		}

	// Change the slave pseudo-terminal device attributes to RAW.
	err	= tcsetattr(slaveFD,TCSANOW,&newTtyTios);
	if(err) {
		perror("tcsetattr(slaveFD,TCSANOW,&newTtyTios):");
		exit(1);
		}

	// Now fork a command (child) if one was given.
	if(command) {
		childPID = fork();
		if(childPID == -1) {
			perror("fork():");
			exit(1);
			}
		if(!childPID) {
			becomeChild(command,name,masterFD,slaveFD);
			exit(1);
			}
		if(childDied) {
			printf("My child died quickly!\n");
			int	status;
			wait(&status);
			exit(status);
			}
		}
	else {
		// Print the name of the slave pseudo-terminal device
		// so that the user may use it with another program.
		printf(	"slave: %s\n",name);
		}

	if(tty) {
		// Open the tty device
		ttyFD	= open(tty,O_RDWR);
		if(ttyFD < 0) {
			perror("open(tty,O_RDWR):");
			exit(1);
			}
		}

	// Change the tty device attributes to RAW.
	err	= tcsetattr(ttyFD,TCSANOW,&newTtyTios);
	if(err) {
		perror("tcsetattr(ttyFD,TCSANOW,&newTtyTios):");
		exit(1);
		}

	// Get rid of any old AF_UNIX socket file.
	unlink(eastTapSocketName);

	int	eastSocketFD;
	//
	// Now, crank-up the tap socket listeners.
	//
	if(eastTapPort) {
		// Create an AF_INET socket.
		struct sockaddr_in	serverAddress;

		// Allocate a socket for the eastern end taps.
		eastSocketFD	= socket(AF_INET,SOCK_STREAM,0);

		// Name the eastern end tap socket.
		serverAddress.sin_family		= AF_INET;
		serverAddress.sin_addr.s_addr	= INADDR_ANY;
		serverAddress.sin_port			= htons(eastTapPort);

		// Associate the eastern end internet address with its socket.
		bind(	eastSocketFD,
				(struct sockaddr*)&serverAddress,
				sizeof(serverAddress)
				);
		}
	else {
		// Create an AF_UNIX socket.
		struct sockaddr_un	serverAddress;

		// Allocate a socket for the eastern end taps.
		eastSocketFD	= socket(AF_UNIX,SOCK_STREAM,0);

		// Name the eastern end tap socket.
		serverAddress.sun_family	= AF_UNIX;
		strncpy(	serverAddress.sun_path,
					eastTapSocketName,
					sizeof(serverAddress.sun_path)
					);
		serverAddress.sun_path[sizeof(serverAddress.sun_path)-1]	= '\0';

		// Associate the eastern end tap name with its socket.
		bind(	eastSocketFD,
				(struct sockaddr*)&serverAddress,
				sizeof(serverAddress)
				);
		}

	// Start listening for connections on the eastern end socket.
	listen(eastSocketFD,5);

	// Get rid of any old AF_UNIX socket file.
	unlink(westTapSocketName);

	int	westSocketFD;
	//
	// Now, crank-up the tap socket listeners.
	//
	if(westTapPort) {
		// Create an AF_INET socket.
		struct sockaddr_in	serverAddress;

		// Allocate a socket for the western end taps.
		westSocketFD	= socket(AF_INET,SOCK_STREAM,0);

		// Name the western end tap socket.
		serverAddress.sin_family		= AF_INET;
		serverAddress.sin_addr.s_addr	= INADDR_ANY;
		serverAddress.sin_port			= htons(westTapPort);

		// Associate the western end internet address with its socket.
		bind(	westSocketFD,
				(struct sockaddr*)&serverAddress,
				sizeof(serverAddress)
				);
		}
	else {
		// Create an AF_UNIX socket.
		struct sockaddr_un	serverAddress;

		// Allocate a socket for the western end taps.
		westSocketFD	= socket(AF_UNIX,SOCK_STREAM,0);

		// Name the western end tap socket.
		serverAddress.sun_family	= AF_UNIX;
		strncpy(	serverAddress.sun_path,
					westTapSocketName,
					sizeof(serverAddress.sun_path)
					);
		serverAddress.sun_path[sizeof(serverAddress.sun_path)-1]	= '\0';

		// Associate the western end tap name with its socket.
		bind(	westSocketFD,
				(struct sockaddr*)&serverAddress,
				sizeof(serverAddress)
				);
		}

	// Start listening for connections on the western end socket.
	listen(westSocketFD,5);

	//
	//	Now, to do all of the I/O multiplexing, we
	//	create the MiddleMan that does all of the run-time
	//	work.
	//
	//	East 	== pseudo tty (masterFD)
	//	West	== device tty (ttyFD)
	//

	Oscl::MiddleMan::App*
	mm	= new Oscl::MiddleMan::App(
					eastSocketFD,
					westSocketFD,
					masterFD,	// eastFD,
					ttyFD		// westFD
					);

	//
	// We run until she closes.
	//
	mm->run();

	// Close all the file descriptors.
	close(eastSocketFD);
	close(westSocketFD);
	close(ttyFD);
	close(slaveFD);
	close(masterFD);
	}

