/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tree_nodeh_
#define _oscl_tree_nodeh_

/** */
namespace Oscl {

/** */
namespace Tree {

/** This template class implements a binary tree node.
	The template parameter class keyType must implement
	the less-than operator '<'.
	For example:
	<p>
	class keyType {
		public:
			bool	operator < (const keyType&) const noexcept;
		};
	</p>
	The template parameter tType must implement an
	accessor function that returns a reference to
	the keyType.
	For example:
	<p>
	class tType {
		public:
			const keyType&	getKey() const noexcept;
		};
	</p>

 */
template <class tType,class keyType>
	class Node {
		public:
			/** */
			class TraversalApi {
				public:
					/** */
					virtual ~TraversalApi(){}
					/** Returns true to terminate the traversal.
					 */
					virtual bool	next(tType& node,unsigned level) noexcept=0;
				};
		public:
			/** Context reference.
			 */
			tType&							__treenode_content;

			/** The left link field.
			 */
			class Node<tType,keyType>*	__treenode_leftlink;

			/** The right link field.
			 */
			class Node<tType,keyType>*	__treenode_rightlink;

		public:

			/** Public constructor initializes left and right pointers.
			 */
			Node(tType& content) noexcept;

			/** Accessor to get content reference.
			 */
			tType&		content() noexcept;

			/** Thread into the linked list at the end of the FIFO tree.
			 */
			void	insert(class Node<tType,keyType>* item) noexcept;

			/** Remove specified element from tree and return a pointer to
				it as a result.
			 */
			class Node<tType,keyType>*
				remove(class Node<tType,keyType>* item) noexcept;

			/** Find element specified by key. Returns null if element
				matching the key is not found.
			 */
			class Node<tType,keyType>*	find(const keyType& key) noexcept;

			/** Traverse the tree in forward order.
			 */
			bool	traverseForward(	TraversalApi&	tapi,
										unsigned		level=0
										) noexcept;

			/** Traverse the tree in forward order.
			 */
			bool	traverseReverse(	TraversalApi&	tapi,
										unsigned		level=0
										) noexcept;
		};

template <class tType,class keyType>
	Node<tType,keyType>::Node(tType& content) noexcept :
			__treenode_content(content),
			__treenode_leftlink(0),
			__treenode_rightlink(0)
			{
		}

template <class tType,class keyType>
	inline void
		Node<tType,keyType>::insert(	class Node<tType,keyType>*	item
											) noexcept{
//		item->__treenode_leftlink	= 0;
//		item->__treenode_rightlink	= 0;
		if(item->__treenode_content.getKey() < __treenode_content.getKey()){
			if(__treenode_leftlink){
				__treenode_leftlink->insert(item);
				}
			else{
				__treenode_leftlink	= item;
				}
			}
		else{
			if(__treenode_rightlink){
				__treenode_rightlink->insert(item);
				}
			else{
				__treenode_rightlink	= item;
				}
			}
		}

template <class tType,class keyType>
	inline tType&		Node<tType,keyType>::content() noexcept{
		return __treenode_content;
		}

template <class tType,class keyType>
	inline class Node<tType,keyType>*
		Node<tType,keyType>::remove(	class Node<tType,keyType>*	item
											) noexcept{
		if(__treenode_leftlink == item){
			if(item->__treenode_leftlink){
				if(item->__treenode_rightlink){
					item->__treenode_rightlink->
						insert(item->__treenode_leftlink);
					__treenode_leftlink	= item->__treenode_rightlink;
					}
				else{ // left link only
					__treenode_leftlink	= item->__treenode_leftlink;
					}
				}
			else if(item->__treenode_rightlink) {
				__treenode_leftlink	= item->__treenode_rightlink;
				}
			else {
				__treenode_leftlink	= 0;
				}
			item->__treenode_leftlink	= 0;
			item->__treenode_rightlink	= 0;
			return item;
			}

		if(__treenode_rightlink == item){
			if(item->__treenode_rightlink){
				if(item->__treenode_leftlink){
					item->__treenode_leftlink->
						insert(item->__treenode_rightlink);
					__treenode_rightlink	= item->__treenode_leftlink;
					}
				else { // right link only
					__treenode_rightlink	= item->__treenode_rightlink;
					}
				}
			else if(item->__treenode_leftlink){
				__treenode_rightlink	= item->__treenode_leftlink;
				}
			else {
				__treenode_rightlink	= 0;
				}
			item->__treenode_leftlink	= 0;
			item->__treenode_rightlink	= 0;
			return item;
			}

		if(__treenode_leftlink){
			if(__treenode_leftlink->remove(item)){
				return item;
				}
			}

		if(__treenode_rightlink){
			if(__treenode_rightlink->remove(item)){
				return item;
				}
			}

		return 0;
		}

template <class tType,class keyType>
	inline class Node<tType,keyType>*
		Node<tType,keyType>::find(const keyType& key) noexcept{

		if(key == __treenode_content.getKey()){
			return this;
			}
		if(key < __treenode_content.getKey()){
			if(!__treenode_leftlink){
				return 0;
				}
			return __treenode_leftlink->find(key);
			}
		else{
			if(!__treenode_rightlink){
				return 0;
				}
			return __treenode_rightlink->find(key);
			}
		}

template <class tType,class keyType>
	inline bool
		Node<tType,keyType>::traverseForward(
			Node<tType,keyType>::TraversalApi&	tapi,
			unsigned								level
			) noexcept{
	if(__treenode_leftlink){
		if(__treenode_leftlink->traverseForward(tapi,level+1)) return true;
		}
	if(tapi.next(__treenode_content,level)) return true;
	if(__treenode_rightlink){
		if(__treenode_rightlink->traverseForward(tapi,level+1)) return true;
		}
	return false;
	}

template <class tType,class keyType>
	inline bool
		Node<tType,keyType>::traverseReverse(
			Node<tType,keyType>::TraversalApi&	tapi,
			unsigned								level
			) noexcept{
	if(__treenode_rightlink){
		if(__treenode_rightlink->traverseReverse(tapi,level+1)) return true;
		}
	if(tapi.next(__treenode_content,level)) return true;
	if(__treenode_leftlink){
		if(__treenode_leftlink->traverseReverse(tapi,level+1)) return true;
		}
	return false;
	}

}
}

#endif
