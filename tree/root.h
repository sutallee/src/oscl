/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tree_rooth_
#define _oscl_tree_rooth_
#include "node.h"

/** */
namespace Oscl {
/** */
namespace Tree {

/** This template class implements a binary tree root.
 */
template <class tType,class keyType>
	class Root {
		public:
			/** The root link field.
			 */
			class Node<tType,keyType>*		_root;

		public:
			/** Public constructor initializes the root pointer.
			 */
			Root() noexcept;

			/** Thread into the linked list at the end of the FIFO tree.
			 */
			void	insert(class Node<tType,keyType>* item) noexcept;

			/** Remove specified element from tree and return a pointer to
				it as a result.
			 */
			class Node<tType,keyType>*
				remove(class Node<tType,keyType>* item) noexcept;

			/** Return first element in tree.
			 */
			class Node<tType,keyType>* first() noexcept;

			/** Find element specified by key. Returns null if element
				matching the key is not found.
			 */
			class Node<tType,keyType>*	find(const keyType& key) noexcept;

			/** Traverse the tree in forward order.
			 */
			bool	traverseForward(
						typename Node<tType,keyType>::TraversalApi&	tapi,
						unsigned										level=0
						) noexcept;

			/** Traverse the tree in forward order.
			 */
			bool	traverseReverse(
						typename Node<tType,keyType>::TraversalApi&	tapi,
						unsigned										level=0
						) noexcept;
		};

template <class tType,class keyType>
	Root<tType,keyType>::Root(void) noexcept :
			_root(0)
			{
		}

template <class tType,class keyType>
	inline void	Root<tType,keyType>::insert(
								class Node<tType,keyType>*	item
								) noexcept{
		item->__treenode_leftlink	= 0;
		item->__treenode_rightlink	= 0;
		if(_root){
			_root->insert(item);
			}
		else{
			_root	= item;
			}
		}

template <class tType,class keyType>
	inline class Node<tType,keyType>*
		Root<tType,keyType>::remove(	class Node<tType,keyType>*	item
											) noexcept{
		if(_root == item){
			if(item->__treenode_leftlink){
				if(item->__treenode_rightlink){
					item->__treenode_rightlink->insert(
						item->__treenode_leftlink
						);
					_root	= item->__treenode_rightlink;
					}
				else{ // left link only
					_root	= item->__treenode_leftlink;
					}
				}
			else if(item->__treenode_rightlink) {
				_root	= item->__treenode_rightlink;
				}
			else {
				_root	= 0;
				}
			item->__treenode_leftlink	= 0;
			item->__treenode_rightlink	= 0;
			return item;
			}
		else if(_root) {
			if(_root->remove(item)) return item;
			}
		return 0;
		}

template <class tType,class keyType>
	class Node<tType,keyType>*
		Root<tType,keyType>::first() noexcept{
		Node<tType,keyType>*	next;
		Node<tType,keyType>*	prev;
		prev	= _root;
		next	= 0;
		if(!prev) return prev;
		for(;(next=prev->__treenode_leftlink);prev=next);
		return prev;
		}

template <class tType,class keyType>
	inline class Node<tType,keyType>*
		Root<tType,keyType>::find(	const keyType&	key
										) noexcept{
		if(!_root) return 0;
		return _root->find(key);
		}

template <class tType,class keyType>
	inline bool
		Root<tType,keyType>::traverseForward(
									typename Node<tType,keyType>::
										TraversalApi&		tapi,
										unsigned			level
										) noexcept{
		if(_root){
			if(_root->traverseForward(tapi,level)) return true;
			}
		return false;
		}

template <class tType,class keyType>
	inline bool
		Root<tType,keyType>::traverseReverse(
								typename Node<tType,keyType>::
											TraversalApi&			tapi,
								unsigned							level
								) noexcept{
		if(_root){
			if(_root->traverseReverse(tapi,level)) return true;
			}
		return false;
		}

}
}

#endif
