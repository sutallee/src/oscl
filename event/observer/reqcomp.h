/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_event_observer_reqcomph_
#define _oscl_event_observer_reqcomph_

#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Event {

/** */
namespace Observer {

/** */
namespace Req {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This interface is used by observers to be notified of
	events.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	The observer sends this message to request notification
			when a new occurrence of the event happens.
		 */
		void	(Context::*_Occurrence)(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg);

		/**	The observer sends this message to request notification
			when a new occurrence of the event happens.
		 */
		void	(Context::*_CancelOccurrence)(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*Occurrence)(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg),
			void	(Context::*CancelOccurrence)(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg)
			) noexcept;

	private:
		/**	The observer sends this message to request notification
			when a new occurrence of the event happens.
		 */
		void	request(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg) noexcept;

		/**	The observer sends this message to request notification
			when a new occurrence of the event happens.
		 */
		void	request(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*Occurrence)(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg),
			void	(Context::*CancelOccurrence)(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg)
			) noexcept:
		_context(context),
		_Occurrence(Occurrence),
		_CancelOccurrence(CancelOccurrence)
		{
	}

template <class Context>
void	Composer<Context>::request(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg) noexcept{
	(_context.*_Occurrence)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg) noexcept{
	(_context.*_CancelOccurrence)(msg);
	}

}
}
}
}
#endif
