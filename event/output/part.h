/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_event_output_parth_
#define _oscl_event_output_parth_

#include "oscl/event/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/event/control/api.h"

/** */
namespace Oscl {

/** */
namespace Event {

/** */
namespace Output {

/** */
class Part :
		public Oscl::Event::Observer::Req::Api,
		public Oscl::Event::Control::Api
		{
	private:
		/** */
		Oscl::Event::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::Event::Observer::Req::Api::OccurrenceReq>	_noteQ;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi
			) noexcept;

		/** */
		Oscl::Event::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify() noexcept;

	public: // Oscl::Event::Control::Api
		/** */
		void	trigger() noexcept;

	public:	// Oscl::Event::Observer::Req::Api
		/** */
		void request(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg) noexcept;

		/** */
		void request(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg) noexcept;
	};

}
}
}

#endif
