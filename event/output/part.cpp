#include "part.h"

using namespace Oscl::Event::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi
	) noexcept:
		_sap(
			*this,
			papi
			)
		{
	}

Oscl::Event::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify() noexcept{
	Oscl::Event::Observer::Req::Api::OccurrenceReq* note;

	while((note=_noteQ.get())){
		note->returnToSender();
		}
	}

void Part::request(Oscl::Event::Observer::Req::Api::OccurrenceReq& msg) noexcept{
	_noteQ.put(&msg);
	}

void Part::request(Oscl::Event::Observer::Req::Api::CancelOccurrenceReq& msg) noexcept{
	for(
		Oscl::Event::Observer::Req::Api::OccurrenceReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::trigger() noexcept{
	notify();
	}

