/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_select_itc_reqapih_
#define _oscl_posix_select_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/posix/select/handler.h"
#include "oscl/posix/select/timer/base.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {
/** */
namespace Req {

/** This interface defines the messages for adding and removing
	select handlers from the select server.
 */
class Api {
	public:
		/** This payload is used for registering the specified
			select handler with the select server.
		 */
		class AttachPayload {
			public:
				/** */
				Select::Handler&	_handler;
			public:
				/** */
				AttachPayload(Select::Handler& handler) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,AttachPayload>		AttachReq;

	public:
		/** This payload is used for removing the specified
			select handler from the select server.
		 */
		class DetachPayload {
			public:
				/** */
				Select::Handler&	_handler;
			public:
				/** */
				DetachPayload(Select::Handler& handler) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,DetachPayload>	DetachReq;

	public:
		/** This payload is used for registering the specified
			select handler with the select server.
		 */
		class StartTimerPayload {
			public:
				/** A reference to the I/O handler to be
					attached.
				 */
				Oscl::Posix::Select::Timer::Base&	_timer;

			public:
				/** The constructor requires a reference to
					the io handler to be attached.
				 */
				StartTimerPayload(Oscl::Posix::Select::Timer::Base& timer) noexcept;
			};

		/** This defines the attach request message.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<Api,StartTimerPayload>		StartTimerReq;

	public:
		/** This payload is used for removing the specified
			select handler from the select server.
		 */
		class StopTimerPayload {
			public:
				/** A reference to the I/O handler to be
					detached.
				 */
				Oscl::Posix::Select::Timer::Base&	_timer;

			public:
				/** The constructor requires a reference to
					the io handler to be detached.
				 */
				StopTimerPayload(Oscl::Posix::Select::Timer::Base& timer) noexcept;
			};

		/** This defines the detach request message.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<Api,StopTimerPayload>	StopTimerReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>	SAP;

		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** */
		virtual ~Api() {}

		/** */
		virtual void	request(AttachReq& msg) noexcept=0;

		/** */
		virtual void	request(DetachReq& msg) noexcept=0;

		/** This operation is invoked when an attach timer
			request message is received.
		 */
		virtual void	request(StartTimerReq& msg) noexcept=0;

		/** This operation is invoked when a detach timer
			request message is received.
		 */
		virtual void	request(StopTimerReq& msg) noexcept=0;
	};

}
}
}
}

#endif
