/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Posix::Select::Req;

Api::AttachPayload::
AttachPayload(Oscl::Posix::Select::Handler&	handler) noexcept:
		_handler(handler)
		{
	}

Api::DetachPayload::
DetachPayload(Oscl::Posix::Select::Handler&	handler) noexcept:
		_handler(handler)
		{
	}

Api::StartTimerPayload::
StartTimerPayload(Oscl::Posix::Select::Timer::Base& timer) noexcept:
		_timer(timer)
		{
	}

Api::StopTimerPayload::
StopTimerPayload(Oscl::Posix::Select::Timer::Base& timer) noexcept:
		_timer(timer)
		{
	}

