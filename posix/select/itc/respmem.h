/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_select_itc_respmemh_
#define _oscl_posix_select_itc_respmemh_
#include "oscl/memory/block.h"
#include "respapi.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {
/** */
namespace Resp {

struct AttachRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::AttachResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::AttachPayload)>	payload;
	};

struct DetachRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::DetachResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::DetachPayload)>	payload;
	};

union Mem {
	/** */
	void*				__qitemlink;
	/** */
	AttachRespMem		transfer;
	/** */
	DetachRespMem	cancelAttach;
	};

}
}
}
}

#endif
