/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_posix_select_mbox_serverh_
#define _oscl_posix_select_mbox_serverh_

#include "oscl/mt/runnable.h"
#include "oscl/mt/itc/mbox/srvapi.h"
#include "oscl/posix/select/api.h"
#include "oscl/mt/itc/mbox/mutex.h"
#include "oscl/posix/select/itc/sync.h"
#include "oscl/posix/select/timer/base.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {

/** This concrete class handles the POSIX select() function for
	many registered clients. It is a special mailbox in that
	it uses a special technique to cause its mailbox to be
	processed. Specifically, whenever the signal() operation
	is invoked, a character is written to a POSIX pipe, which
	is one of the file descriptors being waited on by the select()
	call. When select returns and finds that a read is pending
	on the pipe file descriptor, it processes its mailbox.

	Typically, there is only a single instance of this class
	within a process, though there is no real reason that it
	cannot be otherwise.
 */
class Server :
	public Oscl::Mt::Runnable,
	public Oscl::Mt::Itc::PostMsgApi,
	public Oscl::Mt::Sema::SignalApi,
	public Oscl::Mt::Itc::ServerApi,
	public Oscl::Posix::Select::Api,
	public Req::Api
	{
	private:
		/** */
		SignaledApi*							_signaledApi;

		/** */
		Queue<Oscl::Mt::Itc::Msg>				_mboxQ;

		/** */
		FileDescSets							_fdSets;

		/** */
		Oscl::Queue<Oscl::Posix::Select::Handler>	_selectQ;

		/** */
		Oscl::Mt::Itc::MailboxMutex				_mutex;

		/** */
		Sync									_sync;

		/** This pipe is written when we wish to cause the
			select() operation to wake-up so that it might
			service its mailbox or update its fd sets.
			_signalPipeFD[0] is the reading end of the pipe
			_signalPipeFD[1] is the writing end of the pipe
		 */
		int										_signalPipeFD[2];

	private:
		/** */
		Oscl::Queue<
			Oscl::Posix::Select::Timer::Base
			>									_timers;

		/** */
		struct timeval							_timeout;

	public:
		/** */
		Server() noexcept;

		/** */
		virtual ~Server();

		/** */
		Oscl::Posix::Select::Req::Api::SAP&	getSAP() noexcept;

		/** */
		Oscl::Posix::Select::Api&	getSyncApi() noexcept;

	public: // Oscl::Posix::Select::Api
		/** This operation *may* be invoked before the thread
			begins running. Afterwards, it is only invoked
			by the Server itself upon receiving an AddReq
			message.
		 */
		void	attach(Handler& handler) noexcept;

		/** */
		void	detach(Handler& handler) noexcept;

		/** */
		void	start(Oscl::Posix::Select::Timer::Base& timer) noexcept;

		/** */
		void	stop(Oscl::Posix::Select::Timer::Base& timer) noexcept;

	private:
		/** */
		void	processMsgQ() noexcept;

	public:	// PostMsgApi
		/** */
		void	post(Oscl::Mt::Itc::Msg& msg) noexcept;
		/** */
		void	postSync(Oscl::Mt::Itc::Msg& msg) noexcept;

	public: // SignalApi interface
		/** */
		void signal(void) noexcept;
		/** */
		void suSignal(void) noexcept;

	protected:
		/** */
		virtual void	initialize() noexcept;

	public: // Runnable
		/** */
		virtual void	run() noexcept;

	private:	// ServerApi
		/** */
		void	setSignaledApi(SignaledApi* sapi) noexcept;
		/** */
		Oscl::Mt::Sema::SignalApi&	getSignalApi() noexcept;
		/** */
		Oscl::Mt::Itc::PostMsgApi&	getPostMsgApi() noexcept;

	private:
		/** */
		void	failIoHandlers() noexcept;

		/** */
		void	processIoHandlers(FileDescSets& pend) noexcept;

	private:
		/** */
		struct timeval*	nextTimerExpiration() noexcept;

		/** */
		void	processExpiration() noexcept;

		/** */
		void	updateTimerExpiration(struct timeval& remaining) noexcept;

	public: //Req::Api
		/** */
		void	request(Req::Api::AttachReq& msg) noexcept;

		/** */
		void	request(Req::Api::DetachReq& msg) noexcept;

		/** */
		void	request(Req::Api::StartTimerReq& msg) noexcept;

		/** */
		void	request(Req::Api::StopTimerReq& msg) noexcept;

	};

}
}
}

#endif
