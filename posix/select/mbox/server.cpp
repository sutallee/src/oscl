/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "oscl/mt/scopesync.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"
#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>

using namespace Oscl::Posix::Select;

Server::Server() noexcept:
		_signaledApi(0),
		_mboxQ(),
		_fdSets(),
		_selectQ(),
		_sync(*this,*this)
		{
	int err = pipe(_signalPipeFD);
    if(err){
        perror("Oscl::Posix::Select::Server: Cannot create signal pipe\n");
        exit(err);
        }

	err	= fcntl(_signalPipeFD[1],F_SETFL,O_NONBLOCK);
	if(err){
        perror("Oscl::Posix::Select::Server: Cannot set O_NONBLOCK.\n");
		}
	FD_SET(_signalPipeFD[0],&_fdSets._readfds);
	}

Server::~Server(){
	if(_signalPipeFD[0] > -1){
		close(_signalPipeFD[0]);
		_signalPipeFD[0]	= -1;
		}
	if(_signalPipeFD[1] > -1){
		close(_signalPipeFD[1]);
		_signalPipeFD[1]	= -1;
		}
	}

Oscl::Posix::Select::Req::Api::SAP&	Server::getSAP() noexcept{
	return _sync.getSAP();
	}

Oscl::Posix::Select::Api&	Server::getSyncApi() noexcept{
	return _sync;
	}

void	Server::attach(Handler&	handler) noexcept{
	handler.start(_fdSets);
	_selectQ.put(&handler);
	}

void	Server::detach(Handler&	handler) noexcept{
	if(_selectQ.remove(&handler)){
		handler.stop(_fdSets);
		}
	}

void	Server::start(Oscl::Posix::Select::Timer::Base& timer) noexcept{

	for(
		Oscl::Posix::Select::Timer::Base*
		next	= _timers.first();
		next;
		next	= _timers.next(next)
		){

		if(timercmp(&timer._duration,&next->_duration,<)){

			timersub(&next->_duration,&timer._duration,&next->_duration);

			_timers.insertBefore(next,&timer);

			return;
			}
		else {
			timersub(&timer._duration,&next->_duration,&timer._duration);
			}
		}

	_timers.put(&timer);
	}

void	Server::stop(Oscl::Posix::Select::Timer::Base& timer) noexcept{

	Oscl::Posix::Select::Timer::Base*	next;

	for(
		next=_timers.first();
		next;
		next	= _timers.next(next)
		){

		if(&timer == next){

			Oscl::Posix::Select::Timer::Base*
			nt	= _timers.next(next);

			if(nt){
				timeradd(&nt->_duration,&timer._duration,&nt->_duration);
				}

			_timers.remove(&timer);

			break;
			}
		}
	}

struct timeval*	Server::nextTimerExpiration() noexcept{

	Oscl::Posix::Select::Timer::Base*
	timer	= _timers.first();

	if(!timer){
		return 0;
		}

	_timeout	= timer->_duration;

	return &_timeout;
	}

void	Server::updateTimerExpiration(struct timeval& remaining) noexcept{

	Oscl::Posix::Select::Timer::Base*
	timer	= _timers.first();

	if(!timer){
		return;
		}

	if(!timerisset(&timer->_duration)){
		return;
		}

	if(timercmp(&remaining,&timer->_duration,>)){
		return;
		}

	timer->_duration	= remaining;
	}

void	Server::processExpiration() noexcept{

	Oscl::Posix::Select::Timer::Base*
	timer	= _timers.get();

	if(!timer){
		return;
		}

	Oscl::Queue<
		Oscl::Posix::Select::Timer::Base
		>	expiredTimers;

	expiredTimers.put(timer);

	while((timer = _timers.get())){

		if(timerisset(&timer->_duration)){

			_timers.push(timer);

			break;
			}
		else {
			expiredTimers.put(timer);
			}
		}

	while((timer = expiredTimers.get())){
		timer->expired(_fdSets);
		}
	}

void	Server::processMsgQ() noexcept{
	for(;;){
		Oscl::Mt::Itc::Msg*	msg;
			{
			Oscl::Mt::ScopeSync	sync(_mutex);
			msg = _mboxQ.get();
			}
		if(msg){
			msg->process();
			}
		else {
			break;
			}
		}
	}

void	Server::post(Oscl::Mt::Itc::Msg& msg) noexcept{
		{
		Oscl::Mt::ScopeSync	sync(_mutex);
		_mboxQ.put(&msg);
		signal();
		}
	}

void	Server::postSync(Oscl::Mt::Itc::Msg& msg) noexcept{
	post(msg);
	Oscl::Mt::Thread::getCurrent().syncWait();
	}

void	Server::signal(void) noexcept{
	char	dummy	= 0xA5;

	for(unsigned i;;++i){
		ssize_t	result	= write(_signalPipeFD[1],&dummy,1);
		if(result < 1){
			if(result < 0){
				// "Error" detected
				if(errno != EINTR) {
					// Actual error
					printf(
						"%p: Oscl::Posix::Select::Server::signal(_signalPipeFD[1]:%d) write failed: %d\n",
						this,
						_signalPipeFD[1],
						errno
						);
					break;
					}
				// We were interrupted by
				// a signal so we try a gain.
				}
			// Did not write anything... try again
			// so we don't loose the signal.
			continue;
			}
		else {
			break;
			}
		}
	}

void	Server::suSignal(void) noexcept{
	static const char	dummy[1] = {0};
	ssize_t
	result	= write(_signalPipeFD[1],dummy,1);
	if(result < 0){
		// Nothing to do.
		// This is expected and the mailbox
		// will recover eventually when
		// the pipe empties.
		}
	}

void Server::run() noexcept{
	for(	Handler*	next	= _selectQ.first();
			next ;
			next	= _selectQ.next(next)
			) {
		next->start(_fdSets);
		}

	initialize();

	for(;;){
		int	result;

		FileDescSets	pend(_fdSets);

		struct timeval*
		timeout	= nextTimerExpiration();

		result	= select(	FD_SETSIZE,
							&pend._readfds,
							&pend._writefds,
							&pend._exceptfds,
							timeout
							);
		if(result < 0) {
			if(errno == EINTR) {
				// A signal has occurred.
				printf("EINTR!\n");
				continue;
				}
			if(errno == EBADF) {
				// Someone has registered a bad file descriptor
				// or perhaps the file descriptor has been closed.
				// Lets pretend that life is good and see what the
				// clients think. This can happen when the file
				// descriptor is closed by another thread.
				printf("%p: select() failed, continue: EBADF\n",this);

				failIoHandlers();

				// To prevent a continuous flood of EBADF errors,
				// let's allow other threads to run for a while,
				// such that ITC messages destined for this thread
				// can get to the mailbox.
				Oscl::Mt::Thread::sleep(1000);

				processMsgQ();
				}
			else {
				// Either we did something wrong, or the
				// application has been killed (e.g. with a Ctrl-C.)
				perror("select failed");
				break;
				}
			}
		else if(!result) {
			processExpiration();
			continue;
			}
		else if(timeout) {
			updateTimerExpiration(*timeout);
			}

		if(FD_ISSET(_signalPipeFD[0],&pend._readfds)){
			// Someone has invoked signal()
			char	dummy[1];
			ssize_t
			result	= read(_signalPipeFD[0],dummy,1);
			if(result < 0){
				// We'll ignore this since it is
				// expected?
				}
			processMsgQ();
			continue;
			}

		processIoHandlers(pend);

		}
	}

void Server::initialize() noexcept{
	}

void	Server::setSignaledApi(SignaledApi* sapi) noexcept{
	if(sapi && _signaledApi){
		// To help ensure that only one SignaledApi is set at any
		// given time, the protocol is for the client to set the
		// signal handler, and then set it to zero when it is no
		// longer required. Thus, if there are (by programmer error)
		// two clients that attempt to set the SignaledApi together,
		// the error will be caught at run-time here.
		ErrorFatal::logAndExit("Mt::Itc::Server::setSignaledApi\n");
		}
	_signaledApi	= sapi;
	}

Oscl::Mt::Sema::SignalApi&	Server::getSignalApi() noexcept{
	return *this;
	}

Oscl::Mt::Itc::PostMsgApi&	Server::getPostMsgApi() noexcept{
	return *this;
	}

void	Server::failIoHandlers() noexcept{
	for(	Handler*	next	= _selectQ.first();
			next ;
			) {
		if(next->fdIsClosed()){
			Handler*	h	= next;
			next	= _selectQ.next(next);
			detach(*h);
			}
		else {
			next	= _selectQ.next(next);
			}
		}
	}

void	Server::processIoHandlers(FileDescSets& pend) noexcept{
	time_t	timestamp	= time(0);

	for(	Handler*	next	= _selectQ.first();
			next ;
			) {
		if(next->handleIO(pend, _fdSets, timestamp)){
			Handler*	h	= next;
			next	= _selectQ.next(next);
			detach(*h);
			}
		else {
			next	= _selectQ.next(next);
			}
		}
	}

void	Server::request(Req::Api::AttachReq& msg) noexcept{
	attach(msg._payload._handler);
	msg.returnToSender();
	}

void	Server::request(Req::Api::DetachReq& msg) noexcept{
	detach(msg._payload._handler);
	msg.returnToSender();
	}

void	Server::request(Req::Api::StartTimerReq& msg) noexcept{

	start(msg.getPayload()._timer);

	msg.returnToSender();
	}

void	Server::request(Req::Api::StopTimerReq& msg) noexcept{

	stop(msg.getPayload()._timer);

	msg.returnToSender();
	}

