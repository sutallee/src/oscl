/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <unistd.h>
#include <errno.h>
#include "read.h"

using namespace Oscl::Posix::Select::Transaction;

Read::Read(	int							readFileDescriptor,
			Oscl::Done::Api&			succeeded,
			Oscl::Done::Api&			failed,
			void*						buffer,
			unsigned					nOctetsToRead,
			bool						allowFewerOctets
			) noexcept:
		_readFileDescriptor(readFileDescriptor),
		_succeeded(succeeded),
		_failed(failed),
		_doneApi(0),
		_buffer((unsigned char*)buffer),
		_nRemainingToRead(nOctetsToRead),
		_allowFewerOctets(allowFewerOctets)
		{
	}

void	Read::start(Oscl::Posix::Select::Api& posixSelectApi) noexcept{
	posixSelectApi.attach(*this);
	}

void	Read::start(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_SET(_readFileDescriptor, &next._readfds);
	FD_SET(_readFileDescriptor, &next._exceptfds);
	}

void	Read::stop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_CLR(_readFileDescriptor, &next._readfds);
	FD_CLR(_readFileDescriptor, &next._exceptfds);
	if(_doneApi){
		_doneApi->done();
		}
	}

bool	Read::handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
						Oscl::Posix::Select::FileDescSets&			next,
						time_t&										timestamp
						) noexcept{
	if(!FD_ISSET(_readFileDescriptor, &pending._readfds)){
		if(FD_ISSET(_readFileDescriptor, &pending._exceptfds)){
			_doneApi	= &_failed;
			return true;
			}
		return false;
		}

	ssize_t
	nRead	= ::read(_readFileDescriptor,_buffer,_nRemainingToRead);

	if(nRead < 0 && (errno != EAGAIN)){
		_doneApi	= &_failed;
		return true;
		}

	if(nRead < 0){
		// EAGAIN
		return false;
		}

	if(!nRead){
		_doneApi	= &_failed;
		return true;
		}

	if(_allowFewerOctets){
		_nRemainingToRead	= nRead;
		_doneApi	= &_succeeded;
		return true;
		}

	_nRemainingToRead	-= nRead;

	if(!_nRemainingToRead){
		_doneApi	= &_succeeded;
		return true;
		}

	_buffer	+=	nRead;

	return false;
	}

bool    Read::fdIsClosed() noexcept{
	fd_set			fds;

	FD_ZERO(&fds);
	FD_SET(_readFileDescriptor,&fds);

	struct timeval	timeout;
	timeout.tv_sec	= 0;
	timeout.tv_usec	= 0;

	int	result	= select(FD_SETSIZE,&fds,0,0, &timeout);

	if(result < 0){
		if(errno == EBADF){
			_doneApi	= &_failed;
			return true;
			}
		}

	return false;
	}

unsigned	Read::nOctetsActuallyRead() noexcept{
	// When the allowFewerOctets flag is true,
	// the client will invoke this function to
	// determine the actual number of octets
	// read.
	// If the allowFewerOctets, the implementation
	// sets the value of _nRemainingToRead to the
	// actual number of bytes read before returning.
	return _nRemainingToRead;
	}

