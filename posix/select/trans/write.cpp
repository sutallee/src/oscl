/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <unistd.h>
#include <errno.h>
#include "write.h"

using namespace Oscl::Posix::Select::Transaction;

Write::Write(	int							writeFileDescriptor,
				Oscl::Done::Api&			succeeded,
				Oscl::Done::Api&			failed,
				const void*					buffer,
				unsigned					nOctetsToWrite
				) noexcept:
		_writeFileDescriptor(writeFileDescriptor),
		_succeeded(succeeded),
		_failed(failed),
		_doneApi(0),
		_buffer((const unsigned char*)buffer),
		_nRemainingToWrite(nOctetsToWrite)
		{
	}

void	Write::start(Oscl::Posix::Select::Api& posixSelectApi) noexcept{
	posixSelectApi.attach(*this);
	}

void	Write::start(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_SET(_writeFileDescriptor, &next._writefds);
	FD_SET(_writeFileDescriptor, &next._exceptfds);
	}

void	Write::stop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_CLR(_writeFileDescriptor, &next._writefds);
	FD_CLR(_writeFileDescriptor, &next._exceptfds);
	_doneApi->done();
	}

bool	Write::handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
							Oscl::Posix::Select::FileDescSets&			next,
							time_t&										timestamp
							) noexcept{
	if(!FD_ISSET(_writeFileDescriptor, &pending._writefds)){
		if(FD_ISSET(_writeFileDescriptor, &pending._exceptfds)){
			_doneApi	= &_failed;
			return true;
			}
		return false;
		}

	ssize_t
	nWritten	= ::write(_writeFileDescriptor,_buffer,_nRemainingToWrite);

	if((nWritten < 0) && (errno != EAGAIN)){
		_doneApi	= &_failed;
		return true;
		}

	if(nWritten < 0){
		// EAGAIN
		return false;
		}

	if(!nWritten){
		_doneApi	= &_failed;
		return true;
		}

	_nRemainingToWrite	-= nWritten;

	if(!_nRemainingToWrite){
		_doneApi	= &_succeeded;
		return true;
		}

	_buffer	+= nWritten;

	return false;
	}

bool    Write::fdIsClosed() noexcept{
	fd_set			fds;

	FD_ZERO(&fds);
	FD_SET(_writeFileDescriptor,&fds);

	struct timeval	timeout;
	timeout.tv_sec	= 0;
	timeout.tv_usec	= 0;

	int	result	= select(FD_SETSIZE,&fds,0,0, &timeout);

	if(result < 0){
		if(errno == EBADF){
			_doneApi	= &_failed;
			return true;
			}
		}

	return false;
	}

