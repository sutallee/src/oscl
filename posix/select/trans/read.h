/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_select_trans_readh_
#define _oscl_posix_select_trans_readh_

#include "oscl/posix/select/api.h"
#include "oscl/posix/select/handler.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {
/** */
namespace Transaction {

/**	This interface is used to add and remove select
	handlers from the select server.
 */
class Read: public  Oscl::Posix::Select::Handler {
	public:
		/**	The open and readable file descriptor
			from which the data used to fill the buffer
			will be read.
		 */
		const int					_readFileDescriptor;
		
		/**	The succeeded done operation of this interface
			will be invoked after all octets have
			been sucessfully read.
		 */
		Oscl::Done::Api&				_succeeded;

		/**	The failed done operation of this interface
			will be invoked if there is an error during
			the read transaction. In this case all of
			the specified octets have not been read
			into the buffer and the state of the buffer
			is unknown.
		 */
		Oscl::Done::Api&				_failed;

		/**	This done operation of this pointer
			will be invoked when the read transaction
			completes either successfully or as the
			result of an error. The value of the pointer
			depends upon the success or failure of the
			transaction.
		 */
		Oscl::Done::Api*				_doneApi;

		/**	A pointer to the buffer to be
			filled from the file descriptor.
		 */
		unsigned char*				_buffer;

		/** The number of octets remaining to be
			read from the file descriptor.
		 */
		unsigned					_nRemainingToRead;

		/** When true, the transaction may complete
			before all octets specified by nOctetsToRead
			are read.
		 */
		const bool					_allowFewerOctets;

	public:
		/**	The constructor requires:
			
			readFileDescriptor -
				The open and readable file descriptor
				from which data used to fill the bufer
				will be read.
			succeeded -
				The succeeded done operation of this
				interface will be invoked if all octets
				have been sucessfully read.
			failed -
				The failed done operation of this interface
				will be invoked if there is an error during
				the read transaction. In this case all of
				the specified octets have not been read
				into the buffer and the state of the buffer
				is unknown.
			buffer -
				A pointer to an array where octets read
				from the file descriptor will be written.
			nOctetsToRead -
				The number of octets to be read from the
				file descriptor and written to the buffer.
				NOTE!: This is *not* the maximum size of
				the buffer. This *is* the exact number of
				octets to be read before the transaction
				completes.
		 */
		Read(	int							readFileDescriptor,
				Oscl::Done::Api&			succeeded,
				Oscl::Done::Api&			failed,
				void*						buffer,
				unsigned					nOctetsToRead,
				bool						allowFewerOctets=false
				) noexcept;

		/** Invoking this operation causes the read
			process to start. When the read process
			is complete, either the succeeded or the failed
			done callback is invoked.
		 */
		void	start(Oscl::Posix::Select::Api& posixSelectApi) noexcept;

	public: // Oscl::Posix::Select::Handler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	start(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	stop(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	This operation is invoked whenever the select returns.
			The implementation inspects the pending file descriptor
			sets to see if any of its file descriptors need servicing.
			After servicing the appropriate file descriptors from the
			sets, the implementation may set or clear appropriate bits
			in the next file descriptor set.
		 */
		bool	handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
							Oscl::Posix::Select::FileDescSets&			next,
							time_t&										timestamp
							) noexcept;

		/**	This operation is invoked whenever the select returns
			with a EBADF error.
			The implementation must test to its file descriptors
			and return true if any of them are closed.

			RETURN: The handler returns TRUE if any of its file
			descriptors are closed, in which case it will
			subsequently be stopped by the server.
		 */
		bool	fdIsClosed() noexcept;

	public:
		/**
			When the allowFewerOctets flag is true,
			the client will invoke this function to
			determine the actual number of octets
			read after the transaction completes.
			WARNING: The result of this function is
			is *ONLY* valid if the allowFewerOctets
			is true and the read transaction completes
			successfully.
		 */
		unsigned	nOctetsActuallyRead() noexcept;
	};

}
}
}
}

#endif
