/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_select_trans_writeh_
#define _oscl_posix_select_trans_writeh_

#include "oscl/posix/select/api.h"
#include "oscl/posix/select/handler.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {
/** */
namespace Transaction {

/**	This interface is used to add and remove select
	handlers from the select server.
 */
class Write: public  Oscl::Posix::Select::Handler {
	public:
		/**	The open and writable file descriptor
			to which the buffer contents will be
			written.
		 */
		const int					_writeFileDescriptor;

		/**	The succeeded done operation of this interface
			will be invoked after all octets have
			been sucessfully written.
		 */
		Oscl::Done::Api&				_succeeded;

		/**	The failed done operation of this interface
			will be invoked if there is an error during
			the write transaction. In this case all of
			the specified octets have not been written
			to the file descriptor.
		 */
		Oscl::Done::Api&				_failed;

		/**	This done operation of this pointer
			will be invoked when the write transaction
			completes either successfully or as the
			result of an error. The value of the pointer
			depends upon the success or failure of the
			transaction.
		 */
		Oscl::Done::Api*				_doneApi;

		/**	A pointer to the octets still to be
			written to the file descriptor.
		 */
		const unsigned char*		_buffer;

		/** The number of octets remaining to be
			written to the file descriptor.
		 */
		unsigned					_nRemainingToWrite;

	public:
		/**	The constructor requires:
			
			writeFileDescriptor -
				The open and writable file descriptor
				to which the buffer contents will be
				written.
			succeeded -
				The succeeded done operation of this
				interface will be invoked if all octets
				have been sucessfully written.
			failed -
				The failed done operation of this interface
				will be invoked if there is an error during
				the write transaction. In this case all of
				the specified octets have not been written
				to the file descriptor.
			buffer -
				A pointer to an array of octets to be
				written to the file descriptor.
			nOctetsToWrite -
				The number of octets from the buffer to be
				written to the file descriptor.
		 */
		Write(	int							writeFileDescriptor,
				Oscl::Done::Api&			succeeded,
				Oscl::Done::Api&			failed,
				const void*					buffer,
				unsigned					nOctetsToWrite
				) noexcept;

		/** Invoking this operation causes the write
			transaction to start. When the write process
			is complete, either the succeeded or the failed
			done callback is invoked.
		 */
		void	start(Oscl::Posix::Select::Api& posixSelectApi) noexcept;

	public: // Oscl::Posix::Select::Handler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	start(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	stop(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	This operation is invoked whenever the select returns.
			The implementation inspects the pending file descriptor
			sets to see if any of its file descriptors need servicing.
			After servicing the appropriate file descriptors from the
			sets, the implementation may set or clear appropriate bits
			in the next file descriptor set.
		 */
		bool	handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
							Oscl::Posix::Select::FileDescSets&			next,
							time_t&										timestamp
							) noexcept;

		/**	This operation is invoked whenever the select returns
			with a EBADF error.
			The implementation must test to its file descriptors
			and return true if any of them are closed.

			RETURN: The handler returns TRUE if any of its file
			descriptors are closed, in which case it will
			subsequently be stopped by the server.
		 */
		bool	fdIsClosed() noexcept;
	};

}
}
}
}

#endif
