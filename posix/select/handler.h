/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_select_handlerh_
#define _oscl_posix_select_handlerh_

#include "oscl/posix/select/fdset.h"
#include <sys/select.h>

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Select {

/**	This interface is called by a select thread whenever
	the posix select call returns indicating a change
	in one of its file descriptors.
 */
class Handler {
	public:
		/** For use by the select thread to keep API in a list.
			The implementation of this interface should *never* use this field.
 		 */
		void*	__qitemlink;

	public:
		/** Make GCC happy */
		virtual ~Handler(){}

		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		virtual void	start(FileDescSets& next) noexcept=0;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		virtual void	stop(FileDescSets& next) noexcept=0;

		/**	This operation is invoked whenever the select returns.
			The implementation inspects the pending file descriptor
			sets to see if any of its file descriptors need servicing.
			After servicing the appropriate file descriptors from the
			sets, the implementation may set or clear appropriate bits
			in the next file descriptor set.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the select queue into which it is asserted
			as the result of being attached. This typically happens
			if the handler is designed to read or write a particular
			amount of data and then detach itself from the select
			loop. If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after returning.

			The handler returns FALSE if it wishes to remain active.
		 */
		virtual bool	handleIO(	const FileDescSets&	pending,
									FileDescSets&		next,
									time_t&				timestamp
									) noexcept = 0;

		/**	This operation is invoked whenever the select returns
			with a EBADF error.
			The implementation must test to its file descriptors
			and return true if any of them are closed.

			RETURN: The handler returns TRUE if any of its file
			descriptors are closed, in which case it will
			subsequently be stopped by the server.
		 */
		virtual bool	fdIsClosed() noexcept = 0;
	};

/** This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name clashes.
	This template solves the problem by implementing
	the interface and then invoking member function
	pointers in the context instead.
	One instance of this object is created in the context
	for each interface of this type used in the context.
	Each instance is constructed such that it refers to different
	member functions within the context.
	The interface of this object is passed to any
	object that requires the interface. When the
	object invokes any of the operations of the API,
	the corresponding member function of the context
	will subsequently be invoked.
 */
template <class Context>
class HandlerComposer : public Handler {
	public:
		/** This type defines the function signature
			for a context member operation that
			starts the Handler.
		 */
		typedef void    (Context::*Start)(FileDescSets& next);

		/** This type defines the function signature
			for a context member operation that
			starts the Handler.
		 */
		typedef void    (Context::*Stop)(FileDescSets& next);

		typedef bool	(Context::*HandleIO)(
							const FileDescSets&	pending,
							FileDescSets&		next,
							time_t&				timestamp
							);

		typedef bool	(Context::*FdIsClosed)();

	private:
        /** A reference to the Context, which contains
			the done callback member function.
		 */
		Context&			_context;

	private:
		/** A pointer to a member function within the 
			Context that will be invoked when the
			start operation of the interface
			inherited by this class is invoked.
		 */
		Start				_start;

		/** A pointer to a member function within the 
			Context that will be invoked when the
			stop operation of the interface
			inherited by this class is invoked.
		 */
		Stop				_stop;

		/** A pointer to a member function within the 
			Context that will be invoked when the
			handlIO operation of the interface
			inherited by this class is invoked.
		 */
		HandleIO			_handleIO;

		/** A pointer to a member function within the 
			Context that will be invoked when the
			fdIsClosed operation of the interface
			inherited by this class is invoked.
		 */
		FdIsClosed			_fdIsClosed;

	public:
		/** The constructor requires:
			context -
				A reference to the Context of which
				this class is a member and which contains
				the done callback member function.
			timeout-
				A pointer to a Context member function
				that is invoked when the timer expires.
		 */
		HandlerComposer(
			Context&							context,
			Start								start,
			Stop								stop,
			HandleIO							handleIO,
			FdIsClosed							fdIsClosed
			) noexcept;

	private: // Oscl::Posix::Select::Timer
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	start(FileDescSets& next) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	stop(FileDescSets& next) noexcept;

		/**	This operation is invoked whenever the select returns.
			The implementation inspects the pending file descriptor
			sets to see if any of its file descriptors need servicing.
			After servicing the appropriate file descriptors from the
			sets, the implementation may set or clear appropriate bits
			in the next file descriptor set.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the select queue into which it is asserted
			as the result of being attached. This typically happens
			if the handler is designed to read or write a particular
			amount of data and then detach itself from the select
			loop. If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after returning.

			The handler returns FALSE if it wishes to remain active.
		 */
		bool	handleIO(	const FileDescSets&	pending,
							FileDescSets&		next,
							time_t&				timestamp
							) noexcept;

		/**	This operation is invoked whenever the select returns
			with a EBADF error.
			The implementation must test to its file descriptors
			and return true if any of them are closed.

			RETURN: The handler returns TRUE if any of its file
			descriptors are closed, in which case it will
			subsequently be stopped by the server.
		 */
		bool	fdIsClosed() noexcept;
	};

template <class Context>
HandlerComposer<Context>::
HandlerComposer(
	Context&	context,
	Start		start,
	Stop		stop,
	HandleIO	handleIO,
	FdIsClosed	fdIsClosed
	) noexcept:
		_context(context),
		_start(start),
		_stop(stop),
		_handleIO(handleIO),
		_fdIsClosed(fdIsClosed)
		{
	}

template <class Context>
void	HandlerComposer<Context>::start(FileDescSets& next) noexcept{
	(_context.*_start)(next);
	}

template <class Context>
void	HandlerComposer<Context>::stop(FileDescSets& next) noexcept{
	(_context.*_stop)(next);
	}

template <class Context>
bool	HandlerComposer<Context>::handleIO(
			const FileDescSets&	pending,
			FileDescSets&		next,
			time_t&				timestamp
			) noexcept{
	return (_context.*_handleIO)(
		pending,
		next,
		timestamp
		);
	}

template <class Context>
bool	HandlerComposer<Context>::fdIsClosed() noexcept{
	return (_context.*_fdIsClosed)();
	}

}
}
}

#endif
