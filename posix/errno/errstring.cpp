/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <errno.h>
#include <string.h>
#include "errstring.h"

using namespace Oscl::Posix::Errno;

ErrString::ErrString() noexcept{
	}

const char*	ErrString::value() noexcept{
	_buffer[0]	= '\0';

	// LIE: the XSI-compliant version of strerror_r() is provided by default

	char*
	result	= strerror_r(
				errno,
				_buffer,
				sizeof(_buffer)
				);

	if(!result){
		return "Unexpected";
		}

	return _buffer;
	}

const char*	ErrString::value(int err) noexcept{
	_buffer[0]	= '\0';

	// LIE: the XSI-compliant version of strerror_r() is provided by default

	char*
	result	= strerror_r(
				err,
				_buffer,
				sizeof(_buffer)
				);

	if(!result){
		return "Unexpected";
		}

	return _buffer;
	}

