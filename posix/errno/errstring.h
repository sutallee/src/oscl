/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_errno_errstringh_
#define _oscl_posix_errno_errstringh_

/** */
namespace Oscl {

/** */
namespace Posix {

/** */
namespace Errno {

/** */
class ErrString {
	private:
		/** */
		char	_buffer[128];

	public:
		/** */
		ErrString() noexcept;

		/** Each invocation of this operation examines
			the errno variable and returns a string
			representation of its value. This is normally
			the string returned by the POSIX strerror_r()
			function. The MUST be assumed to have changed
			after this operation is invoked.

			WARNING: The storage for the string returned
			by this operation is only valid for
			the life of this object.
		 */
		const char*	value() noexcept;

		/** This variant assumes the user will supply
			the errno value. This allows the user to
			save the value of errno across calls to
			this function.

			WARNING: The storage for the string returned
			by this operation is only valid for
			the life of this object.
		 */
		const char*	value(int err) noexcept;
	};
}
}
}

#endif
