/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "address.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <resolv.h>

using namespace Oscl::Posix::Socket::IP;

Address::Address() noexcept{
	}

Address::Address(
		const char*	ipAddress,
		uint16_t	port,
		int			af
		) noexcept{
	construct(
		ipAddress,
		port,
		af
		);
	}

void	Address::construct(
			const char*	ipAddress,
			uint16_t	port,
			int			af
			) noexcept{

	void*	socketAddressField	= 0;

	if(af == AF_INET){
		_ipv4.sin_family	= af; 
		_ipv4.sin_port		= htons(port);
		socketAddressField	= &_ipv4.sin_addr;
		}
	else if(af == AF_INET6) {
		_ipv6.sin6_family	= af; 
		_ipv6.sin6_port		= htons(port);
		socketAddressField	= &_ipv6.sin6_addr;
		}
	else {
		Oscl::ErrorFatal::logAndExit(
			"%s: Invalid IP address family: %d\n",
			__PRETTY_FUNCTION__,
			af
			);
		}

	int
	success	= inet_pton(
				af,
				ipAddress,
				socketAddressField
				);

	if(!success){
		Oscl::ErrorFatal::logAndExit(
			"%s: inet_pton: invalid IP address: %s\n",
			__PRETTY_FUNCTION__,
			ipAddress
			);
		}
	}


socklen_t	Address::resolve(
				const char*	hostname,
				uint16_t	port,
				int			af,
				int			socketType
				) noexcept{

	memset(this, 0, sizeof(Address));

	struct addrinfo		hints;

	memset(&hints, 0, sizeof(struct addrinfo));

	hints.ai_family		= af;
	hints.ai_socktype	= socketType;
	hints.ai_flags		= 0;
	hints.ai_protocol	= 0;

	struct addrinfo*	list;

	int
	result	= getaddrinfo(hostname, 0, &hints, &list);
	if (result == EAI_AGAIN) {
		// This can happen if /etc/resolv.conf changes at
		// run-time. For example, if the user changes
		// the network interface.
		// The DNS res_init() is **not** thread-safe. Therefore
		// race-conditions may be introduced if more than one
		// thread attempts to resolve a name at the same time.
		// However, in our case, this should be infrequent.
		// A new (poorly documented) "thread-safe" version of
		// res_init() exists named res_ninit(). However, this
		// is not supported by getaddrinfo.
		::res_init();
		result	= getaddrinfo(hostname, 0, &hints, &list);
		}

	if (result != 0) {
		Oscl::Error::Info::log(
			"%s: getaddrinfo(): %s\n",
			__PRETTY_FUNCTION__,
			gai_strerror(result)
			);
		return 0;
		}

	if(!list){
		Oscl::Error::Info::log(
			"%s: list is NULL\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	if(!list->ai_addr){
		Oscl::Error::Info::log(
			"%s: ai_addr is NULL\n",
			__PRETTY_FUNCTION__
			);
		freeaddrinfo(list);
		return 0;
		}

	socklen_t	len	= list->ai_addrlen;

	if(len > sizeof(Address)){
		Oscl::Error::Info::log(
			"%s: ai_addrlen is too long\n",
			__PRETTY_FUNCTION__
			);
		freeaddrinfo(list);
		return 0;
		}

	memcpy(&_sock,list->ai_addr,len);

	switch(list->ai_family){
		case AF_INET:
			_ipv4.sin_port	= htons(port);
			break;
		case AF_INET6:
			_ipv6.sin6_port	= htons(port);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: ai_addrlen is too long\n",
				__PRETTY_FUNCTION__
				);
			return 0;
		}

	// dynamic memory: ick
	freeaddrinfo(list);

	return len;
	}

const char*	Address::ipAddress(
				char*	buffer,
				size_t	bufferSize
				) const noexcept {
	const void*	address	= 0;

	int
	af	= addressFamily();

	switch(af){
		case AF_INET:
			address	= &_ipv4.sin_addr;
			break;
		case AF_INET6:
			address	= &_ipv6.sin6_addr;
			break;
		default:
			return 0;
		}

	const char*
	ipAddress	= inet_ntop(
					af,
					address,
					buffer,
					bufferSize
					);

	return ipAddress;
	}

int	Address::addressFamily() const noexcept {
	return _sock.sa_family;
	}

const void*	Address::addressField() const noexcept{
	switch(_sock.sa_family){
		case AF_INET:
			return &_ipv4.sin_addr;
		case AF_INET6:
			return &_ipv6.sin6_addr;
		default:
			return 0;
		}
	return 0;
	}

unsigned	Address::addressFieldLength() const noexcept{
	switch(_sock.sa_family){
		case AF_INET:
			return sizeof(_ipv4.sin_addr);
		case AF_INET6:
			return sizeof(_ipv6.sin6_addr);
		default:
			return 0;
		}
	return 0;
	}

uint16_t	Address::clientPort() const noexcept{
	switch(_sock.sa_family){
		case AF_INET:
			return _ipv4.sin_port;
		case AF_INET6:
			return _ipv6.sin6_port;
		default:
			return 0;
		}
	return 0;
	}

