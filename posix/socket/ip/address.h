/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_socket_ip_addressh_
#define _oscl_posix_socket_ip_addressh_

#include <netinet/in.h>

/** */
namespace Oscl {

/** */
namespace Posix {

/** */
namespace Socket {

/** */
namespace IP {

/** This union can handle IPv4 (AF_INET) and IPv6 (AF_INET6)
	socket information.
 */
union Address {
	/** Common socket address */
	struct sockaddr		_sock;

	/** IPv4 socket address */
	struct sockaddr_in	_ipv4;

	/** IPv6 socket address */
	struct sockaddr_in6	_ipv6;

	/** */
	Address() noexcept;

	/** */
	Address(
		const char*	ipAddress,
		uint16_t	port,
		int			af
		) noexcept;

	/** This fills out the appropriate address
		based on the parameters.
	 */
	void	construct(
			const char*	ipAddress,
			uint16_t	port,
			int			af
			) noexcept;

	/** Configures the data member(s) of this union based
		on the resolved hostname and port number.
		DNS is consulted if the hostname is a URI.
	 */
	socklen_t	resolve(
					const char*	hostname,
					uint16_t	port,
					int			af = AF_UNSPEC,
					int			socketType = SOCK_STREAM
					) noexcept;

	/** RETURN: IP address string or zero on error.
	 */
	const char*	ipAddress(
					char*	buff,
					size_t	buffSize
					) const noexcept;

	/** RETURN: IP address family (AF_INET, AF_INET6, etc.)
	 */
	int	addressFamily() const noexcept;

	/** RETURN: IP address field address.
	 */
	const void*	addressField() const noexcept;

	/** RETURN: Length of the address field.
	 */
	unsigned	addressFieldLength() const noexcept;

	/** RETURN: client port in host byte order
	 */
	uint16_t	clientPort() const noexcept;

	};

}
}
}
}

#endif
