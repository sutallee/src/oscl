/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_socket_listener_parth_
#define _oscl_posix_socket_listener_parth_
#include "oscl/posix/select/handler.h"

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Socket {
/** */
namespace Listener {

/** The purpose of this part is to listen for incomming socket
	connection requests and to invoke an external interface that
	is responsible for spinning up a corresponding server.

	This part must share in the same thread
 */
class Part : public Oscl::Posix::Select::Handler {
	public:
		class SessionCreatorApi {
			public:
				/** Make compiler happy */
				virtual ~SessionCreatorApi(){}

				/** This operation is invoked to cause an implementation
					specific entity to spin-up a session that uses the
					new socketFD.
				 */
				virtual void	createSession(int socketFD) noexcept=0;
			};
	private:
		/** */
		SessionCreatorApi&		_sessionCreatorApi;

		/** */
		int						_socketFD;

	public:
		/** This constructor creates a listener that waits for
			connections on a TCP/IP port (AF_INET).
		 */
		Part(	SessionCreatorApi&	sessionCreatorApi,
				unsigned			tcpPort
				) noexcept;

		/** This constructor creates a listener that waits for
			connections on a UNIX socket (AF_UNIX).
		 */
		Part(	SessionCreatorApi&	sessionCreatorApi,
				const char*			unixSocketPathName
				) noexcept;

	private: // Oscl::Posix::Select::Handler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	start(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	stop(Oscl::Posix::Select::FileDescSets& next) noexcept;

		/**	This operation is invoked whenever the select returns.
			The implementation inspects the pending file descriptor
			sets to see if any of its file descriptors need servicing.
			After servicing the appropriate file descriptors from the
			sets, the implementation may set or clear appropriate bits
			in the next file descriptor set.
		 */
		bool	handleIO(	const Oscl::Posix::Select::
							FileDescSets&					pending,
							Oscl::Posix::Select::
							FileDescSets&					next,
							time_t&							timestamp
							) noexcept;

		/** */
		bool	fdIsClosed() noexcept;
	};

}
}
}
}


#endif
