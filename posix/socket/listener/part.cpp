/*
   Copyright (C) 2008 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "oscl/posix/errno/errstring.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

using namespace Oscl::Posix::Socket::Listener;

Part::Part(	SessionCreatorApi&	sessionCreatorApi,
			unsigned			tcpPort
			) noexcept:
		_sessionCreatorApi(sessionCreatorApi)
		{
	int	err;

	_socketFD	= socket(AF_INET,SOCK_STREAM,0);

	Oscl::Posix::Errno::ErrString	errStr;

	if(_socketFD < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}

	int
	opt	= 1;

	err	= setsockopt(
			_socketFD,
			SOL_SOCKET,
			SO_REUSEADDR,
			&opt,
			sizeof(opt)
			);

	if(err < 0){
		Oscl::Error::Info::log(
			"%s: setsockopt() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		}

	struct sockaddr_in				server_address;
	server_address.sin_family		= AF_INET;
	server_address.sin_addr.s_addr	= INADDR_ANY;
	server_address.sin_port			= htons(tcpPort);
	socklen_t
    server_len  = sizeof(server_address);

	err	= bind(_socketFD,(struct sockaddr*)&server_address,server_len);
	if(err < 0) {
		Oscl::ErrorFatal::logAndExit(
			"%s: bind() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}

	err	= listen(_socketFD,1);
	if(err < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: listen() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}

	}

Part::Part(	SessionCreatorApi&	sessionCreatorApi,
			const char*			unixSocketPathName
			) noexcept:
		_sessionCreatorApi(sessionCreatorApi)
		{
	int	err;

	_socketFD	= socket(AF_UNIX,SOCK_STREAM,0);

	Oscl::Posix::Errno::ErrString	errStr;

	if(_socketFD < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: socket() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}

	struct sockaddr_un				server_address;
	server_address.sun_family	= AF_UNIX;
	strncpy(	server_address.sun_path,
				unixSocketPathName,
				sizeof(server_address.sun_path)
				);
	server_address.sun_path[sizeof(server_address.sun_path)-1]	= '\0';

	socklen_t
    server_len  = sizeof(server_address);

	err	= bind(_socketFD,(struct sockaddr*)&server_address,server_len);
	if(err < 0) {
		Oscl::ErrorFatal::logAndExit(
			"%s: bind() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}

	err	= listen(_socketFD,1);
	if(err < 0){
		Oscl::ErrorFatal::logAndExit(
			"%s: listen() \"%s\"\n",
			__PRETTY_FUNCTION__,
			errStr.value()
			);
		return;
		}
	}

void	Part::start(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_SET(_socketFD,&next._readfds);
	}

void	Part::stop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_CLR(_socketFD,&next._readfds);
	}

bool	Part::handleIO(	const Oscl::Posix::Select::
						FileDescSets&					pending,
						Oscl::Posix::Select::
						FileDescSets&					next,
						time_t&							timestamp
						) noexcept{
	if( !FD_ISSET(_socketFD,&pending._readfds) ) {
		return false;
		}

	struct sockaddr_in	client_address;
	socklen_t			client_len = sizeof(client_address);

	int
	client_sockfd	= accept(	_socketFD,
								(struct sockaddr*)&client_address,
								&client_len
								);

	if(client_sockfd){
		_sessionCreatorApi.createSession(client_sockfd);
		}

	return false;
	}

bool	Part::fdIsClosed() noexcept{
	// FIXME
	return false;
	}

