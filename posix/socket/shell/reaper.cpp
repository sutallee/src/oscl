/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <sys/socket.h>
#include "service.h"
#include "reaper.h"
#include "oscl/strings/strip.h"

using namespace Oscl::Posix::Socket::Shell;

Reaper::Reaper(
			Service&	context,
			int			fd
			) noexcept:
		_context(context),
		_fd(fd),
		_readSuccess(
			*this,
			&Reaper::readSuccess
			),
		_readFailed(
			*this,
			&Reaper::readFailed
			),
		_readTrans(0),
		_framer(
			*this
			),
		_stopping(false)
		{

	_out	= fdopen(_fd,"a");
	}

void	Reaper::start() noexcept{

	startRead();

	prompt();
	}

void	Reaper::stop() noexcept{
	stopAll();
	}

void	Reaper::stopAll() noexcept{

	_stopping	= true;

	stopNext();
	}

void	Reaper::stopNext() noexcept{

	if(_readTrans){
		_context._posixSelectApi.detach(*_readTrans);
		}

	if(_fd > -1){
		fclose((FILE*)_out);
		_fd	= -1;
		}

	_context.reap(*this);
	}

void	Reaper::startRead() noexcept{

	if(_stopping){
		return;
		}

	if(_readTrans){
		return;
		}

	_readTrans =	new (&_readMem)
		Oscl::Posix::Select::Transaction::Read (
			_fd,
			_readSuccess,
			_readFailed,
			_readBuffer,
			sizeof(_readBuffer),
			true
			);

	_readTrans->start(_context._posixSelectApi);
	}

void	Reaper::readSuccess() noexcept{

	unsigned	nRead	= _readTrans->_nRemainingToRead;

	_readTrans	= 0;

	if(_stopping){
		return;
		}

	_framer.input(_readBuffer,nRead);

	startRead();
	}

void	Reaper::readFailed() noexcept{

	_readTrans	= 0;

	if(!_stopping){
		stopAll();
		}
	}

void	Reaper::prompt() noexcept{

	const char	prompt[] = {"# "};

	write(_fd,prompt,sizeof(prompt)-1);
	}

void    Reaper::print(
			const char*	format,
			...
			) noexcept{

	va_list	ap;

	va_start(ap, format);

	FILE*	out	= (FILE*)_out;

	vfprintf(
		out,
		format,
		ap
		);

	fflush(out);
	}

void    Reaper::print(
			const char*	format,
			va_list		ap
			) noexcept{

	FILE*	out	= (FILE*)_out;

	vfprintf(
		out,
		format,
		ap
		);

	fflush(out);
	}

void    Reaper::parse(const char* line) noexcept{

	const char*
	command	= Oscl::Strings::stripSpace(line);

	const char*
	args	= Oscl::Strings::stripNotSpace(command);

	ptrdiff_t
	len	= args-command;
	
	args	= Oscl::Strings::stripSpace(args);

	static const char	help[] = {"help"};

	if(!strncmp(command,help,len)){
		printHelp(args);
		prompt();
		return;
		}

	static const char	exit[] = {"exit"};

	if(!strncmp(command,exit,len)){
		print(
			"Good-bye.\n"
			);
		stopAll();
		return;
		}

	Command*
	cmd	= _context.find(command,len);

	if(!cmd){
		print(
			"invalid command: \"%s\"\n",
			command
			);
		prompt();
		return;
		}

	cmd->execute(args,*this);

	prompt();
	}

void	Reaper::printHelp(const char* args) noexcept{
	Command*	cmd;

	print(
		"%s - %s\n",
		"help",
		"Display this help message."
		);

	print(
		"%s - %s\n",
		"exit",
		"Close the shell connection."
		);

	for(
		cmd	= _context._commands.first();
		cmd;
		cmd	= _context._commands.next(cmd)
		){
		print(
			"%s - %s\n",
			cmd->getCommand(),
			cmd->getHelp()
			);
		}
	}

