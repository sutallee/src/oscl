/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdarg.h>
#include <new>

using namespace Oscl::Posix::Socket::Shell;

Service::Service(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Posix::Select::Api&	posixSelectApi,
	unsigned					tcpPort
	) noexcept:
		_papi(papi),
		_posixSelectApi(posixSelectApi),
		_closeSync(
			*this,
			papi
			),
		_listener(
			*this,
			tcpPort
			),
		_printSync(
			papi,
			*this
			),
		_attached(false),
		_opened(false)
		{
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::CloseSyncApi&	Service::getCloseSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&	Service::getOpenSAP() noexcept{
	return _closeSync.getOpenSAP();
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Service::getCloseSAP() noexcept{
	return _closeSync.getSAP();
	}

Oscl::Print::ITC::Api&	Service::getPrintApi() noexcept{
	return _printSync;
	}

void	Service::print(
			const char*	format,
			...
			) noexcept{
	// FIXME:
	// This is NOT thread safe!
	// This function should use synchronous
	// ITC instead of a direct call.
	va_list	ap;
	va_start(ap,format);
	print(format,ap);
	va_end(ap);
	}

void	Service::print(
			const char*	format,
			va_list		ap
			) noexcept{
	_printSync.print(format,ap);
	}

void Service::reap(Reaper& reaper) noexcept{

	_activeReapers.remove(&reaper);

	reaper.~Reaper();

	_freeReaperMem.put((ReaperMem*)&reaper);

	processClosed();
	}

void	Service::add(Command& cmd) noexcept{
	_commands.put(&cmd);
	}

Command*	Service::find(
				const char*	command,
				unsigned	len
				) noexcept{
	Command*	cmd;

	for(
		cmd	= _commands.first();
		cmd;
		cmd	= _commands.next(cmd)
		){
		if(!strncmp(command,cmd->getCommand(),len)){
			return cmd;
			}
		}

	return 0;
	}

void	Service::createSession(
			int			fd
			) noexcept{

	if(!_opened){
		::close(fd);
		return;
		}

	ReaperMem*
	mem	=	_freeReaperMem.get();

	Reaper*	reaper	= 0;

	if(mem){
		reaper	= new(mem)Reaper(
					*this,
					fd
					);
		}
	else {
		reaper	= new Reaper(
					*this,
					fd
					);
		}

	if(!reaper){
		::close(fd);
		return;
		}

	_activeReapers.put(reaper);

	reaper->start();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{
	if(!_attached){
		_posixSelectApi.attach(_listener);
		_attached	= true;
		}

	_opened	= true;

	msg.returnToSender();
	}

void	Service::processClosed() noexcept{
	if(!_pendingCloseReq.first()){
		// We're not closing.
		return;
		}

	if(_activeReapers.first()){
		// We don't finish closing
		// unless all active reapers
		// have been reaped.
		return;
		}

	// All closed.

	Oscl::Mt::Itc::Srv::
	Close::Req::Api::CloseReq*	closeReq;

	while((closeReq=_pendingCloseReq.get())){
		closeReq->returnToSender();
		}

	// FIXME: At this point, we *may* want to
	// free all of the ReaperMem in the
	// _freeReaperMem list.
	// Alternately, we *may* want to keep it
	// until the destructor is called to
	// in case the service is to be re-opened
	// later.
	// This is a trade-off between increasing
	// the probablity of heap fragmentation
	// versus collecting unused memory.
	//
	// Since closing this service will rarely
	// be done in practice, it probably doesn't
	// matter much.

	_opened	= false;
	}

void	Service::request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept{

	if(!_opened){
		msg.returnToSender();
		return;
		}

//	_posixSelectApi.detach(_listener);

	_pendingCloseReq.put(&msg);

	for(
		Reaper* reaper	= _activeReapers.first();
		reaper;
		reaper	= _activeReapers.next(reaper)
		){
		reaper->stop();
		}

	processClosed();
	}

void	Service::request(Oscl::Print::ITC::Req::Api::PrintReq& msg) noexcept{

	Oscl::Print::ITC::Req::Api::PrintPayload&
	payload	= msg.getPayload();

	Reaper*	reaper;

	for(
		reaper	= _activeReapers.first();
		reaper;
		reaper	= _activeReapers.next(reaper)
		){
		reaper->print(
			payload._format,
			payload._ap
			);
		}

	msg.returnToSender();
	}

