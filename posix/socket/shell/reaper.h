/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_socket_shell_reaperh_
#define _oscl_posix_socket_shell_reaperh_

#include "oscl/posix/select/trans/write.h"
#include "oscl/posix/select/trans/read.h"
#include "oscl/done/operation.h"
#include "oscl/memory/block.h"
#include "oscl/posix/select/trans/read.h"
#include "oscl/strings/fixed.h"

//#include "oscl/mt/itc/delay/respcomp.h"
//#include "oscl/mt/itc/delay/respmem.h"
#include "oscl/stream/framer/line/config.h"
#include "oscl/print/api.h"

/** */
namespace Oscl {

/** */
namespace Posix {

/** */
namespace Socket {

/** */
namespace Shell {

/** */
class Service;

/** */
class Reaper:
	public Oscl::Stream::Line::Parse::Api,
	public Oscl::Print::Api
	{
	public:
		/** */
		void*								__qitemlink;

	private:
		/** */
		Service&							_context;

		/** */
		int									_fd;

	private:
		/** */
		Oscl::Memory::AlignedBlock<
			sizeof(
				Oscl::Posix::Select::
				Transaction::Read
				)
			>								_readMem;

		/** */
		Oscl::Done::Operation<Reaper>		_readSuccess;

		/** */
		Oscl::Done::Operation<Reaper>		_readFailed;

		/** */
		unsigned char						_readBuffer[64];

		/** */
		Oscl::Posix::Select::
		Transaction::Read*					_readTrans;

	private:
		/** */
		Oscl::Stream::Framer::Line::Config<1024>	_framer;

	private:
		/** */
		void*								_out;

		/** */
		bool								_stopping;

	public:
		/** */
		Reaper(
			Service&	context,
			int			fd
			) noexcept;

		/** */
		void start() noexcept;

		/** */
		void stop() noexcept;

	private:
		/** */
		void	stopAll() noexcept;

		/** */
		void	startRead() noexcept;

		/** */
		void	stopNext() noexcept;

	private:
		/** */
		void	readSuccess() noexcept;

		/** */
		void	readFailed() noexcept;

	private:
		/** */
		void	prompt() noexcept;

	public: // Oscl::Print::Api
		/** */
		void    print(
					const char*	format,
					...
					) noexcept __attribute__ ((format (printf, 2, 3)));

		/** */
		void    print(
					const char*	format,
					va_list		ap
					) noexcept;

	private: // Oscl::Stream::Line::Parse::Api
		/** */
		void    parse(const char* line) noexcept;

		/** */
		void	printHelp(const char* args) noexcept;

	};
}
}
}
}

#endif
