/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_socket_shell_serviceh_
#define _oscl_posix_socket_shell_serviceh_

#include "oscl/mt/itc/srv/close.h"
#include "oscl/posix/select/mbox/server.h"
#include "oscl/posix/socket/listener/part.h"
#include "oscl/queue/queue.h"
#include "oscl/print/itc/sync.h"
#include "reaper.h"
#include "cmd.h"

/** */
namespace Oscl {

/** */
namespace Posix {

/** */
namespace Socket {

/** */
namespace Shell {

/** */
class Service :
		private Oscl::Mt::Itc::Srv::Close::Req::Api,
		private Oscl::Posix::Socket::Listener::Part::SessionCreatorApi,
		public Oscl::Print::Api,
		private Oscl::Print::ITC::Req::Api
		{
	friend	class Reaper;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_papi;

		/** */
		Oscl::Posix::Select::Api&				_posixSelectApi;

	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync			_closeSync;

	private:
		/** */
		Oscl::Posix::Socket::Listener::Part		_listener;

	private:
		/** */
		Oscl::Print::ITC::Sync					_printSync;

	private:
		/** */
		union ReaperMem{
			/** */
			void*	__qitemlink;

			/** */
			Oscl::Memory::AlignedBlock<sizeof(Reaper)>	_mem;
			};

		/** */
		Oscl::Queue<ReaperMem>					_freeReaperMem;

	private:
		/** */
		Oscl::Queue<Reaper>						_activeReapers;

	private:
		/** */
		Oscl::Queue<Command>					_commands;

	private:
		/** */
		Oscl::Queue<
			Oscl::Mt::Itc::Srv::
			Close::Req::Api::CloseReq
			>									_pendingCloseReq;

		/** */
		bool									_attached;

		/** */
		bool									_opened;

	public:
		/** */
		Service(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Posix::Select::Api&				posixSelectApi,
			unsigned								tcpPort
			) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::CloseSyncApi&	getCloseSyncApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&	getOpenSAP() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

		/** */
		Oscl::Print::ITC::Api&	getPrintApi() noexcept;

		/** Clients MUST only call before open.
		 */
		void	add(Command& cmd) noexcept;

	public: // Oscl::Print::Api
		/** */
		void	print(
					const char*	format,
					...
					) noexcept __attribute__ ((format (printf, 2, 3)));

		/** */
		void	print(
					const char*	format,
					va_list		ap
					) noexcept;

	private:
		/** */
		void	reap(Reaper& reaper) noexcept;

		/** */
		void	processClosed() noexcept;

	private:
		/** */
		Command*	find(
						const char* command,
						unsigned	len
						) noexcept;

	private: // Oscl::Posix::Socket::Listener::Part::SessionCreatorApi
		/** */
		void	createSession(
					int			fd
					) noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept;

	private: // Oscl::Print::ITC::Req::Api
		/**	*/
		void	request(Oscl::Print::ITC::Req::Api::PrintReq& msg) noexcept;
	};

}
}
}
}

#endif
