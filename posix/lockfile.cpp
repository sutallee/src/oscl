/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "lockfile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>


using namespace Oscl::Posix;

LockFile::LockFile(const char* lockFileName) noexcept:
		_fd(-1) {
	_fd	= open(lockFileName,O_CREAT | O_RDWR,0777);
	if(_fd < 0){
		perror("Open failed");
		exit(1);
		}
	}

LockFile::~LockFile() {
	close(_fd);
}

static void	doLockOperation(int fd,short type)
{
	int				stat;
	struct flock	lock;

	lock.l_type		= type;
	lock.l_whence	= SEEK_SET;

	/*file's beginning*/
	lock.l_start	= 0;

	/*start at offset 0 from SEEK_SET*/
	lock.l_len	= 1;
	lock.l_pid	= getpid();

	while( (stat = fcntl(fd, F_SETLKW, &lock) != 0) ){
		if(errno != EINTR){
			perror("fcntl failed");
			exit(1);
			}
		}
	}

void	LockFile::lock() noexcept {
	doLockOperation(_fd,F_WRLCK);
	}

void	LockFile::unlock() noexcept {
	doLockOperation(_fd,F_UNLCK);
	}

