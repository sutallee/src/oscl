/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "execute.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include "oscl/strings/fixed.h"
#include "oscl/strings/tokens.h"
#include "oscl/error/fatal.h"
#include "oscl/strings/dynamic.h"

using namespace Oscl;

extern char	**environ;

/** Despite the non const-ness of the character pointer,
	the source string will not be modified by the program
	that is executed in the execve() routine since it
	executes in a new process... I think.
 */
int ExecuteProgram::sync (const char *commandLine) {
	int			pid;
	int			status;
	if (!commandLine){
		return 1;
		}
	pid = fork();
	if (pid == -1){
		static const char	errorString[] = {"ExecuteProgramSynchronously: fork() failed.\n"};
		Strings::Fixed<255>	s(errorString);
		ErrorFatal::logAndExit(s);
		}
	if (pid == 0) {
		Strings::Tokens	tokens(commandLine);
		Strings::Node*		next;
		unsigned		count=0;
		for(next=tokens.first();next;next=tokens.next(next),++count);
		if(!count) return 0;
		Strings::Dynamic	program(tokens.first()->getString());
		char**		argv	= (char**) malloc(sizeof(char*)*(count+1));
		if(!argv){
			static const char	errorString[] = {"ExecuteProgramSynchronously: Out of Memory.\n"};
			Strings::Fixed<255>	s(errorString);
			ErrorFatal::logAndExit(s);
			}
		for(unsigned i=0;(next=tokens.get());++i){
			argv[i]	= const_cast<char*>(next->getString());
			}
		argv[count]	= 0;
		execve(program, argv, environ);
		static const char	errorString[] = {"ExecuteProgramSynchronously: execve() failed.\n"};
		Strings::Fixed<255>	s(errorString);
		ErrorFatal::logAndExit(s);
		}
	while(true) {
		if (waitpid(pid, &status, 0) == -1) {
			if (errno != EINTR){
				return -1;
				}
			}
		else {
			return status;
			}
		}
	}

int ExecuteProgram::sync (const Strings::Api& commandLine) {
	return sync(commandLine.getString());
	}

