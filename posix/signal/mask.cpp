/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "mask.h"
#include <stdio.h>

using namespace Oscl::Posix::Signal;

Mask::Mask(bool maskAllSignals) noexcept
{
	if(maskAllSignals){
		maskAll();
		}
	else {
		maskNone();
		}
}

void	Mask::maskNone() noexcept{
	if(sigemptyset(&_mask)){
		perror("maskNone");
		}
	}

void	Mask::maskAll() noexcept{
	if(sigfillset(&_mask)){
		perror("maskAll");
		}
	}

void	Mask::includeInMask(int num) noexcept{
	if(sigaddset(&_mask,num)){
		perror("includeInMask");
		}
	}

void	Mask::removeFromMask(int num) noexcept{
	if(sigdelset(&_mask,num)){
		perror("removeFromMask");
		}
	}

bool	Mask::isMember(int num) const noexcept{
	int result;
	result = sigismember(&_mask,num);
	if(result < 0){
		perror("isMember");
		return false;
		}
	return result?true:false;
	}

void Mask::disable(Mask& previous) const noexcept {
	if(sigprocmask(SIG_BLOCK,&_mask,&previous._mask)){
		perror("disable");
		}
   }

void Mask::enable(Mask& previous) const noexcept {
	if(sigprocmask(SIG_UNBLOCK,&_mask,&previous._mask)){
		perror("enable");
		}
   }

void Mask::set(Mask& previous) const noexcept {
	if(sigprocmask(SIG_SETMASK,&_mask,&previous._mask)){
		perror("set");
		}
   }

void Mask::disable() const noexcept {
	if(sigprocmask(SIG_BLOCK,&_mask,0)){
		perror("disable");
		}
   }

void Mask::enable() const noexcept {
	if(sigprocmask(SIG_UNBLOCK,&_mask,0)){
		perror("enable");
		}
   }

void Mask::set() const noexcept {
	if(sigprocmask(SIG_SETMASK,&_mask,0)){
		perror("set");
		}
   }

