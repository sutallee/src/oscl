/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_signalh_
#define _oscl_posix_signalh_
#include <signal.h>

/** */
namespace Oscl {
/** */
namespace Posix {
/** */
namespace Signal {

/** */
class Mask {
	private:
		/** */
		sigset_t	_mask;

	public:
		/** */
		Mask(bool maskAllSignals=false) noexcept;
		/** sigemptyset */
		void	maskNone() noexcept;
		/** sigfillset */
		void	maskAll() noexcept;
		/** sigaddset */
		void	includeInMask(int num) noexcept;
		/** sigdelset */
		void	removeFromMask(int num) noexcept;
		/** sigismember */
		bool	isMember(int num) const noexcept;
		/** sigprocmask */
		void disable(Mask& previous) const noexcept;
		/** sigprocmask */
		void disable() const noexcept;
		/** sigprocmask */
		void enable(Mask& previous) const noexcept;
		/** sigprocmask */
		void enable() const noexcept;
		/** sigprocmask */
		void set(Mask& previous) const noexcept;
		/** sigprocmask */
		void set() const noexcept;
	};

}
}
}

#endif
