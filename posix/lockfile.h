/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_filelockh_
#define _oscl_posix_filelockh_

#include "oscl/mt/mutex/simple.h"

/** */
namespace Oscl {

/** */
namespace Posix {

/** The purpose of this class is to provide a resource
	mutex mechanism that uses a POSIX file system
	mechanism.
*/
class LockFile : public Oscl::Mt::Mutex::Simple::Api {
	private:
		/*
		Lockfile file descriptor.
		*/
		int	_fd;
	public:
		/** */
		LockFile(const char* lockFileName) noexcept;

		/** */
		~LockFile();

	public:	// Oscl::Mt::Mutex::Simple::Api
		/** The lock primitive is invoked by the user prior to
		 *	entering a critical section, to prevent other
		 *	threads of execution from manipulating data at
		 *	the same time. Typically, the current exclusion
		 *	state is saved before being changed.
		 */
		void	lock() noexcept;

		/** The unlock primitive is invoked by the user at
		 *	the end of a critical section to restore the
		 *	exclusion state using a value saved during
		 *	the lock operation.
		 */
		void	unlock() noexcept;
	
	};

}
}

#endif
