/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <alloca.h>
#include <string.h>
#include <errno.h>

int	OsclPosixMkdir(char* path, mode_t mode) {
	int	result;

	/*
		The p pointer always points to the start
		of the duplicated path string.
	*/

	char*	p	= path;

	for(; (p = strpbrk(p,"/")) ; ++p) {

		/*
			Skip a leading '/'
		*/
		if(path == p) continue;

		/*
			Terminate the string to this point.
		*/
		p[0]	= 0;

		/*
			Create the leaf directory
		*/
	    result  = mkdir(path,mode);

		/*
			Replace the directory separator.
		*/
		*p	= '/';

		if((result < 0) && (errno != EEXIST)) {
			return result;
			}
		}

	/*
		If there was no trailing '/' then we
		need to create the leaf directory here.
	*/
	unsigned	len	= strlen(path);
	if(len && (path[len-1] != '/')) {
	    result  = mkdir(path,mode);
		if((result < 0) && (errno != EEXIST)) {
			return result;
			}
		}

	return 0;
	}

int	OsclPosixMkdirConst(const char* path, mode_t mode) {
	char*	writableCopyOfPath	= (char*)alloca(strlen(path)+1);

	writableCopyOfPath[0]	= '\0';

	strcat(writableCopyOfPath,path);

	return OsclPosixMkdir(writableCopyOfPath,mode);
	}

int	OsclPosixMkdirFileLeaf(char* path, mode_t mode) {
	int	result;

	char*	p	= strrchr(path,'/');
	if(!p)
	{
		/*
			There is no directory to create.
		*/
		return 0;
	}

	if(p[1] == '\0')
	{
		/*
			There is a trailing '/' and thus the
			leaf is either NOT a file name, or it
			is a file name with an invalid character.

			The calling program is misusing this function.
		*/
		return 1;
	}

	/*
	Temporarily hide the file name from the
	path specification.
	*/
	p[0]	= '\0';

	/*
		Attempt to create the directory(s).
	*/
	result	= OsclPosixMkdir(path,mode);

	/*
		Restore the file name visibility.
	*/
	p[0]	= '/';

	return result;
	}

int	OsclPosixMkdirFileLeafConst(const char* path, mode_t mode) {

	char*	writableCopyOfPath	= (char*)alloca(strlen(path)+1);

	writableCopyOfPath[0]	= '\0';

	strcat(writableCopyOfPath,path);

	return OsclPosixMkdirFileLeaf(writableCopyOfPath,mode);
	}

