/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_posix_mkdirh_
#define _oscl_posix_mkdirh_

#ifdef __cplusplus
extern "C" {
#endif

/**
	This function creates the directory(s) specified by the path
	argument. Each directory in the path is created with the specified
	mode unless it already exists.

	The leaf of the path specification is assumed to be a
	directory, NOT a file name. If the path specification
	includes a file name, then use the OsclPosixMkdirFile
	instead.

	While the path variable is writeable, the value of memory
	occupied by the string is guaranteed to be identical when
	it is returned.

	The value returned by this function is the zero for
	success, or non-zero otherwise. In the case of a
	non-zero return value, errno is set to the value
	returned by the mkdir system call.
*/
int	OsclPosixMkdir(char* path, mode_t mode);

/**
	This function creates the directory(s) specified by the path
	argument. Each directory in the path is created with the specified
	mode unless it already exists.

	The leaf of the path specification is assumed to be a
	directory, NOT a file name. If the path specification
	includes a file name, then use the OsclPosixMkdirFileConst
	instead.

	WARNING: The implementation of this function dynamically
	allocates stack space that is equal to the length of the
	path string plus the string terminator.

	The value returned by this function is the zero for
	success, or non-zero otherwise. In the case of a
	non-zero return value, errno is set to the value
	returned by the mkdir system call.
*/
int	OsclPosixMkdirConst(const char* path, mode_t mode);

/**
	This function creates the directory(s) specified by the path
	argument. Each directory in the path is created with the specified
	mode unless it already exists.

	The leaf of the path specification is assumed to be a
	file, NOT a directory name. If the path specification
	includes directory names only, then use the
	OsclPosixMkdir function instead.

	The value returned by this function is the zero for
	success, or non-zero otherwise. In the case of a
	non-zero return value, errno is set to the value
	returned by the mkdir system call.
*/
int	OsclPosixMkdirFileLeaf(char* path, mode_t mode);

/**
	This function creates the directory(s) specified by the path
	argument. Each directory in the path is created with the specified
	mode unless it already exists.

	The leaf of the path specification is assumed to be a
	file, NOT a directory name. If the path specification
	includes directory names only, then use the
	OsclPosixMkdirConst function instead.

	WARNING: The implementation of this function dynamically
	allocates stack space that is equal to the length of the
	path string plus the string terminator.

	The value returned by this function is the zero for
	success, or non-zero otherwise. In the case of a
	non-zero return value, errno is set to the value
	returned by the mkdir system call.
*/
int	OsclPosixMkdirFileLeafConst(const char* path, mode_t mode);

#ifdef __cplusplus
}
#endif

#endif
