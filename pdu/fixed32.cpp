/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fixed32.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Pdu;
using namespace Oscl;

Fixed32::Fixed32(FreeStore::Mgr& mgr) noexcept :
		Leaf(mgr),
		_offset(0),
		_length(0)
		{
	}

void Fixed32::operator delete(void *p,size_t size) noexcept{
		((Fixed32*)p)->fsmfree(p);
		}

unsigned	Fixed32::stripHeader(unsigned nOctets) noexcept{
	if(_length < nOctets){
		nOctets	= _length;
		}
	_offset	+= nOctets;
	_length	-= nOctets;
	return nOctets;
	}

unsigned	Fixed32::stripTrailer(unsigned nOctets) noexcept{
	if(_length < nOctets){
		nOctets	= _length;
		}
	_length	-= nOctets;
	return nOctets;
	}

const uint8_t*	Fixed32::getBuffer(	unsigned	offset,
									unsigned&	length
									) const noexcept{
	unsigned	calcOffset	= _offset+offset;

	if(calcOffset > (_size-1)){
		Oscl::ErrorFatal::logAndExit("Oscl::Pdu::Fixed32: calcOffset beyond buffer size\n");
		return 0;
		}

	length	= _length-calcOffset;

	if((_length-calcOffset) > _length) return (uint8_t*)0;

	return &_data[calcOffset];
	}

uint8_t*	Fixed32::getBuffer(	unsigned	offset,
							unsigned&	length
							) noexcept{
	unsigned	calcOffset	= _offset+offset;

	if(calcOffset > (_size-1)){
		Oscl::ErrorFatal::logAndExit("Oscl::Pdu::Fixed32: calcOffset beyond buffer size\n");
		return 0;
		}

	length	= _length-calcOffset;

	if((_length-calcOffset) > _length) return (uint8_t*)0;

	return &_data[calcOffset];
	}

uint8_t*		Fixed32::getBuffer() noexcept{
	return _data;
	}

const uint8_t*	Fixed32::getBuffer() const noexcept{
	return _data;
	}

unsigned	Fixed32::getBufferSize() noexcept{
	return _size;
	}

unsigned	Fixed32::getContentStart() const  noexcept{
	return _offset;
	}

unsigned	Fixed32::getContentLength() const  noexcept{
	return _length;
	}

void		Fixed32::addLength(unsigned length) noexcept{
	_length	+= length;
	}
