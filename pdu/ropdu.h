/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_ropduh_
#define _oscl_krux_pdu_ropduh_
#include <stdint.h>
#include "oscl/handle/refcount.h"
#include "oscl/freestore/object.h"
#include "oscl/handle/hqueue.h"

/** */
namespace Oscl {
/** */
namespace Pdu {

class Visitor;
class ReadOnlyVisitor;

/** This abstract class represents a protocol data unit that is passed
	betwen two layers in a protocol implementation.
 */

class ReadOnlyPdu :	public FreeStore::Object,
					public RefCount
					{
	public:
		/** The constructor needs a reference to a freestore manager which
			is used to free the pdu to the correct memory pool when it is
			deleted by an anonymous client.
		 */
		ReadOnlyPdu(FreeStore::Mgr& mgr) noexcept;

		/** This abstract operation is called when the PDU is visited by
			a client to traverse the PDU tree from beginning to end.
			The position argument is the offset from the very first octet
			in the super PDU. The first valid
			octet (from the client perspective) is the offset from the
			firstValid argument. The nValid is the number of valid octets
			in this PDU subtree.
		 */
		virtual bool /* stop */ visit(	ReadOnlyVisitor&	v,
										unsigned			position,
										unsigned			firstValid,
										unsigned			nValid
										) const noexcept=0;

		/** This abstract operation returns the length of the PDU in octets.
		 */
		virtual unsigned length() const noexcept=0;

		/** This operation reads up to max number of bytes into the octet
			buffer referenced by the data octet pointer. The source octets
			are read beginning from the offset.
		 */
		virtual unsigned 	read(	void*		data,
									unsigned	max,
									unsigned	offset
									) const noexcept=0;
	};

}
}

#endif
