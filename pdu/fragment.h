/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_fragmenth_
#define _oscl_krux_pdu_fragmenth_

#include <stdint.h>
#include "oscl/handle/handle.h"
#include "oscl/handle/hqueue.h"
#include "oscl/freestore/object.h"

/** */
namespace Oscl {
/** */
namespace Pdu {

class Pdu;
class Fragment;
class Visitor;
class ReadOnlyVisitor;

/** This handle class is used wrap another Pdu in such a way that the Pdu
	can be used as an sdu in a composite protocol data unit. The wrapped
	Pdu is protected from changes in its length, offset, and list membership
	properties.
 */

class Fragment :	public FreeStore::Object,
					public RefCount,
					public HQueueItem<Fragment> {
	private:
		/** */
		Handle<Pdu>	_handle;

		/** The offset in octets from the beginning of the
			contained packet to the start of that portion of
			the packet used by the containing packet.
		 */
		unsigned	_offset;

		/** The number of octets in the wrapped packet that are
			used by the containing packet beginning from the offset
			described above.
		 */
		unsigned	_length;

	public:
		/** Requires a pointer to the wrapped Pdu.
		 */
		Fragment(FreeStore::Mgr& fsm,Pdu *pdu=0) noexcept;

		/** Requires a pointer to the wrapped Pdu.
		 */
		Fragment(FreeStore::Mgr& fsm,Handle<Pdu>& pdu) noexcept;

		/** Deep copy */
		void	copy(const Handle<Fragment>& other) noexcept;

		/** Returns the offset into the contained PDU that marks
			the beginning of this fragment.
		 */
		inline unsigned	getOffset() const noexcept {return _offset;}

		/** Returns the length of the contained PDU that begins
			with the offset of this fragment.
		 */
		inline unsigned	getLength() const noexcept { return _length;}

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end. The position
			argument is the offset from the very first octet in the super
			PDU. The first valid octet (from the client perspective) is
			the offset from the firstValid argument. The nValid is the
			number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	ReadOnlyVisitor&	v,
								unsigned			position,
								unsigned			firstValid,
								unsigned			nValid
								) const noexcept;

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end. The position
			argument is the offset from the very first octet in the super
			PDU. The first valid octet (from the client perspective) is
			the offset from the firstValid argument. The nValid is the
			number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	Visitor&	v,
								unsigned	position,
								unsigned	firstValid,
								unsigned	nValid
								) noexcept;

		/** This operation adjusts the packet offsets to strip the
			specified number of octets from the start of the packet.
			This operation returns the actual number of octets stripped.
		 */
		unsigned /* nStripped */	stripHeader(unsigned n) noexcept;

		/** This operation adjusts the packet offsets to strip the
			specified number of octets from the end of the packet.
			This operation returns the actual number of octets stripped.
		 */
		unsigned /* nStripped */	stripTrailer(unsigned n) noexcept;

		/** This operation reads up to max number of bytes into the octet
			buffer referenced by the data octet pointer. The source octets
			are read beginning from the offset.
		 */
		unsigned 	read(	void*		data,
							unsigned	max,
							unsigned	offset
							) const noexcept;

		/** The overloaded delete operator is required of all concrete
			derivatives of Pdu, which is derived from a FreeStore::Object.
		 */
		void	operator delete(void *p,size_t size) noexcept;

	};

}
}

#endif
