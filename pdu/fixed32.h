/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_fixed32h_
#define _oscl_krux_pdu_fixed32h_
#include "leaf.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Pdu {

/** This Pdu derivative contains a buffer of a fixed length.
 */
class Fixed32 : public Leaf {
	private:
		/** This is the length of the buffer.
		 */
		static const unsigned	_size=32;

		/** This array of octets contains the data for this pdu.
		 */
		uint8_t		_data[_size];

		/** The offset in octets from the beginning of the pdu to
			the start of that portion of the valid pdu data.
		 */
		unsigned	_offset;

		/** The number of valid octets in the pdu
			beginning from the offset described above.
		 */
		unsigned	_length;

	public:
		/** All Pdu derivatives require a freestore manager used to free the
			memory when the Pdu is deleted.
		 */
		Fixed32(FreeStore::Mgr& mgr) noexcept;

		/** All concrete derivatives of Pdu are required to implement a
			delete operator which invokes the Pdu's fsmfree() function.
		 */
		void	operator delete(void *p,size_t size) noexcept;

		/** Adjusts the PDU such that the first nOcets are "deleted"
			from the PDU.
		 */
		unsigned	stripHeader(unsigned nOctets) noexcept;

		/** Adjusts the PDU such that the last nOctets are "deleted"
			from the PDU.
		 */
		unsigned	stripTrailer(unsigned nOctets) noexcept;

		/** This operation returns a pointer to the octet buffer implemented
			by this leaf node beginning at the specified offset. The number
			of octets in the buffer is returned in the length argument.
		 */
		const uint8_t*	getBuffer(	unsigned	offset,
									unsigned&	length
									) const noexcept;

		/** This abstract operation returns a pointer to the octet buffer
			implemented by this leaf node beginning at the specified offset,
			containing the number of octets specified by the length argument.
			An out_of_range "exception" is thrown if the specified offset
			and length are not within the bounds of the buffer.
		 */
		uint8_t*	getBuffer(	unsigned	offset,
							unsigned&	length
							) noexcept;

	protected:
		/** This abstract operation must be implemented by sub-classes
			to return a pointer to the start of the buffer.
		 */
		uint8_t*		getBuffer() noexcept;

		/** This abstract operation must be implemented by sub-classes
			to return a pointer to the start of the buffer.
		 */
		const uint8_t*		getBuffer() const noexcept;

		/** This abstract operation must be implemented by sub-classes
			to return the maximum size of the buffer contained by this
			leaf node.
		 */
		unsigned	getBufferSize() noexcept;

		/** This abstract operation must be implemented by sub-classes
			to return the offset from the beginning of the buffer to
			the first valid octet in the buffer.
		 */
		unsigned	getContentStart() const noexcept;

		/** This abstract operation must be implemented by sub-classes
			to return the number of valid octets contained in the buffer.
		 */
		unsigned	getContentLength() const noexcept;

		/** This abstract operation must be implemented by sub-classes
			to add the argument length to the current content length
			of the buffer.
		 */
		void		addLength(unsigned length) noexcept;
	};

}
}

#endif
