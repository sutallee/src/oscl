/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_memory_itc_reqapih_
#define _oscl_pdu_memory_itc_reqapih_

#include "oscl/mt/itc/mbox/msg.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Pdu {
/** */
namespace Memory {
/** */
namespace ITC {
/** */
namespace Req {

/** */
class Api {
	public:
		class FreeReq : public Oscl::Mt::Itc::Msg {
			public:
				/** */
				Api&	_reqapi;

			public:
				/** */
				FreeReq(Api& reqapi) noexcept;

			private:	// Oscl::Mt::Itc::Msg
				/** */
				void	process() noexcept;
			};

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** */
		virtual void	request(FreeReq& msg) noexcept=0;
	};

}
}
}
}
}

#endif
