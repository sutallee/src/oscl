/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/freestore/nullmgr.h"

static Oscl::FreeStore::NullMgr	nullFreestoreMgr;

using namespace Oscl::Pdu::Memory::ITC;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	const char*					instance,
	FixedMem*					fixedMem,
	unsigned					nFixed,
	FragmentMem*				fragmentMem,
	unsigned					nFragment,
	CompositeMem*				compositeMem,
	unsigned					nComposite,
	Oscl::Done::Api*			allFree
	) noexcept:
		_instance(instance),
		_nFixed(nFixed),
		_nFragment(nFragment),
		_nComposite(nComposite),
		_allFree(allFree),
		_bufferFreeStoreApiComposer(
			*this,
			&Part::bufferFree
			),
		_bufferFreeStoreMgrComposer(
			_bufferFreeStoreApiComposer
			),
		_fixedFreeStoreApiComposer(
			*this,
			&Part::fixedFree
			),
		_fixedFreeStoreMgrComposer(
			_fixedFreeStoreApiComposer
			),
		_fragmentFreeStoreApiComposer(
			*this,
			&Part::fragmentFree
			),
		_fragmentFreeStoreMgrComposer(
			_fragmentFreeStoreApiComposer
			),
		_compositeFreeStoreApiComposer(
			*this,
			&Part::compositeFree
			),
		_compositeFreeStoreMgrComposer(
			_compositeFreeStoreApiComposer
			),
		_fixedFreeRequestComposer(
			*this,
			&Part::fixedFreeReq
			),
		_fragmentFreeRequestComposer(
			*this,
			&Part::fragmentFreeReq
			),
		_compositeFreeRequestComposer(
			*this,
			&Part::compositeFreeReq
			),
		_fixedSAP(
			_fixedFreeRequestComposer,
			papi
			),
		_fragmentSAP(
			_fragmentFreeRequestComposer,
			papi
			),
		_compositeSAP(
			_compositeFreeRequestComposer,
			papi
			)
		{
	for(unsigned i=0;i<nFixed;++i){
		_fixedPool.put(&fixedMem[i]);
		}

	for(unsigned i=0;i<nFragment;++i){
		_fragmentPool.put(&fragmentMem[i]);
		}

	for(unsigned i=0;i<nComposite;++i){
		_compositePool.put(&compositeMem[i]);
		}

	}

Part::~Part() noexcept{
	}

bool	Part::allMemoryIsFree() noexcept{
	return	(
				allBuffersFree()
			&&	allFixedsFree()
			&&	allFragmentsFree()
			&&	allCompositesFree()
			);
	}

void	Part::notifyFree() noexcept{
	if(!_allFree){
		return;
		}

	if(!allMemoryIsFree()){
		return;
		}

	_allFree->done();
	}

bool	Part::allFixedsFree() noexcept{
	return (_outstandingFixedMem.first())?false:true;
	}

bool	Part::allFragmentsFree() noexcept{
	return (_outstandingFragmentMem.first())?false:true;
	}

bool	Part::allCompositesFree() noexcept{
	return (_outstandingCompositeMem.first())?false:true;
	}

Oscl::Pdu::Fixed*	Part::allocConstFixed(
							const void*	cmd,
							unsigned	cmdSize
							) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				nullFreestoreMgr,
				(void*)cmd,	// Bad cast!
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fixed*	Part::allocFixed(
							void*		cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				_bufferFreeStoreMgrComposer,
				(void*)cmd,
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fixed*	Part::allocFixed(
							Oscl::FreeStore::Mgr&	bufferFsm,
							void*					cmd,
							unsigned				cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();

	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				bufferFsm,
				(void*)cmd,
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fragment*	Part::allocConstFragment(
							const void* cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				nullFreestoreMgr,
				(void*)cmd,	// Bad cast!
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Fragment*	Part::allocFragmentWrapper(
							void*		cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				_bufferFreeStoreMgrComposer,
				cmd,
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Fragment*	Part::allocFragmentWrapper(
									Oscl::FreeStore::Mgr&	bufferFsm,
									void*					cmd,
									unsigned				cmdSize
									) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				bufferFsm,
				cmd,
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Composite*	Part::allocConstWrapper(
							const void* cmd,
							unsigned	cmdSize
							) noexcept{
	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= allocConstFragment(cmd,cmdSize);
	if(!fragment){
		return 0;
		}

	CompositeMem*
	compMem	= allocCompositeMem();
	if(!compMem){
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= new(compMem) Oscl::Pdu::Composite(
				_compositeFreeStoreMgrComposer
				);

	composite->append(fragment);

	return composite;
	}

Oscl::Pdu::Composite*	Part::allocCompositeWrapper(
							Oscl::Pdu::Pdu&	pdu
							) noexcept{
	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		return 0;
		}

	CompositeMem*
	compMem	= allocCompositeMem();
	if(!compMem){
		freeFragment(fragMem);
		return 0;
		}

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					&pdu
					);

	Oscl::Pdu::Composite*
	composite	= new(compMem) Oscl::Pdu::Composite(
				_compositeFreeStoreMgrComposer
				);

	composite->append(fragment);

	return composite;
	}

Oscl::Pdu::Fragment*	Part::allocFragment(Oscl::Pdu::Pdu& pdu) noexcept{

	FragmentMem*
	fragMem	= allocFragment();

	if(!fragMem){
		return 0;
		}

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					&pdu
					);

	return fragment;
	}

void	Part::freeBuffer(void* buffer) noexcept{
	bufferFree(buffer);
	}

void	Part::fixedFree(Oscl::Pdu::Fixed* fixed) noexcept{

	Oscl::Pdu::Memory::ITC::Req::Api::FreeReq*
	req	= new (fixed) Oscl::Pdu::Memory::ITC::Req::Api::FreeReq(
			_fixedFreeRequestComposer
			);

	_fixedSAP.post(*req);
	}

void	Part::freeFixed(FixedMem* fixed) noexcept{

	if(!_outstandingFixedMem.remove(fixed)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			fixed
			);
		}

	if(_fixedPool.remove(fixed)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			fixed
			);
		}

	_fixedPool.put(fixed);

	notifyFree();
	}

Oscl::Pdu::Memory::ITC::Part::FixedMem*	Part::allocFixed() noexcept{
	FixedMem*
	mem	= _fixedPool.get();

	if(mem){

		if(_outstandingFixedMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstandingFixedMem.put(mem);
		}

	return mem;
	}

void	Part::compositeFree(Oscl::Pdu::Composite* composite) noexcept{

	Oscl::Pdu::Memory::ITC::Req::Api::FreeReq*
	req	= new (composite) Oscl::Pdu::Memory::ITC::Req::Api::FreeReq(
			_compositeFreeRequestComposer
			);

	_fragmentSAP.post(*req);
	}

void	Part::freeComposite(CompositeMem* composite) noexcept{

	if(!_outstandingCompositeMem.remove(composite)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			composite
			);
		}

	if(_compositePool.remove(composite)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			composite
			);
		}

	_compositePool.put(composite);

	notifyFree();
	}

Oscl::Pdu::Memory::ITC::Part::CompositeMem*	Part::allocCompositeMem() noexcept{
	CompositeMem*
	mem	= _compositePool.get();

	if(mem){

		if(_outstandingCompositeMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}

		_outstandingCompositeMem.put(mem);
		}

	return mem;
	}

void	Part::fragmentFree(Oscl::Pdu::Fragment* fragment) noexcept{

	Oscl::Pdu::Memory::ITC::Req::Api::FreeReq*
	req	= new (fragment) Oscl::Pdu::Memory::ITC::Req::Api::FreeReq(
			_fragmentFreeRequestComposer
			);

	_fragmentSAP.post(*req);
	}

void	Part::freeFragment(FragmentMem* fragment) noexcept{

	if(!_outstandingFragmentMem.remove(fragment)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			fragment
			);
		}

	if(_fragmentPool.remove(fragment)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			fragment
			);
		}

	_fragmentPool.put(fragment);

	notifyFree();
	}

Oscl::Pdu::Memory::ITC::Part::FragmentMem*	Part::allocFragment() noexcept{
	FragmentMem*
	mem	= _fragmentPool.get();

	if(mem){

		if(_outstandingFragmentMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstandingFragmentMem.put(mem);
		}

	return mem;
	}

Oscl::Pdu::Composite*	Part::allocComposite() noexcept{
	CompositeMem*
	mem	=	allocCompositeMem();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= new (mem)
					Oscl::Pdu::Composite(
						_compositeFreeStoreMgrComposer
						);

	return composite;
	}

void	Part::fixedFreeReq(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq& msg) noexcept{
	freeFixed((FixedMem*)&msg);
	}

void	Part::fragmentFreeReq(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq& msg) noexcept{
	freeFragment((FragmentMem*)&msg);
	}

void	Part::compositeFreeReq(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq& msg) noexcept{
	freeComposite((CompositeMem*)&msg);
	}

