/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_memory_itc_configh_
#define _oscl_pdu_memory_itc_configh_

#include "part.h"
#include "oscl/error/info.h"

/** */
namespace Oscl {
/** */
namespace Pdu {
/** */
namespace Memory {
/** */
namespace ITC {

/** This Config class encapsulates an ITC
	PDU allocator.
 */
template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
class Config : public Part {
	public:
		/** */
		union BufferUnion {
			Oscl::Memory::AlignedBlock<
				sizeof(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq)
				>	reqMem;
			Oscl::Memory::AlignedBlock<
				bufferSize
				>	mem;
			};

		/** */
		struct BufferMem {                   
			BufferUnion	u;
			void*   __qitemlink;
			};

	private:
		/** */
		Oscl::Queue<BufferMem>					_bufferPool;

		/** */
		Oscl::Queue<BufferMem>					_outstanding;

	private:
		/** */
		BufferMem		_bufferMem[nBuffers];

		/** */
		FixedMem		_fixedMem[nFixed];

		/** */
		FragmentMem		_fragmentMem[nFragment];

		/** */
		CompositeMem	_compositeMem[nComposite];

	private:
		/** */
		Oscl::Pdu::Memory::ITC::Req::Composer<Config>	_bufferFreeRequestComposer;

		/** */
		Oscl::Pdu::Memory::ITC::Req::Api::ConcreteSAP	_bufferSAP;

	public:
		/** The optional allFree callback may
			be used to notify the context when
			all allocated buffers have been
			freed. This is useful in dynamic
			contexts.
		 */
		Config(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			const char*					instance,
			Oscl::Done::Api*			allFree	= 0
			) noexcept;

		~Config() noexcept;

		/** This operation is used to return a
			buffer allocated by allocBuffer() to
			the buffer free store. This is typically
			used by a client only if it uses allocBuffer()
			and then finds that it doesn't have sufficient
			resources to proceed.
		 */
		void	freeBuffer(void* buffer) noexcept;

		/** */
		void*	allocBuffer(unsigned& allocatedSize) noexcept;

	protected:
		/** This operation attempts to allocate a
			buffer.

			RETURN: zero for out-of-resource.
		 */
		BufferMem*	allocBuffer() noexcept;

	private:
		/** */
		void	freeBufferMem(BufferMem* buffer) noexcept;

	private:
		/** */
		bool	allBuffersFree() noexcept;

		/** */
		void	bufferFree(void* buffer) noexcept;

	private:
		/**	*/
		void	bufferFreeReq(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq& msg) noexcept;
	};

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
	>::Config(
		Oscl::Mt::Itc::PostMsgApi&	papi,
		const char*					instance,
		Oscl::Done::Api*			allFree
		) noexcept:
	Part(
		papi,
		instance,
		_fixedMem,
		nFixed,
		_fragmentMem,
		nFragment,
		_compositeMem,
		nComposite,
		allFree
		),
	_bufferFreeRequestComposer(
		*this,
		&Config::bufferFreeReq
		),
	_bufferSAP(
		_bufferFreeRequestComposer,
		papi
		)
		{
	for(unsigned i=0;i<nBuffers;++i){
		_bufferPool.put(&_bufferMem[i]);
		}
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
Config<
	bufferSize,
	nBuffers,
	nFixed,
	nFragment,
	nComposite
	>
::~Config() noexcept{
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::bufferFree(void* buffer) noexcept{

	Oscl::Pdu::Memory::ITC::Req::Api::FreeReq*
	req	= new (buffer) Oscl::Pdu::Memory::ITC::Req::Api::FreeReq(
			_bufferFreeRequestComposer
			);

	_bufferSAP.post(*req);
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::freeBufferMem(BufferMem* buffer) noexcept{
	if(!_outstanding.remove(buffer)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			buffer
			);
		}

	if(_bufferPool.remove(buffer)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			buffer
			);
		}

	_bufferPool.put(buffer);

	notifyFree();
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::freeBuffer(void* buffer) noexcept{
	freeBufferMem((BufferMem*)buffer);
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
typename Oscl::Pdu::Memory::ITC::Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>::BufferMem*
	Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
		>
::allocBuffer() noexcept{
	BufferMem*
	mem	= _bufferPool.get();

	if(mem){

		if(_outstanding.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstanding.put(mem);
		}

	return mem;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
bool	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::allBuffersFree() noexcept{
	return (_outstanding.first())?false:true;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void*	Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
		>
::allocBuffer(unsigned& allocatedSize) noexcept{
	void*
	mem	= allocBuffer();

	if(!mem){
		return 0;
		}

	allocatedSize	= bufferSize;

	return mem;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
		>
::bufferFreeReq(Oscl::Pdu::Memory::ITC::Req::Api::FreeReq& msg) noexcept{
	freeBufferMem((BufferMem*)&msg);
	}

}
}
}
}

#endif
