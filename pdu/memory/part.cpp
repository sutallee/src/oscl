/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>

#define OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC

#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
#include <stdlib.h>
#endif

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/freestore/nullmgr.h"

#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
//#define OSCL_PDU_MEMORY_DEBUG_TRACE
#endif

static Oscl::FreeStore::NullMgr	nullFreestoreMgr;

using namespace Oscl::Pdu::Memory;

static Oscl::Queue<Part>	_activeFreeStoreList;

void	Oscl::Pdu::Memory::DumpFreeStoreStatus(Oscl::Print::Api& printApi) noexcept{
	for(
		Part*	part	= _activeFreeStoreList.first();
		part;
		part	= _activeFreeStoreList.next(part)
		){
		part->dumpStatus(printApi);
		}
	}

Part::Part(
	const char*			instance,
	FixedMem*			fixedMem,
	unsigned			nFixed,
	FragmentMem*		fragmentMem,
	unsigned			nFragment,
	CompositeMem*		compositeMem,
	unsigned			nComposite,
	Oscl::Done::Api*	allFree
	) noexcept:
		_instance(instance),
		_nFixed(nFixed),
		_nFragment(nFragment),
		_nComposite(nComposite),
		_allFree(allFree),
		#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
		_nDynamicFixed(0),
		_nDynamicFragment(0),
		_nDynamicComposite(0),
		_maxDynamicFixed(0),
		_maxDynamicFragment(0),
		_maxDynamicComposite(0),
		#endif
		_bufferFreeStoreApiComposer(
			*this,
			&Part::bufferFree
			),
		_bufferFreeStoreMgrComposer(
			_bufferFreeStoreApiComposer
			),
		_fixedFreeStoreApiComposer(
			*this,
			&Part::fixedFree
			),
		_fixedFreeStoreMgrComposer(
			_fixedFreeStoreApiComposer
			),
		_fragmentFreeStoreApiComposer(
			*this,
			&Part::fragmentFree
			),
		_fragmentFreeStoreMgrComposer(
			_fragmentFreeStoreApiComposer
			),
		_compositeFreeStoreApiComposer(
			*this,
			&Part::compositeFree
			),
		_compositeFreeStoreMgrComposer(
			_compositeFreeStoreApiComposer
			)
		{
	for(unsigned i=0;i<nFixed;++i){
		_fixedPool.put(&fixedMem[i]);
		fixedMem[i]._dynamic	= false;
		}

	for(unsigned i=0;i<nFragment;++i){
		_fragmentPool.put(&fragmentMem[i]);
		fragmentMem[i]._dynamic	= false;
		}

	for(unsigned i=0;i<nComposite;++i){
		_compositePool.put(&compositeMem[i]);
		compositeMem[i]._dynamic	= false;
		}

	_activeFreeStoreList.put(this);
	}

Part::~Part() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: [%s]\n"
		" _maxDynamicFixed: %u\n"
		" _maxDynamicFragment: %u\n"
		" _maxDynamicComposite: %u\n"
		"",
		__PRETTY_FUNCTION__,
		_instance,
		_maxDynamicFixed,
		_maxDynamicFragment,
		_maxDynamicComposite
		);
	#endif

	_activeFreeStoreList.remove(this);

	FixedMem*	fixed;
	while((fixed=_fixedPool.get())){
		if(fixed->_dynamic){
			free(fixed);
			}
		}

	FragmentMem*	fragment;
	while((fragment=_fragmentPool.get())){
		if(fragment->_dynamic){
			free(fragment);
			}
		}

	CompositeMem*	composite;
	while((composite=_compositePool.get())){
		if(composite->_dynamic){
			free(composite);
			}
		}
	}

bool	Part::allMemoryIsFree() noexcept{
	return	(
				allBuffersFree() 
			&&	allFixedsFree()
			&&	allFragmentsFree()
			&&	allCompositesFree()
			);
	}

void	Part::dumpStatus(Oscl::Print::Api& printApi) noexcept{
	dumpStatus(
		printApi,
		"%s [%s]: Status:\n"
		" allBuffersFree(max:%u): %s\n"
		" allFixedsFree(max:%u): %s\n"
		" allFragmentsFree(max:%u): %s\n"
		" allCompositesFree(max:%u): %s\n"
		"",
		__PRETTY_FUNCTION__,
		_instance,
		maxDynamicBuffers(),
		allBuffersFree()?"yes":"no",
		_maxDynamicFixed,
		allFixedsFree()?"yes":"no",
		_maxDynamicFragment,
		allFragmentsFree()?"yes":"no",
		_maxDynamicComposite,
		allCompositesFree()?"yes":"no"
		);
	}

void	Part::dumpStatus(
			Oscl::Print::Api& printApi,
			const char*			format,
			...
			) noexcept{
	va_list	ap;
	va_start(ap,format);
	printApi.print(
		format,
		ap
		);
	va_end(ap);
	}

void	Part::notifyFree() noexcept{
	if(!_allFree){
		return;
		}

	if(!allMemoryIsFree()){
		return;
		}

	_allFree->done();
	}

bool	Part::allFixedsFree() noexcept{
	return (_outstandingFixedMem.first())?false:true;
	}

bool	Part::allFragmentsFree() noexcept{
	return (_outstandingFragmentMem.first())?false:true;
	}

bool	Part::allCompositesFree() noexcept{
	return (_outstandingCompositeMem.first())?false:true;
	}

Oscl::Pdu::Fixed*	Part::allocConstFixed(
							const void*	cmd,
							unsigned	cmdSize
							) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				nullFreestoreMgr,
				(void*)cmd,	// Bad cast!
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fixed*	Part::allocFixed(
							void*		cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				_bufferFreeStoreMgrComposer,
				(void*)cmd,
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fixed*	Part::allocFixed(
							Oscl::FreeStore::Mgr&	bufferFsm,
							void*					cmd,
							unsigned				cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();

	if(!fixedMem){
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				bufferFsm,
				(void*)cmd,
				cmdSize,
				cmdSize
				);

	return fixed;
	}

Oscl::Pdu::Fragment*	Part::allocConstFragment(
							const void* cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				nullFreestoreMgr,
				(void*)cmd,	// Bad cast!
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Fragment*	Part::allocFragmentWrapper(
							void*		cmd,
							unsigned	cmdSize
							) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				_bufferFreeStoreMgrComposer,
				cmd,
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Fragment*	Part::allocFragmentWrapper(
									Oscl::FreeStore::Mgr&	bufferFsm,
									void*					cmd,
									unsigned				cmdSize
									) noexcept{
	FixedMem*
	fixedMem	= allocFixed();
	if(!fixedMem){
		return 0;
		}

	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		freeFixed(fixedMem);
		return 0;
		}

	Oscl::Pdu::Fixed*
	fixed	= new(fixedMem) Oscl::Pdu::Fixed(
				_fixedFreeStoreMgrComposer,
				bufferFsm,
				cmd,
				cmdSize,
				cmdSize
				);

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					fixed
					);

	return fragment;
	}

Oscl::Pdu::Composite*	Part::allocConstWrapper(
							const void* cmd,
							unsigned	cmdSize
							) noexcept{
	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= allocConstFragment(cmd,cmdSize);
	if(!fragment){
		return 0;
		}

	CompositeMem*
	compMem	= allocCompositeMem();
	if(!compMem){
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= new(compMem) Oscl::Pdu::Composite(
				_compositeFreeStoreMgrComposer
				);

	composite->append(fragment);

	return composite;
	}

Oscl::Pdu::Composite*	Part::allocCompositeWrapper(
							Oscl::Pdu::Pdu&	pdu
							) noexcept{
	FragmentMem*
	fragMem	= allocFragment();
	if(!fragMem){
		return 0;
		}

	CompositeMem*
	compMem	= allocCompositeMem();
	if(!compMem){
		freeFragment(fragMem);
		return 0;
		}

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					&pdu
					);

	Oscl::Pdu::Composite*
	composite	= new(compMem) Oscl::Pdu::Composite(
				_compositeFreeStoreMgrComposer
				);

	composite->append(fragment);

	return composite;
	}

Oscl::Pdu::Fragment*	Part::allocFragment(Oscl::Pdu::Pdu& pdu) noexcept{

	FragmentMem*
	fragMem	= allocFragment();

	if(!fragMem){
		return 0;
		}

	Oscl::Pdu::Fragment*
	fragment	= new(fragMem) Oscl::Pdu::Fragment(
					_fragmentFreeStoreMgrComposer,
					&pdu
					);

	return fragment;
	}

void	Part::freeBuffer(void* buffer) noexcept{
	bufferFree(buffer);
	}

void	Part::fixedFree(Oscl::Pdu::Fixed* fixed) noexcept{
	// This would do ITC in an MT application.
	freeFixed((FixedMem*)fixed);
	}

void	Part::freeFixed(FixedMem* fixed) noexcept{

	if(!_outstandingFixedMem.remove(fixed)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			fixed
			);
		}

	if(_fixedPool.remove(fixed)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			fixed
			);
		}

	if(fixed->_dynamic){
		#if DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s] fixed: %p,  _nDynamicFixed: %u\n",
			__FUNCTION__,
			_instance,
			fixed,
			_nDynamicFixed
			);
		#endif
		if(!_nDynamicFixed){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::hexDump(
				fixed,
				sizeof(FixedMem)
				);
			#endif
			}
		--_nDynamicFixed;
		}

	_fixedPool.put(fixed);

	notifyFree();
	}

Oscl::Pdu::Memory::Part::FixedMem*	Part::allocFixed() noexcept{
	FixedMem*
	mem	= _fixedPool.get();

	#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
	if(!mem){
		mem	= (FixedMem*)malloc(sizeof(FixedMem));
		if(!mem){
			#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: %s, malloc() failed: _nDynamicFixed: %u\n",
				__FUNCTION__,
				_instance,
				_nDynamicFixed
				);
			#endif
			return 0;
			}

		mem->_dynamic	= true;

		++_maxDynamicFixed;

		#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s] %p _nDynamicFixed: %u\n",
			__FUNCTION__,
			_instance,
			mem,
			_nDynamicFixed
			);
		#endif
		}
	#endif

	if(mem){

		if(mem->_dynamic){
			++_nDynamicFixed;
			}

		if(_outstandingFixedMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstandingFixedMem.put(mem);
		}

	return mem;
	}

void	Part::compositeFree(Oscl::Pdu::Composite* composite) noexcept{
	// This would do ITC in a MT application.
	freeComposite((CompositeMem*)composite);
	}

void	Part::freeComposite(CompositeMem* composite) noexcept{

	if(!_outstandingCompositeMem.remove(composite)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			composite
			);
		}

	if(_compositePool.remove(composite)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			composite
			);
		}

	if(composite->_dynamic){
		#if DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s] composite: %p, _nDynamicComposite: %u\n",
			__FUNCTION__,
			_instance,
			composite,
			_nDynamicComposite
			);
		#endif
		if(!_nDynamicComposite){
			#if DEBUG_TRACE
			Oscl::Error::Info::hexDump(
				composite,
				sizeof(CompositeMem)
				);
			#endif
			}
		--_nDynamicComposite;
		}

	_compositePool.put(composite);

	notifyFree();
	}

Oscl::Pdu::Memory::Part::CompositeMem*	Part::allocCompositeMem() noexcept{
	CompositeMem*
	mem	= _compositePool.get();

	#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
	if(!mem){
		mem	= (CompositeMem*)malloc(sizeof(CompositeMem));
		if(!mem){
			#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: %s, malloc() failed: _nDynamicComposite: %u\n",
				__FUNCTION__,
				_instance,
				_nDynamicComposite
				);
			#endif
			return 0;
			}

		mem->_dynamic	= true;

		++_maxDynamicComposite;

		#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s], %p _nDynamicComposite: %u\n",
			__FUNCTION__,
			_instance,
			mem,
			_nDynamicComposite
			);
		#endif
		}
	#endif

	if(mem){

		if(mem->_dynamic){
			++_nDynamicComposite;
			}

		if(_outstandingCompositeMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstandingCompositeMem.put(mem);
		}

	return mem;
	}

void	Part::fragmentFree(Oscl::Pdu::Fragment* fragment) noexcept{
	// This would do ITC in an MT application.
	freeFragment((FragmentMem*)fragment);
	}

void	Part::freeFragment(FragmentMem* fragment) noexcept{

	if(!_outstandingFragmentMem.remove(fragment)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			fragment
			);
		}

	if(_fragmentPool.remove(fragment)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			fragment
			);
		}

	if(fragment->_dynamic){
		#if DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s] fragment: %p,  _nDynamicFragment: %u \n",
			__FUNCTION__,
			_instance,
			fragment,
			_nDynamicFragment
			);
		#endif
		if(!_nDynamicFragment){
			#if DEBUG_TRACE
			Oscl::Error::Info::hexDump(
				fragment,
				sizeof(FragmentMem)
				);
			#endif
			}
		--_nDynamicFragment;
		}

	_fragmentPool.put(fragment);

	notifyFree();
	}

Oscl::Pdu::Memory::Part::FragmentMem*	Part::allocFragment() noexcept{
	FragmentMem*
	mem	= _fragmentPool.get();

	#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
	if(!mem){
		mem	= (FragmentMem*)malloc(sizeof(FragmentMem));
		if(!mem){
			#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: %s, malloc() failed: _nDynamicFragment: %u\n",
				__FUNCTION__,
				_instance,
				_nDynamicFragment
				);
			#endif
			return 0;
			}

		mem->_dynamic	= true;

		++_maxDynamicFragment;

		#ifdef OSCL_PDU_MEMORY_DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: [%s], %p _nDynamicFragment: %u\n",
			__FUNCTION__,
			_instance,
			mem,
			_nDynamicFragment
			);
		#endif
		}
	#endif

	if(mem){

		if(mem->_dynamic){
			++_nDynamicFragment;
			}

		if(_outstandingFragmentMem.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstandingFragmentMem.put(mem);
		}

	return mem;
	}

Oscl::Pdu::Composite*	Part::allocComposite() noexcept{
	CompositeMem*
	mem	=	allocCompositeMem();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of composite memory.\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= new (mem)
					Oscl::Pdu::Composite(
						_compositeFreeStoreMgrComposer
						);

	return composite;
	}

