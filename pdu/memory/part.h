/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_memory_parth_
#define _oscl_pdu_memory_parth_

#include <stdint.h>
#include "api.h"
#include "oscl/memory/block.h"
#include "oscl/freestore/composers.h"
#include "oscl/queue/queue.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/composite.h"
#include "oscl/pdu/fragment.h"
#include "oscl/done/api.h"
#include "oscl/print/api.h"

/** */
namespace Oscl {
/** */
namespace Pdu {
/** */
namespace Memory {

void	DumpFreeStoreStatus(Oscl::Print::Api& printApi) noexcept;

/** This class encapsulates a PDU allocator.

	IMPORTANT: The PDU resources allocated by this
	implementation must *NEVER* be sent outside of
	the thread in which this Part operates. The
	dynamic resource free-store-managers implemented
	here are *NOT* thread safe.
 */
class Part :
	public Oscl::Pdu::Memory::Api,
	public Oscl::QueueItem
	{
	protected:
		/** */
		const char*		_instance;

	private:
		/** */
		const unsigned	_nFixed;

		/** */
		const unsigned	_nFragment;

		/** */
		const unsigned	_nComposite;

		/** */
		Oscl::Done::Api*	_allFree;

	#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
	private:
		/** */
		unsigned	_nDynamicFixed;

		/** */
		unsigned	_nDynamicFragment;

		/** */
		unsigned	_nDynamicComposite;

		/** */
		unsigned	_maxDynamicFixed;

		/** */
		unsigned	_maxDynamicFragment;

		/** */
		unsigned	_maxDynamicComposite;
	#endif

	private:
		/** */
		Oscl::FreeStore::Composer<Part,void>	_bufferFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<void>		_bufferFreeStoreMgrComposer;

	public:
		/** */
		struct FixedMem {                   
			Oscl::Memory::AlignedBlock<sizeof(Oscl::Pdu::Fixed)>	mem;
			void*   __qitemlink;
			bool	_dynamic;
			};

	private:
		/** */
		Oscl::Queue<FixedMem>					_fixedPool;

		/** */
		Oscl::Queue<FixedMem>					_outstandingFixedMem;

		/** */
		Oscl::FreeStore::Composer<Part,Oscl::Pdu::Fixed>	_fixedFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<Oscl::Pdu::Fixed>		_fixedFreeStoreMgrComposer;

	public:
		/** */
		struct FragmentMem {                   
			Oscl::Memory::AlignedBlock<sizeof(Oscl::Pdu::Fragment)>	mem;
			void*   __qitemlink;
			bool	_dynamic;
			};

	private:
		/** */
		Oscl::Queue<FragmentMem>					_fragmentPool;

		/** */
		Oscl::Queue<FragmentMem>					_outstandingFragmentMem;

		/** */
		Oscl::FreeStore::Composer<Part,Oscl::Pdu::Fragment>	_fragmentFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<Oscl::Pdu::Fragment>		_fragmentFreeStoreMgrComposer;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_advQ;

	public:
		/** */
		struct CompositeMem {                   
			Oscl::Memory::AlignedBlock<sizeof(Oscl::Pdu::Composite)>	mem;
			void*   __qitemlink;
			bool	_dynamic;
			};

	private:
		/** */
		Oscl::Queue<CompositeMem>					_compositePool;

		/** */
		Oscl::Queue<CompositeMem>					_outstandingCompositeMem;

		/** */
		Oscl::FreeStore::Composer<Part,Oscl::Pdu::Composite>	_compositeFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<Oscl::Pdu::Composite>		_compositeFreeStoreMgrComposer;

	public:
		/** The constructor client specifies the
			memory resources used by this part.
		 */
		Part(
			const char*			instance,
			FixedMem*			fixedMem,
			unsigned			nFixed,
			FragmentMem*		fragmentMem,
			unsigned			nFragment,
			CompositeMem*		compositeMem,
			unsigned			nComposite,
			Oscl::Done::Api*	allFree	= 0
			) noexcept;

		/** */
		virtual ~Part() noexcept;

		/** This operation returns true if all of the
			memory allocated by this Part is currently
			in their respective free stores.

			This is generally used by dynamic objects
			that contain this Part to ensure that all
			PDU resources have been returned by lower
			layer before the client itself is destroyed.
		 */
		bool	allMemoryIsFree() noexcept;

		/** */
		void	dumpStatus(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	dumpStatus(
					Oscl::Print::Api& printApi,
					const char*			format,
					...
					) noexcept;

	protected:
		/** */
		void	notifyFree() noexcept;

	public:	// Oscl::Pdu::Memory::Api
		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFragmentWrapper() instead.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fragment*	allocConstFragment(
									const void* cmd,
									unsigned	cmdSize
									) noexcept;

		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			This operation **assumes** that the
			buffer was allocated by the allocBuffer()
			operation of this instance of this class.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fragment*	allocFragmentWrapper(
									void*		cmd,
									unsigned	cmdSize
									) noexcept;

		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			This operation uses the external free-store
			manager specified by bufferFsm, that will
			be used to reclaim the buffer memory when
			its reference count goes to zero.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fragment*	allocFragmentWrapper(
									Oscl::FreeStore::Mgr&	bufferFsm,
									void*					cmd,
									unsigned				cmdSize
									) noexcept;

		/** Like allocConstFragment(), this operation
			allocates a PDU fragment around the cmd buffer
			of the size specified by cmdSize. It then
			allocates a composite and attaches fragment
			to that composite.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFragmentWrapper() instead.


			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Composite*	allocConstWrapper(
									const void*	cmd,
									unsigned	cmdSize
									) noexcept;

		/**
			This operation allocates a fragment;
			attaches the specified pdu to that fragment;
			allocates a compose; and attaches the fragment
			to the composite.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Composite*	allocCompositeWrapper(
									Oscl::Pdu::Pdu&	pdu
									) noexcept;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFixed() instead.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fixed*	allocConstFixed(
								const void*	cmd,
								unsigned	cmdSize
								) noexcept;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			This operation **assumes** that the
			buffer was allocated by the allocBuffer()
			operation of this instance of this class.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fixed*	allocFixed(
								void*		cmd,
								unsigned	cmdSize
								) noexcept;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			This operation uses the external free-store
			manager specified by bufferFsm, that will
			be used to reclaim the buffer memory when
			its reference count goes to zero.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fixed*	allocFixed(
								Oscl::FreeStore::Mgr&	bufferFsm,
								void*					cmd,
								unsigned				cmdSize
								) noexcept;

		/**	This operation allocates a fragment and
			attaches the specified pdu to that fragment.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Fragment*    allocFragment(Oscl::Pdu::Pdu& pdu) noexcept;

		/**	This operation allocates an empty composite.

			RETURN: zero for out-of-resource.
		 */
		Oscl::Pdu::Composite*	allocComposite() noexcept;

		/** This operation attempts to allocate a
			buffer. If successful, it returns the
			size of the bufer to the variable
			referenced by allocatedSize.

			RETURN: zero for out-of-resource.
		 */
		virtual void*	allocBuffer(unsigned& allocatedSize) noexcept=0;

		/** This operation is used to return a
			buffer allocated by allocBuffer() to
			the buffer free store. This is typically
			used by a client only if it uses allocBuffer()
			and then finds that it doesn't have sufficient
			resources to proceed.
		 */
		void	freeBuffer(void* buffer) noexcept;

	protected:
		/** */
		virtual bool	allBuffersFree() noexcept=0;

		/** */
		virtual unsigned	nDynamicBuffers() noexcept=0;

		/** */
		virtual unsigned	maxDynamicBuffers() noexcept=0;

	private:
		/** */
		bool	allFixedsFree() noexcept;

		/** */
		bool	allFragmentsFree() noexcept;

		/** */
		bool	allCompositesFree() noexcept;

	public: // Oscl::FreeStore::FreeStoreApi	_bufferFreeStoreApiComposer;
		/** */
		virtual void	bufferFree(void* buffer) noexcept=0;

	private: // Oscl::FreeStore::FreeStoreApi	_fixedFreeStoreApiComposer;
		/** */
		void	fixedFree(Oscl::Pdu::Fixed* fixed) noexcept;

		/** */
		void	freeFixed(FixedMem* fixed) noexcept;

		/** */
		FixedMem*	allocFixed() noexcept;

	private: // Oscl::FreeStore::FreeStoreApi	_compositeFreeStoreApiComposer;
		/** */
		void	compositeFree(Oscl::Pdu::Composite* composite) noexcept;

		/** */
		void	freeComposite(CompositeMem* composite) noexcept;

	public:
		/** */
		CompositeMem*	allocCompositeMem() noexcept;

	private: // Oscl::FreeStore::FreeStoreApi	_fragmentFreeStoreApiComposer;
		/** */
		void	fragmentFree(Oscl::Pdu::Fragment* fragment) noexcept;

		/** */
		void	freeFragment(FragmentMem* fragment) noexcept;

		/** */
		FragmentMem*	allocFragment() noexcept;

	};

}
}
}

#endif
