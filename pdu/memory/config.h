/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_memory_configh_
#define _oscl_pdu_memory_configh_

#define OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC

#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
//#define OSCL_PDU_MEMORY_CONFIG_DYNAMIC_DEBUG_TRACE
#endif

#include "part.h"

#include "oscl/error/info.h"

#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
#include <stdlib.h>
#endif

/** */
namespace Oscl {
/** */
namespace Pdu {
/** */
namespace Memory {

/** This Config class encapsulates a PDU allocator.
 */
template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
class Config : public Part {
	public:
		/** */
		struct BufferMem {                   
			Oscl::Memory::AlignedBlock<bufferSize>	mem;
			void*   __qitemlink;
			bool	_dynamic;
			};

	private:
		/** */
		Oscl::Queue<BufferMem>					_bufferPool;

		#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
		/** */
		unsigned								_nDynamicBuffers;

		/** */
		unsigned								_maxDynamicBuffers;
		#endif

		/** */
		Oscl::Queue<BufferMem>					_outstanding;

	private:
		/** */
		BufferMem		_bufferMem[nBuffers];

		/** */
		FixedMem		_fixedMem[nFixed];

		/** */
		FragmentMem		_fragmentMem[nFragment];

		/** */
		CompositeMem	_compositeMem[nComposite];

	public:
		/** The optional allFree callback may
			be used to notify the context when
			all allocated buffers have been
			freed. This is useful in dynamic
			contexts.
		 */
		Config(
			const char*			instance,
			Oscl::Done::Api*	allFree	= 0
			) noexcept;

		~Config() noexcept;

		/** This operation is used to return a
			buffer allocated by allocBuffer() to
			the buffer free store. This is typically
			used by a client only if it uses allocBuffer()
			and then finds that it doesn't have sufficient
			resources to proceed.
		 */
		void	freeBuffer(void* buffer) noexcept;

		/** */
		void*	allocBuffer(unsigned& allocatedSize) noexcept;

		/** */
		unsigned	nDynamicBuffers() noexcept;

		/** */
		unsigned	maxDynamicBuffers() noexcept;

	protected:
		/** This operation attempts to allocate a
			buffer.

			RETURN: zero for out-of-resource.
		 */
		BufferMem*	allocBuffer() noexcept;

	private:
		/** */
		void	freeBufferMem(BufferMem* buffer) noexcept;

	private:
		/** */
		bool	allBuffersFree() noexcept;

		/** */
		void	bufferFree(void* buffer) noexcept;
	};

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
	>::Config(
		const char*			instance,
		Oscl::Done::Api*	allFree
		) noexcept:
	Part(
		instance,
		_fixedMem,
		nFixed,
		_fragmentMem,
		nFragment,
		_compositeMem,
		nComposite,
		allFree
		)
		#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
		,
		_nDynamicBuffers(0),
		_maxDynamicBuffers(0)
		#endif
		{
	for(unsigned i=0;i<nBuffers;++i){
		_bufferPool.put(&_bufferMem[i]);
		_bufferMem[i]._dynamic	= false;
		}
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
Config<
	bufferSize,
	nBuffers,
	nFixed,
	nFragment,
	nComposite
	>
::~Config() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: [%s]"
		" _maxDynamicBuffers: %u\n"
		"",
		__PRETTY_FUNCTION__,
		_instance,
		_maxDynamicBuffers
		);
	#endif
	BufferMem*	mem;
	while((mem=_bufferPool.get())){
		if(mem->_dynamic){
			free(mem);
			}
		}
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::bufferFree(void* buffer) noexcept{
	// This would do ITC in an MT application.
	freeBufferMem((BufferMem*)buffer);
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::freeBufferMem(BufferMem* buffer) noexcept{
	if(!_outstanding.remove(buffer)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] not in _outstanding list!\n",
			__FUNCTION__,
			_instance,
			buffer
			);
		}

	if(_bufferPool.remove(buffer)){
		Oscl::Error::Info::log(
			"%s: [%s] Buffer [%p] already free!\n",
			__FUNCTION__,
			_instance,
			buffer
			);
		}

	if(buffer->_dynamic){
		--_nDynamicBuffers;
		}

	_bufferPool.put(buffer);

	notifyFree();
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::freeBuffer(void* buffer) noexcept{
	freeBufferMem((BufferMem*)buffer);
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
unsigned	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::nDynamicBuffers() noexcept{
	return _nDynamicBuffers;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
unsigned	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::maxDynamicBuffers() noexcept{
	return _maxDynamicBuffers;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
typename Oscl::Pdu::Memory::Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>::BufferMem*
	Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
		>
::allocBuffer() noexcept{
	BufferMem*
	mem	= _bufferPool.get();

	#ifdef OSCL_PDU_MEMORY_ALLOW_DYNAMIC_ALLOC
	if(!mem){
		mem	= (BufferMem*)malloc(sizeof(BufferMem));
		if(!mem){
			#ifdef OSCL_PDU_MEMORY_CONFIG_DYNAMIC_DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: %s, malloc() failed: _nDynamicBuffers: %u\n",
				__FUNCTION__,
				_instance,
				_nDynamicBuffers
				);
			#endif
			return 0;
			}

		mem->_dynamic	= true;

		++_maxDynamicBuffers;

		#ifdef OSCL_PDU_MEMORY_CONFIG_DYNAMIC_DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: %s, _nDynamicBuffers: %u\n",
			__FUNCTION__,
			_instance,
			_nDynamicBuffers
			);
		#endif
		}
	#endif

	if(mem){

		if(mem->_dynamic){
			++_nDynamicBuffers;
			}

		if(_outstanding.remove(mem)){
			Oscl::Error::Info::log(
				"%s: %s, buffer[%p] is already outstanding!\n",
				__FUNCTION__,
				_instance,
				mem
				);
			}
		_outstanding.put(mem);
		}

	return mem;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
bool	Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>
::allBuffersFree() noexcept{
	return (_outstanding.first())?false:true;
	}

template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite
	>
void*	Config<
		bufferSize,
		nBuffers,
		nFixed,
		nFragment,
		nComposite
		>
::allocBuffer(unsigned& allocatedSize) noexcept{
	void*
	mem	= allocBuffer();

	if(!mem){
		return 0;
		}

	allocatedSize	= bufferSize;

	return mem;
	}

}
}
}

#endif
