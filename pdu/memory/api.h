/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_memory_apih_
#define _oscl_pdu_memory_apih_

#include "oscl/pdu/fixed.h"
#include "oscl/pdu/composite.h"
#include "oscl/pdu/fragment.h"

/** */
namespace Oscl {
/** */
namespace Pdu {
/** */
namespace Memory {

/** This interface provides a way for clients to
	allocate various PDU resources.
 */
class Api {
	public:
		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFragmentWrapper() instead.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fragment*
					allocConstFragment(
						const void* cmd,
						unsigned	cmdSize
						) noexcept=0;

		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			This operation **assumes** that the
			buffer was allocated by the allocBuffer()
			operation of this instance of this class.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fragment*
					allocFragmentWrapper(
						void*		cmd,
						unsigned	cmdSize
						) noexcept=0;

		/** This operation allocates a PDU fragment
			around the cmd buffer of the size
			specified by cmdSize.

			This operation uses the external free-store
			manager specified by bufferFsm, that will
			be used to reclaim the buffer memory when
			its reference count goes to zero.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fragment*
					allocFragmentWrapper(
						Oscl::FreeStore::Mgr&	bufferFsm,
						void*					cmd,
						unsigned				cmdSize
						) noexcept=0;

		/** Like allocConstFragment(), this operation
			allocates a PDU fragment around the cmd buffer
			of the size specified by cmdSize. It then
			allocates a composite and attaches fragment
			to that composite.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFragmentWrapper() instead.


			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Composite*
					allocConstWrapper(
						const void*	cmd,
						unsigned	cmdSize
						) noexcept=0;

		/**
			This operation allocates a fragment;
			attaches the specified pdu to that fragment;
			allocates a compose; and attaches the fragment
			to the composite.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Composite*
					allocCompositeWrapper(
						Oscl::Pdu::Pdu&	pdu
						) noexcept=0;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			The buffer referenced by cmd is assumed to
			be a fixed value and always available such as
			a const string.

			The memory referenced by cmd is *NEVER* released
			because it is considered static.

			If cmd is dynamic, you should be using
			allocFixed() instead.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fixed*
					allocConstFixed(
						const void*	cmd,
						unsigned	cmdSize
						) noexcept=0;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			This operation **assumes** that the
			buffer was allocated by the allocBuffer()
			operation of this instance of this class.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fixed*
					allocFixed(
						void*		cmd,
						unsigned	cmdSize
						) noexcept=0;

		/** This operation allocates a PDU fixed
			size wrapper around the cmd buffer of
			the size specified by cmdSize.

			This operation uses the external free-store
			manager specified by bufferFsm, that will
			be used to reclaim the buffer memory when
			its reference count goes to zero.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fixed*
					allocFixed(
						Oscl::FreeStore::Mgr&	bufferFsm,
						void*					cmd,
						unsigned				cmdSize
						) noexcept=0;

		/**	This operation allocates a fragment and
			attaches the specified pdu to that fragment.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Fragment*
					allocFragment(Oscl::Pdu::Pdu& pdu) noexcept=0;

		/**	This operation allocates an empty composite.

			RETURN: zero for out-of-resource.
		 */
		virtual Oscl::Pdu::Composite*
					allocComposite() noexcept=0;

		/** This operation attempts to allocate a
			buffer. If successful, it returns the
			size of the bufer to the variable
			referenced by bufferSize.

			RETURN: zero for out-of-resource.
		 */
		virtual void*	allocBuffer(unsigned& allocatedSize) noexcept=0;

		/** This operation is used to return a
			buffer allocated by allocBuffer() to
			the buffer free store. This is typically
			used by a client only if it uses allocBuffer()
			and then finds that it doesn't have sufficient
			resources to proceed.
		 */
		virtual void	freeBuffer(void* buffer) noexcept=0;

	};

}
}
}

#endif
