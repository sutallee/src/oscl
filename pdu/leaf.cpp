/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "leaf.h"
#include "visitor.h"

using namespace Oscl::Pdu;

Leaf::Leaf(FreeStore::Mgr& mgr) noexcept:
		Pdu(mgr) {
	}

bool /* stop */ Leaf::visit(	ReadOnlyVisitor&	v,
								unsigned			position,
								unsigned			firstValid,
								unsigned			nValid
								) const noexcept{
	return v.accept(*this,position,getContentStart()+firstValid,nValid);
	}

bool /* stop */ Leaf::visit(	Visitor&	v,
								unsigned	position,
								unsigned	firstValid,
								unsigned	nValid
								) noexcept{
	return v.accept(*this,position,getContentStart()+firstValid,nValid);
	}

unsigned	Leaf::length() const noexcept{
	return getContentLength();
	}

unsigned /* nAppended */ Leaf::append(const void* src,unsigned length) noexcept{
	uint8_t*		buf			= getBuffer();
	unsigned	size		= getBufferSize();
	unsigned	start		= getContentStart();
	unsigned	len			= getContentLength();
	unsigned	end			= len+start;
	unsigned	remaining	= size-end;
	unsigned	nAppended;

	if(remaining > size) return 0;

	nAppended	= (remaining < length)?remaining:length;
	memcpy(&buf[end],src,nAppended);
	addLength(nAppended);

	return nAppended;
	}

unsigned 	Leaf::read(	void*		data,
						unsigned	max,
						unsigned	offset
						) const noexcept{
	unsigned	len			= getContentLength();
	if(offset>len) return 0;
	const uint8_t*	buf			= getBuffer();
	unsigned		start		= getContentStart();
	unsigned		n			= len-offset;
	if(max < n) n = max;
	memcpy(data,&buf[start+offset],n);
	return n;
	}

