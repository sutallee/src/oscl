/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_visitorh_
#define _oscl_krux_pdu_visitorh_

/** */
namespace Oscl {
/** */
namespace Pdu {

class Composite;
class ReadOnlyComposite;
class Fragment;
class ReadOnlyFragment;
class Leaf;

/** This class is the abstract visitor which is used
	to implement operations on the PDU hierarchy.
 */

class ReadOnlyVisitor {
	public:	// const versions
		/** Shut-up GCC. */
		virtual ~ReadOnlyVisitor() {}

		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */	accept(	const Composite&	p,
										unsigned			position
										) noexcept=0;

		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */	accept(	const ReadOnlyComposite&	p,
										unsigned					position
										) noexcept=0;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client. The offset indexes
			the first valid octet in the leaf buffer for the current client.
			The length is the the number of valid octets beginning from
			the offset argument.
		 */
		virtual bool /* stop */ accept(	const Leaf&	p,
										unsigned	position,
										unsigned	offset,
										unsigned	length
										) noexcept=0;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */ accept(	const Fragment&	p,
										unsigned		position
										) noexcept=0;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */ accept(	const ReadOnlyFragment&	p,
										unsigned				position
										) noexcept=0;

	};

class Visitor {
	public:	// non-const versions
		/** Shut-up GCC. */
		virtual ~Visitor() {}

		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */	accept(	Composite&	p,
										unsigned	position
										) noexcept=0;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client. The offset indexes
			the first valid octet in the leaf buffer for the current client.
			The length is the the number of valid octets beginning from
			the offset argument.
		 */
		virtual bool /* stop */ accept(	Leaf&		p,
										unsigned	position,
										unsigned	offset,
										unsigned	length
										) noexcept=0;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		virtual bool /* stop */ accept(	Fragment&	p,
										unsigned	position
										) noexcept=0;
	};

}
}

#endif
