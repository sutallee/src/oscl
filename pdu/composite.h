/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_compositeh_
#define _oscl_krux_pdu_compositeh_
#include "api.h"
#include "pdu.h"
#include "fragment.h"

/** */
namespace Oscl {
/** */
namespace Pdu {

class Visitor;
class ReadOnlyVisitor;

/** This class represents a protocol data unit that is composed as an ordered
	group of packet fragments (e.g. header, sdu, and trailer). This class is
	instantiated by a layer which needs to add a header and/or trailer to
	a service data unit from an upper layer to form a protocol data unit
	used to implement the protocol services of its layer.
 */
class Composite : public Pdu , public Oscl::PDU::Api {
	private:
		/** This queue is the container used to implement the ordered list of
			packet fragments.
		 */
		HQueue<Fragment>	_fragments;

	public:
		/** The composite requires a freestore manager used to free the
			memory when the composite is deleted.
		 */
		Composite(FreeStore::Mgr &mgr) noexcept;

		/** Virtual destructors are simply a good idea.
		 */
		~Composite();

		/** Deep copy. Assumes this composite has already been
			equipped with fragments. Will copy the first fragments.
		 */
		void operator=(const Composite& other) noexcept;

		/** This function is used to append a trailer fragment to the end
			of the composite.
		 */
		void	append(Fragment* trailer) noexcept;

		/** This function is used to prepend a header fragment to the
			front of the composite.
		 */
		void	prepend(Fragment* header) noexcept;

		/** The overloaded delete operator is required of all concrete
			derivatives of Pdu, which is derived from a FreeStore::Object.
		 */
		void	operator delete(void *p,size_t size) noexcept;

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end.
			The position argument is the offset from the very first octet
			in the super PDU. The first valid octet (from the client
			perspective) is the offset from the firstValid argument.
			The nValid is the number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	ReadOnlyVisitor&	v,
								unsigned			position,
								unsigned			firstValid,
								unsigned			nValid
								) const noexcept;

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end.
			The position argument is the offset from the very first octet
			in the super PDU. The first valid octet (from the client
			perspective) is the offset from the firstValid argument.
			The nValid is the number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	Visitor&	v,
								unsigned	position,
								unsigned	firstValid,
								unsigned	nValid
								) noexcept;

		/** This operation returns the length of the PDU in octets.
		 */
		unsigned length() const noexcept;

		/** This operation adjusts the packet offsets to strip the
			specified number of octets from the start of the packet.
			This operation returns the actual number of octets stripped.
			Any packet fragments which are no longer used by the composite
			are deleted.
		 */
		unsigned /* nStripped */	stripHeader(unsigned n) noexcept;

		/** This operation adjusts the packet offsets to strip the
			specified number of octets from the end of the packet.
			This operation returns the actual number of octets stripped.
			Any packet fragments which are no longer used by the
			composite are deleted.
		 */
		unsigned /* nStripped */	stripTrailer(unsigned n) noexcept;

		/** This operation reads up to max number of bytes into the octet
			buffer referenced by the data octet pointer. The source octets
			are read beginning from the offset.
		 */
		unsigned 	read(	void*		data,
							unsigned	max,
							unsigned	offset
							) const noexcept;

		/** This operation replaces the "length" octets at the specified
			PDU "offset" with the octets from the "source". The "offset"
			must be within the valid range of the PDU. If the "offset"
			is not within range of the PDU, the PDU will not be changed
			and the operation will return zero. If the "offset" is
			within the valid range of the PDU, but the PDU cannot
			accomadate the "length", then the only the first octets
			from the source that will fit within the valid range
			of the PDU will be copied.
		 */
		unsigned	replace(	const uint8_t*	source,
								unsigned		length,
								unsigned		offset
								) noexcept;

	public:	// Oscl::PDU::Api
		/** Adjusts the PDU such that the first nOcets are "deleted"
			from the PDU.
		 */
//		unsigned	stripHeader(unsigned nOctets) noexcept;
		/** Adjusts the PDU such that the last nOctets are "deleted"
			from the PDU.
		 */
//		unsigned	stripTrailer(unsigned nOctets) noexcept;

		/** This operation returns the PDU to its origninating
			free store pool.
		 */
		void		free() noexcept;

	public:	// Oscl::Buffer::ReadOnly
		/** This operation returns the amount of valid data contained
			in the buffer.
		 */
//		unsigned	length() const noexcept;

		/** This operation adjusts the buffer such that its
			contents are no longer accessible. Subsequent
			append and copyIn operations reuse the empty buffer
			space.
			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset offset and length
			indicies. A scatter/gather buffer chain may release
			all buffers.
		 */
		void	empty() noexcept;

		/** This operation copies up to "length" valid bytes from this buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length
								) const noexcept;

		/** */
		unsigned	copyOut(	Oscl::Buffer::
								ReadWrite&		destination
								) const noexcept;

		/** This operation copies up to "length" valid bytes from this buffer
			at the specified "sourceOffset" from the beginning of the buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length,
								unsigned	sourceOffset
								) const noexcept;

		/** This operation returns the byte at the specified offset.
			If the offset is out of buffer range, the return value
			is undefined. It is the responsiblity of the caller to
			use the length() operation to find out if the offset is
			valid.
		 */
		unsigned char	copyOut(unsigned offset) const noexcept;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		bool	isEqual(	unsigned char	data,
							unsigned		offset
							) const noexcept;

		/** Returns true if the "length" bytes of source are exactly equal
			to the "length" bytes of this buffer starting from the
			"sourceOffset" of this buffer.
		 */
		bool	areEqual(	const void*		source,
							unsigned		length,
							unsigned		sourceOffset
							) const noexcept;

		/** Returns true if all of the bytes of "source" exactly matches
			the bytes of this buffer starting at the "sourceOffset" of
			this buffer.
		 */
		bool	areEqual(	const ReadOnly&		source,
							unsigned			sourceOffset
							) const noexcept;
	};

}
}

#endif
