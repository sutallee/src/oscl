/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "visitor.h"
#include "pdu.h"
#include "rofragment.h"

using namespace Oscl::Pdu;

ReadOnlyFragment::ReadOnlyFragment(	FreeStore::Mgr&		fsm,
									const ReadOnlyPdu*	pdu
									) noexcept :
		FreeStore::Object(fsm),
		_handle(pdu)
		{
	}

ReadOnlyFragment::ReadOnlyFragment(	FreeStore::Mgr&				fsm,
									Handle<const ReadOnlyPdu>&	pdu
									) noexcept:
		FreeStore::Object(fsm),
		_handle(pdu)
		{
	}

unsigned	ReadOnlyFragment::getLength() const noexcept {
	return _handle->length();
	}

bool /* stop */ ReadOnlyFragment::visit(	ReadOnlyVisitor&	v,
											unsigned			position,
											unsigned			firstValid,
											unsigned			nValid
											) const noexcept{
	if(v.accept(*this,position)) return true;
	if(_handle){
		return _handle->visit(v,position,0+firstValid,nValid);
		}
	return false;
	}

unsigned 	ReadOnlyFragment::read(	void*		data,
									unsigned	max,
									unsigned	offset
									) const noexcept{
	if(_handle){
		return _handle->read(data,max,offset+0);
		}
	return 0;
	}

void ReadOnlyFragment::operator delete(void *p,size_t size) noexcept{
		((ReadOnlyFragment*)p)->fsmfree(p);
		}

