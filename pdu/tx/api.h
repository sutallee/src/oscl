/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_tx_apih_
#define _oscl_pdu_tx_apih_

/** */
namespace Oscl {
/** */
namespace PDU {
/** */
namespace TX {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** This operation must return length if successful,
			zero if resources are unavailable, or the maximum
			MTU if resources are availabe, but the specified
			length cannot be accomadated.
		 */
		virtual unsigned	open(unsigned length) noexcept=0;

		/** This operation must return the number of octets
			actually appended. If the returned value is less
			than the requested length, insufficient resources
			are available to transmit the frame.
		 */
		virtual unsigned	append(const void* pdu,unsigned length) noexcept=0;

		/** */
		virtual void		close() noexcept=0;
	};

}
}
}

#endif
