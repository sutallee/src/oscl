/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_rx_itc_fwd_serviceh_
#define _oscl_pdu_rx_itc_fwd_serviceh_
#include "oscl/pdu/rx/itc/respmem.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/pdu/fwd/api.h"

/** */
namespace Oscl {
/** */
namespace PDU {
/** */
namespace RX {
/** */
namespace FWD {

/** */
class Service :	public Oscl::PDU::RX::Resp::Api,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Mt::Itc::Srv::CloseSync
				{
	private:
		/** */
		Oscl::Queue<Resp::RespMem>		_freeRespMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_myPapi;
		/** */
		Oscl::PDU::RX::Req::Api::SAP&	_source;
		/** */
		Oscl::Pdu::FWD::Api&			_destination;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::PDU::RX::Req::Api::SAP&	source,
					Oscl::Pdu::FWD::Api&			destination,
					Oscl::PDU::RX::Resp::RespMem	respMem[],
					unsigned						nRespMem
					) noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private: // Oscl::PDU::RX::Resp::Api
		/** */
		void	response(ReadResp& msg) noexcept;
	};

}
}
}
}



#endif
