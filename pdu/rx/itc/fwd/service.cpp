/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "service.h"

using namespace Oscl::PDU::RX::FWD;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::PDU::RX::Req::Api::SAP&	source,
					Oscl::Pdu::FWD::Api&			destination,
					Oscl::PDU::RX::Resp::RespMem	respMem[],
					unsigned						nRespMem
					) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,papi),
		_myPapi(papi),
		_source(source),
		_destination(destination)
		{
	for(unsigned i=0;i<nRespMem;++i){
		_freeRespMem.put(&respMem[i]);
		}
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	Oscl::PDU::RX::Resp::RespMem*	mem;
	while((mem=_freeRespMem.get())){
		Oscl::PDU::RX::Resp::Api::ReadResp*
		resp	= new (&mem->readMem)
					Oscl::PDU::RX::Resp::Api::
					ReadResp(	_source.getReqApi(),
								*this,
								_myPapi
								);
		_source.post(resp->getSrvMsg());
		}
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	// FIXME: This should cancel all read requests
	// and return the close request after all are
	// returned.
	for(;;);
	}


void	Service::response(ReadResp& msg) noexcept{
	Oscl::PDU::RX::Req::Api::ReadPayload
	payload	= msg.getSrvMsg()._payload;
	if(!payload._pdu){
		// FIXME: Happens only when closed.
		// Affects state and thus there's more
		// to it than just putting it back in
		// the free pool.
		msg.~ReadResp();
		_freeRespMem.put((Oscl::PDU::RX::Resp::RespMem*)&msg);
		return;
		}
	_destination.transfer(payload._pdu);
	payload._pdu	= 0;
	_source.post(msg.getSrvMsg());
	}

