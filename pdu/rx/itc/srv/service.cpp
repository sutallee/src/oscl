/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"

using namespace Oscl::PDU::RX::SRV;

Service::Service(Oscl::Mt::Itc::PostMsgApi& papi) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,papi),
		_sap(*this,papi)
		{
	}

Oscl::PDU::RX::Req::Api::SAP&	Service::getPduRxSAP() noexcept{
	return _sap;
	}

void	Service::transfer(Oscl::Pdu::Pdu* frame) noexcept{
	Oscl::PDU::RX::Req::Api::ReadReq*
	req	= _pending.get();
	if(!req) return;
	req->_payload._pdu	= frame;
	req->returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	// FIXME: What should this do ... anything?
	for(;;);
	}

void	Service::request(Oscl::PDU::RX::Req::Api::ReadReq& msg) noexcept{
	_pending.put(&msg);
	}

