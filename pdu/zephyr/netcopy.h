/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_zephyr_netcopyh_
#define _oscl_pdu_zephyr_netcopyh_

#include "oscl/pdu/visitor.h"
#include "net/buf.h"

/** */
namespace Oscl {
/** */
namespace Pdu {

class Composite;
class ReadOnlyComposite;
class Leaf;
class Fragment;
class ReadOnlyFragment;

/** */
namespace Zephyr {

/** This class is used to copy the content
	of a pdu into a Zephyr OS net_buf.
 */
class NetCopy : public Oscl::Pdu::ReadOnlyVisitor {
	private:
		/** */
		struct net_buf&	_destination;

		/** */
		bool			_overflow;

	public:
		/** */
		NetCopy(struct net_buf& destination) noexcept;

		/** RETURN: true if the net_buf was too small for the pdu.
		 */
		bool	overflow() const noexcept;

	public: // Oscl::Pdu::ReadOnlyVisitor
		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(
							const Composite&	p,
							unsigned			position
							) noexcept;

		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(
							const ReadOnlyComposite&	p,
							unsigned					position
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client. The offset indexes
			the first valid octet in the leaf buffer for the current client.
			The length is the the number of valid octets beginning from
			the offset argument.
		 */
		bool /* stop */ accept(
							const Leaf&		p,
							unsigned		position,
							unsigned		offset,
							unsigned		length
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(
							const Fragment&	p,
							unsigned		position
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(
							const ReadOnlyFragment&	p,
							unsigned				position
							) noexcept;
	};

}
}
}

#endif
