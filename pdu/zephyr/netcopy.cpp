/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "netcopy.h"
#include "oscl/pdu/composite.h"
#include "oscl/pdu/fragment.h"
#include "oscl/pdu/leaf.h"

using namespace Oscl::Pdu::Zephyr;

NetCopy::NetCopy(struct net_buf& destination) noexcept:
		_destination(destination),
		_overflow(false)
		{
	}

bool	NetCopy::overflow() const noexcept{
	return _overflow;
	}

bool /* stop */	NetCopy::accept(
					const Composite&	p,
					unsigned			position
					) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */	NetCopy::accept(
					const ReadOnlyComposite&	p,
					unsigned					position
					) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */ NetCopy::accept(
					const Leaf&	p,
					unsigned	position,
					unsigned	offset,
					unsigned	length
					) noexcept{

	// "position" is the current offset in the visited PDU.
	// "offset" is the offset from the beginning of the
	// leaf buffer to the first valid octet.
	// "length" is the number of valid octets from the "offset".
	unsigned		buffSize;
	const uint8_t*	buffer	= p.getBuffer(offset,buffSize);

	if(!buffer){
		// FIXME: offset is beyond buffer
		// This is a program logic error.
		for(;;);
		}

	if(length > buffSize){
		// This should never happen since the Composite
		// visit obtains "length" from fragment.getLength();
		length	= buffSize;
		}

	size_t
	remaining	= net_buf_tailroom(&_destination);

	if(length > remaining){
		_overflow	= true;
		length	= remaining;
		}

	net_buf_add_mem(
				&_destination,
				buffer,
				length
				);

	return _overflow;
	}

bool /* stop */ NetCopy::accept(
					const Fragment&	p,
					unsigned		position
					) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

bool /* stop */ NetCopy::accept(
					const ReadOnlyFragment&	p,
					unsigned				position
					) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

