/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_leafh_
#define _oscl_krux_pdu_leafh_
#include "pdu.h"

/** */
namespace Oscl {
/** */
namespace Pdu {

/** This abstract class represents all PDUs which are leaf nodes
	in a composite PDU. Subclasses include fixed length buffer
	PDUs and variable length buffer PDUs. Derivatives of this
	class refer to a single contiguous octet buffer.
 */

class Leaf : public Pdu {
	public:
		/** The constructor needs a reference to a freestore manager which
			is used to free the pdu to the correct memory pool when it
			is deleted by an anonymous client.
		 */
		Leaf(FreeStore::Mgr& mgr) noexcept;

		/** Adjusts the PDU such that the first nOcets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripHeader(unsigned nOctets) noexcept=0;
		/** Adjusts the PDU such that the last nOctets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripTrailer(unsigned nOctets) noexcept=0;

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end. The position
			argument is the offset from the very first octet in the super
			PDU. The first valid octet (from the client perspective) is
			the offset from the firstValid argument. The nValid is the
			number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	ReadOnlyVisitor&	v,
								unsigned			position,
								unsigned			firstValid,
								unsigned			nValid
								) const noexcept;

		/** This operation is called when the PDU is visited by a client
			to traverse the PDU tree from beginning to end. The position
			argument is the offset from the very first octet in the super
			PDU. The first valid octet (from the client perspective) is
			the offset from the firstValid argument. The nValid is the
			number of valid octets in this PDU subtree.
		 */
		bool /* stop */ visit(	Visitor&	v,
								unsigned	position,
								unsigned	firstValid,
								unsigned	nValid
								) noexcept;

		/** This abstract operation returns a pointer to the octet buffer
			implemented by this leaf node beginning at the specified offset,
			containing the number of octets specified by the length argument.
			An out_of_range "exception" is thrown if the specified offset
			and length are not within the bounds of the buffer.
		 */
		virtual const uint8_t*	getBuffer(	unsigned	offset,
											unsigned&	length
											) const noexcept = 0;

		/** This abstract operation returns a pointer to the octet buffer
			implemented by this leaf node beginning at the specified offset,
			containing the number of octets specified by the length argument.
			An out_of_range "exception" is thrown if the specified offset
			and length are not within the bounds of the buffer.
		 */
		virtual uint8_t*	getBuffer(	unsigned	offset,
									unsigned&	length
									) noexcept = 0;

		/** This operation attempts to append the specified number of
			octets from the specified source buffer to the Leaf octet
			buffer. The actual number appended is returned as a result.
		 */
		unsigned /* nAppended */ append(const void* src,unsigned length) noexcept;

		/** This operation returns the length of the PDU in octets.
		 */
		unsigned length() const noexcept;

		/** This operation reads up to max number of bytes into the octet
			buffer referenced by the data octet pointer. The source octets
			are read beginning from the offset.
		 */
		unsigned 	read(	void*		data,
							unsigned	max,
							unsigned	offset
							) const noexcept;

	protected:
		/** This abstract operation must be implemented by sub-classes
			to return a pointer to the start of the buffer.
		 */
		virtual uint8_t*			getBuffer() noexcept=0;

		/** This abstract operation must be implemented by sub-classes
			to return a pointer to the start of the buffer.
		 */
		virtual const uint8_t*	getBuffer() const noexcept=0;

		/** This abstract operation must be implemented by sub-classes
			to return the maximum size of the buffer contained by this
			leaf node.
		 */
		virtual unsigned	getBufferSize() noexcept=0;

		/** This abstract operation must be implemented by sub-classes
			to return the offset from the beginning of the buffer to the
			first valid octet in the buffer.
		 */
		virtual unsigned	getContentStart() const  noexcept=0;

		/** This abstract operation must be implemented by sub-classes
			to return the number of valid octets contained in the buffer.
		 */
		virtual unsigned	getContentLength() const  noexcept=0;

		/** This abstract operation must be implemented by sub-classes
			to add the argument length to the current content length of
			the buffer.
		 */
		virtual void		addLength(unsigned length) noexcept=0;
	};

}
}

#endif
