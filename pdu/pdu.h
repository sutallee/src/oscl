/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_pdu_pduh_
#define _oscl_krux_pdu_pduh_
#include "oscl/handle/refcount.h"
#include "oscl/freestore/object.h"
#include "oscl/handle/hqueue.h"
#include "ropdu.h"

/*
 Problem:
	The link field of a linked object is only available to a single
	client. Thus, the link field must be supplied as a separate object
	when associating a pdu with a composite pdu. This creates a problem
	of allocation :-(

	This problem only exists if we allow a PDU to be shared by more than
	one entity. For example if a broadcast packet is received and forwarded
	to more than one upper layer entity. Another example is if an entity
	receives a packet, and has to form a reply using a portion of the packet
	as well as forwarding the packet to the next higher layer.

	The preceding may be an argument for having an upper layer supply
	a handle to the lower layer, that can be used by the lower layer
	when forwarding an incomming packet back up to the upper layer.

+-----------+  +-------------+
| QueueItem |  | Handle<Pdu> |
+-----------+  +-------------+
       |

      +----------+   +-----------------+  +-------------+
      | RefCount |   | FreeStoreObject |  | Handle<Pdu> |
      +----------+   +-----------------+  +-------------+
           A                  A
           |                  |
           +------+   +-------+
                  |   |
                 +-----+
     +---------->| Pdu |
     |           +-----+
     |             A
     |             |
     |          +--+----------------+
     |          |                   |
     |   +--------------+     +-----------+
     +-<>| PduComposite |     | PduBuffer |
         +--------------+     +-----------+
                                     A
                                     |
                              +------+--------------+
                              |                     |
                      +----------------+  +-------------------+
                      | PduBufferFixed |  | PduBufferVariable |
                      +----------------+  +-------------------+


Transmit Scenario:

    +--------------+     +--------------+
    | :Handle<Pdu> |     | :Handle<Pdu> |
    +--------------+     +--------------+
                |          |
                |          |
                V          V
              +---------------+
              | :PduComposite |
              +---------------+
                /          |
               /           |
              |            |
              V            |
+-----------------+        |
| :PduBufferFixed |        |
+-----------------+        V
                      +---------------+
                      | :PduComposite |
                      +---------------+




         +---------------+   +--------------+
         | :PduComposite |   | :Handle<Pdu> |
         +---------------+   +--------------+


+---------------+
| :PduComposite |
+---------------+

+-----------------+
| :PduBufferFixed |
+-----------------+

+--------------+
| :Handle<Pdu> |
+--------------+

+--------------------+
| :PduBufferVariable |
+--------------------+

+-------------+
| :OctetArray |
+-------------+

 */

/** */
namespace Oscl {
/** */
namespace Pdu {

class Visitor;
class ReadOnlyVisitor;

/** This abstract class represents a protocol data unit that is passed
	betwen two layers in a protocol implementation.
 */
class Pdu :	public ReadOnlyPdu , public Oscl::HQueueItem<Pdu> {
	public:
		/** The constructor needs a reference to a freestore manager which
			is used to free the pdu to the correct memory pool when it is
			deleted by an anonymous client.
		 */
		Pdu(FreeStore::Mgr& mgr) noexcept;

		/** This abstract operation is called when the PDU is visited by
			a client to traverse the PDU tree from beginning to end.
			The position argument is the offset from the very first octet
			in the super PDU. The first valid
			octet (from the client perspective) is the offset from the
			firstValid argument. The nValid is the number of valid octets
			in this PDU subtree.
		 */
		virtual bool /* stop */ visit(	Visitor&	v,
										unsigned	position,
										unsigned	firstValid,
										unsigned	nValid
										) noexcept=0;

	public:	// Potential Oscl::PDU::Api
		/** Adjusts the PDU such that the first nOcets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripHeader(unsigned nOctets) noexcept=0;
		/** Adjusts the PDU such that the last nOctets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripTrailer(unsigned nOctets) noexcept=0;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		bool	isEqual(	unsigned char	data,
							unsigned		offset
							) const noexcept;

	public:	// ReadOnlyPdu
		/** This abstract operation is called when the PDU is visited by
			a client to traverse the PDU tree from beginning to end.
			The position argument is the offset from the very first octet
			in the super PDU. The first valid
			octet (from the client perspective) is the offset from the
			firstValid argument. The nValid is the number of valid octets
			in this PDU subtree.
		 */
		virtual bool /* stop */ visit(	ReadOnlyVisitor&	v,
										unsigned			position,
										unsigned			firstValid,
										unsigned			nValid
										) const noexcept=0;


		/** This abstract operation returns the length of the PDU in octets.
		 */
		virtual unsigned length() const noexcept=0;

		/** This operation reads up to max number of bytes into the octet
			buffer referenced by the data octet pointer. The source octets
			are read beginning from the offset.
		 */
		virtual unsigned 	read(	void*		data,
									unsigned	max,
									unsigned	offset
									) const noexcept=0;
	};

}
}

#endif
