/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_apih_
#define _oscl_pdu_apih_
#include "oscl/buffer/api.h"

/** */
namespace Oscl {
/** */
namespace PDU {

/** */
class Api : public Oscl::Buffer::ReadOnly {
	public:
		/** */
		virtual ~Api(){}

	public:
		/** Adjusts the PDU such that the first nOcets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripHeader(unsigned nOctets) noexcept=0;
		/** Adjusts the PDU such that the last nOctets are "deleted"
			from the PDU.
		 */
		virtual unsigned	stripTrailer(unsigned nOctets) noexcept=0;

		/** This operation returns the PDU to its origninating
			free store pool.
		 */
		virtual void		free() noexcept=0;

	public:	// Oscl::Buffer::ReadOnly
		/** This operation returns the amount of valid data contained
			in the buffer.
		 */
		virtual unsigned	length() const noexcept=0;

		/** This operation adjusts the buffer such that its
			contents are no longer accessible. Subsequent
			append and copyIn operations reuse the empty buffer
			space.
			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset offset and length
			indicies. A scatter/gather buffer chain may release
			all buffers.
		 */
		virtual void	empty() noexcept=0;

		/** This operation copies up to "length" valid bytes from this buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		virtual unsigned	copyOut(	void*		destination,
										unsigned	length
										) const noexcept=0;

		/** */
		virtual unsigned	copyOut(	Oscl::Buffer::
										ReadWrite&		destination
										) const noexcept=0;

		/** This operation copies up to "length" valid bytes from this buffer
			at the specified "sourceOffset" from the beginning of the buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		virtual unsigned	copyOut(	void*		destination,
										unsigned	length,
										unsigned	sourceOffset
										) const noexcept=0;

		/** This operation returns the byte at the specified offset.
			If the offset is out of buffer range, the return value
			is undefined. It is the responsiblity of the caller to
			use the length() operation to find out if the offset is
			valid.
		 */
		virtual unsigned char	copyOut(unsigned offset) const noexcept=0;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		virtual bool	isEqual(	unsigned char	data,
									unsigned		offset
									) const noexcept=0;

		/** Returns true if the "length" bytes of source are exactly equal
			to the "length" bytes of this buffer starting from the
			"sourceOffset" of this buffer.
		 */
		virtual bool	areEqual(	const void*		source,
									unsigned		length,
									unsigned		sourceOffset
									) const noexcept=0;

		/** Returns true if all of the bytes of "source" exactly matches
			the bytes of this buffer starting at the "sourceOffset" of
			this buffer.
		 */
		virtual bool	areEqual(	const ReadOnly&		source,
									unsigned			sourceOffset
									) const noexcept=0;
	};

}
}

#endif
