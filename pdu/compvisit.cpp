/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "compvisit.h"
#include "composite.h"
#include "fragment.h"
#include "leaf.h"

using namespace Oscl::Pdu;

CopyRW::CopyRW(Oscl::Buffer::ReadWrite& destination) noexcept:
		_destination(destination)
		{
	}

bool /* stop */	CopyRW::accept(	const Composite&	p,
								unsigned			position
								) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */	CopyRW::accept(	const ReadOnlyComposite&	p,
								unsigned					position
								) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */ CopyRW::accept(	const Leaf&	p,
								unsigned	position,
								unsigned	offset,
								unsigned	length
								) noexcept{
	// "position" is the current offset in the visited PDU.
	// "offset" is the offset from the beginning of the
	// leaf buffer to the first valid octet.
	// "length" is the number of valid octets from the "offset".
	unsigned		buffSize;
	const uint8_t*	buffer	= p.getBuffer(offset,buffSize);

	if(!buffer){
		// FIXME: offset is beyond buffer
		// This is a program logic error.
		for(;;);
		}

	if(length > buffSize){
		// This should never happen since the Composite
		// visit obtains "length" from fragment.getLength();
		length	= buffSize;
		}

	unsigned
	len	= _destination.append(buffer,length);

	if(!len) return true;

	return false;
	}

bool /* stop */ CopyRW::accept(const Fragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

bool /* stop */ CopyRW::accept(const ReadOnlyFragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

EqualRO::EqualRO(const Oscl::Buffer::ReadOnly& source) noexcept:
		_source(source),
		_length(source.length()),
		_match(false)
		{
	}

bool /* stop */	EqualRO::accept(	const Composite&	p,
									unsigned			position
									) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */	EqualRO::accept(	const ReadOnlyComposite&	p,
									unsigned					position
									) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */ EqualRO::accept(	const Leaf&	p,
									unsigned	position,
									unsigned	offset,
									unsigned	length
									) noexcept{
	// "position" is the current offset in the visited PDU.
	// "offset" is the offset from the beginning of the
	// leaf buffer to the first valid octet.
	// "length" is the number of valid octets from the "offset".

	if(position >= _length){
		return true;
		}

	unsigned		buffSize;
	const uint8_t*	buffer	= p.getBuffer(offset,buffSize);

	if(!buffer){
		// FIXME: offset is beyond buffer
		// This is a program logic error.
		for(;;);
		}

	if(length > buffSize){
		// This should never happen since the Composite
		// visit obtains "length" from fragment.getLength();
		length	= buffSize;
		}

	_match	= _source.areEqual(	buffer,
								length,
								position
								);
	if(!_match){
		// Terminate the traversal
		return true;
		}

	return false;
	}

bool /* stop */ EqualRO::accept(const Fragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

bool /* stop */ EqualRO::accept(const ReadOnlyFragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

EqualBuff::EqualBuff(	const void*		source,
						unsigned		length
					) noexcept:
		_source((const unsigned char*)source),
		_length(length),
		_match(false)
		{
	}

bool /* stop */	EqualBuff::accept(	const Composite&	p,
									unsigned			position
									) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */	EqualBuff::accept(	const ReadOnlyComposite&	p,
									unsigned					position
									) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */ EqualBuff::accept(	const Leaf&	p,
									unsigned	position,
									unsigned	offset,
									unsigned	length
									) noexcept{
	// "position" is the current offset in the visited PDU.
	// "offset" is the offset from the beginning of the
	// leaf buffer to the first valid octet.
	// "length" is the number of valid octets from the "offset".

	if(position >= _length){
		return true;
		}

	unsigned		buffSize;
	const uint8_t*	buffer	= p.getBuffer(offset,buffSize);

	if(!buffer){
		// FIXME: offset is beyond buffer
		// This is a program logic error.
		for(;;);
		}

	unsigned	len	= _length - position;

	if(length > buffSize){
		// This should never happen since the Composite
		// visit obtains "length" from fragment.getLength();
		length	= buffSize;
		}

	if(length > len){
		length	= len;
		}

	_match	= (memcmp(&_source[position],buffer,length))?false:true;

	if(!_match){
		// Terminate the traversal
		return true;
		}

	return false;
	}

bool /* stop */ EqualBuff::accept(const Fragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

bool /* stop */ EqualBuff::accept(const ReadOnlyFragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}

Replace::Replace(	const void*		source,
					unsigned		length
					) noexcept:
		_source((const unsigned char*)source),
		_length(length),
		_copied(0)
		{
	}

bool /* stop */	Replace::accept(	Composite&	p,
									unsigned	position
									) noexcept{
	// Nothing special to do for a composite.
	// Just continue the search
	return false;
	}

bool /* stop */ Replace::accept(	Leaf&		p,
									unsigned	position,
									unsigned	offset,
									unsigned	length
									) noexcept{
	// "position" is the current offset in the visited PDU.
	// "offset" is the offset from the beginning of the
	// leaf buffer to the first valid octet.
	// "length" is the number of valid octets from the "offset".

	if(position >= _length){
		return true;
		}

	unsigned		buffSize;
	uint8_t*	buffer	= p.getBuffer(offset,buffSize);

	if(!buffer){
		// FIXME: offset is beyond buffer
		// This is a program logic error.
		for(;;);
		}

	unsigned	len	= _length - position;

	if(length > buffSize){
		// This should never happen since the Composite
		// visit obtains "length" from fragment.getLength();
		length	= buffSize;
		}

	if(length > len){
		length	= len;
		}

	memcpy(buffer,&_source[position],length);

	_copied	+= length;

	if(_copied >= _length){
		// Terminate the traversal
		return true;
		}

	return false;
	}

bool /* stop */ Replace::accept(Fragment& p,unsigned position) noexcept{
	// Nothing special to do for a fragment.
	// Just continue the search
	return false;
	}


