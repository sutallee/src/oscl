/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "composite.h"
#include "compvisit.h"

using namespace Oscl::Pdu;
// Composite

Composite::Composite(FreeStore::Mgr& mgr) noexcept:Pdu(mgr){
	}

Composite::~Composite(){
	while(Handle<Fragment> next=_fragments.get()){
		next	= 0;
		}
	}

void Composite::operator=(const Composite& other) noexcept{
	Handle<Fragment>	empty	= _fragments.first();
	Handle<Fragment>	next	= other._fragments.first();
	if(empty && next){
		do {
			empty->copy(next);
			if(!(next = other._fragments.next(next))) break;
			if(!(empty = _fragments.next(empty))) break;
			} while(true);
		}
	}

void	Composite::append(Fragment* trailer) noexcept {
	_fragments.put(trailer);
	}

void	Composite::prepend(Fragment* header) noexcept {
	_fragments.push(header);
	}

void Composite::operator delete(void *p,size_t size) noexcept{
		((Composite*)p)->fsmfree(p);
		}

bool /* stop */ Composite::visit(	ReadOnlyVisitor&	v,
									unsigned			position,
									unsigned			firstValid,
									unsigned			nValid
									) const noexcept{
	unsigned	len		= 0;
	if(v.accept(*this,position)) return true;

	for(	Handle<Fragment> next=_fragments.first();
			next;
			next=_fragments.next(next)
			){
		len	= next->getLength();
		if(firstValid < len){
			if(nValid < len){
				len	= nValid;
				}
			if(next->visit(v,position,firstValid,len)) return true;
			nValid		-= len;
			firstValid	= 0;
			position	+= len;
			}
		else{
			firstValid	-= len;
			}
		}
	return false;
	}

bool /* stop */ Composite::visit(	Visitor&	v,
									unsigned	position,
									unsigned	firstValid,
									unsigned	nValid
									) noexcept{
	unsigned	len		= 0;
	if(v.accept(*this,position)) return true;

	for(	Handle<Fragment> next=_fragments.first();
			next;
			next=_fragments.next(next)
			){
		len	= next->getLength();
		if(firstValid < len){
			if(nValid < len){
				len	= nValid;
				}
			if(next->visit(v,position,firstValid,len)) return true;
			nValid		-= len;
			firstValid	= 0;
			position	+= len;
			}
		else{
			firstValid	-= len;
			}
		}
	return false;
	}

unsigned	Composite::length() const noexcept{
	unsigned	len	= 0;
	for(	Handle<Fragment> next=_fragments.first();
			next;
			len+=next->getLength(),next=_fragments.next(next)
			);
	return len;
	}

unsigned /* nStripped */	Composite::stripHeader(unsigned n) noexcept{
	unsigned	nStripped;
	unsigned	totalStripped	= 0;
	while(Handle<Fragment> next=_fragments.first()){
		nStripped		= next->stripHeader(n);
		totalStripped	+= nStripped;
		n				-= nStripped;
		if(n==0){
			if(next->getLength()==0){
				_fragments.get();
				next	= 0;
				}
			break;
			}
		_fragments.get();
		next	= 0;
		if(n==0) break;
		}
	return totalStripped;
	}

unsigned /* nStripped */	Composite::stripTrailer(unsigned n) noexcept{
	unsigned	nStripped;
	unsigned	totalStripped	= 0;
	while(Handle<Fragment> next=_fragments.last()){
		nStripped		= next->stripTrailer(n);
		totalStripped	+= nStripped;
		n				-= nStripped;
		if(n==0){
			if(next->getLength()==0){
				_fragments.remove(next);
				next	= 0;
				}
			break;
			}
		_fragments.remove(next);
		next	= 0;
		}
	return totalStripped;
	}

unsigned 	Composite::read(	void*		data,
								unsigned	max,
								unsigned	offset
								) const noexcept{
	unsigned char*	p	= (unsigned char*)data;
	unsigned	actualOctetsRead;
	unsigned	totalOctetsRead	= 0;
	unsigned	len;
	for(	Handle<Fragment> next=_fragments.first();
			next;
			next=_fragments.next(next)
			){
		len	= next->getLength();
		if(offset < len){
			if(max<len){
				len	= max;
				}
			actualOctetsRead	= next->read(p,len,offset);
			offset				= 0;
			max					-= actualOctetsRead;
			p					+= actualOctetsRead;
			totalOctetsRead		+= actualOctetsRead;
			if(!max) return totalOctetsRead;
			}
		else{
			offset	-= len;
			}
		}
	return totalOctetsRead;		// Reached end of buffer
	}

unsigned	Composite::replace(	const uint8_t*	source,
								unsigned		length,
								unsigned		offset
								) noexcept{
	Replace	visitor(source,length);
	visit(visitor,0,offset,length);
	return visitor._copied;
	}

// Oscl::PDU::Api

void		Composite::free() noexcept{
	delete this;
	}

void	Composite::empty() noexcept{
	stripHeader(length());
	}

unsigned	Composite::copyOut(	void*		destination,
								unsigned	length
								) const noexcept{
	return read(destination,length,0);
	}

unsigned	Composite::copyOut(	Oscl::Buffer::
								ReadWrite&		destination
								) const noexcept{
	// This will require a "visitor" to piece together the
	// appropriate fragments to "destination.append()" the
	// entire PDU.
	destination.empty();
	CopyRW	visitor(destination);
	visit(visitor,0,0,~0);
	return destination.length();
	}

unsigned	Composite::copyOut(	void*		destination,
								unsigned	length,
								unsigned	sourceOffset
								) const noexcept{
	return read(destination,length,sourceOffset);
	}

unsigned char	Composite::copyOut(unsigned offset) const noexcept{
	unsigned char	buffer[1];
	read(buffer,1,offset);
	return buffer[0];
	}

bool	Composite::isEqual(	unsigned char	data,
							unsigned		offset
							) const noexcept{
	EqualBuff	visitor(&data,1);
	visit(visitor,0,offset,1);
	return visitor._match;
	}

bool	Composite::areEqual(	const void*		source,
								unsigned		length,
								unsigned		sourceOffset
								) const noexcept{
	EqualBuff	visitor(source,length);
	visit(visitor,0,sourceOffset,length);
	return visitor._match;
	}

bool	Composite::areEqual(	const ReadOnly&		source,
								unsigned			sourceOffset
								) const noexcept{
	EqualRO	visitor(source);
	visit(visitor,0,sourceOffset,source.length());
	return visitor._match;
	}

