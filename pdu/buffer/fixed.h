/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_buffer_fixedh_
#define _oscl_pdu_buffer_fixedh_
#include "oscl/pdu/api.h"

/** */
namespace Oscl {
/** */
namespace PDU {
/** */
namespace Buffer {

namespace Allocator {
	class Api;
}

/** */
class Fixed :	public Oscl::PDU::Api,
				public Oscl::Buffer::ReadWrite
				{
	private:
		/** */
		unsigned			_offset;
		/** */
		unsigned			_length;
		/** */
		const unsigned		_buffSize;
		/** */
		unsigned char*		_buffer;
		/** */
		Oscl::PDU::Buffer::Allocator::Api&		_allocator;

	public:
		/** */
		Fixed(	Oscl::PDU::Buffer::Allocator::Api&	allocator,
				void*			buffer,
				unsigned		buffSize,
				unsigned		length=0
				) noexcept;

#if 0
		/** */
		unsigned	append(	const void*		buffer,
							unsigned		length
							) noexcept;
#endif

	public: // Oscl::PDU::Api
		/** */
		unsigned	stripHeader(unsigned nOctets) noexcept;
		/** */
		unsigned	stripTrailer(unsigned nOctets) noexcept;
		/** */
		void		free() noexcept;

	public:	// Oscl::Buffer::ReadOnly
		/** This operation returns the amount of valid data contained
			in the buffer.
		 */
		unsigned	length() const noexcept;

		/** This operation adjusts the buffer such that its
			contents are no longer accessible. Subsequent
			append and copyIn operations reuse the empty buffer
			space.
			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset offset and length
			indicies. A scatter/gather buffer chain may release
			all buffers.
		 */
		void	empty() noexcept;

		/** This operation copies up to "length" valid bytes from this buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length
								) const noexcept;

		/** */
		unsigned	copyOut(ReadWrite& destination) const noexcept;

		/** This operation copies up to "length" valid bytes from this buffer
			at the specified "sourceOffset" from the beginning of the buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length,
								unsigned	sourceOffset
								) const noexcept;

		/** This operation returns the byte at the specified offset.
			If the offset is out of buffer range, the return value
			is undefined. It is the responsiblity of the caller to
			use the length() operation to find out if the offset is
			valid.
		 */
		unsigned char	copyOut(unsigned offset) const noexcept;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		bool	isEqual(	unsigned char	data,
							unsigned		offset
							) const noexcept;

		/** Returns true if the "length" bytes of source are exactly equal
			to the "length" bytes of this buffer starting from the "sourceOffset"
			of this buffer.
		 */
		bool	areEqual(	const void*	source,
							unsigned	length,
							unsigned	offset
							) const noexcept;

		bool	areEqual(	const ReadOnly&	source,
							unsigned		offset
							) const noexcept;

	public:	// Oscl::Buffer::ReadWrite
		/** This operation copies count bytes of data from the source into
			the buffer starting at the beginning of the buffer. The
			number of bytes actually copied to the buffer is returned
			as a result. The number of bytes actually copied may be
			less than the requested count bytes if this buffer is not large
			enough to hold all the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		unsigned	copyIn(	const void*	source,
							unsigned	count
							) noexcept;

		/** This operation copies the valid bytes in the source buffer into
			this buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of valid bytes in the source buffer if this
			buffer is not large enough to hold all of the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		unsigned	copyIn(const ReadOnly& source) noexcept;

		public: //------------ Append Operations ---------- //

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data.
		 */
		unsigned	append(	const void*	source,
							unsigned	length
							) noexcept;

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data or the source does not contain
			the "length" bytes of data.
		 */
		unsigned	append(	const ReadOnly&	source,
							unsigned		length
							) noexcept;

		/** This operation copies all bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of bytes in the "source" if the buffer is not
			large enough to hold all the source data.
		 */
		unsigned	append(const ReadOnly& source) noexcept;
#if 0
	public:
		/** */
		unsigned	length() const noexcept;

		/** */
		bool	isEqual(	unsigned char	octet,
							unsigned		offset
							) const noexcept;

		/** */
		bool	areEqual(	const void*		octets,
							unsigned		offset,
							unsigned		length
							) const noexcept;

		/** */
		unsigned	copyOut(	void*		buffer,
								unsigned	offset,
								unsigned	length
								) const noexcept;
#endif
	};

}
}
}

#endif
