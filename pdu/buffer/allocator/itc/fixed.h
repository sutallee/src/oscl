/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_buffer_allocator_itc_fixedh_
#define _oscl_pdu_buffer_allocator_itc_fixedh_
#include <new>
#include "oscl/memory/block.h"
#include "oscl/pdu/buffer/fixed.h"
#include "oscl/pdu/buffer/allocator/itc/reqapi.h"
#include "oscl/pdu/buffer/allocator/api.h"

/** */
namespace Oscl {
/** */
namespace PDU {
/** */
namespace Buffer {
/** */
namespace Allocator {

/** */
template <unsigned size>
class Fixed : public Oscl::PDU::Buffer::Allocator::Api {
	public:
		/** */
		Oscl::PDU::Buffer::Fixed	_fixedBuff;

	private:
		/** */
		union {
			Oscl::Memory::AlignedBlock<size>					_buffer;
			Oscl::Memory::AlignedBlock<sizeof(Req::FreeReq)>	_msg;
			}_mem;

		/** */
		Req::Api::SAP&	_sap;

	public:
		/** */
		Fixed(Req::Api::SAP& sap) noexcept:
			_fixedBuff(*this,&_mem,sizeof(_mem)),
			_sap(sap){}

		/** */
		virtual ~Fixed(){}

		/** */
		Oscl::PDU::Api&	getBufferApi() noexcept{
			return _fixedBuff;
			}

		/** */
		unsigned char*	getBuffer() noexcept{
			return (unsigned char*)&_mem;
			}

		/** */
		unsigned		length() const noexcept{
			return sizeof(_mem);
			}

	private:	// Oscl::PDU::Buffer::Allocator::Api
		void	free(void* buffer) noexcept{
			Oscl::PDU::Buffer::Allocator::Req::FreeReq*	req = new (&_mem)
				Oscl::PDU::Buffer::Allocator::Req::FreeReq(	_sap.getReqApi(),
															buffer,
															this
															);
			_sap.post(*req);
			}
	};

#if 0
template <unsigned size>
union FixedMem {
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock< sizeof(Fixed<size>) >	mem;
	};
#endif

}
}
}
}

#endif
