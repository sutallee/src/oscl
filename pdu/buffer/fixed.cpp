/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "fixed.h"
#include "oscl/pdu/buffer/allocator/api.h"

using namespace Oscl::PDU::Buffer;

Fixed::Fixed(	Allocator::Api&		allocator,
				void*				buffer,
				unsigned			buffSize,
				unsigned			length
				) noexcept:
		_offset(0),
		_length(length),
		_buffSize(buffSize),
		_buffer((unsigned char*)buffer),
		_allocator(allocator)
		{
	}

unsigned	Fixed::stripHeader(unsigned nOctets) noexcept{
	if(nOctets > _length){
		nOctets	= _length;
		}
	_offset	+= nOctets;
	_length	-= nOctets;
	return nOctets;
	}

unsigned	Fixed::stripTrailer(unsigned nOctets) noexcept{
	if(nOctets > _length){
		nOctets	= _length;
		}
	_length	-=	nOctets;
	return nOctets;
	}

void		Fixed::free() noexcept{
	_allocator.free(_buffer);
	}

unsigned	Fixed::length() const noexcept{
	return _length;
	}

void	Fixed::empty() noexcept{
	_offset	= 0;
	_length	= 0;
	}

unsigned	Fixed::copyOut(	void*		destination,
							unsigned	length
							) const noexcept{
	if(length > _length){
		length	= _length;
		}
	memcpy(destination,&_buffer[_offset],length);
	return length;
	}

unsigned	Fixed::copyOut(ReadWrite& destination) const noexcept{
	return destination.copyIn(&_buffer[_offset],_length);
	}

unsigned	Fixed::copyOut(	void*		destination,
							unsigned	length,
							unsigned	sourceOffset
							) const noexcept{
	if(sourceOffset >= _length){
		return 0;
		}
	const unsigned	remaining	= _length - sourceOffset;
	if(length > remaining){
		length	= remaining;
		}
	memcpy(destination,&_buffer[_offset+sourceOffset],length);
	return length;
	}

unsigned char	Fixed::copyOut(unsigned offset) const noexcept{
	if(offset >= _length){
		return 0;
		}
	return _buffer[_offset+offset];
	}

bool	Fixed::isEqual(	unsigned char	data,
						unsigned		offset
						) const noexcept{
	if(offset >= _length){
		return 0;
		}
	return (_buffer[_offset+offset] == data);
	}

bool	Fixed::areEqual(	const void*	source,
							unsigned	length,
							unsigned	offset
							) const noexcept{
	if(offset >= _length){
		return false;
		}
	if(length > _length){
		return false;
		}
	return !memcmp(source,&_buffer[_offset+offset],length);
	}


bool	Fixed::areEqual(	const ReadOnly&	source,
							unsigned		offset
							) const noexcept{
	if(offset >= _length){
		return false;
		}
	return source.areEqual(&_buffer[_offset+offset],0,_length);
	}


unsigned	Fixed::copyIn(	const void*	source,
							unsigned	count
							) noexcept{
	if(count > _buffSize){
		count	= _buffSize;
		}
	memcpy(_buffer,source,count);
	_length	= count;
	_offset	= 0;
	return count;
	}

unsigned	Fixed::copyIn(const ReadOnly& source) noexcept{
	_length	= source.copyOut(_buffer,_buffSize);
	_offset	= 0;
	return _length;
	}

unsigned	Fixed::append(	const void*	source,
							unsigned	length
							) noexcept{
	const unsigned	offset		= _length+_offset;
	const unsigned	remaining	= _buffSize-offset;
	if(length > remaining){
		length	= remaining;
		}
	memcpy(&_buffer[offset],source,length);
	_length	+= length;
	return length;
	}

unsigned	Fixed::append(	const ReadOnly&	source,
							unsigned		length
							) noexcept{
	const unsigned	offset		= _length+_offset;
	const unsigned	remaining	= _buffSize-offset;
	if(length > remaining){
		length	= remaining;
		}
	length	= source.copyOut(&_buffer[offset],length);
	_length	+= length;
	return length;
	}

unsigned	Fixed::append(const ReadOnly& source) noexcept{
	const unsigned	offset		= _length+_offset;
	const unsigned	remaining	= _buffSize-offset;
	unsigned		length		= source.copyOut(&_buffer[offset],remaining);
	_length	+= length;
	return length;
	}

