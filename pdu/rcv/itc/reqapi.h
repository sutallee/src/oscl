/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_rcv_itc_reqapih_
#define _oscl_pdu_rcv_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {

/** */
namespace PDU {

/** */
namespace Rcv {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the ReceiveReq
			ITC message.
		 */
		class ReceivePayload {
			public:
				/**	This is a pointer to the PDU data buffer into which the
					incomming data is written.
				 */
				void*	_dest;

				/**	This is the maximum number of bytes that the dest array
					can contain.
				 */
				unsigned int	_length;

			public:
				/** The ReceivePayload constructor. */
				ReceivePayload(

					void*	dest,
					unsigned int	length
					) noexcept;

			};

		/**	This message supplies a buffer to the server that is to
			be used to receive a pdu. The buffer is filled with the
			payload of the receive PDU and then the message is returned.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PDU::Rcv::Req::Api,
					ReceivePayload
					>		ReceiveReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the CancelReceiveReq
			ITC message.
		 */
		class CancelReceivePayload {
			public:
				ReceiveReq&	_requestToCancel;

			public:
				/** The CancelReceivePayload constructor. */
				CancelReceivePayload(ReceiveReq& requestToCancel) noexcept;
			};

		/**	This is a message used to cancel a ReceiveReq.
		 */	
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PDU::Rcv::Req::Api,
					CancelReceivePayload
					>		CancelReceiveReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::PDU::Rcv::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::PDU::Rcv::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a ReceiveReq is received.
		 */
		virtual void	request(	Oscl::PDU::Rcv::
									Req::Api::ReceiveReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a CancelReceiveReq is received.
		 */
		virtual void	request(	Oscl::PDU::Rcv::
									Req::Api::CancelReceiveReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
