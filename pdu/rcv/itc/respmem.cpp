/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::PDU::Rcv;

Resp::Api::ReceiveResp&
	Resp::ReceiveMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							void*	dest,
							unsigned int	length
							) noexcept{
	Req::Api::ReceivePayload*
		p	=	new(&payload)
				Req::Api::ReceivePayload(
							dest,
							length
							);
	Resp::Api::ReceiveResp*
		r	=	new(&resp)
			Resp::Api::ReceiveResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::CancelReceiveResp&
	Resp::CancelReceiveMem::build(	Req::Api::SAP&	sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
						Req::Api::ReceiveReq&	requestToCancel
						) noexcept{

	Req::Api::CancelReceivePayload*
		p	=	new(&payload)
				Req::Api::CancelReceivePayload(	requestToCancel);

	Resp::Api::CancelReceiveResp*
		r	=	new(&resp)
			Resp::Api::CancelReceiveResp(	sap.getReqApi(),
							respApi,
							clientPapi,
							*p
							);

	return *r;
	}

