/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "rocomposite.h"
#include "compvisit.h"

using namespace Oscl::Pdu;
// ReadOnlyComposite

ReadOnlyComposite::ReadOnlyComposite(FreeStore::Mgr& mgr) noexcept:
		ReadOnlyPdu(mgr)
		{
	}

ReadOnlyComposite::~ReadOnlyComposite(){
	while(Handle<ReadOnlyFragment> next=_fragments.get()){
		next	= 0;
		}
	}

void	ReadOnlyComposite::append(ReadOnlyFragment* trailer) noexcept {
	_fragments.put(trailer);
	}

void	ReadOnlyComposite::prepend(ReadOnlyFragment* header) noexcept {
	_fragments.push(header);
	}

void ReadOnlyComposite::operator delete(void *p,size_t size) noexcept{
		((ReadOnlyComposite*)p)->fsmfree(p);
		}

bool /* stop */ ReadOnlyComposite::visit(	ReadOnlyVisitor&	v,
											unsigned			position,
											unsigned			firstValid,
											unsigned			nValid
											) const noexcept{
	unsigned	len		= 0;
	if(v.accept(*this,position)) return true;

	for(	Handle<ReadOnlyFragment> next=_fragments.first();
			next;
			next=_fragments.next(next)
			){
		len	= next->getLength();
		if(firstValid < len){
			if(nValid < len){
				len	= nValid;
				}
			if(next->visit(v,position,firstValid,len)) return true;
			nValid		-= len;
			firstValid	= 0;
			position	+= len;
			}
		else{
			firstValid	-= len;
			}
		}
	return false;
	}

unsigned	ReadOnlyComposite::length() const noexcept{
	unsigned	len	= 0;
	for(	Handle<ReadOnlyFragment> next=_fragments.first();
			next;
			len+=next->getLength(),next=_fragments.next(next)
			);
	return len;
	}

unsigned 	ReadOnlyComposite::read(	void*		data,
										unsigned	max,
										unsigned	offset
										) const noexcept{
	unsigned char*	p	= (unsigned char*)data;
	unsigned	actualOctetsRead;
	unsigned	totalOctetsRead	= 0;
	unsigned	len;
	for(	Handle<ReadOnlyFragment> next=_fragments.first();
			next;
			next=_fragments.next(next)
			){
		len	= next->getLength();
		if(offset < len){
			if(max<len){
				len	= max;
				}
			actualOctetsRead	= next->read(p,len,offset);
			offset				= 0;
			max					-= actualOctetsRead;
			p					+= actualOctetsRead;
			totalOctetsRead		+= actualOctetsRead;
			if(!max) return totalOctetsRead;
			}
		else{
			offset	-= len;
			}
		}
	return totalOctetsRead;		// Reached end of buffer
	}

// Oscl::PDU::Api

void		ReadOnlyComposite::free() noexcept{
	delete this;
	}

void	ReadOnlyComposite::empty() noexcept{
	// FIXME:
	for(;;);
	}

unsigned	ReadOnlyComposite::copyOut(	void*		destination,
										unsigned	length
										) const noexcept{
	return read(destination,length,0);
	}

unsigned	ReadOnlyComposite::copyOut(	Oscl::Buffer::
										ReadWrite&		destination
										) const noexcept{
	// This will require a "visitor" to piece together the
	// appropriate fragments to "destination.append()" the
	// entire PDU.
	destination.empty();
	CopyRW	visitor(destination);
	visit(visitor,0,0,~0);
	return destination.length();
	}

unsigned	ReadOnlyComposite::copyOut(	void*		destination,
										unsigned	length,
										unsigned	sourceOffset
										) const noexcept{
	return read(destination,length,sourceOffset);
	}

unsigned char	ReadOnlyComposite::copyOut(unsigned offset) const noexcept{
	unsigned char	buffer[1];
	read(buffer,1,offset);
	return buffer[0];
	}

bool	ReadOnlyComposite::isEqual(	unsigned char	data,
									unsigned		offset
									) const noexcept{
	EqualBuff	visitor(&data,1);
	visit(visitor,0,offset,1);
	return visitor._match;
	}

bool	ReadOnlyComposite::areEqual(	const void*		source,
										unsigned		length,
										unsigned		sourceOffset
										) const noexcept{
	EqualBuff	visitor(source,length);
	visit(visitor,0,sourceOffset,length);
	return visitor._match;
	}

bool	ReadOnlyComposite::areEqual(	const ReadOnly&		source,
										unsigned			sourceOffset
										) const noexcept{
	EqualRO	visitor(source);
	visit(visitor,0,sourceOffset,source.length());
	return visitor._match;
	}

