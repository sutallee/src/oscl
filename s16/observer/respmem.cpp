/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::S16::Observer;

Resp::Api::ChangeResp&
	Resp::ChangeMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							int16_t	state
							) noexcept{
	Req::Api::ChangePayload*
		p	=	new(&payload)
				Req::Api::ChangePayload(
							state
							);
	Resp::Api::ChangeResp*
		r	=	new(&resp)
			Resp::Api::ChangeResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::CancelChangeResp&
	Resp::CancelChangeMem::build(	Req::Api::SAP&	sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
						Req::Api::ChangeReq&	requestToCancel
						) noexcept{

	Req::Api::CancelChangePayload*
		p	=	new(&payload)
				Req::Api::CancelChangePayload(	requestToCancel);

	Resp::Api::CancelChangeResp*
		r	=	new(&resp)
			Resp::Api::CancelChangeResp(	sap.getReqApi(),
							respApi,
							clientPapi,
							*p
							);

	return *r;
	}

