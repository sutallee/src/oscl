/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_s16_output_parth_
#define _oscl_s16_output_parth_

#include "oscl/s16/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/s16/control/api.h"

/** */
namespace Oscl {

/** */
namespace S16 {

/** */
namespace Output {

/** */
class Part :
		public Oscl::S16::Observer::Req::Api,
		public Oscl::S16::Control::Api
		{
	private:
		/** */
		Oscl::S16::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::S16::Observer::Req::Api::ChangeReq>	_noteQ;

		/** */
		int16_t										_state;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			int16_t						initialState
			) noexcept;

		/** */
		Oscl::S16::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(int16_t state) noexcept;

	public: // Oscl::S16::Control::Api
		/** */
		void	update(int16_t state) noexcept;

	public:	// Oscl::S16::Observer::Req::Api
		/** */
		void request(Oscl::S16::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::S16::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}

#endif
