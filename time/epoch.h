/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_time_epochh_
#define _oscl_time_epochh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Time {
/** */
namespace Epoch {

/** */
uint32_t	secondsSinceEpoch() noexcept;

/** */
void		secondsSinceEpoch(uint32_t value) noexcept;

/** This class is used to perform date/time conversion
	between epoch relative seconds and date format.
 */
class Date {
	public:
		/** The CEC year of the epoch to which
			all dates are relative.
		 */
		const unsigned	_yearOfEpoch;

		/** The CEC year of the date.
			The valid range is 0 to 65535.
		 */
		unsigned short	_yearOfCEC;

		/** The timezone to which the date
			is relative. The default is GMT.
			The timezone value is in hours.
			The value is subtracted from GMT
			to obtain the local date/time.
			E.g. Eastern Standard Time (EST)
			has a timezone value of 5.
			The valid range is 0 to 23.
		 */
		const unsigned char	_timezone;

		/** The month of the year. The
			valid range is 1-12.
		 */
		unsigned char	_monthOfYear;

		/** The day of the month. The valid
			range varies by month, but the
			maximum range is 1 to 31.
		 */
		unsigned char	_dayOfMonth;

		/** The hour of the day. The valid
			range is 0 to 23.
		 */
		unsigned char	_hourOfDay;

		/** The minute of the hour. The valid
			range is 0 to 59.
		 */
		unsigned char	_minute;

		/** The second of the minute. The valid
			range is 0 to 59.
		 */
		unsigned char	_second;

	public:
		/** This constructor defaults the date fields
			as specified. This constructor is typically
			used when converting from a date in YYYY:MM:DD:HH:MM:SS
			format to seconds-since-epoch format. Note
			that the default epoch is the UNIX 1970, other
			epochs include UTC which is 1900.
		 */
		Date(	unsigned short	yearSinceCEC,
				unsigned char	monthOfYear,
				unsigned char	dayOfMonth,
				unsigned char	hourOfDay,
				unsigned char	minute,
				unsigned char	second,
				unsigned char	timezone = 0,
				unsigned		yearOfEpoch = 1970
				) noexcept;

		/** This constructor only initializes the
			yearOfEpoch field. This constructor is intended
			to be used when one of the update() operations
			is going to be invoked subsequently.
		 */
		Date(	unsigned char	timezone = 0,
				unsigned		yearOfEpoch = 1970
				) noexcept;

		/** Calculate date using specified number of
			seconds since the epoch.
		 */
		void	update(unsigned long secondsSinceEpoch) noexcept;

		/** Calculate date using the current secondsSinceEpoch().
		 */
		void	update() noexcept;

		/** Returns the number of seconds since the epoch.
			The value is calculated using the current state
			of the various data members of this class.
		 */
		unsigned long	dateInSecondsSinceEpoch() noexcept;
		
		/** */
		void	log() noexcept;
	};

}
}
}

#endif

