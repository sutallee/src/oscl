/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_time_cech_
#define _oscl_time_cech_

/* NOTE!:
	This file is under development.
	This file is un-tested.
	This file is volatile and may/will change,
	and may be deleted.
 */

/** */
namespace Oscl {
/** */
namespace Time {

/** */
class CommonEraCalendar {
	public:
		/** Returns the number of leap years since year zero.
			The units are days.
			*/
		static inline short	leapYearsSinceYear(short year) noexcept{
			return year/4 + year/100 - year/400;
			}
		/** Returns true if the year is a leap year.
		 */
		static bool			isLeapYear(short year){
			if((year % 4)!=0) return false;
			if((year % 400)==0) return true;
			if((year % 100)==0) return false;
			return true;
			}
	class Year {
		private:
			short	_year;
		public:
			inline Year(short year) noexcept:_year(year){}
			bool	isLeapYear() noexcept{
				return CommonEraCalendar::isLeapYear(_year);
				}
			long	daysSinceYearZero() noexcept{
				short	leapYears	= leapYearsSinceYear(_year);
				short	daysInYear	= 365*_year;
				if(daysInYear < 0){
					//_overflow	= true;
					}
				if(leapYears){
					}
				return 365*leapYearsSinceYear(_year);
				}
		};
	class MonthOfYear {
		private:
			Year&			_year;
			unsigned char	_monthOfYear;
		public:
			inline MonthOfYear(	Year&			year,
								unsigned char	monthOfYear
								) noexcept:
								_year(year),
								_monthOfYear(monthOfYear)
								{}
		};
	class DayOfMonth {
		private:
			MonthOfYear&	_monthOfYear;
			unsigned char	_dayOfMonth;
		public:
			inline DayOfMonth(	MonthOfYear&	monthOfYear,
								unsigned char	dayOfMonth
								) noexcept:
								_monthOfYear(monthOfYear),
								_dayOfMonth(dayOfMonth)
								{};
		};
	class Date {
		};
	};

};
};

#endif
