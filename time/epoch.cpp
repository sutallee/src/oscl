/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include "oscl/error/info.h"

#include "epoch.h"
#include "oscl/time/time.h"

/** */
namespace Oscl {
/** */
namespace Time {
/** */
namespace Epoch {

/** */
uint32_t	secondsSinceEpoch() noexcept;

/** */
void		secondsSinceEpoch(uint32_t value) noexcept;

/** */
/** */
void		date(	unsigned short&	year,
					unsigned char&	month,
					unsigned char&	day,
					unsigned char&	hour,
					unsigned char&	minute,
					unsigned char&	second
					) noexcept{
	Date	date;
	date.update();
	year	= date._yearOfCEC;
	month	= date._monthOfYear;
	day		= date._dayOfMonth;
	hour	= date._hourOfDay;
	minute	= date._minute;
	second	= date._second;
	}

static void	leapYearDate(	unsigned long	dayOfYear,
							unsigned char&	month,
							unsigned char&	day
							) noexcept{
	if(dayOfYear < firstDayOfFebruary){
		month	= monthOfJanuary;
		day		= dayOfYear;
		return;
		}

	if(dayOfYear < firstDayOfMarchLeapYear){
		month	= monthOfFebruary;
		day		= dayOfYear - firstDayOfFebruary;
		return;
		}

	if(dayOfYear < firstDayOfAprilLeapYear){
		month	= monthOfMarch;
		day		= dayOfYear - firstDayOfMarchLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfMayLeapYear){
		month	= monthOfApril;
		day		= dayOfYear - firstDayOfAprilLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfJuneLeapYear){
		month	= monthOfMay;
		day		= dayOfYear - firstDayOfMayLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfJulyLeapYear){
		month	= monthOfJune;
		day		= dayOfYear - firstDayOfJuneLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfAugustLeapYear){
		month	= monthOfJuly;
		day		= dayOfYear - firstDayOfJulyLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfSeptemberLeapYear){
		month	= monthOfAugust;
		day		= dayOfYear - firstDayOfAugustLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfOctoberLeapYear){
		month	= monthOfSeptember;
		day		= dayOfYear - firstDayOfSeptemberLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfNovemberLeapYear){
		month	= monthOfOctober;
		day		= dayOfYear - firstDayOfOctoberLeapYear;
		return;
		}

	if(dayOfYear < firstDayOfDecemberLeapYear){
		month	= monthOfNovember;
		day		= dayOfYear - firstDayOfNovemberLeapYear;
		return;
		}
	month	= monthOfNovember;
	day		= dayOfYear - firstDayOfDecemberLeapYear;
	}

static void	normalYearDate(	unsigned long	dayOfYear,
							unsigned char&	month,
							unsigned char&	day
							) noexcept{
	if(dayOfYear < firstDayOfFebruary){
		month	= monthOfJanuary;
		day		= dayOfYear;
		return;
		}

	if(dayOfYear < firstDayOfMarch){
		month	= monthOfFebruary;
		day		= dayOfYear - firstDayOfFebruary;
		return;
		}

	if(dayOfYear < firstDayOfApril){
		month	= monthOfMarch;
		day		= dayOfYear - firstDayOfMarch;
		return;
		}

	if(dayOfYear < firstDayOfMay){
		month	= monthOfApril;
		day		= dayOfYear - firstDayOfApril;
		return;
		}

	if(dayOfYear < firstDayOfJune){
		month	= monthOfMay;
		day		= dayOfYear - firstDayOfMay;
		return;
		}

	if(dayOfYear < firstDayOfJuly){
		month	= monthOfJune;
		day		= dayOfYear - firstDayOfJune;
		return;
		}

	if(dayOfYear < firstDayOfAugust){
		month	= monthOfJuly;
		day		= dayOfYear - firstDayOfJuly;
		return;
		}

	if(dayOfYear < firstDayOfSeptember){
		month	= monthOfAugust;
		day		= dayOfYear - firstDayOfAugust;
		return;
		}

	if(dayOfYear < firstDayOfOctober){
		month	= monthOfSeptember;
		day		= dayOfYear - firstDayOfSeptember;
		return;
		}

	if(dayOfYear < firstDayOfNovember){
		month	= monthOfOctober;
		day		= dayOfYear - firstDayOfOctober;
		return;
		}

	if(dayOfYear < firstDayOfDecember){
		month	= monthOfNovember;
		day		= dayOfYear - firstDayOfNovember;
		return;
		}
	month	= monthOfNovember;
	day		= dayOfYear - firstDayOfDecember;
	}

}
}
}

using namespace Oscl::Time;
using namespace Oscl::Time::Epoch;

Date::Date(	unsigned short	yearSinceCEC,
			unsigned char	monthOfYear,
			unsigned char	dayOfMonth,
			unsigned char	hourOfDay,
			unsigned char	minute,
			unsigned char	second,
			unsigned char	timezone,
			unsigned		yearOfEpoch
			) noexcept:
		_yearOfEpoch(yearOfEpoch),
		_yearOfCEC(yearSinceCEC),
		_timezone(timezone),
		_monthOfYear(monthOfYear),
		_dayOfMonth(dayOfMonth),
		_hourOfDay(hourOfDay),
		_minute(minute),
		_second(minute)
		{
	}


Date::Date(	unsigned char	timezone,
			unsigned		yearOfEpoch
			) noexcept:
		_yearOfEpoch(yearOfEpoch),
		_timezone(timezone)
		{
	}

void	Date::update(unsigned long secondsSinceEpoch) noexcept{
	secondsSinceEpoch	-= (_timezone * secondsPerHour);
	const unsigned long	nDaysSinceEpoch	= secondsSinceEpoch / secondsPerDay;
	const unsigned long	secondOfDay		= secondsSinceEpoch % secondsPerDay;

	_second		= (unsigned char)(secondOfDay % 60);
	const unsigned minutesOfDay	= secondOfDay / 60;
	_minute		= (unsigned char)(minutesOfDay % 60);
	const unsigned hourOfDay		= minutesOfDay / 60;
	_hourOfDay		= (unsigned char)(hourOfDay);

	const unsigned long	nYearsRemainderOfEpoch		= _yearOfEpoch % 4;
	const unsigned long	nQuadYearsFromZeroToEpoch	= _yearOfEpoch / 4;
	unsigned long	nDaysFromZeroToEpoch;
	switch(nYearsRemainderOfEpoch){
		case 0:
			nDaysFromZeroToEpoch	= nQuadYearsFromZeroToEpoch * daysPerQuad;
			break;
		case 1:
			nDaysFromZeroToEpoch	= nQuadYearsFromZeroToEpoch * daysPerQuad;
			nDaysFromZeroToEpoch	+= 366;
			break;
		case 2:
			nDaysFromZeroToEpoch	= nQuadYearsFromZeroToEpoch * daysPerQuad;
			nDaysFromZeroToEpoch	+= 366 + 365;
			break;
		case 3:
			nDaysFromZeroToEpoch	= nQuadYearsFromZeroToEpoch * daysPerQuad;
			nDaysFromZeroToEpoch	+= 366 + (2*365);
			break;
		default:
			for(;;);
		};

	const unsigned long	nDaysSinceZero	= nDaysSinceEpoch + nDaysFromZeroToEpoch;
	const unsigned long nQuadYears	= nDaysSinceZero / daysPerQuad;
	const unsigned long nDaysSinceLeapYear	= nDaysSinceZero % daysPerQuad;

	unsigned long	nYearsSinceLeapYear;

	unsigned long	nthDayOfYear;
	if(nDaysSinceLeapYear < 366){
		nYearsSinceLeapYear	= 0;
		nthDayOfYear	= nDaysSinceLeapYear;
		leapYearDate(	nthDayOfYear,
						_monthOfYear,
						_dayOfMonth
						);
		_monthOfYear	+= 1;
		_dayOfMonth		+= 1;
		}
	else {
		nYearsSinceLeapYear	= ((nDaysSinceLeapYear-366) / 365) + 1;
		nthDayOfYear		= (nDaysSinceLeapYear-366) % 365;
		normalYearDate(	nthDayOfYear,
						_monthOfYear,
						_dayOfMonth
						);
		_monthOfYear	+= 1;
		_dayOfMonth		+= 1;
		}

	_yearOfCEC	= nQuadYears*4 + nYearsSinceLeapYear;
	}

void	Date::update() noexcept{
	update(secondsSinceEpoch());
	}

unsigned long	daysSinceZero(unsigned short yearOfCEC) noexcept{
	const unsigned long	nQuadYearsSinceZero				= yearOfCEC / 4;
	const unsigned long	nRemainderYearsSinceZero		= yearOfCEC % 4;
	unsigned long	remainderDaysSinceZero;
	switch(nRemainderYearsSinceZero){
		case 0:
			remainderDaysSinceZero	= 0;
			break;
		case 1:
			remainderDaysSinceZero	= 366;
			break;
		case 2:
			remainderDaysSinceZero	= 366+365;
			break;
		case 3:
		default:
			remainderDaysSinceZero	= 366+(2*365);
			break;
		}
	return (nQuadYearsSinceZero*daysPerQuad) + remainderDaysSinceZero;
	}

unsigned long	normalDaysSinceFirstOfYear(unsigned char month) noexcept{
	month	-= 1;
	switch(month){
		case monthOfJanuary:
			return firstDayOfJanuary;
		case monthOfFebruary:
			return firstDayOfFebruary;
		case monthOfMarch:
			return firstDayOfMarch;
		case monthOfApril:
			return firstDayOfApril;
		case monthOfMay:
			return firstDayOfMay;
		case monthOfJune:
			return firstDayOfJune;
		case monthOfJuly:
			return firstDayOfJuly;
		case monthOfAugust:
			return firstDayOfAugust;
		case monthOfSeptember:
			return firstDayOfSeptember;
		case monthOfOctober:
			return firstDayOfOctober;
		case monthOfNovember:
			return firstDayOfNovember;
		case monthOfDecember:
		default:
			return firstDayOfDecember;
		}
	}

unsigned long	leapYearDaysSinceFirstOfYear(unsigned char month) noexcept{
	month	-= 1;
	switch(month){
		case monthOfJanuary:
			return firstDayOfJanuary;
		case monthOfFebruary:
			return firstDayOfFebruary;
		case monthOfMarch:
			return firstDayOfMarchLeapYear;
		case monthOfApril:
			return firstDayOfAprilLeapYear;
		case monthOfMay:
			return firstDayOfMayLeapYear;
		case monthOfJune:
			return firstDayOfJuneLeapYear;
		case monthOfJuly:
			return firstDayOfJulyLeapYear;
		case monthOfAugust:
			return firstDayOfAugustLeapYear;
		case monthOfSeptember:
			return firstDayOfSeptemberLeapYear;
		case monthOfOctober:
			return firstDayOfOctoberLeapYear;
		case monthOfNovember:
			return firstDayOfNovemberLeapYear;
		case monthOfDecember:
		default:
			return firstDayOfDecemberLeapYear;
		}
	}

unsigned long	Date::dateInSecondsSinceEpoch() noexcept{
	const unsigned long	thisYearDaysSinceZero		= daysSinceZero(_yearOfCEC);
	const unsigned long	epochYearDaysSinceZero		= daysSinceZero(_yearOfEpoch);
	const unsigned long	daysSinceEpoch				= thisYearDaysSinceZero-epochYearDaysSinceZero;


	unsigned long	daysSinceFirstOfYear	= (_dayOfMonth-1);
	if(_yearOfCEC % 4){
		daysSinceFirstOfYear	+= normalDaysSinceFirstOfYear(_monthOfYear);
		}
	else {
		daysSinceFirstOfYear	+= leapYearDaysSinceFirstOfYear(_monthOfYear);
		}

	unsigned long
	secsSinceEpoch	= (daysSinceEpoch + daysSinceFirstOfYear)*secondsPerDay;
	secsSinceEpoch	+= _hourOfDay*60*60;
	secsSinceEpoch	+= _minute*60;
	secsSinceEpoch	+= _second;
	secsSinceEpoch	+= (_timezone * secondsPerHour);
	return secsSinceEpoch;
	}

void	Date::log() noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"%4.4u-%2.2u-%2.2u-%2.2u:%2.2u:%2.2u\r\n",
				_yearOfCEC,
				(unsigned)_monthOfYear,
				(unsigned)_dayOfMonth,
				(unsigned)_hourOfDay,
				(unsigned)_minute,
				(unsigned)_second
				);
	Oscl::Error::Info::log(buffer);
	}

