/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_time_timeh_
#define _oscl_time_timeh_

/** */
namespace Oscl {
/** */
namespace Time {

/** */
enum{ secondsPerHour			= (60*60)};
/** */
enum{ secondsPerDay				= (24*60*60)};
/** */
enum{ daysPerQuad				= ((365*3)+366)};
/** */
enum{ secondsPerQuadYear		= daysPerQuad*secondsPerDay};
/** */
enum{ daysInJanuary				= 31};
/** */
enum{ daysInFebruary			= 28};
/** */
enum{ daysInFebruaryLeapYear	= 29};
/** */
enum{ daysInMarch				= 31};
/** */
enum{ daysInApril				= 30};
/** */
enum{ daysInMay					= 31};
/** */
enum{ daysInJune				= 30};
/** */
enum{ daysInJuly				= 31};
/** */
enum{ daysInAugust				= 31};
/** */
enum{ daysInSeptember			= 30};
/** */
enum{ daysInOctober				= 31};
/** */
enum{ daysInNovember			= 30};
/** */
enum{ daysInDecember			= 31};

/** */
enum{
	/** */
	firstDayOfJanuary			= 0,
	/** */
	firstDayOfFebruary			= daysInJanuary,
	/** */
	firstDayOfMarchLeapYear		= firstDayOfFebruary+daysInFebruaryLeapYear,
	/** */
	firstDayOfMarch				= firstDayOfFebruary+daysInFebruary,
	/** */
	firstDayOfAprilLeapYear		= firstDayOfMarchLeapYear+daysInMarch,
	/** */
	firstDayOfApril				= firstDayOfMarch+daysInMarch,
	/** */
	firstDayOfMayLeapYear		= firstDayOfAprilLeapYear+daysInApril,
	/** */
	firstDayOfMay				= firstDayOfApril+daysInApril,
	/** */
	firstDayOfJuneLeapYear		= firstDayOfMayLeapYear+daysInMay,
	/** */
	firstDayOfJune				= firstDayOfMay+daysInMay,
	/** */
	firstDayOfJulyLeapYear		= firstDayOfJuneLeapYear+daysInJune,
	/** */
	firstDayOfJuly				= firstDayOfJune+daysInJune,
	/** */
	firstDayOfAugustLeapYear	= firstDayOfJulyLeapYear+daysInJuly,
	/** */
	firstDayOfAugust			= firstDayOfJuly+daysInJuly,
	/** */
	firstDayOfSeptemberLeapYear	= firstDayOfAugustLeapYear+daysInAugust,
	/** */
	firstDayOfSeptember			= firstDayOfAugust+daysInAugust,
	/** */
	firstDayOfOctoberLeapYear	= firstDayOfSeptemberLeapYear+daysInSeptember,
	/** */
	firstDayOfOctober			= firstDayOfSeptember+daysInSeptember,
	/** */
	firstDayOfNovemberLeapYear	= firstDayOfOctoberLeapYear+daysInOctober,
	/** */
	firstDayOfNovember			= firstDayOfOctober+daysInOctober,
	/** */
	firstDayOfDecemberLeapYear	= firstDayOfNovemberLeapYear+daysInNovember,
	/** */
	firstDayOfDecember			= firstDayOfNovember+daysInNovember
	};

/** */
enum{
	/** */
	monthOfJanuary		= 0,
	/** */
	monthOfFebruary		= 1,
	/** */
	monthOfMarch		= 2,
	/** */
	monthOfApril		= 3,
	/** */
	monthOfMay			= 4,
	/** */
	monthOfJune			= 5,
	/** */
	monthOfJuly			= 6,
	/** */
	monthOfAugust		= 7,
	/** */
	monthOfSeptember	= 8,
	/** */
	monthOfOctober		= 9,
	/** */
	monthOfNovember		= 10,
	/** */
	monthOfDecember		= 11
	};

/** */
enum {
	/** */
	GMT		= 0,
	/** */
	EST		= 5,
	/** */
	CST		= 6,
	/** */
	MST		= 7,
	/** */
	PST		= 8
	};

}
}

#endif
