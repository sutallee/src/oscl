/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_time_yearsh_
#define _oscl_time_yearsh_

/* NOTE!:
	This file is under development.
	This file is un-tested.
	This file is volatile and may/will change,
	and may be deleted.
 */

/** */
namespace Oscl {
/** */
namespace Time {

/** */
class Years {
	private:
		short	_year;
		bool	_overflow;
		static const unsigned short	_msbMask		= ~((~0L)>>1);
		static const short			_largestSigned	= (~_msbMask);
	public:
		inline Years() noexcept:
				_year(0),
				_overflow(false)
				{
			}
		inline Years(const Years& s) noexcept{
			if((_overflow = s._overflow)) return;
			_year	= s._year;
			}
		inline Years(const short s) noexcept:
				_overflow(false)
				{
			_year	= s;
			}
		inline Years(const int s) noexcept:
				_overflow(false)
				{
			_year	= s;
			}
		inline short	getShort() noexcept { return _year; }
		inline bool	overflow() noexcept {return _overflow;}
		inline Years&	operator = (const Years& s) noexcept{
			if(!(_overflow = s._overflow)){
				_year	= s._year;
				}
			return *this;
			}
		inline Years&	operator = (short s) noexcept{
			_year	= s;
			return *this;
			}
		inline Years&	operator = (int s) noexcept{
			_year	= s;
			return *this;
			}
		Years&	operator += (const Years& s) noexcept;
		Years&	operator -= (const Years& s) noexcept;
	};

inline Years&	operator + (const Years& a, const Years& b) noexcept{
	Years	result(a);
	return result += b;
	}

inline Years&	operator - (const Years& a, const Years& b) noexcept{
	Years	result(a);
	return result -= b;
	}

};
};

#endif
