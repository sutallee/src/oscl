#include "part.h"

using namespace Oscl::XYZ::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi
	) noexcept:
		_sap(
			*this,
			papi
			)
		{
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify(const Oscl::XYZ::Vector& state) noexcept{
	Oscl::XYZ::Observer::Req::Api::ChangeReq* note;

	while((note=_noteQ.get())){
		note->getPayload()._vector	= state;
		note->returnToSender();
		}
	}

void Part::request(Oscl::XYZ::Observer::Req::Api::ChangeReq& msg) noexcept{
	_noteQ.put(&msg);
	}

void Part::request(Oscl::XYZ::Observer::Req::Api::CancelChangeReq& msg) noexcept{

	Oscl::XYZ::Observer::Req::Api::ChangeReq&
	requestToCancel	= msg.getPayload()._requestToCancel;

	if(_noteQ.remove(&requestToCancel)){
		requestToCancel.returnToSender();
		}

	msg.returnToSender();
	}

void	Part::update(const Oscl::XYZ::Vector& state) noexcept{
	notify(state);
	}

