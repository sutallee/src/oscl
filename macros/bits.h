/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_macros_bitsh_
#define _oscl_macros_bitsh_

#define OsclMacroHexToBcd(hex)			( ( (hex/10)<<4 ) + (hex%10) )
#define OsclMacroBcdToHex(bcd)			( ( ( (hex>>4) & 0xF0 ) ) * 10) + (hex & ~0xF0))

#define OsclMacroBitMask(bitnum)									(1<<(bitnum))
#define OsclMacroShiftNormalize(bitnum)							(bitnum)
#define OsclMacroValueMask(value,lobitnum)						((value)<<(lobitnum))
#define OsclMacroBigEndianBitMask(size,bitnum)						(1<<(((size)-1)-(bitnum)))
#define OsclMacroBigEndianValueMask(size,value,lobitnum,hibitnum)	((value)<<((size)-(hibitnum)-1))
#define OsclMacroFieldMask(nbits)								((1UL<<(nbits))-1)
#define OsclMacroBitFieldMask(nbits,lsbnum)						(OsclMacroFieldMask(nbits)<<(lsbnum))
#define OsclMacroBigEndianMaxFieldValue(size,lobitnum,hibitnum)	(OsclMacroFieldMask((hibitnum)-(lobitnum)+1))

#if 0
#define OsclMacroBigEndianBitFieldMask(size,lobitnum,hibitnum)	(OsclMacroFieldMask((hibitnum)-(lobitnum)+1)<<((size)-(hibitnum)-1))
#endif
#define OsclMacroBigEndianBitFieldMask(size,lobitnum,hibitnum) (OsclMacroBigEndianMaxFieldValue((size),(lobitnum),(hibitnum))<<((size)-(hibitnum)-1))

#define OsclMacroBigEndianShift(size,hibitnum)						((size)-(hibitnum)-1)

#define OsclMacroSignExtension16(value)							(~((((value&0x008000)>>15)-1)|0x0000FFFF))
#define OsclMacroSignExtend16(value)								((value & 0x0000FFFF)|OsclMacroSignExtension16(value))

#define OsclMacroLower16(value)									(OsclMacroSignExtend16(value))
#define OsclMacroUpper16of32(value)								(OsclMacroLower16((value)>>16))

#define OsclMacroSetBit(var,bitnum)				var |= OsclMacroBitMask(bitnum)
#define OsclMacroClearBit(var,bitnum)				var &= ~OsclMacroBitMask(bitnum)
#define OsclMacroTestBit(var,bitnum)				(var & OsclMacroBitMask(bitnum))

#endif
