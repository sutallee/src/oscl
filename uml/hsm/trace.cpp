/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include "trace.h"
#include "namespace.h"
#include "state.h"
#include "target.h"
#include "event.h"
#include "choices.h"
#include "choice.h"
#include "actions.h"
#include "action.h"
#include "reset.h"
#include "entry.h"

using namespace Oscl::UML::HSM;

bool	Trace::pre( const Reset& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Reset& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Namespace& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Namespace& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const State& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const State& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Target& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Target& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Event& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Event& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Choices& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Choices& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Choice& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Choice& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Actions& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Actions& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Action& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Action& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

bool	Trace::pre( const Entry& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}{\n";
	return true;
	}

bool	Trace::post( const Entry& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n\t}\n}\n";
	return true;
	}

