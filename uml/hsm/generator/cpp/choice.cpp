/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include "choice.h"

using namespace Oscl::UML::HSM::Generator;

Choice::Choice(
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	parameters,
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters,
		void*				context
		):
	Action(
		parameters,
		context,
		globalParameters
		),
	_currentGuard( nullptr ),
	_currentGuardExp( nullptr ),
	_currentGroup( nullptr ),
	_currentVarFunc( nullptr ),
	_currentVarExp( nullptr ),
	_currentBinOperand( nullptr ),
	_currentChoice( nullptr ),
	_result( nullptr )
	{
	pushChoicePopulateResult();
	}

Choice::~Choice() {
	popChoicePopulate();
	}

void	Choice::populateResult( ChoiceRecord* r ) {
	if( _result ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_result is NOT nullptr\n"
			;
		}
	_result	= r;
	}

void	Choice::populateChoice( ActionsRecord* r ) {
	if( ! _currentChoice ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_currentChoice is nullptr\n"
			;
		}
	_currentChoice->_actions	= r;
	}

void	Choice::populateBinOperand( ContextFuncRecord* r ) {
	if( !_currentBinOperand ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_currentBinOperand is nullptr\n"
			;
		}
	_currentBinOperand->_contextFunc	= r;
	}

void	Choice::pushChoicePopulateResult() {

	pushChoicePopulate(
		new PopulateComposer< Choice, ChoiceRecord >(
				*this,
				&Choice::populateResult
				)
		);
	}

void	Choice::pushActionsPopulateChoice() {

	pushActionsPopulate(
		new PopulateComposer< Choice, ActionsRecord >(
				*this,
				&Choice::populateChoice
				)
		);
	}

void	Choice::pushContextFuncPopulateBinOperand() {

	pushContextFuncPopulate(
		new PopulateComposer< Choice, ContextFuncRecord >(
			*this,
			&Choice::populateBinOperand
			)
		);
	}

void	Choice::populateBinOperand( ArgRecord* r ) {
	_currentBinOperand->_arg	= r;
	}

void	Choice::pushArgPopulateBinOperand() {

	pushArgPopulate(
		new PopulateComposer< Choice, ArgRecord >(
			*this,
			&Choice::populateBinOperand
			)
		);
	}

void	Choice::populateBinOperand( VarExpRecord* r ) {
	_currentBinOperand->_varExp	= r;
	}

void	Choice::pushVarExpPopulateBinOperand() {

	pushVarExpPopulate(
		new PopulateComposer< Choice, VarExpRecord >(
			*this,
			&Choice::populateBinOperand
			)
		);
	}

void	Choice::populateChoice( GuardRecord* r ) {
	_currentChoice->_guard	= r;
	}

void	Choice::pushGuardPopulateChoice() {

	pushGuardPopulate(
		new PopulateComposer< Choice, GuardRecord >(
				*this,
				&Choice::populateChoice
				)
		);
	}

void	Choice::populateVarFunc( ArgsRecord* r ) {
	_currentVarFunc->_args	= r;
	}

void	Choice::pushArgsPopulateVarFunc() {
	pushArgsPopulate(
		new PopulateComposer< Choice, ArgsRecord >(
				*this,
				&Choice::populateVarFunc
				)
		);
	}

bool	Choice::pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept {
	/*	A BinOp can appear in:
			GuardExpr
	 */

	auto op	= new BinOperatorRecord( node._binaryoperator );

	_currentGuardExp->_operator.push_back( op );

	_endState._binOpList.put( op );

	return true;
	};
bool	Choice::post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept {
	// Nothing to do here
	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept {

	if( _currentBinOperand ) {
		_stacks._binOperandStack.push( _currentBinOperand );
		}

	_currentBinOperand	= new BinOperandRecord();

	_currentGuardExp->_operand.push_back( _currentBinOperand );

	pushContextFuncPopulateBinOperand();
	pushArgPopulateBinOperand();
	pushVarExpPopulateBinOperand();

	return true;
	}

bool	Choice::post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept {

	popVarExpPopulate();
	popArgPopulate();
	popContextFuncPopulate();

	_endState._binOperandList.put(_currentBinOperand );

	_currentBinOperand	= _stacks._binOperandStack.pop();

	return true;

	}

bool	Choice::pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept {

	if( _currentVarFunc ) {
		_stacks._varFuncStack.push( _currentVarFunc );
		}

	bool
	success	= isValidArgument( node._vars[0] );

	if( !success ) {
		_description	= "Unable to resolve argument.";
		_position		= node._position;
		return false;
		}

	_currentVarFunc	= new VarFuncRecord( node._vars, node._refs);

	if( _currentArgument ) {
		_currentArgument->_expression +="(";
		}

	_currentVarFuncPopulate->populate( _currentVarFunc );

	pushArgsPopulateVarFunc();

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept {

	popArgsPopulate();

	_endState._varFuncList.put( _currentVarFunc );

	_currentVarFunc	= _stacks._varFuncStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {

	if( _currentVarExp ) {
		_stacks._varExpStack.push( _currentVarExp );
		}

	bool
	success	= isValidArgument( node._vars[0] );

	if( !success ) {
		_description	= "Unable to resolve argument.";
		_position		= node._position;
		return false;
		}

	_currentVarExp	= new VarExpRecord(
							node._vars,
							node._refs
							);

	/*	A VarExp can be contained by:
			Arg
			BinOperand
			Action
	 */

	if( _currentGuard ) {
		_currentGuard->_vars.push_back( node._vars[0] );
		}
	else {
		_description	= __PRETTY_FUNCTION__;
		_position		= node._position;
		return false;
		}

	_currentVarExpPopulate->populate( _currentVarExp );

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {

	if( _currentArgument ) {
		}

	_endState._varExpList.put( _currentVarExp );

	_currentVarExp	= _stacks._varExpStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept {

	if( !_currentGuard ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": Warning: _currentGuard is null.\n"
			;
		_description	= "Unexpected GuardExpr without _currentGuard.";
		_position		= node._position;
		return false;
		}

	if( _currentGuardExp ) {
		_stacks._guardExpStack.push( _currentGuardExp );
		}

	_currentGuardExp	= new GuardExpRecord();

	/* Groups have priority over the guard */
	if( _currentGroup ) {
		_currentGroup->set( _currentGuardExp );
		}
	else {
		_currentGuard->_guardExp	= _currentGuardExp;
		}

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept {

	_endState._guardExpList.put( _currentGuardExp );

	_currentGuardExp	= _stacks._guardExpStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept {

	if( !_currentGuardExp ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": Warning: _currentGuardExpr is null.\n"
			;
		_description	= "Unexpected Group without _currentGuardExpr.";
		_position		= node._position;
		return false;
		}

	if( _currentGroup ) {
		_stacks._groupStack.push( _currentGroup );
		}

	_currentGroup	= new GroupRecord();

	_currentGuardExp->_operand.push_back( _currentGroup );

//	_currentGuard->_guardExp	= _currentGuardExp;

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::Group& node ) noexcept {

	_endState._groupList.put( _currentGroup );

	_currentGroup	= _stacks._groupStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept {

	if( _currentGuard ) {
		_stacks._guardStack.push( _currentGuard );
		}

	_currentGuard	= new GuardRecord();

	_currentChoice->_guard	= _currentGuard;

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept {

	_endState._guardList.put( _currentGuard );

	_currentGuard	= _stacks._guardStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept {

	if( _currentChoice ) {
		_stacks._choiceStack.push( _currentChoice );
		}

	_currentChoice	= new ChoiceRecord( _context );

	_currentChoicePopulate->populate( _currentChoice );

	pushActionsPopulateChoice();

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept {

	popActionsPopulate();

	_endState._choiceList.put( _currentChoice );

	_currentChoice	= _stacks._choiceStack.pop();

	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept {

	auto literal = new LiteralRecord();

	literal->_literal	+="\"";
	literal->_literal	+= node._name;
	literal->_literal	+="\"";

	_currentBinOperand->_literal	= literal;

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept {
	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept {

	auto literal = new LiteralRecord();

	literal->_literal	+="\'";
	literal->_literal	+= node._name;
	literal->_literal	+="\'";

	_currentBinOperand->_literal	= literal;

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept {
	return true;
	};

bool	Choice::pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept {

	auto literal = new LiteralRecord();

	literal->_literal	+=" ";
	literal->_literal	+= node._name;
	literal->_literal	+=" ";

	_currentBinOperand->_literal	= literal;

	return true;
	};

bool	Choice::post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept {
	return true;
	};

