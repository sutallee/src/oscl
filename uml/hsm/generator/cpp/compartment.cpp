/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include "compartment.h"

using namespace Oscl::UML::HSM::Generator;

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

std::ostream& operator<<( std::ostream& os, const Compartment& t ){


	for(
		EventRecord*	e = t._endState._eventList.first();
		e;
		e	= t._endState._eventList.next( e )
		) {
		TriggerRecord*
		r	= e->_trigger;

		if( !r ) {
			continue;
			}
		os
			<<	r->_name
			<< "("
			;

		auto parameters	= r->_parameters;

		if( ! parameters ) {
			continue;
			}

		for( unsigned i=0; i < parameters->_parameters.size(); ++i ) {
			if( i != 0 ) {
				os
					<< ","
					;
				}

			for( unsigned j=0; j < parameters->_parameters[i]._type._qualifiers.size(); ++j ) {

				if( j != 0 ) {
					os
						<< " "
						;
					}
				os
					<< r->_parameters->_parameters[i]._type._qualifiers[j]
					;

				}

			for( unsigned j=0; j < r->_parameters->_parameters[i]._type._path.size(); ++j ) {

				if( j != 0 ) {
					os
						<< "::"
						;
					}
				os
					<< r->_parameters->_parameters[i]._type._path[j]
					;

				}

			for( unsigned j=0; j < r->_parameters->_parameters[i]._type._operators.size(); ++j ) {

				os
					<< r->_parameters->_parameters[i]._type._operators[j]
					;

				}

			os
				<< " "
				;

			os
				<<	r->_parameters->_parameters[i]._name
				;
			}
		os
			<< ")"
			<< "\n"
			;

		}

	return os;
	}

}
}
}
}

Compartment::Compartment(
		void*	context,
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters
		):
	Event(
		globalParameters,
		context
		),
	_globalParameters( globalParameters ),
	_currentEntry( nullptr ),
	_currentExit( nullptr ),
	_currentEvents( nullptr ),
	_events( nullptr ),
	_entry( nullptr ),
	_exit( nullptr )
	{
	pushEventsPopulateResult();
	}

Compartment::~Compartment() {
	popEventsPopulate();
	}

void	Compartment::populateResult( EventsRecord* r ) {

	if( _events ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _events is NOT a nullptr\n"
			;
		}

	_events	= _currentEvents;
	}

void	Compartment::pushEventsPopulateResult() {

	pushEventsPopulate(
		new PopulateComposer< Compartment, EventsRecord >(
				*this,
				&Compartment::populateResult
				)
		);
	}

void	Compartment::populateEntry( ActionsRecord* r ) {
	_currentEntry->_actions	= r;
	}

void	Compartment::pushActionsPopulateEntry() {
	pushActionsPopulate(
		new PopulateComposer< Compartment, ActionsRecord >(
				*this,
				&Compartment::populateEntry
				)
		);
	}

void	Compartment::populateExit( ActionsRecord* r ) {
	_currentExit->_actions	= r;
	}

void	Compartment::pushActionsPopulateExit() {
	pushActionsPopulate(
		new PopulateComposer< Compartment, ActionsRecord >(
				*this,
				&Compartment::populateExit
				)
		);
	}

void	Compartment::populateEvents( EventRecord* r ) {

	if( !_currentEvents ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvents is a nullptr\n"
			;
		}
	_currentEvents->_events.push_back( r );
	}

void	Compartment::pushEventPopulateEvents() {

	pushEventPopulate(
		new PopulateComposer< Compartment, EventRecord >(
				*this,
				&Compartment::populateEvents
				)
		);
	}

bool	Compartment::pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept {

	if( _currentEvents ) {
		_stacks._eventsStack.push( _currentEvents );
		}

	_currentEvents	= new EventsRecord();

	pushEventPopulateEvents();

	_currentEventsPopulate->populate( _currentEvents );

	return true;
	};

bool	Compartment::post( const Oscl::UML::Parser::Transition::Events& node ) noexcept {

	_endState._eventsList.put( _currentEvents );

	_events	= _currentEvents;

	_currentEvents	= _stacks._eventsStack.pop();

	popEventPopulate();

	return true;
	};

bool	Compartment::pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept {

	return true;
	};

bool	Compartment::post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept {

	return true;
	};

bool	Compartment::pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept {

	if( _currentEntry ) {
		_stacks._entryStack.push( _currentEntry );
		}

	_currentEntry	= new EntryRecord( _context );

	pushActionsPopulateEntry();

	return true;
	};

bool	Compartment::post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept {

	popActionsPopulate();

	_endState._entryList.put( _currentEntry );

	_entry	= _currentEntry;

	_currentEntry	= _stacks._entryStack.pop();

	return true;
	};

bool	Compartment::pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept {

	if( _currentExit ) {
		_stacks._exitStack.push( _currentExit );
		}

	_currentExit	= new ExitRecord( _context );

	pushActionsPopulateExit();

	return true;
	};

bool	Compartment::post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept {

	popActionsPopulate();

	_endState._exitList.put( _currentExit );

	_exit	= _currentExit;

	_currentExit	= _stacks._exitStack.pop();

	return true;
	};

