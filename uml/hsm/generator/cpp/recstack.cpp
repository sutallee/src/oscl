/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "recstack.h"

using namespace Oscl::UML::HSM::Generator;

RecordStack::~RecordStack() {

	{
		EventRecord*	r;
		while( ( r = _eventStack.get() ) ) {
			delete r;
			}
	}

	{
		EventsRecord*	r;
		while( ( r = _eventsStack.get() ) ) {
			delete r;
			}
	}

	{
		ContextFuncRecord*	r;
		while( ( r = _contextFuncStack.get() ) ) {
			delete r;
			}
	}

	{
		ContextArgsRecord*	r;
		while( ( r = _contextArgsStack.get() ) ) {
			delete r;
			}
	}

	{
		GuardRecord*	r;
		while( ( r = _guardStack.get() ) ) {
			delete r;
			}
	}

	{
		GuardExpRecord*	r;
		while( ( r = _guardExpStack.get() ) ) {
			delete r;
			}
	}

	{
		GroupRecord*	r;
		while( ( r = _groupStack.get() ) ) {
			delete r;
			}
	}

	{
		ActionsRecord*	r;
		while( ( r = _actionsStack.get() ) ) {
			delete r;
			}
	}

	{
		ActionRecord*	r;
		while( ( r = _actionStack.get() ) ) {
			delete r;
			}
	}

	{
		VarFuncRecord*	r;
		while( ( r = _varFuncStack.get() ) ) {
			delete r;
			}
	}

	{
		ArgsRecord*	r;
		while( ( r = _argumentsStack.get() ) ) {
			delete r;
			}
	}

	{
		ArgRecord*	r;
		while( ( r = _argumentStack.get() ) ) {
			delete r;
			}
	}

	{
		VarExpRecord*	r;
		while( ( r = _varExpStack.get() ) ) {
			delete r;
			}
	}

	{
		BinOperandRecord*	r;
		while( ( r = _binOperandStack.get() ) ) {
			delete r;
			}
	}

	{
		TriggerRecord*	r;
		while( ( r = _triggerStack.get() ) ) {
			delete r;
			}
	}

	{
		EntryRecord*	r;
		while( ( r = _entryStack.get() ) ) {
			delete r;
			}
	}

	{
		ExitRecord*	r;
		while( ( r = _exitStack.get() ) ) {
			delete r;
			}
	}

	{
		ArgsRecord*	r;
		while( ( r = _argumentsStack.get() ) ) {
			delete r;
			}
	}

	{
		ArgRecord*	r;
		while( ( r = _argumentStack.get() ) ) {
			delete r;
			}
	}

	{
		VarExpRecord*	r;
		while( ( r = _varExpStack.get() ) ) {
			delete r;
			}
	}

	{
		NamespaceRecord*	r;
		while( ( r = _namespaceStack.get() ) ) {
			delete r;
			}
	}

	{
		IncludeFileRecord*	r;
		while( ( r = _includeFileStack.get() ) ) {
			delete r;
			}
	}

	{
		TransitionRecord*	r;
		while( ( r = _transitionStack.get() ) ) {
			delete r;
			}
	}

	{
		StateRecord*	r;
		while( ( r = _stateStack.get() ) ) {
			delete r;
			}
	}

	{
		ChoicesRecord*	r;
		while( ( r = _choicesStack.get() ) ) {
			delete r;
			}
	}

	{
		TargetRecord*	r;
		while( ( r = _targetStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ActionsRecord >*	r;
		while( ( r = _actionsPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ContextFuncRecord >*	r;
		while( ( r = _contextFuncPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ChoiceRecord >*	r;
		while( ( r = _choicePopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< GuardRecord >*	r;
		while( ( r = _guardPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< EventRecord >*	r;
		while( ( r = _eventPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< EventsRecord >*	r;
		while( ( r = _eventsPopStack.get() ) ) {
			delete r;
			}
	}

	{
		ParametersRecord*	r;
		while( ( r = _parametersStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ParametersRecord >*	r;
		while( ( r = _parametersPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< EntryRecord >*	r;
		while( ( r = _entryPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< VarExpRecord >*	r;
		while( ( r = _varExpPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ArgRecord >*	r;
		while( ( r = _argPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< VarFuncRecord >*	r;
		while( ( r = _varFuncPopStack.get() ) ) {
			delete r;
			}
	}

	{
		Populate< ArgsRecord >*	r;
		while( ( r = _argsPopStack.get() ) ) {
			delete r;
			}
	}

	}

