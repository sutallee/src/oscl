/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include "event.h"

using namespace Oscl::UML::HSM::Generator;

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

std::ostream& operator<<( std::ostream& os, const Event& t ){

	for(
		EventRecord*	e = t._endState._eventList.first();
		e;
		e	= t._endState._eventList.next( e )
		) {
		TriggerRecord*
		trigger	= e->_trigger;

		if( !trigger ) {
			continue;
			}
		os
			<<	trigger->_name
			<< "("
			;

		ParametersRecord*	parameters	= trigger->_parameters;

		if( !parameters ) {
			continue;
			}

		for( unsigned i=0; i < parameters->_parameters.size(); ++i ) {
			if( i != 0 ) {
				os
					<< ","
					;
				}

			for( unsigned j=0; j < parameters->_parameters[i]._type._qualifiers.size(); ++j ) {

				if( j != 0 ) {
					os
						<< " "
						;
					}
				os
					<< parameters->_parameters[i]._type._qualifiers[j]
					;

				}

			for( unsigned j=0; j < parameters->_parameters[i]._type._path.size(); ++j ) {

				if( j != 0 ) {
					os
						<< "::"
						;
					}
				os
					<< parameters->_parameters[i]._type._path[j]
					;

				}

			for( unsigned j=0; j < parameters->_parameters[i]._type._operators.size(); ++j ) {

				os
					<< parameters->_parameters[i]._type._operators[j]
					;

				}

			os
				<< " "
				;

			os
				<<	parameters->_parameters[i]._name
				;
			}
		os
			<< ")"
			<< "\n"
			;

		}

	return os;
	}

}
}
}
}

Event::Event(
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters,
		void*	context
		):
	Choice(
		nullptr,	// parameters
		globalParameters,
		context
		),
	_currentEvent( nullptr ),
	_currentTrigger( nullptr ),
	_currentParameters( nullptr ),
	_result( nullptr )
	{
	pushEventPopulateResult();
	}

Event::~Event() {
	popEventPopulate();
	}

bool	Event::isValidArgument(
			const std::string&	arg
			) const {

	Oscl::UML::HSM::Generator::TriggerRecord*
	trigger	= nullptr;

	if( _currentEvent ) {
		trigger	= _currentEvent->_trigger;
		}

	if( trigger ) {
		for( unsigned i=0; i < trigger->_parameters->_parameters.size(); ++i ) {

			if( trigger->_parameters->_parameters[i]._name == arg ) {
				return true;
				}
			}
		}

	if( Choice::isValidArgument( arg ) ) {
		return true;
		}

	return false;
	}

bool	Event::resolveType(
			const std::string&						arg,
			Oscl::UML::Parser::Types::Parameter&	parameter
			) const {

	Oscl::UML::HSM::Generator::TriggerRecord*
	trigger	= nullptr;

	if( _currentEvent ) {
		trigger	= _currentEvent->_trigger;
		}

	if( trigger ) {
		for( unsigned i=0; i < trigger->_parameters->_parameters.size(); ++i ) {

			if( trigger->_parameters->_parameters[i]._name == arg ) {
				parameter	= trigger->_parameters->_parameters[i];
				return true;
				}
			}
		}

	return Choice::resolveType(
				arg,
				parameter
				);
	}

bool	Event::pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {

	if( _currentVarExp ) {
		_stacks._varExpStack.push( _currentVarExp );
		}

	bool
	success	= isValidArgument( node._vars[0] );

	if( !success ) {
		_description	= "Unable to resolve argument.";
		_position		= node._position;
		return false;
		}

	_currentVarExp	= new VarExpRecord(
							node._vars,
							node._refs
							);

	/*	A VarExp can be contained by:
			Arg
			BinOperand
			Action
	 */

	if( _currentArgument ) {
		_currentArgument->_varExp	= _currentVarExp;
		}
	else if( _currentBinOperand ) {
		_currentBinOperand->_varExp	= _currentVarExp;
		}
	else if( _currentAction ) {
		_currentAction->_varExp	= _currentVarExp;
		}
	else {
		_description	= __PRETTY_FUNCTION__;
		_position		= node._position;
		return false;
		}

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {

	_endState._varExpList.put( _currentVarExp );

	_currentVarExp	= _stacks._varExpStack.pop();

	return true;
	};

/*	Apparently, the compiler cannot resolve overloaded
	superclass calls. Therefore, since we want to be
	able call the superclass of this supperclass from
	its subclass, we need to do it ourselves here. :-/
 */
bool	Event::pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept {
	return Choice::pre( node );
	}

bool	Event::pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept {

	if( !_currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_currentEvent is null\n"
			;
		_description	= "Unexpected Trigger without _currentEvent.";
		_position		= node._position;
		return false;
		}

	if( _currentTrigger ) {
		_stacks._triggerStack.push( _currentTrigger );
		}

	TriggerRecord*
	t	= new TriggerRecord(
			node._name,
			_currentEvent->_context,
			node._position
			);

	_currentTrigger		= t;

	_currentEvent->_trigger	= t;

	pushParametersPopulateTrigger();

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept {

	popParametersPopulate();

	if( !_currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_currentEvent is null\n"
			;
		_description	= "Unexpected Trigger without _currentEvent.";
		_position		= node._position;
		return false;
		}

	_currentEvent->_trigger	= _currentTrigger;

	_endState._triggerList.put( _currentTrigger );

	_currentTrigger	= _stacks._triggerStack.pop();

	return true;
	};

void	Event::populateResult( EventRecord* r ) {

	if( _result ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _result is NOT nullptr\n"
			;
		}
	_result	= r;
	}

void	Event::populateEvent( ActionsRecord* r ) {

	if( ! _currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvent is nullptr\n"
			;
		}

	_currentEvent->set( r );
	}

void	Event::populateEvent( ChoiceRecord* r ) {

	if( ! _currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvent is nullptr\n"
			;
		}

	_currentEvent->_choice->_choices.push_back( r );
	}

void	Event::populateTrigger( ParametersRecord* r ) {

	if( ! _currentTrigger ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentTrigger is nullptr\n"
			;
		}
	if( _currentTrigger->_parameters ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentTrigger->_parameters is NOT nullptr\n"
			;
		}
	_currentTrigger->_parameters	= r;
	}

void	Event::pushActionsPopulateEvent() {

	pushActionsPopulate(
		new PopulateComposer< Event, ActionsRecord >(
				*this,
				&Event::populateEvent
				)
		);
	}

void	Event::pushEventPopulateResult() {

	pushEventPopulate(
		new PopulateComposer< Event, EventRecord >(
				*this,
				&Event::populateResult
				)
		);
	}

void	Event::pushParametersPopulateTrigger() {

	pushParametersPopulate(
		new PopulateComposer< Event, ParametersRecord >(
				*this,
				&Event::populateTrigger
				)
		);
	}

void	Event::pushChoicePopulateEvent() {

	pushChoicePopulate(
		new PopulateComposer< Event, ChoiceRecord >(
				*this,
				&Event::populateEvent
				)
		);
	}

bool	Event::pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept {

	if( _currentEvent ) {
		_stacks._eventStack.push( _currentEvent );
		}

	_currentEvent	= new EventRecord( _context );

	_currentEvent->_choice	= new ChoicesRecord();

	_currentEventPopulate->populate( _currentEvent );

	pushActionsPopulateEvent();
	pushChoicePopulateEvent();

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::Event& node ) noexcept {

	/*	An Event node can only be contained in an Events node
	 */

	popGuardPopulate();
	popActionsPopulate();

	_endState._eventList.put( _currentEvent );

	_events.push_back( _currentEvent );

	_currentEvent	= _stacks._eventStack.pop();

	return true;
	};

bool	Event::pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept {
	/*	An Events node can only be contained in a Compartment
	 */

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::Events& node ) noexcept {

	return true;
	};

bool	Event::pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept {

	if( !_currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvent is null\n"
			;
		_description	= "Unexpected Parameter without _currentEvent.";
		_position		= node._position;
		return false;
		}

	if( !_currentEvent->_trigger ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvent->_trigger is null\n"
			;
		_description	= "Unexpected Parameter without _trigger.";
		_position		= node._position;
		return false;
		}

	_currentParameters->_parameters.push_back( node._parameter );

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept {

	return true;
	};

bool	Event::pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept {

	if( _currentParameters ) {
		_stacks._parametersStack.push( _currentParameters );
		}

	_currentParameters	= new ParametersRecord();

	_currentParametersPopulate->populate( _currentParameters );

	return true;
	};

bool	Event::post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept {

	_endState._parametersList.put( _currentParameters );

	_currentParameters	= _stacks._parametersStack.pop();

	return true;
	};

