/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_recstackh_
#define _oscl_uml_hsm_generator_cpp_recstackh_

#include <vector>
#include <string>
#include "oscl/queue/queue.h"
#include "records.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** */
class RecordStack {
	public:
		/** This stack is used to deal with nesting
			EventRecords. It is empty at the end of
			visitation.
		 */
		Oscl::Queue< EventRecord >	_eventStack;

		/** This stack is used to deal with nesting
			ContextFuncRecords. It is empty at the end of
			visitation.
		 */
		Oscl::Queue< ContextFuncRecord >	_contextFuncStack;

		/** */
		Oscl::Queue< ContextArgsRecord >	_contextArgsStack;

		/** This stack is used to deal with nesting
			GuardRecords. It is empty at the end of
			visitation.
		 */
		Oscl::Queue< GuardRecord >	_guardStack;

		/** */
		Oscl::Queue< GuardExpRecord >	_guardExpStack;

		/** */
		Oscl::Queue< GroupRecord >	_groupStack;

		/** This stack is used to deal with nesting
			GuardRecords. It is empty at the end of
			visitation.
		 */
		Oscl::Queue< ActionsRecord >	_actionsStack;

		/** */
		Oscl::Queue< ActionRecord >		_actionStack;

		/** */
		Oscl::Queue< VarFuncRecord >	_varFuncStack;

		/** */
		Oscl::Queue< ArgsRecord >	_argumentsStack;

		/** */
		Oscl::Queue< ArgRecord >	_argumentStack;

		/** */
		Oscl::Queue< VarExpRecord >	_varExpStack;

		/** */
		Oscl::Queue< BinOperandRecord >	_binOperandStack;

		/** This stack is used to deal with nesting
			GuardRecords. It is empty at the end of
			visitation.
		 */
		Oscl::Queue< TriggerRecord >	_triggerStack;

		/** */
		Oscl::Queue< EntryRecord >	_entryStack;

		/** */
		Oscl::Queue< ExitRecord >	_exitStack;

		/** */
		Oscl::Queue< ChoiceRecord >	_choiceStack;

		/** */
		Oscl::Queue< ParametersRecord >	_parametersStack;

		/** */
		Oscl::Queue< EventsRecord >	_eventsStack;

		/** */
		Oscl::Queue< NamespaceRecord >	_namespaceStack;

		/** */
		Oscl::Queue< IncludeFileRecord >	_includeFileStack;

		/** */
		Oscl::Queue< TransitionRecord >	_transitionStack;

		/** */
		Oscl::Queue< StateRecord >	_stateStack;

		/** */
		Oscl::Queue< ChoicesRecord >	_choicesStack;

		/** */
		Oscl::Queue< TargetRecord >	_targetStack;

	public:
		/** */
		Oscl::Queue< Populate< ActionsRecord > >	_actionsPopStack;

		/** */
		Oscl::Queue< Populate< ContextFuncRecord > >	_contextFuncPopStack;

		/** */
		Oscl::Queue< Populate< ChoiceRecord > >	_choicePopStack;

		/** */
		Oscl::Queue< Populate< GuardRecord > >	_guardPopStack;

		/** */
		Oscl::Queue< Populate< EventRecord > >	_eventPopStack;

		/** */
		Oscl::Queue< Populate< EventsRecord > >	_eventsPopStack;

		/** */
		Oscl::Queue< Populate< ParametersRecord > >	_parametersPopStack;

		/** */
		Oscl::Queue< Populate< EntryRecord > >	_entryPopStack;

		/** */
		Oscl::Queue< Populate< VarExpRecord > >	_varExpPopStack;

		/** */
		Oscl::Queue< Populate< ArgRecord > >	_argPopStack;

		/** */
		Oscl::Queue< Populate< VarFuncRecord > >	_varFuncPopStack;

		/** */
		Oscl::Queue< Populate< ArgsRecord > >	_argsPopStack;

		~RecordStack();
	};

}
}
}
}

#endif
