# C++ Code Generation

## HSM C++ StateVar header (fsm.h)
Declares the StateVar class that represents the object
that is instantiated and used by the client context.

```
class StateVar {
    public:
        class ContextApi {
            virtual void <action>() = 0;
            };
    private:
        class State {
            public:
                virtual void    <trigger>(
                                    ContextApi& context
                                    StateVar&   stateVar
                                    [,<trigger_arguments>]
                                    ) const;
            };
        friend class State;

        class StateA : public State {
            public:
                StateA();
                void    <trigger>(
                            ContextApi& context
                            StateVar&   stateVar
                            [,<trigger_arguments>]
                            ) const override;
            };
        friend class StateA;

        class StateAStateB : public StateA {
            public:
                void    <trigger>(
                            ContextApi& context
                            StateVar&   stateVar
                            [,<trigger_arguments>]
                            ) const override;
            };
    private:
        const static StateA     _StateA;
        const static StateAStateB    _StateAStateB;

    private:
        ContextApi& _context;
        const State*    _state;

    public:
        StateVar(
            ContextApi&             context,
            const StateVar::State*  initialState    = 0
            );

        void    <trigger>(
                    [,<trigger_arguments>]
                    );

    private:
        void    changeToStateAStateB();
    public:
        static const StateVar::State&   getStateAStateB();
    private:
        void    protocolError();

    };

```

### HSM C++ State
Declares the abstract State class that contains
an operation for each trigger identified in the HSM.

## HSM C++ StateVar implementation (fsm.cpp)
Defines the C++ StateVar implementation

```
const StateVar::StateAStateB StateVar::_StateAStateB;

StateVar::StateVar(
    ):
        _context(context),
        _state( initialState?initialState:&_StateAStateB)
        {
    }

void    StateVar::<trigger>(
            [<trigger_arguments>]
            ) {
    _state-><trigger>(_context,this [,<trigger_arguments>]);
    }

void    StateVar::changeToStateAStateB() {
    _state  = &_StateAStateB;
    }

const StateVar::State&  StateVar::getStateAStateB();

void    StateVar::State::<trigger>(
            ContextApi& context,
            StateVar&   stateVar
            [,<trigger_arguments>]
            ) {
    stateVar.protocolError();
    }
```

## HSM State C++ implementation (state.cpp)
Defines the C++ State transitions.

The implementation of the abstract State class trigger operation
performs the default action that is executed if no other State
subclass handles the trigger. Typically, this action would be to
cause a run-time "exception" indicating a protocol error. Such a
default action is the safest course of action since a best practice
is to ensure that every trigger is handled explicitly in the HSM
definition.

```
void    StateVar::StateAStateB::StateAStateB() {
    }

void    StateVar::StateAStateB::<trigger>(
            ContextApi& context,
            StateVar&   stateVar
            [,<trigger_arguments>]
            ) {
    if( <guard_expression> ) {
        if( <guard_expression> ) {
                if( guard_expression ) {
                    [<exit_actions>]
                    [<transition_actions>]
                    [<entry_actions>]
                    stateVar.changeTo<target_state>();
                    return;
                    }
                else {
                    [<exit_actions>]
                    [<transition_actions>]
                    [<entry_actions>]
                    stateVar.changeTo<target_state>();
                    return;
                    }
            return;
            }

        if( <guard_expression> ) {
            [<exit_actions>]
            [<transition_actions>]
            [<entry_actions>]
            stateVar.changeTo<target_state>();
            return;
            }
        else {
            [<exit_actions>]
            [<transition_actions>]
            [<entry_actions>]
            stateVar.changeTo<target_state>();
            return;
            }
        return;
        }

    StateA::<trigger>(
        context,
        stateVar
        [,<trigger_arguments>]
        );
    }
```

## Guard Expressions and Action Statements
Guard expressions and action statements are tricky because they
could consist of a mix of operators and statements. The individual
statements may be related to either `trigger_arguments` parameters OR
members of the "abstract" context. For example, consider the following
transistion:

```
    ev( T& t ) [ t.a && w || z() ] / a()
```

This might to partially translate to C++ like this:

```
void State::ev( 
        ContextApi& context,
        StateVar&   stateVar,
        T&          t
        ) {
    if( t.a && context.w && context.z() ) {
        context.a();
        }
    }
```

The logic to generate the guard expression might
prefix each guard component with `context.` only
if the guard component begins with a "name" that
does not match an argument name (e.g., `t`).
