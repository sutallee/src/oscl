/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_compartmenth_
#define _oscl_uml_hsm_generator_cpp_compartmenth_

#include <vector>
#include <string>
#include "event.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** This visitor extracts all of the events, guards and
	actions an event from a single node of parsed UML
	compartment text.
 */
class Compartment : public Event {
	public:
		/*	Without these using declarations the
			new compiler (gcc (GCC) 13.2.1 20231205)
			generates warnings:
			hidden [-Werror=overloaded-virtual=]

			Apparently, without these "using" declarations,
			the compiler first looks up the name of the
			functions (e.g., pre, post) ignoring the parameters
			and does not go further into inherited scopes if
			it finds any match, ignoring overloading.

			Adding these "using" declarations tells the compiler
			to delve deeper and consider the base class definitions.

			Furthermore, in all but the most derived class, these
			"using" declarations MUST be public.

			It is possible to specify the "using" declarations
			for the root inherited class. For example, in this
			case:

			using Oscl::UML::Parser::Transition::Visitor::pre;
			using Oscl::UML::Parser::Transition::Visitor::post;

			The advantage is that the intermediate classes do
			not need the "public:"  to be specified for their
			"using" declarations.

			However, IMHO, that affects maintainability for the
			case where the root inherited class changes names
			and this class doesn't care since it inherits the
			root indirectly (e.g., via Event).

			There's a discussion of possible rationale in this thread:

			https://forums.codeguru.com/showthread.php?459396-Overloading-virtual-functions

			The intent is to warn the user that he/she may have made
			a mistake.

			Personally, I find this offensive. It seems to me that
			the compiler should automatically consider all of the
			inherited virtual functions as well. In particular if
			it cannot find a matching parameter signature in this
			class scope.

			https://www.stroustrup.com/bs_faq2.html#overloadderived
		 */
		using Event::pre;
		using Event::post;

	public:
		/** */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_globalParameters;

	private:
		/** */
		EntryRecord*	_currentEntry;

		/** */
		ExitRecord*		_currentExit;

		/** */
		EventsRecord*	_currentEvents;

	public:
		/** */
		EventsRecord*	_events;

		/** */
		EntryRecord*	_entry;

		/** */
		ExitRecord*		_exit;

	public:
		/** */
		Compartment(
			void* context	= nullptr,
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters = nullptr
			);

		/** */
		~Compartment();

		/** */
		friend std::ostream& operator << ( std::ostream& os, const Compartment& t );

	private:
		/** */
		void	populateResult( EventsRecord* r );

		/** */
		void	pushEventsPopulateResult();

	private:
		/** */
		void	populateEntry( ActionsRecord* r );

		/** */
		void	pushActionsPopulateEntry();

	private:
		/** */
		void	populateExit( ActionsRecord* r );

		/** */
		void	pushActionsPopulateExit();

	private:
		/** */
		void	populateEvents( EventRecord* r );

		/** */
		void	pushEventPopulateEvents();

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;
	};

}
}
}
}

#endif
