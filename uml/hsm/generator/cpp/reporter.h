#ifndef _oscl_uml_hsm_generator_cpp_reporterh_
#define _oscl_uml_hsm_generator_cpp_reporterh_

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/**	This class allows the HSM Generator to report
	semantic errors independent of the context specifics.

	FIXME:
		There are times when we need to report
		a single error for multiple contexts.

		It might be "better" if the reportError
		function were to be parameterized such
		that multiple contexts could be supplied
		in a single invocation.

		Or perhaps we could change the abstract
		Reporter to require multiple methods to
		add individual context/position before 
		invoking a final method with a description
		to perform the actual output of the
		error report.
 */
class Reporter {
	public:
		/**	Invoked to report a semantic error.
			description - a string that desribes the error.
			context		-	A pointer to the application-specific
							data that is the source of the error.
							This may be used by the context to
							highlight the a specific element
							(e.g. GUI text field).
			position	-	This is the last position within the text
							field where the error was encountered.
							This value may be negative, which indicates
							that the entire text field is to be selected.
		 */
		virtual void	reportError(
							const char*	description,
							void*		context,
							unsigned	position
							) = 0;
	};
}
}
}
}
#endif
