/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_eventh_
#define _oscl_uml_hsm_generator_cpp_eventh_

#include <vector>
#include <string>
#include "choice.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** This visitor extracts an event from a single node
	of parsed UML event transition text.
 */
class Event : public Choice {
	public:
		/*	Without these using elements the
			new compiler (gcc (GCC) 13.2.1 20231205)
			generates warnings:
			hidden [-Werror=overloaded-virtual=]
		 */
		using Choice::pre;
		using Choice::post;

	public:
		/** */
		std::vector< EventRecord* >	_events;

		/** */
		std::vector< ContextFuncRecord* >	_actions;

	protected:
		/** */
		EventRecord*	_currentEvent;

		/** */
		TriggerRecord*	_currentTrigger;

		/** */
		ParametersRecord*	_currentParameters;

	public:
		/** */
		EventRecord*	_result;

	public:
		/** */
		Event(
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters,
			void* context	= nullptr
			);

		/** */
		~Event();

		/** */
		friend std::ostream& operator << ( std::ostream& os, const Event& t );

		/** RETURN true if a parameter with the name arg
			is in the parameter list.
		 */
		bool	isValidArgument(
					const std::string&	arg
					) const override;

		/** RETURN true if a parameter with the name arg
			is in the parameter list and copies the matching
			parameter into the parameter variable.
		 */
		bool	resolveType(
					const std::string&						arg,
					Oscl::UML::Parser::Types::Parameter&	parameter
					) const override;

	private:
		/** */
		void	populateResult( EventRecord* r );

		/** */
		void	populateEvent( ActionsRecord* r );

		/** */
		void	populateTrigger( ParametersRecord* r );

		/** */
		void	populateEvent( ChoiceRecord* r );

	private:
		/** */
		void	pushEventPopulateResult();

		/** */
		void	pushActionsPopulateEvent();

	private:
		/** */
		void	pushParametersPopulateTrigger();

		/** */
		void	pushChoicePopulateEvent();

	public:
		/** re-implement */
		bool	pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

		/** re-implement */
		bool	post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

	public:
		/** pass-through
			WORK-AROUND
			Apparently, the compiler cannot resolve overloaded
			superclass calls. Therefore, since we want to be
			able call the superclass of this supperclass from
			its subclass, we need to do it ourselves here.
			The implementation of this does nothing more
			than forward the call to the superclass.
		 */
		bool	pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;
	};

}
}
}
}

#endif
