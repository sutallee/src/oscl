/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_recordsh_
#define _oscl_uml_hsm_generator_cpp_recordsh_

#include <vector>
#include <string>
#include "oscl/uml/parser/types/parameter.h"
#include "oscl/queue/queue.h"
#include "reporter.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** */
struct LiteralRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_literal;

	/** */
	LiteralRecord();

	/** */
	~LiteralRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

#if 0
/** */
struct ExpRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	ExpRecord();

	/** */
	~ExpRecord();
	};
#endif

/** */
struct ContextArgRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	Oscl::UML::Parser::Types::Parameter _parameter;

	/** */
	ContextArgRecord( const std::string& name );

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&		s,
				unsigned			indent
				);
	};

/** */
struct ContextArgsRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::vector< ContextArgRecord* >	_args;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&		s,
				unsigned			indent
				);
	};

/** */
struct ParametersRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::vector< Oscl::UML::Parser::Types::Parameter >	_parameters;

	/** */
	ParametersRecord( );

	/** */
	~ParametersRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&		s,
				unsigned			indent
				);
	};

/**	A context function ( guard or action function )
	may have zero or more arguments that consist
	of a single identifier.
 */
struct ContextFuncRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string	_name;

	/** */
	ContextArgsRecord*	_args;

	/** */
	std::string		_expression;

	/** */
	ContextFuncRecord( const std::string& name );

	/** */
	~ContextFuncRecord();

	/** */
	bool	checkVars( const ParametersRecord* parameters ) const;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

/** */
struct VarFuncRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_name;

	/** */
	std::vector< std::string >	_vars;

	/** */
	std::vector< std::string >	_refs;

	/** */
	struct ArgsRecord*	_args;

	/** */
	std::string		_expression;

	/** */
	VarFuncRecord(
		const std::vector< std::string >&	vars,
		const std::vector< std::string >&	refs
		);

	/** */
	~VarFuncRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

/** */
struct VarExpRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	std::vector< std::string >	_vars;

	/** */
	std::vector< std::string >	_refs;

	/** */
	VarExpRecord(
		const std::vector< std::string >&	vars,
		const std::vector< std::string >&	refs
		);

	/** */
	~VarExpRecord();

	/** */
	void appendToExpression( std::string& expression ) const;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

/** */
struct ActionRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string	_expression;

	/*	An ActionRecord can be one of:
		ContextFuncRecord
		VarFunc
		VarExpr
	 */
	/** */
	ContextFuncRecord*	_contextFunc;

	/** */
	VarFuncRecord*		_varFunc;

	/** */
	VarExpRecord*		_varExp;

	/** */
	struct ChoicesRecord*	_choices;

	/** */
	ActionRecord( );

	/** */
	~ActionRecord();

	/** */
	bool	checkVars( const ParametersRecord* parameters ) const;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

/** */
struct TransitionRecord {

	/** */
	void*	__qitemlink;

	/** */
	struct ActionsRecord*	_actions;

	/** */
	struct TargetRecord*	_target;

	/** */
	struct ChoicesRecord*	_choices;

	/** */
	TransitionRecord();

	/** */
	~TransitionRecord();

	/** */
	void	printTransitionTree( unsigned indent, const std::string& source );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);

	/** */
	bool	allNull();

	/** */
	void	set( ActionsRecord* actions );

	/** */
	void	set( TargetRecord* target );

	/** */
	void	set( ChoicesRecord* choices );
	};

/** */
struct ActionsRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::vector< ActionRecord* >	_actions;

	/** */
	std::vector< std::string >	_expressions;

	/** */
	TransitionRecord*	_transition;

	/** */
	ActionsRecord();

	/** */
	~ActionsRecord();

	/** */
	void	printTransitionTree( unsigned indent , bool expectTransition=true);

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);

	};

/** */
struct ResetRecord {

	/** */
	void*	__qitemlink;

	/** */
	TransitionRecord*	_transition;

	/** */
	ResetRecord();

	/** */
	~ResetRecord();

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream& file,
				unsigned indent
				);
	};

/** */
struct EntryRecord {

	/** */
	void*	__qitemlink;

	/** optional
		The context is responsible for the life-cycle
		of the _actions.
	 */
	ActionsRecord*	_actions;

	/** */
	void*	const	_context;

	/** */
	EntryRecord( void* context );

	/** */
	~EntryRecord();

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);
	};

/** */
struct ExitRecord {

	/** */
	void*	__qitemlink;

	/** optional
		The context is responsible for the life-cycle
		of the _actions.
	 */
	ActionsRecord*	_actions;

	/** */
	void*	const	_context;

	/** */
	ExitRecord( void* context );

	/** */
	~ExitRecord();
	};

/** */
struct BinOperatorRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string	_operator;

	/** */
	BinOperatorRecord( const std::string& name );

	/** */
	~BinOperatorRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);
	};

/** */
struct OperandApi {
	/** */
	virtual void	print( unsigned indent )=0;

	/** */
	virtual void	genStateCpp(
						std::string&	s,
						unsigned		indent
						)=0;
	/** */
//	virtual void	expression(
//						std::string&	s
//						)=0;

	/** */
	virtual struct ContextFuncRecord*	getContextFunc()=0;

	/** */
	virtual struct ArgRecord*	getArg()=0;
	};

/** */
struct BinOperandRecord : public OperandApi {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	class ArgRecord*		_arg;

	/** */
	class VarExpRecord*		_varExp;

	/** */
	class ContextFuncRecord*		_contextFunc;

	/** */
	class LiteralRecord*		_literal;

	/** */
	BinOperandRecord();

	/** */
	virtual ~BinOperandRecord();

	/** */
	void	print( unsigned indent ) override;

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				) override;

	/** */
	struct ContextFuncRecord*	getContextFunc() override;

	/** */
	struct ArgRecord*	getArg() override;
	};

/** */
struct GuardExpRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	std::vector< OperandApi* >	_operand;

	/** */
	std::vector< BinOperatorRecord* >	_operator;

	/** */
	GuardExpRecord();

	/** */
	~GuardExpRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent
				);

	/** */
	void	expression(
				std::string&	s
				);
	};

/** */
struct GroupRecord : public OperandApi {

	/** */
	void*	__qitemlink;

#if 0
	/** */
	std::string		_expression;

	/** */
	std::vector< BinOperandRecord* >	_operand;

	/** */
	std::vector< BinOperatorRecord* >	_operator;
#endif

	/** */
	GuardExpRecord* _exp;

	/** */
	GroupRecord();

	/** */
	virtual ~GroupRecord();

	/** */
	bool	set( GuardExpRecord* exp);

	/** */
	void	print( unsigned indent ) override;

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				) override;

	/** */
	struct ContextFuncRecord*	getContextFunc() override;

	/** */
	struct ArgRecord*	getArg() override;

	/** */
	void	expression(
				std::string&	s
				);
	};

/** */
struct GuardRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::vector< std::string >	_vars;

	/** */
	std::vector< ContextFuncRecord* >	_guardFunctions;

	/** */
	std::string 				_expression;

	/** */
	GuardExpRecord*			_guardExp;

	/** */
	GuardRecord();

	/** */
	~GuardRecord();

	/** */
	bool	checkVars( const ParametersRecord* parameters ) const;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent
				);
	};

/** */
struct ArgRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	ContextFuncRecord*	_contextFunc;

	/** */
	VarFuncRecord*	_varFunc;

	/** */
	VarExpRecord*	_varExp;

	/** */
	ArgRecord();

	/** */
	~ArgRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);

	};

/**	List of of arguments to a function.
 */
struct ArgsRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string		_expression;

	/** */
	std::vector< ArgRecord* >	_args;

	/** */
	ArgsRecord();

	/** */
	~ArgsRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::string&	s,
				unsigned		indent
				);

	};

/** */
struct ChoiceRecord {

	/** */
	void*	__qitemlink;

	/** */
	void*	const	_context;

	/** */
	GuardRecord*	_guard;

	/** */
	ActionsRecord*	_actions;

	/** */
	TransitionRecord*	_transition;

	/** */
	ChoiceRecord( void* context = nullptr );

	/** */
	~ChoiceRecord();

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);

	};

/** */
struct ChoicesRecord {

	/** */
	void*	__qitemlink;

	/** Individual choices
	 */
	std::vector< ChoiceRecord* >	_choices;

	/** */
	ChoicesRecord( );

	/** */
	~ChoicesRecord();

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);
	};

/** */
struct TriggerRecord {

	/** */
	void*	__qitemlink;

	/** */
	std::string	_name;

	/** */
	ParametersRecord*	_parameters;

	/** */
	void*	const _context;

	/** */
	const unsigned	_position;

	/** */
	TriggerRecord(
		const std::string&	name,
		void*				context,
		unsigned			position
		);

	/** */
	~TriggerRecord();

	/** */
	bool	operator == ( const TriggerRecord& other) const;

	/** */
	bool	typeMatch ( const TriggerRecord& other) const;

	/**	Given the var name, copy the corresponding parameter as a string
		into the type string including the var name.
		RETURN: false if the var name is not found in the
		parameter list.
	 */
	bool	resolveType( const std::string& var, std::string& type ) const;

	/** */
	bool	resolveType( const std::string& var, Oscl::UML::Parser::Types::Type& type ) const;

	/** */
	bool	resolveType(
				const std::string&						var,
				Oscl::UML::Parser::Types::Parameter&	parameter
				) const;

	/** */
	void	print( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream& file,
				unsigned indent
				);
	};

/** */
struct EventRecord {

	/** */
	void*	__qitemlink;

	/** Mandantory but may be initially null
		until a trigger is encountered in the
		parsing.
		The context is responsible for the life-cycle
		of the _trigger.
	 */
	TriggerRecord*	_trigger;

	/** optional
		The context is responsible for the life-cycle
		of the _guard.
		The _choice member is mutually exclusive with
		the _actions member.
	 */
	ChoicesRecord*	_choice;

	/** optional
		The context is responsible for the life-cycle
		of the _actions.

		The _choice member is mutually exclusive with
		the _actions member.

		FIXME:
			* Why multiple ActionsRecords?
			* Why multiple ActionsRecords?
	 */
	ActionsRecord*	_actions;

	/** */
	TransitionRecord*	_transition;

	/** */
	void*	const	_context;

	/** */
	EventRecord( void* context = nullptr);

	/** */
	~EventRecord();

	/** */
	void	set( ActionsRecord* actions );

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&		file,
				unsigned			indent,
				const std::string&	stateName,
				const std::string&	parentName
				);
	};

/** */
struct EventsRecord {
	/** */
	void*		__qitemlink;

	/** */
	std::vector< Oscl::UML::HSM::Generator::EventRecord* >	_events;

	/** */
	void	merge( std::vector< Oscl::UML::HSM::Generator::EventRecord* >& events );

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&		file,
				unsigned			indent,
				const std::string&	stateName,
				const std::string&	parentName
				);
	};

/** */
struct StateRecord {
	/** */
	void*		__qitemlink;

	/** */
	Oscl::UML::HSM::Generator::Reporter&	_reporter;

	/** */
	std::string	_name;

	/** */
	std::string	_parent;

	/** */
	std::vector< const TriggerRecord* >	_triggers;

	/** */
	EventsRecord*		_events;

	/** */
	EntryRecord*		_entry;

	/** */
	ExitRecord*			_exit;

	/** */
	void*	const _context;

	/** */
	StateRecord(
		Oscl::UML::HSM::Generator::Reporter&			reporter,
		const std::string&	name,
		const std::string&	parent,
		void*				context = nullptr
		);

	/** */
	bool	checkEvents();

	/** */
	bool	checkArguments();

	/** */
	bool	checkTriggers();

	/** */
	bool	buildUniqueTriggers();

	/**
		RETURN: true if the
	 */
	bool	resolveArgumentType( const std::string arg, std::string& type);

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream& file,
				unsigned indent
				);
	};

/** */
struct TargetRecord {
	/** */
	void*		__qitemlink;

	/** */
	std::string		_statePath;

	/** */
	std::string		_lcaPath;

	/** */
	bool			_hasSourceState;

	/** */
	std::vector< EntryRecord* >	_entry;

	/** */
	TargetRecord(
		const std::string&	statePath,
		const std::string&	lcaPath,
		bool				hasSourceState
		);

	/** */
	~TargetRecord();

	/** */
	void	printTransitionTree( unsigned indent );

	/** */
	void	genStateCpp(
				std::ostream&				file,
				unsigned					indent,
				std::vector< std::string >&	actions
				);
	};

/**
 */
struct NamespaceRecord {
	/** */
	void*		__qitemlink;

	/** */
	std::string	_name;

	/** */
	NamespaceRecord(
		const std::string&	name
		);
	};

/**
 */
struct IncludeFileRecord {
	/** */
	void*		__qitemlink;

	/** */
	std::string	_name;

	/** */
	IncludeFileRecord(
		const std::string&	name
		);
	};

/**	This template allows subclasses to override
	the actions taken in a base class when
	a pointer to a record of type T needs to be stored
	in a location other than the default.
	The __qitemlink MUST NOT be used by the
	implementation.
 */
template <class T>
class Populate {
	public:
		/** */
		void*		__qitemlink;

	public:
		virtual ~Populate() {}
		virtual void	populate( T* t ) = 0;
	};

/** This Composer template makes it easy for
	classes to create/use composition to handle
	Populate<T> objects without inheritance.
 */
template < class Context, class T >
class PopulateComposer: public Populate<T> {
	private:
		Context&	_context;
		void	(Context::*_populate)( T* t );

	public:
		PopulateComposer(
			Context& context,
			void	(Context::*populate)( T* t )
			):
		_context( context ),
		_populate( populate )
		{
		}
	void	populate( T* t ) {
		(_context.*_populate)( t );
		}
	};

}
}
}
}

#endif
