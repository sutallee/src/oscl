/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include <algorithm>
#include "stateh.h"
#include "compartment.h"
#include "event.h"
#include "action.h"
#include "choice.h"
#include "oscl/uml/hsm/namespace.h"
#include "oscl/uml/hsm/state.h"
#include "oscl/uml/hsm/target.h"
#include "oscl/uml/hsm/event.h"
#include "oscl/uml/hsm/choice.h"
#include "oscl/uml/hsm/action.h"
#include "oscl/uml/hsm/entry.h"
#include "oscl/uml/hsm/include.h"
#include "oscl/uml/parser/transition/driver.h"
#include "oscl/uml/parser/transition/visitor.h"
#include "oscl/uml/parser/transition/binop.h"
#include "oscl/uml/parser/transition/varexp.h"
#include "oscl/uml/parser/transition/ctxfunc.h"
#include "oscl/uml/parser/transition/argument.h"
#include "oscl/uml/parser/transition/arguments.h"
#include "oscl/uml/parser/transition/guardexpr.h"
#include "oscl/uml/parser/transition/literal.h"
#include "oscl/uml/parser/transition/literal.h"

static void	writeStateTriggers(
			std::ostream&										file,
			std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&		triggers,
			std::string											virt,
			std::string											over
			) {
	// write State events
	for( unsigned i=0; i < triggers.size(); ++i ) {
		file
			<< "\t\t\t\t"
			<< "/** */\n"
			<< "\t\t\t\t"
			;
		if( virt.size() ) {
			file
				<< virt << " "
				;
			}
			;
		file
			<< "void\t"
			<< triggers[i]->_name
			<< "(\n"
			<< "\t\t\t\t\t"
			<< "ContextApi&	context,\n"
			<< "\t\t\t\t\t"
			<< "StateVar&	stateVar"
			;

		if( triggers[i]->_parameters) {
			for( unsigned j=0; j < triggers[i]->_parameters->_parameters.size(); ++j ){
				file
					<< ",\n"
					<< "\t\t\t\t\t"
					<< triggers[i]->_parameters->_parameters[j]
					;
				}
			}

		file
			<< "\n"
			<< "\t\t\t\t\t"
			<< ") const noexcept"
			;
		if( over.size() ) {
			file
				<< " " << over
				;
			}

		file 
			<< ";"
			<< "\n\n"
			;
		}
	}

static void	writeStateTriggerImplementations(
			std::ostream&										file,
			std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&	triggers
			) {
	// write State events
	for( unsigned i=0; i < triggers.size(); ++i ) {
		file
			<< "void\tStateVar::State::"
			<< triggers[i]->_name
			<< "(\n"
			<< "\t\t"
			<< "ContextApi&	context,\n"
			<< "\t\t"
			<< "StateVar&	stateVar"
			;

		if( triggers[i]->_parameters ) {
			for( unsigned j=0; j < triggers[i]->_parameters->_parameters.size(); ++j ){
				file
					<< ",\n"
					<< "\t\t"
					<< triggers[i]->_parameters->_parameters[j]
					;
				}
			}

		file
			<< "\n"
			<< "\t\t"
			<< ") const noexcept {\n"
			;

		file 
			<< "\t"
			<< "stateVar.protocolError();\n"
			<< "\t"
			<< "}\n"
			<< "\n"
			;
		}
	}

static void	writeEventTriggers(
			std::ostream&										file,
			std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&	triggers
			) {

	// write State events
	for( unsigned i=0; i < triggers.size(); ++i ) {
		file
			<< "\t\t"
			<< "/** */\n"
			<< "\t\t"
			;
		file
			<< "void\t"
			<< triggers[i]->_name
			<< "("
			;

		if( triggers[i]->_parameters ) {
			for( unsigned j=0; j < triggers[i]->_parameters->_parameters.size(); ++j ){
				if( j==0 ) {
					file
						<< "\n"
						;
					}
				else {
					file
						<< ",\n"
						;
					}
				file
					<< "\t\t\t"
					<< triggers[i]->_parameters->_parameters[j]
					;
				}
			}

		file
			<< "\n"
			<< "\t\t\t"
			<< ") noexcept;"
			<< "\n\n"
			;
		}
	}

static void	writeEventTriggerImplementations(
			std::ostream&										file,
			std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&	triggers
			) {

	// write State events
	for( unsigned i=0; i < triggers.size(); ++i ) {
		file
			<< "\n"
			<< "void	StateVar::"
			<< triggers[i]->_name
			<< "("
			;

		if( triggers[i]->_parameters ) {
			for( unsigned j=0; j < triggers[i]->_parameters->_parameters.size(); ++j ){
				if( j==0 ) {
					file
						<< "\n"
						;
					}
				else {
					file
						<< ",\n"
						;
					}
				file
					<< "\t\t\t"
					<< triggers[i]->_parameters->_parameters[j]
					;
				}
			}

		file
			<< "\n"
			<< "\t\t\t"
			<< ") noexcept {\n"
			<< "\n"
			<< "\t"
			<< "_state->"
			<< triggers[i]->_name
			<< "(\n"
			<< "\t\t"
			<< "_context,\n"
			<< "\t\t"
			<< "*this"
			;

		if( triggers[i]->_parameters ) {
			for( unsigned j=0; j < triggers[i]->_parameters->_parameters.size(); ++j ){
				file
					<< ",\n"
					<< "\t\t"
					<< triggers[i]->_parameters->_parameters[j]._name
					;
				}
			}

		file
			<< "\n"
			<< "\t\t"
			<< ");\n"
			<< "\t"
			<< "}\n"
			;

		}
	}

using namespace Oscl::UML::HSM::Generator::StateH;

Visitor::Visitor(
		Oscl::UML::HSM::Generator::Reporter&	reporter,
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters
		):
	_reporter( reporter ),
	_globalParameters( globalParameters ),
	_currentNamespace( nullptr ),
	_currentState( nullptr ),
	_currentEvent( nullptr ),
	_currentChoices( nullptr ),
	_currentChoice( nullptr ),
	_currentActions( nullptr ),
	_currentReset( nullptr ),
	_currentEntry( nullptr ),
	_currentTarget( nullptr ),
	_currentTransition( nullptr ),
	_currentChoicePopulate( nullptr ),
	_currentActionsPopulate( nullptr ),
	_currentEntryPopulate( nullptr ),
	_currentEventPopulate( nullptr )
	{
	}

Visitor::~Visitor() {
	}

void	Visitor::populateReset( ActionsRecord* r) {

	if( ! _currentReset ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentReset is nullptr\n"
			;
		}

//	_currentReset->_actions	= r;
	}

void	Visitor::populateEvent( ActionsRecord* r) {

	if( ! _currentEvent ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEvent is nullptr\n"
			;
		}

//	_currentEvent->_actions.push_back( r );
	_currentEvent->set( r );
	}

void	Visitor::populateEntry( ActionsRecord* r) {

	if( ! _currentEntry ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEntry is nullptr\n"
			;
		}

	if( _currentEntry->_actions ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentEntry is NOT nullptr\n"
			;
		}

	_currentEntry->_actions	= r;
	}

void	Visitor::populateChoices( ChoiceRecord* r) {

	if( ! _currentChoices ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentChoices is nullptr\n"
			;
		}

	_currentChoices->_choices.push_back(r);

	}

void	Visitor::pushActionsPopulateReset() {

	pushActionsPopulate(
		new PopulateComposer< Visitor, ActionsRecord >(
				*this,
				&Visitor::populateReset
				)
		);
	}

void	Visitor::pushActionsPopulateEvent() {

	pushActionsPopulate(
		new PopulateComposer< Visitor, ActionsRecord >(
				*this,
				&Visitor::populateEvent
				)
		);
	}

void	Visitor::pushActionsPopulateEntry() {

	pushActionsPopulate(
		new PopulateComposer< Visitor, ActionsRecord >(
				*this,
				&Visitor::populateEntry
				)
		);
	}

void	Visitor::pushChoicePopulateChoices() {

	pushChoicePopulate(
		new PopulateComposer< Visitor, ChoiceRecord >(
				*this,
				&Visitor::populateChoices
				)
		);
	}

void	Visitor::populateEvent( ChoiceRecord* r ) {
	_currentEvent->_choice->_choices.push_back( r );
	}

void	Visitor::pushChoicePopulateEvent() {

	pushChoicePopulate(
		new PopulateComposer< Visitor, ChoiceRecord >(
				*this,
				&Visitor::populateEvent
				)
		);
	}

void	Visitor::pushChoicePopulate( Populate< ChoiceRecord >*  p) {

	if( _currentChoicePopulate ) {
		_stacks._choicePopStack.push( _currentChoicePopulate );
		}

	_currentChoicePopulate	= p;
	}

void	Visitor::popChoicePopulate() {

	delete _currentChoicePopulate;

	_currentChoicePopulate	= _stacks._choicePopStack.pop();
	}

void	Visitor::pushActionsPopulate( Populate< ActionsRecord >*  p) {

	if( _currentActionsPopulate ) {
		_stacks._actionsPopStack.push( _currentActionsPopulate );
		}

	_currentActionsPopulate	= p;
	}

void	Visitor::popActionsPopulate() {

	delete _currentActionsPopulate;

	_currentActionsPopulate	= _stacks._actionsPopStack.pop();
	}

void	Visitor::pushEntryPopulate( Populate< EntryRecord >*  p) {

	if( _currentEntryPopulate ) {
		_stacks._entryPopStack.push( _currentEntryPopulate );
		}

	_currentEntryPopulate	= p;
	}

void	Visitor::popEntryPopulate() {

	delete _currentEntryPopulate;

	_currentEntryPopulate	= _stacks._entryPopStack.pop();
	}

void	Visitor::pushEventPopulate( Populate< EventRecord >*  p) {

	if( _currentEventPopulate ) {
		_stacks._eventPopStack.push( _currentEventPopulate );
		}

	_currentEventPopulate	= p;
	}

void	Visitor::popEventPopulate() {

	delete _currentEventPopulate;

	_currentEventPopulate	= _stacks._eventPopStack.pop();
	}

void	Visitor::populateTarget( EntryRecord* r ) {

	if( ! _currentTarget ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentTarget is nullptr\n"
			;
		}

	_currentTarget->_entry.push_back( r );
	}

void	Visitor::populateState( EventRecord* r ) {

	if( ! _currentState ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentState is nullptr\n"
			;
		}

	if( !_currentState->_events ) {
		_currentState->_events	= new EventsRecord();
		}

	_currentState->_events->_events.push_back(r);
	}

void	Visitor::pushEntryPopulateTarget() {

	pushEntryPopulate(
		new PopulateComposer< Visitor, EntryRecord >(
				*this,
				&Visitor::populateTarget
				)
		);
	}

void	Visitor::pushEventPopulateState() {

	pushEventPopulate(
		new PopulateComposer< Visitor, EventRecord >(
				*this,
				&Visitor::populateState
				)
		);
	}

bool	Visitor::pre( const Oscl::UML::HSM::Reset& node ) noexcept {
	_currentReset	= new ResetRecord();

	if( _currentTransition ) {
		_stacks._transitionStack.push( _currentTransition );
		}

	_currentTransition	= new TransitionRecord();

	_currentReset->_transition	= _currentTransition;

	pushActionsPopulateReset();

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Reset& node ) noexcept {

	if( _currentTransition->allNull() ) {
		_currentReset->_transition	= nullptr;
		delete _currentTransition;
		}

	_currentTransition	= _stacks._transitionStack.pop();

	popActionsPopulate();

	_reset	= _currentReset;

	_endState._resetList.put( _currentReset );

	// There should only be one ResetRecord
	_currentReset	= nullptr;

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Namespace& node ) noexcept {

	if( _currentNamespace ) {
		_stacks._namespaceStack.push( _currentNamespace );
		}

	_currentNamespace	= new NamespaceRecord(
							node._name
							);

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Namespace& node ) noexcept {

	_endState._namespaceList.push( _currentNamespace );

	_currentNamespace	= _stacks._namespaceStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Include& node ) noexcept {

	if( _currentIncludeFile ) {
		_stacks._includeFileStack.push( _currentIncludeFile );
		}

	_currentIncludeFile	= new IncludeFileRecord(
							node._name
							);

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Include& node ) noexcept {

	_endState._includeFileList.push( _currentIncludeFile );

	_currentIncludeFile	= _stacks._includeFileStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::State& node ) noexcept {

	if( _currentState ) {
		_stacks._stateStack.push( _currentState );
		}

	_currentState	=
	new StateRecord(
			_reporter,
			( _currentState != nullptr )? _currentState->_name + "_" + node._name:node._name,
			( _currentState != nullptr )? _currentState->_name:"State",
			node._compartment._context
			);

	Oscl::UML::HSM::Generator::Compartment	ts( node._compartment._context );

	bool
	success	= node._compartment._parseTree.accept(ts);

	if( !success ) {

		_reporter.reportError(
			ts._description,
			ts._context,
			ts._position
			);

		delete _currentState;

		_currentState	= 0;

		return false;
		}

	_endState	+= ts._endState;

	_currentState->_events	= ts._events;
	_currentState->_entry	= ts._entry;
	_currentState->_exit	= ts._exit;

	pushEventPopulateState();

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::State& node ) noexcept {

	popEventPopulate();

	_endState._stateList.push( _currentState );

	_currentState	= _stacks._stateStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Target& node ) noexcept {
	/*	An indication that a transition branch
		has reached a state that terminates the
		transition.
	 */

	if( _currentTarget ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentTarget is NOT nullptr\n"
			;
		return false;
		}

	std::string	statePath;

	for( unsigned i=0; i < node._statePath.size(); ++i ){
		if( i != 0 ) {
			statePath	+= "_";
			}
		statePath	+= node._statePath[i];
		}

	std::string	lcaPath;

	for( unsigned i=0; i < node._lcaPath.size(); ++i ){
		if( i != 0 ) {
			lcaPath	+= "_";
			}
		lcaPath	+= node._lcaPath[i];
		}

	_currentTarget	= new TargetRecord(
							statePath,
							lcaPath,
							node._hasSourceState
							);

	pushEntryPopulateTarget();

	_currentTransition->set( _currentTarget );

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Target& node ) noexcept {
	_currentTarget	= nullptr;
	popEntryPopulate();
	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Event& node ) noexcept {

	if( !_currentState ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ":_currentState is null\n"
			<< "\n"
			;
		return false;
		}

	if( _currentEvent ) {
		_stacks._eventStack.push( _currentEvent );
		}

	Oscl::UML::HSM::Generator::Event
	ts(
		_globalParameters,
		node._driver._context
		);

	bool
	success	= node._driver._parseTree.accept(ts);

	if( !success ) {
		_reporter.reportError(
			ts._description,
			ts._context,
			ts._position
			);
		return false;
		}

	auto	trigger	= ts._endState._triggerList.first();

	if( !trigger ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ":No trigger in Event\n"
			<< "\n"
			;
		return false;
		}

	/*	There will only ever be one EventRecord
		for a Generator::Event.
		Since we're going to be maintaining a
		stack of EventRecords (for some reason)
		we now take control of the EventRecord
		and will put it back in the _endState
		in the post operation.
	 */

	if( !ts._result ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ":No event\n"
			<< "\n"
			;
		return false;
		}

	ts._endState._eventList.remove( ts._result );

	_currentEvent	= ts._result;

	_endState	+= ts._endState;

	if( _currentTransition ) {
		_stacks._transitionStack.push( _currentTransition );
		}

	_currentTransition	= new TransitionRecord();

	// Conditionally place _currentTransition in
	// existing Choice?
	if( _currentEvent->_choice->_choices.size() ) {
		if( _currentEvent->_choice->_choices.size() != 1 ){
			std::cerr
				<< __PRETTY_FUNCTION__
				<< "WARNING: more than one transition in choices\n"
				;
			}
		if( _currentEvent->_choice->_choices[0]->_transition  ){
			std::cerr
				<< __PRETTY_FUNCTION__
				<< "WARNING: transition is not nullptr in choices\n"
				;
			}
		_currentEvent->_choice->_choices[0]->_transition	= _currentTransition;
		}
	else {
		_currentEvent->_transition	= _currentTransition;
		}

	pushActionsPopulateEvent();
	pushChoicePopulateEvent();

	_currentEventPopulate->populate( _currentEvent );

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Event& node ) noexcept {

	if( _currentTransition->allNull() ) {
		_currentEvent->_transition	= nullptr;
		delete _currentTransition;
		}

	_currentTransition	= _stacks._transitionStack.pop();

	popChoicePopulate();
	popActionsPopulate();

	_endState._eventList.put( _currentEvent );

	_currentEvent	= _stacks._eventStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Choices& node ) noexcept {
	if( _currentChoices ) {
		_stacks._choicesStack.push( _currentChoices );
		}

	_currentChoices	= new ChoicesRecord();

	_currentTransition->set( _currentChoices );

	pushChoicePopulateChoices();

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Choices& node ) noexcept {

	popChoicePopulate();

	/*	FIXME:
		Do something with the Choices
		Choices are found in:
		Event(s)
		other Choices(s)
	 */

	_endState._choicesList.put( _currentChoices );

	_currentChoices	= _stacks._choicesStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Choice& node ) noexcept {

	const std::vector< Oscl::UML::Parser::Types::Parameter >*	triggerParameters	= nullptr;

	if( _currentEvent ) {
		auto trigger	= _currentEvent->_trigger;
		if( trigger ) {
			triggerParameters	= &trigger->_parameters->_parameters;
			}
		}

	Oscl::UML::HSM::Generator::Choice
	ts(
		triggerParameters,
		_globalParameters,
		node._driver._context
		);

	bool
	success	= node._driver._parseTree.accept(ts);

	if( !success ) {
		_reporter.reportError(
			ts._description,
			ts._context,
			ts._position
			);
		return false;
		}

	_endState	+= ts._endState;

	_currentChoice	= ts._result;

	_currentChoicePopulate->populate( _currentChoice );

	if( _currentTransition ) {
		_stacks._transitionStack.push( _currentTransition );
		}

	_currentTransition	= new TransitionRecord();

	_currentChoice->_transition	= _currentTransition;

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Choice& node ) noexcept {

	if( _currentTransition->allNull() ) {
		_currentChoice->_transition	= nullptr;
		delete _currentTransition;
		}

	_currentTransition	= _stacks._transitionStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Actions& node ) noexcept {

	if( _currentActions ) {
		_stacks._actionsStack.push( _currentActions );
		}

	_currentActions	= new ActionsRecord();

	if( _currentTransition ) {
		_currentTransition->set( _currentActions );
		}

	if( _currentTransition ) {
		_stacks._transitionStack.push( _currentTransition );
		}

	_currentTransition	= new TransitionRecord();

	_currentActions->_transition	= _currentTransition;

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Actions& node ) noexcept {
	/*	FIXME:
		These actions need to go somewhwere!
		What has actions?
		Compartments (State(s))
		Event(s)
		Choice(s)
		Entry
		Exit
		Target
	 */

	if( _currentTransition->allNull() ) {
		_currentActions->_transition	= nullptr;
		delete _currentTransition;
		}

	_currentTransition	= _stacks._transitionStack.pop();

	_endState._actionsList.put( _currentActions );

	_currentActions	= _stacks._actionsStack.pop();

	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Action& node ) noexcept {

	const std::vector< Oscl::UML::Parser::Types::Parameter >*	triggerParameters	= nullptr;

	if( _currentEvent ) {

		auto trigger	= _currentEvent->_trigger;

		if( trigger ) {
			triggerParameters	= &trigger->_parameters->_parameters;
			}
		}

	Oscl::UML::HSM::Generator::Action
	ts(
		triggerParameters,
		node._driver._context
		);

	bool
	success	= node._driver._parseTree.accept(ts);

	if( !success ) {

		_reporter.reportError(
			ts._description,
			ts._context,
			ts._position
			);

		return false;
		}

	_endState	+= ts._endState;

	if( ts._result ) {
		for( unsigned i=0; i < ts._result->_actions.size(); ++i ) {
			_currentActions->_actions.push_back( ts._result->_actions[i] );
			}
		}

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Action& node ) noexcept {
	return true;
	}

bool	Visitor::pre( const Oscl::UML::HSM::Entry& node ) noexcept {
	/*	An Entry is created when reaching a Target State.
		It is epected to contain a set of actions to be
		executed when entering a state.
	 */

	Oscl::UML::HSM::Generator::Compartment	ts( node._compartment._context );

	bool
	success	= node._compartment._parseTree.accept(ts);

	if( !success ) {

		_reporter.reportError(
			ts._description,
			ts._context,
			ts._position
			);

		return false;
		}

	_endState	+= ts._endState;

	if( _currentEntry ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": ERROR: Expected _currentEntry to be nullptr\n"
			;
		return false;
		}

	if( !ts._entry ) {
		return true;
		}

	_currentEntry	= ts._entry;

	_currentEntryPopulate->populate( _currentEntry );

	pushActionsPopulateEntry();

	return true;
	}

bool	Visitor::post( const Oscl::UML::HSM::Entry& node ) noexcept {

	popActionsPopulate();

	_currentEntry	= nullptr;

	return true;
	}

static bool	triggerTypeMatch(
				const std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&	triggers,
				const Oscl::UML::HSM::Generator::TriggerRecord*	trigger
				) {

	for( unsigned i=0; i < triggers.size(); ++i ) {

		if( trigger->_name != triggers[i]->_name ) {
			continue;
			}

		if( trigger->typeMatch( *triggers[i] ) ) {
			return true;
			}
		}

	return false;
	}

bool	Visitor::checkStateEvents() {

	for(
			StateRecord*	si	= _endState._stateList.first();
			si;
			si	= _endState._stateList.next( si )
		) {
		if( ! si->checkEvents() ) {
			return false;
			}
		}

	return true;
	}

bool	Visitor::buildUniqueTriggers() {

	for(
			StateRecord*	si	= _endState._stateList.first();
			si;
			si	= _endState._stateList.next( si )
		) {
		if( ! si->buildUniqueTriggers() ) {
			std::cerr
				<< __PRETTY_FUNCTION__
				<< "FAIL: "
				<< si->_name
				<< "\n"
				;
			return false;
			}

		for( unsigned i=0; i < si->_triggers.size(); ++i ) {

			if( triggerTypeMatch( _triggers, si->_triggers[i] ) ) {
				continue;
				}

			_triggers.push_back( si->_triggers[i] );
			}
		}

	return true;
	}

static bool	contextFuncMatch(
		const std::vector< const Oscl::UML::HSM::Generator::ContextFuncRecord* >&	contextFunctions,
		const Oscl::UML::HSM::Generator::ContextFuncRecord*							contextFunction
		) {

	for( unsigned i=0; i < contextFunctions.size(); ++i ) {

		auto	f	= contextFunctions[i];

		if( f->_name != contextFunction->_name ) {
			continue;
			}

		if( (! f->_args) && (! contextFunction->_args ) ) {
			return true;
			}

		if( (! f->_args ) || ( ! contextFunction->_args ) ) {
			continue;
			}

		auto	fargs = f->_args->_args;
		auto	gfargs = contextFunction->_args->_args;

		if( fargs.size() != gfargs.size() ) {
			continue;
			}

		if( !gfargs.size() ) {
			return true;
			}

		for( unsigned j=0; j < fargs.size(); ++j ) {
			if( fargs[j]->_parameter.typeMatch( gfargs[j]->_parameter ) ) {
				return true;
				}
			}
		}

	return false;
	}

bool	Visitor::buildUniqueGuardFunctions() {

	for(	auto g	= _endState._guardList.first();
			g;
			g	= _endState._guardList.next( g )
		) {

		auto gexp	= g->_guardExp;

		if( !gexp ) {
			std::cerr
				<< __PRETTY_FUNCTION__
				<< ": Guard without expression!\n"
				;
			continue;
			}
		for( unsigned j=0; j < gexp->_operand.size(); ++j) {
			auto	op = gexp->_operand[j];

			auto cf	= op->getContextFunc();

			if( !cf ) {
				auto arg	= op->getArg();

				if( !arg ) {
					continue;
					}
				cf	= arg->_contextFunc;
				if( !cf ) {
					continue;
					}
				}

			if( contextFuncMatch( _guards, cf ) ) {
				continue;
				}

			_guards.push_back( cf );
			}
		}

	return true;
	}

bool	Visitor::buildUniqueActionFunctions() {

	for(	auto a = _endState._actionList.first();
			a;
			a	= _endState._actionList.next( a )
		) {
		auto cf	= a->_contextFunc;
		if( !cf ) {
			continue;
			}
		if( contextFuncMatch( _uniqueActions, cf ) ) {
			continue;
			}
		_uniqueActions.push_back( cf );
		}

	return true;
	}

void	Visitor::printTransitionTree() {
	if( ! _reset ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _reset is nullptr\n"
			;
		}
	else {
		_reset->printTransitionTree( 0 );
		}

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		state->printTransitionTree( 0 );
		}
	}

void	Visitor::genStateCpp(
			std::ostream&	file
			) {

	// This is a bit of a hack
	file
		<< "#include \"hsm.h\"\n"
		<< "using namespace "
		;

	bool	first	= true;
	for(
		NamespaceRecord*	ns = _endState._namespaceList.first();
		ns;
		ns	=  _endState._namespaceList.next( ns )
		) {
		if( first ) {
			first	= false;
			}
		else {
			file << "::";
			}
		file << ns->_name;
		}

	file << ";\n";

	file
		<< "void\tStateVar::initialTransition() noexcept {\n"
		<< "\tContextApi&	context	= _context;\n"
		<< "\tStateVar&	stateVar	= *this;\n"
		;

	if( ! _reset ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _reset is nullptr\n"
			;
		}
	else {
		_reset->genStateCpp( file, 1 );
		}

	file
		<< "\t"
		<< "}\n"
		<< "\n"
		;

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		state->genStateCpp( file, 0 );
		}

	}

void	Visitor::genHsmHeader(
			std::ostream&		file,
			const std::string&	filename
			) {

	if( checkStateEvents() ) {
		std::cerr
			<< "visitor.checkStateEvents(): SUCCESS\n"
			;
		}
	else {
		std::cerr
			<< "visitor.checkStateEvents(): FAIL\n"
			;
		}

	buildUniqueTriggers();

	buildUniqueGuardFunctions();
	buildUniqueActionFunctions();

	// Header guard

	std::string	headerGuard;

	for(
			auto ns	= _endState._namespaceList.first();
			ns;
			ns	= _endState._namespaceList.next( ns )
		) {
		headerGuard += "_" + ns->_name;
		}

	headerGuard	+= "_" + filename + "_";

	std::transform(
		headerGuard.begin(),
		headerGuard.end(),
		headerGuard.begin(), 
		[](unsigned char c){
			if( c == '.' ) {
				return (int)'_';
				}
			return std::tolower(c);
			}
		);

	file
		<< "#ifndef " << headerGuard << "\n"
		<< "#define " << headerGuard << "\n"
		;

	// Include files
	for(
			auto include	= _endState._includeFileList.first();
			include;
			include	= _endState._includeFileList.next( include )
		) {
		file
			<< include->_name
			<< "\n"
			;
		}

	// open namespaces
	for(
			auto ns	= _endState._namespaceList.first();
			ns;
			ns	= _endState._namespaceList.next( ns )
		) {
		file
			<< "/** */\n"
			<< "namespace " <<  ns->_name << " {"
			<< "\n"
			;
		}

	// open StateVar
	file
		<< "/** */\n"
		<< "class StateVar {"
		<< "\n"
		;

	// open ContextApi
	file
		<< "\t"
		<< "public:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "class ContextApi {"
		<< "\n"
		<< "\t\t\t"
		<< "public:\n"
		;

	// guards

	for( unsigned i=0; i < _guards.size(); ++i) {
		auto	gf	= _guards[i];

		file
			<< "\t\t\t\t"
			<< "/** guard */\n"
			<< "\t\t\t\t"
			<< "virtual bool\t"
			<< gf->_name
			<< "("
			;

		for( unsigned j=0; gf->_args && j < gf->_args->_args.size(); ++j) {

			const Oscl::UML::Parser::Types::Parameter&
			p = gf->_args->_args[j]->_parameter;

			if( j != 0 ) {
				file
					<< ",\n"
					;
				}
			else {
				file
					<< "\n"
					;
				}

			file
				<< "\t\t\t\t\t\t\t\t\t"
				;

			for( unsigned k=0; k < p._type._qualifiers.size(); ++k ){
				if( k != 0 ) {
					file
						<< " "
						;
					}
				file
					<<	p._type._qualifiers[k]
					;
				}

			file
				<< " "
				;

			for( unsigned k=0; k < p._type._path.size(); ++k ){

				if( k != 0 ) {
					file
						<< "::"
						;
					}
				file
					<<	p._type._path[k]
					;
				}

			for( unsigned k=0; k < p._type._operators.size(); ++k ){
				file
					<<	p._type._operators[k]
					;
				}

			file
				<< "\t"
				<<	p._name
				;
			}

		file
			<< "\n\t\t\t\t\t\t\t\t\t"
			<< ")"
			<< "=0;"
			<< "\n"
			;
		}

	// actions

	std::cerr
		<< "Do actions\n"
		;

	for( unsigned i=0; i< _uniqueActions.size(); ++i ) {
		auto	action	= _uniqueActions[i];

		file
			<< "\t\t\t\t"
			<< "/** action */\n"
			<< "\t\t\t\t"
			<< "virtual void"
			<< "\t"
			<< action->_name
			<< "("
			;

		for( unsigned j=0; action->_args && j < action->_args->_args.size(); ++j) {
			const Oscl::UML::Parser::Types::Parameter&
			p = action->_args->_args[j]->_parameter;

			if( j != 0 ) {
				file
					<< ",\n"
					;
				}
			else {
				file
					<< "\n"
					;
				}

			file
				<< "\t\t\t\t\t\t\t\t\t"
				;

			for( unsigned k=0; k < p._type._qualifiers.size(); ++k ){
				if( k != 0 ) {
					file
						<< " "
						;
					}
				file
					<<	p._type._qualifiers[k]
					;
				}

			file
				<< " "
				;

			for( unsigned k=0; k < p._type._path.size(); ++k ){
				if( k != 0 ) {
					file
						<< "::"
						;
					}
				file
					<<	p._type._path[k]
					;
				}

			for( unsigned k=0; k < p._type._operators.size(); ++k ){
				file
					<<	p._type._operators[k]
					;
				}

			file
				<< "\t"
				<<	p._name
				;
			}

		file
			<< "\n\t\t\t\t\t\t\t\t\t"
			<< ")"
			<< "=0;"
			<< "\n"
			;
		}

	// close ContextApi
	file
		<< "\t\t\t};"
		<< "\n"
		;

	// open base State
	file
		<< "\t"
		<< "protected:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "class State {"
		<< "\n"
		<< "\t\t\t"
		<< "public:"
		<< "\n"
		;

	writeStateTriggers(
			file,
			_triggers,
			"virtual",
			""
			);

	file
		<< "\t\t\t"
		<< "public:"
		<< "\n"
		<< "\t\t\t\t"
		<< "/** */\n"
		<< "\t\t\t\t"
		<< "virtual void exit( ContextApi& context, const StateVar& stateVar, const State* root ) const noexcept;\n"
		<< "\n"
		;

	// close base State
	file
		<< "\t\t\t"
		<< "};"
		<< "\n\n"
		<< "\t\t/** */\n"
		<< "\t\tfriend class State;"
		<< "\n\n"
		;

	// Sub States

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {

		// open sub State
		file
			<< "\t"
			<< "private:\n"
			<< "\t\t/** */\n"
			<< "\t\tclass " << state->_name << ": public " << state->_parent << " {\n"
			<< "\t\t\tpublic:\n"
			;

		writeStateTriggers(
				file,
				state->_triggers,
				"",
				"override"
				);

	file
		<< "\t\t\t"
		<< "public:"
		<< "\n"
		<< "\t\t\t\t"
		<< "/** */\n"
		<< "\t\t\t\t"
		<< "void exit( ContextApi& context, const StateVar& stateVar, const State* root ) const noexcept override;\n"
		<< "\n"
		;
		// close sub State
		file
			<< "\t\t\t};"
			<< "\n\n"
			<< "\t\t/** */\n"
			<< "\t\tfriend class " << state->_name << ";\n"
			<< "\n"
			;
		}

	// Fly-Weight States
	/*	We make the fly-weight states public
		since they are const and useful for
		testing using currentState().
	 */

	file
		<< "\t"
		<< "public:\n"
		;

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		file
			<< "\t\t/** */\n"
			<< "\t\tconst static " << state->_name << "\t_" << state->_name << ";\n"
			<< "\n"
			;
		}

	// Member variables
	file
		<< "\t"
		<< "private:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "ContextApi&\t_context;\n"
		<< "\n"
		<< "\t"
		<< "protected:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "const State*\t_state;\n"
		<< "\n"
		;

	// Constructors/destructors
	file
		<< "\t"
		<< "public:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "StateVar(\n"
		<< "\t\t\t"
		<< "ContextApi&\t\t\tcontext\n"
		<< "\t\t\t"
		<< ") noexcept;\n"
		<< "\n"
		;

	// HSM initialize member function
	file
		<< "\t\t"
		<< "/** This operation must be called once to initialize the HSM at run-time. */\n"
		<< "\t\t"
		<< "void	initialTransition() noexcept;\n"
		<< "\n"
		;

	// HSM state accessor
	file
		<< "\t\t"
		<< "/**\tThis operation can be used to access the current state.\n"
		<< "\t\t\tKnowing the current state is useful for testing.\n"
		<< "\t\t */\n"
		<< "\t\t"
		<< "const State*	currentState() const noexcept;\n"
		<< "\n"
		;


	// events
	file
		<< "\t"
		<< "public:\n"
		;

	writeEventTriggers(
		file,
		_triggers
		);

	// state changers
	file
		<< "\t"
		<< "private:\n"
		;

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		file
			<< "\t\t/** */\n"
			<< "\t\tvoid	changeTo" << state->_name << "() noexcept;\n"
			<< "\n"
			;
		}

	// exit state
	file
		<< "\t"
		<< "private:\n"
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "void exit( const State* root ) const noexcept;\n"
		<< "\n"
		;

	// protocol error

	file
		<< "\t"
		<< "private:\n"
		;

	file
		<< "\t\t"
		<< "/** */\n"
		<< "\t\t"
		<< "void	protocolError() noexcept;\n"
		<< "\n"
		;

	// close StateVar
	file
		<< "\t"
		<< "};"
		<< "\n"
		;

	// close namespaces
	for(
			auto ns	= _endState._namespaceList.first();
			ns;
			ns	= _endState._namespaceList.next( ns )
		) {
		file
			<< "}"
			<< "\n"
			;
		}

	// close header guard
	file << "#endif\n";
	}

void	Visitor::genHsmCpp(
			std::ostream&		file
			) {

	if( checkStateEvents() ) {
		std::cerr
			<< "checkStateEvents(): SUCCESS\n"
			;
		}
	else {
		std::cerr
			<< "checkStateEvents(): FAIL\n"
			;
		}

	buildUniqueTriggers();

	buildUniqueGuardFunctions();
	buildUniqueActionFunctions();

	// include
	file
		<< "#include \"hsm.h\"\n"
		<< "\n"
		;

	// using namespaces
	std::string	usingNamespace;

	bool	first	= true;
	for(
			auto ns	= _endState._namespaceList.first();
			ns;
			ns	= _endState._namespaceList.next( ns )
		) {
		if( !first ) {
			usingNamespace += "::";
			}
		else {
			first	= false;
			}
		usingNamespace += ns->_name;
		}

	file
		<< "using namespace "
		<< usingNamespace
		<< ";\n"
		<< "\n"
		;

	// Fly-Weight declarations
	file
		<< "//--------------- Concrete Fly-Weight States ----------------\n"
		;

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		file
			<< "const StateVar::" << state->_name
			<< "\t"
			<< "StateVar::_" << state->_name << ";\n"
			;
		}

	file
		<< "\n"
		;

	// StateVar constructor
	file
		<< "//--------------- StateVar constructor ----------------\n"
		<< "\n"
		<< "StateVar::StateVar(\n"
		<< "\t"
		<< "ContextApi&		context\n"
		<< "\t"
		<< ") noexcept:\n"
		<< "\t\t"
		<< "_context( context )\n"
		<< "\t\t"
		<< "{\n"
		<< "\t"
		<< "}\n"
		<< "\n"
		;

	file
		<< "const StateVar::State*\tStateVar::currentState() const noexcept {\n"
		<< "\treturn _state;\n"
		<< "\t"
		<< "}\n"
		<< "\n"
		;


	// exit
	file
		<< "void StateVar::exit( const State* root ) const noexcept{\n"
		<< "\t"
		<< "_state->exit( _context, *this, root );\n"
		<< "\t}\n"
		<< "\n"
		;

	// events
	file
		<< "//--------------- StateVar events ----------------\n"
		;

	writeEventTriggerImplementations(
			file,
			_triggers
			);

	// state changers
	file
		<< "//--------------- StateVar state-change operations ----------------\n"
		;

	for(
		auto state	= _endState._stateList.first();
		state;
		state	= _endState._stateList.next( state )
		) {
		file
			<< "void	StateVar::changeTo" << state->_name << "() noexcept {\n"
			<< "\t"
			<< "_state	= &_" << state->_name << ";\n"
			<< "\t"
			<< "}\n"
			<< "\n"
			;
		}

	// State default event behavior
	file
		<< "//--------------- State ----------------\n"
		;

	writeStateTriggerImplementations(
		file,
		_triggers
		);

	file
		<< "void\tStateVar::State::exit( ContextApi& context, const StateVar& stateVar, const State* root ) const noexcept {\n"
		<< "\tif( root == nullptr ) {\n"
		<< "\t\treturn;\n"
		<< "\t\t}\n"
		<< "\t"
		<< "// We have a code generation error if this ever happens.\n"
		<< "\t"
		<< "for(;;);\n"
		<< "\t"
		<< "}\n"
		;

	}
