/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "endstate.h"

using namespace Oscl::UML::HSM::Generator;

EndState::~EndState() {

	{
		EventRecord*	r;
		while( ( r = _eventList.get() ) ) {
			delete r;
			}
	}

	{
		TriggerRecord*	r;
		while( ( r = _triggerList.get() ) ) {
			delete r;
			}
	}

	{
		ContextFuncRecord*	r;
		while( ( r = _contextFuncList.get() ) ) {
			delete r;
			}
	}

	{
		ContextArgsRecord*	rec;

		while( ( rec = _contextArgsList.get() ) ) {
			delete rec;
			}
	}

	{
		ContextArgRecord*	rec;

		while( ( rec = _contextArgList.get() ) ) {
			delete rec;
			}
	}

	{
		ArgsRecord*	rec;

		while( ( rec = _argsList.get() ) ) {
			delete rec;
			}
	}

	{
		ArgRecord*	rec;

		while( ( rec = _argList.get() ) ) {
			delete rec;
			}
	}

	{
		GuardRecord*	r;
		while( ( r = _guardList.get() ) ) {
			delete r;
			}
	}

	{
		GuardExpRecord*	r;
		while( ( r = _guardExpList.get() ) ) {
			delete r;
			}
	}

	{
		GroupRecord*	r;
		while( ( r = _groupList.get() ) ) {
			delete r;
			}
	}

	{
		ActionsRecord*	r;
		while( ( r = _actionsList.get() ) ) {
			delete r;
			}
	}

	{
		ActionRecord*	r;
		while( ( r = _actionList.get() ) ) {
			delete r;
			}
	}

	{
	VarFuncRecord*	r;
	while( ( r = _varFuncList.get() ) ) {
		delete r;
		}
	}

	{
	VarExpRecord*	r;
	while( ( r = _varExpList.get() ) ) {
		delete r;
		}
	}

	{
		EntryRecord*	r;
		while( ( r = _entryList.get() ) ) {
			delete r;
			}
	}

	{
		ExitRecord*	r;
		while( ( r = _exitList.get() ) ) {
			delete r;
			}
	}

	{
		BinOperatorRecord*	r;
		while( ( r = _binOpList.get() ) ) {
			delete r;
			}
	}

	{
		BinOperandRecord*	r;
		while( ( r = _binOperandList.get() ) ) {
			delete r;
			}
	}

	{
		ChoiceRecord*	r;
		while( ( r = _choiceList.get() ) ) {
			delete r;
			}
	}

	{
		EventsRecord*	r;
		while( ( r = _eventsList.get() ) ) {
			delete r;
			}
	}

	{
		ParametersRecord*	r;
		while( ( r = _parametersList.get() ) ) {
			delete r;
			}
	}

	{
		ResetRecord*	r;
		while( ( r = _resetList.get() ) ) {
			delete r;
			}
	}

	{
		NamespaceRecord*	r;
		while( ( r = _namespaceList.get() ) ) {
			delete r;
			}
	}

	{
		IncludeFileRecord*	r;
		while( ( r = _includeFileList.get() ) ) {
			delete r;
			}
	}

	{
		StateRecord*	r;
		while( ( r = _stateList.get() ) ) {
			delete r;
			}
	}

	{
		ChoicesRecord*	r;
		while( ( r = _choicesList.get() ) ) {
			delete r;
			}
	}

	{
		TargetRecord*	r;
		while( ( r = _targetList.get() ) ) {
			delete r;
			}
	}
	}

void	EndState::operator += ( EndState& other ) noexcept {
	_resetList			+= other._resetList;
	_eventList			+= other._eventList;
	_triggerList		+= other._triggerList;
	_contextFuncList	+= other._contextFuncList;
	_contextArgsList	+= other._contextArgsList;
	_contextArgList		+= other._contextArgList;
	_guardList			+= other._guardList;
	_actionsList		+= other._actionsList;
	_actionList			+= other._actionList;
	_varFuncList		+= other._varFuncList;
	_entryList			+= other._entryList;
	_exitList			+= other._exitList;
	_argsList			+= other._argsList;
	_argList			+= other._argList;
	_guardExpList		+= other._guardExpList;
	_groupList			+= other._groupList;
	_varExpList			+= other._varExpList;
	_binOpList			+= other._binOpList;
	_binOperandList		+= other._binOperandList;
	_choiceList			+= other._choiceList;
	_eventsList			+= other._eventsList;
	_parametersList		+= other._parametersList;
	_namespaceList		+= other._namespaceList;
	_stateList			+= other._stateList;
	_choicesList		+= other._choicesList;
	_targetList			+= other._targetList;

	}

