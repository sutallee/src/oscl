/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_choiceh_
#define _oscl_uml_hsm_generator_cpp_choiceh_

#include <vector>
#include <string>
#include "action.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** This visitor extracts all of the guards and
	actions from a single node of parsed UML
	guard text.
 */
class Choice : public Action {
	public:
		/*	Without these using elements the
			new compiler (gcc (GCC) 13.2.1 20231205)
			generates warnings:
			hidden [-Werror=overloaded-virtual=]
		 */
		using Action::pre;
		using Action::post;
	private:
		/** */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_parameters;

		/** */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_globalParameters;

	public:
		/** */
		std::vector< ActionRecord* >	_actions;

	protected:
		/** */
		GuardRecord*	_currentGuard;

		/** */
		GuardExpRecord*	_currentGuardExp;

		/** */
		GroupRecord*	_currentGroup;

		/** */
		VarFuncRecord*		_currentVarFunc;

		/** */
		VarExpRecord*		_currentVarExp;

		/** */
		BinOperandRecord*	_currentBinOperand;

		/** */
		ChoiceRecord*		_currentChoice;

	public:
		/** */
		ChoiceRecord*		_result;

	public:
		/** */
		Choice(
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	parameters,
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters,
			void*				context	= nullptr
			);

		/** */
		~Choice();

	private:
		/** */
		void	populateResult( ChoiceRecord* r );

		/** */
		void	populateChoice( ActionsRecord* r );

	private:
		/** */
		void	pushChoicePopulateResult();

		/** */
		void	pushActionsPopulateChoice();

	private:
		/** */
		void	populateBinOperand( ContextFuncRecord* r );

		/** */
		void	pushContextFuncPopulateBinOperand();

	private:
		/** */
		void	populateBinOperand( ArgRecord* r );

		/** */
		void	pushArgPopulateBinOperand();

	private:
		/** */
		void	populateBinOperand( VarExpRecord* r );

		/** */
		void	pushVarExpPopulateBinOperand();

	private:
		/** */
		void	populateChoice( GuardRecord* r );

		/** */
		void	pushGuardPopulateChoice();

	private:
		/** */
		void	populateVarFunc( ArgsRecord* r );

		/** */
		void	pushArgsPopulateVarFunc();

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

	public:
		/** override */
		bool	pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;

		/** override */
		bool	post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;
	};

}
}
}
}

#endif
