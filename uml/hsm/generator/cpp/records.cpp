/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include "records.h"

using namespace Oscl::UML::HSM::Generator;

static const std::string&	buildIndention( std::string& s, unsigned indent ) {
	for( unsigned i=0; i < indent ; ++i ) {
		s	+= "\t";
		}

	return s;
	}

ResetRecord::ResetRecord():
	_transition( nullptr )
	{
	}

ResetRecord::~ResetRecord() {
	}

void	ResetRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "InitialReset {\n"
		;

	buildIndention( tabs, 1 );

	if( !_transition ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< "Reset : No transition.\n"
			;

		std::cout
			<< tabs
			<< "}\n"
			;
		return;
		}

	_transition->printTransitionTree( indent + 1, "Reset" );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	ResetRecord::genStateCpp(
			std::ostream& file,
			unsigned indent
			) {

	std::cout
		<< __PRETTY_FUNCTION__
		<< "\n"
		;

	std::string	tabs;

	buildIndention( tabs, indent );

	buildIndention( tabs, 1 );

	if( !_transition ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< "Reset : No transition.\n"
			;
		return;
		}

	std::vector< std::string >	actions;

	_transition->genStateCpp( file, indent , actions );
	}

ParametersRecord::ParametersRecord()
	{
	}

ParametersRecord::~ParametersRecord() {
	}

void	ParametersRecord::print( unsigned indent ) {
	for( unsigned i=0; i < _parameters.size(); ++i ) {
		if( i != 0 ) {
			std::cout
				<< ", "
				;
			}
		std::cout
			<< _parameters[i];
			;
		}
	}

void	ParametersRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			){

	std::ostringstream	oss;

	for( unsigned i=0; i < _parameters.size(); ++i ) {
		if( i != 0 ) {
//			s	+= ", ";
			oss	<< ", ";
			}
//		s	+= _parameters[i]._name;
		oss	<< _parameters[i];
		}

		std::string	p	= oss.str();
		s	+= p;
	}

TriggerRecord::TriggerRecord(
		const std::string&	name,
		void*				context,
		unsigned			position
		):
	_name( name ),
	_parameters( nullptr ),
	_context( context ),
	_position( position )
	{}

TriggerRecord::~TriggerRecord()
	{}

bool	TriggerRecord::operator == ( const TriggerRecord& other) const {
	if( _name != other._name ) {
		return false;
		}

	if( ! _parameters && ! other._parameters ) {
		return true;
		}

	if( !_parameters || !other._parameters) {
		return false;
		}

	if( _parameters->_parameters.size() != other._parameters->_parameters.size() ) {
		return false;
		}

	for( unsigned i=0; i < _parameters->_parameters.size(); ++i ) {
		if( _parameters->_parameters[i] == other._parameters->_parameters[i] ) {
			continue;
			}
		return false;
		}

	return true;
	}

bool	TriggerRecord::typeMatch( const TriggerRecord& other) const {

	if( !_parameters && !other._parameters ) {
		return true;
		}

	if( ! _parameters ) {
		return false;
		}

	if( ! other._parameters ) {
		return false;
		}

	if( _parameters->_parameters.size() != other._parameters->_parameters.size() ) {
		return false;
		}

	for( unsigned i=0; i < _parameters->_parameters.size(); ++i ) {
		if( _parameters->_parameters[i].typeMatch( other._parameters->_parameters[i] ) ) {
			continue;
			}
		return false;
		}

	return true;
	}

bool	TriggerRecord::resolveType( const std::string& var, std::string& type ) const {

	if( ! _parameters ) {
		return false;
		}

	for( unsigned i=0; i < _parameters->_parameters.size(); ++i ) {

		if( _parameters->_parameters[i]._name == var ) {
			std::ostringstream	stream;
			stream
				<< _parameters->_parameters[i]
				;
			type	= stream.str();
			return true;
			}
		}

	return false;
	}

bool	TriggerRecord::resolveType( const std::string& var, Oscl::UML::Parser::Types::Type& type ) const {

	if( ! _parameters ) {
		return false;
		}

	for( unsigned i=0; i < _parameters->_parameters.size(); ++i ) {

		if( _parameters->_parameters[i]._name == var ) {
			type	= _parameters->_parameters[i]._type;
			return true;
			}
		}

	return false;
	}

bool	TriggerRecord::resolveType(
				const std::string&						var,
				Oscl::UML::Parser::Types::Parameter&	parameter
				) const {

	if( ! _parameters ) {
		return false;
		}

	for( unsigned i=0; i < _parameters->_parameters.size(); ++i ) {

		if( _parameters->_parameters[i]._name == var ) {
			parameter	= _parameters->_parameters[i];
			return true;
			}
		}

	return false;
	}

void	TriggerRecord::print( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Trigger: "
		<< _name
		<< "("
		;

	if( _parameters ) {
		_parameters->print( indent + 1 );
		}

	std::cout
		<< ")"
		<< "\n"
		;
	}

void	TriggerRecord::genStateCpp(
			std::ostream& file,
			unsigned indent
			){
	std::string	tabs;

	buildIndention( tabs, indent );

	file
		<< _name
		<< "( "
		<< "ContextApi& context, "
		<< "StateVar& stateVar"
		;

	std::string	s;
	if( _parameters ) {
		_parameters->genStateCpp( s, indent + 1 );
		}

	if( s.size() ) {
		file
			<< ", "
			<<	s
			<< " "
			;
		}

	file
		<< ")"
		;
	}

ContextFuncRecord::ContextFuncRecord( const std::string& name ):
	_name( name ),
	_args( nullptr )
	{
	}

ContextFuncRecord::~ContextFuncRecord() {
	}

bool	ContextFuncRecord::checkVars( const ParametersRecord* parameters ) const {

	if( !_args ) {
		return true;
		}

	if( ! _args->_args.size() ) {
		return true;
		}

	if( !parameters ) {
		return false;
		}

	const auto cargs = _args->_args;

	for ( unsigned i=0; i < cargs.size(); ++i ) {

		bool	match	= false;

		for( unsigned j=0; j < parameters->_parameters.size(); ++j ) {
			if( parameters->_parameters[j]._name == cargs[i]->_parameter._name ) {
				match	= true;
				break;
				}
			}

		if( !match ) {
			std::cerr
				<< __PRETTY_FUNCTION__
				<< ": No parameter match for action var: "
				<< cargs[i]->_parameter._name
				<< "_expression: "
				<< _expression
				<< "\n"
				;
			return false;
			}
		}

	return true;
	}

void	ContextFuncRecord::print( unsigned indent ) {

	std::string	tabs;
	buildIndention( tabs, indent );

	std::cout
		<< _name
		<< "("
		;

	if( _args ) {
		_args->print( indent );
		}

	std::cout
		<< ")"
		;
	}

void	ContextFuncRecord::genStateCpp(
			std::string&	s,
			unsigned indent
			) {

	std::string	tabs;
	buildIndention( tabs, indent );

	s	+= _name + "(";

	if( _args ) {
		_args->genStateCpp( s, indent );
		}

	s	+= ")";
	}

void	ContextArgsRecord::print( unsigned indent ) {
	for( unsigned i=0; i < _args.size(); ++i ) {
		if( i != 0 ) {
			std::cout
				<< ", "
				;
			}
		_args[i]->print( indent );
		}
	}

void	ContextArgsRecord::genStateCpp(
			std::string&		s,
			unsigned			indent
			) {

	for( unsigned i=0; i < _args.size(); ++i ) {
		if( i != 0 ) {
			s	+= ", ";
			}
		_args[i]->genStateCpp( s, indent );
		}
	}

VarFuncRecord::VarFuncRecord(
	const std::vector< std::string >&	vars,
	const std::vector< std::string >&	refs
	) :
	_args( nullptr )
	{
	for( unsigned i=0; i < vars.size(); ++i ) {
		if( i!= 0 ) {
			_name	+= refs[ i - 1 ];
			_refs.push_back( refs[ i - 1 ] );
			}
		_name	+= vars[i];
		_vars.push_back( vars[i] );
		}
	}

VarFuncRecord::~VarFuncRecord() {
	}

void	VarFuncRecord::print( unsigned indent ) {

	for( unsigned i=0; i < _vars.size(); ++i ) {
		if( i != 0 ) {
			std::cout	<< _refs[i -1 ];
			}
		std::cout
			<< _vars[i]
			;
		}

	std::cout
		<< "("
		;

	if( _args ) {
		_args->print( indent + 1 );
		}

	std::cout
		<< ")"
		;
	}

void	VarFuncRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {

	for( unsigned i=0; i < _vars.size(); ++i ) {
		if( i != 0 ) {
			s += _refs[i -1 ];
			}
		s	+= _vars[i];
		}

	s	+= "(";

	if( _args ) {
		_args->genStateCpp( s, indent + 1 );
		}

	s	+= ")";
	}

ArgsRecord::ArgsRecord() {
	}

ArgsRecord::~ArgsRecord() {
	}

void	ArgsRecord::print( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	for( unsigned i=0; i < _args.size(); ++i ) {
		if( i != 0 ) {
			std::cout
				<< ","
				;
			}
		_args[i]->print( indent + 1 );
		}
	}

void	ArgsRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {
	std::string	tabs;

	buildIndention( tabs, indent );

	for( unsigned i=0; i < _args.size(); ++i ) {
		if( i != 0 ) {
			s	+= ", ";
			}
		_args[i]->genStateCpp( s, indent + 1 );
		}
	}

ArgRecord::ArgRecord():
	_contextFunc( nullptr ),
	_varFunc( nullptr ),
	_varExp( nullptr )
	{
	}

ArgRecord::~ArgRecord() {
	}

void	ArgRecord::print( unsigned indent ) {

	if( _contextFunc ) {
		_contextFunc->print( indent );
		return;
		}
	else if( _varFunc ) {
		_varFunc->print( indent );
		return;
		}
	else if( _varExp ) {
		_varExp->print( indent );
		return;
		}

	std::string	tabs;
	std::cerr
		<< buildIndention( tabs, indent )
		<<	__PRETTY_FUNCTION__
		<< "No argument!\n"
		;
	}

void	ArgRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {

	if( _contextFunc ) {
		s += "context.";
		_contextFunc->genStateCpp( s, indent );
		return;
		}
	else if( _varFunc ) {
		_varFunc->genStateCpp( s, indent );
		return;
		}
	else if( _varExp ) {
		_varExp->genStateCpp( s, indent );
		return;
		}

	std::string	tabs;
	std::cerr
		<<	__PRETTY_FUNCTION__
		<< "No argument!\n"
		;
	}

VarExpRecord::VarExpRecord(
		const std::vector< std::string >&	vars,
		const std::vector< std::string >&	refs
		) {
	for( unsigned i=0; i<vars.size(); ++i ) {
		_vars.push_back( vars[i] );
		}
	for( unsigned i=0; i<refs.size(); ++i ) {
		_refs.push_back( refs[i] );
		}
	}

VarExpRecord::~VarExpRecord() {
	}

void VarExpRecord::appendToExpression( std::string& expression ) const {

	for( unsigned i=0; i<_vars.size(); ++i ){
		if( i != 0 ) {
			expression += _refs[ i - 1 ];
			}
		expression += _vars[ i ];
		}
	}

void	VarExpRecord::print( unsigned indent ) {

	for( unsigned i=0; i < _vars.size(); ++i ) {
		if( i != 0 ) {
			std::cout
				<<	_refs[ i - 1 ]
				;
			}
		std::cout
			<< _vars[i]
			;
		}
	}

void	VarExpRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {

	for( unsigned i=0; i < _vars.size(); ++i ) {
		if( i != 0 ) {
			s	+= _refs[ i - 1 ];
				;
			}
		s	+= _vars[i];
		}
	}

LiteralRecord::LiteralRecord() {
	}

LiteralRecord::~LiteralRecord() {
	}

void	LiteralRecord::print( unsigned indent ) {
	std::cout
		<< _literal
		;
	}

void	LiteralRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {
	s	+= _literal;
	}

BinOperandRecord::BinOperandRecord():
	_arg( nullptr ),
	_varExp( nullptr ),
	_contextFunc( nullptr ),
	_literal( nullptr )
	{
	}
BinOperandRecord::~BinOperandRecord(){
	}

void	BinOperandRecord::print( unsigned indent ) {
	/*
	class ArgRecord*		_arg;
	class VarExpRecord*		_varExp;
	class ContextFuncRecord*		_contextFunc;
	class LiteralRecord*		_literal;
	*/

	if( _arg ) {
		_arg->print( indent );
		return;
		}
	else if( _varExp ) {
		_varExp->print( indent );
		return;
		}
	else if( _contextFunc ) {
		_contextFunc->print( indent );
		return;
		}
	else if( _literal ) {
		_literal->print( indent );
		return;
		}

	std::string	tabs;
	buildIndention( tabs, indent );
	std::cerr
		<< tabs
		<< __PRETTY_FUNCTION__
		<< ": Missing operand.\n"
		;

	}

void	BinOperandRecord::genStateCpp(
			std::string&				s,
			unsigned					indent
			) {

	if( _arg ) {
		_arg->genStateCpp( s, indent );
		return;
		}
	else if( _varExp ) {
		_varExp->genStateCpp( s, indent );
		return;
		}
	else if( _contextFunc ) {
		s += "context.";
		_contextFunc->genStateCpp( s, indent );
		return;
		}
	else if( _literal ) {
		_literal->genStateCpp( s, indent );
		return;
		}

	std::cerr
		<< __PRETTY_FUNCTION__
		<< ": Missing operand.\n"
		;

	}

ContextFuncRecord*	BinOperandRecord::getContextFunc() {
	return _contextFunc;
	}

ArgRecord*	BinOperandRecord::getArg() {
	return _arg;
	}

GuardExpRecord::GuardExpRecord()
	{
	}

GuardExpRecord::~GuardExpRecord() {
	}

void	GuardExpRecord::print( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "["
		;
	for( unsigned i=0; i < _operand.size(); ++i ) {
		_operand[i]->print( indent );
		if( i < _operator.size() ) {
			_operator[i]->print( indent );
			}
		}
	std::cout
		<< "]\n"
		;
	}

void	GuardExpRecord::expression(
			std::string&	s
			) {

	for( unsigned i=0; i < _operand.size(); ++i ) {

		_operand[i]->genStateCpp( s, 0 );

		if( i < _operator.size() ) {
			_operator[i]->genStateCpp( s, 0 );
			}
		}
	}

void	GuardExpRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent
			) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::string	s;

	expression( s );

	if( s == "else" ) {
		file
			<< "else"
			;
		}
	else {
		file
			<< "if( "
			<< s
			<< " )"
			;
		}

	}

/////////
GroupRecord::GroupRecord():
	_exp( nullptr ) {
	}

GroupRecord::~GroupRecord() {
	}

void	GroupRecord::print( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "("
		;

	_exp->print(indent);

	std::cout
		<< ")\n"
		;
	}

bool	GroupRecord::set( GuardExpRecord* exp ) {

	if( _exp != nullptr ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_exp is NOT nullptr\n"
			;
		return false;
		}

	_exp	= exp;

	return true;
	}

void	GroupRecord::expression(
			std::string&	s
			) {
	s += "( ";
	_exp->expression( s );
	s += " )";
	}

void	GroupRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {
	expression( s );
	}

ContextFuncRecord*	GroupRecord::getContextFunc() {
	return nullptr;
	}

ArgRecord*	GroupRecord::getArg() {
	return nullptr;
	}

////////

#if 0
ExpRecord::ExpRecord() {
	}

ExpRecord::~ExpRecord() {
	}
#endif

GuardRecord::GuardRecord():
	_guardExp( nullptr )
	{
	}

GuardRecord::~GuardRecord() {
	}

bool	GuardRecord::checkVars( const ParametersRecord* parameters ) const {

	if( !parameters && _vars.size() ) {
		return false;
		}

	for ( unsigned i=0; i < _vars.size(); ++i ) {

		bool	match	= false;

		for( unsigned j=0; j < parameters->_parameters.size(); ++j ) {
			if( parameters->_parameters[j]._name == _vars[i] ) {
				match	= true;
				break;
				}
			}

		if( !match ) {
			if( _vars[i] != "else" ) {
				std::cerr
					<< "No parameter match for guard var: "
					<< _vars[i]
					<< "\n"
					;
				return false;
				}
			}
		}

	return true;
	}

void	GuardRecord::print( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	if( ! _guardExp ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< "_guardExp is nullptr\n"
			;
		return;
		}
	_guardExp->print( indent + 1);

	}

void	GuardRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent
			) {
	std::string	tabs;

	buildIndention( tabs, indent );

	if( ! _guardExp ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_guardExp is nullptr\n"
			;
		return;
		}

	_guardExp->genStateCpp( file, indent );
	}

ActionsRecord::ActionsRecord():
	_transition( nullptr )
	{
	}

ActionsRecord::~ActionsRecord(){
	}

void	ActionsRecord::printTransitionTree( unsigned indent, bool expectTransition ) {

	std::string	tabs;

	std::cerr
		<< buildIndention( tabs, indent )
		<< "Actions {\n"
		;

	buildIndention( tabs, 1 );

	for( unsigned i=0; i < _actions.size(); ++i ) {
		std::cout
			<< tabs
			;
		_actions[i]->print( indent + 1 );
		std::cout
			<< "\n"
			;
		}

	if( !_transition ) {
		if( !expectTransition ) {
			std::cout
				<< tabs
				<< "}\n"
				;
			return;
			}
		std::cerr
			<< tabs
			<< "Actions: No _transition\n"
			<< tabs
			<< "}\n"
			;
		return;
		}

	_transition->printTransitionTree( indent + 1, "Actions" );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	ActionsRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			) {

	std::string	tabs;
	buildIndention( tabs, indent );

	for( unsigned i=0; i < _actions.size(); ++i ) {
//		std::string	action	= tabs;
		std::string	action;
		_actions[i]->genStateCpp( action, indent );
		action	+= ";\n";
		actions.push_back( action );
		}

	if( !_transition ) {
		return;
		}

	_transition->genStateCpp( file, indent , actions );
	}


ActionRecord::ActionRecord( ):
	_contextFunc( nullptr ),
	_varFunc( nullptr ),
	_varExp( nullptr ),
	_choices( nullptr )
	{
	}

ActionRecord::~ActionRecord() {
	}

bool	ActionRecord::checkVars( const ParametersRecord* parameters ) const {

	if( _contextFunc ) {
		return _contextFunc->checkVars( parameters );
		}
	else if( _varFunc ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _varFunc without checkVars\n"
			;
		return false;
		}
	else if( _varExp ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _varExp without checkVars\n"
			;
		return false;
		}

	return true;
	}

void	ActionRecord::print( unsigned indent ) {

	if( _contextFunc ) {
		return _contextFunc->print( indent );
		}
	else if( _varFunc ) {
		return _varFunc->print( indent );
		}
	else if( _varExp ) {
		return _varExp->print( indent );
		}

	std::string	tabs;
	std::cerr
		<< buildIndention( tabs, indent )
		<<	__PRETTY_FUNCTION__
		<< ": no actions!\n"
		;
	}

void	ActionRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {

	if( _contextFunc ) {
		s += "context.";
		return _contextFunc->genStateCpp( s, indent );
		}
	else if( _varFunc ) {
		return _varFunc->genStateCpp( s, indent );
		}
	else if( _varExp ) {
		return _varExp->genStateCpp( s, indent );
		}

	std::cerr
		<<	__PRETTY_FUNCTION__
		<< ": no actions!\n"
		;
	}


ChoiceRecord::ChoiceRecord( void* context ):
	_context( context ),
	_guard( nullptr ),
	_actions( nullptr ),
	_transition( nullptr )
	{
	}

ChoiceRecord::~ChoiceRecord() {
	}

void	ChoiceRecord::print( unsigned indent ) {
	std::string	tabs;
	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Choice {\n"
		;

	buildIndention( tabs, 1 );

	if( _guard ) {
		_guard->print( indent + 1);
		}

	if( _actions ) {
		_actions->printTransitionTree( indent + 1 );
		}

	if( _transition ) {
		_transition->printTransitionTree( indent + 1, "Choice" );
		}

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	ChoiceRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			){
	std::string	tabs;
	buildIndention( tabs, indent );

	file
		<< tabs
		;

	if( _guard ) {
		_guard->genStateCpp( file, indent );
		}

	file
		<< " {\n"
		;

	if( _actions ) {
		_actions->genStateCpp( file, indent , actions );
		}

	if( _transition ) {
		_transition->genStateCpp( file, indent + 1, actions );
		}
	else {
		for( unsigned i=0; i < actions.size(); ++i ) {
			file
				<< tabs
				<< "\t"
				<< actions[i]
				;
			}
		file
			<< tabs
			<< "\treturn;\n"
			;
		}

	file
		<< tabs
		<< "\t}\n"
		;
	}

ChoicesRecord::ChoicesRecord( )
	{
	}

ChoicesRecord::~ChoicesRecord() {
	}

void	ChoicesRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;
	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Choices {\n"
		;

	buildIndention( tabs, 1 );

	for( unsigned i=0; i < _choices.size(); ++i ) {
		_choices[i]->print( indent + 1);
		}

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	ChoicesRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			) {

	for( unsigned i=0; i < _choices.size(); ++i ) {
		if( ! _choices[i]->_guard ) {
			continue;
			}
		if( !_choices[i]->_guard->_guardExp ) {
			continue;
			}

		std::string	gexp;

		_choices[i]->_guard->_guardExp->expression( gexp );

		std::vector< std::string >	act = actions;

		if( gexp != "else" ) {
			_choices[i]->genStateCpp( file, indent , act );
			}
		}

	for( unsigned i=0; i < _choices.size(); ++i ) {

		if( ! _choices[i]->_guard ) {
			continue;
			}
		if( !_choices[i]->_guard->_guardExp ) {
			continue;
			}

		std::string	gexp;

		_choices[i]->_guard->_guardExp->expression( gexp );

		std::vector< std::string >	act = actions;

		if( gexp == "else" ) {
			_choices[i]->genStateCpp( file, indent , act );
			}
		}
	}

EventRecord::EventRecord(
	void*	context
	):
	_trigger( nullptr ),
	_choice( nullptr ),
	_actions( nullptr ),
	_transition( nullptr ),
	_context( context )
	{}

EventRecord::~EventRecord() {
	}

void	EventRecord::set( ActionsRecord* actions ) {
	if( _actions ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _actions is NOT nullptr.\n"
			;
		}

	if( _choice ) {
		if( !_choice->_choices.empty() ) {
			std::cerr
				<< __PRETTY_FUNCTION__
				<< ": _choice->_choices is NOT empty.\n"
				;
			}
		}

	_actions	= actions;
	}

void	EventRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	std::cout
		<< buildIndention( tabs, indent )
		<< "Event {"
		<< "\n"
		;

	buildIndention( tabs, 1 );

	if( !_trigger ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< "_trigger is nullptr\n"
			;
		}
	else {
		_trigger->print( indent + 1 );
		}

	if( _choice ) {
		_choice->printTransitionTree( indent + 1 );
		}

	if( _actions ) {
		_actions->printTransitionTree( indent + 1 );
		}

	if( ! _transition ) {
		std::cout
			<< tabs
			<< "Event: No transition\n"
			;
		std::cout
			<< tabs
			<< "}\n"
			;
		return;
		}

	_transition->printTransitionTree( indent + 1, "Event" );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	EventRecord::genStateCpp(
			std::ostream&		file,
			unsigned			indent,
			const std::string&	stateName,
			const std::string&	parentName
			){

	if( !_trigger ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "_trigger is nullptr\n"
			;
		return;
		}

	std::string	tabs;

	buildIndention( tabs, indent +1 );

	file
		<< "void\tStateVar::"
		<< stateName
		<< "::"
		;

	_trigger->genStateCpp( file, indent + 1 );

	file
		<< "const noexcept {\n"
		;

	std::vector< std::string >	actions;

	if( _choice && _choice->_choices.size() ) {
		_choice->genStateCpp( file, indent + 1, actions );
		}

	if( _actions && _actions->_actions.size() ) {
		_actions->genStateCpp( file, indent + 1, actions );
		if( !_transition ) {
			for( unsigned i=0; i < actions.size(); ++i ) {
				file
					<< tabs
					<< actions[i]
					;
				}
			file
				<< tabs
				<< "return;\n"
				;
			}
		}

	if( _transition ) {
		_transition->genStateCpp( file, indent + 1, actions );
		}

	if( parentName.size() ) {
		file
			<< tabs
			<< parentName
			<< "::"
			<< _trigger->_name
			<< "( context, stateVar "
			;
		if( _trigger->_parameters ) {
			auto& parameters	= _trigger->_parameters->_parameters;
			for( unsigned i=0; i < parameters.size(); ++i ) {
				file
					<< ", "
					;
				file
					<< parameters[i]._name
					;
				}
			}
		file
			<< " );\n"
			;
		}

	file
		<< tabs
		<< "}\n"
		<< "\n"
		;
	}

EntryRecord::EntryRecord(
	void*	context
	):
	_actions( nullptr ),
	_context( context )
	{}

EntryRecord::~EntryRecord() {
	}

void EntryRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Entry {\n"
		;

	buildIndention( tabs, 1 );

	if( !_actions ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< ": No _actions record\n"
			;
		return;
		}

	_actions->printTransitionTree( indent + 1, false );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	EntryRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			){
	std::string	tabs;

	buildIndention( tabs, indent );

	if( !_actions ) {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< ": No _actions record\n"
			;
		return;
		}

	_actions->genStateCpp( file, indent + 1, actions );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

ExitRecord::ExitRecord(
	void*	context
	):
	_context( context )
	{}

ExitRecord::~ExitRecord() {
	}

StateRecord::StateRecord(
		Oscl::UML::HSM::Generator::Reporter&			reporter,
		const std::string&	name,
		const std::string&	parent,
		void*				context
		):
	_reporter( reporter ),
	_name( name ),
	_parent( parent ),
	_events( nullptr ),
	_entry( nullptr ),
	_exit( nullptr ),
	_context( context )
	{}

void	StateRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "State "
		<< _name
		<< " {\n"
		;

	if( _events ) {
		_events->printTransitionTree( indent + 1 );
		}
	if( _entry ) {
		_entry->printTransitionTree( indent + 1 );
		}
	if( _exit ) {
		//_events->printTransitionTree( indent + 1 );
		}

	buildIndention( tabs, 1 );

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	StateRecord::genStateCpp(
				std::ostream& file,
				unsigned indent
				){

	file
		<< "void\tStateVar::" << _name << "::exit( ContextApi& context, const StateVar& stateVar, const State* root ) const noexcept {\n"
		;

	file
		<< "\tconst State*	me = &StateVar::_"
		<< _name
		<< ";\n"
		<< "\tif( root == me ) {\n"
		<< "\t\treturn;\n"
		<< "\t\t}\n"
		;

	if( _exit ) {
		if( _exit->_actions ) {
			auto& actions	= _exit->_actions->_actions;
			for( unsigned i=0; i < actions.size(); ++i ) {
				std::string	s;
				actions[i]->genStateCpp( s, 0 );
				file
					<< "\t"
					<< s
					<< ";\n"
					;
				}
			}
		}

	file
		<< "\t" << _parent << "::exit( context, stateVar, root );\n"
		;
	file
		<< "\t}\n"
		<< "\n"
		;

	if( !_events ) {
		return;
		}

	_events->genStateCpp( file, indent, _name, _parent );
	}

bool	StateRecord::checkArguments() {

	if( !_events ) {
		/*	A State without events may
			be semantically incorrect, but
			it may be relying on entry/exit
			actions and/or parent state events.
			So this is not really a failure.
		 */
		return true;
		}

	std::vector< Oscl::UML::HSM::Generator::EventRecord* >&
	events	= _events->_events;

	for(
		unsigned	i = 0;
		i < events.size();
		++i
		) {

		auto	er	= events[i];

		if( !er->_trigger ) {
			continue;
			}

		if( er->_choice ) {

			auto& choices	= er->_choice->_choices;

			for( unsigned j=0; j < choices.size(); ++j ){

				auto guard	= choices[j]->_guard;

				if( !guard ) {
					continue;
					}

				ParametersRecord* parameters	= er->_trigger->_parameters;

				if( !parameters ){
					if( ! guard->checkVars( nullptr ) ) {
						return false;
						}
					}
				else if( ! guard->checkVars( parameters ) ) {
					return false;
					}
				}
			}

		const auto&	actions	= er->_actions;

		if( actions ) {
			const auto&	action	= actions->_actions;

			for( unsigned aj=0; aj < action.size() ; ++aj ) {

				if( ! action[aj]->checkVars( er->_trigger->_parameters ) ){
					return false;
					}
				}
			}
		}

	return true;
	}

bool	StateRecord::checkTriggers() {

	if( !_events ) {
		/*	A State without events may
			be semantically incorrect, but
			it may be relying on entry/exit
			actions and/or parent state events.
			So this is not really a failure.
		 */
		return true;
		}

	std::vector< Oscl::UML::HSM::Generator::EventRecord* >&
	events	= _events->_events;

	for(
		unsigned	i = 0;
		i < events.size();
		++i
		) {

		auto	er	= events[i];

		if( !er->_trigger ) {
			continue;
			}
		for(
			unsigned	j = 0;
			j < events.size();
			++j
			) {
			auto	other	= events[j];
			if( er == other ) {
				continue;
				}
			if( !er->_trigger ) {
				continue;
				}
			if( !other->_trigger ) {
				continue;
				}
			TriggerRecord*	tr	= er->_trigger;
			TriggerRecord*	otr	= other->_trigger;

			if( tr->_name != otr->_name ) {
				continue;
				}

			if( tr->_parameters != otr->_parameters ) {
				continue;
				}

			if( tr->_parameters ) {
				// There are no parameters
				continue;
				}

			auto tparameters	= tr->_parameters;

			auto oparameters	= otr->_parameters;

			if( tparameters->_parameters.size() != oparameters->_parameters.size() ) {
				continue;
				}
			for( unsigned i=0; i < tparameters->_parameters.size(); ++i ) {
				if( tparameters->_parameters[i].typeMatch( oparameters->_parameters[i]) ) {
					if( ! ( tparameters->_parameters[i] == oparameters->_parameters[i] ) ) {
						// Types match but names do not match
						// All triggers with the same parameter type signature
						// must have parameter names that match.
						std::cerr
							<< "Trigger \""
							<< tr->_name
							<< "\" has parameter \""
							<< tparameters->_parameters[i]
							<< "\" that does not match \""
							<< oparameters->_parameters[i]
							<< "\".\n"
							;
						const char*	description = "Trigger parameters names must match when the types are the same.";

						_reporter.reportError(
							description,
							tr->_context,
							tr->_position
							);
						_reporter.reportError(
							description,
							otr->_context,
							otr->_position
							);
						return false;
						}
					}
				else {
					// Different trigger types
					break;
					}
				}
			}
		}

	return true;
	}

bool	StateRecord::checkEvents() {

	if( !checkTriggers() ) {
		return false;
		}

	if( !checkArguments() ) {
		return false;
		}

	return true;
	}

static bool	triggerMatch(
				const std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >&	triggers,
				const Oscl::UML::HSM::Generator::TriggerRecord*	trigger
				) {

	for( unsigned i=0; i < triggers.size(); ++i ) {

		if( trigger->_name != triggers[i]->_name ) {
			continue;
			}

		if( *trigger == *triggers[i] ) {
			return true;
			}
		}

	return false;
	}

static EventRecord*	eventMatch(
				std::vector< Oscl::UML::HSM::Generator::EventRecord* >&	events,
				const Oscl::UML::HSM::Generator::EventRecord*	event
				) {

	for( unsigned i=0; i < events.size(); ++i ) {

		if( !event->_trigger ) {
			if( !events[i]->_trigger ) {
				continue;
				}
			return nullptr;
			}

		if( event->_trigger->_name != events[i]->_trigger->_name ) {
			continue;
			}

		std::cout
			<< __PRETTY_FUNCTION__
			<< "Consider merging "
			<< event->_trigger->_name
			<< " and "
			<< events[i]->_trigger->_name
			;

		if( *event->_trigger == *events[i]->_trigger ) {
			std::cout
				<< ": MATCHED\n"
				;
			return events[i];
			}

		std::cout
			<< ": FAILED\n"
			;
		}

	return nullptr;
	}

bool	StateRecord::buildUniqueTriggers() {

	if( !_events ) {
		/*	A state without events may
			depend on super-state events,
			so this is not necessarily a failure.
		 */
		return true;
		}

	std::vector< Oscl::UML::HSM::Generator::EventRecord* >&

	events	= _events->_events;

	for(
		unsigned	i = 0;
		i < events.size();
		++i
		) {

		auto er	= events[i];

		if( !er->_trigger ) {
			continue;
			}

		if( triggerMatch( _triggers, er->_trigger ) ) {
			continue;
			}

		_triggers.push_back( er->_trigger );
		}

	return true;
	}

void	EventsRecord::merge( std::vector< Oscl::UML::HSM::Generator::EventRecord* >& events) {

	for( unsigned i=0; i < _events.size(); ++i ) {

		auto er	= _events[i];

		if( !er->_trigger ) {
			continue;
			}

		auto	e	= eventMatch( events, er );

		if( e ) {
			/*	This event has the same trigger
				signature as an existing trigger (e).
				Now we want to merge er into e.
			 */
			std::cout
				<< __PRETTY_FUNCTION__
				<< ": Merging event\n"
				;

			if(
					( e->_actions && !er->_actions )
				||	( !e->_actions && er->_actions )
				) {
				std::cerr
					<< __PRETTY_FUNCTION__
					<< ": ERROR e->_actions(" << e->_actions << ")"
					<< "and er->_actions(" << er->_actions << ")"
					<< " are not both nullptr!\n"
					;
				}

			if( er->_actions && !e->_actions ) {
				e->_actions	= er->_actions;
				}
			else if( er->_actions && e->_actions ) {
				for( unsigned j=0; j < er->_actions->_actions.size(); ++j ) {
					e->_actions->_actions.push_back( er->_actions->_actions[j] );
					}
				}

			for( unsigned j=0; j < er->_choice->_choices.size(); ++j ) {
				e->_choice->_choices.push_back( er->_choice->_choices[j] );
				}

			continue;
			}

		events.push_back( er );
		}
	}

void	EventsRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Events {\n"
		;

	buildIndention( tabs, 1 );

	for( unsigned i=0; i < _events.size(); ++i ) {
		_events[i]->printTransitionTree( indent + 1 );
		}

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	EventsRecord::genStateCpp(
				std::ostream&		file,
				unsigned			indent,
				const std::string&	stateName,
				const std::string&	parentName
				){

	std::vector< Oscl::UML::HSM::Generator::EventRecord* >	events;

	merge( events );

	for( unsigned i=0; i < events.size(); ++i ) {
		events[i]->genStateCpp( file, indent, stateName, parentName );
		}

	}

ContextArgRecord::ContextArgRecord( const std::string& name ):
	_expression( name )
	{
	}

void	ContextArgRecord::print( unsigned indent ) {
	std::cout
		<< _parameter._name
		;
	}

void	ContextArgRecord::genStateCpp(
			std::string&		s,
			unsigned			indent
			) {
	s	+= _parameter._name;
	}

BinOperatorRecord::BinOperatorRecord( const std::string& name ):
	_operator( name )
	{
	}

BinOperatorRecord::~BinOperatorRecord() {
	}

void BinOperatorRecord::print( unsigned indent ) {
	std::cout
		<< " "
		<< _operator
		<< " "
		;
	}

void	BinOperatorRecord::genStateCpp(
			std::string&	s,
			unsigned		indent
			) {
	s	+= " ";
	s	+= _operator;
	s	+= " ";
	}

NamespaceRecord::NamespaceRecord(
		const std::string&	name
		):
	_name( name )
	{
	}

IncludeFileRecord::IncludeFileRecord(
		const std::string&	name
		):
	_name( name )
	{
	}

TransitionRecord::TransitionRecord():
	_actions( nullptr ),
	_target( nullptr ),
	_choices( nullptr )
	{
	}

TransitionRecord::~TransitionRecord(){
	}

bool	TransitionRecord::allNull() {
		if( _actions || _target || _choices ) {
			return false;
			}
		return true;
		}

void	TransitionRecord::set( ActionsRecord* actions ) {
	if( !allNull() ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": ERROR attempt to set non-null record."
			<< "_actions: " << _actions
			<< ",_target: " << _target
			<< ",_choices: " << _choices
			<< "\n"
			;
		}
	_actions	= actions;
	}

void	TransitionRecord::set( TargetRecord* target ) {
	if( !allNull() ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": ERROR attempt to set non-null record."
			<< "_actions: " << _actions
			<< ",_target: " << _target
			<< ",_choices: " << _choices
			<< "\n"
			;
		}
	_target	= target;
	}

void	TransitionRecord::set( ChoicesRecord* choices ) {
	if( !allNull() ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": ERROR attempt to set non-null record."
			<< "_actions: " << _actions
			<< ",_target: " << _target
			<< ",_choices: " << _choices
			<< "\n"
			;
		}
	_choices	= choices;
	}

void TransitionRecord::printTransitionTree( unsigned indent, const std::string& source ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Transition {"
		<< "\n"
		;

	buildIndention( tabs, 1 );

	if( _actions ) {
		_actions->printTransitionTree( indent + 1 );
		}
	else if( _target ) {
		_target->printTransitionTree( indent + 1 );
		}
	else if( _choices ) {
		_choices->printTransitionTree( indent + 1 );
		}
	else {
		std::cerr
			<< tabs
			<< __PRETTY_FUNCTION__
			<< "\": No actions, target or choices!\n"
			;
		}

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	TransitionRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			) {
//	std::string	tabs;

//	buildIndention( tabs, indent );

	if( _actions ) {
		_actions->genStateCpp( file, indent, actions );
		}
	else if( _target ) {
		_target->genStateCpp( file, indent, actions );
		}
	else if( _choices ) {
		_choices->genStateCpp( file, indent, actions );
		}
	else {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< "\": No actions, target or choices!\n"
			;
		}
	}

TargetRecord::TargetRecord(
	const std::string&	statePath,
	const std::string&	lcaPath,
	bool				hasSourceState
	):
	_statePath( statePath ),
	_lcaPath( lcaPath ),
	_hasSourceState( hasSourceState )
	{
	}

TargetRecord::~TargetRecord(){
	}

void TargetRecord::printTransitionTree( unsigned indent ) {
	std::string	tabs;

	buildIndention( tabs, indent );

	std::cout
		<< tabs
		<< "Target "
		<< _statePath
		<< " {\n"
		;

	buildIndention( tabs, 1 );

	for( unsigned i=0; i < _entry.size(); ++i ) {
		_entry[i]->printTransitionTree( indent + 1 );
		}

	std::cout
		<< tabs
		<< "}\n"
		;
	}

void	TargetRecord::genStateCpp(
			std::ostream&				file,
			unsigned					indent,
			std::vector< std::string >&	actions
			) {
	std::string	tabs;

	buildIndention( tabs, indent );

	///// Exit Actions

	std::cout
		<< __PRETTY_FUNCTION__
		<< ": _statePath: "
		<< _statePath
		<< ", _lcaPath: "
		<< _lcaPath
		<< "\n"
		;

	if( _hasSourceState ) {
		file
			<< tabs
			<< "// Exit actions\n"
			;

		if( _lcaPath.size() ) {
			file
				<< tabs
				<< "stateVar.exit( &StateVar::_"
				<< _lcaPath
				<< " );\n"
				;
			}
		else {
			file
				<< tabs
				<< "stateVar.exit( nullptr );\n"
				;
			}
		}

	///// Transition Actions
	file
		<< tabs
		<< "// Transition actions\n"
		;

	for( unsigned i=0; i < actions.size(); ++i ) {
		file
			<< tabs
			<< actions[i]
			;
		}

	///// Entry Actions
	std::vector< std::string >	entryActions;

	for( unsigned i=0; i < _entry.size(); ++i ) {
		_entry[i]->genStateCpp( file, indent + 1, entryActions );
		}

	file
		<< tabs
		<< "// Entry actions\n"
		;

	for( unsigned i=0; i < entryActions.size(); ++i ) {
		file
			<< tabs
			<< entryActions[i]
			;
		}

	///// State change
	file
		<< tabs
		<< "stateVar.changeTo"
		<< _statePath
		<< "();\n"
		<< tabs
		<< "return;\n"
		;
	}
