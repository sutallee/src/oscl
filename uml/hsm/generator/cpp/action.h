/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_actionh_
#define _oscl_uml_hsm_generator_cpp_actionh_

#include <vector>
#include <string>
#include "oscl/uml/parser/transition/visitor.h"
#include "endstate.h"
#include "recstack.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {

/** This visitor extracts actions from a single node
	of parsed UML action text.
 */
class Action : public Oscl::UML::Parser::Transition::Visitor {
	public:
		/*	Without these using elements the
			new compiler (gcc (GCC) 13.2.1 20231205)
			generates warnings:
			hidden [-Werror=overloaded-virtual=]
		 */
		using Oscl::UML::Parser::Transition::Visitor::pre;
		using Oscl::UML::Parser::Transition::Visitor::post;
	private:
		/**	A list of parameters that are available
			to the actions.
		 */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_parameters;

	public:
		/** */
		EndState	_endState;

	protected:
		/** */
		RecordStack	_stacks;

	public:
		/** */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_globalParameters;

	protected:
		/** */
		ActionsRecord*	_currentActions;

		/** */
		ActionRecord*	_currentAction;

		/** */
		ContextFuncRecord*	_currentContextFunc;

		/** */
		ContextArgsRecord*	_currentContextArgs;

		/** */
		ArgsRecord*			_currentArguments;

		/** */
		ArgRecord*			_currentArgument;

		/** */
		Populate< ActionsRecord >*	_currentActionsPopulate;

		/** */
		Populate< ContextFuncRecord >*	_currentContextFuncPopulate;

		/** */
		Populate< ChoiceRecord >*	_currentChoicePopulate;

		/** */
		Populate< EventRecord >*	_currentEventPopulate;

		/** */
		Populate< EventsRecord >*	_currentEventsPopulate;

		/** */
		Populate< ParametersRecord >*	_currentParametersPopulate;

		/** */
		Populate< GuardRecord >*	_currentGuardPopulate;

		/** */
		Populate< ArgRecord >*	_currentArgPopulate;

		/** */
		Populate< VarExpRecord >*	_currentVarExpPopulate;

		/** */
		Populate< VarFuncRecord >*	_currentVarFuncPopulate;

		/** */
		Populate< ArgsRecord >*	_currentArgsPopulate;

	public:
		/** */
		void*	const	_context;

		/** */
		const char*		_description;

		/** */
		unsigned		_position;

		/** */
		ActionsRecord*	_result;

	public:
		/** */
		Action(
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	parameters	= nullptr,
			void*				context	= nullptr,
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters = nullptr
			);

		/** */
		~Action();

		/** RETURN true if a parameter with the name arg
			is in the parameter list.
		 */
		virtual bool	isValidArgument(
							const std::string&	arg
							) const;

		/** RETURN true if a parameter with the name arg
			is in the parameter list and copies the matching
			parameter into the parameter variable.
		 */
		virtual bool	resolveType(
							const std::string&						arg,
							Oscl::UML::Parser::Types::Parameter&	parameter
							) const;

	private:
		/** */
		void	populateResult( ActionsRecord* r );

		/** */
		void	populateAction( ContextFuncRecord* r );

		/** */
		void	populateArgument( ContextFuncRecord* r );

		/** */
		void	populateArgument( VarFuncRecord* r );

		/** */
		void	populateArgument( VarExpRecord* r );

	private:
		/** */
		void	pushContextFuncPopulateAction();

		/** */
		void	pushContextFuncPopulateArgument();

		/** */
		void	pushActionsPopulateResult();

	private:
		/** */
		void	pushVarFuncPopulateArgument();

	private:
		/** */
		void	pushVarExpPopulateArgument();

	protected:
		/** */
		void	pushActionsPopulate( Populate< ActionsRecord >* p);

		/** */
		void	popActionsPopulate();

	protected:
		/** */
		void	pushContextFuncPopulate( Populate< ContextFuncRecord >* p);

		/** */
		void	popContextFuncPopulate();

	protected:
		/** */
		void	pushVarExpPopulate( Populate< VarExpRecord >* p);

		/** */
		void	popVarExpPopulate();

	protected:
		/** */
		void	pushVarFuncPopulate( Populate< VarFuncRecord >* p);

		/** */
		void	popVarFuncPopulate();

	protected:
		/** */
		void	pushArgPopulate( Populate< ArgRecord >* p);

		/** */
		void	popArgPopulate();

	protected:
		/** */
		void	pushArgsPopulate( Populate< ArgsRecord >* p);

		/** */
		void	popArgsPopulate();

	private:
		/** */
		void	populateArgs( ArgRecord* r );

		/** */
		void	pushArgPopulateArgs();

	protected:
		/** */
		void	pushChoicePopulate( Populate< ChoiceRecord >* p);

		/** */
		void	popChoicePopulate();

	protected:
		/** */
		void	pushGuardPopulate( Populate< GuardRecord >* p);

		/** */
		void	popGuardPopulate();

	protected:
		/** */
		void	pushEventPopulate( Populate< EventRecord >* p);

		/** */
		void	popEventPopulate();

	protected:
		/** */
		void	pushEventsPopulate( Populate< EventsRecord >* p);

		/** */
		void	popEventsPopulate();

	protected:
		/** */
		void	popParameterPopulate();

	protected:
		/** */
		void	pushParametersPopulate( Populate< ParametersRecord >*	p );

		/** */
		void	popParametersPopulate();

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Actions& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Actions& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Action& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Action& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Argument& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Argument& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;
	};

}
}
}
}

#endif
