/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <sstream>
#include "action.h"

using namespace Oscl::UML::HSM::Generator;

Action::Action(
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	parameters,
		void*				context,
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters
		):
	_parameters( parameters ),
	_globalParameters( globalParameters ),
	_currentActions( nullptr ),
	_currentAction( nullptr ),
	_currentContextFunc( nullptr ),
	_currentContextArgs( nullptr ),
	_currentArguments( nullptr ),
	_currentArgument( nullptr ),
	_currentActionsPopulate( nullptr ),
	_currentContextFuncPopulate( nullptr ),
	_currentChoicePopulate( nullptr ),
	_currentEventPopulate( nullptr ),
	_currentEventsPopulate( nullptr ),
	_currentParametersPopulate( nullptr ),
	_currentGuardPopulate( nullptr ),
	_currentArgPopulate( nullptr ),
	_currentVarExpPopulate( nullptr ),
	_currentVarFuncPopulate( nullptr ),
	_currentArgsPopulate( nullptr ),
	_context( context ),
	_description( "Unknown error" ),
	_position( 0 ),
	_result( nullptr )
	{
	pushActionsPopulateResult();
	}


Action::~Action() {
	popActionsPopulate();
	}

bool	Action::isValidArgument(
			const std::string&	arg
			) const {

	if( _parameters ) {
		for( unsigned i=0; i < _parameters->size(); ++i ) {

			if( (*_parameters)[i]._name == arg ) {
				return true;
				}
			}
		}

	if( _globalParameters ) {
		for( unsigned i=0; i < _globalParameters->size(); ++i ) {

			if( (*_globalParameters)[i]._name == arg ) {
				return true;
				}
			}
		}

	/** Special case for the else key-word */
	if( arg == "else" ) {
		return true;
		}

	return false;
	}

bool	Action::resolveType(
			const std::string&						var,
			Oscl::UML::Parser::Types::Parameter&	parameter
			) const {

	if( _parameters ) {
		for( unsigned i=0; i < _parameters->size(); ++i ) {

			if( (*_parameters)[i]._name == var ) {
				parameter	= (*_parameters)[i];
				return true;
				}
			}
		}

	if( _globalParameters ) {
		for( unsigned i=0; i < _globalParameters->size(); ++i ) {

			if( (*_globalParameters)[i]._name == var ) {
				parameter	= (*_globalParameters)[i];
				return true;
				}
			}
		}

	return false;
	}

bool	Action::pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	}

bool	Action::post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	}

bool	Action::pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept {

	/*
		A ContextFunc node can be a part of
		several nodes:
			* BinOperand
			* Action
			* Argument
	 */

	if( _currentContextFunc ) {
		_stacks._contextFuncStack.push( _currentContextFunc );
		}

	_currentContextFunc	= new ContextFuncRecord( node._name );

	_currentContextFuncPopulate->populate( _currentContextFunc );

	pushContextFuncPopulateArgument();

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept {

	popContextFuncPopulate();

	/*	A ContextFunc can generally appear as:
			Action
			BinOperand
			GuardExpr
			Arg
		However, for an Action transition, it can only appear as an Action
	 */

	if( _currentAction ) {
		_currentAction->_contextFunc	= _currentContextFunc;
		}

	_endState._contextFuncList.put( _currentContextFunc );

	_currentContextFunc	= _stacks._contextFuncStack.pop();

	return true;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept {

	if( _currentContextArgs ) {
		_stacks._contextArgsStack.push( _currentContextArgs );
		}

	_currentContextArgs	= new ContextArgsRecord();

	_currentContextFunc->_args	= _currentContextArgs;

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept {

	_endState._contextArgsList.put( _currentContextArgs );

	_currentContextArgs	= _stacks._contextArgsStack.pop();

	return true;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept {

	ContextArgRecord*
	arg	= new ContextArgRecord( node._name );

	_endState._contextArgList.put( arg );

	_currentContextArgs->_args.push_back( arg );

	/*	We need to resolve the argument
		to parameters.
	 */

	bool
	success	= resolveType(
				node._name,
				arg->_parameter
				);

	if(! success ) {
		_description	= "Unable to resolve parameter.";
		_position		= node._position;
		return false;
		}

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept {

	return true;
	};

void	Action::populateResult( ActionsRecord* r ) {
	if( _result ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _result is NOT nullptr\n"
			;
		}
	_result	= r;
	}

void	Action::populateAction( ContextFuncRecord* r ) {
	if( ! _currentAction ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentAction is nullptr\n"
			;
		}
	_currentAction->_contextFunc	= r;
	}

void	Action::populateArgument( ContextFuncRecord* r ) {
	if( ! _currentArgument ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentArgument is nullptr\n"
			;
		}
	_currentArgument->_contextFunc	= r;
	}

void	Action::populateArgument( VarFuncRecord* r ) {
	if( ! _currentArgument ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentArgument is nullptr\n"
			;
		}
	_currentArgument->_varFunc	= r;
	}

void	Action::populateArgument( VarExpRecord* r ) {
	if( ! _currentArgument ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentArgument is nullptr\n"
			;
		}
	_currentArgument->_varExp	= r;
	}


void	Action::populateArgs( ArgRecord* r ) {
	if( ! _currentArguments ) {
		std::cerr
			<< __PRETTY_FUNCTION__
			<< ": _currentAction is nullptr\n"
			;
		}
	_currentArguments->_args.push_back( r );
	}

void	Action::pushActionsPopulate( Populate< ActionsRecord >*  p) {

	if( _currentActionsPopulate ) {
		_stacks._actionsPopStack.push( _currentActionsPopulate );
		}

	_currentActionsPopulate	= p;
	}

void	Action::pushActionsPopulateResult() {

	pushActionsPopulate(
		new PopulateComposer< Action, ActionsRecord >(
				*this,
				&Action::populateResult
				)
		);

	}

void	Action::popActionsPopulate() {

	delete _currentActionsPopulate;

	_currentActionsPopulate	= _stacks._actionsPopStack.pop();
	}

void	Action::pushVarExpPopulate( Populate< VarExpRecord >*	p ) {

	if( _currentVarExpPopulate ) {
		_stacks._varExpPopStack.push( _currentVarExpPopulate );
		}

	_currentVarExpPopulate	= p;
	}

void	Action::popVarExpPopulate() {

	delete _currentVarExpPopulate;

	_currentVarExpPopulate	= _stacks._varExpPopStack.pop();
	}

void	Action::pushVarFuncPopulate( Populate< VarFuncRecord >*	p ) {

	if( _currentVarFuncPopulate ) {
		_stacks._varFuncPopStack.push( _currentVarFuncPopulate );
		}

	_currentVarFuncPopulate	= p;
	}

void	Action::popVarFuncPopulate() {

	delete _currentVarFuncPopulate;

	_currentVarFuncPopulate	= _stacks._varFuncPopStack.pop();
	}

void	Action::pushArgPopulateArgs() {

	pushArgPopulate(
		new PopulateComposer< Action, ArgRecord >(
				*this,
				&Action::populateArgs
				)
		);

	}

void	Action::pushArgPopulate( Populate< ArgRecord >*	p ) {

	if( _currentArgPopulate ) {
		_stacks._argPopStack.push( _currentArgPopulate );
		}

	_currentArgPopulate	= p;
	}

void	Action::popArgPopulate() {

	delete _currentArgPopulate;

	_currentArgPopulate	= _stacks._argPopStack.pop();
	}

void	Action::pushArgsPopulate( Populate< ArgsRecord >*	p ) {

	if( _currentArgsPopulate ) {
		_stacks._argsPopStack.push( _currentArgsPopulate );
		}

	_currentArgsPopulate	= p;
	}

void	Action::popArgsPopulate() {

	delete _currentArgsPopulate;

	_currentArgsPopulate	= _stacks._argsPopStack.pop();
	}

void	Action::pushContextFuncPopulate( Populate< ContextFuncRecord >*	p ) {

	if( _currentContextFuncPopulate ) {
		_stacks._contextFuncPopStack.push( _currentContextFuncPopulate );
		}

	_currentContextFuncPopulate	= p;
	}

void	Action::pushContextFuncPopulateAction() {

	pushContextFuncPopulate(
		new PopulateComposer< Action, ContextFuncRecord >(
				*this,
				&Action::populateAction
				)
		);
	}

void	Action::pushContextFuncPopulateArgument() {

	pushContextFuncPopulate(
		new PopulateComposer< Action, ContextFuncRecord >(
				*this,
				&Action::populateArgument
				)
		);
	}

void	Action::pushVarFuncPopulateArgument() {

	pushVarFuncPopulate(
		new PopulateComposer< Action, VarFuncRecord >(
				*this,
				&Action::populateArgument
				)
		);
	}

void	Action::pushVarExpPopulateArgument() {

	pushVarExpPopulate(
		new PopulateComposer< Action, VarExpRecord >(
				*this,
				&Action::populateArgument
				)
		);
	}

void	Action::popContextFuncPopulate() {

	delete _currentContextFuncPopulate;

	_currentContextFuncPopulate	= _stacks._contextFuncPopStack.pop();
	}

void	Action::pushChoicePopulate( Populate< ChoiceRecord >*	p ) {

	if( _currentChoicePopulate ) {
		_stacks._choicePopStack.push( _currentChoicePopulate );
		}

	_currentChoicePopulate	= p;
	}

void	Action::popChoicePopulate() {

	delete _currentChoicePopulate;

	_currentChoicePopulate	= _stacks._choicePopStack.pop();
	}

void	Action::pushGuardPopulate( Populate< GuardRecord >*	p ) {

	if( _currentGuardPopulate ) {
		_stacks._guardPopStack.push( _currentGuardPopulate );
		}

	_currentGuardPopulate	= p;
	}

void	Action::popGuardPopulate() {

	delete _currentGuardPopulate;

	_currentGuardPopulate	= _stacks._guardPopStack.pop();
	}

void	Action::pushEventPopulate( Populate< EventRecord >*	p ) {

	if( _currentEventPopulate ) {
		_stacks._eventPopStack.push( _currentEventPopulate );
		}

	_currentEventPopulate	= p;
	}

void	Action::popEventPopulate() {

	delete _currentEventPopulate;

	_currentEventPopulate	= _stacks._eventPopStack.pop();
	}

void	Action::pushEventsPopulate( Populate< EventsRecord >*	p ) {

	if( _currentEventsPopulate ) {
		_stacks._eventsPopStack.push( _currentEventsPopulate );
		}

	_currentEventsPopulate	= p;
	}

void	Action::popEventsPopulate() {

	delete _currentEventsPopulate;

	_currentEventsPopulate	= _stacks._eventsPopStack.pop();
	}

void	Action::pushParametersPopulate( Populate< ParametersRecord >*	p ) {

	if( _currentParametersPopulate ) {
		_stacks._parametersPopStack.push( _currentParametersPopulate );
		}

	_currentParametersPopulate	= p;
	}

void	Action::popParametersPopulate() {

	delete _currentParametersPopulate;

	_currentParametersPopulate	= _stacks._parametersPopStack.pop();
	}

bool	Action::pre( const Oscl::UML::Parser::Transition::Actions& node ) noexcept {

	if( _currentActions ) {
		_stacks._actionsStack.push( _currentActions );
		}

	_currentActions	= new ActionsRecord();

	_currentActionsPopulate->populate( _currentActions );

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Actions& node ) noexcept {

	_endState._actionsList.put( _currentActions );

	_currentActions	= _stacks._actionsStack.pop();

	return true;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Action& node ) noexcept {

	if( _currentAction ) {
		_stacks._actionStack.push( _currentAction );
		}

	_currentAction	= new ActionRecord();

	pushContextFuncPopulateAction();

	return true;
	}

bool	Action::post( const Oscl::UML::Parser::Transition::Action& node ) noexcept {

	popContextFuncPopulate();

	_currentActions->_actions.push_back( _currentAction );

	_endState._actionList.put( _currentAction );

	_currentAction	= _stacks._actionStack.pop();

	return true;
	}

bool	Action::pre( const Oscl::UML::Parser::Transition::Argument& node ) noexcept {

	if( _currentArgument ) {
		_stacks._argumentStack.push( _currentArgument );
		}

	_currentArgument	= new ArgRecord();

	_currentArgPopulate->populate( _currentArgument );

	pushContextFuncPopulateArgument();
	pushVarFuncPopulateArgument();
	pushVarExpPopulateArgument();

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Argument& node ) noexcept {

	popVarExpPopulate();
	popVarFuncPopulate();
	popContextFuncPopulate();

	if( _currentArguments ) {
		_currentArguments->_expression	+= _currentArgument->_expression;
		}

	_currentArgument	= _stacks._argumentStack.pop();

	return true;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept {

	if( _currentArguments ) {
		_stacks._argumentsStack.push( _currentArguments );
		}

	_currentArguments	= new ArgsRecord();

	pushArgPopulateArgs();

	_currentArgsPopulate->populate( _currentArguments );

	return true;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept {

	popArgPopulate();

	_endState._argsList.put( _currentArguments );

	_currentArguments	= _stacks._argumentsStack.pop();

	return true;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Group& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Event& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Events& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept {
	std::cerr
		<< __PRETTY_FUNCTION__
		<< "Unhandled StringLiteral\n"
		;
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept {
	std::cerr
		<< __PRETTY_FUNCTION__
		<< "Unhandled CharLiteral\n"
		;
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept {
	std::cerr
		<< __PRETTY_FUNCTION__
		<< "Unhandled NumberLiteral\n"
		;
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

bool	Action::post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept {
	_description	= __PRETTY_FUNCTION__;
	_position		= node._position;
	return false;
	};

