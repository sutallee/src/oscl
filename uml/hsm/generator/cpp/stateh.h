/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_generator_cpp_statehh_
#define _oscl_uml_hsm_generator_cpp_statehh_

#include <string>
#include <vector>

#include "oscl/uml/hsm/trace.h"
#include "oscl/uml/parser/types/trigger.h"
#include "oscl/uml/parser/types/parameter.h"
#include "oscl/queue/queue.h"
#include "records.h"
#include "reporter.h"
#include "endstate.h"
#include "recstack.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {
/** */
namespace Generator {
/** */
namespace StateH {

/** */
class Visitor : public Oscl::UML::HSM::Visitor {
	public:
		/** */
		std::vector< std::string >	_namespaces;

		/** */
		std::vector< const Oscl::UML::HSM::Generator::TriggerRecord* >	_triggers;

		/** */
		std::vector< const ContextFuncRecord* >	_actions;

		/** */
		std::vector< const ContextFuncRecord* >	_uniqueActions;

		/** */
		std::vector< const ContextFuncRecord* >	_guards;

		/** */
		std::vector< std::string >		_guardFunctions;

		/** */
		Oscl::UML::HSM::Generator::Reporter&						_reporter;

		/** */
		const std::vector< Oscl::UML::Parser::Types::Parameter >*	_globalParameters;

	public:
		/** Accumulated state is responsible for
			the life-cycle of the parsed text.
		 */
		EndState	_endState;

	private:
		/** */
		RecordStack	_stacks;

	private:
		/** */
		NamespaceRecord*	_currentNamespace;

		/** */
		IncludeFileRecord*	_currentIncludeFile;

		/** */
		StateRecord*	_currentState;

		/** */
		EventRecord*	_currentEvent;

		/** */
		ChoicesRecord*	_currentChoices;

		/** */
		ChoiceRecord*	_currentChoice;

		/** */
		ActionsRecord*	_currentActions;

		/** */
		ResetRecord*	_currentReset;

		/** */
		EntryRecord*	_currentEntry;

		/** */
		TargetRecord*	_currentTarget;

		/** */
		TransitionRecord*	_currentTransition;

	private:
		/** */
		Populate< ChoiceRecord >*	_currentChoicePopulate;

		/** */
		Populate< ActionsRecord >*	_currentActionsPopulate;

		/** */
		Populate< EntryRecord >*	_currentEntryPopulate;

		/** */
		Populate< EventRecord >*	_currentEventPopulate;

	public:
		/** */
		ResetRecord*	_reset;

	public:
		/** */
		Visitor(
			Oscl::UML::HSM::Generator::Reporter& _reporter,
			const std::vector< Oscl::UML::Parser::Types::Parameter >*	globalParameters	= nullptr
			);

		/** */
		~Visitor();

		/** */
		bool	checkStateEvents();

		/** */
		bool	buildUniqueTriggers();

		/** */
		bool	buildUniqueGuardFunctions();

		/** */
		bool	buildUniqueActionFunctions();

		/** */
		void	printTransitionTree();

		/** */
		void	genStateCpp(
					std::ostream&	file
					);

		/** */
		void	genHsmHeader(
					std::ostream&		file,
					const std::string&	filename
					);

		/** */
		void	genHsmCpp(
					std::ostream&		file
					);
	private:
		/** */
		void	populateEvent( ChoiceRecord* r );

	private:
		/** */
		void	pushActionsPopulate( Populate< ActionsRecord >*  p);

		/** */
		void	popActionsPopulate();

	private:
		/** */
		void	pushEntryPopulate( Populate< EntryRecord >*  p);

		/** */
		void	popEntryPopulate();

	private:
		/** */
		void	pushEventPopulate( Populate< EventRecord >*  p);

		/** */
		void	popEventPopulate();

	private:
		/** */
		void	populateTarget( EntryRecord* r );

		/** */
		void	pushEntryPopulateTarget();

	private:
		/** */
		void	populateState( EventRecord* r );

		/** */
		void	pushEventPopulateState();

	private:
		/** */
		void	populateReset( ActionsRecord* r );

		/** */
		void	pushActionsPopulateReset();

	private:
		/** */
		void	populateEvent( ActionsRecord* r);

		/** */
		void	pushActionsPopulateEvent();

	private:
		/** */
		void	populateEntry( ActionsRecord* r);

		/** */
		void	pushActionsPopulateEntry();

	private:
		/** */
		void	populateChoices( ChoiceRecord* r );

		/** */
		void	pushChoicePopulateChoices();

	private:
		/** */
		void	pushChoicePopulate( Populate< ChoiceRecord >* p );

		/** */
		void	popChoicePopulate();

	private:
		/** */
		void	pushChoicePopulateEvent();

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Reset& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Reset& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Namespace& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Namespace& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Include& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Include& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::State& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::State& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Target& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Target& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Event& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Event& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Choices& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Choices& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Choice& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Choice& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Actions& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Actions& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Action& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Action& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::HSM::Entry& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::HSM::Entry& node ) noexcept override;
	};

}
}
}
}
}

#endif
