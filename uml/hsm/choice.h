/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_choiceh_
#define _oscl_uml_hsm_choiceh_

#include "node.h"
#include "list.h"
#include <iostream>
#include "oscl/uml/parser/types/choice.h"
#include "oscl/uml/parser/transition/driver.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {

/** A Choice represents one possible diverging
	transition for a given event.

	Choices may be originate from a State shape
	or from a Choice shape.

	Choices originating from a State shape follow
	a Trigger specification.

	Choices originating from a Choice shape have
	no Trigger specification.
 */
struct Choice : public Node {
	/** */
	Oscl::UML::Parser::Transition::Driver	_driver;

	/** */
	Oscl::UML::Parser::Types::Choice	_choice;

	/** */
	Oscl::UML::HSM::List	_nodeList;

	~Choice();

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Choice& t );

	/** */
	virtual bool accept(Visitor& visitor) const noexcept override;
	};

}
}
}


#endif
