/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_hsm_visitor_traceh_
#define _oscl_uml_hsm_visitor_traceh_

#include "visitor.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace HSM {

/** */
class Trace : public Visitor{
	public:
		/** */
		bool	pre( const class Reset& node ) noexcept override;

		/** */
		bool	post( const class Reset& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Namespace& node ) noexcept override;

		/** */
		bool	post( const class Namespace& node ) noexcept override;

	public:
		/** */
		bool	pre( const class State& node ) noexcept override;

		/** */
		bool	post( const class State& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Target& node ) noexcept override;

		/** */
		bool	post( const class Target& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Event& node ) noexcept override;

		/** */
		bool	post( const class Event& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Choices& node ) noexcept override;

		/** */
		bool	post( const class Choices& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Choice& node ) noexcept override;

		/** */
		bool	post( const class Choice& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Actions& node ) noexcept override;

		/** */
		bool	post( const class Actions& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Action& node ) noexcept override;

		/** */
		bool	post( const class Action& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Entry& node ) noexcept override;

		/** */
		bool	post( const class Entry& node ) noexcept override;
	};

}
}
}


#endif
