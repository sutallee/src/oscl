# UML Heirarchical State Machine
This directory contains components used in
the processing of UML Hierarchical State Machines (HSM).

# Motivation

These components were initially developed for the
Software Diagrammer HSM code generator tool.

# Goals
These components are intended to be UML specific and
application independent.

# General Abstractions

The HSM diagram is presumed to be represented as a
tree-like structure that can be traversed. The primary
tree containment mechanism is the nested/hierarchical
State. Transitions between a source States and target States
are triggered by events.

State
: A State may contain a text block describing entry, action and internal transitions.

TargetState
: A TargetState marks the State that is the of a Transition.

Trigger
: A Trigger causes a transition from one state to one or more states. Triggers
are global within an HSM and the actions taken depend upon the current State.

Choice
: A choice represents a point along a transition where the a decision
is made to choose a path towards two or more TargetStates.

Guard
: A Guard is a boolean expression that is evaluated within a Choice
to determine which transition path to take.

Action
: An Action is a bit of processing that is executed on the context
along the transition path.

EntryAction
: An EntryAction is an action that is executed whenever a
state is entered.

ExitAction
: An ExitAction is an action that is executed whenever a
state is exited.


