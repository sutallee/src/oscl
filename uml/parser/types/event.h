#ifndef _oscl_uml_parser_types_eventh_
#define _oscl_uml_parser_types_eventh_

#include <string>
#include <vector>
#include "trigger.h"
#include "choice.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**
	<trigger> <guards> <actions>
 */
struct Event {
	/** This string will hold the event signature
		when parsing is successful. The signature
		looks somewhat like a C++ function declaration
		without a return type.
		For example:
			event( const Type& b, unsigned count)
	 */
	Trigger		_trigger;

	/** This member contains an optional Guard expression
		and a list of actions.
	 */
	Oscl::UML::Parser::Types::Choice	_choice;

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Event& t );
	};
}
}
}
}

#endif
