#include <iostream>
#include "trigger.h"

using namespace Oscl::UML::Parser::Types;

const Argument*	Trigger::find( const std::string& name ) const noexcept {

	for( unsigned i=0; i < _arguments.size(); ++i ) {
		if( _arguments[i]._name == name ) {
			return &_arguments[i];
			}
		}

	return nullptr;
	}

bool	Trigger::operator == ( const Trigger& other) const noexcept {

	if( !( _name == other._name)  ){
		return false;
		}

	if( !( _arguments == other._arguments ) ) {
		return false;
		}

	return true;
	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator << ( std::ostream& os, const Trigger& t ){

	os << t._name << "(";

	bool	first	= true;

	for( unsigned i=0;i < t._arguments.size(); ++i){
		if( !first ){
			os << ",";
			}
		else {
			first	= false;
			}
		os << t._arguments[i];
		}

	os << ")";

	return os;
	}

}
}
}
}
