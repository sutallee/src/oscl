#include <iostream>
#include "choice.h"

using namespace Oscl::UML::Parser::Types;

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator << ( std::ostream& os, const Choice& t ) {

	if( t._guard.size() ) {
		os	<< "[" << t._guard << "]";
		os	<< "_userData(" << t._userData << ")";
		}

	if( t._actions.size() ) {
		os	<< "/\n";
		for( unsigned i=0; i < t._actions.size() ; ++i) {
			os	<< "\t" << t._actions[i] << "\n";
			}
		}
	else {
		os	<< "\n";
		}

	return os;
	}
}
}
}
}
