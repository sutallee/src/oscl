#include <iostream>
#include "guard.h"

using namespace Oscl::UML::Parser::Types;

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator << ( std::ostream& os, const Guard& t ) {

	if( t._guard.size() ) {
		os	<< "[" << t._guard << "]";
		os	<< "_userData(" << t._userData << ")";
		}
	else {
		os	<< "\n";
		}

	return os;
	}
}
}
}
}
