#include <iostream>
#include "event.h"

using namespace Oscl::UML::Parser::Types;

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator << ( std::ostream& os, const Event& t ){

	os << t._trigger;

	if( t._choice._guard.size() ) {
		os	<< "\t[" << t._choice._guard << "]";
		}

	if( t._choice._actions.size() ) {
		os	<< "/\n";
		for( unsigned i=0; i < t._choice._actions.size() ; ++i) {
			os	<< "\t" << t._choice._actions[i] << "\n";
			}
		}

	return os;
	}
}
}
}
}
