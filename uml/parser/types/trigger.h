#ifndef _oscl_uml_parser_types_triggerh_
#define _oscl_uml_parser_types_triggerh_

#include <string>
#include <vector>
#include <iostream>
#include "argument.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**	A Trigger is somehat like a C++ function
	declaration without a return type.
		For example:
			t1( const Type& b, unsigned count)
 */
struct Trigger {
	/** This string will hold the name of the
		trigger.
	 */
	std::string	_name;

	/**	This vector contains zero or more
		arguments.
 	 */
	std::vector< Argument >	_arguments;

	/** */
	bool	operator ==(const Trigger& other) const noexcept;

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Trigger& t );

	/** */
	const Argument*	find( const std::string& name ) const noexcept;
	};

}
}
}
}

#endif
