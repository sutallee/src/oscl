#ifndef _oscl_uml_parser_types_guardh_
#define _oscl_uml_parser_types_guardh_

#include <string>
#include <vector>
#include <iostream>

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**
	<guards> <actions>
 */
struct Guard {
	/** This string may hold a guard expression
		if parsing is successful. If the parsed
		string did not have a guard expression,
		then this string will be empty.
	 */
	std::string	_guard;

	/** */
	void*		_userData;

	/** */
	Guard():_userData(nullptr){}

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Guard& t );
	};
}
}
}
}

#endif
