#include <iostream>
#include "argument.h"

using namespace Oscl::UML::Parser::Types;

bool	Argument::typeMatch( const Argument& other) const {
	if( !( _type == other._type ) ) {
		return false;
		}

	if( _operators != other._operators ) {
		return false;
		}

	return true;
	}

bool	Argument::operator == ( const Argument& other) const {

	if(! typeMatch( other ) ) {
		return false;
		}

	return ( _name == other._name );
	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator<<( std::ostream& os, const Argument& t ){
	os << t._type;

	for( unsigned i=0; i < t._operators.size(); ++i ) {
		os	<< t._operators[i];
		}

	os << "\t" << t._name;

	return os;
	}

}
}
}
}
