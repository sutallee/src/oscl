#include <iostream>
#include "type.h"

using namespace Oscl::UML::Parser::Types;

bool	Type::operator ==(const Type& other) const noexcept {

	if( _operators.size() != other._operators.size() ) {
		return false;
		}

	for( unsigned i=0; i < _operators.size(); ++i ) {
		if( _operators[i] != other._operators[i] ) {
			return false;
			}
		}

	if( _path.size() != other._path.size() ) {
		return false;
		}

	for( unsigned i=0; i < _path.size(); ++i ) {
		if( _path[i] != other._path[i] ) {
			return false;
			}
		}

	if( _qualifiers.size() != other._qualifiers.size() ) {
		return false;
		}

	for( unsigned i=0; i<_qualifiers.size(); ++i){
		if( other.hasQualifier( _qualifiers[i]) ){
			continue;
			}
		return false;
		}

	return true;
	}

bool	Type::hasQualifier( const std::string s ) const noexcept{
	for( unsigned i=0; i<_qualifiers.size(); ++i){
		if( s == _qualifiers[i] ) {
			return true;
			}
		}
	return false;
	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Types {

std::ostream& operator<<( std::ostream& os, const Type& t ) {

	bool	first	= true;

	if( t._qualifiers.size() ) {

		for( unsigned i=0;i < t._qualifiers.size(); ++i){

			if( !first ){
				os << " ";
				}
			else {
				first	= false;
				}

			os << t._qualifiers[i];
			}

		os << " ";
		}

	first	= true;

	for( unsigned i=0;i < t._path.size(); ++i){

		if( !first ){
			os << "::";
			}
		else {
			first	= false;
			}

		os << t._path[i];
		}

	for( unsigned i=0;i < t._operators.size(); ++i){
		os << t._operators[i];
		}

	return os;
	}

}
}
}
}

