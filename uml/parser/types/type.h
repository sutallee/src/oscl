#ifndef _oscl_uml_parser_types_typeh_
#define _oscl_uml_parser_types_typeh_

#include <string>
#include <vector>
#include <iostream>

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**	A type includes all of the elements
	that make the type unique, such as
	qualifiers and symbols that indicate
	whether or we have a pointer or reference
	to the type.
 */
struct Type {
	/** The type path consists of all
		identifier characters and any
		namespace elements, such as
		A::B::_c::name
	 */
	std::vector< std::string >	_path;

	/**	
 	 */
	std::vector< std::string >	_qualifiers;

	/**	
 	 */
	std::vector< std::string >	_operators;

	/** */
	bool	operator ==(const Type& other) const noexcept;

	/** */
	bool	hasQualifier( const std::string s ) const noexcept;

	/** */
	friend std::ostream& operator<<( std::ostream& os, const Type& t );
	};
}
}
}
}

#endif
