#ifndef _oscl_uml_parser_types_parameterh_
#define _oscl_uml_parser_types_parameterh_

#include <string>
#include <vector>
#include <iostream>
#include "type.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**
	<type> <operators> <name>
	e.g.
		A::B::T &* blah
 */
struct Parameter {
	/** */
	std::string	_name;

	/** */
	Type	_type;

	/**	*/
	std::vector< std::string >	_operators;

	/** */
	bool	typeMatch( const Parameter& other) const;
 
	/** */
	bool	operator == ( const Parameter& other) const;
 
	/** */
	friend std::ostream& operator<<( std::ostream& os, const Parameter& t );
	};
}
}
}
}

#endif
