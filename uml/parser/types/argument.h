#ifndef _oscl_uml_parser_types_argumenth_
#define _oscl_uml_parser_types_argumenth_

#include <string>
#include <vector>
#include <iostream>
#include "type.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**
	<type> <operators> <name>
	e.g.
		A::B::T &* blah
 */
struct Argument {
	/** */
	std::string	_name;

	/** */
	Type	_type;

	/**	*/
	std::vector< std::string >	_operators;

	/** */
	bool	typeMatch( const Argument& other) const;
 
	/** */
	bool	operator == ( const Argument& other) const;
 
	/** */
	friend std::ostream& operator<<( std::ostream& os, const Argument& t );
	};
}
}
}
}

#endif
