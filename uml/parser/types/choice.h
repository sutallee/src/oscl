#ifndef _oscl_uml_parser_types_choiceh_
#define _oscl_uml_parser_types_choiceh_

#include <string>
#include <vector>
#include <iostream>

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Types {

/**
	<guards> <actions>
 */
struct Choice {
	/** This string may hold a guard expression
		if parsing is successful. If the parsed
		string did not have a guard expression,
		then this string will be empty.
	 */
	std::string	_guard;

	/**	This member may hold a sequence of action
		expressions if parsing is successful.
		If the parsed string contained no actions
		then this member will be empty.
 	 */
	std::vector< std::string >	_actions;

	/** */
	void*		_userData;

	/** */
	Choice():_userData(nullptr){}

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Choice& t );
	};
}
}
}
}

#endif
