#ifndef _oscl_uml_parser_transition_driverh_
#define _oscl_uml_parser_transition_driverh_

#include <string>
#include <vector>
#include "list.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Transition {

/**	This class is used to coordinate the parsing
	of a string and holds the results from the
	parsing which can then be used by the application.
 */
class Driver {
	public:
		/** Holds the final result after a
			string is parsed. Before the string is
			parsed, the _result is -1 .
			The result is zero for success
			and non-zero on failure.
		 */
		int			_result;

		/**	This is the parse tree for the
			transition expression.
		 */
		Oscl::UML::Parser::Transition::List	_parseTree;

		/** This is a copy of the string to be parsed.
		 */
		std::string	_arg;

		/**	True if bison parsing should print
			its progress for debuging.
		 */
		bool	_trace_parsing;

		/**	True if the lexical scanner (flex) should
			print its progress for debugging.
		 */
		bool	_trace_scanning;

		/** */
		bool	_first_token;

		/**	This parameter is used to track the
			current position in the _arg string
			during parsing. When _result is non-zero
			(parsing fails) the _position is roughly
			the character positon where the parsing
			failed.
		 */
		unsigned	_position;

		/** This member may be used by the
			context to store information within
			the driver that can be used by the
			context for higher level functions
			such as syntax/error highlighting.
			The motivation for this involves the
			use of a pointer to the Qt QTextDocument
			that contains the string being parsed.
		 */
		void*		_context;

	public:
		/**	The constructor.
		 */
		Driver(void* const context = nullptr);

		/**	The destructor.
		 */
		~Driver();

		/**	Run the parser on the string s.
			Return 0 on success.
		 */
		int	parse( const std::string& s );

		/**	Run the parser on the string s.
			Return 0 on success.
		 */
		int	parseEvent( const std::string& s );

		/**	Run the parser on the string s.
			Return 0 on success.
		 */
		int	parseChoice( const std::string& s );

		/**	Run the parser on the string s.
			Return 0 on success.
		 */
		int	parseAction( const std::string& s );

		/** Prepare the scanner before parse()
			is invoked.
		 */
		void	scan_begin();

		/**	Clean-up the scanner after parse()
			is invoked.
		 */
		void	scan_end();

	/** */
	friend std::ostream& operator << ( std::ostream& os, const Driver& t );
	};
}
}
}
}

#endif
