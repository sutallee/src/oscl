/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "binoperand.h"
#include "visitor.h"
#include <iostream>

using namespace Oscl::UML::Parser::Transition;

BinOperand::BinOperand(
	unsigned			position
	):
		_position( position )
		{
	}

bool BinOperand::accept( Visitor& visitor ) const noexcept {

	if( !visitor.pre( *this ) ) {
		return false;
		}

	if( !_arguments.accept( visitor ) ) {
		return false;
		}

	if( !visitor.post( *this ) ) {
		return false;
		}

	return true;
	}

BinOperand::~BinOperand() {
#if 0
	std::cout
		<< __PRETTY_FUNCTION__
		<< ":"
		<< this
		<< *this
		<< "\n"
		;
#endif
	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Transition {

std::ostream& operator<<( std::ostream& os, const BinOperand& t ){

	return os;
	}

}
}
}
}

