/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_parser_transition_treeh_
#define _oscl_uml_parser_transition_treeh_

#include "visitor.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace Parser {
/** */
namespace Transition {

/** */
class Tree : public Visitor {
	private:
		/** */
		unsigned	_indent;

	public:
		/** */
		Tree():_indent(0){}

	private:
		/** */
		void	indent();

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Actions& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Actions& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Action& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Action& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Argument& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Argument& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Group& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Event& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Events& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;

		/** */
		bool	post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept override;
	};

}
}
}
}


#endif
