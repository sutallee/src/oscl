# UML Transition Parser

This code is a Bison/Lex parser that for UML HSM state transitions.
The design concept is to have a single parser with multiple
entry points which allow it to be used for all of the different
types of UML transition text fields for a C++ type language.

A UML Hierarchical State Machine consists of a number of
text fields that describe various aspects of a transition.
Some of the text fields allow more syntax elements than others.
For example, a State Compartment text field may hold self transitions as
well as entry and exit actions and Initial transitions may
only hold actions.

Initial efforts for this parser had a separate parser for
each of these different types of text fields. However, this
led to great deal of redundancy in implementation creating
a significant maintenance burden. Here, the idea is to
have a single parser that can be started at different 
points to allow for the different types of text fields to
be correctly parsed.

Here is a list of the different expected entry points and
the types of fields they may contain.

- Compartment
    - May contain any of the syntax elements
        - Entry actions
        - Exit actions
        - Events
- Event
    - May NOT contain Entry or Exit actions
    - May contain
        - trigger
        - guard expression
        - actions
- Choice
    - May NOT contain Entry or Exit actions
    - May NOT contain a trigger
    - May contain
        - guard expression
        - actions

- Action
    - May NOT contain Entry or Exit actions
    - May NOT contain a trigger
    - May NOT contain a guard expression
    - May ONLY contain actions

The entry points will be selected as described in the Bison
documentation section **13.5 Multiple start-symbols**. This
method describes a work-around for the fact that Bison does
not support multiple start-symbols (entry points). The
work-around uses the definition of special scanner tokens
and application code to choose a pseudo-start-symbol.

