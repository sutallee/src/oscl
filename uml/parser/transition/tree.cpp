/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include "tree.h"

using namespace Oscl::UML::Parser::Transition;

void	Tree::indent() {
	for( unsigned i=0; i < _indent; ++i ) {
		std::cout << "  ";
		}
	}

bool	Tree::pre( const class BinOperand& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class BinOperand& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class BinOp& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class BinOp& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class VarExp& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class VarExp& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class VarFunc& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class VarFunc& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class ContextFunc& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class ContextFunc& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class ContextArgs& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class ContextArgs& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class ContextArg& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class ContextArg& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Actions& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Actions& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Action& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Action& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Argument& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Argument& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Arguments& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Arguments& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class GuardExpr& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class GuardExpr& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Group& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Group& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Guard& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Guard& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Choice& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Choice& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Trigger& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Trigger& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Event& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Event& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Events& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Events& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Parameter& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Parameter& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Parameters& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Parameters& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Compartment& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Compartment& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Entry& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Entry& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class Exit& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class Exit& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class StringLiteral& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class StringLiteral& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class CharLiteral& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class CharLiteral& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

bool	Tree::pre( const class NumberLiteral& node ) noexcept {
	indent();
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "\n";
	++_indent;
	return true;
	}

bool	Tree::post( const class NumberLiteral& node ) noexcept {
	indent();
	std::cout << "}\n";
	--_indent;
	return true;
	}

