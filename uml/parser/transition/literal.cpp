/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "literal.h"
#include "visitor.h"
#include <iostream>

using namespace Oscl::UML::Parser::Transition;

#if 0
Literal::Literal(
	const std::string&	name,
	unsigned			position
	):
	_name( name ),
	_position( position )
	{
	}

bool Literal::accept( Visitor& visitor ) const noexcept {
	if( !visitor.pre( *this ) ) {
		return false;
		}

	if( !visitor.post( *this ) ) {
		return false;
		}

	return true;
	}

Literal::~Literal() {

	std::shared_ptr< Node >	node;

	while( ( node = _arguments.get() ) ) {
		node.reset();
		}

	}
#endif

StringLiteral::StringLiteral(
	const std::string&	name,
	unsigned			position
	):
	_name( name ),
	_position( position )
	{
	}

bool StringLiteral::accept( Visitor& visitor ) const noexcept {
	if( !visitor.pre( *this ) ) {
		return false;
		}

	if( !visitor.post( *this ) ) {
		return false;
		}

	return true;
	}

StringLiteral::~StringLiteral() {

	std::shared_ptr< Node >	node;

	while( ( node = _arguments.get() ) ) {
		node.reset();
		}

	}

CharLiteral::CharLiteral(
	const std::string&	name,
	unsigned			position
	):
	_name( name ),
	_position( position )
	{
	}

bool CharLiteral::accept( Visitor& visitor ) const noexcept {
	if( !visitor.pre( *this ) ) {
		return false;
		}

	if( !visitor.post( *this ) ) {
		return false;
		}

	return true;
	}

CharLiteral::~CharLiteral() {

	std::shared_ptr< Node >	node;

	while( ( node = _arguments.get() ) ) {
		node.reset();
		}

	}

NumberLiteral::NumberLiteral(
	const std::string&	name,
	unsigned			position
	):
	_name( name ),
	_position( position )
	{
	}

bool NumberLiteral::accept( Visitor& visitor ) const noexcept {
	if( !visitor.pre( *this ) ) {
		return false;
		}

	if( !visitor.post( *this ) ) {
		return false;
		}

	return true;
	}

NumberLiteral::~NumberLiteral() {

	std::shared_ptr< Node >	node;

	while( ( node = _arguments.get() ) ) {
		node.reset();
		}

	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Transition {

std::ostream& operator<<( std::ostream& os, const Literal& t ){

	// TODO
	os
		<< "_name:" << t._name
		<< ","
		<< "_position:" << t._position
		;

	return os;
	}

std::ostream& operator<<( std::ostream& os, const StringLiteral& t ){

	// TODO
	os
		<< "_name:" << t._name
		<< ","
		<< "_position:" << t._position
		;

	return os;
	}

std::ostream& operator<<( std::ostream& os, const CharLiteral& t ){

	// TODO
	os
		<< "_name:'" << t._name << "'"
		<< ","
		<< "_position:" << t._position
		;

	return os;
	}

std::ostream& operator<<( std::ostream& os, const NumberLiteral& t ){

	// TODO
	os
		<< "_name:" << t._name
		<< ","
		<< "_position:" << t._position
		;

	return os;
	}

}
}
}
}

