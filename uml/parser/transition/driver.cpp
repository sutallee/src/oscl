#include "driver.h"
#include "parser.h"
#include "lex.h"
#include "tree.h"
#include <iostream>
#include "parser.hpp"

using namespace Oscl::UML::Parser::Transition;

Driver::Driver( void* const context ):
	_result( -1 ),
	_trace_parsing(false),
	_trace_scanning(false),
	_first_token(true),
	_position( 0 ),
	_context( context )
	{
	}

Driver::~Driver() {
	}

int	Driver::parse (const std::string &s) {

	_arg = s;

	scan_begin ();

	_first_token	= false;

	Oscl::UML::Parser::Transition::parser::token::token_kind_type
	t = Oscl::UML::Parser::Transition::parser::token::token_kind_type::TOK_START_AT_EVENT;

	Oscl::UML::Parser::Transition::parser parse( *this, &t );

	parse.set_debug_level( _trace_parsing );

	_result	= parse();

	scan_end();

	return _result;
	}

int	Driver::parseEvent (const std::string &s) {

	_position	= 0;

	_arg = s;

	scan_begin ();

	Oscl::UML::Parser::Transition::parser::token::token_kind_type
	t = Oscl::UML::Parser::Transition::parser::token::token_kind_type::TOK_START_AT_EVENT;

	Oscl::UML::Parser::Transition::parser parse( *this, &t );

	parse.set_debug_level( _trace_parsing );

	_result	= parse();

	scan_end();

	return _result;
	}

int	Driver::parseChoice (const std::string &s) {

	_arg = s;

	scan_begin ();

	Oscl::UML::Parser::Transition::parser::token::token_kind_type
	t = Oscl::UML::Parser::Transition::parser::token::token_kind_type::TOK_START_AT_CHOICE;

	Oscl::UML::Parser::Transition::parser parse( *this, &t );

	parse.set_debug_level( _trace_parsing );

	_result	= parse();

	scan_end();

	return _result;
	}

int	Driver::parseAction (const std::string &s) {

	_arg = s;

	scan_begin ();

	Oscl::UML::Parser::Transition::parser::token::token_kind_type
	t = Oscl::UML::Parser::Transition::parser::token::token_kind_type::TOK_START_AT_ACTION;

	Oscl::UML::Parser::Transition::parser parse( *this, &t );

	parse.set_debug_level( _trace_parsing );

	_result	= parse();

	scan_end();

	return _result;
	}

void	Driver::scan_begin () {

	transitionset_debug( _trace_scanning );

	transition_scan_string( _arg.c_str() );
	}

void	Driver::scan_end () {
	transitionlex_destroy();
	}

namespace Oscl {
namespace UML {
namespace Parser {
namespace Transition {

std::ostream& operator << ( std::ostream& os, const Driver& t ) {

	os
		<< "_arg: "
		<< t._arg
		<< "\n"
		;

	Oscl::UML::Parser::Transition::Tree	trace;
	t._parseTree.accept( trace );

	return os;
	}

}
}
}
}

