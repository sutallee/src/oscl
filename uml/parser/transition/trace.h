/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_parser_transition_traceh_
#define _oscl_uml_parser_transition_traceh_

#include "visitor.h"

/** */
namespace Oscl {
/** */
namespace UML {
/** */
namespace Parser {
/** */
namespace Transition {

/** */
class Trace : public Visitor{
	public:
		/** */
		bool	pre( const class BinOp& node ) noexcept override;

		/** */
		bool	post( const class BinOp& node ) noexcept override;

	public:
		/** */
		bool	pre( const class VarExp& node ) noexcept override;

		/** */
		bool	post( const class VarExp& node ) noexcept override;

	public:
		/** */
		bool	pre( const class ContextFunc& node ) noexcept override;

		/** */
		bool	post( const class ContextFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const class VarFunc& node ) noexcept override;

		/** */
		bool	post( const class VarFunc& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Actions& node ) noexcept override;

		/** */
		bool	post( const class Actions& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Argument& node ) noexcept override;

		/** */
		bool	post( const class Argument& node ) noexcept override;

	public:
		/** */
		bool	pre( const class ContextArg& node ) noexcept override;

		/** */
		bool	post( const class ContextArg& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Arguments& node ) noexcept override;

		/** */
		bool	post( const class Arguments& node ) noexcept override;

	public:
		/** */
		bool	pre( const class ContextArgs& node ) noexcept override;

		/** */
		bool	post( const class ContextArgs& node ) noexcept override;

	public:
		/** */
		bool	pre( const class GuardExpr& node ) noexcept override;

		/** */
		bool	post( const class GuardExpr& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Guard& node ) noexcept override;

		/** */
		bool	post( const class Guard& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Choice& node ) noexcept override;

		/** */
		bool	post( const class Choice& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Trigger& node ) noexcept override;

		/** */
		bool	post( const class Trigger& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Event& node ) noexcept override;

		/** */
		bool	post( const class Event& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Events& node ) noexcept override;

		/** */
		bool	post( const class Events& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Parameter& node ) noexcept override;

		/** */
		bool	post( const class Parameter& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Parameters& node ) noexcept override;

		/** */
		bool	post( const class Parameters& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Compartment& node ) noexcept override;

		/** */
		bool	post( const class Compartment& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Entry& node ) noexcept override;

		/** */
		bool	post( const class Entry& node ) noexcept override;

	public:
		/** */
		bool	pre( const class Exit& node ) noexcept override;

		/** */
		bool	post( const class Exit& node ) noexcept override;

	public:
		/** */
		bool	pre( const class StringLiteral& node ) noexcept override;

		/** */
		bool	post( const class StringLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const class CharLiteral& node ) noexcept override;

		/** */
		bool	post( const class CharLiteral& node ) noexcept override;

	public:
		/** */
		bool	pre( const class NumberLiteral& node ) noexcept override;

		/** */
		bool	post( const class NumberLiteral& node ) noexcept override;
	};

}
}
}
}


#endif
