/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include "trace.h"

using namespace Oscl::UML::Parser::Transition;

bool	Trace::pre( const class BinOp& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class BinOp& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class VarExp& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class VarExp& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class ContextFunc& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class ContextFunc& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class VarFunc& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class VarFunc& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Actions& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Actions& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Argument& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Argument& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class ContextArg& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class ContextArg& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Arguments& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Arguments& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class ContextArgs& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class ContextArgs& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class GuardExpr& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class GuardExpr& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Guard& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Guard& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Choice& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Choice& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Trigger& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Trigger& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Event& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Event& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Events& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Events& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Parameter& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Parameter& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Parameters& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Parameters& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Compartment& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Compartment& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Entry& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Entry& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class Exit& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class Exit& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class StringLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class StringLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class CharLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class CharLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::pre( const class NumberLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

bool	Trace::post( const class NumberLiteral& node ) noexcept {
	std::cout << __PRETTY_FUNCTION__ << "{"	<< node << "}\n";
	return true;
	};

