/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uml_parser_transition_visitorh_
#define _oscl_uml_parser_transition_visitorh_

#include "binop.h"
#include "varexp.h"
#include "ctxfunc.h"
#include "actions.h"
#include "action.h"
#include "argument.h"
#include "arguments.h"
#include "guardexpr.h"
#include "guard.h"
#include "group.h"
#include "choice.h"
#include "trigger.h"
#include "event.h"
#include "events.h"
#include "parameter.h"
#include "parameters.h"
#include "compartment.h"
#include "entry.h"
#include "exit.h"
#include "literal.h"
#include "ctxarg.h"
#include "ctxargs.h"
#include "ctxfunc.h"
#include "varfunc.h"
#include "binoperand.h"

/** */
namespace Oscl {

/** */
namespace UML {

/** */
namespace Parser {

/** */
namespace Transition {

/** */
class Visitor {
	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::BinOp& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::BinOperand& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::VarExp& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::ContextFunc& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::VarFunc& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Actions& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Actions& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Action& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Action& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Argument& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Argument& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::ContextArg& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Arguments& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::ContextArgs& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::GuardExpr& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Group& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Group& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Guard& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Guard& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Choice& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Choice& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Trigger& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Event& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Event& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Events& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Events& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Parameter& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Parameters& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Compartment& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Entry& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Entry& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::Exit& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::Exit& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::StringLiteral& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::CharLiteral& node ) noexcept =0;

	public:
		/** */
		virtual bool	pre( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept =0;

		/** */
		virtual bool	post( const Oscl::UML::Parser::Transition::NumberLiteral& node ) noexcept =0;
	};

}
}
}
}


#endif
