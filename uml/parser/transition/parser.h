#ifndef _oscl_uml_parser_transition_parserh_
#define _oscl_uml_parser_transition_parserh_

#include "driver.h"
#include "parser.hpp"

# define YY_DECL \
	Oscl::UML::Parser::Transition::parser::symbol_type oscl_uml_parser_transitionlex( Oscl::UML::Parser::Transition::Driver& drv, Oscl::UML::Parser::Transition::parser::token::token_kind_type* start_token )

YY_DECL;

#endif
