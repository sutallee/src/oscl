/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_arp_ieee48_ip_pduh_
#define _oscl_protocol_arp_ieee48_ip_pduh_
#include "oscl/protocol/ip/address.h"
#include "oscl/protocol/ieee/mac.h"
#include "oscl/protocol/arp/opcode.h"
#include <stdint.h>
#include "oscl/compiler/align.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ARP {
/** */
namespace IEEE48Bit {
/** */
namespace IP {

/** */
struct PDU {
	/** */
	uint8_t							hwAddressLength;	// IEEE48Bit = 6
	/** */
	uint8_t							protoAddressLength;	// IP = 4
	/** */
	Oscl::Protocol::ARP::Opcode::Var	opcode;
	/** */
	Oscl::Protocol::IEEE::MacAddress	srcHardwareAddress;
	/** */
	Oscl::Protocol::IP::Address			srcProtocolAddress;
	/** */
	Oscl::Protocol::IEEE::MacAddress	targetHardwareAddress;
	/** */
	Oscl::Protocol::IP::Address			targetProtocolAddress;
	}OsclPackedPostMacro();

}
}
}
}
}

#endif
