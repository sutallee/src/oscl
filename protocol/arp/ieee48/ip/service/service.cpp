/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "service.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/mt/thread.h"
#include "oscl/protocol/ieee/mac.h"
#include "oscl/protocol/ip/address.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Protocol::ARP::IEEE48Bit::IP;

const Oscl::Protocol::IEEE::MacAddressInit
	myMacAddress(0x00,0x03,0xE0,0x12,0x34,0x56);

const Oscl::Protocol::IP::AddressInit
	targetIpAddress("192.168.0.2");

enum{oneSecondDelayInMilliseconds=1000};
 
Service::Service(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	brodcastSAP,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	rawTxSAP,
					Oscl::Frame::Pdu::Rx::Req::Api::SAP&	rxSAP,
					Oscl::Mt::Itc::
					Delay::Req::Api::SAP&					delaySAP,
					const Oscl::Protocol::IP::Address&		myIpAddress,
					NotifyApi&								pendMonitor,
					RxMem									rxMem[],
					unsigned								nRxRequests,
					ArpTransMem								transMem[],
					unsigned								nReqTranss,
					BindMem									bindMem[],
					unsigned								nBindMem
					) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_delayPayload(oneSecondDelayInMilliseconds),
		_delayResp(	delaySAP.getReqApi(),
					*this,
					myPapi,
					_delayPayload
					),
		_delayCancelPayload(_delayResp.getSrvMsg()),
		_delayCancelResp(	delaySAP.getReqApi(),
							*this,
							myPapi,
							_delayCancelPayload
							),
		_brodcastSAP(brodcastSAP),
		_rawTxSAP(rawTxSAP),
		_rxSAP(rxSAP),
		_delaySAP(delaySAP),
		_arpSAP(*this,myPapi),
		_myPapi(myPapi),
		_myIpAddress(myIpAddress),
		_pendMonitor(pendMonitor),
		_open(false),
		_closeReq(0)
		{
	for(unsigned i=0;i<nRxRequests;++i){
		_freeRxMem.put(&rxMem[i]);
		}
	for(unsigned i=0;i<nReqTranss;++i){
		_freeTransMem.put(&transMem[i]);
		}
	for(unsigned i=0;i<nBindMem;++i){
		_freeBindings.put(&bindMem[i]);
		}
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Service::getTxSAP() noexcept{
	return _arpSAP;
	}

void	Service::free(Binding* binding) noexcept{
	if(!binding->_macAddressIsValid){
		_pendMonitor.removed(binding->ipAddr);
		}
	binding->~Binding();
	_freeBindings.put((BindMem*)binding);
	}

Service::Binding*
Service::allocBinding(	const Oscl::Protocol::IP::Address&		ipAddr,
						const Oscl::Protocol::IEEE::MacAddress&	macAddr
						) noexcept{
	BindMem*	mem	= _freeBindings.get();
	if(!mem){
		// If no more free binding memory, reuse the oldest
		// binding in the queue.
		Binding*
		binding	= _bindings.get();
		if(!binding){
			Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::ARP::IEEE48Bit::IP::Service: "
											"binding management error"
											);
			}
		free(binding);
		mem	= _freeBindings.get();
		if(!mem){
			Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::ARP::IEEE48Bit::IP::Service: "
											"binding management error 2"
											);
			}
		}
	return new (mem) Binding(ipAddr,macAddr);
	}

Service::Binding*
Service::allocBinding(	const Oscl::Protocol::IP::Address&	ipAddr) noexcept{
	BindMem*	mem	= _freeBindings.get();
	if(!mem){
		// If no more free binding memory, reuse the oldest
		// binding in the queue.
		Binding*
		binding	= _bindings.get();
		if(!binding){
			Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::ARP::IEEE48Bit::IP::Service: "
											"binding management error"
											);
			}
		free(binding);
		mem	= _freeBindings.get();
		if(!mem){
			Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::ARP::IEEE48Bit::IP::Service: "
											"binding management error 2"
											);
			}
		}
	return new (mem) Binding(ipAddr);
	}

void	Service::learnBinding(	const Oscl::Protocol::IP::Address&		ipAddr,
								const Oscl::Protocol::IEEE::MacAddress&	macAddr
								) noexcept{
	Binding*	binding	= lookup(ipAddr);
	if(binding){
		bool	macAddressIsValid	= binding->_macAddressIsValid;
		binding->refresh(macAddr);
		if(!macAddressIsValid){
			_pendMonitor.discovered(ipAddr);
			}
		return;
		}
	binding	= allocBinding(ipAddr,macAddr);
	_bindings.put(binding);
	}

Service::Binding*
Service::lookup(const Oscl::Protocol::IP::Address& ipAddr) const noexcept{
	Binding*	next;
	for(	next	= _bindings.first();
			next;
			next	= _bindings.next(next)
			){
		if(next->ipAddr == ipAddr){
			return next;
			}
		}
	return 0;
	}

bool	Service::resolve(	const Oscl::Protocol::IP::Address&	ipAddr,
							Oscl::Protocol::IEEE::MacAddress&	macAddr
							) noexcept{
	Binding*	binding	= lookup(ipAddr);
	if(!binding){
		// FIXME: allocate binding pending
		// mac address association here.
		binding	= allocBinding(ipAddr);
		_bindings.put(binding);
		sendArpReq(ipAddr);
		return false;
		}
	if(!binding->_macAddressIsValid){
		return false;
		}
	binding->resetAging();
	macAddr	= binding->macAddr;
	return true;
	}

void	Service::sendArpReq(const Oscl::Protocol::IP::Address& dest) noexcept{
	ArpTransMem*	mem	= _freeTransMem.get();
	if(!mem) return;
	new (mem) ReqTrans(	*this,
						_myPapi,
						_brodcastSAP,
						myMacAddress,
						_myIpAddress,
						dest
						);
	}

void	Service::sendArpReq(	const Oscl::Protocol::IP::Address&		dest,
								const Oscl::Protocol::IEEE::MacAddress&	macAddr
								) noexcept{
	ArpTransMem*	mem	= _freeTransMem.get();
	if(!mem) return;
	new (mem) DirectReqTrans(	*this,
								_myPapi,
								_rawTxSAP,
								myMacAddress,
								_myIpAddress,
								macAddr,
								dest
								);
	}

void	Service::done(DirectReqTrans& trans) noexcept{
	trans.~DirectReqTrans();
	_freeTransMem.put((ArpTransMem*)&trans);
	}

void	Service::done(ReqTrans& trans) noexcept{
	trans.~ReqTrans();
	_freeTransMem.put((ArpTransMem*)&trans);
	}

void	Service::done(ReplyTrans& trans) noexcept{
	trans.~ReplyTrans();
	_freeTransMem.put((ArpTransMem*)&trans);
	}

void	Service::request(CloseReq& msg) noexcept{
	// FIXME: Need to implement this
	// * Use nOutstandingTransactions method.
	// * Cancel all outstanding RxResp.
	for(;;);

	if(_closeReq){
		for(;;);	// FIXME: protocol error
		}
	_open		= false;
	_closeReq	= &msg;
	_delaySAP.post(_delayCancelResp.getSrvMsg());
	}

void	Service::request(OpenReq& msg) noexcept{
	if(_open || _closeReq){
		for(;;);	// FIXME: protocol error
		}
	RxMem*	mem;
	while((mem=_freeRxMem.get())){
		Oscl::Frame::Pdu::Rx::Req::Api::RxPayload*	payload;
		Oscl::Frame::Pdu::Rx::Resp::Api::RxResp*		resp;
		payload	= new(&mem->rxRespMem.payload)	Oscl::Frame::Pdu::
											Rx::Req::Api::RxPayload();
		resp	= new(&mem->rxRespMem.resp)
					Oscl::Frame::Pdu::
					Rx::Resp::Api::RxResp(	_rxSAP.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
		_rxSAP.post(resp->getSrvMsg());
		}
	_open	= true;
	restartTimer();
	msg.returnToSender();
	}

void	Service::free(void* fso) noexcept{
	// do nothing
	}

void	Service::request(	Oscl::Frame::Pdu::
							Tx::Req::Api::TxReq&	msg
							) noexcept{
	// allocate & send ARP TxReq
	sendArpReq(targetIpAddress);
	msg.returnToSender();
	}

void	Service::request(	Oscl::Protocol::
							IP::TX::Srv::Req::Api::SendReq&	msg
							) noexcept{
	sendArpReq(targetIpAddress);
	msg.returnToSender();
	}

void	Service::response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::RxResp&	msg
							) noexcept{
	// FIXME: Should test for MacType and ProtoType before
	//	stripping.

	// This is stripping the ARP "hardware" type (e.g. Ethernet)
	// and "protocol" type (e.g. IP) fields from the ARP PDU.
	msg.getPayload()._handle->stripHeader(4); // FIXME: use symbol
	Oscl::Protocol::ARP::IEEE48Bit::IP::PDU	pkt;
	msg.getPayload()._handle->read(	(uint8_t*)&pkt,
									sizeof(pkt),
									0
									);
	if(pkt.opcode == Oscl::Protocol::ARP::Opcode::Request){
		sendArpReply(pkt);
		}
	else if(pkt.opcode == Oscl::Protocol::ARP::Opcode::Reply){
		processArpReply(pkt);
		}
	msg.getPayload()._handle	= 0;
	_rxSAP.post(msg.getSrvMsg());
	}

void	Service::sendArpReply(	const Oscl::Protocol::ARP::
								IEEE48Bit::IP::PDU&			req
								) noexcept{
	if(req.targetProtocolAddress == _myIpAddress){
		learnBinding(	req.srcProtocolAddress,
						req.srcHardwareAddress
						);
		ArpTransMem*	mem	= _freeTransMem.get();
		if(!mem) return;
		new (mem) ReplyTrans(	*this,
								_myPapi,
								_rawTxSAP,
								myMacAddress,
								_myIpAddress,
								req.srcHardwareAddress,
								req.srcProtocolAddress
								);
		}
	}

void	Service::processArpReply(	const Oscl::Protocol::ARP::
									IEEE48Bit::IP::PDU&			reply
									) noexcept{
	learnBinding(	reply.srcProtocolAddress,
					reply.srcHardwareAddress
					);
	}

void	Service::response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::CancelAllResp&	msg
							) noexcept{
	// FIXME: Is cancel really necessary?
	}

void	Service::response(	Oscl::Mt::Itc::
							Delay::Resp::Api::DelayResp&	msg
							) noexcept{
	if(!_open) return;
	oneSecondProtocolTimerExpired();
	restartTimer();
	}

void	Service::response(	Oscl::Mt::Itc::
							Delay::Resp::Api::CancelResp&	msg
							) noexcept{
	if(_closeReq){	// FIXME: Change this after add TX and RX requests handling.
		_closeReq	= 0;
		_closeReq->returnToSender();
		}
	}

void	Service::restartTimer() noexcept{
	if(!_open) return;
	_delayResp._payload._timer._milliseconds	= oneSecondDelayInMilliseconds;
	_delaySAP.post(_delayResp.getSrvMsg());
	}

void	Service::oneSecondProtocolTimerExpired() noexcept{
	Oscl::Queue<Binding>	bindings	= _bindings;
	Binding*	next;
	while((next=bindings.get())){
		if(next->readyToRemove()){
			free(next);
			continue;
			}
		if(next->needsRefresh()){
			sendArpReq(next->ipAddr,next->macAddr);
			}
		next->tick();
		_bindings.put(next);
		}
	}

Service::Binding::Binding(	const Oscl::Protocol::IP::Address&		ipAddress,
							const Oscl::Protocol::IEEE::MacAddress&	macAddress
							) noexcept:
		_lastRefreshValue(initialSecondsBeforeRefresh),
		_secondsBeforeRefresh(initialSecondsBeforeRefresh),
		_nRetriesRemaining(maxRetries),
		_ageSecondsRemaining(nSecondsBeforeAgeDeath),
		_macAddressIsValid(true)
		{
	ipAddr	= ipAddress;
	macAddr	= macAddress;
	}

Service::Binding::Binding(const Oscl::Protocol::IP::Address& ipAddress) noexcept:
		_lastRefreshValue(initialSecondsBeforeRefresh),
		_secondsBeforeRefresh(1),
		_nRetriesRemaining(maxRetries),
		_ageSecondsRemaining(nSecondsBeforeAgeDeath),
		_macAddressIsValid(false)
		{
	ipAddr	= ipAddress;
	}

void	Service::Binding::refresh(	const Oscl::Protocol::
									IEEE::MacAddress&		macAddress
									) noexcept{
	unsigned		newRefreshValue	= _lastRefreshValue << 1;
	if(newRefreshValue < maxRefreshValue){
		_lastRefreshValue	= newRefreshValue;
		}
	_secondsBeforeRefresh	= _lastRefreshValue;
	_nRetriesRemaining		= maxRetries;
	macAddr					= macAddress;
	_macAddressIsValid		= true;
	}

void	Service::Binding::resetAging() noexcept{
	_ageSecondsRemaining	= nSecondsBeforeAgeDeath;
	}

bool	Service::Binding::readyToRemove() noexcept{
	if(!_ageSecondsRemaining){
		return true;
		}
	if(!_secondsBeforeRefresh && !_nRetriesRemaining){
		return true;
		}
	return false;
	}

bool	Service::Binding::needsRefresh() noexcept{
	if(!_secondsBeforeRefresh && _nRetriesRemaining){
		return true;
		}
	return false;
	}

void	Service::Binding::tick() noexcept{
	if(_ageSecondsRemaining){
		--_ageSecondsRemaining;
		}
	if(_secondsBeforeRefresh){
		--_secondsBeforeRefresh;
		return;
		}
	if(_nRetriesRemaining){
		--_nRetriesRemaining;
		return;
		}
	}

Stimulator::Stimulator(	Oscl::Frame::Pdu::Tx::
						Req::Api::SAP&				txSAP
						) noexcept:
		_txSAP(txSAP)
		{
	}

void Stimulator::run() noexcept{
	while(true){
#if 0
		uint8_t	pkt[1];
		Oscl::Pdu::Fixed	pdu(	*this,
									*this,
									(uint8_t*)&pkt,
									sizeof(pkt),
									sizeof(pkt)
									);
		Oscl::Frame::Pdu::Tx::Req::Api::TxPayload	payload(pdu);

		Oscl::Mt::Itc::SyncReturnHandler	srh;

		Oscl::Frame::Pdu::Tx::Req::Api::TxReq	req(	_txSAP.getReqApi(),
														payload,
														srh
														);
		_txSAP.postSync(req);
#endif
		Oscl::Mt::Thread::sleep(2000);
		}
	}

void	Stimulator::free(void* fso) noexcept{
	// Do nothing
	}
