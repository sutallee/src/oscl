/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "service.h"
#include "transaction.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/mt/thread.h"
#include "oscl/protocol/ieee/mac.h"
#include "oscl/protocol/ip/address.h"

using namespace Oscl::Protocol::ARP::IEEE48Bit::IP;

const Oscl::Protocol::IEEE::MacAddressInit
	nullAddress(0x00,0x00,0x00,0x00,0x00,0x00);

ReqTrans::ReqTrans(	Service&					context,
					Oscl::Mt::Itc::PostMsgApi&	myPapi,
					Oscl::Frame::Pdu::Tx::
					Req::Api::SAP&				txSAP,
					const Oscl::Protocol::
					IEEE::MacAddress&			myMacAddress,
					const Oscl::Protocol::
					IP::Address&				myIpAddress,
					const Oscl::Protocol::
					IP::Address&				targetIpAddress
					) noexcept:
		_context(context)
		{
	_pkt.macType	= Oscl::Protocol::ARP::MacType::IEEE48Bit;
	_pkt.protoType	= Oscl::Protocol::ARP::ProtoType::IP;
	_pkt.pdu.hwAddressLength	= sizeof(Oscl::Protocol::IEEE::MacAddress);
	_pkt.pdu.protoAddressLength	= sizeof(Oscl::Protocol::IP::Address);
	_pkt.pdu.opcode				= Oscl::Protocol::ARP::Opcode::Request;
	_pkt.pdu.srcHardwareAddress	= myMacAddress;
	_pkt.pdu.srcProtocolAddress	= myIpAddress;
	_pkt.pdu.targetHardwareAddress	= nullAddress;
	_pkt.pdu.targetProtocolAddress	= targetIpAddress;
	Oscl::Pdu::Fixed*	pdu;
	pdu	= new (&_msgMem.pdu) Oscl::Pdu::Fixed(	*this,
												*this,
												(uint8_t*)&_pkt,
												sizeof(_pkt),
												sizeof(_pkt)
												);
	Oscl::Frame::Pdu::Tx::Req::Api::TxPayload*	payload;
	payload	= new (&_msgMem.payload)	Oscl::Frame::Pdu::Tx::
										Req::Api::TxPayload(*pdu);

	Oscl::Frame::Pdu::Tx::Resp::Api::TxResp*	msg;
	msg	= new (&_msgMem.resp)	Oscl::Frame::Pdu::
								Tx::Resp::Api::TxResp(	txSAP.getReqApi(),
														*this,
														myPapi,
														*payload
														);
	txSAP.post(msg->getSrvMsg());
	}

void	ReqTrans::response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::TxResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	ReqTrans::free(void* fso) noexcept{
	// do nothing
	}

/// ReplyTrans

ReplyTrans::ReplyTrans(	Service&					context,
						Oscl::Mt::Itc::PostMsgApi&	myPapi,
						Oscl::Frame::Pdu::Tx::
						Req::Api::SAP&				txSAP,
						const Oscl::Protocol::
						IEEE::MacAddress&			myMacAddress,
						const Oscl::Protocol::
						IP::Address&				myIpAddress,
						const Oscl::Protocol::
						IEEE::MacAddress&			sourceMacAddress,
						const Oscl::Protocol::
						IP::Address&				sourceIpAddress
						) noexcept:
		_context(context)
		{
	_pkt.header.destination	= sourceMacAddress;
	_pkt.header.source		= myMacAddress;
	_pkt.header.type		= 0x0806;
	_pkt.macType	= Oscl::Protocol::ARP::MacType::IEEE48Bit;
	_pkt.protoType	= Oscl::Protocol::ARP::ProtoType::IP;
	_pkt.pdu.hwAddressLength	= sizeof(Oscl::Protocol::IEEE::MacAddress);
	_pkt.pdu.protoAddressLength	= sizeof(Oscl::Protocol::IP::Address);
	_pkt.pdu.opcode				= Oscl::Protocol::ARP::Opcode::Reply;
	_pkt.pdu.srcHardwareAddress		= myMacAddress;
	_pkt.pdu.srcProtocolAddress		= myIpAddress;
	_pkt.pdu.targetHardwareAddress	= sourceMacAddress;
	_pkt.pdu.targetProtocolAddress	= sourceIpAddress;
	Oscl::Pdu::Fixed*	pdu;
	pdu	= new (&_msgMem.pdu) Oscl::Pdu::Fixed(	*this,
												*this,
												(uint8_t*)&_pkt,
												sizeof(_pkt),
												sizeof(_pkt)
												);
	Oscl::Frame::Pdu::Tx::Req::Api::TxPayload*	payload;
	payload	= new (&_msgMem.payload)	Oscl::Frame::Pdu::Tx::
										Req::Api::TxPayload(*pdu);

	Oscl::Frame::Pdu::Tx::Resp::Api::TxResp*	msg;
	msg	= new (&_msgMem.resp)	Oscl::Frame::Pdu::
								Tx::Resp::Api::TxResp(	txSAP.getReqApi(),
														*this,
														myPapi,
														*payload
														);
	txSAP.post(msg->getSrvMsg());
	}

void	ReplyTrans::response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::TxResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	ReplyTrans::free(void* fso) noexcept{
	// do nothing
	}

/// DirectReqTrans

DirectReqTrans::
DirectReqTrans(	Service&					context,
				Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Oscl::Frame::Pdu::Tx::
				Req::Api::SAP&				txSAP,
				const Oscl::Protocol::
				IEEE::MacAddress&			myMacAddress,
				const Oscl::Protocol::
				IP::Address&				myIpAddress,
				const Oscl::Protocol::
				IEEE::MacAddress&			sourceMacAddress,
				const Oscl::Protocol::
				IP::Address&				sourceIpAddress
				) noexcept:
		_context(context)
		{
	_pkt.header.destination	= sourceMacAddress;
	_pkt.header.source		= myMacAddress;
	_pkt.header.type		= 0x0806;
	_pkt.macType			= Oscl::Protocol::ARP::MacType::IEEE48Bit;
	_pkt.protoType			= Oscl::Protocol::ARP::ProtoType::IP;
	_pkt.pdu.hwAddressLength	= sizeof(Oscl::Protocol::IEEE::MacAddress);
	_pkt.pdu.protoAddressLength	= sizeof(Oscl::Protocol::IP::Address);
	_pkt.pdu.opcode				= Oscl::Protocol::ARP::Opcode::Request;
	_pkt.pdu.srcHardwareAddress		= myMacAddress;
	_pkt.pdu.srcProtocolAddress		= myIpAddress;
	_pkt.pdu.targetHardwareAddress	= sourceMacAddress;
	_pkt.pdu.targetProtocolAddress	= sourceIpAddress;
	Oscl::Pdu::Fixed*	pdu;
	pdu	= new (&_msgMem.pdu) Oscl::Pdu::Fixed(	*this,
												*this,
												(uint8_t*)&_pkt,
												sizeof(_pkt),
												sizeof(_pkt)
												);
	Oscl::Frame::Pdu::Tx::Req::Api::TxPayload*	payload;
	payload	= new (&_msgMem.payload)	Oscl::Frame::Pdu::Tx::
										Req::Api::TxPayload(*pdu);

	Oscl::Frame::Pdu::Tx::Resp::Api::TxResp*	msg;
	msg	= new (&_msgMem.resp)	Oscl::Frame::Pdu::
								Tx::Resp::Api::TxResp(	txSAP.getReqApi(),
														*this,
														myPapi,
														*payload
														);
	txSAP.post(msg->getSrvMsg());
	}

void	DirectReqTrans::response(	Oscl::Frame::Pdu::
									Tx::Resp::Api::TxResp&	msg
									) noexcept{
	_context.done(*this);
	}

void	DirectReqTrans::free(void* fso) noexcept{
	// do nothing
	}

