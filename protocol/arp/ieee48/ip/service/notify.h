/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_arp_ieee48_ip_notifyh_
#define _oscl_protocol_arp_ieee48_ip_notifyh_
#include "oscl/protocol/ip/address.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ARP {
/** */
namespace IEEE48Bit {
/** */
namespace IP {

/** This interface is used to notify an upper layer of changes
	in pending arp table entries.
	The idea is that the upper layer may want to keep a transmit
	request for which there is no ARP table entry until ARP
	has time to resolve the address or NOT.
 */
class NotifyApi {
	public:
		/** */
		virtual ~NotifyApi() {}

		/** This operation is invoked when a pending entry matching
			the "ipAddress" is removed from the table.
		 */
		virtual	void	removed(	const Oscl::Protocol::IP::
									Address						ipAddress
									) noexcept=0;

		/** This operation is invoked when a pending entry matching
			the "ipAddress" is validated in the ARP table.
		 */
		virtual	void	discovered(	const Oscl::Protocol::IP::
									Address						ipAddress
									) noexcept=0;
	};

}
}
}
}
}

#endif
