/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_arp_ieee48_ip_service_transactionh_
#define _oscl_protocol_arp_ieee48_ip_service_transactionh_
#include "oscl/memory/block.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/pdu/fixed.h"
#include "oscl/protocol/arp/ieee48/ip/pdu.h"
#include "oscl/protocol/arp/mac.h"
#include "oscl/protocol/arp/proto.h"
#include "oscl/protocol/ieee/ethernet/header.h"
#include "oscl/compiler/align.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ARP {
/** */
namespace IEEE48Bit {
/** */
namespace IP {

/** */
typedef struct MsgMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Oscl::Pdu::Fixed)>			pdu;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(	Oscl::Frame::Pdu::Tx::
							Req::Api::TxPayload
							)>						payload;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(	Oscl::Frame::Pdu::Tx::
							Resp::Api::TxResp
							)>						resp;
	}MsgMem;

/** */
class ReqTrans :	private Oscl::Frame::Pdu::Tx::Resp::Api,
					private Oscl::FreeStore::Mgr
					{
	public:
		/** */
		struct Packet {
			/** */
			Oscl::Protocol::ARP::MacType::Var		macType;
			/** */
			Oscl::Protocol::ARP::ProtoType::Var		protoType;
			/** */
			Oscl::Protocol::ARP::IEEE48Bit::IP::PDU	pdu;
			} OsclPackedPostMacro();

	public:
		/** */
		Packet					_pkt;
		/** */
		MsgMem					_msgMem;
		/** */
		class Service&			_context;

	public:
		/** */
		ReqTrans(	Service&									context,
					Oscl::Mt::Itc::PostMsgApi&					myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&		txSAP,
					const Oscl::Protocol::IEEE::MacAddress&	myMacAddress,
					const Oscl::Protocol::IP::Address&		myIpAddress,
					const Oscl::Protocol::IP::Address&		targetIpAddress
					) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Tx::Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

/** */
class DirectReqTrans :	private Oscl::Frame::Pdu::Tx::Resp::Api,
						private Oscl::FreeStore::Mgr
						{
	public:
		/** */
		struct Packet {
			/** */
			Oscl::Protocol::IEEE::Ethernet::Header	header;
			/** */
			Oscl::Protocol::ARP::MacType::Var		macType;
			/** */
			Oscl::Protocol::ARP::ProtoType::Var		protoType;
			/** */
			Oscl::Protocol::ARP::IEEE48Bit::IP::PDU	pdu;
			} OsclPackedPostMacro();

	public:
		/** */
		Packet					_pkt;
		/** */
		MsgMem					_msgMem;
		/** */
		class Service&			_context;

	public:
		/** */
		DirectReqTrans(	Service&						context,
						Oscl::Mt::Itc::PostMsgApi&		myPapi,
						Oscl::Frame::Pdu::
						Tx::Req::Api::SAP&				txSAP,
						const Oscl::Protocol::
						IEEE::MacAddress&				myMacAddress,
						const Oscl::Protocol::
						IP::Address&					myIpAddress,
						const Oscl::Protocol::
						IEEE::MacAddress&				sourceMacAddress,
						const Oscl::Protocol::
						IP::Address&					sourceIpAddress
						) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Tx::Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

/** */
class ReplyTrans :	private Oscl::Frame::Pdu::Tx::Resp::Api,
					private Oscl::FreeStore::Mgr
					{
	public:
		/** */
		struct Packet {
			/** */
			Oscl::Protocol::IEEE::Ethernet::Header	header;
			/** */
			Oscl::Protocol::ARP::MacType::Var		macType;
			/** */
			Oscl::Protocol::ARP::ProtoType::Var		protoType;
			/** */
			Oscl::Protocol::ARP::IEEE48Bit::IP::PDU	pdu;
			}OsclPackedPostMacro();
	public:
		/** */
		Packet					_pkt;
		/** */
		MsgMem					_msgMem;
		/** */
		class Service&			_context;

	public:
		/** */
		ReplyTrans(	Service&								context,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	txSAP,
					const Oscl::Protocol::IEEE::MacAddress&	myMacAddress,
					const Oscl::Protocol::IP::Address&		myIpAddress,
					const Oscl::Protocol::IEEE::MacAddress&	sourceMacAddress,
					const Oscl::Protocol::IP::Address&		sourceIpAddress
					) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Tx::Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
#endif
