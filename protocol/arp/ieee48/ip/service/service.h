/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_arp_ieee48_ip_serviceh_
#define _oscl_protocol_arp_ieee48_ip_serviceh_
#include "oscl/memory/block.h"
#include "oscl/mt/runnable.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/frame/pdu/rx/respapi.h"
#include "oscl/pdu/fixed.h"
#include "oscl/protocol/arp/ieee48/ip/pdu.h"
#include "oscl/protocol/arp/mac.h"
#include "oscl/protocol/arp/proto.h"
#include "transaction.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/protocol/ip/addr/enet/resolv/api.h"
#include "oscl/mt/itc/delay/respapi.h"
#include "notify.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ARP {
/** */
namespace IEEE48Bit {
/** */
namespace IP {

/** */
class Service :	public Oscl::FreeStore::Mgr,
				public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Frame::Pdu::Tx::Req::Api,
				private Oscl::Frame::Pdu::Rx::Resp::Api,
				private Oscl::Protocol::IP::TX::Srv::Req::Api,
				private Oscl::Mt::Itc::Delay::Resp::Api,
				public Oscl::Protocol::IP::Addr::ENET::Resolv::Api
				{
	private:
		friend class ReqTrans;
		friend class DirectReqTrans;
		friend class ReplyTrans;

	public:
		/** */
		union ArpTransMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(ReqTrans)>		_req;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(ReplyTrans)>		_resp;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(DirectReqTrans)>	_direct;
			};

	private:
		/** */
		struct RxRespMem {
			/** */
			Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Frame::Pdu::Rx::Req::Api::RxPayload)>	payload;
			/** */
			Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Frame::Pdu::Rx::Resp::Api::RxResp)>	resp;
			};

	public:
		/** */
		union RxMem {
			/** */
			void*		__qitemlink;
			/** */
			RxRespMem	rxRespMem;
			};

	private:

		/** */
		struct Binding {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Protocol::IP::Address			ipAddr;
			/** */
			Oscl::Protocol::IEEE::MacAddress	macAddr;
			/** */
			enum{nSecondsBeforeAgeDeath=60};
			/** This value MUST be larger than nSecondsBeforeAgeDeath.
			 */
			enum{initialSecondsBeforeRefresh=nSecondsBeforeAgeDeath+2};
			/** This value MUST be larger than initialSecondsBeforeRefresh.
			 */
			enum{maxRefreshValue=10*nSecondsBeforeAgeDeath};
			/** */
			unsigned							_lastRefreshValue;
			/** */
			unsigned							_secondsBeforeRefresh;
			/** */
			enum{maxRetries=3};
			/** */
			unsigned							_nRetriesRemaining;
			/** */
			unsigned							_ageSecondsRemaining;
			/** */
			bool								_macAddressIsValid;
	
			/** */
			Binding(	const Oscl::Protocol::IP::Address&		ipAddr,
						const Oscl::Protocol::IEEE::MacAddress&	macAddr
						) noexcept;
	
			/** */
			Binding(const Oscl::Protocol::IP::Address&	ipAddr) noexcept;
	
			/** This operation is invoked when a successful arp reply
				or request is received with the appropriate protocol
				address. Causes refresh sequence to be restarted.
			 */
			void	refresh(const Oscl::Protocol::IEEE::MacAddress& macAddr) noexcept;
	
			/** */
			void	resetAging() noexcept;

			/** Returns true if binding has expired.
				This should be called first each timer expiry.
			 */
			bool	readyToRemove() noexcept;

			/** Returns true if the binding should be removed.
				This should be called if readyToRemove() returns false.
				Indicates an ARP request needs to be sent for this binding.
			 */
			bool	needsRefresh() noexcept;

			/** Should be called at the end of every timer period
				unless readyToRemove() returns true.
			 */
			void	tick() noexcept;
			};

	public:
		/** */
		union BindMem {
			/** */
			void*											__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock< sizeof(Binding) >	_bindMem;
			};

	private:
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::DelayPayload	_delayPayload;
		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::DelayResp		_delayResp;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::CancelPayload	_delayCancelPayload;
		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::CancelResp		_delayCancelResp;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&			_brodcastSAP;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&			_rawTxSAP;
		/** */
		Oscl::Frame::Pdu::Rx::Req::Api::SAP&			_rxSAP;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&			_delaySAP;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::ConcreteSAP		_arpSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		const Oscl::Protocol::IP::Address&			_myIpAddress;
		/** */
		NotifyApi&									_pendMonitor;
		/** */
		Oscl::Queue<ArpTransMem>					_freeTransMem;
		/** */
		Oscl::Queue<RxMem>							_freeRxMem;
		/** */
		Oscl::Queue<BindMem>						_freeBindings;
		/** List of active IP/MAC address bindings.
		 */
		Oscl::Queue<Binding>						_bindings;
		/** */
		bool										_open;
		/** */
		CloseReq*									_closeReq;

		/** */
		friend class Binding;
	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	brodcastSAP,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	rawTxSAP,
					Oscl::Frame::Pdu::Rx::Req::Api::SAP&	rxSAP,
					Oscl::Mt::Itc::
					Delay::Req::Api::SAP&					delaySAP,
					const Oscl::Protocol::IP::Address&		myIpAddress,
					NotifyApi&								pendMonitor,
					RxMem									rxMem[],
					unsigned								nRxRequests,
					ArpTransMem								transMem[],
					unsigned								nReqTranss,
					BindMem									bindMem[],
					unsigned								nBindMem
					) noexcept;

		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&	getTxSAP() noexcept;

	private:
		/** */
		void		free(Binding* binding) noexcept;
		/** */
		Binding*	allocBinding(	const Oscl::Protocol::IP::Address&		ipAddr,
									const Oscl::Protocol::IEEE::MacAddress&	macAddr
									) noexcept;
		/** */
		Binding*	allocBinding(const Oscl::Protocol::IP::Address& ipAddr) noexcept;

		/** */
		void	learnBinding(	const Oscl::Protocol::IP::Address&		ipAddr,
								const Oscl::Protocol::IEEE::MacAddress&	macAddr
								) noexcept;
		/** */
		Binding*	lookup(	const Oscl::Protocol::IP::Address&	ipAddr
							) const noexcept;
		/** */
		void	restartTimer() noexcept;
		/** */
		void	oneSecondProtocolTimerExpired() noexcept;

	private: // Oscl::Protocol::IP::Addr::ENET::Resolv::Api
		/** */
		bool	resolve(	const Oscl::Protocol::IP::Address&	ipAddr,
							Oscl::Protocol::IEEE::MacAddress&	macAddr
							) noexcept;

	private:
		/** */
		void	sendArpReq(const Oscl::Protocol::IP::Address& dest) noexcept;

		/** */
		void	sendArpReq(	const Oscl::Protocol::IP::Address&		dest,
							const Oscl::Protocol::IEEE::MacAddress&	macAddr
							) noexcept;

		/** */
		void	sendArpReply(	const Oscl::Protocol::ARP::
								IEEE48Bit::IP::PDU&				request
								) noexcept;

		/** */
		void	processArpReply(	const Oscl::Protocol::ARP::
									IEEE48Bit::IP::PDU&			reply
									) noexcept;

		/** */
		void	done(ReqTrans& trans) noexcept;

		/** */
		void	done(ReplyTrans& trans) noexcept;

		/** */
		void	done(DirectReqTrans& trans) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;

	private: // Close::Req::Api
		void	request(CloseReq& msg) noexcept;

	private: // Open::Req::Api
		void	request(OpenReq& msg) noexcept;

	private: // Oscl::Frame::Pdu::Tx::Req::Api
		/** */
		void	request(Oscl::Frame::Pdu::Tx::Req::Api::TxReq&	msg) noexcept;

	private: // Oscl::Protocol::IP::TX::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::
							IP::TX::Srv::Req::Api::SendReq&	msg
							) noexcept;

	private: // Oscl::Frame::Pdu::Rx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Rx::Resp::Api::RxResp& msg) noexcept;
		/** */
		void	response(	Oscl::Frame::Pdu::Rx::
							Resp::Api::CancelAllResp& msg
							) noexcept;
	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::
							Delay::Resp::Api::DelayResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::
							Delay::Resp::Api::CancelResp&	msg
							) noexcept;
	};

class Stimulator :	public Oscl::Mt::Runnable,
					private Oscl::FreeStore::Mgr
					{
	private:
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&			_txSAP;
	public:
		/** */
		Stimulator(	Oscl::Frame::Pdu::Tx::
					Req::Api::SAP&				txSAP
					) noexcept;
	public:	// Runnable
		/** */
		void	run() noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
#endif
