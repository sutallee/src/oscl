/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_subsys_srv_rx_parth_
#define _oscl_protocol_ip_subsys_srv_rx_parth_
#include "oscl/protocol/ip/rx/srv/if/sync.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Subsystem {
/** */
namespace Srv {
/** */
namespace RX {

/** */
class Part :	public Oscl::Protocol::IP::RX::Srv::IF::Req::Api,
				public Oscl::Protocol::IP::RX::Srv::IF::Sync
				{
	private:
		/** */
		Oscl::Protocol::IP::
		RX::Srv::IF::Req::Api::ConcreteSAP			_sap;

		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						RX::IF::RxInterface
						>							_activeInterfaces;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi
				) noexcept;

		/** */
		Oscl::Protocol::IP::
		RX::Srv::IF::Req::Api::SAP&	getSAP() noexcept;

	public: // Oscl::Protocol::IP::RX::Srv::IF::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							RX::Srv::IF::Req::Api::AttachReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							RX::Srv::IF::Req::Api::DetachReq&	msg
							) noexcept;
	};

}
}
}
}
}
}

#endif
