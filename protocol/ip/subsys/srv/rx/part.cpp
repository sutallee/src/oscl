/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Protocol::IP::Subsystem::Srv::RX;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi
			) noexcept:
		Sync(_sap),
		_sap(*this,myPapi)
		{
	}

Oscl::Protocol::IP::RX::Srv::IF::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::request(	Oscl::Protocol::IP::
						RX::Srv::IF::Req::Api::AttachReq&	msg
						) noexcept{
	_activeInterfaces.put(&msg._payload._interface);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						RX::Srv::IF::Req::Api::DetachReq&	msg
						) noexcept{
	_activeInterfaces.remove(&msg._payload._interface);
	// FIXME: flush interface such that all TX requests
	// are completed before calling return to sender.
	// Of course this implies that the interface has
	// to support the call.
	// Alternately, it may be the responsibility of the Monitor::Part
	// to do this *after* this request completes.
	msg.returnToSender();
	}

