/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_subsys_srv_fwd_parth_
#define _oscl_protocol_ip_subsys_srv_fwd_parth_
#include "oscl/memory/block.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/protocol/ip/fwd/srv/if/sync.h"
#include "oscl/protocol/ip/tx/srv/sync.h"
#include "oscl/queue/queue.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Subsystem {
/** */
namespace Srv {
/** */
namespace FWD {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Trans) >	trans;
	};

/** */
class Part :	private Oscl::Protocol::IP::FWD::Srv::IF::Req::Api,
				private	Oscl::Protocol::IP::TX::Srv::Req::Api
				{
	private:
		/** */
		friend class Trans;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;

		/** */
		Oscl::Protocol::IP::
		FWD::Srv::IF::Req::Api::ConcreteSAP			_registrarSAP;

		/** */
		Oscl::Protocol::IP::
		TX::Srv::Req::Api::ConcreteSAP				_fwdSAP;

		/** */
		Oscl::Protocol::IP::FWD::Srv::IF::Sync		_registrarSync;

		/** */
		Oscl::Protocol::IP::TX::Srv::Sync			_fwdSync;

		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						FWD::IF::FwdInterface
						>							_activeInterfaces;

		/** */
		Oscl::Queue<TransMem>						_freeTransMem;

		/** */
		struct {
			/** */
			unsigned long	nSendReqs;
			/** */
			unsigned long	nDroppedSendReqs;
			/** */
			unsigned long	nUnreachableSendReqs;
			} _stats;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				TransMem					transMem[],
				unsigned					nTransMem
				) noexcept;

		/** */
		void	done(Trans& trans) noexcept;

		/** */
		Oscl::Protocol::IP::
		FWD::Srv::IF::Req::Api::SAP&		getRegistrarSAP() noexcept;

		/** */
		Oscl::Protocol::IP::FWD::IF::Api&	getRegistrarApi() noexcept;

		/** */
		Oscl::Protocol::IP::
		TX::Srv::Req::Api::SAP&				getFwdSAP() noexcept;

		/** */
		Oscl::Protocol::IP::FWD::Api&		getFwdApi() noexcept;

	public: // Oscl::Protocol::IP::FWD::Srv::IF::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							FWD::Srv::IF::Req::Api::AttachReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							FWD::Srv::IF::Req::Api::DetachReq&	msg
							) noexcept;

	private: // Oscl::Protocol::IP::TX::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							TX::Srv::Req::Api::SendReq&	msg
							) noexcept;

	private:
		/** This operation looks for the interface identified by the
			specified IP address in the "routing" table. If a match
			is found, and the IP address is on a locally/directly attached
			network, then the interface is returned and the destination
			address remains unmodified. If a match is found and the
			destination address is not on a locally/directly attached
			network, then the destination address is modified to the
			value of an appropriate gateway, and the interface corresponding
			to the gateway is returned. Otherwise, no interface is
			returned.
		 */
		Oscl::Protocol::IP::
		IF::TX::Srv::Api*	lookup(	Oscl::Protocol::IP::
									Address&				destination
									) noexcept;
	};

}
}
}
}
}
}

#endif
