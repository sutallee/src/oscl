/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::Protocol::IP::Subsystem::Srv::FWD;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			TransMem					transMem[],
			unsigned					nTransMem
			) noexcept:
		_myPapi(myPapi),
		_registrarSAP(*this,myPapi),
		_fwdSAP(*this,myPapi),
		_registrarSync(_registrarSAP),
		_fwdSync(_fwdSAP)
		{
	_stats.nSendReqs			= 0;
	_stats.nDroppedSendReqs		= 0;
	_stats.nUnreachableSendReqs	= 0;
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

void	Part::done(Trans& trans) noexcept{
	trans.~Trans();
	_freeTransMem.put((TransMem*)&trans);
	}

Oscl::Protocol::IP::
FWD::Srv::IF::Req::Api::SAP&	Part::getRegistrarSAP() noexcept{
	return _registrarSAP;
	}

Oscl::Protocol::IP::FWD::IF::Api&	Part::getRegistrarApi() noexcept{
	return _registrarSync;
	}

Oscl::Protocol::IP::
TX::Srv::Req::Api::SAP&			Part::getFwdSAP() noexcept{
	return _fwdSAP;
	}

Oscl::Protocol::IP::FWD::Api&	Part::getFwdApi() noexcept{
	return _fwdSync;
	}

void	Part::request(	Oscl::Protocol::IP::
						FWD::Srv::IF::Req::Api::AttachReq&	msg
						) noexcept{
	_activeInterfaces.put(&msg._payload._interface);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						FWD::Srv::IF::Req::Api::DetachReq&	msg
						) noexcept{
	_activeInterfaces.remove(&msg._payload._interface);
	// FIXME: flush interface such that all TX requests
	// are completed before calling return to sender.
	// Of course this implies that the interface has
	// to support the call.
	// Alternately, it may be the responsibility of the Monitor::Part
	// to do this *after* this request completes.
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						TX::Srv::Req::Api::SendReq&	msg
						) noexcept{
	++_stats.nSendReqs;
	TransMem*	mem	= _freeTransMem.get();
	if(!mem){
		++_stats.nDroppedSendReqs;
		msg.returnToSender();
		return;
		}

	Oscl::Protocol::IP::IF::TX::Srv::Api*
	interface	= lookup(msg._payload._destination);

	if(!interface){
		// Destination un-reachable.
		// Either there were no interfaces attached
		// or there was no default gateway configured
		// and no other gateway configured.
		++_stats.nUnreachableSendReqs;
		msg.returnToSender();
		return;
		}

	// OK, forward that message!
	new(mem) Trans(	*this,
					_myPapi,
					interface->getDirectSAP(),
					msg
					);
	}

Oscl::Protocol::IP::
IF::TX::Srv::Api*	Part::lookup(	Oscl::Protocol::IP::
									Address&				destination
									) noexcept{
	// FIXME: There *must* be a proper lookup done
	// using the IP address in the packet
	Oscl::Protocol::IP::FWD::IF::FwdInterface*
	interface	= _activeInterfaces.first();
	if(!interface) return 0;
	return &interface->interface;
	}

