/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Protocol::IP::Subsystem::Srv::TCP;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
			Oscl::Protocol::IP::
			Port::Mux::Srv::Registrar::TCP::TransMem	regTransMem[],
			unsigned									nRegTransMem
			) noexcept:
		_mux(	myPapi,
				ipFwdSAP,
				regTransMem,
				nRegTransMem
				),
		_rx(myPapi)
		{
	}

Oscl::Protocol::IP::RX::IF::Api&	Part::getIpRx() noexcept{
	return _rx;
	}

Oscl::Protocol::IP::Mux::Client::Receiver&	Part::getReceiver() noexcept{
	return _mux;
	}

Oscl::Protocol::IP::RX::Srv::IF::Req::Api::SAP&	Part::getIpRxSAP() noexcept{
	return _rx.getSAP();
	}

Oscl::Protocol::IP::
Port::Mux::Registrar::Srv::Req::Api::SAP&	Part::getMuxSAP() noexcept{
	return _mux.getSAP();
	}

Oscl::Protocol::IP::Port::Mux::Registrar::Api&	Part::getMuxApi() noexcept{
	return _mux.getSyncApi();
	}

