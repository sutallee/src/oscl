/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Protocol::IP::Subsystem::Srv::Basic;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			fwdPapi,
			Oscl::Mt::Itc::PostMsgApi&			udpRxPapi,
			Oscl::Mt::Itc::PostMsgApi&			tcpRxPapi,
			Oscl::Protocol::IP::
			Subsystem::Srv::FWD::TransMem		fwdTransMem[],
			unsigned							nFwdTransMem,
			Oscl::Protocol::IP::Port::
			Mux::Srv::Registrar::TCP::TransMem	tcpRegTransMem[],
			unsigned							nTcpRegTransMem
			) noexcept:
		_fwdSubsytem(	fwdPapi,
						fwdTransMem,
						nFwdTransMem
						),
		_udpRxSubsystem(udpRxPapi),
		_tcpRxSubsystem(	tcpRxPapi,
							_fwdSubsytem.getFwdSAP(),
							tcpRegTransMem,
							nTcpRegTransMem
							)
		{
	}

Oscl::Protocol::IP::
Subsystem::RX::Srv::UDP::Api&	Part::getUdpRxSubsystem() noexcept{
	return _udpRxSubsystem;
	}

Oscl::Protocol::IP::
Subsystem::RX::Srv::TCP::Api&	Part::getTcpRxSubsystem() noexcept{
	return _tcpRxSubsystem;
	}

Oscl::Protocol::IP::FWD::IF::Api&	Part::getIpFwd() noexcept{
	return _fwdSubsytem.getRegistrarApi();
	}

Oscl::Protocol::IP::
FWD::Srv::IF::Req::Api::SAP&	Part::getIpFwdSAP() noexcept{
	return _fwdSubsytem.getRegistrarSAP();
	}

Oscl::Protocol::IP::
TX::Srv::Req::Api::SAP&		Part::getIpFwdTxSAP() noexcept{
	return _fwdSubsytem.getFwdSAP();
	}

Oscl::Protocol::IP::FWD::Api&	Part::getIpFwdTxApi() noexcept{
	return _fwdSubsytem.getFwdApi();
	}

Oscl::Protocol::IP::
Subsystem::RX::Srv::Api&		Part::getUdpRxSrv() noexcept{
	return _udpRxSubsystem;
	}

Oscl::Protocol::IP::
Subsystem::RX::Srv::Api&		Part::getTcpRxSrv() noexcept{
	return _tcpRxSubsystem;
	}

