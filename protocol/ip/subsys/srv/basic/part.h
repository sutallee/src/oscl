/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_subsys_srv_basic_parth_
#define _oscl_protocol_ip_subsys_srv_basic_parth_
#include "oscl/protocol/ip/subsys/srv/api.h"
#include "oscl/protocol/ip/subsys/srv/udp/part.h"
#include "oscl/protocol/ip/subsys/srv/tcp/part.h"
#include "oscl/protocol/ip/subsys/srv/fwd/part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Subsystem {
/** */
namespace Srv {
/** */
namespace Basic {

/** */
class Part : public Oscl::Protocol::IP::Subsystem::Srv::Api {
	private:
		/** */
		Oscl::Protocol::IP::Subsystem::Srv::FWD::Part	_fwdSubsytem;
		/** */
		Oscl::Protocol::IP::Subsystem::Srv::UDP::Part	_udpRxSubsystem;
		/** */
		Oscl::Protocol::IP::Subsystem::Srv::TCP::Part	_tcpRxSubsystem;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			fwdPapi,
				Oscl::Mt::Itc::PostMsgApi&			udpRxPapi,
				Oscl::Mt::Itc::PostMsgApi&			tcpRxPapi,
				Oscl::Protocol::IP::
				Subsystem::Srv::FWD::TransMem		fwdTransMem[],
				unsigned							nFwdTransMem,
				Oscl::Protocol::IP::Port::
				Mux::Srv::Registrar::TCP::TransMem	tcpRegTransMem[],
				unsigned							nTcpRegTransMem
				) noexcept;

		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::UDP::Api&	getUdpRxSubsystem() noexcept;

		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::TCP::Api&	getTcpRxSubsystem() noexcept;

	public:	// Oscl::Protocol::IP::Subsystem::Srv::Api
		/** */
		Oscl::Protocol::IP::
		FWD::Srv::IF::Req::Api::SAP&		getIpFwdSAP() noexcept;

		/** */
		Oscl::Protocol::IP::FWD::IF::Api&	getIpFwd() noexcept;

		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::Api&			getUdpRxSrv() noexcept;

		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::Api&			getTcpRxSrv() noexcept;

		/** */
		Oscl::Protocol::IP::
		TX::Srv::Req::Api::SAP&				getIpFwdTxSAP() noexcept;

		/** */
		Oscl::Protocol::IP::FWD::Api&		getIpFwdTxApi() noexcept;
	};

}
}
}
}
}
}

#endif
