/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_subsys_srv_udp_parth_
#define _oscl_protocol_ip_subsys_srv_udp_parth_
#include "oscl/protocol/ip/port/mux/srv/registrar/udp/part.h"
#include "oscl/protocol/ip/subsys/rx/srv/udp/api.h"
#include "oscl/protocol/ip/subsys/srv/rx/part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Subsystem {
/** */
namespace Srv {
/** */
namespace UDP {

/** This part implements the IP UDP RX Subsystem.
	Eventually, it will have a UDP Port MUX Registrar
	interface with which UDP sockets can register to
	attach a UDP server for a particular UDP port.
 */
class Part :	public	Oscl::Protocol::IP::Subsystem::RX::Srv::UDP::Api {
	private:
		/** */
		Oscl::Protocol::IP::
		Port::Mux::Srv::Registrar::UDP::Part	_mux;

		/** */
		Oscl::Protocol::IP::
		Subsystem::Srv::RX::Part				_rx;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi
				) noexcept;

	private: // Oscl::Protocol::IP::Subsystem::RX::Srv::UDP::Api
		/** */
		Oscl::Protocol::IP::RX::IF::Api&	getIpRx() noexcept;

		/** */
		Oscl::Protocol::IP::
		Mux::Client::Receiver&				getReceiver() noexcept;

		/** */
		Oscl::Protocol::IP::
		RX::Srv::IF::Req::Api::SAP&			getIpRxSAP() noexcept;

		/** */
		Oscl::Protocol::IP::
		Port::Mux::Registrar::Srv::Req::Api::SAP&	getMuxSAP() noexcept;

		/** */
		Oscl::Protocol::IP::
		Port::Mux::Registrar::Api&					getMuxApi() noexcept;
	};

}
}
}
}
}
}

#endif
