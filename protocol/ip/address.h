/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_addressh_
#define _oscl_protocol_ip_addressh_
#include "oscl/endian/be.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {

/** */
struct Address {
	public:
		/** The actual big endian value */
//		Oscl::Endian::Big::U32	_v;
		Oscl::Endian::BEU32		_v;
	public:
		/** */
		inline operator uint32_t() const noexcept{
			return _v;
			}

		/** */
		Address&	set(const char *s) noexcept;
		/** */
		inline Address&	assign(unsigned long addr) noexcept{
			_v	= addr;
			return *this;
			}
		/** */
		inline bool operator==(const Address& other) const noexcept{
			return _v == other._v;
			}
		/** */
//		inline bool operator==(const Oscl::Endian::BEU32& other) const noexcept{
//			return _v == other;
//			}
	};

class AddressInit : public Address {
	public:
		AddressInit(const char* s) noexcept{
			Oscl::Protocol::IP::Address::set(s);
			}
		AddressInit(unsigned long addr) noexcept{
			_v	= addr;
			}
	};
}
}
}

#endif
