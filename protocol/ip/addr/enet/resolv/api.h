/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_addr_enet_resolv_apih_
#define _oscl_protocol_ip_addr_enet_resolv_apih_
#include "oscl/protocol/ip/address.h"
#include "oscl/protocol/ieee/mac.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Addr {
/** */
namespace ENET {
/** */
namespace Resolv {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** Returns true and sets the "macAddr" value if a MAC
			address corresponding to the "ipAddr" is found.
			Returns false and does not modify "macAddr" if
			no match can be found. Matches can include gateway
			addresses if configured.
		 */
		virtual bool	resolve(	const Oscl::Protocol::IP::Address&	ipAddr,
									Oscl::Protocol::IEEE::MacAddress&	macAddr
									) noexcept=0;
	};

}
}
}
}
}
}

#endif
