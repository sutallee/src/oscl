/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_rx_apih_
#define _oscl_protocol_ip_rx_apih_
#include "oscl/protocol/ip/header.h"
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace RX {

/** This interface describes the mechanism for passing
	received IP PDUs between IP layer peers.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** This operation transfers the IP PDU to a peer
			processing subsystem. The PDU is a fully formed
			IP PDU with a header that is validated. Responsiblity
			for the release of the PDU is transferred to the
			implementation peer, and thus the implementation
			must assign the PDU to a Handle before the operation
			completes if it would keep the copy beyond the
			lifespan of the operation.
			A copy of the IP header for the PDU has been copied into 
			the "ipHeader" argument by the caller, and may be
			modified and/or copied by the implementation.
		 */
		virtual void	transfer(	Oscl::Pdu::Pdu&				ipPDU,
									Oscl::Protocol::IP::Header&	ipHeader
									) noexcept=0;
	};

}
}
}
}

#endif
