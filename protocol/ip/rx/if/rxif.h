/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_rx_if_rxifh_
#define _oscl_protocol_ip_rx_if_rxifh_
#include "oscl/queue/queueitem.h"
#include "oscl/protocol/ip/if/rx/srv/api.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace RX {
/** */
namespace IF {

/** This class is used by the IP receive subsystem to
	keep track of its attached interfaces. The QueueItem
	is exclusively for use by the IP receive server.
 */
class RxInterface : public Oscl::QueueItem {
	public:
		/** The actual interface being attached.
		 */
		Oscl::Protocol::IP::IF::RX::Srv::Api&	interface;
	public:
		/** */
		RxInterface(Oscl::Protocol::IP::IF::RX::Srv::Api& interface) noexcept;
	};

}
}
}
}
}

#endif
