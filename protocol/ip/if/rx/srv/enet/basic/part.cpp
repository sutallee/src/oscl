/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/iana.h"

using namespace Oscl::Protocol::IP::IF::RX::Srv::ENET::Basic;

class Dummy :	public Oscl::Protocol::IP::RX::Api
				{
	private:	// Oscl::Protocol::IP::RX::Api,
		void	transfer(	Oscl::Pdu::Pdu&				ipPDU,
							Oscl::Protocol::IP::Header&	ipHeader
							) noexcept{
					}
	};

static Dummy	dummy;


Part::Part(	Oscl::Mt::Itc::PostMsgApi&					rxIfPapi,
			Oscl::Mt::Itc::PostMsgApi&					rxFramerPapi,
			Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	rxFramer,
			Oscl::Protocol::IP::Addr::Local::Api&		localAddrApi,
			Oscl::Protocol::IP::
			IF::RX::Srv::Client::TransMem				rxCliTransMem[],
			unsigned									nRxCliTransMem
			) noexcept:
		_ipRxMuxServer(rxIfPapi),
		_ipProtocolRx(rxFramerPapi,Oscl::Protocol::IANA::DodIp),
		_rxClient(	rxIfPapi,
					_ipProtocolRx.getRxSAP(),
					localAddrApi,
					dummy,
					_ipRxMuxServer,
					rxCliTransMem,
					nRxCliTransMem
					),
		_rxFramer(rxFramer)
		{
	}

void	Part::syncOpen() noexcept{
	_ipProtocolRx.syncOpen();
	_rxClient.syncOpen();
	_rxFramer.attach(_ipProtocolRx);
	}

void	Part::syncClose() noexcept{
	_rxFramer.detach(_ipProtocolRx);
	_rxClient.syncClose();
	_ipProtocolRx.syncClose();
	}

Oscl::Protocol::IP::Mux::Registrar::Api&	Part::getMuxSyncApi() noexcept{
	return _ipRxMuxServer.getSyncApi();
	}

Oscl::Protocol::IP::
Mux::Registrar::Srv::Req::Api::SAP&	Part::getMuxSAP() noexcept{
	return _ipRxMuxServer.getSAP();
	}

