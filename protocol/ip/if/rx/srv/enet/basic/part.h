/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_rx_srv_enet_basic_parth_
#define _oscl_protocol_ip_if_rx_srv_enet_basic_parth_
#include "oscl/protocol/ip/if/rx/srv/api.h"
#include "oscl/frame/pdu/rx/protomux/driver/driver.h"
#include "oscl/protocol/ip/if/rx/srv/client/config.h"
#include "oscl/frame/pdu/rx/protomux/syncapi.h"
#include "oscl/protocol/ip/mux/srv/registrar/part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace RX {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Basic {

class Part :	public Oscl::Protocol::IP::IF::RX::Srv::Api,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi
				{
	private:
		/** Parsite that allows for different IP protocols to
			attach to capture their packets (e.g. UDP/TCP).
			This driver is attached procedurally to the
			IP protocol discriminator.
		 */
		Oscl::Protocol::IP::Mux::Srv::Registrar::Part	_ipRxMuxServer;

		/** This parasitic protocol receiver is the LLC server
			for IP PDs.
		 */
		Oscl::Frame::Pdu::Rx::ProtoMux::Driver			_ipProtocolRx;

		/** */
		Oscl::Protocol::IP::IF::RX::Srv::Client::Part	_rxClient;

		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&		_rxFramer;


	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					rxIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					rxFramerPapi,
				Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	rxFramer,
				Oscl::Protocol::IP::Addr::Local::Api&		localAddrApi,
				Oscl::Protocol::IP::
				IF::RX::Srv::Client::TransMem				rxCliTransMem[],
				unsigned									nRxCliTransMem
				) noexcept;
		/** */

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	public:	// Oscl::Protocol::IP::IF::RX::Srv::Api
		/** Returns API used to attach and detach IP receivers.
		 */
		Oscl::Protocol::IP::
		Mux::Registrar::Api&				getMuxSyncApi() noexcept;

		/** Returns SAP used to attach and detach IP receivers.
		 */
		Oscl::Protocol::IP::
		Mux::Registrar::Srv::Req::Api::SAP&	getMuxSAP() noexcept;
	};

}
}
}
}
}
}
}
}

#endif
