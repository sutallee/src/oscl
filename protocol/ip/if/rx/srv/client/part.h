/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_rx_srv_client_parth_
#define _oscl_protocol_ip_if_rx_srv_client_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/frame/pdu/rx/respmem.h"
#include "oscl/queue/queue.h"
#include "oscl/protocol/ip/rx/api.h"
#include "oscl/protocol/ip/addr/local/api.h"
#include "transaction.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace RX {
/** */
namespace Srv {
/** */
namespace Client {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock<sizeof(Transaction)>	trans;
	};

/** This concrete class implements a client service for
	receiving IP PDUs from an LLC layer.
	In addition to managing ITC receive resources, this
	class also:
		* validates the IP header
		* discriminates between local and remote IP destinations.
	If any of the validation tests fail, the PDU is discarded.

	FIXME: Issues:
		What about various IP options?
			* Source route?
			* Security (11 bytes)
			* Loose Source Routing (var bytes)
			* Strict Source Routing (var bytes)
			* Record Route (var bytes)
			* Stream ID (4 bytes)
			* Internet Timestamp (var bytes)
 */
class Part :	public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Frame::Pdu::Rx::Resp::Api
				{
	private:
		/** */
		friend class Transaction;

	private:
		/** */
		Oscl::Frame::Pdu::Rx::Resp::CancelMem	_rxCancelMem;

		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;

		/** Refers to the lower layer IP protocol LLC receive server.
		 */
		Oscl::Frame::Pdu::Rx::Req::Api::SAP&	_ipLlcRxSAP;

		/** Refers to an interface that matches local addresses.
		 */
		Oscl::Protocol::IP::Addr::Local::Api&	_localAddressMatcher;

		/** Refers to an interface that forwards PDUs with
			remote destination addresses.
		 */
		Oscl::Protocol::IP::RX::Api&			_ipRoute;

		/** Refers to an interface that forwards PDUs with
			local destination addresses.
		 */
		Oscl::Protocol::IP::RX::Api&			_ipLocal;

		/** */
		Oscl::Queue<TransMem>					_freeTransMem;

		/** */
		bool											_open;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&				papi,
				Oscl::Frame::Pdu::Rx::Req::Api::SAP&	ipLlcRxSAP,
				Oscl::Protocol::IP::Addr::Local::Api&	localAddressMatcher,
				Oscl::Protocol::IP::RX::Api&			ipRoute,
				Oscl::Protocol::IP::RX::Api&			ipLocal,
				TransMem								transMem[],
				unsigned								nTransMem
				) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Frame::Pdu::Rx::Resp::Api
		/** This operation is never invoked in this class.  */
		void	response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::RxResp&	msg
							) noexcept;

		/** This operation is invoked when a cancel
			operation is pending after all RX PDUs for
			the lower layer are canceled. Such a cancellation
			is typical when this service receives a close
			request from an upper layer.
		 */
		void	response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::CancelAllResp&	msg
							) noexcept;

	private:
		/** */
		void	sendIpLlcRxClose() noexcept;

		/** */
		void	free(Transaction& trans) noexcept;
	};

}
}
}
}
}
}
}
#endif
