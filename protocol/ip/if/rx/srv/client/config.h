/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_rx_srv_client_configh_
#define _oscl_protocol_ip_if_rx_srv_client_configh_
#include <new>
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace RX {
/** */
namespace Srv {
/** */
namespace Client {

/* */
template <unsigned nTransactions>
class Config {
	private:
		/** */
		TransMem								_transMem[nTransactions];
		/** */
		Oscl::Memory::AlignedBlock< sizeof(Part) >	_partMem;

	public:
		/** */
		Part&										_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&				papi,
				Oscl::Frame::Pdu::Rx::Req::Api::SAP&	ipLlcRxSAP,
				Oscl::Protocol::IP::Addr::Local::Api&	localAddressMatcher,
				Oscl::Protocol::IP::RX::Api&			ipRoute,
				Oscl::Protocol::IP::RX::Api&			ipLocal
				) noexcept:
				_part(	*new (&_partMem)
						Part(	papi,
								ipLlcRxSAP,
								localAddressMatcher,
								ipRoute,
								ipLocal,
								_transMem,
								nTransactions
								)
						)
				{}
	};

}
}
}
}
}
}
}

#endif
