/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_rx_srv_client_transactionh_
#define _oscl_protocol_ip_if_rx_srv_client_transactionh_
#include "oscl/frame/pdu/rx/respmem.h"
#include "oscl/protocol/ip/rx/api.h"
#include "oscl/protocol/ip/addr/local/api.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace RX {
/** */
namespace Srv {
/** */
namespace Client {

/** */
class Part;

/** This class handles memory management and behavior
	for a single receive PDU transaction.
 */
class Transaction :	public Oscl::Frame::Pdu::Rx::Resp::Api {
	private:
		/** */
		Part&									_context;

		/** */
		Oscl::Frame::Pdu::Rx::Resp::RxMem		_rxMem;

	public:
		/** */
		Transaction(Part& context) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api

	private:	// Oscl::Frame::Pdu::Rx::Resp::Api
		/** This operation is invoked when an IP PDU
			is received from the lower layer.
		 */
		void	response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::RxResp&	msg
							) noexcept;

		/** This operation is not used in this class, since
			this class will never issue a cancel request.
		 */
		void	response(	Oscl::Frame::Pdu::
							Rx::Resp::Api::CancelAllResp&	msg
							) noexcept;

	private:
		/** */
		void	sendRxReq(Oscl::Frame::Pdu::Rx::Resp::RxMem* mem) noexcept;
		/** */
		void	recycle(	Oscl::Frame::Pdu::
							Rx::Resp::Api::RxResp&	msg
							) noexcept;
		/** */
		void	release(	Oscl::Frame::Pdu::
							Rx::Resp::Api::RxResp&	msg
							) noexcept;
	};

}
}
}
}
}
}
}
#endif
