/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/ip/header.h"

using namespace Oscl::Protocol::IP::IF::RX::Srv::Client;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Frame::Pdu::Rx::Req::Api::SAP&	ipLlcRxSAP,
			Oscl::Protocol::IP::Addr::Local::Api&	localAddressMatcher,
			Oscl::Protocol::IP::RX::Api&			ipRoute,
			Oscl::Protocol::IP::RX::Api&			ipLocal,
			TransMem								transMem[],
			unsigned								nTransMem
			) noexcept:
		CloseSync(*this,papi),
		_myPapi(papi),
		_ipLlcRxSAP(ipLlcRxSAP),
		_localAddressMatcher(localAddressMatcher),
		_ipRoute(ipRoute),
		_ipLocal(ipLocal),
		_open(false),
		_closeReq(0)
		{
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

void	Part::request(OpenReq& msg) noexcept{
	TransMem*	mem;
	while((mem=_freeTransMem.get())){
		new (mem) Transaction(*this);
		}
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(CloseReq& msg) noexcept{
	if(!_open){
		msg.returnToSender();
		}
	_open		= false;
	_closeReq	= &msg;
	sendIpLlcRxClose();
	}

void	Part::response(RxResp& msg) noexcept{
	// This should NEVER happen
	// RxResp processing is delegated to the
	// Transaction class.
	for(;;);
	}

void	Part::response(CancelAllResp& msg) noexcept{
	if(!_closeReq) return;
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

void	Part::sendIpLlcRxClose() noexcept{
	Oscl::Frame::Pdu::Rx::Resp::Api::CancelAllResp*
	resp	= new(&_rxCancelMem.response)
				Oscl::Frame::Pdu::
				Rx::Resp::Api::CancelAllResp(	_ipLlcRxSAP.getReqApi(),
												*this,
												_myPapi
												);
	_ipLlcRxSAP.post(resp->getSrvMsg());
	}

void	Part::free(Transaction& trans) noexcept{
	trans.~Transaction();
	_freeTransMem.put((TransMem*)&trans);
	}
