/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "transaction.h"
#include "part.h"
#include "oscl/protocol/ip/header.h"

using namespace Oscl::Protocol::IP::IF::RX::Srv::Client;

Transaction::Transaction(Part& context) noexcept:
		_context(context)
		{
	sendRxReq(&_rxMem);
	}

void	Transaction::response(RxResp& msg) noexcept{
	if(!_context._open){
		release(msg);
		return;
		}
	Oscl::Protocol::IP::Header	header;
	Oscl::Pdu::Pdu&	pdu	= msg._payload._handle;
	unsigned	len;
	len	= pdu.read(	&header,
					Header::minHeaderBytes,
					0
					);
	if(len < Header::minHeaderBytes){
		recycle(msg);
		return;
		}
	unsigned	headerLength	= (header.getHeaderLength()<<2);
	if(headerLength < Header::minHeaderBytes){
		recycle(msg);
		return;
		}
	if(header.getVersion() != 4){
		recycle(msg);
		return;
		}
	unsigned	optionsLen	= headerLength - Header::minHeaderBytes;
	if(optionsLen){
		len	= pdu.read(	header.options,
						optionsLen,
						Header::minHeaderBytes
						);
		if(len < optionsLen){
			recycle(msg);
			return;
			}
		}
	if(!header.checksumOK(optionsLen)){
		recycle(msg);
		return;
		}
	if(_context._localAddressMatcher.isLocal(header.destination)){
		_context._ipLocal.transfer(pdu,header);
		}
	_context._ipRoute.transfer(pdu,header);
	recycle(msg);
	}

void	Transaction::response(CancelAllResp& msg) noexcept{
	// This should NEVER happen.
	for(;;);
	}

void	Transaction::sendRxReq(Oscl::Frame::Pdu::Rx::Resp::RxMem* mem) noexcept{
	Oscl::Frame::Pdu::Rx::Req::Api::RxPayload*

	payload	= new(&mem->payload)
				Oscl::Frame::Pdu::
				Rx::Req::Api::RxPayload();

	Oscl::Frame::Pdu::Rx::Resp::Api::RxResp*

	resp	= new(&mem->response)
				Oscl::Frame::Pdu::
				Rx::Resp::Api::RxResp(	_context._ipLlcRxSAP.getReqApi(),
										*this,
										_context._myPapi,
										*payload
										);
	_context._ipLlcRxSAP.post(resp->getSrvMsg());
	}

void	Transaction::recycle(	Oscl::Frame::Pdu::
								Rx::Resp::Api::RxResp&	msg
								) noexcept{
	msg._payload._handle	= 0;
	_context._ipLlcRxSAP.post(msg.getSrvMsg());
	}

void	Transaction::release(	Oscl::Frame::Pdu::
								Rx::Resp::Api::RxResp&	msg
								) noexcept{
	msg.~RxResp();
	_context.free(*this);
	}

