/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/ip/ipreg.h"

using namespace Oscl::Protocol::IP::IF::Dynamic::Monitor;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
			Oscl::Mt::Itc::Dyn::Adv::Cli::
			SyncApi<	Oscl::Protocol::IP::
						IF::Dynamic::Interface
						>&							advFind,
			Oscl::Mt::Itc::Dyn::Adv::Cli::
			Req::Api<	Oscl::Protocol::IP::
						IF::Dynamic::Interface
						>::SAP&						advSAP,
			Oscl::Protocol::IP::
			Subsystem::Srv::Api&					ipSubsystemApi,
			const Oscl::ObjectID::RO::Api&			location,
			Oscl::Protocol::IP::
			Mux::Srv::Client::TransMem				udpRxCliTransMem[],
			unsigned								nUdpRxCliTransMem,
			Oscl::Protocol::IP::
			Mux::Srv::Client::TransMem				tcpRxCliTransMem[],
			unsigned								nTcpRxCliTransMem
			) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::Monitor<Interface>(	advSAP,
														advFind,
														myPapi,
														_match
														),
		_myPapi(myPapi),
		_ipSubsystemApi(ipSubsystemApi),
		_match(location),
		_udpRxCliTransMem(udpRxCliTransMem),
		_nUdpRxCliTransMem(nUdpRxCliTransMem),
		_tcpRxCliTransMem(tcpRxCliTransMem),
		_nTcpRxCliTransMem(nTcpRxCliTransMem)
		{
	}

void	Part::activate() noexcept{
	attachInterfaceToIpRx();
	attachInterfaceToIpFwd();
	}

void	Part::detachInterfaceFromIP() noexcept{
	// This is where we begin to asynchronously detach
	// the interface from the IP subsystem.
	detachInterfaceFromIpFwd();
	}

void	Part::attachInterfaceToIpFwd() noexcept{
	_fwdIf	=
		new(&_fwdIfMem)
			Oscl::Protocol::IP::
			FWD::IF::
			FwdInterface(_handle->getDynSrv()._ipService.getIpTransmitter());

	_ipSubsystemApi.getIpFwd().attach(*_fwdIf);
	}

void	Part::attachInterfaceToIpRx() noexcept{
	// UDP
	_udpRxIf	=
		new(&_udpRxIfMem)
			Oscl::Protocol::IP::
			Subsystem::Srv::IF::
			Part(	_handle->getDynSrv()._ipService.getIpReceiver().getMuxSAP(),
					_ipSubsystemApi.getUdpRxSrv().getIpRxSAP(),
					Oscl::Protocol::IP::
					TtlProtoCheck::Protocol::Value_UDP,
					_ipSubsystemApi.getUdpRxSrv().getReceiver(),
					_udpRxCliTransMem,
					_nUdpRxCliTransMem
					);
	_udpRxIf->syncOpen();
	_handle->getDynSrv()._ipService.getIpReceiver().getMuxSyncApi().
		attach(_udpRxIf->getAcceptor());
	// TCP
	_tcpRxIf	=
		new(&_tcpRxIfMem)
			Oscl::Protocol::IP::
			Subsystem::Srv::IF::
			Part(	_handle->getDynSrv()._ipService.getIpReceiver().getMuxSAP(),
					_ipSubsystemApi.getTcpRxSrv().getIpRxSAP(),
					Oscl::Protocol::IP::
					TtlProtoCheck::Protocol::Value_TCP,
					_ipSubsystemApi.getTcpRxSrv().getReceiver(),
					_tcpRxCliTransMem,
					_nTcpRxCliTransMem
					);
	_tcpRxIf->syncOpen();
	_handle->getDynSrv()._ipService.getIpReceiver().getMuxSyncApi().
		attach(_tcpRxIf->getAcceptor());
	}

void	Part::detachInterfaceFromIpFwd() noexcept{
	Oscl::Protocol::IP::FWD::Srv::IF::Req::DetachPayload*
	payload	= new(&_respMem._ipFwdRespMem.detachMem.payload)
				Oscl::Protocol::IP::
				FWD::Srv::IF::Req::
				DetachPayload(*_fwdIf);

	Oscl::Protocol::IP::FWD::Srv::IF::Resp::Api::DetachResp*
	resp	= new(&_respMem._ipFwdRespMem.detachMem.response)
				Oscl::Protocol::IP::
				FWD::Srv::IF::Resp::Api::
				DetachResp(	_ipSubsystemApi.getIpFwdSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);

	_ipSubsystemApi.getIpFwdSAP().post(*resp);
	}

void	Part::detachInterfaceFromIpRx() noexcept{
	_udpRxIf->syncClose();
	_udpRxIf->~Part();
	_udpRxIf	= 0;
	_tcpRxIf->syncClose();
	_tcpRxIf->~Part();
	_tcpRxIf	= 0;
	deactivateDone();
	}

void	Part::deactivate() noexcept{
	detachInterfaceFromIP();
	}

void	Part::response(	Oscl::Protocol::IP::
						FWD::Srv::IF::Resp::Api::AttachResp&	msg
						) noexcept{
	// Not used
	}

void	Part::response(	Oscl::Protocol::IP::
						FWD::Srv::IF::Resp::Api::DetachResp&	msg
						) noexcept{
	_fwdIf	= 0;
	// Now detach from IP RX
	detachInterfaceFromIpRx();
	}

void	Part::response(	Oscl::Protocol::IP::
						RX::Srv::IF::Resp::Api::AttachResp&	msg
						) noexcept{
	// Not used
	}

void	Part::response(	Oscl::Protocol::IP::
						RX::Srv::IF::Resp::Api::DetachResp&	msg
						) noexcept{
	_udpRxIf	= 0;
	_tcpRxIf	= 0;
	deactivateDone();
	}


