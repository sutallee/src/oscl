/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_dyn_monitor_parth_
#define _oscl_protocol_ip_if_dyn_monitor_parth_
#include "match.h"
#include "oscl/protocol/ip/if/dyn/if.h"
#include "oscl/protocol/ip/fwd/srv/if/respmem.h"
#include "oscl/protocol/ip/rx/srv/if/respmem.h"
#include "oscl/protocol/ip/subsys/srv/api.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/protocol/ip/rx/if/rxifmem.h"
#include "oscl/protocol/ip/fwd/if/fwdifmem.h"
#include "oscl/protocol/ip/subsys/srv/if/part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Dynamic {
/** */
namespace Monitor {

/** This concrete class is a single IP Interface monitor.
	It waits for a new IP Interface to be created, claims the
	interface, and then registers the interface with the IP
	subsystem.
	When the IP interface is removed the interface is un-registered
	from the IP subsystem, the interface is released,
	and the monitor waits for a new interface to be created.
	This class is responsible for listening for exactly one
	specific IP interface.
 */
class Part :
		public	Oscl::Mt::Itc::Dyn::
				Unit::Monitor<	Oscl::Protocol::IP::
								IF::Dynamic::Interface
								>,
		private Oscl::Protocol::IP::FWD::Srv::IF::Resp::Api,
		private Oscl::Protocol::IP::RX::Srv::IF::Resp::Api
		{
	private:
		/** */
		Oscl::Protocol::IP::
		FWD::IF::FwdInterfaceMem				_fwdIfMem;

		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Protocol::IP::
								Subsystem::Srv::IF::Part
								)
						>						_udpRxIfMem;

		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Protocol::IP::
								Subsystem::Srv::IF::Part
								)
						>						_tcpRxIfMem;

		union {
			/** */
			Oscl::Protocol::IP::
			FWD::Srv::IF::Resp::RespMem	_ipFwdRespMem;
			/** */
			Oscl::Protocol::IP::
			RX::Srv::IF::Resp::RespMem	_ipRxRespMem;
			}									_respMem;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Oscl::Protocol::IP::Subsystem::Srv::Api&	_ipSubsystemApi;
		/** */
		Match										_match;
		/** */
		Oscl::Protocol::IP::
		FWD::IF::FwdInterface*						_fwdIf;
		/** */
		Oscl::Protocol::IP::
		Subsystem::Srv::IF::Part*					_udpRxIf;
		/** */
		Oscl::Protocol::IP::
		Subsystem::Srv::IF::Part*					_tcpRxIf;
		/** */
		Oscl::Protocol::IP::
		Mux::Srv::Client::TransMem*					_udpRxCliTransMem;
		/** */
		const unsigned								_nUdpRxCliTransMem;
		/** */
		Oscl::Protocol::IP::
		Mux::Srv::Client::TransMem*					_tcpRxCliTransMem;
		/** */
		const unsigned								_nTcpRxCliTransMem;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Protocol::IP::
							IF::Dynamic::Interface
							>&						advFind,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Protocol::IP::
							IF::Dynamic::Interface
							>::SAP&					advSAP,
				Oscl::Protocol::IP::
				Subsystem::Srv::Api&				ipSubsystemApi,
				const Oscl::ObjectID::RO::Api&		location,
				Oscl::Protocol::IP::
				Mux::Srv::Client::TransMem			udpRxCliTransMem[],
				unsigned							nUdpRxCliTransMem,
				Oscl::Protocol::IP::
				Mux::Srv::Client::TransMem			tcpRxCliTransMem[],
				unsigned							nTcpRxCliTransMem
				) noexcept;
	private:
		/** */
		void	attachInterfaceToIP() noexcept;
		/** */
		void	detachInterfaceFromIP() noexcept;
		/** */
		void	attachInterfaceToIpFwd() noexcept;
		/** */
		void	attachInterfaceToIpRx() noexcept;
		/** */
		void	detachInterfaceFromIpFwd() noexcept;
		/** */
		void	detachInterfaceFromIpRx() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;

	private:	// Oscl::Protocol::IP::FWD::Srv::IF::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							FWD::Srv::IF::Resp::Api::AttachResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Protocol::IP::
							FWD::Srv::IF::Resp::Api::DetachResp&	msg
							) noexcept;

	private:	// Oscl::Protocol::IP::RX::Srv::IF::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							RX::Srv::IF::Resp::Api::AttachResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Protocol::IP::
							RX::Srv::IF::Resp::Api::DetachResp&	msg
							) noexcept;
	};

}
}
}
}
}
}

#endif

