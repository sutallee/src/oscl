/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_dyn_monitor_configh_
#define _oscl_protocol_ip_if_dyn_monitor_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Dynamic {
/** */
namespace Monitor {

/** */
template<	unsigned	nUdpRxCliTransMem,
			unsigned	nTcpRxCliTransMem
			>
class Config {
	private:
		/** */
		Oscl::Protocol::IP::
		Mux::Srv::Client::TransMem				_udpRxCliTransMem[nUdpRxCliTransMem];
		/** */
		Oscl::Protocol::IP::
		Mux::Srv::Client::TransMem				_tcpRxCliTransMem[nTcpRxCliTransMem];

	public:
		/** */
		Part									_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<	Oscl::Protocol::IP::
							IF::Dynamic::Interface
							>&						advFind,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<	Oscl::Protocol::IP::
							IF::Dynamic::Interface
							>::SAP&					advSAP,
				Oscl::Protocol::IP::
				Subsystem::Srv::Api&				ipSubsystemApi,
				const Oscl::ObjectID::RO::Api&		location
				) noexcept:
				_part(	myPapi,
						advFind,
						advSAP,
						ipSubsystemApi,
						location,
						_udpRxCliTransMem,
						nUdpRxCliTransMem,
						_tcpRxCliTransMem,
						nTcpRxCliTransMem
						)
				{
			}
	};

}
}
}
}
}
}

#endif

