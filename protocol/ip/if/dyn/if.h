/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_dyn_ifh_
#define _oscl_protocol_ip_if_dyn_ifh_
#include "oscl/oid/roapi.h"
#include "oscl/protocol/ip/if/srv/api.h"
#include "oscl/mt/itc/dyn/adv/core.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Dynamic {

/** Instances of this class represent a single IP
	interface.
 */
class Interface : public Oscl::Mt::Itc::Dyn::Adv::Core<Interface> {
	public:
		/** */
		Oscl::Protocol::IP::IF::Srv::Api&		_ipService;

		/** */
		const Oscl::ObjectID::RO::Api&			_location;

	public:
		/** */
		Interface(	Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Mt::Itc::Dyn::
					Adv::Core<Interface>::ReleaseApi&		releaseApi,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Protocol::IP::IF::Srv::Api&		ipService,
					const Oscl::ObjectID::RO::Api&			location
					) noexcept;

		/** */
		const Oscl::ObjectID::RO::Api&	location() const noexcept;

	private:	// Oscl::Mt::Itc::Dyn::Adv::Core<Interface>
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "open" the sub-class. This typically entails binding
			to lower layer services. Function call semantics are
			assumed, and thus ITC must be done synchronously.
		 */
		void	openDynamicService() noexcept;
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "close" the sub-class in preparation for subsequent
			destruction. Function call semantics are assumed,
			and thus ITC must be done synchronously.
		 */
		void	closeDynamicService() noexcept;
		/** This operation is implemented by sub-classses.
			It returns a reference to the actual Dynamic Service.
		 */
		Interface&		getDynSrv() noexcept;
	};

}
}
}
}
}

#endif
