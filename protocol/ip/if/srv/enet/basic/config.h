/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_srv_enet_basic_configh_
#define _oscl_protocol_ip_if_srv_enet_basic_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Basic {

/** */
template <	unsigned nRxCliTransMem,
			unsigned nIfIcmpTransMem,
			unsigned nIfTxTransMem,
			unsigned nArpTransMem,
			unsigned nArpRxMem,
			unsigned nArpBindMem
			>
class Config {
	private:
		/** */
		Oscl::Protocol::IP::
		IF::RX::Srv::Client::TransMem		_rxCliTransMem[nRxCliTransMem];
		/** */
		Oscl::Protocol::IP::
		ICMP::Srv::Server::TransMem			_ifIcmpTransMem[nIfIcmpTransMem];
		/** */
		Oscl::Protocol::IP::
		IF::TX::Srv::ENET::Server::TransMem	_ifTxTransMem[nIfTxTransMem];
		/** */
		Oscl::Protocol::ARP::
		IEEE48Bit::IP::Service::ArpTransMem	_arpTransMem[nArpTransMem];
		/** */
		Oscl::Protocol::ARP::
		IEEE48Bit::IP::Service::RxMem		_arpRxMem[nArpRxMem];
		/** */
		Oscl::Protocol::ARP::
		IEEE48Bit::IP::Service::BindMem		_arpBindMem[nArpBindMem];

	public:
		/** */
		Part								_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&					txIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					rxIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					rxFramerPapi,
				Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	enetRxProtoMux,
				Oscl::Frame::Pdu::Tx::Req::Api::SAP&		enetTxSAP,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP,
				Oscl::Protocol::IP::	// NEW
				TX::Srv::Req::Api::SAP&						ipFwdSAP, // NEW
				const Oscl::Protocol::IP::Address&			myIpAddress,
				const Oscl::Protocol::IEEE::MacAddress&		myMacAddress,
				unsigned									mtuSize
				) noexcept:
				_part(	txIfPapi,
						rxIfPapi,
						rxFramerPapi,
						enetRxProtoMux,
						enetTxSAP,
						delaySAP,
						ipFwdSAP,	// NEW
						myIpAddress,
						myMacAddress,
						mtuSize,
						_rxCliTransMem,
						nRxCliTransMem,
						_ifIcmpTransMem,
						nIfIcmpTransMem,
						_ifTxTransMem,
						nIfTxTransMem,
						_arpTransMem,
						nArpTransMem,
						_arpRxMem,
						nArpRxMem,
						_arpBindMem,
						nArpBindMem
						)
					{
				}
	};

}
}
}
}
}
}
}

#endif
