/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/protocol/iana.h"

using namespace Oscl::Protocol::IP::IF::Srv::ENET::Basic;

static const Oscl::Protocol::IEEE::MacAddressInit	broadcastMacAddress(0xFF,
																		0xFF,
																		0xFF,
																		0xFF,
																		0xFF,
																		0xFF
																		);


Part::Part(	Oscl::Mt::Itc::PostMsgApi&					txIfPapi,
			Oscl::Mt::Itc::PostMsgApi&					rxIfPapi,
			Oscl::Mt::Itc::PostMsgApi&					rxFramerPapi,
			Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	enetRxProtoMux,
			Oscl::Frame::Pdu::Tx::Req::Api::SAP&		enetTxSAP,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&						ipFwdSAP,
			const Oscl::Protocol::IP::Address&			myIpAddress,
			const Oscl::Protocol::IEEE::MacAddress&		myMacAddress,
			unsigned									mtuSize,
			Oscl::Protocol::IP::
			IF::RX::Srv::Client::TransMem				rxCliTransMem[],
			unsigned									nRxCliTransMem,
			Oscl::Protocol::IP::
			ICMP::Srv::Server::TransMem					ifIcmpTransMem[],
			unsigned									nIfIcmpTransMem,
			Oscl::Protocol::IP::
			IF::TX::Srv::ENET::Server::TransMem			ifTxTransMem[],
			unsigned									nIfTxTransMem,
			Oscl::Protocol::ARP::
			IEEE48Bit::IP::Service::ArpTransMem			arpTransMem[],
			unsigned									nArpTransMem,
			Oscl::Protocol::ARP::
			IEEE48Bit::IP::Service::RxMem				arpRxMem[],
			unsigned									nArpRxMem,
			Oscl::Protocol::ARP::
			IEEE48Bit::IP::Service::BindMem				arpBindMem[],
			unsigned									nArpBindMem
			) noexcept:
		_myIpAddress(myIpAddress),
		_myMacAddress(myMacAddress),
		_enetRxProtoMux(enetRxProtoMux),
		_ipTx(	txIfPapi,
				enetTxSAP,
				_arpService,	// FIXME: order?
				myMacAddress,
				mtuSize,
				ifTxTransMem,
				nIfTxTransMem
				),
		_ipRx(	rxIfPapi,
				rxFramerPapi,
				enetRxProtoMux,
				*this,
				rxCliTransMem,
				nRxCliTransMem
				),
		_broadcastDriver(	enetTxSAP,
							enetTxSAP,
							broadcastMacAddress,
							myMacAddress,
							Oscl::Protocol::IANA::ARP,
							2	// FIXME: n transaction records
							),
		_arpRxDriver(	rxFramerPapi,
						Oscl::Protocol::IANA::ARP
						),
		_arpService(	txIfPapi,
						_broadcastDriver.getTxSAP(),
						enetTxSAP,
						_arpRxDriver.getRxSAP(),
						delaySAP,
						myIpAddress,
						_ipTx,
						arpRxMem,
						nArpRxMem,
						arpTransMem,
						nArpTransMem,
						arpBindMem,
						nArpBindMem
						),
		_icmpServer(	rxIfPapi,
						ipFwdSAP,
						ifIcmpTransMem,
						nIfIcmpTransMem
						)
		{
	}

void	Part::syncOpen() noexcept{
	_ipTx.syncOpen();
	_ipRx.syncOpen();
	_broadcastDriver.syncOpen();
	_arpRxDriver.syncOpen();
	_arpService.syncOpen();
	_icmpServer.syncOpen();
	_enetRxProtoMux.attach(_arpRxDriver);
	_ipRx.getMuxSyncApi().attach(_icmpServer);
	}

void	Part::syncClose() noexcept{
	_enetRxProtoMux.detach(_arpRxDriver);
	_ipRx.getMuxSyncApi().detach(_icmpServer);
	_icmpServer.syncClose();
	_arpService.syncClose();
	_arpRxDriver.syncClose();
	_broadcastDriver.syncClose();
	_ipTx.syncClose();
	_ipRx.syncClose();
	}

Oscl::Protocol::IP::IF::RX::Srv::Api&	Part::getIpReceiver() noexcept{
	return _ipRx;
	}

Oscl::Protocol::IP::IF::TX::Srv::Api&	Part::getIpTransmitter() noexcept{
	return _ipTx;
	}

bool	Part::isLocal(const Oscl::Protocol::IP::Address& local) noexcept{
	return _myIpAddress == local;
	}

