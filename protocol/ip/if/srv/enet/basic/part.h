/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_srv_enet_basic_parth_
#define _oscl_protocol_ip_if_srv_enet_basic_parth_
#include "oscl/protocol/ip/if/rx/srv/enet/basic/part.h"
#include "oscl/protocol/ip/if/srv/api.h"
#include "oscl/protocol/ip/if/tx/srv/enet/server/part.h"
#include "oscl/frame/pdu/tx/enet/driver.h"
#include "oscl/frame/pdu/rx/protomux/driver/driver.h"
#include "oscl/protocol/arp/ieee48/ip/service/service.h"
#include "oscl/protocol/ip/icmp/srv/server/part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Basic {

/** */
class Part :	public Oscl::Protocol::IP::IF::Srv::Api,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private Oscl::Protocol::IP::Addr::Local::Api
				{
	private:
		/** */
		const Oscl::Protocol::IP::Address				_myIpAddress;
		/** */
		const Oscl::Protocol::IEEE::MacAddress			_myMacAddress;
		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&		_enetRxProtoMux;
		/** */
		Oscl::Protocol::IP::
		IF::TX::Srv::ENET::Server::Part					_ipTx;
		/** */
		Oscl::Protocol::IP::
		IF::RX::Srv::ENET::Basic::Part					_ipRx;
		/** */
		Oscl::Frame::Pdu::Tx::ENET::Driver				_broadcastDriver;
		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::Driver			_arpRxDriver;
		/** */
		Oscl::Protocol::ARP::
		IEEE48Bit::IP::Service							_arpService;
		/** */
		Oscl::Protocol::IP::
		ICMP::Srv::Server::Part							_icmpServer;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					txIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					rxIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					rxFramerPapi,
				Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	enetRxProtoMux,
				Oscl::Frame::Pdu::Tx::Req::Api::SAP&		enetTxSAP,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&						ipFwdSAP,
				const Oscl::Protocol::IP::Address&			myIpAddress,
				const Oscl::Protocol::IEEE::MacAddress&		myMacAddress,
				unsigned									mtuSize,
				Oscl::Protocol::IP::
				IF::RX::Srv::Client::TransMem				rxCliTransMem[],
				unsigned									nRxCliTransMem,
				Oscl::Protocol::IP::
				ICMP::Srv::Server::TransMem					ifIcmpTransMem[],
				unsigned									nIfIcmpTransMem,
				Oscl::Protocol::IP::
				IF::TX::Srv::ENET::Server::TransMem			ifTxTransMem[],
				unsigned									nIfTxTransMem,
				Oscl::Protocol::ARP::
				IEEE48Bit::IP::Service::ArpTransMem			arpTransMem[],
				unsigned									nArpTransmem,
				Oscl::Protocol::ARP::
				IEEE48Bit::IP::Service::RxMem				arpRxMem[],
				unsigned									nArpRxMem,
				Oscl::Protocol::ARP::
				IEEE48Bit::IP::Service::BindMem				arpBindMem[],
				unsigned									nArpBindMem
				) noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	public:	// Oscl::Protocol::IP::IF::Srv::Api
		/** Returns IP receiver interface */
		Oscl::Protocol::IP::IF::RX::Srv::Api&	getIpReceiver() noexcept;

		/** Returns IP transmitter interface */
		Oscl::Protocol::IP::IF::TX::Srv::Api&	getIpTransmitter() noexcept;

	private:	// Oscl::Protocol::IP::Addr::Local::Api
		/** */
		bool	isLocal(const Oscl::Protocol::IP::Address& local) noexcept;
	};

}
}
}
}
}
}
}

#endif
