/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "transaction.h"
#include "part.h"
#include "oscl/protocol/iana.h"

using namespace Oscl::Protocol::IP::IF::TX::Srv::ENET::Server;

ReqTrans::ReqTrans(	Part&							context,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Frame::Pdu::Tx::
					Req::Api::SAP&					txSAP,
					const Oscl::Protocol::
					IEEE::MacAddress&				myMacAddress,
					const Oscl::Protocol::
					IEEE::MacAddress&				destMacAddress,
					Oscl::Protocol::IP::
					TX::Srv::Req::Api::SendReq&		req
					) noexcept:
		_fixedPDU(	*this,
					*this,
					&_macHeader,
					Oscl::Protocol::IEEE::Ethernet::Header::size,
					Oscl::Protocol::IEEE::Ethernet::Header::size
					),
		_headerFragment(	*this,
							&_fixedPDU
							),
		_payloadFragment(	*this,
							req._payload._pdu
							),
		_enetPDU(*this),
		_payload(_enetPDU),
		_resp(	txSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_context(context),
		_req(req)
		{
	Oscl::Protocol::IEEE::Ethernet::Header&
	enetHeader	= *(Oscl::Protocol::IEEE::Ethernet::Header*)&_macHeader;
	enetHeader.destination	= destMacAddress;
	enetHeader.source		= myMacAddress;
	enetHeader.type			= Oscl::Protocol::IANA::DodIp;
	_enetPDU.append(&_headerFragment);
	_enetPDU.append(&_payloadFragment);
	txSAP.post(_resp.getSrvMsg());
	}

void	ReqTrans::response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::TxResp&	msg
							) noexcept{
	_req.returnToSender();
	_context.done(*this);
	}

void	ReqTrans::free(void* fso) noexcept{
	// do nothing
	}

