/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_tx_srv_enet_server_parth_
#define _oscl_protocol_ip_if_tx_srv_enet_server_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/protocol/ip/addr/enet/resolv/api.h"
#include "oscl/protocol/ip/if/tx/srv/api.h"
#include "transaction.h"
#include "oscl/protocol/arp/ieee48/ip/service/notify.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace TX {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Server {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(ReqTrans) >	_reqTrans;
	};

/** */
class ReqTrans;

/** */
class Part :	public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Protocol::IP::TX::Srv::Req::Api,
				public Oscl::Protocol::IP::IF::TX::Srv::Api,
				public	Oscl::Protocol::ARP::IEEE48Bit::IP::NotifyApi
				{
	private:
		/** */
		friend class ReqTrans;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;

		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::ConcreteSAP	_sap;

		/** Refers to the lower layer ethernet transmit server. */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&			_enetTxSAP;

		/** */
		Oscl::Protocol::IP::Addr::ENET::Resolv::Api&	_macAddressResolver;

		/** */
		const Oscl::Protocol::IEEE::MacAddress			_myMacAddress;

		/** */
		const unsigned									_mtuSize;

		/** */
		Oscl::Queue<TransMem>							_freeTransMem;

		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						TX::Srv::Req::Api::SendReq
						>								_txReqPendingArp;

		/** */
		bool											_open;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

		/** */
		struct {
			/** */
			unsigned long	nSendReqs;
			/** */
			unsigned long	nDiscards;
			/** */
			unsigned long	nResolutionAttempts;
			/** */
			unsigned long	nUnresolved;
			} _stats;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					papi,
				Oscl::Frame::Pdu::Tx::Req::Api::SAP&		enetTxSAP,
				Oscl::Protocol::
				IP::Addr::ENET::Resolv::Api&				macAddressResolver,
				const Oscl::Protocol::IEEE::MacAddress&		myMacAddress,
				unsigned									mtuSize,
				TransMem									transMem[],
				unsigned									nTransMem
				) noexcept;

		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	getIpTxSAP() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** This operation is never invoked in this class.  */
		void	response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::TxResp&	msg
							) noexcept;

#if 0
		/** This operation is invoked when a cancel
			operation is pending after all TX PDUs for
			the lower layer are canceled. Such a cancellation
			is typical when this service receives a close
			request from an upper layer.
		 */
		void	response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::CancelAllResp&	msg
							) noexcept;
#endif

	private: // Oscl::Protocol::IP::TX::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							TX::Srv::Req::Api::SendReq&	msg
							) noexcept;

	public:	// Oscl::Protocol::IP::IF::TX::Srv::Api
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	getDirectSAP() noexcept;

	public: //	Oscl::Protocol::ARP::IEEE48Bit::IP::NotifyApi
		/** This operation is invoked when a pending entry matching
			the "ipAddress" is removed from the table.
		 */
		void	removed(	const Oscl::Protocol::IP::
							Address						ipAddress
							) noexcept;

		/** This operation is invoked when a pending entry matching
			the "ipAddress" is validated in the ARP table.
		 */
		void	discovered(	const Oscl::Protocol::IP::
							Address						ipAddress
							) noexcept;
		
	private:
		/** */
		void	done(ReqTrans& trans) noexcept;
	};

}
}
}
}
}
}
}
}
#endif
