/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/ip/header.h"

using namespace Oscl::Protocol::IP::IF::TX::Srv::ENET::Server;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&						papi,
			Oscl::Frame::Pdu::Tx::Req::Api::SAP&			enetTxSAP,
			Oscl::Protocol::IP::Addr::ENET::Resolv::Api&	macAddressResolver,
			const Oscl::Protocol::IEEE::MacAddress&			myMacAddress,
			unsigned										mtuSize,
			TransMem										transMem[],
			unsigned										nTransMem
			) noexcept:
		CloseSync(*this,papi),
		_myPapi(papi),
		_sap(*this,papi),
		_enetTxSAP(enetTxSAP),
		_macAddressResolver(macAddressResolver),
		_myMacAddress(myMacAddress),
		_mtuSize(mtuSize),
		_open(false),
		_closeReq(0)
		{
	_stats.nSendReqs			= 0;
	_stats.nDiscards			= 0;
	_stats.nResolutionAttempts	= 0;
	_stats.nUnresolved			= 0;
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	Part::getIpTxSAP() noexcept{
	return _sap;
	}

void	Part::request(OpenReq& msg) noexcept{
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(CloseReq& msg) noexcept{
	if(!_open){
		msg.returnToSender();
		}
	_open		= false;
	_closeReq	= &msg;
	for(;;);	// FIXME: fix this after adding cancel to LLC TX
	}

void	Part::response(	Oscl::Frame::Pdu::
						Tx::Resp::Api::TxResp&	msg
						) noexcept{
	// This should NEVER happen
	// TxResp processing is delegated to the
	// Transaction class.
	for(;;);
	}

void	Part::request(	Oscl::Protocol::IP::
						TX::Srv::Req::Api::SendReq&	msg
						) noexcept{
	const Oscl::Pdu::ReadOnlyPdu&
	pdu	= msg._payload._pdu;
	const Oscl::Protocol::IP::Address&
	destIpAddress	= msg._payload._destination;
	Oscl::Protocol::IEEE::MacAddress	destMacAddress;

	if(!_macAddressResolver.resolve(destIpAddress,destMacAddress)){
		// Unknown destination, give ARP a chance.
		++_stats.nResolutionAttempts;
		_txReqPendingArp.put(&msg);
		return;
		}

	++_stats.nSendReqs;

	if(pdu.length() > _mtuSize-Oscl::Protocol::IEEE::Ethernet::Header::size){
		// FIXME: Must fragment the PDU
		for(;;);
		}

	TransMem*	mem	= _freeTransMem.get();
	if(!mem){
		// Discard
		++_stats.nDiscards;
		msg.returnToSender();
		return;
		}
	new(mem) ReqTrans(	*this,
						_myPapi,
						_enetTxSAP,
						_myMacAddress,
						destMacAddress,
						msg
						);
	}

Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	Part::getDirectSAP() noexcept{
	return _sap;
	}

void	Part::removed(	const Oscl::Protocol::IP::
						Address						ipAddress
						) noexcept{
	// FIXME: this is where I remove and returnToSender() pending TX requests
	// that match the ipAddress.
	Oscl::Queue<	Oscl::Protocol::IP::
					TX::Srv::Req::Api::SendReq
					>	pending;

	pending	= _txReqPendingArp;

	Oscl::Protocol::IP::TX::Srv::Req::Api::SendReq* req;

	while((req	= pending.get())){
		if(req->_payload._destination == ipAddress){
			++_stats.nSendReqs;
			++_stats.nUnresolved;
			req->returnToSender();
			}
		else {
			_txReqPendingArp.put(req);
			}
		}
	}

void	Part::discovered(	const Oscl::Protocol::IP::
							Address						ipAddress
							) noexcept{
	// FIXME: this is where I restart pending TX requests
	// that match the ipAddress.
	Oscl::Queue<	Oscl::Protocol::IP::
					TX::Srv::Req::Api::SendReq
					>	pending;

	pending	= _txReqPendingArp;

	Oscl::Protocol::IP::TX::Srv::Req::Api::SendReq* req;

	while((req	= pending.get())){
		if(req->_payload._destination == ipAddress){
			req->process();
			}
		else {
			_txReqPendingArp.put(req);
			}
		}
	}

void	Part::done(ReqTrans& trans) noexcept{
	trans.~ReqTrans();
	_freeTransMem.put((TransMem*)&trans);
	}
