/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_tx_srv_enet_server_transactionh_
#define _oscl_protocol_ip_if_tx_srv_enet_server_transactionh_
#include "oscl/memory/block.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rocomposite.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/protocol/ieee/ethernet/header.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace TX {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Server {

/** */
class Part;

/** */
class ReqTrans :	private Oscl::Frame::Pdu::Tx::Resp::Api,
					private Oscl::FreeStore::Mgr
					{
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Protocol::IEEE::
								Ethernet::Header
								)
						>								_macHeader;
		/** */
		Oscl::Pdu::Fixed								_fixedPDU;
		/** */
		Oscl::Pdu::ReadOnlyFragment						_headerFragment;
		/** */
		Oscl::Pdu::ReadOnlyFragment						_payloadFragment;
		/** */
		Oscl::Pdu::ReadOnlyComposite					_enetPDU;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::TxPayload		_payload;
		/** */
		Oscl::Frame::Pdu::Tx::Resp::Api::TxResp			_resp;
		/** */
		class Part&										_context;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SendReq&	_req;

	public:
		/** */
		ReqTrans(	Part&										context,
					Oscl::Mt::Itc::PostMsgApi&					myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&		txSAP,
					const Oscl::Protocol::IEEE::MacAddress&		myMacAddress,
					const Oscl::Protocol::IEEE::MacAddress&		destMacAddress,
					Oscl::Protocol::IP::
					TX::Srv::Req::Api::SendReq&					req
					) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Tx::Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
