/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_tx_srv_enet_basic_parth_
#define _oscl_protocol_ip_if_tx_srv_enet_basic_parth_
#include "oscl/protocol/ip/if/tx/srv/api.h"
#include "oscl/frame/pdu/tx/protomux/driver/driver.h"
#include "oscl/protocol/ip/if/tx/srv/client/config.h"
#include "oscl/frame/pdu/tx/protomux/syncapi.h"
#include "oscl/protocol/ip/mux/service/driver.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace TX {
/** */
namespace Srv {
/** */
namespace ENET {
/** */
namespace Basic {

class Part :	public Oscl::Protocol::IP::IF::TX::Srv::Api,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi
				{
	private:
		/** Parsite that allows for different IP protocols to
			attach to capture their packets (e.g. UDP/TCP).
			This driver is attached procedurally to the
			IP protocol discriminator.
		 */
		Oscl::Protocol::IP::Mux::Driver				_ipTxMuxServer;

		/** This parasitic protocol receiver is the LLC server
			for IP PDs.
		 */
		Oscl::Frame::Pdu::Tx::ProtoMux::Driver		_ipProtocolTx;

		/** */
		Oscl::Protocol::IP::IF::TX::Srv::Client::Config<32>	_txClient;

		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&		_txFramerSAP;


	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					txIfPapi,
				Oscl::Mt::Itc::PostMsgApi&					txFramerPapi,
				Oscl::Frame::Pdu::Tx::Req::Api::SAP&		txFramerSAP
				) noexcept;
		/** */

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	private:	// Oscl::Protocol::IP::IF::TX::Srv::Api
		/** */
		Oscl::Protocol::IP::IF::TX::Req::Api::SAP&	getDirectSAP() noexcept;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&		getBroadcastSAP() noexcept;
	};

}
}
}
}
}
}
}
}

#endif
