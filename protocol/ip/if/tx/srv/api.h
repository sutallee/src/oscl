/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_tx_srv_apih_
#define _oscl_protocol_ip_if_tx_srv_apih_
#include "oscl/protocol/ip/tx/srv/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace TX {
/** */
namespace Srv {

/**	This interface provides the means for the IP layer to transmit
	IP PDUs on the interface.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual Oscl::Protocol::
				IP::TX::Srv::Req::Api::SAP&	getDirectSAP() noexcept=0;
	};

}
}
}
}
}
}

#endif
