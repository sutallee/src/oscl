/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_creator_srv_client_basic_parth_
#define _oscl_protocol_ip_if_creator_srv_client_basic_parth_
#include "oscl/protocol/ip/if/creator/srv/client/rec.h"
#include "oscl/protocol/ip/if/creator/srv/client/api.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Creator {
/** */
namespace Client {
/** */
namespace Basic {

/** */
class Part : public Oscl::Protocol::IP::IF::Creator::Client::Api {
	private:
		/** */
		Oscl::Mt::Itc::Dyn::
		Adv::Creator::SyncApi<	Oscl::Protocol::
								IP::IF::Dynamic::Interface
								>&	_advertiser;
		/** */
		Oscl::Mt::Itc::Dyn::
		Adv::Core<	Oscl::Protocol::
					IP::IF::Dynamic::Interface
					>::ReleaseApi&	_releaseApi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_advPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_dynSrvPapi;

		/** */
		Oscl::Queue<InterfaceMem>		_freeIfRecs;

		/** */
		Oscl::Queue<InterfaceRec>		_activeInterfaces;

		/** */
		const Oscl::ObjectID::RO::Api&	_location;

	public:
		/** */
		Part(	Oscl::Mt::Itc::Dyn::
				Adv::Creator::SyncApi<	Oscl::Protocol::
										IP::IF::Dynamic::Interface
										>&						advertiser,
				Oscl::Mt::Itc::Dyn::
				Adv::Core<	Oscl::Protocol::
							IP::IF::Dynamic::Interface
							>::ReleaseApi&						releaseApi,
				Oscl::Mt::Itc::PostMsgApi&						advPapi,
				Oscl::Mt::Itc::PostMsgApi&						dynSrvPapi,
				InterfaceMem									ifmem[],
				const Oscl::ObjectID::RO::Api&					location,
				unsigned										nIfMem
				) noexcept;

	public:	// Oscl::Protocol::IP::IF::Creator::Client::Api
		/** Attempts to register a new IP interface with the
			system. Returns true if successful or false if
			resources are unavailable to bring the interface up.
			Once this operation succeeds, the "interface" resources
			must remain intact until the corresponding down()
			operation completes.
		 */
		bool	up(Oscl::Protocol::IP::IF::Srv::Api& interface) noexcept;

		/** This operation un-registers the specified interface "interface"
			from the IP system. When this operation completes, the
			resources associated with the "interface" may be released.
		 */
		void	down(Oscl::Protocol::IP::IF::Srv::Api& interface) noexcept;
	};

}
}
}
}
}
}
}

#endif
