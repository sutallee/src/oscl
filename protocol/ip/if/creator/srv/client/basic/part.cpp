/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::Protocol::IP::IF::Creator::Client::Basic;
Part::Part(	Oscl::Mt::Itc::Dyn::
				Adv::Creator::SyncApi<	Oscl::Protocol::
										IP::IF::Dynamic::Interface
										>&						advertiser,
				Oscl::Mt::Itc::Dyn::
				Adv::Core<	Oscl::Protocol::
							IP::IF::Dynamic::Interface
							>::ReleaseApi&						releaseApi,
				Oscl::Mt::Itc::PostMsgApi&						advPapi,
				Oscl::Mt::Itc::PostMsgApi&						dynSrvPapi,
				Oscl::Protocol::IP::
				IF::Creator::Client::InterfaceMem				ifMem[],
				const Oscl::ObjectID::RO::Api&					location,
				unsigned										nIfMem
				) noexcept:
		_advertiser(advertiser),
		_releaseApi(releaseApi),
		_advPapi(advPapi),
		_dynSrvPapi(dynSrvPapi),
		_location(location)
		{
	for(unsigned i=0;i<nIfMem;++i){
		_freeIfRecs.put(&ifMem[i]);
		}
	}

bool	Part::up(Oscl::Protocol::IP::IF::Srv::Api& interface) noexcept{
	InterfaceMem*	mem	= _freeIfRecs.get();
	if(!mem) return false;
	InterfaceRec*	ifRec	= new(mem) InterfaceRec(	_releaseApi,
														_advPapi,
														_dynSrvPapi,
														interface,
														_location
														);
	_activeInterfaces.put(ifRec);
	_advertiser.add(ifRec->_interface);
	return true;
	}

void	Part::down(Oscl::Protocol::IP::IF::Srv::Api& interface) noexcept{
	InterfaceRec*	ifRec;
	for(	ifRec	= _activeInterfaces.first();
			ifRec;
			ifRec	= _activeInterfaces.next(ifRec)
			){
		if(&interface == &ifRec->_interface._ipService){
			_activeInterfaces.remove(ifRec);
			break;
			}
		}

	if(ifRec){
		ifRec->_interface.deactivate();
		ifRec->~InterfaceRec();
		_freeIfRecs.put((InterfaceMem*)ifRec);
		}
	}

