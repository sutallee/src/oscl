/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_creator_srv_client_rech_
#define _oscl_protocol_ip_if_creator_srv_client_rech_
#include "oscl/memory/block.h"
#include "oscl/protocol/ip/if/srv/api.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"
#include "oscl/protocol/ip/if/dyn/if.h"
#include "oscl/queue/queueitem.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Creator {
/** */
namespace Client {

/** This class is to be used by client implementations
	to keep track of interface resources.
 */
class InterfaceRec : public Oscl::QueueItem {
	public:
		/** */
		Oscl::Protocol::IP::IF::Dynamic::Interface	_interface;
	public:
		/** */
		InterfaceRec(	Oscl::Mt::Itc::Dyn::
						Adv::Core<	Oscl::Protocol::IP::
						IF::Dynamic::Interface>::ReleaseApi&	releaseApi,
						Oscl::Mt::Itc::PostMsgApi&				advPapi,
						Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
						Oscl::Protocol::IP::IF::Srv::Api&		interface,
						const Oscl::ObjectID::RO::Api&			location
						) noexcept;
	};

/** This interface may be used by clients to maintain a pool
	of InterfaceRec memory to be used in the allocation of
	IP interfaces.
 */
union InterfaceMem {
	/** */
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock< sizeof(InterfaceRec) >	ifMem;
	};

}
}
}
}
}
}

#endif
