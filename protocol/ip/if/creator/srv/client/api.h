/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_if_creator_srv_client_apih_
#define _oscl_protocol_ip_if_creator_srv_client_apih_
#include "oscl/protocol/ip/if/srv/api.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace IF {
/** */
namespace Creator {
/** */
namespace Client {

/** This interface represents the procedural interface to
	the client portion of an IP Interface creator. Implementations
	are responsible for notifying the IP subsystem that a new IP
	interface has been entered into the system.
 */
class Api {
	public:
		/** Attempts to register a new IP interface with the
			system. Returns true if successful or false if
			resources are unavailable to bring the interface up.
			Once this operation succeeds, the "interface" resources
			must remain intact until the corresponding down()
			operation completes.
		 */
		virtual bool	up(	Oscl::Protocol::
							IP::IF::Srv::Api&	interface
							) noexcept=0;

		/** This operation un-registers the specified interface "interface"
			from the IP system. When this operation completes, the
			resources associated with the "interface" may be released.
		 */
		virtual void	down(	Oscl::Protocol::
								IP::IF::Srv::Api&	interface
								) noexcept=0;
	};

}
}
}
}
}
}

#endif
