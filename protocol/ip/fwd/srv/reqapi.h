/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_fwd_srv_reqapih_
#define _oscl_protocol_ip_fwd_srv_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/protocol/ip/address.h"
#include "oscl/pdu/ropdu.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace FWD {
/** */
namespace Srv {
/** */
namespace Req {

/** This payload is used to submit an IP PDU to the IP
	forwarding layer. The PDU will be "routed" to the
	appropriate interface based on the user destination
	IP address. The PDU itself may be copied and fragmented
	by the forwarding or IP interface layer, but it will
	not be modified. When the PDU is either successfully
	transmitted toward the destination or discarded, the
	message containing this payload will be returned to
	the client, at which point the client is responsible
	for releasing the resources associated with the PDU.
 */
class FwdPayload {
	public:
		/** Complete IP PDU to be forwarded. The entire
			IP is complteted including header checksums
			ant Time-To-Live (TTL) fields.
		 */
		const Oscl::Pdu::ReadOnlyPdu&		_ipPDU;

		/** Ultimate destination of the PDU.
		 */
		const Oscl::Protocol::IP::Address	_destination;

	public:
		/** */
		FwdPayload(	const Oscl::Pdu::ReadOnlyPdu&		ipPDU,
					const Oscl::Protocol::IP::Address&	dest
					) noexcept;
	};

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,FwdPayload>	FwdReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>						SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>				ConcreteSAP;
	public:
		/** */
		virtual void	request(FwdReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
