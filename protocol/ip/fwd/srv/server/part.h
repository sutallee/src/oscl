/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_fwd_srv_server_parth_
#define _oscl_protocol_ip_fwd_srv_server_parth_
#include "oscl/protocol/ip/fwd/srv/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace FWD {
/** */
namespace Srv {
/** */
namespace Server {

/** This is the global IP forwarding task.
	1. Listen to IP Interface advertiser.
	2. Receive IP forwarding requests.
	3. Consult IP routing table to determine the
		appropriate interface through which to
		send the PDU.

	Idea:
		Is it posible for the IP forwarding task
		to share ReqApi's with the IP Interfaces
		such that the Request Message received
		by the forwarder can be posted directly
		to the appropriate IP interface after
		possibly replacing the destination IP
		address in the message payload with the
		address of a gateway if the destination
		not on a directly attached network.

		Canceling transmit requests may be
		difficult or impossible for IP forwarding.
		Certainly, a cancel-all technique may
		be unworkable since the transmitter
		has potentially many clients and
		tracking PDUs by client would be
		undesirable at best.

		If canceling transmit requests is not
		allowed, then the IP system must
		ensure that a stopped interface does
		not keep outgoing transmit requests
		from completing indefinitly.

		To do this, I will move the files:
			oscl/protocol/ip/if/tx/reqapi.h
			oscl/protocol/ip/if/tx/reqapi.cpp
			oscl/protocol/ip/if/tx/respapi.h
			oscl/protocol/ip/if/tx/sources.b

		to
			oscl/protocol/ip/tx/srv
		DONE

		Modify the namespaces and remove the
		const attribute from the _destination
		IP address member.
		DONE

		The following files will need to be modified.
		*	oscl/protocol/ip/if/tx/srv/enet/server/part.h
		*	oscl/protocol/ip/if/tx/srv/enet/server/part.cpp
			oscl/protocol/ip/if/tx/srv/enet/server/config.h
		*	oscl/protocol/ip/if/tx/srv/enet/server/transaction.h
		*	oscl/protocol/ip/if/tx/srv/enet/server/transaction.cpp
		*	oscl/protocol/ip/if/tx/srv/api.h
			oscl/protocol/arp/ieee48/ip/service/service.h

			oscl/protocol/ip/icmp/srv/server/config.h
			oscl/protocol/ip/icmp/srv/server/part.h
			oscl/protocol/ip/icmp/srv/server/part.cpp

	Before the routing table mechanisms are
	implemented, all forward requests can
	be forwarded to the first (only) attached
	interface assmuming that there is no
	routing to be done.
 */
class Part :	private Oscl::Protocol::IP::FWD::Req::Api
				{
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	papi,
				) noexcept;

	private: // Oscl::Protocol::IP::FWD::Req::Api
		/** */
		void	request(FwdReq& msg) noexcept;
	};

}
}
}
}
}
}

#endif
