/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_port_pathh_
#define _oscl_protocol_ip_port_pathh_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Port {

/** This record contains the components that
	constitute a unique end-to-end path in
	an IP transaction. For each IP protocol
	(e.g. UDP and TCP) each endpoint in an
	IP path is identified by a host IP address
	and a port number. Thus, the entire path,
	can be described by the two peer endpoints.
 */
struct Path {
	public:
		/** The IPv4 address of the remote peer
			host in the path.
		 */
		unsigned long	peerIpAddress;
		/** The IPv4 address of the local peer
			host in the path. Note that the
			local host may have more than one
			IP address (e.g. one for each
			physical media connection) and
			so this information is provided
			to allow upper layers to know
			which address is being used by the
			path.
		 */
		unsigned long	localIpAddress;
		/** The IP port number of the remote
			peer. This could be a TCP or UDP
			port number depending on the protocol.
		 */
		unsigned		peerPortNum;
		/** The IP port number of the local
			peer. This could be a TCP or UDP
			port number depending on the protocol.
		 */
		unsigned		localPortNum;
	public:
		/** */
		inline Path() noexcept{}
				
		/** */
		inline Path(	unsigned long	peerIpAddr,
						unsigned long	localIpAddr,
						unsigned		peerPort,
						unsigned		localPort
						) noexcept:
				peerIpAddress(peerIpAddr),
				localIpAddress(localIpAddr),
				peerPortNum(peerPort),
				localPortNum(localPort)
				{
			}
	};

}
}
}
}
#endif
