/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_port_mux_srv_server_parth_
#define _oscl_protocol_ip_port_mux_srv_server_parth_
#include "oscl/protocol/ip/port/mux/server/srv/reqapi.h"
#include "oscl/protocol/ip/port/mux/registrar/acceptor.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Port {
/** */
namespace Mux {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class Part :	private	Oscl::Protocol::IP::Port::Mux::Server::Srv::Req::Api,
				public	Oscl::Protocol::IP::Port::Mux::Registrar::Acceptor
				{
	private:
		/** */
		Oscl::Protocol::IP::
		Port::Mux::Server::Srv::Req::Api::ConcreteSAP	_sap;

		/** */
		const unsigned							_localPort;

		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						Port::Mux::Server::Srv::Req::Api::RecvReq
						>						_pendingRecvReqs;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				unsigned					localPort
				) noexcept;

		/** */
		Oscl::Protocol::IP::
		Port::Mux::Server::Srv::Req::Api::SAP&	getSAP() noexcept;

	private: // Oscl::Protocol::IP::Port::Mux::Registrar::Acceptor
		/** */
		bool	accept(	Oscl::Pdu::Pdu&	pdu,
						unsigned long	sourceAddress,
						unsigned long	destinationAddress,
						unsigned		srcPort,
						unsigned		destPort,
						unsigned		ipPayloadLength
						) noexcept;

	private: // Oscl::Protocol::IP::Port::Mux::Server::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							Port::Mux::Server::Srv::Req::Api::RecvReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							Port::Mux::Server::Srv::Req::Api::CancelReq&	msg
							) noexcept;
	};

}
}
}
}
}
}
}

#endif
