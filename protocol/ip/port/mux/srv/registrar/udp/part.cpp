/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/checksum/onescomp/b16/pdu/visitor.h"
#include "part.h"

using namespace Oscl::Protocol::IP::Port::Mux::Srv::Registrar::UDP;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi) noexcept:
		_sap(*this,myPapi),
		_sync(_sap)
		{
	}

Oscl::Protocol::IP::Port::
Mux::Registrar::Srv::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

Oscl::Protocol::IP::Port::Mux::Registrar::Api&		Part::getSyncApi() noexcept{
	return _sync;
	}

void	Part::receive(	Oscl::Pdu::Pdu&				pdu,
						const Oscl::Protocol::IP::
						Address&					source,
						const Oscl::Protocol::IP::
						Address&					destination,
						unsigned					ipPayloadLength
						) noexcept{
	struct {
		Oscl::Endian::Big::U16	sourcePort;
		Oscl::Endian::Big::U16	destinationPort;
		Oscl::Endian::Big::U16	length;
		Oscl::Endian::Big::U16	checksum;
		} portHeader;

	const unsigned udpHeaderSize	=		sizeof(portHeader.sourcePort)
										+	sizeof(portHeader.destinationPort)
										+	sizeof(portHeader.length)
										+	sizeof(portHeader.checksum)
										;

	unsigned	len	= pdu.read((uint8_t*)&portHeader,udpHeaderSize,0);

	if(len < udpHeaderSize){
		// PDU not big enough for UDP header
		return;
		}

	unsigned		pduLen	= pdu.length();
	const unsigned	udpLength	= portHeader.length;
	if(pduLen < udpLength){
		// PDU shorter than length field suggests.
		return;
		}

	if(portHeader.checksum){
		// Checksum needs to be performed.
		Oscl::Checksum::OnesComp::B16::PDU::Visitor	visitor;
		Oscl::Endian::Big::U16	protocol;
		protocol	= TtlProtoCheck::Protocol::Value_UDP;
		visitor._csum.accumulate(&source,sizeof(source));
		visitor._csum.accumulate(&destination,sizeof(destination));
		visitor._csum.accumulate(&protocol,sizeof(protocol));
		visitor._csum.accumulate(&portHeader.length,sizeof(portHeader.length));
		pdu.visit(visitor,0,0,udpLength);
		if(udpLength & 0x0001){
			unsigned char	buffer[1];
			buffer[0]	= 0;
			visitor._csum.accumulate(buffer,1);
			}
		if(visitor._csum.final() != 0){
			// Bad UDP checksum
			return;
			}
		}

	pdu.stripHeader(udpHeaderSize);

	const unsigned long	ipSourceAddr		= source;
	const unsigned long	ipDestinatinAddr	= destination;
	const unsigned		udpSourcePort		= portHeader.sourcePort;
	const unsigned		udpDestinationPort	= portHeader.destinationPort;

	Oscl::Protocol::IP::Port::Mux::Registrar::Acceptor*	next;
	for(	next	= _acceptorList.first();
			next;
			next	= _acceptorList.next(next)
			){
		if(next->accept(	pdu,
							ipSourceAddr,
							ipDestinatinAddr,
							udpSourcePort,
							udpDestinationPort,
							ipPayloadLength
							)){
			return;
			}
		}
	}

void	Part::request(	Oscl::Protocol::IP::
						Port::Mux::Registrar::Srv::Req::Api::AttachReq&	msg
						) noexcept{
	_acceptorList.put(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						Port::Mux::Registrar::Srv::Req::Api::DetachReq&	msg
						) noexcept{
	_acceptorList.remove(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

