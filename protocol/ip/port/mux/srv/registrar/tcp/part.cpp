/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/checksum/onescomp/b16/pdu/visitor.h"
#include "part.h"

using namespace Oscl::Protocol::IP::Port::Mux::Srv::Registrar::TCP;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
			TransMem									transMem[],
			unsigned									nTransMem
			) noexcept:
		_sap(*this,myPapi),
		_sync(_sap),
		_myPapi(myPapi),
		_ipFwdSAP(ipFwdSAP)
		{
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

Oscl::Protocol::IP::Port::
Mux::Registrar::Srv::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

Oscl::Protocol::IP::Port::Mux::Registrar::Api&		Part::getSyncApi() noexcept{
	return _sync;
	}

void	Part::done(RstTrans& trans) noexcept{
	trans.~RstTrans();
	_freeTransMem.put((TransMem*)&trans);
	}

void	Part::receive(	Oscl::Pdu::Pdu&				pdu,
						const Oscl::Protocol::IP::
						Address&					source,
						const Oscl::Protocol::IP::
						Address&					destination,
						unsigned					ipPayloadLength
						) noexcept{
//	Oscl::Protocol::IP::TCP::Header	tcpHeader;
	struct {
		Oscl::Endian::Big::U16	sourcePort;
		Oscl::Endian::Big::U16	destinationPort;
		} tcpMuxHeader;

	const unsigned muxHeaderSize	=	sizeof(tcpMuxHeader);

	unsigned	len	= pdu.read((uint8_t*)&tcpMuxHeader,muxHeaderSize,0);

	if(len < muxHeaderSize){
		// PDU not big enough for TCP header
		return;
		}

	unsigned		pduLen	= pdu.length();
	const unsigned	tcpLength	= ipPayloadLength;
	if(pduLen < tcpLength){
		// PDU shorter than length field suggests.
		return;
		}

	Oscl::Checksum::OnesComp::B16::PDU::Visitor	visitor;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_TCP;
	visitor._csum.accumulate(&source,sizeof(source));
	visitor._csum.accumulate(&destination,sizeof(destination));
	visitor._csum.accumulate(&protocol,sizeof(protocol));
	Oscl::Endian::Big::U16	ipTcpLength;
	ipTcpLength	= tcpLength;
	visitor._csum.accumulate(&ipTcpLength,sizeof(ipTcpLength));
	pdu.visit(visitor,0,0,tcpLength);
	if(visitor._csum.final() != 0){
		// Bad TCP checksum
		return;
		}

	const unsigned long	ipSourceAddr		= source;
	const unsigned long	ipDestinatinAddr	= destination;
	const unsigned		tcpSourcePort		= tcpMuxHeader.sourcePort;
	const unsigned		tcpDestinationPort	= tcpMuxHeader.destinationPort;

	Oscl::Protocol::IP::Port::Mux::Registrar::Acceptor*	next;
	for(	next	= _acceptorList.first();
			next;
			next	= _acceptorList.next(next)
			){
		// Note that (unlike UDP) the PDU is NOT stripped of the TCP
		// header at this point. The PDU has, however, been validated.
		if(next->accept(	pdu,
							ipSourceAddr,
							ipDestinatinAddr,
							tcpSourcePort,
							tcpDestinationPort,
							ipPayloadLength
							)){
			return;
			}
		}
	TransMem*	mem	= _freeTransMem.get();
	if(!mem) return;
	new(mem) RstTrans(	*this,
						_myPapi,
						_ipFwdSAP,
						source,
						destination,
						pdu,
						ipPayloadLength
						);
	}

void	Part::request(	Oscl::Protocol::IP::
						Port::Mux::Registrar::Srv::Req::Api::AttachReq&	msg
						) noexcept{
	_acceptorList.put(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						Port::Mux::Registrar::Srv::Req::Api::DetachReq&	msg
						) noexcept{
	_acceptorList.remove(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

