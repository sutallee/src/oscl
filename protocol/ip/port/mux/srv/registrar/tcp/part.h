/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_port_mux_srv_registrar_tcp_parth_
#define _oscl_protocol_ip_port_mux_srv_registrar_tcp_parth_
#include "oscl/protocol/ip/mux/client/receiver.h"
#include "oscl/protocol/ip/port/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/port/mux/registrar/srv/sync.h"
#include "oscl/queue/queue.h"
#include "oscl/protocol/ip/port/mux/registrar/srv/reqapi.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Port {
/** */
namespace Mux {
/** */
namespace Srv {
/** */
namespace Registrar {
/** */
namespace TCP {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(RstTrans) >	rst;
	};

/** */
class Part :	public Oscl::Protocol::IP::Mux::Client::Receiver,
				private Oscl::Protocol::IP::Port::Mux::Registrar::Srv::Req::Api
				{
	private:
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						Port::Mux::Registrar::Acceptor
						>									_acceptorList;
		/** */
		Oscl::Protocol::IP::
		Port::Mux::Registrar::Srv::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Protocol::IP::Port::Mux::Registrar::Srv::Sync	_sync;

		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;

		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&			_ipFwdSAP;

		/** */
		Oscl::Queue<TransMem>								_freeTransMem;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
				TransMem									transMem[],
				unsigned									nTransMem
				) noexcept;
		/** */
		virtual ~Part(){}

		/** */
		Oscl::Protocol::IP::
		Port::Mux::Registrar::Srv::Req::Api::SAP&			getSAP() noexcept;

		/** */
		Oscl::Protocol::IP::Port::Mux::Registrar::Api&	getSyncApi() noexcept;

		/** */
		void	done(RstTrans& trans) noexcept;

	public:	// Oscl::Protocol::IP::Mux::Client::Receiver
		/** */
		void	receive(	Oscl::Pdu::Pdu&				pdu,
							const Oscl::Protocol::IP::
							Address&					source,
							const Oscl::Protocol::IP::
							Address&					destination,
							unsigned					ipPayloadLength
							) noexcept;

	public:	// Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							Port::Mux::Registrar::Srv::Req::Api::AttachReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							Port::Mux::Registrar::Srv::Req::Api::DetachReq&	msg
							) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
