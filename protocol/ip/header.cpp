/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/atomic/incdec.h"
#include "header.h"
using namespace Oscl::Protocol::IP;
using namespace Oscl;

uint16_t	Oscl::Protocol::IP::getNewIpID() noexcept{
	static Oscl::Atomic::Integer	id;
	return (uint16_t)OsclAtomicIncrement(&id);
	}

uint16_t	Header::getChecksum() const noexcept{
	uint32_t	t	= type;
	t	&=	TtlProtoCheck::Checksum::FieldMask;
	t	>>=	TtlProtoCheck::Checksum::Lsb;
	return (uint16_t)t;
	}

unsigned char	Header::getHeaderLength() const noexcept{
	uint32_t	t	= length;
	t	&=	TypeLen::IHL::FieldMask;
	t	>>=	TypeLen::IHL::Lsb;
	return (unsigned char)t;
	}

unsigned	Header::getVersion() const noexcept{
	uint32_t	t	= length;
	t	&=	TypeLen::Version::FieldMask;
	t	>>=	TypeLen::Version::Lsb;
	return (unsigned)t;
	}

unsigned char	Header::getProtocolType() const noexcept{
	uint32_t	t	= type;
	t	&=	TtlProtoCheck::Protocol::FieldMask;
	t	>>=	TtlProtoCheck::Protocol::Lsb;
	return (unsigned char)t;
	}

unsigned	Header::getTotalLength() const noexcept{
	uint32_t	t	= length;
	t	&=	TypeLen::TotalLength::FieldMask;
	t	>>=	TypeLen::TotalLength::Lsb;
	return (unsigned)t;
	}

unsigned	Header::getPayloadLength() const noexcept{
	uint32_t	totalLen	= length;
	uint32_t	hdrLen		= totalLen;
	totalLen	&=	TypeLen::TotalLength::FieldMask;
	totalLen	>>=	TypeLen::TotalLength::Lsb;
	hdrLen		&=	TypeLen::IHL::FieldMask;
	hdrLen		>>=	TypeLen::IHL::Lsb;
	return (unsigned)(totalLen - (hdrLen<<2));
	}

void	Header::updateChecksum(unsigned nOptionBytes) noexcept{
	uint16_t	csum	= computeChecksum(nOptionBytes);
	if(!csum){
		csum	= ~csum;
		}
	type.changeBits(	TtlProtoCheck::Checksum::FieldMask,
						(csum<<TtlProtoCheck::Checksum::Lsb)
						);
	}

uint16_t	Header::computeChecksum(unsigned nOptionBytes) const noexcept{
	uint32_t	csum	= 0;
	uint32_t	l;
	l	= length;
	csum	+=	(l>>16) & 0x0000FFFF;
	csum	+=	l & 0x0000FFFF;
	l	= frag;
	csum	+=	(l>>16) & 0x0000FFFF;
	csum	+=	l & 0x0000FFFF;
	l	= type;
	l	&= ~(Oscl::Protocol::IP::TtlProtoCheck::Checksum::FieldMask);
	csum	+=	(l>>16) & 0x0000FFFF;
	csum	+=	l & 0x0000FFFF;
	l	=	source;
	csum	+=	(l>>16) & 0x0000FFFF;
	csum	+=	l & 0x0000FFFF;
	l	=	destination;
	csum	+=	(l>>16) & 0x0000FFFF;
	csum	+=	l & 0x0000FFFF;
	for(unsigned i=0;i<nOptionBytes;i+=2){
		uint16_t	w;
		w	= options[i];
		w	<<= 8;
		w	|=	options[i+1];
		csum	+= w;
		}
	csum	+= (csum >> 16);
	csum	= ((~csum) & 0x0000FFFF);
	return csum;
	}

bool	Header::checksumOK(unsigned nOptionBytes) const noexcept{
	uint16_t	check	= getChecksum();
	if(check == 0){
		// Checksum not used by sender.
		return true;
		}
	uint16_t	csum	= computeChecksum(nOptionBytes);
	if(check == csum){
		return true;
		}
	return false;
	}

