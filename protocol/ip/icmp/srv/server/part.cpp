/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/checksum/onescomp/b16/pdu/visitor.h"
#include "oscl/pdu/leaf.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Protocol::IP::ICMP::Srv::Server;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&					papi,
			Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
			TransMem									transMem[],
			unsigned									nTransMem
			) noexcept:
		CloseSync(*this,papi),
		_myPapi(papi),
		_ipFwdSAP(ipFwdSAP),
		_open(false),
		_nOutstandingTrans(0),
		_closeReq(0)
		{
	_stats.nDroppedEchoReq	= 0;
	_stats.nRxdEchoReq		= 0;
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

TransMem*	Part::allocTransMem() noexcept{
	TransMem*	mem	= _freeTransMem.get();
	if(!mem) return 0;
	++_nOutstandingTrans;
	return mem;
	}

void		Part::freeTransMem(TransMem* transMem) noexcept{
	_freeTransMem.put(transMem);
	--_nOutstandingTrans;
	if(_closeReq){
		_closeReq->returnToSender();
		_closeReq	= 0;
		}
	}

void	Part::echoRequest(	Oscl::Pdu::Pdu&						pdu,
							const Oscl::Protocol::IP::Header&	header,
							unsigned							icmpLen
							) noexcept{
	++_stats.nRxdEchoReq;
	TransMem*	mem	= allocTransMem();
	if(!mem){
		// No resources for reply
		++_stats.nDroppedEchoReq;
		return;
		}

	const unsigned	icmpHeaderLength	= 4;	// FIXME: use enumerated
	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;
	const unsigned	headerLength	= ipHeaderLength + icmpHeaderLength;
	const unsigned	totalLength		= ipHeaderLength+icmpLen;

	pdu.stripHeader(icmpHeaderLength);
	Trans&	trans	= *new(mem) Trans(	*this,
										_myPapi,
										_ipFwdSAP,
										header.source,
										pdu
										);

	unsigned char*	p	= (unsigned char*)&trans._pduMem;
	Oscl::Protocol::IP::Header&	ipHeader	= *(Oscl::Protocol::IP::Header*)p;
	if(sizeof(trans._pduMem) < (ipHeaderLength+icmpHeaderLength)){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::"
										"ICMP::Srv::Server::Part: "
										"header buffer too small"
										);
		done(trans);
		return;
		}
	ipHeader.source			= header.destination;
	ipHeader.destination	= header.source;
	ipHeader.length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader.frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader.type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_ICMP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader.updateChecksum(0);
	Oscl::Protocol::IP::ICMP::Packet::EchoHeader*
	icmpPdu	= (Oscl::Protocol::IP::ICMP::Packet::EchoHeader*)ipHeader.options;
	icmpPdu->header.type		= 0;
	icmpPdu->header.code		= 0;
	icmpPdu->header.checksum	= 0;
	const unsigned	icmpDataLen	= icmpLen-icmpHeaderLength;

	Oscl::Checksum::OnesComp::B16::PDU::Visitor	v;
	v._csum.accumulate(icmpPdu,icmpHeaderLength);
	pdu.visit(v,0,0,icmpDataLen);
	unsigned	checksum	= v._csum.final();

	icmpPdu->header.checksum	= checksum;
	trans._fixedPDU.addLength(headerLength);
	trans.start();
	}

void	Part::done(Trans& trans) noexcept{
	trans.~Trans();
	freeTransMem((TransMem*)&trans);
	}

bool	Part::accept(	Oscl::Pdu::Pdu&						pdu,
						const Oscl::Protocol::IP::Header&	header
						) noexcept{
	if(		header.getProtocolType()
		!=	Oscl::Protocol::IP::TtlProtoCheck::Protocol::Value_ICMP
		){
		return false;
		}

	if(!_open) return true;

	unsigned				len	= pdu.length();
	unsigned				icmpLen	= header.getPayloadLength();
	if(icmpLen > len){
		// Truncated ... discard
		return true;
		}

	Oscl::Checksum::OnesComp::B16::PDU::Visitor	v;
	pdu.visit(v,0,0,icmpLen);
	if(v._csum.final() != 0){
		// Bad ICMP checksum
		// discard
		return true;
		}

	struct {
		unsigned char			type;
		unsigned char			code;
		}icmpHeader;

	len	= pdu.read(&icmpHeader,2,0);
	if(len != 2) return true;

	switch(icmpHeader.type){
		case 3:		// Destination unreachable
			// This is a "Trap".
			// * No response to the network/peer is required.
			// * Local protocol specific session/transmitter/socket
			//   may need notification?
			switch(icmpHeader.code){
				case 0:	// net unreachable;
				case 1: // host unreachable;
				case 2: // protocol unreachable;
				case 3: // port unreachable;
				case 4: // fragmentation needed and DF set;
				case 5:	// source route failed.
					return true;
				default:
					return true;
				}
			return true;

		case 11:	// Time Exceeded Message
			// This is a "Trap".
			// * No response to the network/peer is required.
			// * Local protocol specific session/transmitter/socket
			//   may need notification?
			switch(icmpHeader.code){
				case 0:	// time to live exceeded in transit;
				case 1:	// fragment reassembly time exceeded.
					return true;
				default:
					return true;
				}
			return true;

		case 12:	// Parameter Problem Message
			// This is a "Trap".
			// * No response to the network/peer is required.
			// * Local protocol specific session/transmitter/socket
			//   may need notification?
			switch(icmpHeader.code){
				case 0:	// pointer indicates the error
					return true;
				default:
					return true;
				}
			return true;

		case 4:		// Source Quench Message
			// This is a "flow control" request. Its ultimate
			// destination is the socket identified by the
			// payload of this message (IP header).
			// * No response to the network/peer is required.
			if(icmpHeader.code != 0) return true;
			return true;

		case 5:		// Redirect Message
			// This is a "Trap".
			// Sent by gateways to cause the host to use a
			// different router than the "default".
			// * No response to the network/peer is required.
			// * Update local ARP cache for more direct route.
			switch(icmpHeader.code){
				case 0:	// Redirect datagrams for the Network.
				case 1:	// Redirect datagrams for the Host.
				case 2:	// Redirect datagrams for the Type of
						// Service and Network.
				case 3:	// Redirect datagrams for the Type of Service and Host.
				default:
					return true;
				}
			return true;

		case 0:		// Echo Reply
			// This is a response to an Echo Request.
			// Its ultimate destination is the matching
			// echo request transaction.
			// * No response to the network/peer is required.
			if(icmpHeader.code != 0) return true;
			return true;

		case 8:		// Echo
			// This is a request for an Echo reply.
			// This process will send an Echo Reply
			// to the host indicated by this message.
			if(icmpHeader.code != 0) return true;
			echoRequest(pdu,header,icmpLen);
			return true;

		case 14:	// Timestamp Reply
		case 13:	// Timestamp
		case 15:	// Information Request
		case 16:	// Information Reply
			return true;
		};
	return true;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	if(_open){
		// Attempt to open twice
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::"
										"ICMP::Srv::Server::Part: "
										"attempt to open twice"
										);
		return;
		}
	if(_closeReq){
		// Attempt to open before close completes
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::"
										"ICMP::Srv::Server::Part: "
										"attempt to open before close completes"
										);
		return;
		}
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	if(_closeReq){
		// Only one close request allowed.
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::"
										"ICMP::Srv::Server::Part: "
										"close protocol error"
										);
		return;
		}
	_open		= false;
	if(_nOutstandingTrans){
		_closeReq	= &msg;
		return;
		}
	msg.returnToSender();
	}

