/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_icmp_srv_server_parth_
#define _oscl_protocol_ip_icmp_srv_server_parth_
#include "oscl/protocol/ip/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/tx/srv/respapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "transaction.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace ICMP {
/** */
namespace Srv {
/** */
namespace Server {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Trans) >	trans;
	};

/** The Part is responsible for processing ICMP/IP protocol
	PDUs destined for this interface of this host.
	(E.g. Receive an ICMP Echo-Request PDU and respond with
	an ICMP Echo-Reply PDU.)
 */
class Part :	public Oscl::Protocol::IP::Mux::Registrar::Acceptor,
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api
				{
	private:
		/** */
		friend class Trans;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&		_ipFwdSAP;
		/** */
		Oscl::Queue<TransMem>							_freeTransMem;
		/** */
		bool											_open;
		/** */
		unsigned										_nOutstandingTrans;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

		/** */
		struct {
			/** */
			unsigned long	nDroppedEchoReq;
			/** */
			unsigned long	nRxdEchoReq;
			} _stats;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					papi,
				Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
				TransMem									transMem[],
				unsigned									nTransMem
				) noexcept;
	private:
		/** */
		TransMem*	allocTransMem() noexcept;
		/** */
		void		freeTransMem(TransMem* transMem) noexcept;
		/** */
		void	echoRequest(	Oscl::Pdu::Pdu&						pdu,
								const Oscl::Protocol::IP::Header&	header,
								unsigned							icmpLen
								) noexcept;
		/** */
		void	done(Trans& trans) noexcept;

	private: // Oscl::Protocol::IP::Mux::Registrar::Acceptor
		/** */
		bool	accept(	Oscl::Pdu::Pdu&						pdu,
						const Oscl::Protocol::IP::Header&	header
						) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api,
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	};

}
}
}
}
}
}

#endif
