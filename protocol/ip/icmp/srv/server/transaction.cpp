/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "transaction.h"
#include "part.h"

using namespace Oscl::Protocol::IP::ICMP::Srv::Server;

Trans::Trans(	Part&										context,
				Oscl::Mt::Itc::PostMsgApi&					papi,
				Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
				const Oscl::Protocol::IP::Address&			destIpAddr,
				Oscl::Pdu::ReadOnlyPdu&						icmpPayload
				) noexcept:
		_fixedPDU(	*this,
					*this,
					&_pduMem,
					0
					),
		_headerFragment(	*this,
							&_fixedPDU
							),
		_payloadFragment(	*this,
							&icmpPayload
							),
		_composite(*this),
		_context(context),
		_payload(	_composite,
					destIpAddr
					),
		_resonse(	ipFwdSAP.getReqApi(),
					*this,
					papi,
					_payload
					),
		_ipFwdSAP(ipFwdSAP)
		{
	}

void	Trans::start() noexcept{
	_composite.append(&_headerFragment);
	_composite.append(&_payloadFragment);
	_ipFwdSAP.post(_resonse.getSrvMsg());
	}

void	Trans::response(	Oscl::Protocol::IP::
							TX::Srv::Resp::Api::SendResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	Trans::free(void* fso) noexcept{
	// Do nothing
	}

