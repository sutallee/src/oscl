/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_icmp_srv_server_transactionh_
#define _oscl_protocol_ip_icmp_srv_server_transactionh_
#include "oscl/protocol/ip/tx/srv/respapi.h"
#include "oscl/memory/block.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/pdu/rocomposite.h"
#include "oscl/protocol/ip/header.h"
#include "oscl/protocol/ip/icmp/packet/echo.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace ICMP {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class Part;

/** This union is used to define a maximum sized data
	block used to hold all different ICMP reply messages.
 */
union PduMem {
	/** */
	enum{icmpHeaderSize=sizeof(Oscl::Protocol::IP::ICMP::Packet::EchoHeader)};
	/** */
	Oscl::Memory::
	AlignedBlock<		sizeof(Oscl::Protocol::IP::Header)
					+	sizeof(Oscl::Protocol::IP::ICMP::Packet::EchoHeader)
					>	_echoReply;
	};

/** */
class Trans :	private Oscl::Protocol::IP::TX::Srv::Resp::Api,
				private Oscl::FreeStore::Mgr
				{
	public:
		/** This memory may be directly or indirectly (via _fixedPDU)
			modified by the context after this object is constructed to
			hold the ICMP reply PDU value. Once the value has been set,
			the start() operation is invoked to begin the transaction.
			If this memory is directly modified, _fixedPDU must
			be notified of changes in length (e.g. addLength()).
		 */
		PduMem												_pduMem;

		/** This member is used by the context after this object
			is constructed to hold the ICMP reply PDU. After
			the PDU value has been set, the context invokes
			the start() operation to begin the transaction.
		 */
		Oscl::Pdu::Fixed								_fixedPDU;

		/** */
		Oscl::Pdu::ReadOnlyFragment						_headerFragment;

		/** */
		Oscl::Pdu::ReadOnlyFragment						_payloadFragment;

		/** */
		Oscl::Pdu::ReadOnlyComposite					_composite;

	private:

		/** */
		Part&												_context;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::SendPayload		_payload;
		/** */
		Oscl::Protocol::IP::TX::Srv::Resp::Api::SendResp	_resonse;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&			_ipFwdSAP;
		/** */
		Oscl::Handle<Oscl::Pdu::ReadOnlyPdu>				_handle;

	public:
		/** */
		Trans(	Part&											context,
				Oscl::Mt::Itc::PostMsgApi&						papi,
				Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&		ipFwdSAP,
				const Oscl::Protocol::IP::Address&				destIpAddr,
				Oscl::Pdu::ReadOnlyPdu&							icmpPayload
				) noexcept;
		/** This operation is invoked by the context after the
			context sets the fields of the "_pduMem".
		 */
		void	start() noexcept;

	private: // Oscl::Protocol::IP::TX::Srv::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							TX::Srv::Resp::Api::SendResp&	msg
							) noexcept;

	private: // Oscl::FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
}

#endif
