/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"

using namespace Oscl::Protocol::IP::UDP::Port::RX;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::Protocol::Socket::
					Connectionless::TX::Api&		transmitter,
					unsigned						port
					) noexcept:
		_decorator(*this,port),
		_myPapi(papi),
		_sync(*this,papi),
		_socket(_sync,transmitter)
		{
	}

Oscl::Protocol::IP::UDP::Port::RX::Entry&	Service::getPortMuxEntry() noexcept{
	return _decorator;
	}

void	Service::request(	Oscl::Protocol::IP::
							UDP::Port::RX::Req::Api::RecvReq&	msg
							) noexcept{
	_pending.put(&msg);
	}

Oscl::PDU::Api*	Service::forward(	Oscl::PDU::Api&			frame,
									const Oscl::Protocol::
									IP::Port::Path&			path
									) noexcept{
	Oscl::Protocol::IP::UDP::Port::RX::Req::Api::RecvReq*	req;
	// FIXME: Need to be able to queue some number of
	// frames for this socket.
	req	= _pending.get();
	if(!req){
		frame.free();
		return 0;
		}
	req->getPayload()._pdu	= &frame;
	req->getPayload()._path	= path;
	req->returnToSender();
	return 0;
	}

