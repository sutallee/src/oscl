/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_udp_port_tx_sendh_
#define _oscl_protocol_ip_udp_port_tx_sendh_
#include "oscl/protocol/ip/port/path.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace UDP {
/** */
namespace Port {
/** */
namespace TX {

/** */
class SendApi {
	public:
		/** */
		virtual ~SendApi() {}
		/** This operation forwards the "datagram" that is "length"
			bytes toward the "destination". Returns "length" if
			successful; zero if the port is closed; a value greater
			than "length" if the "datagram" cannot be deliverd
			for any reason (e.g. addressing), or the maximum datagram
			size if the datagram size exceeds system limitations.
		 */
		virtual unsigned	send(	const Oscl::Protocol::
									IP::Port::Path&				destination,
									const void*					datagram,
									unsigned					length
									) noexcept=0;
	};
}
}
}
}
}
}

#endif
