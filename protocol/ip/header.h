/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_headerh_
#define _oscl_protocol_ip_headerh_
#include "address.h"
#include "ipreg.h"
#include "oscl/endian/type.h"
#include <stdint.h>
//#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {

uint16_t	getNewIpID() noexcept;

/** */
//typedef Oscl::Endian::Big::U32	Option;

/** */
struct Header {
	/** */
	typedef Oscl::Endian::Big::U32	Length;
	/** */
	typedef Oscl::Endian::Big::U32	Frag;
	/** */
	typedef Oscl::Endian::Big::U32	Type;
	/** */
	enum{minHeaderBytes=(sizeof(uint32_t)*TypeLen::IHL::Value_Minimum)};
	/** */
	enum{maxHeaderBytes=(sizeof(uint32_t)*TypeLen::IHL::Value_Maximum)};
	/** */
	enum{optionWordSize=sizeof(uint32_t)};
	/** */
//	enum{maxOptionBytes=maxHeaderBytes-sizeof(Header)};
	enum{	maxOptionBytes=		maxHeaderBytes
							-	(		sizeof(uint32_t)
									*TypeLen::IHL::Value_Minimum
									)
			};
	/** */
	Length		length;
	/** */
	Frag		frag;
	/** */
	Type		type;
	/** */
	Address		source;
	/** */
	Address		destination;
	/** */
	uint8_t	options[maxOptionBytes];

	/** */
	uint16_t	getChecksum() const noexcept;

	/** */
	unsigned char	getHeaderLength() const noexcept;

	/** */
	unsigned	getVersion() const noexcept;

	/** */
	unsigned char	getProtocolType() const noexcept;

	/** */
	unsigned	getTotalLength() const noexcept;

	/** */
	unsigned	getPayloadLength() const noexcept;

	/** */
	bool	checksumOK(unsigned nOptionBytes) const noexcept;
	/** */
	uint16_t	computeChecksum(unsigned nOptionBytes) const noexcept;
	/** */
	void	updateChecksum(unsigned nOptionBytes) noexcept;
	};

}
}
}

#endif
