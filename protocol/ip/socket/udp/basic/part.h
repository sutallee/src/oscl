/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_udp_basic_parth_
#define _oscl_protocol_ip_socket_udp_basic_parth_
#include "oscl/protocol/socket/connectionless/udp/itc/rx/sync.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/protocol/socket/connectionless/udp/itc/tx/sync.h"
#include "oscl/protocol/ip/socket/udp/srv/subsys/rx/part.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/protocol/ip/subsys/rx/srv/udp/api.h"
#include "oscl/protocol/ip/fwd/api.h"
#include "oscl/protocol/socket/connectionless/udp/tx/srv/reqapi.h"
#include "oscl/protocol/socket/connectionless/udp/rx/srv/reqapi.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace UDP {
/** */
namespace Basic {

union TransMem {
	/** */
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock< sizeof(SendTrans) >	sendTrans;
	};

/** */
class Part :	private	Oscl::Protocol::IP::Port::Mux::Client::Receiver,
				private Oscl::Protocol::IP::UDP::Port::TX::SendApi,
				private Oscl::Protocol::IP::UDP::Port::RX::RecvApi,
				public	Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				private	Oscl::Protocol::Socket::
						Connectionless::UDP::TX::Srv::Req::Api,
				private	Oscl::Protocol::Socket::
						Connectionless::UDP::RX::Srv::Req::Api,
				private Oscl::FreeStore::Mgr
				{
	public:
		/** */
		friend class SendTrans;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::TX::Srv::Req::Api::ConcreteSAP	_txSAP;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::RX::Srv::Req::Api::ConcreteSAP	_rxSAP;

		/** */
		Oscl::Mt::Itc::Srv::CloseSync						_sync;
		/** */
		Oscl::Protocol::IP::
		Socket::UDP::Srv::Subsystem::RX::Part				_rx;
		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::UDP::Api&						_ipRxSubsystem;
		/** */
		Oscl::Protocol::IP::
		TX::Srv::Req::Api::SAP&								_ipFwdSAP;
		/** */
		Oscl::Protocol::IP::FWD::Api&						_ipFwdApi;
		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::Itc::TX::Sync					_txSock;
		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::Itc::RX::Sync					_rxSock;
		/** */
		Oscl::Queue<TransMem>								_freeTransMem;
		/** */
		Oscl::Queue<	Oscl::Protocol::Socket::
						Connectionless::UDP::
						TX::Srv::Req::Api::SendReq
						>							_pendingTxRequests;
		/** */
		Oscl::Queue<	Oscl::Protocol::Socket::
						Connectionless::UDP::
						RX::Srv::Req::Api::RecvReq
						>							_pendingRxRequests;
		/** */
		bool										_open;
		/** */
		unsigned									_nPendingTxTransactions;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::PostMsgApi&			ipRxSubsystemPapi,
				Oscl::Protocol::IP::
				Subsystem::RX::Srv::UDP::Api&		ipRxSubsystem,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Protocol::IP::FWD::Api&		ipFwdApi,
				unsigned							localPort,
				TransMem							transMem[],
				unsigned							nTransMem,
				Oscl::Protocol::IP::
				Port::Mux::Srv::Client::TransMem	rxTransMem[],
				unsigned							nRxTransMem
				) noexcept;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::TX::Api&	getTxSocketApi() noexcept;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::RX::Api&	getRxSocketApi() noexcept;

		/** */
		void	syncOpen() noexcept;

		/** */
		void	syncClose() noexcept;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::TX::Srv::Req::Api::SAP&	getTxSAP() noexcept;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::UDP::RX::Srv::Req::Api::SAP&	getRxSAP() noexcept;

	private:
		/** */
		void	done(SendTrans& trans) noexcept;

	public: // Oscl::Protocol::IP::UDP::Port::TX::SendApi
		/** */
		unsigned	send(	const Oscl::Protocol::
							IP::Port::Path&				destination,
							const void*					datagram,
							unsigned					length
							) noexcept;

	public: // Oscl::Protocol::IP::UDP::Port::TX::SendApi
		/** */
		unsigned	recv(	Oscl::Protocol::
							IP::Port::Path&				path,
							void*						datagram,
							unsigned					maxLength
							) noexcept;

	private: // Oscl::Protocol::IP::Port::Mux::Client::Receiver
		/** */
		void	receive(	Oscl::Pdu::Pdu&				pdu,
							unsigned long				remoteAddress,
							unsigned long				localAddress,
							unsigned					remotePort,
							unsigned					localPort,
							unsigned					ipPayloadLength
							) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private: // Oscl::Protocol::Socket::Connectionless::UDP::TX::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::Socket::Connectionless::
							UDP::TX::Srv::Req::Api::SendReq&	msg
							) noexcept;

	private: // Oscl::Protocol::Socket::Connectionless::UDP::RX::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::Socket::Connectionless::
							UDP::RX::Srv::Req::Api::RecvReq&	msg
							) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
}

#endif
