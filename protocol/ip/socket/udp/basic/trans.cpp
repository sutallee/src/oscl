/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "trans.h"
#include "part.h"
#include "oscl/checksum/onescomp/b16/calculator.h"

using namespace Oscl::Protocol::IP::Socket::UDP::Basic;

SendTrans::SendTrans(	Part&										context,
						Oscl::Mt::Itc::PostMsgApi&					papi,
						Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
						Oscl::Protocol::Socket::Connectionless::
						UDP::TX::Srv::Req::Api::SendReq&			msg
						) noexcept:
		_headerPDU(	*this,
					*this,
					&_headerMem,
					(		Header::minHeaderBytes
						+	sizeof(Oscl::Protocol::IP::UDP::Header)
						),
					(		Header::minHeaderBytes
						+	sizeof(Oscl::Protocol::IP::UDP::Header)
						)
					),
		_payloadPDU(	*this,
						*this,
						(void*)msg._payload._datagram,	// FIXME: const
						msg._payload._length,
						msg._payload._length
						),
		_headerFragment(	*this,
							&_headerPDU
							),
		_payloadFragment(	*this,
							&_payloadPDU
							),
		_composite(*this),
		_context(context),
		_destAddr(msg._payload._path.peerIpAddress),
		_payload(	_composite,
					_destAddr
					),
		_response(	ipFwdSAP.getReqApi(),
					*this,
					papi,
					_payload
					),
		_msg(msg)
		{
	Oscl::Protocol::IP::Header*
	ipHeader	= (Oscl::Protocol::IP::Header*)&_headerMem;
	Oscl::Protocol::IP::UDP::Header*
	udpHeader	= (Oscl::Protocol::IP::UDP::Header*)ipHeader->options;

	const Oscl::Protocol::IP::Port::Path&	destination	= msg._payload._path;
	const unsigned	length		= msg._payload._length;
	const void*		datagram	= msg._payload._datagram;

	const unsigned	udpHeaderLength	= sizeof(IP::UDP::Header);
	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;
	const unsigned	totalLength		= ipHeaderLength+udpHeaderLength+length;
	const unsigned	udpLength		= udpHeaderLength+length;

	ipHeader->source.assign(destination.localIpAddress);
	ipHeader->destination.assign(destination.peerIpAddress);
	ipHeader->length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader->frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader->type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_UDP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader->updateChecksum(0);

	Oscl::Checksum::OnesComp::B16::Calculator	csum;

	// Pseudo header
	csum.accumulate(&ipHeader->source,sizeof(ipHeader->source));
	csum.accumulate(&ipHeader->destination,sizeof(ipHeader->destination));
	Oscl::Endian::Big::U16	uLength;
	uLength	= udpLength;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_UDP;
	csum.accumulate(&protocol,sizeof(protocol));
	csum.accumulate(&uLength,sizeof(uLength));

	// UDP header
	udpHeader->sourcePort		= destination.localPortNum;
	udpHeader->destinationPort	= destination.peerPortNum;
	udpHeader->length			= udpLength;
	udpHeader->checksum			= 0;
	csum.accumulate(udpHeader,udpHeaderLength);

	// Datagram
	csum.accumulate(datagram,length);
	uint16_t	checksum	= csum.final();
	if(!checksum){
		checksum	= ~checksum;
		}

	// Finish the UDP header
	udpHeader->length			= udpLength;
	udpHeader->checksum			= checksum;

	// Build the PDU
	_composite.append(&_headerFragment);
	_composite.append(&_payloadFragment);

	ipFwdSAP.post(_response.getSrvMsg());
	}

void	SendTrans::response(	Oscl::Protocol::IP::
								TX::Srv::Resp::Api::SendResp&	msg
								) noexcept{
	_msg.returnToSender();
	_context.done(*this);
	}

void	SendTrans::free(void* fso) noexcept{
	// Do nothing
	}

