/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/error/info.h"
#include "part.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/pdu/rocomposite.h"
#include "oscl/protocol/ip/udp/header.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Protocol::IP::Socket::UDP::Basic;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Mt::Itc::PostMsgApi&			ipRxSubsystemPapi,
			Oscl::Protocol::IP::
			Subsystem::RX::Srv::UDP::Api&		ipRxSubsystem,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&				ipFwdSAP,
			Oscl::Protocol::IP::FWD::Api&		ipFwdApi,
			unsigned							localPort,
			TransMem							transMem[],
			unsigned							nTransMem,
			Oscl::Protocol::IP::
			Port::Mux::Srv::Client::TransMem	rxTransMem[],
			unsigned							nRxTransMem
			) noexcept:
		_myPapi(myPapi),
		_txSAP(*this,myPapi),
		_rxSAP(*this,myPapi),
		_sync(*this,myPapi),
		_rx(	ipRxSubsystemPapi,
				myPapi,
				localPort,
				*this,	// receiver is me for now.
				rxTransMem,
				nRxTransMem
				),
		_ipRxSubsystem(ipRxSubsystem),
		_ipFwdSAP(ipFwdSAP),
		_ipFwdApi(ipFwdApi),
		_txSock(*this),
		_rxSock(*this,_txSock),
		_open(false),
		_nPendingTxTransactions(0),
		_closeReq(0)
		{
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

Oscl::Protocol::Socket::
Connectionless::TX::Api&	Part::getTxSocketApi() noexcept{
	return _txSock;
	}

Oscl::Protocol::Socket::
Connectionless::RX::Api&	Part::getRxSocketApi() noexcept{
	return _rxSock;
	}

void	Part::syncOpen() noexcept{
	_sync.syncOpen();
	// The _rx.syncOpen() MUST be invoked from the callers
	// thread AND the caller must NOT share a thread with
	// the socket. Otherwise, we'll deadlock.
	// This is because the _rx._client service shares a
	// thread with this socket, but must asynchronously
	// cancel its receive requests.
	// _sync.syncOpen above only deals with issues
	// within the Socket service itself.
	_rx.syncOpen();
	_ipRxSubsystem.getMuxApi().attach(_rx.getAcceptor());
	}

void	Part::syncClose() noexcept{
	// See the comments in syncOpen() concerning
	// the threading issues involved in this.
	_ipRxSubsystem.getMuxApi().detach(_rx.getAcceptor());
	_rx.syncClose();
	_sync.syncClose();
	}

Oscl::Protocol::Socket::
Connectionless::UDP::TX::Srv::Req::Api::SAP&	Part::getTxSAP() noexcept{
	return _txSAP;
	}

Oscl::Protocol::Socket::
Connectionless::UDP::RX::Srv::Req::Api::SAP&	Part::getRxSAP() noexcept{
	return _rxSAP;
	}

void	Part::done(SendTrans& trans) noexcept{
	--_nPendingTxTransactions;
	trans.~SendTrans();
	_freeTransMem.put((TransMem*)&trans);
	if(!_open){
		if(_nPendingTxTransactions) return;
		if(_closeReq){
			_closeReq->returnToSender();
			}
		}

	Oscl::Protocol::Socket::Connectionless::
	UDP::TX::Srv::Req::Api::SendReq*
	req	= _pendingTxRequests.get();
	if(req){
		req->process();
		}
	}

unsigned	Part::send(	const Oscl::Protocol::
						IP::Port::Path&				destination,
						const void*					datagram,
						unsigned					length
						) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler			srh;
	Oscl::Protocol::Socket::Connectionless::
	UDP::TX::Srv::Req::SendPayload	payload(	destination,
												datagram,
												length
												);

	Oscl::Protocol::Socket::Connectionless::
	UDP::TX::Srv::Req::Api::SendReq	request(	*this,
												payload,
												srh
												);
	_txSAP.postSync(request);
	return payload._length;
	}

unsigned	Part::recv(	Oscl::Protocol::
						IP::Port::Path&				path,
						void*						datagram,
						unsigned					maxLength
						) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler			srh;
	Oscl::Protocol::Socket::Connectionless::
	UDP::RX::Srv::Req::RecvPayload	payload(	datagram,
												maxLength
												);

	Oscl::Protocol::Socket::Connectionless::
	UDP::RX::Srv::Req::Api::RecvReq	request(	*this,
												payload,
												srh
												);
	_txSAP.postSync(request);
	path	= payload._path;
	return payload._length;
	}

void	Part::receive(	Oscl::Pdu::Pdu&		pdu,
						unsigned long		remoteAddress,
						unsigned long		localAddress,
						unsigned			remotePort,
						unsigned			localPort,
						unsigned			ipPayloadLength
						) noexcept{
	Oscl::Protocol::Socket::Connectionless::
	UDP::RX::Srv::Req::Api::RecvReq*
	msg	= _pendingRxRequests.get();
	if(!msg) return;
	msg->_payload._path.peerIpAddress	= remoteAddress;
	msg->_payload._path.localIpAddress	= localAddress;
	msg->_payload._path.peerPortNum		= remotePort;
	msg->_payload._path.localPortNum	= localPort;
	msg->_payload._length	= pdu.read(	msg->_payload._datagram,
										msg->_payload._length,
										0
										);
	msg->returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_open	= false;
	if(_nPendingTxTransactions){
		_closeReq	= &msg;
		return;
		}
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::Socket::Connectionless::
						UDP::TX::Srv::Req::Api::SendReq&	msg
						) noexcept{
	TransMem*	mem	= _freeTransMem.get();
	if(!mem){
		_pendingTxRequests.put(&msg);
		return;
		}
	++_nPendingTxTransactions;
	new (mem) SendTrans(	*this,
							_myPapi,
							_ipFwdSAP,
							msg
							);
	}

void	Part::request(	Oscl::Protocol::Socket::Connectionless::
						UDP::RX::Srv::Req::Api::RecvReq&	msg
						) noexcept{
	_pendingRxRequests.put(&msg);
	}

void	Part::free(void* fso) noexcept{
	// do nothing
	}
