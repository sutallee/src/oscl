/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_udp_srv_subsys_rx_parth_
#define _oscl_protocol_ip_socket_udp_srv_subsys_rx_parth_
#include "oscl/protocol/ip/port/mux/srv/client/part.h"
#include "oscl/protocol/ip/port/mux/srv/server/part.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace UDP {
/** */
namespace Srv {
/** */
namespace Subsystem {
/** */
namespace RX {

/** */
class Part {
	private:
		/** */
		Oscl::Protocol::IP::Port::Mux::Srv::Server::Part	_server;

		/** */
		Oscl::Protocol::IP::Port::Mux::Srv::Client::Part	_client;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			subsysRxPapi,
				Oscl::Mt::Itc::PostMsgApi&			socketRxPapi,
				unsigned							localPort,
				Oscl::Protocol::IP::
				Port::Mux::Client::Receiver&		receiver,
				Oscl::Protocol::IP::
				Port::Mux::Srv::Client::TransMem	clientTransMem[],
				unsigned							nClientTransMem
				) noexcept;

		/** */
		Oscl::Protocol::IP::
		Port::Mux::Registrar::Acceptor&	getAcceptor() noexcept;

	public:
		/** */
		void	syncOpen() noexcept;

		/** */
		void	syncClose() noexcept;
	};

}
}
}
}
}
}
}
}

#endif
