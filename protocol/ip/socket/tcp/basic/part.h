/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_tcp_basic_parth_
#define _oscl_protocol_ip_socket_tcp_basic_parth_
#include "oscl/protocol/ip/socket/tcp/srv/subsys/rx/part.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/protocol/ip/subsys/rx/srv/tcp/api.h"
#include "oscl/protocol/ip/tcp/conn/mux/srv/registrar/part.h"
#include "oscl/protocol/ip/socket/tcp/api.h"
#include "oscl/mt/itc/delay/respapi.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace TCP {
/** */
namespace Basic {

/** */
class Part :	public	Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				public	Oscl::Protocol::IP::Socket::TCP::Api,
				private Oscl::Mt::Itc::Delay::Resp::Api
				{
	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync			_sync;
		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Srv::Registrar::Part	_mux;
		/** */
		Oscl::Protocol::IP::
		Socket::TCP::Srv::Subsystem::RX::Part	_rx;
		/** */
		Oscl::Protocol::IP::
		Subsystem::RX::Srv::TCP::Api&			_ipRxSubsystem;
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						Socket::TCP::
						TickObserver
						>						_tickObservers;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	_delaySAP;
		/** */
		Oscl::Mt::Itc::Delay::
		Req::Api::DelayPayload					_delayPayload;
		/** */
		Oscl::Mt::Itc::Delay::
		Resp::Api::DelayResp					_delayResp;
		/** */
		Oscl::Mt::Itc::Delay::
		Req::Api::CancelPayload					_delayCancelPayload;
		/** */
		Oscl::Mt::Itc::Delay::
		Resp::Api::CancelResp					_delayCancelResp;
		/** */
		const unsigned							_localPortNum;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::PostMsgApi&			ipRxSubsystemPapi,
				Oscl::Protocol::IP::
				Subsystem::RX::Srv::TCP::Api&		ipRxSubsystem,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Mt::Itc::
				Delay::Req::Api::SAP&				delaySAP,
				unsigned							localPort,
				Oscl::Protocol::IP::
				Port::Mux::Srv::Client::TransMem	rxTransMem[],
				unsigned							nRxTransMem,
				Oscl::Protocol::IP::
				TCP::Conn::Mux::Srv::
				Registrar::TransMem					muxTransMem[],
				unsigned							nMuxTransMem
				) noexcept;

		/** */
		void	syncOpen() noexcept;

		/** */
		void	syncClose() noexcept;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::
		Srv::Req::Api::SAP&				getMuxSAP() noexcept;

	public:	// Oscl::Protocol::IP::Socket::TCP::Api

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::Api&	getMuxApi() noexcept;

		/** */
		void	attach(TickObserver& to) noexcept;

		/** */
		void	detach(TickObserver& to) noexcept;

		/** */
		unsigned	getLocalPortNumber() const noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Delay::
							Resp::Api::DelayResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Delay::
							Resp::Api::CancelResp&	msg
							) noexcept;

	};

}
}
}
}
}
}

#endif
