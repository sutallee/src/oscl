/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_tcp_basic_transh_
#define _oscl_protocol_ip_socket_tcp_basic_transh_
#include "oscl/protocol/ip/tx/srv/respapi.h"
#include "oscl/memory/block.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/pdu/rocomposite.h"
#include "oscl/protocol/ip/header.h"
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/protocol/socket/connection/tcp/tx/srv/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace TCP {
/** */
namespace Basic {

/** */
class Part;

/** This union is used to define a maximum sized data
	block used to hold all different TCP reply messages.
 */
union HeaderMem {
	/** */
	Oscl::Memory::
	AlignedBlock<		sizeof(Oscl::Protocol::IP::Header)
					+	sizeof(Oscl::Protocol::IP::TCP::Header)
					>	header;
	};

/** */
class SendTrans :	private Oscl::Protocol::IP::TX::Srv::Resp::Api,
					private Oscl::FreeStore::Mgr
					{
	public:
		/** This memory is used to hold an TCP/IP header.
		 */
		HeaderMem										_headerMem;

		/** */
		Oscl::Pdu::Fixed								_headerPDU;

		/** */
		Oscl::Pdu::Fixed								_payloadPDU;

		/** */
		Oscl::Pdu::ReadOnlyFragment						_headerFragment;

		/** */
		Oscl::Pdu::ReadOnlyFragment						_payloadFragment;

		/** */
		Oscl::Pdu::ReadOnlyComposite					_composite;

	private:
		/** */
		Part&												_context;
		/** */
		Oscl::Protocol::IP::AddressInit						_destAddr;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::SendPayload		_payload;
		/** */
		Oscl::Protocol::IP::TX::Srv::Resp::Api::SendResp	_response;
		/** */
		Oscl::Protocol::Socket::Connection::
		TCP::TX::Srv::Req::Api::SendReq&					_msg;

	public:
		/** */
		SendTrans(	Part&											context,
					Oscl::Mt::Itc::PostMsgApi&						papi,
					Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&		ipFwdSAP,
					Oscl::Protocol::Socket::Connection::
					TCP::TX::Srv::Req::Api::SendReq&				msg
					) noexcept;

	private: // Oscl::Protocol::IP::TX::Srv::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							TX::Srv::Resp::Api::SendResp&	msg
							) noexcept;

	private: // Oscl::FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

/** */
class RstTrans :	private Oscl::Protocol::IP::TX::Srv::Resp::Api,
					private Oscl::FreeStore::Mgr
					{
	public:
		/** */
		enum{pduSize	=		Oscl::Protocol::IP::
								Header::minHeaderBytes
							+	Oscl::Protocol::IP::
								TCP::Header::minHeaderSize
								};
		/** */
		Oscl::Memory::
		AlignedBlock< pduSize >							_pduMem;

		/** */
		Oscl::Pdu::Fixed								_pdu;

	private:
		/** */
		Part&												_context;
		/** */
		Oscl::Protocol::IP::AddressInit						_destAddr;
		/** */
		Oscl::Protocol::IP::TX::Srv::Req::SendPayload		_payload;
		/** */
		Oscl::Protocol::IP::TX::Srv::Resp::Api::SendResp	_response;

	public:
		/** */
		RstTrans(	Part&										context,
					Oscl::Mt::Itc::PostMsgApi&					papi,
					Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
					unsigned long								remoteAddress,
					unsigned long								localAddress,
					const Oscl::Pdu::ReadOnlyPdu&				segment,
					unsigned									ipPayloadLength
					) noexcept;

	private: // Oscl::Protocol::IP::TX::Srv::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							TX::Srv::Resp::Api::SendResp&	msg
							) noexcept;

	private: // Oscl::FreeStoreMgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}
}
}

#endif
