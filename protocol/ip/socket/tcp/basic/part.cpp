/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/ip/tcp/header.h"

using namespace Oscl::Protocol::IP::Socket::TCP::Basic;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Mt::Itc::PostMsgApi&			ipRxSubsystemPapi,
			Oscl::Protocol::IP::
			Subsystem::RX::Srv::TCP::Api&		ipRxSubsystem,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&				ipFwdSAP,
			Oscl::Mt::Itc::
			Delay::Req::Api::SAP&				delaySAP,
			unsigned							localPort,
			Oscl::Protocol::IP::
			Port::Mux::Srv::Client::TransMem	rxTransMem[],
			unsigned							nRxTransMem,
			Oscl::Protocol::IP::
			TCP::Conn::Mux::Srv::
			Registrar::TransMem					muxTransMem[],
			unsigned							nMuxTransMem
			) noexcept:
		_sync(	*this,
				myPapi
				),
		_mux(	myPapi,
				ipFwdSAP,
				muxTransMem,
				nMuxTransMem
				),
		_rx(	ipRxSubsystemPapi,
				myPapi,
				localPort,
				_mux,
				rxTransMem,
				nRxTransMem
				),
		_ipRxSubsystem(ipRxSubsystem),
		_delaySAP(delaySAP),
		_delayPayload(100),	// FIXME: 100ms
		_delayResp(	delaySAP.getReqApi(),
					*this,
					myPapi,
					_delayPayload
					),
		_delayCancelPayload(_delayResp.getSrvMsg()),
		_delayCancelResp(	delaySAP.getReqApi(),
							*this,
							myPapi,
							_delayCancelPayload
							),
		_localPortNum(localPort),
		_closeReq(0)
		{
	}

void	Part::syncOpen() noexcept{
	_mux.syncOpen();
	// The _rx.syncOpen() MUST be invoked from the callers
	// thread AND the caller must NOT share a thread with
	// the socket. Otherwise, we'll deadlock.
	// This is because the _rx._client service shares a
	// thread with this socket, but must asynchronously
	// cancel its receive requests.
	_rx.syncOpen();
	_ipRxSubsystem.getMuxApi().attach(_rx.getAcceptor());
	_sync.syncOpen();
	}

void	Part::syncClose() noexcept{
	_sync.syncClose();
	// See the comments in syncOpen() concerning
	// the threading issues involved in this.
	_ipRxSubsystem.getMuxApi().detach(_rx.getAcceptor());
	_rx.syncClose();
	_mux.syncClose();
	}

Oscl::Protocol::IP::TCP::Conn::Mux::Registrar::Api&	Part::getMuxApi() noexcept{
	return _mux.getSyncApi();
	}

Oscl::Protocol::IP::
TCP::Conn::Mux::Registrar::Srv::Req::Api::SAP&	Part::getMuxSAP() noexcept{
	return _mux.getSAP();
	}

void	Part::attach(TickObserver& to) noexcept{
	_tickObservers.put(&to);
	}

void	Part::detach(TickObserver& to) noexcept{
	_tickObservers.remove(&to);
	}

unsigned	Part::getLocalPortNumber() const noexcept{
	return _localPortNum;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_delaySAP.post(_delayResp.getSrvMsg());
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	_delaySAP.post(_delayCancelResp.getSrvMsg());
	}

void	Part::response(	Oscl::Mt::Itc::Delay::
						Resp::Api::DelayResp&	msg
						) noexcept{
	if(_closeReq) return;
	Oscl::Protocol::IP::Socket::TCP::TickObserver*	to;
	for(	to	= _tickObservers.first();
			to;
			to	= _tickObservers.next(to)
			){
		to->tick();
		}
	_delayPayload._timer._milliseconds	= 100;
	_delaySAP.post(_delayResp.getSrvMsg());
	}

void	Part::response(	Oscl::Mt::Itc::Delay::
						Resp::Api::CancelResp&	msg
						) noexcept{
	// FIXME: This where I need to
	// finish the close process.
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

