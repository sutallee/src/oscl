/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_tcp_basic_configh_
#define _oscl_protocol_ip_socket_tcp_basic_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace TCP {
/** */
namespace Basic {

/** */
template <	unsigned	nRxTransMem,
			unsigned	nMuxTransMem
			>
class Config {
	private:
		/** */
		Oscl::Protocol::IP::
		Port::Mux::Srv::Client::TransMem	rxTransMem[nRxTransMem];
		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Srv::
		Registrar::TransMem					muxTransMem[nMuxTransMem];

	public:
		/** */
		Part								_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Mt::Itc::PostMsgApi&			ipRxSubsystemPapi,
				Oscl::Protocol::IP::
				Subsystem::RX::Srv::TCP::Api&		ipRxSubsystem,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Mt::Itc::
				Delay::Req::Api::SAP&				delaySAP,
				unsigned							localPort
				) noexcept:
				_part(	myPapi,
						ipRxSubsystemPapi,
						ipRxSubsystem,
						ipFwdSAP,
						delaySAP,
						localPort,
						rxTransMem,
						nRxTransMem,
						muxTransMem,
						nMuxTransMem
						)
				{
			}
	};

}
}
}
}
}
}

#endif
