/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_socket_tcp_apih_
#define _oscl_protocol_ip_socket_tcp_apih_
#include "oscl/protocol/ip/tcp/conn/mux/registrar/api.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Socket {
/** */
namespace TCP {

/** This abstract class is defined to allow TCP connections
	to receive the periodic timer ticks they need to implement
	retransmission and delayed ACK timers. To reduce the
	need for each connection of a particular socket having
	its own periodic delay infrastructure, the socket is
	responsible for providing the delay service. This is
	possible as long as the socket and its connections
	all run in the same thread.
 */
class TickObserver : public Oscl::QueueItem {
	public:
		/** */
		virtual ~TickObserver(){}
		/** Invoked once every period. The period is intentionally
			left unspecified (as it is in the TCP spec), but will
			typically be between 10ms and 1s.
		 */
		virtual void	tick() noexcept=0;
	};

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual	Oscl::Protocol::IP::
				TCP::Conn::Mux::Registrar::Api&	getMuxApi() noexcept=0;
		/** */
		virtual	void	attach(TickObserver& to) noexcept=0;
		/** */
		virtual	void	detach(TickObserver& to) noexcept=0;
		/** */
		virtual unsigned	getLocalPortNumber() const noexcept=0;
	};

}
}
}
}
}

#endif
