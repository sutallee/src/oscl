/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_opth_
#define _oscl_protocol_ip_tcp_opth_
#include "oscl/endian/type.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {

/** */
struct Opt {
	public:
		/** */
		unsigned char	kind;
		/** */
		unsigned char	size;
	private:
		/** */
		unsigned char	tsval[4];
		/** */
		unsigned char	tsecr[4];
	public:
		/** */
		inline unsigned long	getTSval() const noexcept{
			unsigned long	value;
			value	=	tsval[0];
			value	<<=	8;
			value	|=	tsval[1];
			value	<<=	8;
			value	|=	tsval[2];
			value	<<=	8;
			value	|=	tsval[3];
			return value;
			}
		/** */
		inline void	setTSval(unsigned long value) const noexcept{
			tsval[3]	= (unsigned char)(value>> 0);
			tsval[2]	= (unsigned char)(value>> 8);
			tsval[1]	= (unsigned char)(value>>16);
			tsval[0]	= (unsigned char)(value>>32);
			}

		/** */
		inline unsigned long	getTSecr() const noexcept{
			unsigned long	value;
			value	=	tsecr[0];
			value	<<=	8;
			value	|=	tsecr[1];
			value	<<=	8;
			value	|=	tsecr[2];
			value	<<=	8;
			value	|=	tsecr[3];
			return value;
			}
		/** */
		inline void	setTSecr(unsigned long value) const noexcept{
			tsecr[3]	= (unsigned char)(value>> 0);
			tsecr[2]	= (unsigned char)(value>> 8);
			tsecr[1]	= (unsigned char)(value>>16);
			tsecr[0]	= (unsigned char)(value>>32);
			}
	};

}
}
}
}

#endif
