/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_port_rx_itc_srv_serviceh_
#define _oscl_protocol_ip_tcp_port_rx_itc_srv_serviceh_
#include "oscl/protocol/ip/tcp/port/rx/mux/mux.h"
#include "oscl/protocol/ip/tcp/port/rx/mux/entry.h"
#include "oscl/protocol/ip/tcp/port/rx/itc/sync.h"
#include "oscl/protocol/socket/connection/tx/api.h"
//#include "oscl/protocol/socket/connection/tcp/itc/rx/sync.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Port {
/** */
namespace RX {

/** This ITC service is the "top" layer of the IP/TCP
	protocol receive chain. It is responsible for
	receving datagrams directed at a single TCP
	port, and interacting with user requests to
	read the datagrams.
 */
class Service :	private Oscl::Protocol::IP::TCP::Port::RX::ForwardApi,
				private Oscl::Protocol::IP::TCP::Port::RX::Req::Api
				{
	private:
		/** */
		Oscl::Protocol::IP::TCP::Port::RX::Mux::Entry				_decorator;
		/** */
		Oscl::Mt::Itc::PostMsgApi&									_myPapi;
		/** */
		Oscl::Protocol::IP::TCP::Port::RX::Sync						_sync;
		/** */
//		Oscl::Protocol::Socket::Connection::TCP::Itc::RX::Sync	_socket;
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						TCP::Port::RX::Req::Api::RecvReq
						>										_pending;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::Protocol::Socket::
					Connection::TX::Api&		transmitter,
					unsigned						port
					) noexcept;

		/** */
		Oscl::Protocol::IP::TCP::Port::RX::Entry&	getPortMuxEntry() noexcept;

	private: // Oscl::Protocol::IP::TCP::Port::RX::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							TCP::Port::RX::Req::Api::RecvReq&	msg
							) noexcept;

	private:	// Oscl::Protocol::IP::TCP::Port::RX::Api
		Oscl::PDU::Api* forward(	Oscl::PDU::Api&			frame,
									const Oscl::Protocol::
									IP::Port::Path&			path
									) noexcept;
	};
}
}
}
}
}
}

#endif
