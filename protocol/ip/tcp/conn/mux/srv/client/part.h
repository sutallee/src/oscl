/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_mux_srv_client_parth_
#define _oscl_protocol_ip_tcp_conn_mux_srv_client_parth_
#include "oscl/mt/itc/srv/close.h"
#include "oscl/protocol/ip/tcp/conn/mux/client/receiver.h"
#include "oscl/protocol/ip/tcp/conn/mux/server/srv/respapi.h"
#include "oscl/memory/block.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Mux {
/** */
namespace Srv {
/** */
namespace Client {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Trans) >	trans;
	};

/** */
class Part :	public	Oscl::Mt::Itc::Srv::CloseSync,
				public	Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Protocol::IP::
						TCP::Conn::Mux::Server::Srv::Resp::Api
				{
	public:
		/** */
		friend class Trans;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Server::Srv::Req::Api::SAP&			_rxSAP;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Client::Receiver&					_receiver;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Server::Srv::Resp::Api::CancelResp	_cancelResp;

		/** */
		Oscl::Queue<TransMem>					_freeTransMem;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

		/** */
		bool									_open;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Protocol::IP::
				TCP::Conn::Mux::Server::Srv::Req::Api::SAP&	rxSAP,
				Oscl::Protocol::IP::
				TCP::Conn::Mux::Client::Receiver&			receiver,
				TransMem									transMem[],
				unsigned									nTransMem
				) noexcept;

		/** */
		void	done(Trans& transaction) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::
							Srv::Close::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::
							Srv::Close::Req::Api::CloseReq&	msg
							) noexcept;

	private: // Oscl::Protocol::IP::TCP::Conn::Mux::Server::Srv::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Server::
							Srv::Resp::Api::RecvResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Server::
							Srv::Resp::Api::CancelResp&	msg
							) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
