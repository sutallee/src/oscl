/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Mux::Srv::Client;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::Protocol::IP::
			TCP::Conn::Mux::Server::Srv::Req::Api::SAP&	rxSAP,
			Oscl::Protocol::IP::
			TCP::Conn::Mux::Client::Receiver&			receiver,
			TransMem									transMem[],
			unsigned									nTransMem
			) noexcept:
		CloseSync(*this,myPapi),
		_myPapi(myPapi),
		_rxSAP(rxSAP),
		_receiver(receiver),
		_cancelResp(	rxSAP.getReqApi(),
						*this,
						myPapi
						),
		_closeReq(0),
		_open(false)
		{
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

void	Part::done(Trans& transaction) noexcept{
	transaction.~Trans();
	if(_open){
		new (&transaction) Trans(	*this,
									_myPapi,
									_rxSAP
									);
		}
	else {
		_freeTransMem.put((TransMem*)&transaction);
		}
	}

void	Part::request(	Oscl::Mt::Itc::
						Srv::Close::Req::Api::OpenReq&	msg
						) noexcept{
	TransMem*	next;

	while((next=_freeTransMem.get())){
		new (next) Trans(	*this,
							_myPapi,
							_rxSAP
							);
		}
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::
						Srv::Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	_open		= false;
	_rxSAP.post(_cancelResp.getSrvMsg());
	}

void	Part::response(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Server::Srv::Resp::Api::RecvResp&	msg
						) noexcept{
	// Not used
	}

void	Part::response(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Server::Srv::Resp::Api::CancelResp&	msg
						) noexcept{
	if(_closeReq){
		_closeReq->returnToSender();
		}
	_closeReq	= 0;
	}

