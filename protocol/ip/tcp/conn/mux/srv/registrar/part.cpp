/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/checksum/onescomp/b16/pdu/visitor.h"
#include "part.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Mux::Srv::Registrar;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
			TransMem									transMem[],
			unsigned									nTransMem
			) noexcept:
		CloseSync(*this,myPapi),
		_sap(*this,myPapi),
		_sync(_sap),
		_myPapi(myPapi),
		_ipFwdSAP(ipFwdSAP),
		_nPendingTransactions(0),
		_closeReq(0),
		_open(false)
		{
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	}

Oscl::Protocol::IP::TCP::Conn::
Mux::Registrar::Srv::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

Oscl::Protocol::IP::
TCP::Conn::Mux::Registrar::Api&		Part::getSyncApi() noexcept{
	return _sync;
	}

void	Part::done(RstTrans& trans) noexcept{
	trans.~RstTrans();
	free((TransMem*)&trans);
	}

void	Part::done(RejectTrans& trans) noexcept{
	trans.~RejectTrans();
	free((TransMem*)&trans);
	}

void	Part::free(TransMem* trans) noexcept{
	--_nPendingTransactions;
	_freeTransMem.put(trans);
	if(!_open){
		if(!_nPendingTransactions){
			if(_closeReq){
				_closeReq->returnToSender();
				_closeReq	= 0;
				}
			}
		}
	else{
		servicePendingTransactions();
		}
	}

TransMem*	Part::allocTransMem() noexcept{
	TransMem*	trans	= _freeTransMem.get();
	if(trans){
		++_nPendingTransactions;
		}
	return trans;
	}

void	Part::servicePendingTransactions() noexcept{
	if(!_open) return;
	if(_rejectList.first()){
		TransMem*	mem	= allocTransMem();
		if(!mem) return;
		for(;;);	// FIXME: send RST
		}
	}

void	Part::receive(	Oscl::Pdu::Pdu&				pdu,
						unsigned long				peerIpAddress,
						unsigned long				localAddress,
						unsigned					peerPort,
						unsigned					localPort,
						unsigned					ipPayloadLength
						) noexcept{
	// When we get here, the TCP payload checksum has already
	// been verified, and the PDU length is at least as large
	// as ipPayloadLength.
	Oscl::Protocol::IP::TCP::Header	tcpHeader;
	unsigned len	= pdu.read(	&tcpHeader,
								Oscl::Protocol::IP::TCP::Header::minHeaderSize,
								0
								);

	if(len < Oscl::Protocol::IP::TCP::Header::minHeaderSize){
		// Discard
		return;
		}
	if(!tcpHeader.headerLengthIsValid()){
		// Discard
		return;
		}

//	const unsigned	tcpHeaderLength		= tcpHeader.getHeaderLength();
//	const unsigned	tcpDataOffset		= tcpHeader.getDataOffset();
	const unsigned	tcpOptionsLength	= tcpHeader.getOptionsLength();
	if(tcpOptionsLength){
		unsigned len	= pdu.read(	tcpHeader.options,
									tcpOptionsLength,
									Oscl::Protocol::IP::
									TCP::Header::minHeaderSize
									);
		if(len < tcpOptionsLength){
			// Discard
			return;
			}
		}
	// At this point, the TCP header has been validated,
	// and read into the tcpHeader.

	Oscl::Protocol::IP::TCP::Conn::Mux::Registrar::Acceptor*	next;
	for(	next	= _acceptorList.first();
			next;
			next	= _acceptorList.next(next)
			){
		// Note that (unlike UDP) the PDU is NOT stripped of the TCP
		// header at this point. The PDU has, however, been validated.
		// FIXME: Should the TCP Header have already been read
		// and stripped, and thus we simply pass a reference to the
		// header so that individual connections don't have to do it?
		if(next->accept(	pdu,
							tcpHeader,
							peerIpAddress,
							localAddress,
							peerPort,
							localPort,
							ipPayloadLength
							)){
			return;
			}
		}
	if(_listenerList.first()){
		// LISTEN "state"
		if(tcpHeader.isRST()){
			// Ignore
			return;
			}
		else if(tcpHeader.isACK()){
			// Fall through to send RST
			}
		else if(tcpHeader.isSYN()){
			Oscl::Protocol::IP::
			TCP::Conn::Mux::Registrar::
			Srv::Req::Api::ListenReq*	req	= _listenerList.get();
			req->_payload._path.path.peerIpAddress	= peerIpAddress;
			req->_payload._path.path.localIpAddress	= localAddress;
			req->_payload._path.path.peerPortNum	= peerPort;
			req->_payload._path.path.localPortNum	= localPort;
			req->_payload._path.ipPayloadLength		= ipPayloadLength;
			req->_payload._path.tcpPayload			= &pdu;
			req->_payload._closed					= false;
			req->returnToSender();
			return;
			}
		else{
			// Drop it.
			return;
			}
		}
	TransMem*	mem	= allocTransMem();
	if(!mem) return;
	new(mem) RstTrans(	*this,
						_myPapi,
						_ipFwdSAP,
						peerIpAddress,
						localAddress,
						pdu,
						ipPayloadLength
						);
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	// FIXME: Need to wait until _nPendingTransactions is zero.
	for(;;);
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::
						Srv::Req::Api::ListenReq&	msg
						) noexcept{
	// FIXME: not implemented yet
	_listenerList.put(&msg);
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::
						Srv::Req::Api::RejectReq&	msg
						) noexcept{
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_rejectList.put(&msg);
		return;
		}
	// Can't use RstTrans for reject:
	// However, this reminds me that the ListenReq must
	// contain the TCP header information, not only for
	// Reject, but also to initialize the connection with
	// various options sent in the SYN.
	new(mem) RejectTrans(	*this,
							_myPapi,
							_ipFwdSAP,
							msg
							);
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::
						Srv::Req::Api::AttachReq&	msg
						) noexcept{
	_acceptorList.put(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::
						Srv::Req::Api::DetachReq&	msg
						) noexcept{
	_acceptorList.remove(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

