/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_mux_srv_registrar_tcp_parth_
#define _oscl_protocol_ip_tcp_conn_mux_srv_registrar_tcp_parth_
#include "oscl/protocol/ip/port/mux/client/receiver.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/srv/sync.h"
#include "oscl/queue/queue.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/srv/reqapi.h"
#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Mux {
/** */
namespace Srv {
/** */
namespace Registrar {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(RstTrans) >		rst;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(RejectTrans) >	reject;
	};

/** */
class Part :	public	Oscl::Protocol::IP::Port::Mux::Client::Receiver,
				private	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::Srv::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				public	Oscl::Mt::Itc::Srv::CloseSync
				{
	private:
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar::Acceptor
						>										_acceptorList;
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar:: 
						Srv::Req::Api::ListenReq
						>										_listenerList;
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						TCP::Conn::Mux::Registrar:: 
						Srv::Req::Api::RejectReq
						>										_rejectList;
		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::
		Srv::Req::Api::ConcreteSAP						_sap;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::Srv::Sync			_sync;

		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;

		/** */
		Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&		_ipFwdSAP;

		/** */
		Oscl::Queue<TransMem>							_freeTransMem;

		/** */
		unsigned										_nPendingTransactions;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

		/** */
		bool											_open;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Protocol::IP::TX::Srv::Req::Api::SAP&	ipFwdSAP,
				TransMem									transMem[],
				unsigned									nTransMem
				) noexcept;
		/** */
		virtual ~Part(){}

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::
		Srv::Req::Api::SAP&				getSAP() noexcept;

		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::Api&	getSyncApi() noexcept;

	private:
		/** */
		friend class RstTrans;
		/** */
		friend class RejectTrans;

		/** */
		void	done(RstTrans& trans) noexcept;

		/** */
		void	done(RejectTrans& trans) noexcept;

	private:
		/** */
		TransMem*	allocTransMem() noexcept;
		/** */
		void	free(TransMem* trans) noexcept;
		/** */
		void	servicePendingTransactions() noexcept;
	
	private:	// Oscl::Protocol::IP::Port::Mux::Client::Receiver
		/** */
		void	receive(	Oscl::Pdu::Pdu&				pdu,
							unsigned long				remoteAddress,
							unsigned long				localAddress,
							unsigned					remotePort,
							unsigned					localPort,
							unsigned					ipPayloadLength
							) noexcept;

	public:	// Oscl::Mt::Itc::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	public:	// Oscl::Protocol::IP::TCP::Conn::Mux::Registrar::Srv::Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Registrar::
							Srv::Req::Api::ListenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Registrar::
							Srv::Req::Api::RejectReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Registrar::
							Srv::Req::Api::AttachReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							TCP::Conn::Mux::Registrar::
							Srv::Req::Api::DetachReq&	msg
							) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
