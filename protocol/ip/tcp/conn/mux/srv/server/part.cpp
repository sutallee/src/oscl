/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Mux::Srv::Server;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			unsigned					localPort
			) noexcept:
		_sap(*this,myPapi),
		_localPort(localPort)
		{
	}

Oscl::Protocol::IP::
TCP::Conn::Mux::Server::Srv::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

bool	Part::accept(	Oscl::Pdu::Pdu&	pdu,
						unsigned long	sourceAddress,
						unsigned long	destinationAddress,
						unsigned		sourcePort,
						unsigned		destinationPort,
						unsigned		ipPayloadLength
						) noexcept{
	if(destinationPort != _localPort){
		return false;
		}

	Oscl::Protocol::IP::TCP::Conn::Mux::Server::Srv::Req::Api::RecvReq*
	next	= _pendingRecvReqs.get();

	if(!next) return true;

	next->_payload._payload				= &pdu;
	next->_payload._remoteAddress		= sourceAddress;
	next->_payload._localAddress		= destinationAddress;
	next->_payload._remotePort			= sourcePort;
	next->_payload._localPort			= destinationPort;
	next->_payload._ipPayloadLength		= ipPayloadLength;

	next->returnToSender();

	return true;
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Server::
						Srv::Req::Api::RecvReq&	msg
						) noexcept{
	_pendingRecvReqs.put(&msg);
	}

void	Part::request(	Oscl::Protocol::IP::
						TCP::Conn::Mux::Server::
						Srv::Req::Api::CancelReq&	msg
						) noexcept{
	Oscl::Protocol::IP::
	TCP::Conn::Mux::Server::Srv::Req::Api::RecvReq*	next;

	while((next=_pendingRecvReqs.get())){
		next->returnToSender();
		}

	msg.returnToSender();
	}

