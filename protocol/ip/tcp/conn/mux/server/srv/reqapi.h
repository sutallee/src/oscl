/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_mux_server_srv_reqapih_
#define _oscl_protocol_ip_tcp_conn_mux_server_srv_reqapih_
#include "oscl/protocol/ip/address.h"
#include "oscl/handle/handle.h"
#include "oscl/pdu/pdu.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Mux {
/** */
namespace Server {
/** */
namespace Srv {
/** */
namespace Req {

/** */
class RecvPayload {
	public:
		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_payload;
		/** */
		unsigned long					_remoteAddress;
		/** */
		unsigned long					_localAddress;
		/** */
		unsigned						_remotePort;
		/** */
		unsigned						_localPort;
		/** */
		unsigned						_ipPayloadLength;
	};

/** */
class CancelPayload {};

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>						SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>				ConcreteSAP;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,RecvPayload>	RecvReq;
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Api,CancelPayload>	CancelReq;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	request(RecvReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}
}
}

#endif
