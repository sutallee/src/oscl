/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_mux_registrar_apih_
#define _oscl_protocol_ip_tcp_conn_mux_registrar_apih_
#include "acceptor.h"
#include "path.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Mux {
/** */
namespace Registrar {

/**	*/
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** Returns null/zero if the socket is closed.
			Otherwise, fills out "path" structure and 
			returns a pointer to it. Typically, the
			path is subsequently to either create a
			connection or reject one. "Path" is an output
			only and is initially empty/uninitialized.
		 */
		virtual	Path*	listen(Path& path) noexcept=0;

		/** This operation is used after the listen() caller
			determines it does NOT want to accept the incomming
			connection request.
		 */
		virtual	void	reject(const Path& path) noexcept=0;

		/** This operation is used to attach a connection
			to the socket. The connection is a subclass
			of Acceptor.
		 */
		virtual void	attach(Acceptor& acceptor) noexcept=0;

		/** This operation is used to detach a connection
			from the socket. The connection is a subclass
			of Acceptor.
		 */
		virtual void	detach(Acceptor& acceptor) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
