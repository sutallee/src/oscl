/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_mux_registrar_acceptorh_
#define _oscl_protocol_ip_tcp_conn_mux_registrar_acceptorh_
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/queue/queueitem.h"
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Mux {
/** */
namespace Registrar {

/** */
class Acceptor :	public Oscl::QueueItem {
	public:
		/** Shut-up GCC */
		virtual ~Acceptor() {}

		/** The implementation returns true to keep
			the "segment" and false otherwise. The implementation
			must return true if it would accept the "segment" but
			does not currently have the resources. If the "segment"
			is accepted the implementation must reference the "segment"
			through a Handle<Oscl::Pdu::Pdu>.
			The "segment" includes the complete TCP header.
			The "tcpHeader" contains the copied contents of the
			TCP header. Any portion of the "tcpHeader" that the
			implementation wishes to keep beyond the lifetime
			of this operation MUST be copied.
			The "ipPayloadLength" is the length of the entire segment
			including the TCP header.
		 */
		virtual bool	accept(	Oscl::Pdu::Pdu&					segment,
								const Oscl::Protocol::IP::
								TCP::Header&					tcpHeader,
								unsigned long					peerIpAddress,
								unsigned long					localIpAddress,
								unsigned						peerPort,
								unsigned						localPort,
								unsigned						ipPayloadLength
								) noexcept=0;
	};

}
}
}
}
}
}
}

#endif

