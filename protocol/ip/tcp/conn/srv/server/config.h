/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_configh_
#define _oscl_protocol_ip_tcp_conn_srv_server_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

template <	unsigned	nTransMem,
			unsigned	nTxSegmentMem,
			unsigned	nTxSegFragMem,
			unsigned	nTextTransMem,
			unsigned	nTxBufferMem,
			unsigned	txBufferSize
			>
class Config {
	private:
		/** */
		TransMem					transMem[nTransMem];
		/** */
		SegTransMem					txSegmentMem[nTxSegmentMem];
		/** */
		SegFragTransMem				txSegFragMem[nTxSegFragMem];
		/** */
		TextTransMem				textTransMem[nTextTransMem];
		/** */
		Oscl::Memory::
		AlignedBlock<txBufferSize>	txBufferMem[nTxBufferMem];

	public:
		/** */
		Part						_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Protocol::IP::FWD::Api&		ipFwdApi,
				Oscl::Protocol::IP::
				Socket::TCP::Api&					socketApi,
				unsigned							maxTextTxMTU
				) noexcept:
				_part(	myPapi,
						ipFwdSAP,
						ipFwdApi,
						socketApi,
						maxTextTxMTU,
						transMem,
						nTransMem,
						txSegmentMem,
						nTxSegmentMem,
						txSegFragMem,
						nTxSegFragMem,
						textTransMem,
						nTextTransMem,
						txBufferMem,
						sizeof(txBufferMem[0])
						)
				{
			}
	};

}
}
}
}
}
}
}

#endif
