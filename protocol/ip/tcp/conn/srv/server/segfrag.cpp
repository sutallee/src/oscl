/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "segfrag.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Srv::Server;

SegFragment::SegFragment(	Part&					context,
							const void*				buffer,
							Oscl::Modulo::U32		sequenceNumber,
							unsigned				textOffset,
							unsigned				textLength,
							Oscl::Stream::Output::
							Req::Api::WriteReq*		msg
							) noexcept:
		_context(context),
		_msg(msg),
		_sequenceNumber(sequenceNumber),
		_nUnacknowledgedBytes(textLength),
		_buffer(buffer),
		_offset(textOffset),
		_length(textLength)
		{
	}

unsigned	SegFragment::ack(Oscl::Modulo::U32 acknowledgeNumber) noexcept{
	unsigned		nAcknowledgedBytes	= _length-_nUnacknowledgedBytes;
	Oscl::Modulo::U32	unAckSeqNumber	= _sequenceNumber + nAcknowledgedBytes;

	if(acknowledgeNumber <= unAckSeqNumber){
		return 0;
		
		}
	unsigned long delta	= acknowledgeNumber-unAckSeqNumber;

#if 0
	unsigned long	unAckSeqNumber		= _sequenceNumber+nAcknowledgedBytes;
	unsigned long	delta;
	if(acknowledgeNumber > unAckSeqNumber){
		// |0123456789012345678901234567890123|
		// |-----|-----------------------|----|
		//      seq                     ack
		delta	= acknowledgeNumber-unAckSeqNumber;
		if(delta > 0x80000000){
			return 0;
			}
		}
	else {
		// |0123456789012345678901234567890123|
		// |-----|-----------------------|----|
		//      ack                     seq
		delta	= unAckSeqNumber-acknowledgeNumber;
		if(delta < 0x80000000){
			// Invalid range
			return 0;
			}
		delta	=	0-delta;
		}
#endif
	if(delta > _nUnacknowledgedBytes){
		unsigned nAcked	= _nUnacknowledgedBytes;
		_nUnacknowledgedBytes	= 0;
		return nAcked;
		}
	else {
		unsigned nAcked	= delta;
		_nUnacknowledgedBytes	-= nAcked;
		return nAcked;
		}
	}

bool		SegFragment::fullyAcknowledged() const noexcept{
	return !_nUnacknowledgedBytes;
	}

void		SegFragment::returnReadRequestNormal() noexcept{
	if(_msg){
		_msg->_payload._nWritten	= _msg->_payload._buffer.length();
		_msg->returnToSender();
		}
	_context.done(*this);
	}

void		SegFragment::returnReadRequestClosing() noexcept{
	if(_msg){
		_msg->_payload._nWritten	= 0;
		_msg->returnToSender();
		}
	_context.done(*this);
	}

