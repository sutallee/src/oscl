/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_rxh_
#define _oscl_protocol_ip_tcp_conn_srv_server_rxh_
#include "oscl/protocol/ip/tcp/conn/mux/registrar/api.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/path.h"
#include "oscl/protocol/ip/tcp/conn/fsm/var.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/stream/output/itc/sync.h"
#include "oscl/stream/io/api.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class RxContext {
	public:
		/** Shut-up GCC. */
		virtual ~RxContext() {}

		/** */
		virtual void	rxRequest(	Oscl::Stream::Input::
									Req::Api::ReadReq&		msg
									) noexcept=0;
	};

/** */
class Rx :	private	Oscl::Stream::Input::Req::Api {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::Stream::Input::Sync			_inputSync;
		/** */
		RxContext&							_context;

	public:
		/** */
		Rx(		Oscl::Mt::Itc::PostMsgApi&	myPapi,
				RxContext&					context
				) noexcept;

		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getInputSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&				getSyncApi() noexcept;

	private: // Oscl::Stream::Input::Req::Api
		/** */
		void	request(Oscl::Stream::Input::Req::Api::ReadReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Input::Req::Api::CancelReq& msg) noexcept;
	};

}
}
}
}
}
}
}

#endif
