/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "segment.h"
#include "oscl/checksum/onescomp/b16/calculator.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/pdu/rocomposite.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Srv::Server;

Segment::Segment(	void*						buffer,
					unsigned					bufferSize,
					unsigned long				peerIpAddress,
					unsigned long				localIpAddress,
					unsigned					peerPort,
					unsigned					localPort,
					Oscl::Modulo::U32			sequenceNumber,
					unsigned					urgentPointer,
					bool						urgFlag,
					bool						ackFlag,
					bool						pshFlag,
					bool						rstFlag,
					bool						synFlag,
					bool						finFlag
					) noexcept:
		_buffer((unsigned char*)buffer),
		_bufferSize(bufferSize),
		_peerIpAddress(peerIpAddress),
		_localIpAddress(localIpAddress),
		_peerPort(peerPort),
		_localPort(localPort),
		_sequenceNumber(sequenceNumber),
		_finSeqNumber(sequenceNumber),
		_urgentPointer(urgentPointer),
		_flags(		(urgFlag<<5)
				|	(ackFlag<<4)
				|	(pshFlag<<3)
				|	(rstFlag<<2)
				|	(synFlag<<1)
				|	(finFlag<<0)
				|	(0x5000)		// data offset
				),
		_textLength(0),
		_nUnacknowledgedBytes(_textLength),
		_synAcked(!synFlag),
		_finAcked(!finFlag)
		{
	}

void	Segment::send(	Oscl::Protocol::IP::FWD::Api&	ipFwdApi,
						Oscl::Modulo::U32				acknowledgeNumber,
						unsigned						window
						) noexcept{
	// Issues:
	// - The Stream Output buffer is not cache aligned.
	// - A buffer for the TCP/IP header built on the stack
	//   may not be cache aligned.
	//   * Segment needs to have its own buffer resources.
	//   * Maximum segment size have an impact on the amount
	//		of memory consumed by a single socket/connection.
	//	 * The situation where the stream output request is
	//		larger than the maximum segment size arises.
	//	 * Data *must* be copied from the stream output buffers
	//		into the segment buffers (yuk).
	//	 * Copying would be done during construction, NOT
	//		during this send() operation.
	//   * Having the copied into a contiguous buffer simplifies
	//     building the TX PDU.
	//   * Acknowledged portions of the buffer need not be
	//     retransmitted.
	// - Do we need to combine many small stream output
	//   requests into a single segment?
	//   * Segment might contain a Queue of stream output
	//     requests.
	// - Transmission would still be synchronous.

	Oscl::Protocol::IP::Header*
	ipHeader	= (Oscl::Protocol::IP::Header*)&_headerMem;

	Oscl::Protocol::IP::TCP::Header*
	tcpHeader	= (Oscl::Protocol::IP::TCP::Header*)ipHeader->options;

	const unsigned	tcpHeaderLength	=	Oscl::Protocol::IP::
										TCP::Header::minHeaderSize;

	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;

	const unsigned	totalLength		=		ipHeaderLength
										+	tcpHeaderLength
										+	_textLength
										;

	const unsigned	tcpLength		=		tcpHeaderLength
										+	_textLength
										;

	ipHeader->source.assign(_localIpAddress);
	ipHeader->destination.assign(_peerIpAddress);
	ipHeader->length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader->frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader->type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_TCP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader->updateChecksum(0);

	Oscl::Checksum::OnesComp::B16::Calculator	csum;

	// Pseudo header
	csum.accumulate(&ipHeader->source,sizeof(ipHeader->source));
	csum.accumulate(&ipHeader->destination,sizeof(ipHeader->destination));
	Oscl::Endian::Big::U16	uLength;
	uLength	= tcpLength;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_TCP;
	csum.accumulate(&protocol,sizeof(protocol));
	csum.accumulate(&uLength,sizeof(uLength));

	// TCP header
	tcpHeader->sourcePort			= _localPort;
	tcpHeader->destinationPort		= _peerPort;
	tcpHeader->dataOffsetAndFlags	= _flags;
	tcpHeader->sequenceNumber		= _sequenceNumber.getValue();
	tcpHeader->acknowlegementNumber	= acknowledgeNumber.getValue();
	tcpHeader->window				= window;
	tcpHeader->checksum				= 0;
	tcpHeader->urgentPointer		= 0;

	csum.accumulate(tcpHeader,tcpHeaderLength);
	csum.accumulate(_buffer,_textLength);

	uint16_t	checksum	= csum.final();
	if(!checksum){
		checksum	= ~checksum;
		}

	// Finish the TCP header
	tcpHeader->checksum			= checksum;

	const unsigned	headerLength	= ipHeaderLength+tcpHeaderLength;

	Oscl::Pdu::Fixed				headerPDU(	*this,
												*this,
												ipHeader,
												headerLength,
												headerLength
												);

	Oscl::Pdu::Fixed				textPDU(	*this,
												*this,
												_buffer,
												_textLength,
												_textLength
												);

	Oscl::Pdu::ReadOnlyFragment		headerFragment(	*this,
													&headerPDU
													);
	Oscl::Pdu::ReadOnlyFragment		textFragment(	*this,
													&textPDU
													);
	Oscl::Pdu::ReadOnlyComposite	composite(*this);

	composite.append(&headerFragment);
	composite.append(&textFragment);
	Oscl::Protocol::IP::AddressInit	destAddr(_peerIpAddress);
	ipFwdApi.forward(composite,destAddr);
	}

unsigned	Segment::ack(Oscl::Modulo::U32 acknowledgeNumber) noexcept{
	Oscl::Queue<SegFragment>	frags	= _fragmentQueue;
	unsigned					totalAcked	= 0;
	SegFragment*				frag;
	if(!_synAcked){
		if(acknowledgeNumber >= _sequenceNumber){
			_synAcked	= true;
			totalAcked	+= 1;
			}
		else {
			return 0;
			}
#if 0
		if(acknowledgeNumber > synSeqNumber){
			unsigned long	delta	= acknowledgeNumber-synSeqNumber;
			if(delta <= 0x80000000){
				_synAcked	= true;
				totalAcked	+= 1;
				}
			return 0;
			}
		else {
			unsigned long	delta	= synSeqNumber-acknowledgeNumber;
			if(delta >= 0x80000000){
				_synAcked	= true;
				totalAcked	+= 1;
				}
			return 0;
			}
#endif
		}
	unsigned long	textAcked	= 0;
	while((frag = frags.get())){
		unsigned	acked	= frag->ack(acknowledgeNumber);
		if(frag->fullyAcknowledged()){
			frag->returnReadRequestNormal();
			}
		else {
			_fragmentQueue.put(frag);
			}
		if(!acked){
			break;
			}
		textAcked	+= acked;
		}
	while((frag=frags.get())){
		_fragmentQueue.put(frag);
		}
	_nUnacknowledgedBytes	-= textAcked;
	totalAcked				+= textAcked;
	if(!_fragmentQueue.first()){	// Only if all fragments are ACKed
		// FIN processing
		if(!_finAcked){
			if(acknowledgeNumber >= _finSeqNumber){
				_finAcked	= true;
				totalAcked	+= 1;
				}
			else {
				return 0;
				}
#if 0
			if(acknowledgeNumber > _finSeqNumber){
				unsigned long	delta	= acknowledgeNumber-_finSeqNumber;
				if(delta <= 0x80000000){
					_finAcked	= true;
					totalAcked	+= 1;
					}
				}
			else {
				unsigned long	delta	= _finSeqNumber-acknowledgeNumber;
				if(delta >= 0x80000000){
					_finAcked	= true;
					totalAcked	+= 1;
					}
				}
#endif
			}
		}
	return totalAcked;
	}


bool		Segment::fullyAcknowledged() const noexcept{
	return (!_nUnacknowledgedBytes && _finAcked && _synAcked);
	}

bool		Segment::isSYN() noexcept{
	return (_flags & (1<<1));
	}

bool		Segment::isFIN() noexcept{
	return (_flags & (1<<0));
	}


void		Segment::returnReadRequestNormal() noexcept{
	SegFragment*				frag;
	while((frag = _fragmentQueue.get())){	// This should be empty already
		frag->returnReadRequestNormal();
		}
	}

void		Segment::returnReadRequestClosing() noexcept{
	SegFragment*				frag;
	while((frag = _fragmentQueue.get())){
		frag->returnReadRequestClosing();
		}
	}

void		Segment::append(SegFragment& fragment) noexcept{
	// Assume that remaining() was checked by client
	// before fragment was appended.
	const unsigned	fragmentLength	= fragment._length;
	if(fragmentLength > remaining()){
		for(;;);	// FIXME: protocol error
		}
	_fragmentQueue.put(&fragment);
	memcpy(&_buffer[_textLength],fragment._buffer,fragmentLength);
	_textLength				+= fragmentLength;
	_nUnacknowledgedBytes	+= fragmentLength;
	_finSeqNumber			+= fragmentLength;
	}

unsigned	Segment::remaining() const noexcept{
	return _bufferSize-_textLength;
	}

void	Segment::free(void* mem) noexcept{
	}

unsigned long	Segment::firstFragmentSequenceNumber() noexcept{
	return _sequenceNumber.getValue()+isSYN();
	}

unsigned long	Segment::nextSequenceNumber() noexcept{
	return _finSeqNumber.getValue()+1;
	}

