/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_parth_
#define _oscl_protocol_ip_tcp_conn_srv_server_parth_
#include "segment.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/api.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/tcp/conn/mux/registrar/path.h"
#include "oscl/protocol/ip/tcp/conn/fsm/var.h"
#include "oscl/protocol/ip/tcp/conn/srv/api.h"
#include "oscl/protocol/ip/tcp/conn/srv/itc/sync.h"
#include "oscl/protocol/ip/fwd/api.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/stream/output/itc/sync.h"
#include "oscl/stream/io/api.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/protocol/ip/socket/tcp/api.h"
#include "oscl/modulo/u32.h"
#include "trans.h"
#include "text.h"
#include "rx.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** */
union TransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(RstTrans) >		rst;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SynAckTrans) >	synAck;
	};

union TextTransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Text) >	text;
	};

union SegTransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Segment) >	segment;
	};

union SegFragTransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SegFragment) >	fragment;
	};

union SegBufferMem {
	/** */
	void*	__qitemlink;
	};

/** */
class Part;

/** */
class UrgReceiver : public RxContext {
	private:
		/** */
		Part&		_context;

	public:
		/** */
		UrgReceiver(Part& context) noexcept;

	private: // RxContext
		/** */
		void	rxRequest(	Oscl::Stream::Input::
							Req::Api::ReadReq&		msg
							) noexcept;
	};

/** */
class NormalReceiver : public RxContext {
	private:
		/** */
		Part&		_context;

	public:
		/** */
		NormalReceiver(Part& context) noexcept;

	private: // RxContext
		/** */
		void	rxRequest(	Oscl::Stream::Input::
							Req::Api::ReadReq&		msg
							) noexcept;
	};

/** */
class Ticker : public Oscl::Protocol::IP::Socket::TCP::TickObserver {
	private:
		/** */
		Part&		_context;

	public:
		/** */
		Ticker(Part& context) noexcept;

	private:	// Oscl::Protocol::IP::Socket::TCP::TickObserver
		void	tick() noexcept;
	};

/** */
class Part :	public	Oscl::Protocol::IP::TCP::Conn::Mux::Registrar::Acceptor,
				private	Oscl::Stream::Output::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				public	Oscl::Mt::Itc::Srv::CloseSync,
				public	Oscl::Stream::IO::Api,
				private Oscl::Protocol::IP::TCP::Conn::FSM::Context,
				private Oscl::FreeStore::Mgr,
				public Oscl::Protocol::IP::TCP::Conn::Srv::Api,
				public Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Req::Api
				{
	private:
		/** */
		TransMem								_syncTransMem;
		/** */
		Ticker									_ticker;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Protocol::IP::
		TX::Srv::Req::Api::SAP&					_ipFwdSAP;
		/** */
		Oscl::Protocol::IP::FWD::Api&			_ipFwdApi;
		/** */
		Oscl::Protocol::IP::
		Socket::TCP::Api&						_socketApi;
		/** */
		Oscl::Protocol::IP::
		TCP::Conn::Mux::Registrar::Path			_path;
		/** */
		UrgReceiver								_urgRx;
		/** */
		Rx										_urgInput;
		/** */
		NormalReceiver							_normalRx;
		/** */
		Rx										_normalInput;
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::ITC::Sync					_connSync;
		/** */
		Oscl::Stream::Output::Sync				_outputSync;
		/** */
		// FIXME: FSM goes here somewhere
		Oscl::Protocol::IP::TCP::Conn::FSM::Var	_state;
		/** */
		Oscl::Queue<TransMem>					_freeTransMem;
		/** */
		Oscl::Queue<Segment>					_retransmissionQueue;
		/** */
		Oscl::Queue<TextTransMem>				_freeTextTransMem;
		/** */
		Oscl::Queue<SegTransMem>				_freeSegTransMem;
		/** */
		Oscl::Queue<SegFragTransMem>			_freeSegFragTransMem;
		/** */
		Oscl::Queue<SegBufferMem>				_freeSegBufferMem;
		/** */
		const unsigned							_maxTextTxMTU;
		/** */
		Oscl::Queue<	Oscl::Stream::
						Input::Req::
						Api::ReadReq
						>						_pendingReads;
		/** */
		Oscl::Queue<	Oscl::Stream::
						Input::Req::
						Api::ReadReq
						>						_pendingUrgReads;
		/** */
		Oscl::Queue<	Oscl::Stream::
						Output::Req::
						Api::WriteReq
						>						_pendingWrites;
		/** */
		Oscl::Queue<	Oscl::Stream::
						Output::Req::
						Api::FlushReq
						>						_pendingFlushes;
		/** */
		Oscl::Queue<	Oscl::Mt::Itc::Srv::
						Close::Req::Api::
						CloseReq
						>						_pendingCloses;
		/** */
		Oscl::Queue< Text >						_pendingText;
		/** */
		unsigned								_nPendingTransactions;
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::ITC::Req::Api::ListenReq*	_openReq;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;
		/** */
		Oscl::Stream::Input::
		Req::Api::ReadReq*						_readReq;
		/** */
		Oscl::Stream::Input::
		Req::Api::ReadReq*						_urgReadReq;
		/** */
		Oscl::Stream::Output::
		Req::Api::WriteReq*						_writeReq;
		/** */
		Oscl::Stream::Output::
		Req::Api::FlushReq*						_pushReq;
		/** */
		bool									_open;
		/** */
		bool									_passive;
		/** */
		struct {
			/** */
			Oscl::Pdu::Pdu*							segment;
			/** */
			const Oscl::Protocol::IP::TCP::Header*	tcpHeader;
			/** */
			unsigned long							peerIpAddress;
			/** */
			unsigned long							localIpAddress;
			/** */
			unsigned								peerPort;
			/** */
			unsigned								localPort;
			/** */
			unsigned								ipPayloadLength;
			/** */
			Oscl::Stream::Input::
			Req::Api::ReadReq*						urgReadReq;
			/** */
			Oscl::Stream::Input::
			Req::Api::ReadReq*						normalReadReq;
			/** */
			Oscl::Stream::Output::
			Req::Api::WriteReq*						writeReq;
			/** */
			const unsigned char*					writeBuffer;
			/** */
			unsigned								writeLength;
			/** */
			unsigned								writeOffset;
			/** */
			unsigned long							sequence;
			/** */
			unsigned long							remaining;
			/** */
			unsigned long							acknowledge;
			/** */
			unsigned long							nUrgent;
			} _current;

		/** Send Unacknowledged */
		Oscl::Modulo::U32						_sndUna;
		/** Send Next */
		Oscl::Modulo::U32						_sndNxt;
		/** Send Window */
		unsigned long							_sndWnd;
		/** Send Urgent Pointer */
		unsigned long							_sndUp;
		/** Segement sequence number used for last window update */
		Oscl::Modulo::U32						_sndWl1;
		/** Segment acknowledgement number used for last window update */
		Oscl::Modulo::U32						_sndWl2;
		/** Initial Send Sequence number */
		Oscl::Modulo::U32						_iss;
		/** Receive Next */
		Oscl::Modulo::U32						_rcvNxt;
		/** Receive Window */
		unsigned long							_rcvWnd;
		/** Receive Urgent Pointer */
		unsigned long							_rcvUp;
		/** Initial Receive Sequence Number */
		Oscl::Modulo::U32						_irs;
		/** */
		Oscl::Modulo::U32					_lastSegmentSentTimeInMicroseconds;

		// Timestamp variables

		/** */
		Oscl::Modulo::U32						_retransmissionTimer;

		/** */
		bool									_userTimerEnabled;
		/** */
		Oscl::Modulo::U32						_userTimer;

		/** Timestamp to echo whenever a segment is sent.
			IF SEG.SEQ <= Last.ACK.sent < SEG.SEQ + SEG.LEN
			THEN copy SEG.TSval to TS.RECENT (_tsRecent).
			Otherwise, ignore SEG.TSval.
		 */
		Oscl::Modulo::U32						_tsRecent;

		/** Sequence number of the last ACK sent.
			This will be the same as RCV.NXT (_rcvNxt)
			except when ACKs have been delayed.
		 */
		Oscl::Modulo::U32						_lastAckSent;

		/** */
		bool									_urgentMode;
		/** */
		bool									_piggyBack;
		/** */
		bool									_finPending;
		/** */
		bool									_finAcknowledged;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Protocol::IP::FWD::Api&		ipFwdApi,
				Oscl::Protocol::IP::
				Socket::TCP::Api&					socketApi,
				unsigned							maxTextTxMTU,
				TransMem							transMem[],
				unsigned							nTransMem,
				SegTransMem							txSegmentMem[],
				unsigned							nTxSegmentMem,
				SegFragTransMem						txSegFragMem[],
				unsigned							nTxSegFragMem,
				TextTransMem						textTransMem[],
				unsigned							nTextTransMem,
				void*								txBufferMem,
				unsigned							txBufferMemSize
				) noexcept;

		/** This operation is intended for simple client threads that
			wish to block until the connection is open. Clients that
			use this operation should not use the start() operation.
		 */
		void	open() noexcept;

		/** This operation is intended for simple client threads that
			wish to block until the connection is closed. Clients that
			use this operation should not use the stop() operation.
		 */
		void	close() noexcept;

		/** This operation is intended for asynchronous clients that
			do NOT block until the connection is open. Such clients
			should invoke this operation before issuing the asynchronous
			Open request. Clients that use this operation should NOT
			use the open() operation.
		 */
		void	start() noexcept;

		/** This operation is intended for asynchronous clients that
			do NOT block until the connection is closed. Such clients
			should invoke this operation after the asynchronous
			Close request completes. Clients that use this operation
			should NOT use the close() operation.
		 */
		void	stop() noexcept;

		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getUrgInputSAP() noexcept;

		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getNormalInputSAP() noexcept;

		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getOutputSAP() noexcept;
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::ITC::Req::Api::SAP&			getConnSAP() noexcept;
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Api&								getConnApi() noexcept;


	private:
		/** */
		friend class RstTrans;
		/** */
		void	done(RstTrans& trans) noexcept;
		/** */
		friend class SynAckTrans;
		/** */
		void	done(SynAckTrans& trans) noexcept;
		/** */
		friend class SegFragment;
		/** */
		void	done(SegFragment& trans) noexcept;
		/** */
		friend class Segment;
		/** */
		void	done(Segment& trans) noexcept;

		/** */
		void	free(TransMem* trans) noexcept;

		/** */
		void	free(Text& text) noexcept;

		/** */
		TransMem*	allocTransMem() noexcept;

		/** */
		void		scheduleTransaction() noexcept;

		/** */
		friend class UrgReceiver;
		/** */
		void	urgRxRequest(	Oscl::Stream::Input::
								Req::Api::ReadReq&		msg
								) noexcept;

		/** */
		friend class NormalReceiver;
		/** */
		void	normalRxRequest(	Oscl::Stream::Input::
									Req::Api::ReadReq&		msg
									) noexcept;
		/** */
		friend class Ticker;
		/** */
		void	tick() noexcept;

		/** */
		void	restartRetransmissionTimer() noexcept;

		/** */
		void		queueText(Text& text) noexcept;
		/** */
		unsigned	deliverCurrentTextSegment() noexcept;
		/** */
		unsigned	deliverQueuedTextSegments() noexcept;

		/** */
		void	sendSimple(	unsigned long	seq,
							unsigned long	ack,
							unsigned		flags,
							unsigned		window
							) noexcept;

	private: // Oscl::Protocol::IP::TCP::Conn::Mux::Registrar::Acceptor
		/** */
		bool	accept(	Oscl::Pdu::Pdu&					segment,
						const Oscl::Protocol::IP::
						TCP::Header&					tcpHeader,
						unsigned long					peerIpAddress,
						unsigned long					localIpAddress,
						unsigned						peerPort,
						unsigned						localPort,
						unsigned						ipPayloadLength
						) noexcept;

		/** */
		Oscl::Stream::Input::Api&	urgInput() noexcept;

	public:	// Oscl::Stream::IO::Api
		/** */
		Oscl::Stream::Input::Api&	input() noexcept;
		/** */
		Oscl::Stream::Output::Api&	output() noexcept;
	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	public:	// Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Req::Api,
		/** */
		void	request(	Oscl::Protocol::IP::TCP::
							Conn::Srv::ITC::Req::Api::ListenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::TCP::
							Conn::Srv::ITC::Req::Api::CancelReq&	msg
							) noexcept;

	private: // Oscl::Stream::Output::Req::Api
		/** */
		void	request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept;

	private: // Oscl::Protocol::IP::TCP::Conn::FSM::Context
		/** */
		void	sendRstAck(	unsigned long	seq,
							unsigned long	ack
							) noexcept;
		void	sendSyn(unsigned long 	seq) noexcept;
		void	sendFin() noexcept;
		void	sendSynAck(	unsigned long	seq,
									unsigned long	ack
									) noexcept;
		void	sendRst(unsigned long 	seq) noexcept;
		void	sendAck(	unsigned long	seq,
									unsigned long	ack
									) noexcept;
		void	sendPiggybackAck(	unsigned long	seq,
											unsigned long	ack
											) noexcept;
		/** Send queued requests as resources and flow control allow,
			with piggyback acknowledgment etc. Context is responsible
			for returning completed send requests to user after each
			is segmentized transmitted and placed in the retransmission
			queue.
		 */
		void	sendQueuedPiggyback() noexcept;
		/** If there are any queued received segments and there
			are queued receive requests, satisfy as many receive
			requests with the queued segments as possible, and
			return completed requests.
		 */
		void	processQueuedReceiveSegments() noexcept;
		/** Return current rx request with a closing error. */
		void	returnRxRequestWithClosingError() noexcept;
		void	queueSendRequest() noexcept;
		void	queueNormalReceiveRequest() noexcept;
		void	queueUrgentReceiveRequest() noexcept;
		void	queueCloseRequest() noexcept;
		void	queuePushRequest() noexcept;
		void	copyNewConnectionValuesFromSegment() noexcept;
		void	acknowledgeRetransmissionQueue() noexcept;
		/** See RFC793 page 69 paragraph 1 */
		void	trimSegmentToFitWindow() noexcept;
		void	removeAllFromRetransmissionQueue() noexcept;
		void	resetOutstandingReceiveRequests() noexcept;
		void	resetOutstandingTransmitRequests() noexcept;
		/** All pending receive requests are returned with
			closing error.
		 */
		void	returnAllPendingReceives() noexcept;
		void	returnAllPendingSends() noexcept;
		void	flushAllSegmentQueues() noexcept;
		void	signalConnectionClosing() noexcept;
		void	signalConnectionReset() noexcept;
		void	signalUrgentData() noexcept;
		void	signalUrgentDataIfRequired() noexcept;
		void	adjustWindow(unsigned nBytesReceived) noexcept;
		void	startTimeWaitTimer() noexcept;
		void	stopTimeWaitTimer() noexcept;
		void	startUserTimer() noexcept;
		void	stopUserTimer() noexcept;
		void	startRetransmissionTimer() noexcept;
		void	stopRetransmissionTimer() noexcept;
		void	acknowledgeUserClose() noexcept;
		void	deleteTheTCB() noexcept;
		void	setActiveOpen() noexcept;
		void	returnSendReqWithNotOpenError() noexcept;
		unsigned	deliverTextToRxBuffers() noexcept;
		void	returnOpenReq() noexcept;
		void	listening() noexcept;

		bool	userTimerIsExpired() noexcept;

		/** SND */
		void	setSndUna(const Oscl::Modulo::U32& value) noexcept;
		void	setSndNxt(const Oscl::Modulo::U32& value) noexcept;
		void	setSndWnd(unsigned long value) noexcept;
		void	setSndUp(unsigned long value) noexcept;
		void	setSndWl1(const Oscl::Modulo::U32& value) noexcept;
		void	setSndWl2(const Oscl::Modulo::U32& value) noexcept;
		void	setISS(const Oscl::Modulo::U32& value) noexcept;

		/** RCV */
		void	setRcvNxt(const Oscl::Modulo::U32& value) noexcept;
		void	setRcvWnd(unsigned long value) noexcept;
		void	setRcvUp(unsigned long value) noexcept;
		void	setIRS(const Oscl::Modulo::U32& value) noexcept;

		/** */
		bool	isACK() const noexcept;
		bool	isRST() const noexcept;
		bool	isSYN() const noexcept;
		bool	isURG() const noexcept;
		bool	isFIN() const noexcept;
		bool	securityCompartmentMatches() const noexcept;
		bool	isPassiveOpen() const noexcept;
		bool	finIsAcknowledged() const noexcept;
		/** Returns true if any segments have been transmitted. */
		bool	segmentsHaveBeenSent() const noexcept;
		bool	sendRequestQueueIsEmpty() const noexcept;
		bool	closeRequestQueueIsEmpty() const noexcept;
		bool	pendingRxSegmentQueueIsEmpty() const noexcept;
		bool	retransmissionQueueIsEmpty() const noexcept;

		/** */
		Oscl::Modulo::U32	getSegSEQ() const noexcept;
		/** */
		unsigned long	getSegLEN() const noexcept;
		/** */
		Oscl::Modulo::U32	getSegACK() const noexcept;
		/** */
		unsigned long	getSegUP() const noexcept;
		/** */
		unsigned long	getSegWND() const noexcept;
		/** */
		Oscl::Modulo::U32	getNewISS() const noexcept;

		/** */
		Oscl::Modulo::U32	getSndUna() const noexcept;
		/** */
		Oscl::Modulo::U32	getSndNxt() const noexcept;
		/** */
		unsigned long	getSndWnd() const noexcept;
		/** */
		unsigned long	getSndUp() const noexcept;
		/** */
		Oscl::Modulo::U32	getSndWl1() const noexcept;
		/** */
		Oscl::Modulo::U32	getSndWl2() const noexcept;
		/** */
		Oscl::Modulo::U32	getISS() const noexcept;
		/** */
		Oscl::Modulo::U32	getRcvNxt() const noexcept;
		/** */
		unsigned long	getRcvWnd() const noexcept;
		/** */
		unsigned long	getRcvUp() const noexcept;
		/** */
		Oscl::Modulo::U32	getIRS() const noexcept;

	private: //Oscl::FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;

	public: // Oscl::Protocol::IP::TCP::Conn::Srv::Api
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		urgInputSAP() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		normalInputSAP() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	outputSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&				syncInput() noexcept;
		/** */
		Oscl::Stream::Output::Api&				syncOutput() noexcept;
	};

}
}
}
}
}
}
}

#endif
