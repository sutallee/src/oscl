/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_texth_
#define _oscl_protocol_ip_tcp_conn_srv_server_texth_
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/memory/block.h"
#include "oscl/queue/queueitem.h"
#include "oscl/handle/handle.h"
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** This class describes a text segment that has been
	received and is acceptable, but which has neither
	been fully acknowledged nor copied into a user's
	receive buffer. These items may either be queued
	because there was no receive buffer into which to
	copy the text, or because the sequence number was
	higher than RCV.NXT (e.g. the packet was received
	out-of-sequence). When queued, these text segments
	must be queued in sequence-number order.
 */
class Text :	public	Oscl::QueueItem {
	public:
		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_segment;
		/** */
		unsigned long					_sequenceNumber;
		/** */
		unsigned long					_remaining;
		/** */
		unsigned						_nUrgent;
		/** */
		bool							_push;

	public:
		/** */
		Text(	Oscl::Pdu::Pdu&			segment,
				unsigned long			sequenceNumber,
				unsigned long			remaining,
				unsigned				nUrgent,
				bool					push
				) noexcept;
	};

}
}
}
}
}
}
}

#endif
