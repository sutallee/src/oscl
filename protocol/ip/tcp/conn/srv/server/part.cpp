/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/error/fatal.h"
#include "part.h"
#include "segment.h"
#include "segfrag.h"
#include "oscl/timer/counter.h"
#include "oscl/entropy/random.h"
#include "visit.h"
#include "oscl/checksum/onescomp/b16/calculator.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Srv::Server;

static unsigned	getSegmentTextParams(	unsigned long	segSeq,
										unsigned 		segLen,
										unsigned long	rcvNxt,
										unsigned&		segOffset
										) noexcept{
	if(segSeq == rcvNxt){
		segOffset		= 0;
		return segLen;
		}
	else if(segSeq < rcvNxt){
		// Easy case
		unsigned long	delta		= rcvNxt-segSeq;
		if(delta >= segLen){
			segOffset	= 0;
			return 0;
			}
		else {
			segOffset		= delta;
			return segLen-delta;
			}
		}
	else { // (segSeq > rcvNxt)
		unsigned long	delta		= segSeq-rcvNxt;
		if((segSeq ^ rcvNxt) & 0x80000000){
			// Signs are different
			if(delta < 0x80000000){
				// Data is beyond RCV.NXT
				segOffset	= 0;
				return 0;
				}
			else {
				// Segment starts before RCV.NXT
				// Thus delta is the amount of
				// previously received data.
				unsigned long	negSegSeq	= (~segSeq)+1;
				unsigned long	diff		= (negSegSeq + rcvNxt)&0x7FFFFFFF;
				if(diff >= segLen){
					// All data in segment has previously
					// been received.
					segOffset	= 0;
					return 0;
					}
				else {
					segOffset	= diff;
					return segLen-diff;
					}
				}
			}
		else{
			// Signs are the same
			// SEG.SEQ is beyond the RCV.NXT
			segOffset	= 0;
			return 0;
			}
		}
	}

Part::Part(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&			ipFwdSAP,
			Oscl::Protocol::IP::FWD::Api&	ipFwdApi,
			Oscl::Protocol::IP::
			Socket::TCP::Api&				socketApi,
			unsigned						maxTextTxMTU,
			TransMem						transMem[],
			unsigned						nTransMem,
			SegTransMem						txSegmentMem[],
			unsigned						nTxSegmentMem,
			SegFragTransMem					txSegFragMem[],
			unsigned						nTxSegFragMem,
			TextTransMem					textTransMem[],
			unsigned						nTextTransMem,
			void*							txBufferMem,
			unsigned						txBufferMemSize
			) noexcept:
		CloseSync(*this,myPapi),
		_ticker(*this),
		_myPapi(myPapi),
		_ipFwdSAP(ipFwdSAP),
		_ipFwdApi(ipFwdApi),
		_socketApi(socketApi),
		_path(),
		_urgRx(*this),
		_urgInput(myPapi,_urgRx),
		_normalRx(*this),
		_normalInput(myPapi,_normalRx),
		_connSync(*this,myPapi),
		_outputSync(myPapi,*this),
		_state(*this),
		_maxTextTxMTU((maxTextTxMTU>txBufferMemSize)?txBufferMemSize:maxTextTxMTU),
		_nPendingTransactions(0),
		_openReq(0),
		_closeReq(0),
		_open(false),
		_passive(true),
		_sndUna(0),
		_sndNxt(0),
		_sndWnd(0),
		_sndUp(0),
		_sndWl1(0),
		_sndWl2(0),
		_iss(0),
		_rcvNxt(0),
		_rcvWnd(0),
		_rcvUp(0),
		_irs(0),
		_lastSegmentSentTimeInMicroseconds(0),
		_retransmissionTimer(0),
		_urgentMode(false),
		_piggyBack(false),
		_finPending(false),
		_finAcknowledged(false)
		{
	_path.path.localPortNum	= socketApi.getLocalPortNumber();
	_current.normalReadReq	= 0;
	_current.urgReadReq		= 0;
	_current.writeReq		= 0;
	for(unsigned i=0;i<nTextTransMem;++i){
		_freeTextTransMem.put(&textTransMem[i]);
		}
	for(unsigned i=0;i<nTransMem;++i){
		_freeTransMem.put(&transMem[i]);
		}
	for(unsigned i=0;i<nTxSegmentMem;++i){
		_freeSegTransMem.put(&txSegmentMem[i]);
		}
	for(unsigned i=0;i<nTxSegFragMem;++i){
		_freeSegFragTransMem.put(&txSegFragMem[i]);
		}
	if(txBufferMemSize < maxTextTxMTU){
		for(;;);	// Bad config
		}
	// Split up buffer into Maximum Transfer Unit sized chunks
	// Buffers will only be flushed, not invalidated and thus
	// there is no need to worry about cache alignment here
	// as long as the txBufferMem was aligned by the creator.
	unsigned char*	buffMem	= (unsigned char*)txBufferMem;
	for(unsigned i=0;i<txBufferMemSize;i+=_maxTextTxMTU){
		_freeSegBufferMem.put((SegBufferMem*)&buffMem[i]);
		}
	}

void	Part::open() noexcept{
	if(_open){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::TCP::"
										"Conn::Srv::Server:"
										"open protocol violation 1"
										);
		}
	start();
	syncOpen();
	}

void	Part::close() noexcept{
	if(!_open){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Protocol::IP::TCP::"
										"Conn::Srv::Server:"
										"close protocol violation 1"
										);
		}
	syncClose();
	stop();
	}

void	Part::start() noexcept{
	_socketApi.getMuxApi().attach(*this);
	}

void	Part::stop() noexcept{
	_socketApi.getMuxApi().detach(*this);
	}

void	Part::done(RstTrans& trans) noexcept{
	trans.~RstTrans();
	free((TransMem*)&trans);
	}

void	Part::done(SynAckTrans& trans) noexcept{
	trans.~SynAckTrans();
	free((TransMem*)&trans);
	}

void	Part::done(SegFragment& trans) noexcept{
	trans.~SegFragment();
	_freeSegFragTransMem.put((SegFragTransMem*)&trans);
	//FIXME: closing cleanup
	}

void	Part::done(Segment& trans) noexcept{
	_freeSegBufferMem.put((SegBufferMem*)trans._buffer);
	trans.~Segment();
	_freeSegTransMem.put((SegTransMem*)&trans);
	// FIXME: closing cleanup
	}

void	Part::free(TransMem* trans) noexcept{
	--_nPendingTransactions;
	_freeTransMem.put(trans);
	if(!_open){
		if(!_nPendingTransactions){
			if(_closeReq){
				_socketApi.detach(_ticker);
				_closeReq->returnToSender();
				_closeReq	= 0;
				_open	= false;
				}
			}
		}
	else{
		scheduleTransaction();
		}
	}
void	Part::free(Text& text) noexcept{
	text.~Text();
	_freeTextTransMem.put((TextTransMem*)&text);
	}

TransMem*	Part::allocTransMem() noexcept{
	TransMem*	trans	= _freeTransMem.get();
	if(trans){
		++_nPendingTransactions;
		}
	return trans;
	}

void	Part::scheduleTransaction() noexcept{
	// FIXME:
	// Fire off pending transactions.
	// E.g. send next segment or acknowledgement.
	_lastSegmentSentTimeInMicroseconds	= OsclTimerGetMicrosecondCounter();// FIXME
	}

void	Part::urgRxRequest(	Oscl::Stream::Input::
							Req::Api::ReadReq&		msg
							) noexcept{
	// Indicates a new URG read request is pending.
	_urgReadReq	= &msg;
	_state.urgentRx();
	}

void	Part::normalRxRequest(	Oscl::Stream::Input::
								Req::Api::ReadReq&		msg
								) noexcept{
	// Indicates a new normal read request is pending.
	_readReq	= &msg;
	_state.normalRx();
	}

void	Part::tick() noexcept{
	// This operation is invoked every socket tick period.
	// FIXME: Generate timer expiry events into the FSM
	// based on the current time. E.g. :
	//	* userTimeout()
	//	* retransmissionTimeout()
	//	* timeWaitTimeout()
	if(_retransmissionTimer.getValue() != 0){
		if(_retransmissionQueue.first()){
			Oscl::Modulo::U32	newTime	= OsclTimerGetMillisecondCounter();
			if(newTime >= _retransmissionTimer){
				_retransmissionQueue.first()->send(	_ipFwdApi,
													_rcvNxt,
													_rcvWnd
													);
				restartRetransmissionTimer();
				return;
				}
			}
		}
	if(_piggyBack){
		sendAck(	_sndNxt.getValue(),
					_rcvNxt.getValue()
					);
		_piggyBack	= false;
		}
	_state.tick();
	}

void	Part::restartRetransmissionTimer() noexcept{
	_retransmissionTimer	= OsclTimerGetMillisecondCounter() + 1000;
	}

void	Part::sendSimple(	unsigned long	seq,
							unsigned long	ack,
							unsigned		flags,
							unsigned		window
							) noexcept{
	Oscl::Protocol::IP::Header*
	ipHeader	= (Oscl::Protocol::IP::Header*)&_syncTransMem;
	Oscl::Protocol::IP::TCP::Header*
	tcpHeader	= (Oscl::Protocol::IP::TCP::Header*)ipHeader->options;

	const unsigned	tcpHeaderLength	=	Oscl::Protocol::IP::
										TCP::Header::minHeaderSize;
	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;
	const unsigned	totalLength		= ipHeaderLength+tcpHeaderLength;
	const unsigned	tcpLength		= tcpHeaderLength;

	ipHeader->source.assign(_current.localIpAddress);
	ipHeader->destination.assign(_current.peerIpAddress);
	ipHeader->length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader->frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader->type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_TCP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader->updateChecksum(0);

	Oscl::Checksum::OnesComp::B16::Calculator	csum;

	// Pseudo header
	csum.accumulate(&ipHeader->source,sizeof(ipHeader->source));
	csum.accumulate(&ipHeader->destination,sizeof(ipHeader->destination));
	Oscl::Endian::Big::U16	uLength;
	uLength	= tcpLength;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_TCP;
	csum.accumulate(&protocol,sizeof(protocol));
	csum.accumulate(&uLength,sizeof(uLength));

	// TCP header
	tcpHeader->sourcePort			= _current.localPort;
	tcpHeader->destinationPort		= _current.peerPort;
	tcpHeader->sequenceNumber		= seq;
	tcpHeader->acknowlegementNumber	= ack;
	tcpHeader->dataOffsetAndFlags	= (0x5000 | flags);
	tcpHeader->window				= window;
	tcpHeader->checksum				= 0;
	tcpHeader->urgentPointer		= 0;

	csum.accumulate(tcpHeader,tcpHeaderLength);

	uint16_t	checksum	= csum.final();
	if(!checksum){
		checksum	= ~checksum;
		}

	// Finish the TCP header
	tcpHeader->checksum			= checksum;
	Oscl::Pdu::Fixed			pdu(	*this,
										*this,
										ipHeader,
										totalLength,
										totalLength
										);
	Oscl::Protocol::IP::Address	destAddr;
	destAddr.assign(_current.peerIpAddress);
	_ipFwdApi.forward(pdu,destAddr);
	}

bool	Part::accept(	Oscl::Pdu::Pdu&					segment,
						const Oscl::Protocol::IP::
						TCP::Header&					tcpHeader,
						unsigned long					peerIpAddress,
						unsigned long					localIpAddress,
						unsigned						peerPort,
						unsigned						localPort,
						unsigned						ipPayloadLength
						) noexcept{
	if(localPort != _path.path.localPortNum){
		return false;
		}

	if(_open){
		if(	!(
					(peerIpAddress == _path.path.peerIpAddress)
				&&	(localIpAddress	== _path.path.localIpAddress)
				&&	(peerPort	== _path.path.peerPortNum)
				)
			){
			return false;
			}
		}

	_current.segment			= &segment;
	_current.tcpHeader			= &tcpHeader;
	_current.peerIpAddress		= peerIpAddress;
	_current.localIpAddress		= localIpAddress;
	_current.peerPort			= peerPort;
	_current.localPort			= localPort;
	_current.ipPayloadLength	= ipPayloadLength;

	_state.segmentArrival();

	return true;
	}


Oscl::Stream::Input::Req::Api::SAP&		Part::getUrgInputSAP() noexcept{
	return _urgInput.getInputSAP();
	}

Oscl::Stream::Input::Req::Api::SAP&		Part::getNormalInputSAP() noexcept{
	return _normalInput.getInputSAP();
	}

Oscl::Stream::Output::Req::Api::SAP&	Part::getOutputSAP() noexcept{
	return _outputSync.getSAP();
	}

Oscl::Protocol::IP::TCP::
Conn::Srv::ITC::Req::Api::SAP&	Part::getConnSAP() noexcept{
	return _connSync.getSAP();
	}

Oscl::Stream::Input::Api&	Part::urgInput() noexcept{
	return _urgInput.getSyncApi();
	}

Oscl::Stream::Input::Api&	Part::input() noexcept{
	return _normalInput.getSyncApi();
	}

Oscl::Stream::Output::Api&	Part::output() noexcept{
	return _outputSync;
	}

Oscl::Protocol::IP::TCP::
Conn::Api&	Part::getConnApi() noexcept{
	return _connSync;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	_state.close();
	}

void	Part::request(	Oscl::Protocol::IP::TCP::
						Conn::Srv::ITC::Req::Api::ListenReq&	msg
						) noexcept{
	_openReq	= &msg;
	_socketApi.attach(_ticker);
	_rcvWnd		= 0;
	_state.open();
	}

void	Part::request(	Oscl::Protocol::IP::TCP::
						Conn::Srv::ITC::Req::Api::CancelReq&	msg
						) noexcept{
	if(_openReq){
		_openReq->returnToSender();
		_openReq	= 0;
		}
	msg.returnToSender();
	}

void	Part::request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept{
	_writeReq	= &msg;
	_state.send();
	}

void	Part::request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept{
	_pushReq	= &msg;
	_state.push();
	}

void	Part::sendRstAck(	unsigned long	seq,
							unsigned long	ack
							) noexcept{
	sendSimple(	seq,
				ack,
				(_current.tcpHeader->isACK())?0x0004:0x0014,
				_rcvWnd
				);
	}

void	Part::sendSyn(unsigned long	seq) noexcept{
	sendSimple(	seq,
				0,
				0x0002,	// FIXME: SYN,ACK
				_rcvWnd
				);
	}

void	Part::sendFin() noexcept{
	if(_finPending) return;
	_finAcknowledged	= false;
	SegTransMem*		stmem	= _freeSegTransMem.get();
	SegBufferMem*		segBuff	= _freeSegBufferMem.get();
	if(!stmem || !segBuff){
		if(stmem){
			_freeSegTransMem.put(stmem);
			}
		if(segBuff){
			_freeSegBufferMem.put(segBuff);
			}
		_finPending	= true;
		return;
		}
	Segment*
	segment	= new(stmem)
				Segment(	segBuff,
							_maxTextTxMTU,
							_current.peerIpAddress,
							_current.localIpAddress,
							_current.peerPort,
							_current.localPort,
							_sndNxt,
							0,		// Urgent pointer
							false,	// URG flag
							true,	// ACK flag
							true,	// PSH flag ??
							false,	// RST flag
							false,	// SYN flag
							true	// FIN flag
							);
	segment->send(	_ipFwdApi,
					_rcvNxt,
					_rcvWnd
					);
	_sndNxt	+= 1;
	_retransmissionQueue.put(segment);
	restartRetransmissionTimer();
	}

void	Part::sendSynAck(	unsigned long	seq,
							unsigned long	ack
							) noexcept{
	sendSimple(	seq,
				ack,
				0x0012,	// FIXME: SYN,ACK
				_rcvWnd
				);
	}

void	Part::sendRst(unsigned long 	seq) noexcept{
	sendSimple(	seq,
				0,
				0x0004,	// FIXME: RST
				0
				);
	}

void	Part::sendAck(	unsigned long	seq,
						unsigned long	ack
						) noexcept{
	sendSimple(	seq,
				ack,
				0x0010,	// FIXME: ACK
				_rcvWnd
				);
	}

void	Part::sendPiggybackAck(	unsigned long	seq,
								unsigned long	ack
								) noexcept{
	_piggyBack	= true;
	}

void	Part::sendQueuedPiggyback() noexcept{
	Segment*	segment	= 0;
	Oscl::Modulo::U32	available;
	available 	= _sndUna;
	available	+= _sndWnd;
	available	-= _sndNxt;
	unsigned long	nAvailable	= available.getValue();
	while(nAvailable){
		if(!_current.writeReq){
			_current.writeReq	= _pendingWrites.get();
			if(_current.writeReq){
				_current.writeBuffer	= (const unsigned char*)_current.writeReq->_payload._buffer.getBuffer();
				_current.writeLength	= _current.writeReq->_payload._buffer.length();
				_current.writeOffset	= 0;
				}
			}
		if(!_current.writeReq){
			if(segment){
				if(segment->isFIN()){
					_sndNxt		+= 1;
					nAvailable	-= 1;
					}
				segment->send(	_ipFwdApi,
								_rcvNxt,
								_rcvWnd
								);
				_retransmissionQueue.put(segment);
				restartRetransmissionTimer();
				}
			return;
			}
		Oscl::Stream::Output::Req::Api::WriteReq*
		req	= _current.writeReq;

		SegFragTransMem*	sftmem;
		if(!segment){
			SegTransMem*		stmem	= _freeSegTransMem.get();
			SegBufferMem*		segBuff	= _freeSegBufferMem.get();
			sftmem				= _freeSegFragTransMem.get();
			if(!stmem || !sftmem || !segBuff){
				if(stmem){
					_freeSegTransMem.put(stmem);
					}
				if(sftmem){
					_freeSegFragTransMem.put(sftmem);
					}
				if(segBuff){
					_freeSegBufferMem.put(segBuff);
					}
				return;
				}
			segment	= new(stmem)
						Segment(	segBuff,
									_maxTextTxMTU,
									_current.peerIpAddress,
									_current.localIpAddress,
									_current.peerPort,
									_current.localPort,
									_sndNxt,
									0,		// Urgent pointer
									false,	// URG flag
									true,	// ACK flag
									true,	// PSH flag
									false,	// RST flag
									false,	// SYN flag
									false	// FIN flag
									);
			if(segment->isSYN()){
				_sndNxt		+= 1;
				nAvailable	-= 1;
				}
			}
		else {
			sftmem	= _freeSegFragTransMem.get();
			if(!sftmem){
//				if(sftmem){
//					_freeSegFragTransMem.put(sftmem);
//					}
				if(segment->isFIN()){
					_sndNxt		+= 1;
					nAvailable	-= 1;
					}
				segment->send(	_ipFwdApi,
								_rcvNxt,
								_rcvWnd
								);
				_retransmissionQueue.put(segment);
				restartRetransmissionTimer();
				return;
				}
			}

		unsigned	remaining	= segment->remaining();
		if(!remaining){
			if(segment->isFIN()){
				_sndNxt		+= 1;
				nAvailable	-= 1;
				}
			segment->send(	_ipFwdApi,
							_rcvNxt,
							_rcvWnd
							);
			_retransmissionQueue.put(segment);
			restartRetransmissionTimer();
			segment	= 0;
			continue;
			}
		unsigned	len	= _current.writeLength;
		if(len > nAvailable){
			len	= nAvailable;
			}
		if(len > remaining){
			if(len == remaining){
				Oscl::ErrorFatal::logAndExit("oops");
				}
			len	= remaining;
			req	= 0;
			}
		SegFragment*
		frag	= new(sftmem)
					SegFragment(	*this,
									&_current.writeBuffer[_current.writeOffset],
									_sndNxt,	// seq
									0,			// offset
									len,		// fragment length
									req
									);
		segment->append(*frag);
		if(req){
			_current.writeReq	= 0;
			}
		else {
			_current.writeLength	-= len;
			_current.writeOffset	+= len;
			}
		_sndNxt		+= len;
		nAvailable	-= len;
		}
	if(segment){
		if(segment->isFIN()){
			_sndNxt		+= 1;
			nAvailable	-= 1;
			}
		segment->send(	_ipFwdApi,
						_rcvNxt,
						_rcvWnd
						);
		_retransmissionQueue.put(segment);
		restartRetransmissionTimer();
		}
	}

void	Part::processQueuedReceiveSegments() noexcept{
	unsigned	nDelivered	= deliverQueuedTextSegments();
	_rcvNxt	+= nDelivered;
	}

void	Part::returnRxRequestWithClosingError() noexcept{
	_readReq->returnToSender();
	_readReq	= 0;
	}

void	Part::queueSendRequest() noexcept{
	_pendingWrites.put(_writeReq);
	}

void	Part::queueNormalReceiveRequest() noexcept{
	_pendingReads.put(_readReq);
	unsigned	oldRcvWnd	= _rcvWnd;
	_rcvWnd	+= _readReq->_payload._buffer.remaining();
	if(!oldRcvWnd){
		sendAck(	_sndNxt.getValue(),
					_rcvNxt.getValue()
					);
		}
	}

void	Part::queueUrgentReceiveRequest() noexcept{
	_pendingUrgReads.put(_readReq);
	}

void	Part::queueCloseRequest() noexcept{
	_pendingCloses.put(_closeReq);
	_closeReq	= 0;
	}

void	Part::queuePushRequest() noexcept{
	_pendingFlushes.put(_pushReq);
	}

void	Part::copyNewConnectionValuesFromSegment() noexcept{
	if(!_passive){
		for(;;);	// FIXME
		}
	_path.path.peerIpAddress	= _current.peerIpAddress;
	_path.path.localIpAddress	= _current.localIpAddress;
	_path.path.peerPortNum		= _current.peerPort;
	_path.path.localPortNum		= _current.localPort;
	_open		= true;
	}

void	Part::acknowledgeRetransmissionQueue() noexcept{
	Segment*	next;
	for(	next	= _retransmissionQueue.first();
			next;
			next	= _retransmissionQueue.next(next)
			){
		unsigned	nAcked;
		nAcked	= next->ack(_sndUna);
		if(nAcked){
			restartRetransmissionTimer();
			}
		if(next->fullyAcknowledged()){
			next	= _retransmissionQueue.get();
			next->returnReadRequestNormal();
			if(next->isFIN()){
				_finAcknowledged	= true;
				}
			done(*next);
			if(_finPending){
				_finPending	= false;
				sendFin();
				}
			}
		}
	// Now queue pending transmissions that
	// may be pending due to lack of segment
	// buffers.
	sendQueuedPiggyback();
	}

void	Part::trimSegmentToFitWindow() noexcept{
	unsigned long	seq	= _current.tcpHeader->sequenceNumber;
	unsigned long	len	=		_current.ipPayloadLength
							-	_current.tcpHeader->getHeaderLength()
							;
	unsigned		segOffset;
	unsigned		nBytesToCopy;
	nBytesToCopy	= getSegmentTextParams(	seq,
											len,
											_rcvNxt.getValue(),
											segOffset
											);
	if(_current.tcpHeader->isURG()){
		_urgentMode				= true;
		_current.nUrgent		= _current.tcpHeader->urgentPointer;
		_current.nUrgent		-= segOffset;
		}
	else{
		_current.nUrgent		= 0;
		}
	_current.sequence		= seq + segOffset;
	_current.remaining		= nBytesToCopy;
	_current.segment->stripHeader(		_current.tcpHeader->getHeaderLength()
									+	segOffset
									);
	}

void	Part::removeAllFromRetransmissionQueue() noexcept{
	Segment*	next;
	for(	next	= _retransmissionQueue.first();
			next;
			next	= _retransmissionQueue.next(next)
			){
		next->ack(_sndUna);
		if(next->fullyAcknowledged()){
			next	= _retransmissionQueue.get();
			next->returnReadRequestClosing();
			done(*next);
			}
		}
	if(_current.writeReq){
		_current.writeReq->_payload._nWritten	= 0;
		_current.writeReq->returnToSender();
		_current.writeReq	= 0;
		}
	}

void	Part::resetOutstandingReceiveRequests() noexcept{
	Oscl::Stream::Input::Req::Api::ReadReq*	req;
	if(_current.normalReadReq){
		_current.normalReadReq->returnToSender();
		_current.normalReadReq	= 0;
		}
	if(_current.urgReadReq){
		_current.urgReadReq->returnToSender();
		_current.urgReadReq	= 0;
		}
	while((req=_pendingUrgReads.get())){
		req->returnToSender();
		}
	while((req=_pendingReads.get())){
		req->returnToSender();
		}
	}

void	Part::resetOutstandingTransmitRequests() noexcept{
	// Called when connection is RST
	removeAllFromRetransmissionQueue();
	}

void	Part::returnAllPendingReceives() noexcept{
	// Called when FIN is received.
	// Called when close() is invoked.
	// This is the equivalent of a PUSH.
	Oscl::Stream::Input::Req::Api::ReadReq*	req;
	if(_current.normalReadReq){
		_rcvWnd	-= _current.normalReadReq->_payload._buffer.remaining();
		_current.normalReadReq->returnToSender();
		_current.normalReadReq	= 0;
		}
	while((req=_pendingReads.get())){
		_rcvWnd	-= req->_payload._buffer.remaining();
		req->returnToSender();
		}
	if(_current.urgReadReq){
		_rcvWnd	-= _current.urgReadReq->_payload._buffer.remaining();
		_current.urgReadReq->returnToSender();
		_current.urgReadReq	= 0;
		}
	while((req=_pendingUrgReads.get())){
		_rcvWnd	-= req->_payload._buffer.remaining();
		req->returnToSender();
		}
	}

void	Part::returnAllPendingSends() noexcept{
	// Called when close() is invoked.
	}

void	Part::flushAllSegmentQueues() noexcept{
	// Called when connection is RST
	Text*	text;
	while((text	= _pendingText.get())){
		free(*text);
		}
	}

void	Part::signalConnectionClosing() noexcept{
	// Called when FIN is received.
	}

void	Part::signalConnectionReset() noexcept{
	// Called when connection is RST
	// Called when connection is SYN
	// Called when security/compartment does not match
	}

void	Part::signalUrgentData() noexcept{
	// FIXME: Not invoked by FSM?
	for(;;);
	}

void	Part::signalUrgentDataIfRequired() noexcept{
	// Called when segment has URG
	}

void	Part::adjustWindow(unsigned nBytesReceived) noexcept{
	_rcvWnd	-= nBytesReceived;
	}

void	Part::startTimeWaitTimer() noexcept{
	// Called from FIN-WAIT-* and TIME-WAIT
	// FIXME:
	for(;;);
	}

void	Part::stopTimeWaitTimer() noexcept{
	// Called from TIME-WAIT segment arrival event
	// FIXME:
	for(;;);
	}

void	Part::startUserTimer() noexcept{
	_userTimerEnabled	= true;
	_userTimer			= OsclTimerGetMillisecondCounter() + 10000;
	}

void	Part::stopUserTimer() noexcept{
	_userTimerEnabled	= false;
	return;
	// Called from FIN-WAIT-* when FIN is acked.
	// FIXME:
//	for(;;);
	}

bool	Part::userTimerIsExpired() noexcept{
	Oscl::Modulo::U32	newTime	= OsclTimerGetMillisecondCounter();
	bool	expired	= (newTime >= _userTimer);
	if(expired){
		_userTimerEnabled	= false;
		}
	return expired;
	}

void	Part::startRetransmissionTimer() noexcept{
	// Not called by FSM?
	// FIXME:
	for(;;);
	}

void	Part::stopRetransmissionTimer() noexcept{
	// Called from FIN-WAIT-* when FIN is acked.
	// FIXME:
	for(;;);
	}

void	Part::acknowledgeUserClose() noexcept{
	// Called from FIN-WAIT-2 when retransmissionQueueIsEmpty()
	}

void	Part::deleteTheTCB() noexcept{
	// Called from LAST-ACK after FIN is acknowledged.
	// Called from LISTEN on close event
	// Called from SYN-SENT on close event
	if(_closeReq){
		_closeReq->returnToSender();
		_closeReq	= 0;
		}
	Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	closeReq;
	while((closeReq=_pendingCloses.get())){
		closeReq->returnToSender();
		}
	_open	= false;
	}

void	Part::setActiveOpen() noexcept{
	// Called from LISTEN on open event if isPassiveOpen()
	_passive	= false;
	}

void	Part::returnSendReqWithNotOpenError() noexcept{
	// Called from CLOSED on send event.
	_writeReq->_payload._nWritten	= 0;
	_writeReq->returnToSender();
	_writeReq	= 0;
	}

void		Part::queueText(Text& text) noexcept{
	Text*	next;
	for(	next	= _pendingText.first();
			next;
			next	= _pendingText.next(next)
			){
		if(text._sequenceNumber < next->_sequenceNumber){
			_pendingText.insertBefore(next,&text);
			return;
			}
		}
	_pendingText.put(&text);
	}

unsigned	Part::deliverQueuedTextSegments() noexcept{
	unsigned	nDelivered=0;
	Text*	text;
	while((text	= _pendingText.first())){
		if(text->_sequenceNumber != _rcvNxt){
			return nDelivered;
			}
		while(text->_nUrgent){
			if(!_current.urgReadReq){
				_current.urgReadReq	= _pendingUrgReads.get();
				}
			if(!_current.urgReadReq){
				return nDelivered;
				}
			unsigned	len;
			len	= _current.urgReadReq->_payload._buffer.remaining();
			Append	appendVisitor(_current.urgReadReq->_payload._buffer);
			text->_segment->visit(	appendVisitor,
									0,
									0,
									text->_nUrgent
									);
			len	-= _current.urgReadReq->_payload._buffer.remaining();
			if(!_current.urgReadReq->_payload._buffer.remaining()){
				_current.urgReadReq->returnToSender();
				_current.urgReadReq	= 0;
				}
			text->_remaining	-= len;
			text->_nUrgent		-= len;
			text->_segment->stripHeader(len);
			nDelivered	+= len;
			}

		if(!text->_remaining){
			Text*	oldText;
			oldText	= _pendingText.get();
			text	= _pendingText.first();
			if(oldText->_push){
				if(!text || !text->_push){
					if(_current.normalReadReq){
						if(_current.normalReadReq->_payload._buffer.length()){
							_rcvWnd	-= _current.normalReadReq->_payload._buffer.remaining();
							_current.normalReadReq->returnToSender();
							_current.normalReadReq	= 0;
							}
						}
					}
				}
			free(*oldText);
			continue;
			}
		else {
			if(_current.urgReadReq){
				_rcvWnd	-= _current.normalReadReq->_payload._buffer.remaining();
				_current.urgReadReq->returnToSender();
				_current.urgReadReq	= 0;
				}
			}

		while(text->_remaining){
			if(!_current.normalReadReq){
				_current.normalReadReq	= _pendingReads.get();
				}
			if(!_current.normalReadReq){
				return nDelivered;
				}
			unsigned	len;
			len	= _current.normalReadReq->_payload._buffer.remaining();
			Append	appendVisitor(_current.normalReadReq->_payload._buffer);
			text->_segment->visit(	appendVisitor,
									0,
									0,
									text->_remaining
									);
			len	-= _current.normalReadReq->_payload._buffer.remaining();
			if(!_current.normalReadReq->_payload._buffer.remaining()){
				_current.normalReadReq->returnToSender();
				_current.normalReadReq	= 0;
				}
			text->_remaining	-= len;
			text->_segment->stripHeader(len);
			nDelivered	+= len;
			}
		text	= _pendingText.get();
		free(*text);
		}
	return nDelivered;
	}

unsigned	Part::deliverCurrentTextSegment() noexcept{
	if(!_current.segment){
		return 0;
		}
	if(!_current.remaining){
		_current.segment	= 0;
		return 0;
		}
	if(_current.sequence != _rcvNxt){
		TextTransMem*	mem	= _freeTextTransMem.get();
		if(!mem){
			// No queue space for segment, thus drop it.
			_current.segment	= 0;
			_current.remaining	= 0;
			return 0;
			}
		Text*
		text	= new (mem)Text(	*_current.segment,
									_current.sequence,
									_current.remaining,
									_current.nUrgent,
									_current.tcpHeader->isPSH()
									);
		queueText(*text);
		_current.segment	= 0;
		_current.remaining	= 0;
		return 0;
		}
	unsigned	nCopied	= 0;
	while(_current.nUrgent){
		if(!_current.urgReadReq){
			_current.urgReadReq	= _pendingUrgReads.get();
			}
		if(!_current.urgReadReq){
			return nCopied;
			}
		unsigned	len;
		len	= _current.urgReadReq->_payload._buffer.remaining();
		Append	appendVisitor(_current.urgReadReq->_payload._buffer);
		_current.segment->visit(	appendVisitor,
									0,
									0,
									_current.nUrgent
									);
		len	-= _current.urgReadReq->_payload._buffer.remaining();
		if(!_current.urgReadReq->_payload._buffer.remaining()){
			_current.urgReadReq->returnToSender();
			_current.urgReadReq	= 0;
			}
		_current.remaining	-= len;
		_current.nUrgent	-= len;
		_current.segment->stripHeader(len);
		nCopied	+= len;
		}

	if(_current.remaining && _current.urgReadReq){
		_rcvWnd	-= _current.remaining;
		_current.urgReadReq->returnToSender();
		_current.urgReadReq	= 0;
		}

	while(_current.remaining){
		if(!_current.normalReadReq){
			_current.normalReadReq	= _pendingReads.get();
			}
		if(!_current.normalReadReq){
			return nCopied;
			}
		unsigned	len;
		len	= _current.normalReadReq->_payload._buffer.remaining();
		Append	appendVisitor(_current.normalReadReq->_payload._buffer);
		_current.segment->visit(	appendVisitor,
									0,
									0,
									_current.remaining
									);
		len	-= _current.normalReadReq->_payload._buffer.remaining();
		if(!_current.normalReadReq->_payload._buffer.remaining()){
			_current.normalReadReq->returnToSender();
			_current.normalReadReq	= 0;
			}
		_current.remaining	-= len;
		_current.segment->stripHeader(len);
		nCopied	+= len;
		}
	if(_current.normalReadReq){
		if(_current.tcpHeader->isPSH()){
			if(_current.normalReadReq->_payload._buffer.length()){
				_rcvWnd	-= _current.normalReadReq->_payload._buffer.remaining();
				_current.normalReadReq->returnToSender();
				_current.normalReadReq	= 0;
				}
			}
		}
	_current.segment	= 0;
	return nCopied;
	}

unsigned	Part::deliverTextToRxBuffers() noexcept{
	unsigned	nDelivered	= deliverCurrentTextSegment();

	if(_current.remaining){
		TextTransMem*	mem	= _freeTextTransMem.get();
		if(!mem){
			// No queue space for segment, thus drop it.
			_current.segment	= 0;
			_current.remaining	= 0;
			return nDelivered;
			}
		Text*
		text	= new (mem)Text(	*_current.segment,
									_current.sequence,
									_current.remaining,
									_current.nUrgent,
									_current.tcpHeader->isPSH()
									);
		queueText(*text);
		_current.segment	= 0;
		_current.remaining	= 0;
		return nDelivered;
		}

	nDelivered	+= deliverQueuedTextSegments();

	return nDelivered;
	}

void	Part::returnOpenReq() noexcept{
	if(_openReq){
		_openReq->returnToSender();
		_openReq	= 0;
		}
	}

void	Part::listening() noexcept{
	_open		= false;
	}

void	Part::setSndUna(const Oscl::Modulo::U32& value) noexcept{
	_sndUna	= value;
	}

void	Part::setSndNxt(const Oscl::Modulo::U32& value) noexcept{
	_sndNxt	= value;
	}

void	Part::setSndWnd(unsigned long value) noexcept{
	_sndWnd	= value;
	}

void	Part::setSndUp(unsigned long value) noexcept{
	_sndUp	= value;
	}

void	Part::setSndWl1(const Oscl::Modulo::U32& value) noexcept{
	_sndWl1	= value;
	}

void	Part::setSndWl2(const Oscl::Modulo::U32& value) noexcept{
	_sndWl2	= value;
	}

void	Part::setISS(const Oscl::Modulo::U32& value) noexcept{
	_iss	= value;
	}

void	Part::setRcvNxt(const Oscl::Modulo::U32& value) noexcept{
	_rcvNxt	= value;
	}

void	Part::setRcvWnd(unsigned long value) noexcept{
	_rcvWnd	= value;
	}

void	Part::setRcvUp(unsigned long value) noexcept{
	_rcvUp	= value;
	}

void	Part::setIRS(const Oscl::Modulo::U32& value) noexcept{
	_irs	= value;
	}

bool	Part::isACK() const noexcept{
	return _current.tcpHeader->isACK();
	}

bool	Part::isRST() const noexcept{
	return _current.tcpHeader->isRST();
	}

bool	Part::isSYN() const noexcept{
	return _current.tcpHeader->isSYN();
	}

bool	Part::isURG() const noexcept{
	return _current.tcpHeader->isURG();
	}

bool	Part::isFIN() const noexcept{
	return _current.tcpHeader->isFIN();
	}

bool	Part::securityCompartmentMatches() const noexcept{
	return true;
	}

bool	Part::isPassiveOpen() const noexcept{
	return _passive;
	}

bool	Part::finIsAcknowledged() const noexcept{
	return _finAcknowledged;
	}

bool	Part::segmentsHaveBeenSent() const noexcept{
	for(;;);
	return false;
	}

bool	Part::sendRequestQueueIsEmpty() const noexcept{
	return (		!_pendingWrites.first()
				&&	!_retransmissionQueue.first()
				);
	}

bool	Part::closeRequestQueueIsEmpty() const noexcept{
	return !_pendingCloses.first();
	}

bool	Part::pendingRxSegmentQueueIsEmpty() const noexcept{
	return !_pendingText.first();
	}

bool	Part::retransmissionQueueIsEmpty() const noexcept{
	return !_retransmissionQueue.first();
	}

Oscl::Modulo::U32	Part::getSegSEQ() const noexcept{
	unsigned long	seqNum	= _current.tcpHeader->sequenceNumber;
	return seqNum;
	}

unsigned long	Part::getSegLEN() const noexcept{
	return _current.ipPayloadLength -_current.tcpHeader->getHeaderLength();
	}

Oscl::Modulo::U32	Part::getSegACK() const noexcept{
	unsigned long	ackNum	= _current.tcpHeader->acknowlegementNumber;
	return ackNum;
	}

unsigned long	Part::getSegUP() const noexcept{
	return _current.tcpHeader->urgentPointer;
	}

unsigned long	Part::getSegWND() const noexcept{
	return _current.tcpHeader->window;
	}

Oscl::Modulo::U32	Part::getNewISS() const noexcept{
	return OsclEntropyGetRandom();
	}

Oscl::Modulo::U32	Part::getSndUna() const noexcept{
	return _sndUna;
	}

Oscl::Modulo::U32	Part::getSndNxt() const noexcept{
	return _sndNxt;
	}

unsigned long	Part::getSndWnd() const noexcept{
	return _sndWnd;
	}

unsigned long	Part::getSndUp() const noexcept{
	return _sndUp;
	}

Oscl::Modulo::U32	Part::getSndWl1() const noexcept{
	return _sndWl1;
	}

Oscl::Modulo::U32	Part::getSndWl2() const noexcept{
	return _sndWl2;
	}

Oscl::Modulo::U32	Part::getISS() const noexcept{
	return _iss;
	}

Oscl::Modulo::U32	Part::getRcvNxt() const noexcept{
	return _rcvNxt;
	}

unsigned long	Part::getRcvWnd() const noexcept{
	return _rcvWnd;
	}

unsigned long	Part::getRcvUp() const noexcept{
	return _rcvUp;
	}

Oscl::Modulo::U32	Part::getIRS() const noexcept{
	return _irs;
	}

void	Part::free(void* fso) noexcept{
	// Do nothing
	}

///////////////
Oscl::Stream::Input::Req::Api::SAP&	Part::urgInputSAP() noexcept{
	return _urgInput.getInputSAP();
	}

Oscl::Stream::Input::Req::Api::SAP&	Part::normalInputSAP() noexcept{
	return _normalInput.getInputSAP();
	}

Oscl::Stream::Output::Req::Api::SAP&	Part::outputSAP() noexcept{
	return _outputSync.getSAP();
	}

Oscl::Stream::Input::Api&			Part::syncInput() noexcept{
	return _normalInput.getSyncApi();
	}

Oscl::Stream::Output::Api&			Part::syncOutput() noexcept{
	return _outputSync;
	}

///////////////
UrgReceiver::UrgReceiver(Part& context) noexcept:
		_context(context)
		{
	}

void	UrgReceiver::rxRequest(	Oscl::Stream::Input::
								Req::Api::ReadReq&		msg
								) noexcept{
	_context.urgRxRequest(msg);
	}

NormalReceiver::NormalReceiver(Part& context) noexcept:
		_context(context)
		{
	}

void	NormalReceiver::rxRequest(	Oscl::Stream::Input::
									Req::Api::ReadReq&		msg
									) noexcept{
	_context.normalRxRequest(msg);
	}

/////// Ticker

Ticker::Ticker(Part& context) noexcept:
		_context(context)
		{
	}

void	Ticker::tick() noexcept{
	_context.tick();
	}

