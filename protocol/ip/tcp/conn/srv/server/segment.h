/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_segmenth_
#define _oscl_protocol_ip_tcp_conn_srv_server_segmenth_
#include "oscl/protocol/ip/fwd/api.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "segfrag.h"
#include "oscl/protocol/ip/header.h"
#include "oscl/protocol/ip/tcp/header.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class Segment :	public	Oscl::QueueItem,
				private	Oscl::FreeStore::Mgr
				{
	private:
		/** */
		enum{tcpIpHeaderSize	=	IP::TCP::Header::minHeaderSize
								+	IP::Header::minHeaderBytes
								};
		/** */
		Oscl::Memory::
		AlignedBlock<tcpIpHeaderSize>			_headerMem;
	public:
		/** Buffer MUST be cache aligned
			Made public so the Part may free the buffer.
			Alternately, I could pass the Part context
			reference and let the Segment destructor
			do this.
		 */
		unsigned char*							_buffer;
	private:
		/** */
		const unsigned							_bufferSize;
		/** */
		const unsigned long						_peerIpAddress;
		/** */
		const unsigned long						_localIpAddress;
		/** */
		const unsigned							_peerPort;
		/** */
		const unsigned							_localPort;
		/** */
		const Oscl::Modulo::U32					_sequenceNumber;
		/** */
		Oscl::Modulo::U32						_finSeqNumber;
		/** */
		const unsigned							_urgentPointer;
		/** Data offset and URG,ACK,PSH,RST,SYN,FIN
			flags in value mask format.
		 */
		const unsigned							_flags;
		/** */
		unsigned								_textLength;

		/** */
		Oscl::Queue<SegFragment>				_fragmentQueue;
		/** */
		unsigned								_nUnacknowledgedBytes;
		/** */
		bool									_synAcked;
		/** */
		bool									_finAcked;

	public:
		/** */
		Segment(	void*						buffer,
					unsigned					bufferSize,
					unsigned long				peerIpAddress,
					unsigned long				localIpAddress,
					unsigned					peerPort,
					unsigned					localPort,
					Oscl::Modulo::U32			sequenceNumber,
					unsigned					urgentPointer,
					bool						urgFlag,
					bool						ackFlag,
					bool						pshFlag,
					bool						rstFlag,
					bool						synFlag,
					bool						finFlag
					) noexcept;

		/** */
		void	send(	Oscl::Protocol::IP::FWD::Api&	ipFwdApi,
						Oscl::Modulo::U32				acknowledgeNumber,
						unsigned						window
						) noexcept;

		/** This operation is invoked as ACK segments are received
			acknowledgements. Returns total number of bytes acknowledged.
			*/
		unsigned	ack(Oscl::Modulo::U32	acknowledgeNumber) noexcept;

		/** */
		bool		fullyAcknowledged() const noexcept;

		/** */
		bool		isSYN() noexcept;

		/** */
		bool		isFIN() noexcept;

		/** */
		void		returnReadRequestNormal() noexcept;

		/** */
		void		returnReadRequestClosing() noexcept;

		/** */
		void		append(SegFragment& fragment) noexcept;

		/** */
		unsigned	remaining() const noexcept;

		/** This operation is called after initial construction
			to get the sequence number that should be
			assigned to the first fragment. This number is
			adjusted for SYN segments.
		 */
		unsigned long	firstFragmentSequenceNumber() noexcept;

		/** This operation should be called after all
			fragments are added to get sequence number
			that includes all sequence numbers in this
			segment, including SYN and FIN.
		 */
		unsigned long	nextSequenceNumber() noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* mem) noexcept;
	};

/** */
template <unsigned bufferSize>
struct SegmentMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Segment) >	segment;
	/** */
	Oscl::Memory::AlignedBlock<bufferSize>			buffer;
	};

}
}
}
}
}
}
}

#endif
