/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "trans.h"
#include "part.h"
#include "oscl/checksum/onescomp/b16/calculator.h"

using namespace Oscl::Protocol::IP::TCP::Conn::Srv::Server;

RstTrans::RstTrans(	Part&							context,
					Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::Protocol::IP::
					TX::Srv::Req::Api::SAP&			ipFwdSAP,
					unsigned long					peerIpAddress,
					unsigned long					localIpAddress,
					const Oscl::Protocol::IP::
					TCP::Header&					segTcpHeader,
					unsigned						ipPayloadLength
					) noexcept:
		_pdu(	*this,
				*this,
				&_pduMem,
				pduSize,
				pduSize
				),
		_context(context),
		_destAddr(peerIpAddress),
		_payload(	_pdu,
					_destAddr
					),
		_response(	ipFwdSAP.getReqApi(),
					*this,
					papi,
					_payload
					)
		{
	Oscl::Protocol::IP::Header*
	ipHeader	= (Oscl::Protocol::IP::Header*)&_pduMem;
	Oscl::Protocol::IP::TCP::Header*
	tcpHeader	= (Oscl::Protocol::IP::TCP::Header*)ipHeader->options;

	const unsigned	tcpHeaderLength	=	Oscl::Protocol::IP::
										TCP::Header::minHeaderSize;
	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;
	const unsigned	totalLength		= ipHeaderLength+tcpHeaderLength;
	const unsigned	tcpLength		= tcpHeaderLength;

	ipHeader->source.assign(localIpAddress);
	ipHeader->destination.assign(peerIpAddress);
	ipHeader->length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader->frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader->type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_TCP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader->updateChecksum(0);

	Oscl::Checksum::OnesComp::B16::Calculator	csum;

	// Pseudo header
	csum.accumulate(&ipHeader->source,sizeof(ipHeader->source));
	csum.accumulate(&ipHeader->destination,sizeof(ipHeader->destination));
	Oscl::Endian::Big::U16	uLength;
	uLength	= tcpLength;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_TCP;
	csum.accumulate(&protocol,sizeof(protocol));
	csum.accumulate(&uLength,sizeof(uLength));

	// TCP header
	tcpHeader->sourcePort		= segTcpHeader.destinationPort;
	tcpHeader->destinationPort	= segTcpHeader.sourcePort;
	if(segTcpHeader.isACK()){
		tcpHeader->sequenceNumber		= segTcpHeader.acknowlegementNumber;
		tcpHeader->acknowlegementNumber	= 0;
		tcpHeader->dataOffsetAndFlags	= 0x5004; // RST
		}
	else{
		tcpHeader->sequenceNumber		= 0;
		tcpHeader->acknowlegementNumber	= segTcpHeader.sequenceNumber + 1;
		tcpHeader->dataOffsetAndFlags	= 0x5014; // FIXME: RST,ACK
		}
	tcpHeader->window			= 0;
	tcpHeader->checksum			= 0;
	tcpHeader->urgentPointer	= 0;

	csum.accumulate(tcpHeader,tcpHeaderLength);

	uint16_t	checksum	= csum.final();
	if(!checksum){
		checksum	= ~checksum;
		}

	// Finish the TCP header
	tcpHeader->checksum			= checksum;

	ipFwdSAP.post(_response.getSrvMsg());
	}

void	RstTrans::response(	Oscl::Protocol::IP::
							TX::Srv::Resp::Api::SendResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	RstTrans::free(void* fso) noexcept{
	// Do nothing
	}

// SynAckTrans
SynAckTrans::SynAckTrans(	Part&							context,
							Oscl::Mt::Itc::PostMsgApi&		papi,
							Oscl::Protocol::IP::
							TX::Srv::Req::Api::SAP&			ipFwdSAP,
							unsigned long					peerIpAddress,
							unsigned long					localIpAddress,
							const Oscl::Protocol::IP::
							TCP::Header&					segTcpHeader,
							unsigned						ipPayloadLength
					) noexcept:
		_pdu(	*this,
				*this,
				&_pduMem,
				pduSize,
				pduSize
				),
		_context(context),
		_destAddr(peerIpAddress),
		_payload(	_pdu,
					_destAddr
					),
		_response(	ipFwdSAP.getReqApi(),
					*this,
					papi,
					_payload
					)
		{
	Oscl::Protocol::IP::Header*
	ipHeader	= (Oscl::Protocol::IP::Header*)&_pduMem;
	Oscl::Protocol::IP::TCP::Header*
	tcpHeader	= (Oscl::Protocol::IP::TCP::Header*)ipHeader->options;

	const unsigned	tcpHeaderLength	=	Oscl::Protocol::IP::
										TCP::Header::minHeaderSize;
	const unsigned	ipHeaderLength	= TypeLen::IHL::Value_Minimum<<2;
	const unsigned	totalLength		= ipHeaderLength+tcpHeaderLength;
	const unsigned	tcpLength		= tcpHeaderLength;

	ipHeader->source.assign(localIpAddress);
	ipHeader->destination.assign(peerIpAddress);
	ipHeader->length
		=	(		(4<<TypeLen::Version::Lsb)
				|	(TypeLen::IHL::ValueMask_Minimum)
				|	(TypeLen::TypeOfService::R::Value_Normal)
				|	(TypeLen::TypeOfService::T::ValueMask_Normal)
				|	(TypeLen::TypeOfService::D::ValueMask_Normal)
				|	((totalLength)<<TypeLen::TotalLength::Lsb)
				);
	ipHeader->frag
		=	(		(Oscl::Protocol::IP::getNewIpID()<<Frag::ID::Lsb)
				|	(Frag::Flags::MF::ValueMask_LastFragment)
				|	(Frag::Flags::DF::ValueMask_MayFragment)
				|	(0<<Frag::Offset::Lsb)
				);
	ipHeader->type
		=	(		(TtlProtoCheck::TimeToLive::ValueMask_Typical)
				|	(TtlProtoCheck::Protocol::ValueMask_TCP)
				|	(TtlProtoCheck::Checksum::ValueMask_Initial)
				);
	ipHeader->updateChecksum(0);

	Oscl::Checksum::OnesComp::B16::Calculator	csum;

	// Pseudo header
	csum.accumulate(&ipHeader->source,sizeof(ipHeader->source));
	csum.accumulate(&ipHeader->destination,sizeof(ipHeader->destination));
	Oscl::Endian::Big::U16	uLength;
	uLength	= tcpLength;
	Oscl::Endian::Big::U16	protocol;
	protocol	= TtlProtoCheck::Protocol::Value_TCP;
	csum.accumulate(&protocol,sizeof(protocol));
	csum.accumulate(&uLength,sizeof(uLength));

	// TCP header
	tcpHeader->sourcePort		= segTcpHeader.destinationPort;
	tcpHeader->destinationPort	= segTcpHeader.sourcePort;
	if(segTcpHeader.isACK()){
		tcpHeader->sequenceNumber		= segTcpHeader.acknowlegementNumber;
		tcpHeader->acknowlegementNumber	= 0;
		tcpHeader->dataOffsetAndFlags	= 0x5004; // RST
		}
	else{
		tcpHeader->sequenceNumber		= 0;
		tcpHeader->acknowlegementNumber	= segTcpHeader.sequenceNumber + 1;
		tcpHeader->dataOffsetAndFlags	= 0x5014; // FIXME: RST,ACK
		}
	tcpHeader->window			= 0;
	tcpHeader->checksum			= 0;
	tcpHeader->urgentPointer	= 0;

	csum.accumulate(tcpHeader,tcpHeaderLength);

	uint16_t	checksum	= csum.final();
	if(!checksum){
		checksum	= ~checksum;
		}

	// Finish the TCP header
	tcpHeader->checksum			= checksum;
	ipFwdSAP.post(_response.getSrvMsg());
	}

void	SynAckTrans::response(	Oscl::Protocol::IP::
								TX::Srv::Resp::Api::SendResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	SynAckTrans::free(void* fso) noexcept{
	// Do nothing
	}

