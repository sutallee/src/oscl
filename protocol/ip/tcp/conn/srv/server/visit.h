/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_visith_
#define _oscl_protocol_ip_tcp_conn_srv_server_visith_
#include "oscl/pdu/visitor.h"

/** */
namespace Oscl {

/** */
namespace Pdu {
	class Composite;
	class ReadOnlyComposite;
	class Leaf;
	class Fragment;
	class ReadOnlyFragment;
};

/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class Append : public Oscl::Pdu::ReadOnlyVisitor {
	private:
		/** */
		Oscl::Buffer::ReadWrite&	_destination;

	public:
		/** */
		Append(Oscl::Buffer::ReadWrite& destination) noexcept;

	public:
		/** This operation is called when a ReadOnlyComposite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(	const Oscl::Pdu::Composite&	p,
								unsigned					position
								) noexcept;

		/** This operation is called when a ReadOnlyComposite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
								unsigned							position
								) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client. The offset indexes
			the first valid octet in the leaf buffer for the current client.
			The length is the the number of valid octets beginning from
			the offset argument.
		 */
		bool /* stop */ accept(	const Oscl::Pdu::Leaf&	p,
								unsigned				position,
								unsigned				offset,
								unsigned				length
								) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(	const Oscl::Pdu::Fragment&	p,
								unsigned					position
								) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
								unsigned							position
								) noexcept;
	};

}
}
}
}
}
}
}

#endif
