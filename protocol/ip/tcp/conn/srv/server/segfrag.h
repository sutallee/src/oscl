/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_srv_server_segfragh_
#define _oscl_protocol_ip_tcp_conn_srv_server_segfragh_
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/modulo/u32.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace Srv {
/** */
namespace Server {

/** */
class Part;

/** */
class SegFragment : public Oscl::QueueItem {
	private:
		/** */
		Part&					_context;
		/** */
		Oscl::Stream::Output::
		Req::Api::WriteReq*		_msg;
		/** */
		const Oscl::Modulo::U32	_sequenceNumber;
		/** */
		unsigned				_nUnacknowledgedBytes;

	public:
		/** Buffer need not be cache aligned. */
		const void*		const	_buffer;
		/** */
		const unsigned			_offset;
		/** */
		const unsigned			_length;

	public:
		/** */
		SegFragment(	Part&					context,
						const void*				buffer,
						Oscl::Modulo::U32		sequenceNumber,
						unsigned				textOffset,
						unsigned				textLength,
						Oscl::Stream::Output::
						Req::Api::WriteReq*		msg=0
						) noexcept;

		/** This operation is invoked as ACK segments are received
			acknowledgements. Returns total number of bytes acknowledged.
			*/
		unsigned	ack(Oscl::Modulo::U32	acknowledgeNumber) noexcept;

		/** */
		bool		fullyAcknowledged() const noexcept;

		/** */
		void		returnReadRequestNormal() noexcept;

		/** */
		void		returnReadRequestClosing() noexcept;
	};

}
}
}
}
}
}
}

#endif
