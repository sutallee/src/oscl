/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
#include "closed.h"
#include "listen.h"
#include "synrcvd.h"
#include "synsent.h"
#include "estab.h"
#include "finwait1.h"
#include "closewait.h"
#include "finwait2.h"
#include "closing.h"
#include "lastack.h"
#include "timewait.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const static Closed		stateClosed;
const static Listen		stateListen;
const static SynRcvd	stateSynRcvd;
const static SynSent	stateSynSent;
const static Estab		stateEstab;
const static FinWait1	stateFinWait1;
const static CloseWait	stateCloseWait;
const static FinWait2	stateFinWait2;
const static Closing	stateClosing;
const static LastAck	stateLastAck;
const static TimeWait	stateTimeWait;

Var::Var(Context& context) noexcept:
		_context(context),
		_state(&stateClosed)
		{
	}

void	Var::open() noexcept{
	_state->open(_context,*this);
	}

void	Var::close() noexcept{
	_state->close(_context,*this);
	}

void	Var::send() noexcept{
	_state->send(_context,*this);
	}

void	Var::push() noexcept{
	_state->push(_context,*this);
	}

void	Var::normalRx() noexcept{
	_state->normalRx(_context,*this);
	}

void	Var::urgentRx() noexcept{
	_state->urgentRx(_context,*this);
	}

void	Var::segmentArrival() noexcept{
	NextStep	nextStep	= _state->segStage1(_context,*this);
	if(nextStep == Done) return;
	while(true){
		NextStep	prevStep	= nextStep;
		switch(nextStep){
			case Step1:
				nextStep	= _state->segStage2Step1(_context,*this);
				break;
			case Step2:
				nextStep	= _state->segStage2Step2(_context,*this);
				break;
			case Step3:
				nextStep	= _state->segStage2Step3(_context,*this);
				break;
			case Step4:
				nextStep	= _state->segStage2Step4(_context,*this);
				break;
			case Step5:
				nextStep	= _state->segStage2Step5(_context,*this);
				break;
			case Step6:
				nextStep	= _state->segStage2Step6(_context,*this);
				break;
			case Step7:
				nextStep	= _state->segStage2Step7(_context,*this);
				break;
			case Step8:
				nextStep	= _state->segStage2Step8(_context,*this);
				break;
			default:
				// Illegal STEP!
				// This should NEVER happen.
				for(;;);	// FIXME: use standard fatal error
				break;
			}
		if(nextStep == Done) break;
		if(nextStep <= prevStep){
			// Illegal! Cannot go back a step.
			// This should NEVER happen
			for(;;);	// FIXME: use standard fatal error
			}
		}
	}

void	Var::userTimeout() noexcept{
	_state->userTimeout(_context,*this);
	}

void	Var::retransmissionTimeout() noexcept{
	_state->retransmissionTimeout(_context,*this);
	}

void	Var::timeWaitTimeout() noexcept{
	_state->timeWaitTimeout(_context,*this);
	}

void	Var::tick() noexcept{
	_state->tick(_context,*this);
	}

void	Var::protocolError(	const char*	stateName,
							const char*	eventName
							){
	Oscl::ErrorFatal::logAndExit(
		"%s: %s\n",
		stateName,
		eventName
		);
	}

void	Var::changeToClosed() noexcept{
	_state	= &stateClosed;
	}

void	Var::changeToListen() noexcept{
	_state	= &stateListen;
	}

void	Var::changeToSynRcvd() noexcept{
	_state	= &stateSynRcvd;
	}

void	Var::changeToSynSent() noexcept{
	_state	= &stateSynSent;
	}

void	Var::changeToEstab() noexcept{
	_state	= &stateEstab;
	}

void	Var::changeToFinWait1() noexcept{
	_state	= &stateFinWait1;
	}

void	Var::changeToCloseWait() noexcept{
	_state	= &stateCloseWait;
	}

void	Var::changeToFinWait2() noexcept{
	_state	= &stateFinWait2;
	}

void	Var::changeToClosing() noexcept{
	_state	= &stateClosing;
	}

void	Var::changeToLastAck() noexcept{
	_state	= &stateLastAck;
	}

void	Var::changeToTimeWait() noexcept{
	_state	= &stateTimeWait;
	}

