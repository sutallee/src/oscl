/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_fsm_finwait2h_
#define _oscl_protocol_ip_tcp_conn_fsm_finwait2h_
#include "state.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace FSM {

/** */
class Var;

/** */
class Context;

/** */
class FinWait2 : public State {
	private:
		/** */
		const char*	getStateName() const noexcept;

		/** */
		void	normalRx(	Context&	context,
							Var&		var
							) const noexcept;

		/** */
		void	urgentRx(	Context&	context,
							Var&		var
							) const noexcept;

		/** */
		NextStep	segStage1(	Context&	context,
								Var&		var
								) const noexcept;
		/** */
		NextStep	segStage2Step1(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step2(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step3(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step4(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step5(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step6(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step7(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		NextStep	segStage2Step8(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		void	userTimeout(	Context&	context,
								Var&		var
								) const noexcept;

		/** */
		void	retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept;

		/** */
		void	timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		void	tick(	Context&	context,
						Var&		var
						) const noexcept;
	};

}
}
}
}
}
}

#endif
