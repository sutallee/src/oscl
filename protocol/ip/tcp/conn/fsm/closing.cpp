/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "closing.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	Closing::getStateName() const noexcept{
	return "Closing";
	}

void	Closing::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.returnRxRequestWithClosingError();
	}

void	Closing::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.returnRxRequestWithClosingError();
	}

NextStep	Closing::segStage1(	Context&	context,
								Var&		var
								) const noexcept{
	return Step1;
	}

NextStep	Closing::segStage2Step1(	Context&	context,
										Var&		var
										) const noexcept{
	// Acceptability
	// "If the RCV.WND is zero, no segments will be acceptable,
	// but special allowance shold be made to accept valid ACKs,
	// URGs and RSTs."

	Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();
	// first
	if(!isAcceptable(context)){
		if(context.isRST()){
			return Done;
			}
		if(!processACK(context)){
			return Done;
			}
		Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
		context.sendAck(sndNxt.getValue(),rcvNxt.getValue());
		return Done;
		}
	context.trimSegmentToFitWindow(); // See RFC793 page 69 paragraph 1
	return Step2;
	}

NextStep	Closing::segStage2Step2(	Context&	context,
										Var&		var
										) const noexcept{
	// second
	// The following part of "second" is the same for
	// CLOSING,LAST-ACK, and TIME-WAIT states.
	// Can you say subroutine?
	if(context.isRST()){
		var.changeToClosed();
		return Done;
		}
	return Step3;
	}

NextStep	Closing::segStage2Step3(	Context&	context,
										Var&		var
										) const noexcept{
	return Step4;
	}

NextStep	Closing::segStage2Step4(	Context&	context,
										Var&		var
										) const noexcept{
	// fourth
	// The following is the same for SYN-RECEIVED,
	// ESTABLISHED,FIN-WAIT-1,FIN-WAIT-2,COLSE-WAIT,CLOSING,LAST-ACK,
	// and TIME-WAIT states.
	if(context.isSYN()){
		context.resetOutstandingReceiveRequests();
		context.resetOutstandingTransmitRequests();
		context.flushAllSegmentQueues();
		context.signalConnectionReset();
		context.sendRst(context.getSegACK().getValue());
		var.changeToClosed();
		return Done;
		}
	return Step5;
	}

NextStep	Closing::segStage2Step5(	Context&	context,
										Var&		var
										) const noexcept{
	// fifth
	if(!processACK(context)){
		return Done;
		}
	if(context.finIsAcknowledged()){
		var.changeToTimeWait();
		}
	return Step6;
	}

NextStep	Closing::segStage2Step6(	Context&	context,
										Var&		var
										) const noexcept{
	if(context.isURG()){
		// Ignore the URG
		// This should NEVER happen.
		}
	return Step7;
	}

NextStep	Closing::segStage2Step7(	Context&	context,
										Var&		var
										) const noexcept{
	// Should not be any text to process.
	// Ignore the segment text.
	return Step8;
	}

NextStep	Closing::segStage2Step8(	Context&	context,
										Var&		var
										) const noexcept{
	if(!context.isFIN()) return Done;
	context.signalConnectionClosing();
	context.returnAllPendingReceives();
	// FIXME: Advance RCV.NXT over the FIN ?
	context.setRcvNxt(context.getRcvNxt()+context.getRcvWnd());	// FIXME:???
	context.sendAck(	context.getSndNxt().getValue(),	// FIXME: ??
						context.getRcvNxt().getValue()
						);
	return Done;
	}

void	Closing::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	Closing::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	}

void	Closing::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	Closing::tick(	Context&	context,
						Var&		var
						) const noexcept{
	context.stopUserTimer();
	}

