/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "finwait2.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	FinWait2::getStateName() const noexcept{
	return "FinWait2";
	}

void	FinWait2::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueNormalReceiveRequest();
	context.processQueuedReceiveSegments();
	}

void	FinWait2::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueUrgentReceiveRequest();
	context.processQueuedReceiveSegments();
	}

NextStep	FinWait2::segStage1(	Context&	context,
									Var&		var
									) const noexcept{
	return Step1;
	}

NextStep	FinWait2::segStage2Step1(	Context&	context,
										Var&		var
										) const noexcept{
	// "If the RCV.WND is zero, no segments will be acceptable,
	// but special allowance shold be made to accept valid ACKs,
	// URGs and RSTs."

	Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();
	// first
	if(!isAcceptable(context)){
		if(context.isRST()){
			return Done;
			}
		if(!processACK(context)){
			return Done;
			}
		Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
		context.sendAck(sndNxt.getValue(),rcvNxt.getValue());
		return Done;
		}
	context.trimSegmentToFitWindow(); // See RFC793 page 69 paragraph 1
	return Step2;
	}

NextStep	FinWait2::segStage2Step2(	Context&	context,
										Var&		var
										) const noexcept{
	// second
	// The following part of "second" is the same for
	// ESTABLISHED,FIN-WAIT-1,FIN-WAIT-2, and CLOSE-WAIT.
	// Can you say subroutine?
	if(context.isRST()){
		context.resetOutstandingReceiveRequests();
		context.resetOutstandingTransmitRequests();
		context.flushAllSegmentQueues();
		context.signalConnectionReset();
		var.changeToClosed();
		return Done;
		}
	return Step3;
	}

NextStep	FinWait2::segStage2Step3(	Context&	context,
										Var&		var
										) const noexcept{
	return Step4;
	}

NextStep	FinWait2::segStage2Step4(	Context&	context,
										Var&		var
										) const noexcept{
	// fourth
	// The following is the same for SYN-RECEIVED,
	// ESTABLISHED,FIN-WAIT-1,FIN-WAIT-2,COLSE-WAIT,CLOSING,LAST-ACK,
	// and TIME-WAIT states.
	if(context.isSYN()){
		context.resetOutstandingReceiveRequests();
		context.resetOutstandingTransmitRequests();
		context.flushAllSegmentQueues();
		context.signalConnectionReset();
		context.sendRst(context.getSegACK().getValue());
		var.changeToClosed();
		return Done;
		}
	return Step5;
	}

NextStep	FinWait2::segStage2Step5(	Context&	context,
										Var&		var
										) const noexcept{
	// fifth
	if(!processACK(context)){
		return Done;
		}
	if(context.retransmissionQueueIsEmpty()){
		context.acknowledgeUserClose();
		}
	return Step6;
	}

NextStep	FinWait2::segStage2Step6(	Context&	context,
										Var&		var
										) const noexcept{
	// sixth
	if(context.isURG()){
		unsigned long	rcvUp	= context.getRcvUp();
		unsigned long	segUp	= context.getSegUP();
		if(rcvUp > segUp){
			context.setRcvUp(rcvUp);
			}
		else {
			context.setRcvUp(segUp);
			}
		context.signalUrgentDataIfRequired();
		}
	return Step7;
	}

NextStep	FinWait2::segStage2Step7(	Context&	context,
										Var&		var
										) const noexcept{
	// seventh
	unsigned	nBytesDelivered	= context.deliverTextToRxBuffers();
	if(!nBytesDelivered){
		return Step8;
		}
	context.setRcvNxt(context.getRcvNxt()+nBytesDelivered);
	context.adjustWindow(nBytesDelivered);
	// ??? "The total of RCV.NXT and RCV.WND should not be reduced.
	context.sendPiggybackAck(	context.getSndNxt().getValue(),
								context.getRcvNxt().getValue()
								);
	return Step8;
	}

NextStep	FinWait2::segStage2Step8(	Context&	context,
										Var&		var
										) const noexcept{
	context.startTimeWaitTimer();
	context.stopUserTimer();
	context.stopRetransmissionTimer();
	var.changeToTimeWait();
	return Done;
	}

void	FinWait2::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	FinWait2::retransmissionTimeout(	Context&	context,
											Var&		var
											) const noexcept{
	}

void	FinWait2::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	FinWait2::tick(	Context&	context,
						Var&		var
						) const noexcept{
	context.stopUserTimer();
	}
