/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "synsent.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	SynSent::getStateName() const noexcept{
	return "SynSent";
	}

void	SynSent::close(	Context&	context,
						Var&		var
						) const noexcept{
	context.returnAllPendingReceives();
	context.returnAllPendingSends();
	context.deleteTheTCB();
	var.changeToClosed();
	}

void	SynSent::send(	Context&	context,
						Var&		var
						) const noexcept{
	context.queueSendRequest();
	}

void	SynSent::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueNormalReceiveRequest();
	}

void	SynSent::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueUrgentReceiveRequest();
	}

NextStep	SynSent::segStage1(	Context&	context,
								Var&		var
								) const noexcept{
	// first
	bool	isACK			= context.isACK();
	bool	ackIsAcceptable	= false;
	if(isACK){
		Oscl::Modulo::U32	iss		= context.getISS();
		Oscl::Modulo::U32	segAck	= context.getSegACK();
		Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
		Oscl::Modulo::U32	sndUna	= context.getSndUna();
		if((segAck <= iss) || (segAck > sndNxt)){
			if(context.isRST()){
				return Done;
				}
			context.sendRst(segAck.getValue());
			return Done;
			}
		ackIsAcceptable	= ((sndUna <= segAck) && (segAck <= sndNxt));
		}
	// second
	if(context.isRST()){
		if(ackIsAcceptable){
			// Signal the user "error: connection reset"
			var.changeToClosed();
			}
		return Done;
		}
	// third
	if(!context.securityCompartmentMatches()){
		if(isACK){
			context.sendRst(context.getSegACK().getValue());
			}
		else{
			context.sendRstAck(	0,
								(		context.getSegACK()
									+	context.getSegLEN()
									).getValue()
								);
			}
		return Done;
		}
	// fourth
	if(!context.isSYN()){
		// fifth
		return Done;
		}
	Oscl::Modulo::U32	segSeq	= context.getSegSEQ();
	Oscl::Modulo::U32	segAck	= context.getSegACK();
	context.setRcvNxt(segSeq+1);
	context.setIRS(segSeq);
	if(isACK){
		context.setSndUna(segAck.getValue());
		context.acknowledgeRetransmissionQueue();
		}
	Oscl::Modulo::U32	sndUna	= context.getSndUna();
	Oscl::Modulo::U32	iss		= context.getISS();
	if(sndUna > iss){	// FIXME: modulo
		Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
		Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();
		context.sendAck(sndNxt.getValue(),rcvNxt.getValue());
		var.changeToEstab();
		context.returnOpenReq();
		// "Data or controls which were queued for
		// transmission *may* be included. If there are other
		// controls or text in the segment then continue processing
		// at the sixth step below where the URG bit is checked,
		// otherwise return.
		return Step6;
		}
	else {
		Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();
		context.sendSynAck(iss.getValue(),rcvNxt.getValue());
		var.changeToSynRcvd();
		// FIXME: "If there are other controls or text in the
		// segment, queue them for processing after the ESTABLISHED
		// state has been reached..."
		return Step2;
		}
	// fifth
	return Done;	// unreachable?
	}

void	SynSent::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	SynSent::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	}

void	SynSent::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	SynSent::tick(	Context&	context,
						Var&		var
						) const noexcept{
	context.stopUserTimer();
	}

