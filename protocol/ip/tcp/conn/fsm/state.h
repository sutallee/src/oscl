/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_fsm_stateh_
#define _oscl_protocol_ip_tcp_conn_fsm_stateh_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace FSM {

/** */
class Var;

/** */
class Context;

typedef enum {
	Step1,
	Step2,
	Step3,
	Step4,
	Step5,
	Step6,
	Step7,
	Step8,
	Done,
	NoImp	// Temporary to allow compile, remove when ready
	} NextStep;

/** */
class State {
	public:
		/** */
		virtual ~State(){}

		/** */
		virtual const char*	getStateName() const noexcept=0;

		/** */
		virtual void	open(	Context&	context,
								Var&		var
								) const noexcept;

		/** */
		virtual void	close(	Context&	context,
								Var&		var
								) const noexcept;

		/** */
		virtual void	send(	Context&	context,
								Var&		var
								) const noexcept;

		/** */
		virtual void	push(	Context&	context,
								Var&		var
								) const noexcept;

		/** */
		virtual void	normalRx(	Context&	context,
									Var&		var
									) const noexcept;
		/** */
		virtual void	urgentRx(	Context&	context,
									Var&		var
									) const noexcept;

		/** Return true to stop */
		virtual NextStep	segStage1(	Context&	context,
										Var&		var
										) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step1(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step2(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step3(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step4(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step5(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step6(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step7(	Context&	context,
											Var&		var
											) const noexcept;
		/** Return true to stop */
		virtual NextStep	segStage2Step8(	Context&	context,
											Var&		var
											) const noexcept;

		/** */
		virtual void	userTimeout(	Context&	context,
										Var&		var
										) const noexcept;

		/** */
		virtual void	retransmissionTimeout(	Context&	context,
												Var&		var
												) const noexcept;

		/** */
		virtual void	timeWaitTimeout(	Context&	context,
											Var&		var
											) const noexcept;
		/** */
		virtual void	tick(	Context&	context,
								Var&		var
								) const noexcept;
	protected:
		/** */
		bool	isAcceptable(Context& context) const noexcept;
		/** */
		bool /* continue */	processACK(Context& context) const noexcept;
		/** */
		bool /* continue */ checkSecurity(Context& context) const noexcept;
	};

}
}
}
}
}
}

#endif
