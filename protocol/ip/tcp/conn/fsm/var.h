/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_fsm_varh_
#define _oscl_protocol_ip_tcp_conn_fsm_varh_
#include "context.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace FSM {

/** */
class State;
/** */
class Closed;
/** */
class Listen;
/** */
class SynRcvd;
/** */
class SynSent;
/** */
class Estab;
/** */
class FinWait1;
/** */
class CloseWait;
/** */
class FinWait2;
/** */
class Closing;
/** */
class LastAck;
/** */
class TimeWait;

/** */
class Var {
	private:
		/** */
		Context&		_context;
		/** */
		const State*	_state;

	public:
		/** */
		Var(Context& context) noexcept;

		/** */
		void	open() noexcept;

		/** */
		void	close() noexcept;

		/** */
		void	send() noexcept;

		/** */
		void	push() noexcept;

		/** */
		void	normalRx() noexcept;

		/** */
		void	urgentRx() noexcept;

		/** */
		void	segmentArrival() noexcept;

		/** */
		void	userTimeout() noexcept;

		/** */
		void	retransmissionTimeout() noexcept;

		/** */
		void	timeWaitTimeout() noexcept;

		/** */
		void	tick() noexcept;

	private:
		/** */
		friend class State;
		friend class Closed;
		friend class Listen;
		friend class SynRcvd;
		friend class SynSent;
		friend class Estab;
		friend class FinWait1;
		friend class CloseWait;
		friend class FinWait2;
		friend class Closing;
		friend class LastAck;
		friend class TimeWait;

		/** */
		void	protocolError(	const char*	stateName,
								const char*	eventName
								);

		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToListen() noexcept;
		/** */
		void	changeToSynRcvd() noexcept;
		/** */
		void	changeToSynSent() noexcept;
		/** */
		void	changeToEstab() noexcept;
		/** */
		void	changeToFinWait1() noexcept;
		/** */
		void	changeToCloseWait() noexcept;
		/** */
		void	changeToFinWait2() noexcept;
		/** */
		void	changeToClosing() noexcept;
		/** */
		void	changeToLastAck() noexcept;
		/** */
		void	changeToTimeWait() noexcept;
	};

}
}
}
}
}
}

#endif
