/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "listen.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	Listen::getStateName() const noexcept{
	return "Listen";
	}

void	Listen::open(	Context&	context,
						Var&		var
						) const noexcept{
	if(context.isPassiveOpen()){
		Oscl::Modulo::U32	iss	= context.getNewISS();
		context.setActiveOpen();
		context.setISS(iss);
		context.setSndUna(iss);
		context.setSndNxt(iss+1);
		context.sendSyn(iss.getValue());
		var.changeToSynSent();
		}
	}

void	Listen::close(	Context&	context,
						Var&		var
						) const noexcept{
	context.returnAllPendingReceives();
	context.deleteTheTCB();
	var.changeToClosed();
	}

void	Listen::send(	Context&	context,
						Var&		var
						) const noexcept{
	if(context.isPassiveOpen()){
		context.returnSendReqWithNotOpenError();
		return;
		}
	context.queueSendRequest();
	}

void	Listen::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueNormalReceiveRequest();
	}

void	Listen::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueUrgentReceiveRequest();
	}

NextStep	Listen::segStage1(	Context&	context,
							Var&		var
							) const noexcept{
	if(context.isRST()){
		return Done;
		}
	if(context.isACK()){
		context.sendRst(context.getSegACK().getValue());
		return Done;
		}
	if(!context.isSYN()){
		return Done;
		}
	if(!context.securityCompartmentMatches()){
		context.sendRst(context.getSegACK().getValue());
		return Done;
		}
	Oscl::Modulo::U32	segSEQ	= context.getSegSEQ();
	context.setRcvNxt(segSEQ+1);
	context.setIRS(segSEQ);
	Oscl::Modulo::U32	iss	= context.getNewISS();
	context.sendSynAck(	iss.getValue(),
						context.getRcvNxt().getValue()
						);
	context.setSndNxt(iss+1);
	context.setSndUna(iss);
	context.setSndWnd(context.getSegWND());
	context.startUserTimer();
	var.changeToSynRcvd();
	// "Note that any other incomming control or
	// data (combined with SYN) will be processed
	// in the SYN-RECEIVED state, but processing of
	// SYN and ACK should not be repeated."
	// Huh???
	context.copyNewConnectionValuesFromSegment();
	return Step1;
	}

NextStep	Listen::segStage2Step1(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step2(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step3(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step4(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step5(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step6(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step7(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

NextStep	Listen::segStage2Step8(	Context&	context,
									Var&		var
									) const noexcept{
	return NoImp;
	}

void	Listen::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	Listen::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	}

void	Listen::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	Listen::tick(	Context&	context,
						Var&		var
						) const noexcept{
	context.stopUserTimer();
	}
