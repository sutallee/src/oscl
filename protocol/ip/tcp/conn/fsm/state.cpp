/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "state.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

void	State::open(	Context&	context,
						Var&		var
						) const noexcept{
	var.protocolError(getStateName(),"open");
	}

void	State::close(	Context&	context,
						Var&		var
						) const noexcept{
	var.protocolError(getStateName(),"close");
	}

void	State::send(	Context&	context,
						Var&		var
						) const noexcept{
	var.protocolError(getStateName(),"send");
	}

void	State::push(	Context&	context,
						Var&		var
						) const noexcept{
	var.protocolError(getStateName(),"send");
	}

void	State::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	var.protocolError(getStateName(),"normalRx");
	}

void	State::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	var.protocolError(getStateName(),"urgentRx");
	}

NextStep	State::segStage1(	Context&	context,
								Var&		var
								) const noexcept{
	var.protocolError(getStateName(),"segStage1");
	return Done;
	}

NextStep	State::segStage2Step1(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step1");
	return Done;
	}

NextStep	State::segStage2Step2(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step2");
	return Done;
	}

NextStep	State::segStage2Step3(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step3");
	return Done;
	}

NextStep	State::segStage2Step4(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step4");
	return Done;
	}

NextStep	State::segStage2Step5(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step5");
	return Done;
	}

NextStep	State::segStage2Step6(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step6");
	return Done;
	}

NextStep	State::segStage2Step7(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step7");
	return Done;
	}

NextStep	State::segStage2Step8(	Context&	context,
									Var&		var
									) const noexcept{
	var.protocolError(getStateName(),"segStage2Step8");
	return Done;
	}

void	State::userTimeout(	Context&	context,
							Var&		var
							) const noexcept{
	var.protocolError(getStateName(),"userTimeout");
	}

void	State::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	var.protocolError(getStateName(),"retransmissionTimeout");
	}

void	State::timeWaitTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	var.protocolError(getStateName(),"timeWaitTimeout");
	}

void	State::tick(	Context&	context,
						Var&		var
						) const noexcept{
	var.protocolError(getStateName(),"tick");
	}

// This operation returns true if the text contained in
// the segment is within the expected receive window.
// It makes no determination about the acknowledgements.
// Perhaps it should be called "containsNewText()".
// RFC793 indicates that the purpose of this test
// is to discard old duplicates.
bool	State::isAcceptable(Context& context) const noexcept{
	Oscl::Modulo::U32	segSeq	= context.getSegSEQ();
	Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();
	Oscl::Modulo::U32	rcvWnd	= context.getRcvWnd();
	Oscl::Modulo::U32	segLen	= context.getSegLEN();

	if(segLen.getValue() == 0){
		if(rcvWnd.getValue() == 0){
			return (segSeq == rcvNxt);
			}
		else {
			return (		(rcvNxt <= segSeq)
						&&	(segSeq < (rcvNxt+rcvWnd))
						);	
			}
		}
	else if(rcvWnd.getValue() != 0){
		return (		(		(rcvNxt <= segSeq)
							&&	(segSeq < (rcvNxt+rcvWnd))
							)
					||	(		(rcvNxt <= (segSeq+segLen-1))
							&&	((segSeq+segLen-1) < (rcvNxt+rcvWnd))
							)
					);
		}
	else {
		return false;
		}
	}

bool /* continue */	State::processACK(Context& context) const noexcept{
	if(!context.isACK()){
		return false;
		}
	Oscl::Modulo::U32	sndUna	= context.getSndUna();
	Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
	Oscl::Modulo::U32	segAck	= context.getSegACK();
	bool			segmentAcceptable	= ((sndUna <= segAck) && (segAck <= sndNxt));
	if(segmentAcceptable){
		context.setSndUna(segAck);
		if(segAck > sndNxt){
			// Ack of something not yet sent.
			for(;;);
			context.sendAck(0,0); // What SEQ,ACK to use?
			return false;
			}
		if(segAck < sndUna){
			// Duplicate ACK
			}
		else {
			context.acknowledgeRetransmissionQueue();
			}
		}
	if((sndUna < segAck) && (segAck <= sndNxt)){
		Oscl::Modulo::U32	sndWl1	= context.getSndWl1();
		Oscl::Modulo::U32	sndWl2	= context.getSndWl2();
		Oscl::Modulo::U32	segSeq	= context.getSegSEQ();
		if((sndWl1 < segSeq) || (((sndWl1 == segSeq)) && (sndWl2 <= segAck))){
			unsigned long	segWnd	= context.getSegWND();
			context.setSndWnd(segWnd);
			context.setSndWl1(segSeq);
			context.setSndWl2(segAck);
			}
		}
	return true;
	}

bool /* continue */ State::checkSecurity(Context& context) const noexcept{
	if(!context.securityCompartmentMatches()){
		context.resetOutstandingReceiveRequests();
		context.resetOutstandingTransmitRequests();
		context.flushAllSegmentQueues();
		context.signalConnectionReset();
		context.sendRst(context.getSegACK().getValue());
		return false;
		}
	return true;
	}
