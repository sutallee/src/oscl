/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "closed.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	Closed::getStateName() const noexcept{
	return "Closed";
	}

void	Closed::open(	Context&	context,
						Var&		var
						) const noexcept{
	if(context.isPassiveOpen()){
		context.listening();
		var.changeToListen();
		}
	else {
		Oscl::Modulo::U32	iss	= context.getNewISS();
		context.setISS(iss);
		context.setSndUna(iss);
		context.setSndNxt(iss+1);
		context.sendSyn(iss.getValue());
		var.changeToSynSent();
		}
	}

void	Closed::send(	Context&	context,
						Var&		var
						) const noexcept{
	context.returnSendReqWithNotOpenError();
	}

NextStep	Closed::segStage1(	Context&	context,
								Var&		var
								) const noexcept{
	if(context.isACK()){
		context.sendRstAck(	0,
							(context.getSegSEQ()+context.getSegLEN()).getValue()
							);
		}
	else{
		context.sendRst(context.getSegACK().getValue());
		}
	return Done;
	}

void	Closed::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	Closed::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	}

void	Closed::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	Closed::tick(	Context&	context,
						Var&		var
						) const noexcept{
	context.stopUserTimer();
	}

