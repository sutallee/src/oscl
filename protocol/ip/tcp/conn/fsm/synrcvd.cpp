/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "synrcvd.h"
#include "context.h"
#include "var.h"

using namespace Oscl::Protocol::IP::TCP::Conn::FSM;

const char*	SynRcvd::getStateName() const noexcept{
	return "SynRcvd";
	}

void	SynRcvd::close(	Context&	context,
						Var&		var
						) const noexcept{
	if(!context.segmentsHaveBeenSent()){
		context.sendFin();
		var.changeToFinWait1();
		return;
		}
	context.queueCloseRequest();
	}

void	SynRcvd::send(	Context&	context,
						Var&		var
						) const noexcept{
	context.queueSendRequest();
	}

void	SynRcvd::normalRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueNormalReceiveRequest();
	}

void	SynRcvd::urgentRx(	Context&	context,
							Var&		var
							) const noexcept{
	context.queueUrgentReceiveRequest();
	}

NextStep	SynRcvd::segStage1(	Context&	context,
								Var&		var
								) const noexcept{
	return Step1;
	}

NextStep	SynRcvd::segStage2Step1(	Context&	context,
										Var&		var
										) const noexcept{
	// "If the RCV.WND is zero, no segments will be acceptable,
	// but special allowance shold be made to accept valid ACKs,
	// URGs and RSTs."

	Oscl::Modulo::U32	rcvNxt	= context.getRcvNxt();

	// first
	if(!isAcceptable(context)){
		if(context.isRST()){
			return Done;
			}
		if(!context.securityCompartmentMatches()){
			context.sendRst(context.getSegACK().getValue());
			return Done;
			}
		if(!processACK(context)){
			return Done;
			}
		Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
		context.sendAck(sndNxt.getValue(),rcvNxt.getValue());
		return Done;
		}
	context.trimSegmentToFitWindow(); // See RFC793 page 69 paragraph 1
	return Step2;
	}

NextStep	SynRcvd::segStage2Step2(	Context&	context,
										Var&		var
										) const noexcept{
	// second
	if(context.isRST()){
		context.removeAllFromRetransmissionQueue();
		if(context.isPassiveOpen()){
			context.listening();
			var.changeToListen();
			}
		else {
			var.changeToClosed();
			}
		return Done;
		}
	return Step3;
	}

NextStep	SynRcvd::segStage2Step3(	Context&	context,
										Var&		var
										) const noexcept{
	// third
	if(!context.securityCompartmentMatches()){
		context.sendRst(context.getSegACK().getValue());
		return Done;
		}
	return Step4;
	}

NextStep	SynRcvd::segStage2Step4(	Context&	context,
										Var&		var
										) const noexcept{
	// fourth
	// The following is the same for SYN-RECEIVED,
	// ESTABLISHED,FIN-WAIT-1,FIN-WAIT-2,COLSE-WAIT,CLOSING,LAST-ACK,
	// and TIME-WAIT states.
	if(context.isSYN()){
		context.resetOutstandingReceiveRequests();
		context.resetOutstandingTransmitRequests();
		context.flushAllSegmentQueues();
		context.signalConnectionReset();
		context.sendRst(context.getSegACK().getValue());
		var.changeToClosed();
		return Done;
		}
	return Step5;
	}

NextStep	SynRcvd::segStage2Step5(	Context&	context,
										Var&		var
										) const noexcept{
	// fifth
	if(!context.isACK()){
		return Done;
		}
	Oscl::Modulo::U32	sndUna	= context.getSndUna();
	Oscl::Modulo::U32	sndNxt	= context.getSndNxt();
	Oscl::Modulo::U32	segAck	= context.getSegACK();
	bool	segmentAcceptable	= ((sndUna <= segAck) && (segAck <= sndNxt));
	if(!segmentAcceptable){
		// Segment not acceptable?
		// RFC page 71 paragraph ~5
		context.sendRst(segAck.getValue());
		return Done;	// FIXME: Should I continue?
						// It would *seem* NOT.
		}
	var.changeToEstab();
	context.returnOpenReq();
	if(!processACK(context)){
		return Done;
		}
	return Step6;	// changed to Established before this
	}

void	SynRcvd::userTimeout(	Context&	context,
								Var&		var
								) const noexcept{
	}

void	SynRcvd::retransmissionTimeout(	Context&	context,
										Var&		var
										) const noexcept{
	}

void	SynRcvd::timeWaitTimeout(	Context&	context,
									Var&		var
									) const noexcept{
	}

void	SynRcvd::tick(	Context&	context,
						Var&		var
						) const noexcept{
	if(!context.userTimerIsExpired()) return;
	context.stopUserTimer();
	context.flushAllSegmentQueues();
	context.resetOutstandingReceiveRequests();
	context.resetOutstandingTransmitRequests();
	var.changeToClosed();
	context.deleteTheTCB();
	context.returnOpenReq();
	}
