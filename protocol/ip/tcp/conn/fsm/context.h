/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_conn_fsm_contexth_
#define _oscl_protocol_ip_tcp_conn_fsm_contexth_
#include "oscl/modulo/u32.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {
/** */
namespace Conn {
/** */
namespace FSM {

/** */
class Context {
	public:
		/** */
		virtual ~Context(){}

		/** */
		virtual void	sendRstAck(	unsigned long	seq,
									unsigned long	ack
									) noexcept=0;
		virtual void	sendSyn(unsigned long 	seq) noexcept=0;
		virtual void	sendFin() noexcept=0;
		virtual void	sendSynAck(	unsigned long	seq,
									unsigned long	ack
									) noexcept=0;
		virtual void	sendRst(unsigned long 	seq) noexcept=0;
		virtual void	sendAck(	unsigned long	seq,
									unsigned long	ack
									) noexcept=0;
		virtual void	sendPiggybackAck(	unsigned long	seq,
											unsigned long	ack
											) noexcept=0;
		/** Send queued requests as resources and flow control allow,
			with piggyback acknowledgment etc. Context is responsible
			for returning completed send requests to user after each
			is segmentized transmitted and placed in the retransmission
			queue.
		 */
		virtual void	sendQueuedPiggyback() noexcept=0;
		/** If there are any queued received segments and there
			are queued receive requests, satisfy as many receive
			requests with the queued segments as possible, and
			return completed requests.
		 */
		virtual void	processQueuedReceiveSegments() noexcept=0;
		/** Return current rx request with a closing error. */
		virtual void	returnRxRequestWithClosingError() noexcept=0;
		virtual void	queueSendRequest() noexcept=0;
		virtual void	queueNormalReceiveRequest() noexcept=0;
		virtual void	queueUrgentReceiveRequest() noexcept=0;
		virtual void	queuePushRequest() noexcept=0;
		virtual void	queueCloseRequest() noexcept=0;
		virtual void	copyNewConnectionValuesFromSegment() noexcept=0;
		virtual void	acknowledgeRetransmissionQueue() noexcept=0;
		virtual void	trimSegmentToFitWindow() noexcept=0; // See RFC793 page 69 paragraph 1
		virtual void	removeAllFromRetransmissionQueue() noexcept=0;
		virtual void	resetOutstandingReceiveRequests() noexcept=0;
		virtual void	resetOutstandingTransmitRequests() noexcept=0;
		/** All pending receive requests are returned with
			closing error.
		 */
		virtual void	returnAllPendingReceives() noexcept=0;
		virtual void	returnAllPendingSends() noexcept=0;
		virtual void	flushAllSegmentQueues() noexcept=0;
		virtual void	signalConnectionClosing() noexcept=0;
		virtual void	signalConnectionReset() noexcept=0;
		virtual void	signalUrgentData() noexcept=0;
		virtual void	signalUrgentDataIfRequired() noexcept=0;
		virtual void	adjustWindow(unsigned nBytesReceived) noexcept=0;
		virtual void	startTimeWaitTimer() noexcept=0;
		virtual void	stopTimeWaitTimer() noexcept=0;
		virtual void	startUserTimer() noexcept=0;
		virtual void	stopUserTimer() noexcept=0;
		virtual void	startRetransmissionTimer() noexcept=0;
		virtual void	stopRetransmissionTimer() noexcept=0;
		virtual void	acknowledgeUserClose() noexcept=0;
		virtual void	deleteTheTCB() noexcept=0;
		virtual void	setActiveOpen() noexcept=0;
		virtual void	returnSendReqWithNotOpenError() noexcept=0;
		virtual unsigned	deliverTextToRxBuffers() noexcept=0;
		virtual void	returnOpenReq() noexcept=0;
		virtual void	listening() noexcept=0;

		virtual bool	userTimerIsExpired() noexcept=0;

		/** SND */
		virtual void	setSndUna(const Oscl::Modulo::U32& value) noexcept=0;
		virtual void	setSndNxt(const Oscl::Modulo::U32& value) noexcept=0;
		virtual void	setSndWnd(unsigned long value) noexcept=0;
		virtual void	setSndUp(unsigned long value) noexcept=0;
		virtual void	setSndWl1(const Oscl::Modulo::U32& value) noexcept=0;
		virtual void	setSndWl2(const Oscl::Modulo::U32& value) noexcept=0;
		virtual void	setISS(const Oscl::Modulo::U32& value) noexcept=0;

		/** RCV */
		virtual void	setRcvNxt(const Oscl::Modulo::U32& value) noexcept=0;
		virtual void	setRcvWnd(unsigned long value) noexcept=0;
		virtual void	setRcvUp(unsigned long value) noexcept=0;
		virtual void	setIRS(const Oscl::Modulo::U32& value) noexcept=0;

		/** */
		virtual bool	isACK() const noexcept=0;
		virtual bool	isRST() const noexcept=0;
		virtual bool	isSYN() const noexcept=0;
		virtual bool	isURG() const noexcept=0;
		virtual bool	isFIN() const noexcept=0;
		virtual bool	securityCompartmentMatches() const noexcept=0;
		virtual bool	isPassiveOpen() const noexcept=0;
		virtual bool	finIsAcknowledged() const noexcept=0;
		/** Returns true if any segments have been transmitted. */
		virtual bool	segmentsHaveBeenSent() const noexcept=0;
		virtual bool	sendRequestQueueIsEmpty() const noexcept=0;
		virtual bool	closeRequestQueueIsEmpty() const noexcept=0;
		virtual bool	pendingRxSegmentQueueIsEmpty() const noexcept=0;
		virtual bool	retransmissionQueueIsEmpty() const noexcept=0;

		/** */
		virtual Oscl::Modulo::U32	getSegSEQ() const noexcept=0;
		/** */
		virtual unsigned long		getSegLEN() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getSegACK() const noexcept=0;
		/** */
		virtual unsigned long		getSegUP() const noexcept=0;
		/** */
		virtual unsigned long		getSegWND() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getNewISS() const noexcept=0;

		/** */
		virtual Oscl::Modulo::U32	getSndUna() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getSndNxt() const noexcept=0;
		/** */
		virtual unsigned long		getSndWnd() const noexcept=0;
		/** */
		virtual unsigned long		getSndUp() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getSndWl1() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getSndWl2() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getISS() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getRcvNxt() const noexcept=0;
		/** */
		virtual unsigned long		getRcvWnd() const noexcept=0;
		/** */
		virtual unsigned long		getRcvUp() const noexcept=0;
		/** */
		virtual Oscl::Modulo::U32	getIRS() const noexcept=0;
	};

}
}
}
}
}
}

#endif
