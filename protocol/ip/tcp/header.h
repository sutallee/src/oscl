/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_tcp_headerh_
#define _oscl_protocol_ip_tcp_headerh_
#include "oscl/endian/type.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace TCP {

struct DataOffsetAndFlags {
	/** */
	Oscl::BitField<uint16_t>	dataOffsetAndFlags;
	/** */
	inline DataOffsetAndFlags(uint16_t dOffsetAndFlags) noexcept
			{
		dataOffsetAndFlags	= dOffsetAndFlags;
		}
	/** */
	inline unsigned char	getDataOffsetIn32BitWords() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		word	&= 0xF000;
		word	>>= 12;
		return (unsigned char)word;
		}
	/** */
	inline unsigned		getHeaderLength() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		word	&= 0xF000;
		word	>>= 10;
		return word;
		}
	/** */
	inline bool	isFIN() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<0));
		}
	/** */
	inline bool	isSYN() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<1));
		}
	/** */
	inline bool	isRST() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<2));
		}
	/** */
	inline bool	isPSH() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<3));
		}
	/** */
	inline bool	isACK() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<4));
		}
	/** */
	inline bool	isURG() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<5));
		}

	/** */
	inline void	setFIN() noexcept{
		dataOffsetAndFlags.setBits(1<<0);
		}
	/** */
	inline void	setSYN() noexcept{
		dataOffsetAndFlags.setBits(1<<1);
		}
	/** */
	inline void	setRST() noexcept{
		dataOffsetAndFlags.setBits(1<<2);
		}
	/** */
	inline void	setPSH() noexcept{
		dataOffsetAndFlags.setBits(1<<3);
		}
	/** */
	inline void	setACK() noexcept{
		dataOffsetAndFlags.setBits(1<<4);
		}
	/** */
	inline void	setURG() noexcept{
		dataOffsetAndFlags.setBits(1<<5);
		}

	/** */
	inline void	clearFIN() noexcept{
		dataOffsetAndFlags.clearBits(1<<0);
		}
	/** */
	inline void	clearSYN() noexcept{
		dataOffsetAndFlags.clearBits(1<<1);
		}
	/** */
	inline void	clearRST() noexcept{
		dataOffsetAndFlags.clearBits(1<<2);
		}
	/** */
	inline void	clearPSH() noexcept{
		dataOffsetAndFlags.clearBits(1<<3);
		}
	/** */
	inline void	clearACK() noexcept{
		dataOffsetAndFlags.clearBits(1<<4);
		}
	/** */
	inline void	clearURG() noexcept{
		dataOffsetAndFlags.clearBits(1<<5);
		}

	/** */
	inline void	clearAllFlags() noexcept{
		dataOffsetAndFlags.clearBits(0x001F);
		}
	};

/** */
struct Header {
	/** */
	Oscl::Endian::Big::U16	sourcePort;
	/** */
	Oscl::Endian::Big::U16	destinationPort;
	/** */
	Oscl::Endian::Big::U32	sequenceNumber;
	/** */
	Oscl::Endian::Big::U32	acknowlegementNumber;
	/** */
	Oscl::Endian::Big::U16	dataOffsetAndFlags;
	/** */
	Oscl::Endian::Big::U16	window;
	/** */
	Oscl::Endian::Big::U16	checksum;
	/** */
	Oscl::Endian::Big::U16	urgentPointer;
	/** */
	enum{minHeaderSize=0x05*sizeof(uint32_t)};
	/** */
	enum{maxHeaderSize=0x0F*sizeof(uint32_t)};
	/** */
	enum{maxOptionSize=maxHeaderSize-minHeaderSize};
	/** */
	unsigned char			options[maxOptionSize];
	/** */
	inline unsigned char	getDataOffsetIn32BitWords() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		word	&= 0xF000;
		word	>>= 12;
		return (unsigned char)word;
		}
	/** */
	inline	bool	headerLengthIsValid() const noexcept{
		unsigned char	headerLength	= getDataOffsetIn32BitWords();
		const unsigned char	minHeaderWords	= 5;
		return (headerLength >= minHeaderWords);
		}
	/** */
	inline unsigned		getHeaderLength() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		word	&= 0xF000;
		word	>>= 10;
		return word;
		}
	/** */
	inline unsigned		getOptionsLength() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		word	&= 0xF000;
		word	>>= 10;
		return word-minHeaderSize;
		}
	/** Returns prevOffset if there are no more options. */
	inline unsigned		nextOptionOffset(unsigned prevOffset) const noexcept{
		unsigned		i;
		unsigned char	length;
		for(i=prevOffset;i<maxOptionSize;++i){
			switch(options[i]){
				case 0:		// End of options
					return prevOffset;
				case 1:		// Noop
					continue;
				default:	// All others
					length	= options[i+1];
					return i+length;
				};
			}
		return prevOffset;
		}
	/** */
	inline unsigned	getDataOffset() const noexcept{
		return getDataOffsetIn32BitWords()*4;
		}
	/** */
	inline unsigned char*	getDataPointer() noexcept{
		return &options[getOptionsLength()];
		}
	/** */
	inline bool	isFIN() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<0));
		}
	/** */
	inline bool	isSYN() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<1));
		}
	/** */
	inline bool	isRST() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<2));
		}
	/** */
	inline bool	isPSH() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<3));
		}
	/** */
	inline bool	isACK() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<4));
		}
	/** */
	inline bool	isURG() const noexcept{
		uint16_t	word	= dataOffsetAndFlags;
		return (word & (1<<5));
		}

	/** */
	inline void	setFIN() noexcept{
		dataOffsetAndFlags.setBits(1<<0);
		}
	/** */
	inline void	setSYN() noexcept{
		dataOffsetAndFlags.setBits(1<<1);
		}
	/** */
	inline void	setRST() noexcept{
		dataOffsetAndFlags.setBits(1<<2);
		}
	/** */
	inline void	setPSH() noexcept{
		dataOffsetAndFlags.setBits(1<<3);
		}
	/** */
	inline void	setACK() noexcept{
		dataOffsetAndFlags.setBits(1<<4);
		}
	/** */
	inline void	setURG() noexcept{
		dataOffsetAndFlags.setBits(1<<5);
		}

	/** */
	inline void	clearFIN() noexcept{
		dataOffsetAndFlags.clearBits(1<<0);
		}
	/** */
	inline void	clearSYN() noexcept{
		dataOffsetAndFlags.clearBits(1<<1);
		}
	/** */
	inline void	clearRST() noexcept{
		dataOffsetAndFlags.clearBits(1<<2);
		}
	/** */
	inline void	clearPSH() noexcept{
		dataOffsetAndFlags.clearBits(1<<3);
		}
	/** */
	inline void	clearACK() noexcept{
		dataOffsetAndFlags.clearBits(1<<4);
		}
	/** */
	inline void	clearURG() noexcept{
		dataOffsetAndFlags.clearBits(1<<5);
		}

	/** */
	inline void	clearAllFlags() noexcept{
		dataOffsetAndFlags.clearBits(0x001F);
		}
	};

}
}
}
}

#endif
