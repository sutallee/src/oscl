/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _protocol_ip_headerh_
#define _protocol_ip_headerh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Protocol { // Namespace description
		namespace IP { // Namespace description
			namespace TypeLen { // Register description
				typedef uint32_t	Reg;
				namespace Version { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0xF0000000};
					};
				namespace IHL { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x0F000000};
					enum {Value_Minimum = 0x5};
					enum {ValueMask_Minimum = 0x05000000};
					enum {Value_Maximum = 0xF};
					enum {ValueMask_Maximum = 0x0F000000};
					};
				namespace TypeOfService { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					namespace R { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_High = 0x1};
						enum {ValueMask_High = 0x00040000};
						};
					namespace T { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_High = 0x1};
						enum {ValueMask_High = 0x00080000};
						};
					namespace D { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Low = 0x1};
						enum {ValueMask_Low = 0x00100000};
						};
					};
				namespace TotalLength { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace TypeLen { // Register description
				typedef uint32_t	Reg;
				};
			namespace Frag { // Register description
				typedef uint32_t	Reg;
				namespace ID { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					};
				namespace Flags { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x0000E000};
					namespace MF { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_LastFragment = 0x0};
						enum {ValueMask_LastFragment = 0x00000000};
						enum {Value_MoreFragments = 0x1};
						enum {ValueMask_MoreFragments = 0x00002000};
						};
					namespace DF { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_MayFragment = 0x0};
						enum {ValueMask_MayFragment = 0x00000000};
						enum {Value_DontFragment = 0x1};
						enum {ValueMask_DontFragment = 0x00004000};
						};
					};
				namespace Offset { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					};
				};
			namespace TtlProtoCheck { // Register description
				typedef uint32_t	Reg;
				namespace TimeToLive { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					enum {Value_Typical = 0x3C};
					enum {ValueMask_Typical = 0x3C000000};
					};
				namespace Protocol { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					enum {Value_ICMP = 0x1};
					enum {ValueMask_ICMP = 0x00010000};
					enum {Value_IGMP = 0x2};
					enum {ValueMask_IGMP = 0x00020000};
					enum {Value_GGP = 0x3};
					enum {ValueMask_GGP = 0x00030000};
					enum {Value_IP = 0x4};
					enum {ValueMask_IP = 0x00040000};
					enum {Value_ST = 0x5};
					enum {ValueMask_ST = 0x00050000};
					enum {Value_TCP = 0x6};
					enum {ValueMask_TCP = 0x00060000};
					enum {Value_UCL = 0x7};
					enum {ValueMask_UCL = 0x00070000};
					enum {Value_EGP = 0x8};
					enum {ValueMask_EGP = 0x00080000};
					enum {Value_IGP = 0x9};
					enum {ValueMask_IGP = 0x00090000};
					enum {Value_BbnRccMon = 0xA};
					enum {ValueMask_BbnRccMon = 0x000A0000};
					enum {Value_NVPII = 0xB};
					enum {ValueMask_NVPII = 0x000B0000};
					enum {Value_PUP = 0xC};
					enum {ValueMask_PUP = 0x000C0000};
					enum {Value_ARGUS = 0xD};
					enum {ValueMask_ARGUS = 0x000D0000};
					enum {Value_EMCON = 0xE};
					enum {ValueMask_EMCON = 0x000E0000};
					enum {Value_XNET = 0xF};
					enum {ValueMask_XNET = 0x000F0000};
					enum {Value_CHAOS = 0x10};
					enum {ValueMask_CHAOS = 0x00100000};
					enum {Value_UDP = 0x11};
					enum {ValueMask_UDP = 0x00110000};
					enum {Value_MUX = 0x12};
					enum {ValueMask_MUX = 0x00120000};
					enum {Value_DcnMeas = 0x13};
					enum {ValueMask_DcnMeas = 0x00130000};
					enum {Value_HMP = 0x14};
					enum {ValueMask_HMP = 0x00140000};
					enum {Value_PRM = 0x15};
					enum {ValueMask_PRM = 0x00150000};
					enum {Value_XnsIdp = 0x16};
					enum {ValueMask_XnsIdp = 0x00160000};
					enum {Value_Trunk1 = 0x17};
					enum {ValueMask_Trunk1 = 0x00170000};
					enum {Value_Trunk2 = 0x18};
					enum {ValueMask_Trunk2 = 0x00180000};
					enum {Value_Leaf1 = 0x19};
					enum {ValueMask_Leaf1 = 0x00190000};
					enum {Value_Leaf2 = 0x1A};
					enum {ValueMask_Leaf2 = 0x001A0000};
					enum {Value_RDP = 0x1B};
					enum {ValueMask_RDP = 0x001B0000};
					enum {Value_IRTP = 0x1C};
					enum {ValueMask_IRTP = 0x001C0000};
					enum {Value_IsoTp4 = 0x1D};
					enum {ValueMask_IsoTp4 = 0x001D0000};
					enum {Value_NETBLT = 0x1E};
					enum {ValueMask_NETBLT = 0x001E0000};
					enum {Value_MfeNsp = 0x1F};
					enum {ValueMask_MfeNsp = 0x001F0000};
					enum {Value_MeritInp = 0x20};
					enum {ValueMask_MeritInp = 0x00200000};
					enum {Value_SEP = 0x21};
					enum {ValueMask_SEP = 0x00210000};
					enum {Value__3PC = 0x22};
					enum {ValueMask__3PC = 0x00220000};
					enum {Value_IDPR = 0x23};
					enum {ValueMask_IDPR = 0x00230000};
					enum {Value_XTP = 0x24};
					enum {ValueMask_XTP = 0x00240000};
					enum {Value_DDP = 0x25};
					enum {ValueMask_DDP = 0x00250000};
					enum {Value_IdprCmtp = 0x26};
					enum {ValueMask_IdprCmtp = 0x00260000};
					enum {Value_TPPP = 0x27};
					enum {ValueMask_TPPP = 0x00270000};
					enum {Value_IL = 0x28};
					enum {ValueMask_IL = 0x00280000};
					enum {Value_AHIP = 0x3D};
					enum {ValueMask_AHIP = 0x003D0000};
					enum {Value_CFTP = 0x3E};
					enum {ValueMask_CFTP = 0x003E0000};
					enum {Value_ALN = 0x3F};
					enum {ValueMask_ALN = 0x003F0000};
					enum {Value_SatExpak = 0x40};
					enum {ValueMask_SatExpak = 0x00400000};
					enum {Value_KRYPTOLAN = 0x41};
					enum {ValueMask_KRYPTOLAN = 0x00410000};
					enum {Value_RVD = 0x42};
					enum {ValueMask_RVD = 0x00420000};
					enum {Value_IPPC = 0x43};
					enum {ValueMask_IPPC = 0x00430000};
					enum {Value_ADFS = 0x44};
					enum {ValueMask_ADFS = 0x00440000};
					enum {Value_SatMon = 0x45};
					enum {ValueMask_SatMon = 0x00450000};
					enum {Value_VISA = 0x46};
					enum {ValueMask_VISA = 0x00460000};
					enum {Value_IPCV = 0x47};
					enum {ValueMask_IPCV = 0x00470000};
					enum {Value_CPNX = 0x48};
					enum {ValueMask_CPNX = 0x00480000};
					enum {Value_CPHB = 0x49};
					enum {ValueMask_CPHB = 0x00490000};
					enum {Value_WSN = 0x4A};
					enum {ValueMask_WSN = 0x004A0000};
					enum {Value_PVP = 0x4B};
					enum {ValueMask_PVP = 0x004B0000};
					enum {Value_BrSatMon = 0x4C};
					enum {ValueMask_BrSatMon = 0x004C0000};
					enum {Value_SunNd = 0x4D};
					enum {ValueMask_SunNd = 0x004D0000};
					enum {Value_WbMon = 0x4E};
					enum {ValueMask_WbMon = 0x004E0000};
					enum {Value_WbExpak = 0x4F};
					enum {ValueMask_WbExpak = 0x004F0000};
					enum {Value_IsoIP = 0x50};
					enum {ValueMask_IsoIP = 0x00500000};
					enum {Value_VMTP = 0x51};
					enum {ValueMask_VMTP = 0x00510000};
					enum {Value_SecureVmtp = 0x52};
					enum {ValueMask_SecureVmtp = 0x00520000};
					enum {Value_VINES = 0x53};
					enum {ValueMask_VINES = 0x00530000};
					enum {Value_TTP = 0x54};
					enum {ValueMask_TTP = 0x00540000};
					enum {Value_NfsnetIgp = 0x55};
					enum {ValueMask_NfsnetIgp = 0x00550000};
					enum {Value_DGP = 0x56};
					enum {ValueMask_DGP = 0x00560000};
					enum {Value_TCF = 0x57};
					enum {ValueMask_TCF = 0x00570000};
					enum {Value_IGRP = 0x58};
					enum {ValueMask_IGRP = 0x00580000};
					enum {Value_OSPFIGP = 0x59};
					enum {ValueMask_OSPFIGP = 0x00590000};
					enum {Value_SpriteRpc = 0x5A};
					enum {ValueMask_SpriteRpc = 0x005A0000};
					enum {Value_LARP = 0x5B};
					enum {ValueMask_LARP = 0x005B0000};
					enum {Value_MTP = 0x5C};
					enum {ValueMask_MTP = 0x005C0000};
					enum {Value_AX25 = 0x5D};
					enum {ValueMask_AX25 = 0x005D0000};
					enum {Value_IPIP = 0x5E};
					enum {ValueMask_IPIP = 0x005E0000};
					enum {Value_MICP = 0x5F};
					enum {ValueMask_MICP = 0x005F0000};
					enum {Value_AesSp3D = 0x60};
					enum {ValueMask_AesSp3D = 0x00600000};
					enum {Value_ETHERIP = 0x61};
					enum {ValueMask_ETHERIP = 0x00610000};
					enum {Value_ENCAP = 0x62};
					enum {ValueMask_ENCAP = 0x00620000};
					};
				namespace Checksum { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_Initial = 0x0};
					enum {ValueMask_Initial = 0x00000000};
					};
				};
			};
		};
	};
#endif
