/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_mux_srv_registrar_parth_
#define _oscl_protocol_ip_mux_srv_registrar_parth_
#include "oscl/protocol/ip/mux/registrar/acceptor.h"
#include "oscl/protocol/ip/rx/api.h"
#include "oscl/queue/queue.h"
#include "oscl/protocol/ip/mux/registrar/srv/sync.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Mux {
/** */
namespace Srv {
/** */
namespace Registrar {

/** */
class Part :	public	Oscl::Protocol::IP::RX::Api,
				private	Oscl::Protocol::IP::Mux::Registrar::Srv::Req::Api
				{
	private:
		/** */
		Oscl::Queue<	Oscl::Protocol::IP::
						Mux::Registrar::Acceptor
						>								_acceptorList;
		/** */
		Oscl::Protocol::IP::
		Mux::Registrar::Srv::Req::Api::ConcreteSAP		_sap;

		/** */
		Oscl::Protocol::IP::Mux::Registrar::Srv::Sync	_sync;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi) noexcept;
		/** */
		virtual ~Part(){}

		/** */
		Oscl::Protocol::IP::
		Mux::Registrar::Srv::Req::Api::SAP&			getSAP() noexcept;

		/** */
		Oscl::Protocol::IP::Mux::Registrar::Api&	getSyncApi() noexcept;

	private: // Oscl::Protocol::IP::RX::Api
		void	transfer(	Oscl::Pdu::Pdu&				ipPDU,
							Oscl::Protocol::IP::Header&	ipHeader
							) noexcept;
	public:	// Req::Api
		/** */
		void	request(	Oscl::Protocol::IP::
							Mux::Registrar::Srv::Req::Api::AttachReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Protocol::IP::
							Mux::Registrar::Srv::Req::Api::DetachReq&	msg
							) noexcept;
	};

}
}
}
}
}
}

#endif
