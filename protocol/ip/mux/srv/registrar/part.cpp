/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Protocol::IP::Mux::Srv::Registrar;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi) noexcept:
		_sap(*this,myPapi),
		_sync(_sap)
		{
	}

Oscl::Protocol::IP::Mux::Registrar::Srv::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

Oscl::Protocol::IP::Mux::Registrar::Api&		Part::getSyncApi() noexcept{
	return _sync;
	}

void	Part::transfer(	Oscl::Pdu::Pdu&				ipPDU,
						Oscl::Protocol::IP::Header&	ipHeader
						) noexcept{
	ipPDU.stripHeader(ipHeader.getHeaderLength()<<2);

	Oscl::Protocol::IP::Mux::Registrar::Acceptor*	next;
	for(	next	= _acceptorList.first();
			next;
			next	= _acceptorList.next(next)
			){
		if(next->accept(ipPDU,ipHeader)){
			return;
			}
		}
	}

void	Part::request(	Oscl::Protocol::IP::
						Mux::Registrar::Srv::Req::Api::AttachReq&	msg
						) noexcept{
	_acceptorList.put(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Protocol::IP::
						Mux::Registrar::Srv::Req::Api::DetachReq&	msg
						) noexcept{
	_acceptorList.remove(&msg.getPayload()._acceptor);
	msg.returnToSender();
	}

