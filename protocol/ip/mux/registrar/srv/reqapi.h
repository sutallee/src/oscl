/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_mux_registrar_srv_reqapih_
#define _oscl_protocol_ip_mux_registrar_srv_reqapih_
#include "oscl/protocol/ip/mux/registrar/acceptor.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Mux {
/** */
namespace Registrar {
/** */
namespace Srv {
/** */
namespace Req {

/** */
class AttachPayload {
	public:
		/** */
		Oscl::Protocol::IP::
		Mux::Registrar::Acceptor&	_acceptor;
	public:
		/** */
		AttachPayload(	Oscl::Protocol::IP::
						Mux::Registrar::Acceptor&	acceptor
						) noexcept:
			_acceptor(acceptor){}
	};

/** */
class DetachPayload {
	public:
		/** */
		Oscl::Protocol::IP::
		Mux::Registrar::Acceptor&	_acceptor;
	public:
		/** */
		DetachPayload(	Oscl::Protocol::IP::
						Mux::Registrar::Acceptor&	acceptor
						) noexcept:
			_acceptor(acceptor){}
	};

/**	These operations may be invoked while the receiver is operating.
	Typically attachment happens after the sycophant receiver is
	opened, and detachment happens before the sycophant reciever is
	closed.
 */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Req::Api,AttachPayload>	AttachReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Req::Api,DetachPayload>	DetachReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Req::Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Req::Api>	ConcreteSAP;

	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	request(AttachReq& msg) noexcept=0;
		/** */
		virtual void	request(DetachReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
