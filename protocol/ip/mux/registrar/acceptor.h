/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ip_mux_registrar_acceptorh_
#define _oscl_protocol_ip_mux_registrar_acceptorh_
#include "oscl/protocol/ip/header.h"
#include "oscl/queue/queueitem.h"
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IP {
/** */
namespace Mux {
/** */
namespace Registrar {

/** */
class Acceptor :	public Oscl::QueueItem {
	public:
		/** Shut-up GCC. */
		virtual ~Acceptor() {}
		/** The implementation must copy the header or parts thereof
			if necessary. The implementation returns true to keep
			the PDU and false otherwise. The implementation must
			return true if it would accept the PDU but does not
			currently have the resources. If the PDU is accepted
			the implementation must reference the PDU through
			a Handle<Composite>.
			
		 */
		virtual bool	accept(	Oscl::Pdu::Pdu&						pdu,
								const Oscl::Protocol::IP::Header&	header
								) noexcept=0;
	};

}
}
}
}
}

#endif

