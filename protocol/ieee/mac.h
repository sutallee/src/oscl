/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ieee_mach_
#define _oscl_protocol_ieee_mach_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IEEE {

/**  Represents a 48bit IEEE Mac address. Octets of the
	address are ordered as they are transmitted on the wire.
	The LSB of each octet is transmitted first. The first
	octet is transmitted first and the sixth octet is
	transmitted last.
	No constructor allowed. This is to allow the type
	to be used in packet structures unions. The MacAddressInit
	type is a subclass that allows for initialized instantiation.
 */
struct MacAddress {
	public:
		/** */
		enum{nOctets=6};
		/** */
		enum{nBits=8*nOctets};
	public:
		/** */
		uint8_t	_addr[nOctets];
#if 1
	public:
		/** */
		inline uint8_t	firstOctet() const noexcept{
			return _addr[0];
			}
		/** */
		inline uint8_t	secondOctet() const noexcept{
			return _addr[1];
			}
		/** */
		inline uint8_t	thirdOctet() const noexcept{
			return _addr[2];
			}
		/** */
		inline uint8_t	fourthOctet() const noexcept{
			return _addr[3];
			}
		/** */
		inline uint8_t	fifthOctet() const noexcept{
			return _addr[4];
			}
		/** */
		inline uint8_t	sixthOctet() const noexcept{
			return _addr[5];
			}
	public:
		/** */
		inline void	firstOctet(uint8_t octet) noexcept{
			_addr[0]	= octet;
			}
		/** */
		inline void	secondOctet(uint8_t octet) noexcept{
			_addr[1]	= octet;
			}
		/** */
		inline void	thirdOctet(uint8_t octet) noexcept{
			_addr[2]	= octet;
			}
		/** */
		inline void	fourthOctet(uint8_t octet) noexcept{
			_addr[3]	= octet;
			}
		/** */
		inline void	fifthOctet(uint8_t octet) noexcept{
			_addr[4]	= octet;
			}
		/** */
		inline void	sixthOctet(uint8_t octet) noexcept{
			_addr[5]	= octet;
			}
#endif
#if 0
		/** */
        inline MacAddress&    operator =(const MacAddress& other) noexcept{
			_addr[0] 	= other._addr[0];
			_addr[1] 	= other._addr[1];
			_addr[2] 	= other._addr[2];
			_addr[3] 	= other._addr[3];
			_addr[4] 	= other._addr[4];
			_addr[5] 	= other._addr[5];
			return *this;
			}
#endif
        inline bool    operator ==(const MacAddress& other) noexcept{
			return(		(_addr[0] == other._addr[0])
					&&	(_addr[1] == other._addr[1])
					&&	(_addr[2] == other._addr[2])
					&&	(_addr[3] == other._addr[3])
					&&	(_addr[4] == other._addr[4])
					&&	(_addr[5] == other._addr[5])
					);
			}
	};

/** Adds constructor to MacAddress class. */
class MacAddressInit : public MacAddress {
	public:
		/** */
		MacAddressInit(	uint8_t	first,
						uint8_t	second,
						uint8_t	third,
						uint8_t	fourth,
						uint8_t	fifth,
						uint8_t	sixth
						) noexcept;
	};
}
}
}

#endif
