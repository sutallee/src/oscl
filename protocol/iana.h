/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_ianah_
#define _oscl_protocol_ianah_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IANA {

/** */
typedef enum {
	/** */
	XeroxPup			= 0x0200,
	/** */
	PupAddrTrans201		= 0x0201,
	/** */
	XeroxNsIdp			= 0x0600,
	/** */
	DodIp				= 0x0800,
	/** Same as DodIp above */
	IPv4				= 0x0800,
	/** */
	X75Internet			= 0x0801,
	/** */
	NbsInternet			= 0x0802,
	/** */
	EcmaInternet		= 0x0803,
	/** */
	Chaosnet			= 0x0804,
	/** */
	X25Level3			= 0x0805,
	/** */
	ARP					= 0x0806,
	/** */
	XnsCompatability	= 0x0807,
	/** */
	SymbolicsPrivate	= 0x081C,
	/** */
	Xyplex8				= 0x081C,
	/** */
	Xyplex9				= 0x0889,
	/** */
	XyplexA				= 0x088A,
	/** */
	UngermannBassNetDebugger	= 0x0900,
	/** */
	ZeroxIEEE802_3Pup	= 0x0A00,
	/** */
	PUPAddrTransA01		= 0x0A01,
	/** */
	BanyanSystems		= 0x0BAD,
	/** */
	BerkeleyTrailerNego	= 0x1000,
	/** */
	BerkeleyTrailerEncap1	= 0x1001,
	/** */
	BerkeleyTrailerEncap2	= 0x1002,
	/** */
	BerkeleyTrailerEncap3	= 0x1003,
	/** */
	BerkeleyTrailerEncap4	= 0x1004,
	/** */
	BerkeleyTrailerEncap5	= 0x1005,
	/** */
	BerkeleyTrailerEncap6	= 0x1006,
	/** */
	BerkeleyTrailerEncap7	= 0x1007,
	/** */
	BerkeleyTrailerEncap8	= 0x1008,
	/** */
	BerkeleyTrailerEncap9	= 0x1009,
	/** */
	BerkeleyTrailerEncapA	= 0x100A,
	/** */
	BerkeleyTrailerEncapB	= 0x100B,
	/** */
	BerkeleyTrailerEncapC	= 0x100C,
	/** */
	BerkeleyTrailerEncapD	= 0x100D,
	/** */
	BerkeleyTrailerEncapE	= 0x100E,
	/** */
	BerkeleyTrailerEncapF	= 0x100F,
	/** */
	ValidSystems			= 0x1600,
	/** */
	PcsBasicBlockProtocol	= 0x4242,
	/** */
	BbnSimnet				= 0x5208,
	/** */
	DecUnassigned0			= 0x6000,
	/** */
	DecMopDumpLoad			= 0x6001,
	/** */
	DecMopRemoteConsole		= 0x6002,
	/** */
	DecDecnetPhaseIVRoute	= 0x6003,
	/** */
	DecLat					= 0x6004,
	/** */
	DecDiagProtocol			= 0x6005,
	/** */
	DecCustomerProtocol		= 0x6006,
	/** */
	DecLavcSca				= 0x6007,
	/** */
	DecUnassigned8			= 0x6008,
	/** */
	DecUnassigned9			= 0x6009,
	/** */
	_3ComCorp0				= 0x6010,
	/** */
	_3ComCorp1				= 0x6011,
	/** */
	_3ComCorp2				= 0x6012,
	/** */
	_3ComCorp3				= 0x6013,
	/** */
	_3ComCorp4				= 0x6014,
	/** */
	UngermannBassDownload	= 0x7000,
	/** */
	UngermannBassDiaLoop	= 0x7002,
	/** */
	LRT0					= 0x7020,
	/** */
	LRT1					= 0x7021,
	/** */
	LRT2					= 0x7022,
	/** */
	LRT3					= 0x7023,
	/** */
	LRT4					= 0x7024,
	/** */
	LRT5					= 0x7025,
	/** */
	LRT6					= 0x7026,
	/** */
	LRT7					= 0x7027,
	/** */
	LRT8					= 0x7028,
	/** */
	LRT9					= 0x7029,
	/** */
	Proteon					= 0x7030,
	/** */
	Cabletron				= 0x7034,
	/** */
	CronusVln				= 0x8003,
	/** */
	CronusDirect			= 0x8004,
	/** */
	HpProbe					= 0x8005,
	/** */
	Nestar					= 0x8006,
	/** */
	ATT						= 0x8008,
	/** */
	Excelan					= 0x8010,
	/** */
	SgiDiagnostics			= 0x8013,
	/** */
	SgiNetworkGames			= 0x8014,
	/** */
	SgiReserved				= 0x8015,
	/** */
	SgiBounceServer			= 0x8016,
	/** */
	ApolloComputers			= 0x8019,
	/** */
	Tymshare				= 0x802E,
	/** */
	Tigan					= 0x802F,
	/** */
	ReverseARP				= 0x8035,
	/** */
	AeonicSystems			= 0x8036,
	/** */
	DecLanBridge			= 0x8038,
	/** */
	DecUnassigned8039		= 0x8039,
	/** */
	DecUnassigned803A		= 0x803A,
	/** */
	DecUnassigned803B		= 0x803B,
	/** */
	DecUnassigned803C		= 0x803C,
	/** */
	PlanningResearchCorp	= 0x8044,
	/** */
	ATT8046					= 0x8046,
	/** */
	ATT8047					= 0x8047,
	/** */
	ExperData				= 0x8049,
	/** */
	StanfordVKernelExp		= 0x805B,
	/** */
	StanfordVKernelProd		= 0x805C,
	/** */
	EvansSutherland			= 0x805D,
	/** */
	LittleMachines			= 0x8060,
	/** */
	CounterpointComputers	= 0x8062,
	/** */
	UnivOfMassAtAmherst5	= 0x8065,
	/** */
	UnivOfMassAtAmherst6	= 0x8066,
	/** */
	VeecoIntegratedAuto		= 0x8067,
	/** */
	GeneralDynamics			= 0x8068,
	/** */
	ATT8069					= 0x8069,
	/** */
	Autophon				= 0x806A,
	/** */
	ComDesign				= 0x806C,
	/** */
	Computgraphic			= 0x806D,
	/** */
	LandmarkGraphicsE		= 0x806E,
	/** */
	LandmarkGraphicsF		= 0x806F,
	/** */
	LandmarkGraphics0		= 0x8070,
	/** */
	LandmarkGraphics1		= 0x8071,
	/** */
	LandmarkGraphics2		= 0x8072,
	/** */
	LandmarkGraphics3		= 0x8073,
	/** */
	LandmarkGraphics4		= 0x8074,
	/** */
	LandmarkGraphics5		= 0x8075,
	/** */
	LandmarkGraphics6		= 0x8076,
	/** */
	LandmarkGraphics7		= 0x8077,
	/** */
	Matra					= 0x807A,
	/** */
	DanskDataElektronik		= 0x807B,
	/** */
	MeritInternodal			= 0x807C,
	/** */
	VitalinkCommunicationsD	= 0x807D,
	/** */
	VitalinkCommunicationsE	= 0x807E,
	/** */
	VitalinkCommunicationsF	= 0x807F,
	/** */
	VitalinkTransLANIII		= 0x8080,
	/** */
	CounterpointComputers1	= 0x8081,
	/** */
	CounterpointComputers2	= 0x8082,
	/** */
	CounterpointComputers3	= 0x8083,
	/** */
	Appletalk				= 0x809B,
	/** */
	Datability809C			= 0x809C,
	/** */
	Datability809D			= 0x809D,
	/** */
	Datability809E			= 0x809E,
	/** */
	SpiderSystems			= 0x809F,
	/** */
	NixdorfComputers		= 0x80A3,
	/** */
	SiemensGammasonics4		= 0x80A4,
	/** */
	SiemensGammasonics5		= 0x80A5,
	/** */
	SiemensGammasonics6		= 0x80A6,
	/** */
	SiemensGammasonics7		= 0x80A7,
	/** */
	SiemensGammasonics8		= 0x80A8,
	/** */
	SiemensGammasonics9		= 0x80A9,
	/** */
	SiemensGammasonicsA		= 0x80AA,
	/** */
	SiemensGammasonicsB		= 0x80AB,
	/** */
	SiemensGammasonicsC		= 0x80AC,
	/** */
	SiemensGammasonicsD		= 0x80AD,
	/** */
	SiemensGammasonicsE		= 0x80AE,
	/** */
	SiemensGammasonicsF		= 0x80AF,
	/** */
	SiemensGammasonics0		= 0x80B0,
	/** */
	SiemensGammasonics1		= 0x80B1,
	/** */
	SiemensGammasonics2		= 0x80B2,
	/** */
	SiemensGammasonics3		= 0x80B3,
	/** */
	DcaDataExchangeCluster0	= 0x80C0,
	/** */
	DcaDataExchangeCluster1	= 0x80C1,
	/** */
	DcaDataExchangeCluster2	= 0x80C2,
	/** */
	DcaDataExchangeCluster3	= 0x80C3,
	/** */
	PacerSoftware			= 0x80C6,
	/** */
	Applitek				= 0x80C7,
	/** */
	Intergraph8				= 0x80C8,
	/** */
	Intergraph9				= 0x80C9,
	/** */
	IntergraphA				= 0x80CA,
	/** */
	IntergraphB				= 0x80CB,
	/** */
	IntergraphC				= 0x80CC,
	/** */
	HarrisD					= 0x80CD,
	/** */
	HarrisE					= 0x80CE,
	/** */
	TaylorInstrumentF		= 0x80CF,
	/** */
	TaylorInstrument0		= 0x80D0,
	/** */
	TaylorInstrument1		= 0x80D1,
	/** */
	TaylorInstrument2		= 0x80D2,
	/** */
	Rosemount3				= 0x80D3,
	/** */
	Rosemount4				= 0x80D4,
	/** */
	IbmSnaServiceOnEther	= 0x80D5,
	/** */
	VarianAssociates		= 0x80DD,
	/** */
	IntegratedSolutionsE	= 0x80DE,
	/** */
	IntegratedSolutionsF	= 0x80DF,
	/** */
	AllenBradley0			= 0x80E0,
	/** */
	AllenBradley1			= 0x80E1,
	/** */
	AllenBradley2			= 0x80E2,
	/** */
	AllenBradley3			= 0x80E3,
	/** */
	Datability80E4			= 0x80E4,
	/** */
	Datability80E5			= 0x80E5,
	/** */
	Datability80E6			= 0x80E6,
	/** */
	Datability80E7			= 0x80E7,
	/** */
	Datability80E8			= 0x80E8,
	/** */
	Datability80E9			= 0x80E9,
	/** */
	Datability80EA			= 0x80EA,
	/** */
	Datability80EB			= 0x80EB,
	/** */
	Datability80EC			= 0x80EC,
	/** */
	Datability80ED			= 0x80ED,
	/** */
	Datability80EE			= 0x80EE,
	/** */
	Datability80EF			= 0x80EF,
	/** */
	Datability80F0			= 0x80F0,
	/** */
	Retix					= 0x80F2,
	/** */
	AppletalkAarp			= 0x80F3,
	/** */
	Kinetics4				= 0x80F4,
	/** */
	Kinetics5				= 0x80F5,
	/** */
	ApolloComputer80F7		= 0x80F7,
	/** */
	WellfleetComm80FF		= 0x80FF,
	/** */
	WellfleetComm8100		= 0x8100,
	/** */
	WellfleetComm8101		= 0x8101,
	/** */
	WellfleetComm8102		= 0x8102,
	/** */
	WellfleetComm8103		= 0x8103,
	/** */
	SymbolicsPrivate7		= 0x8107,
	/** */
	SymbolicsPrivate8		= 0x8108,
	/** */
	SymbolicsPrivate9		= 0x8109,
	/** */
	WaterlooMicro			= 0x8130,
	/** */
	VgLaboratorySystems		= 0x8131,
	/** */
	Novell7					= 0x8137,
	/** */
	Novell8					= 0x8138,
	/** */
	KTI9					= 0x8139,
	/** */
	KTIA					= 0x813A,
	/** */
	KTIB					= 0x813B,
	/** */
	KTIC					= 0x813C,
	/** */
	KTID					= 0x813D,
	/** */
	SNMP					= 0x814C,
	/** */
	IPv6					= 0x86DD,
	/** */
	Loopback				= 0x9000,
	/** */
	_3ComXnsSysMgmt			= 0x9001,
	/** */
	_3ComTcpIpSys			= 0x9002,
	/** */
	_3ComLoopDetect			= 0x9003,
	/** */
	BbnVitalLanBridgeCache	= 0xFF00
	} Value;

}
}
}

#endif
