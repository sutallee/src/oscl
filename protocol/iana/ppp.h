/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_iana_ppph_
#define _oscl_protocol_iana_ppph_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IANA {
/** */
namespace PPP {

// RFC1700
enum {
	PaddingProtocol									=0x0001,
	InternetProtocol								=0x0021,
	OSINetworkLayer									=0x0023,
	XeroxNSIDP										=0x0025,
	DECnetPhaseIV									=0x0027,
	Appletalk										=0x0029,
	NovellIPX										=0x002b,
	VanJacobsonCompressedTCP_IP						=0x002d,
	VanJacobsonUncompressedTCP_IP					=0x002f,
	BridgingPDU										=0x0031,
	StreamProtocolST_II								=0x0033,
	BanyanVines										=0x0035,
	AppleTalkEDDP									=0x0039,
	AppleTalkSmartBuffered							=0x003b,
	MultiLink										=0x003d,
	NETBIOSFraming									=0x003f,
	CiscoSystems									=0x0041,
	AscomTimeplex00									=0x0043,
	FujitsuLinkBackupandLoadBalancingLblb			=0x0045,
	DCARemoteLan									=0x0047,
	SerialDataTransportProtocolPppSdtp				=0x0049,
	SNAover802_2									=0x004b,
	SNA												=0x004d,
	IP6HeaderCompression							=0x004f,
	StampedeBridging								=0x006f,
	NtcipSTMF										=0x00c1,
	compressiononsinglelinkinmultilinkgroup			=0x00fb,
	firstchoicecompression							=0x00fd,
	ieee802_1dHelloPackets							=0x0201,
	IBMSourceRoutingBPDU							=0x0203,
	DECLANBridge100SpanningTree						=0x0205,
	Luxcom											=0x0231,
	SigmaNetworkSystems								=0x0233,
	ieee801fNotUsed									=0x8001,
	InternetProtocolControlProtocol					=0x8021,
	OSINetworkLayerControlProtocol					=0x8023,
	XeroxNSIDPControlProtocol						=0x8025,
	DECnetPhaseIVControlProtocol					=0x8027,
	AppletalkControlProtocol						=0x8029,
	NovellIPXControlProtocol						=0x802b,
	BridgingNCP										=0x8031,
	StreamProtocolControlProtocol					=0x8033,
	BanyanVinesControlProtocol						=0x8035,
	MultiLinkControlProtocol						=0x803d,
	NETBIOSFramingControlProtocol					=0x803f,
	CiscoSystemsControlProtocol						=0x8041,
	AscomTimeplex80									=0x8043,
	FujitsuLBLBControlProtocol						=0x8045,
	DCARemoteLanNetworkControlProtocolRLNCP			=0x8047,
	SerialDataControlProtocolPppSdcp				=0x8049,
	SNAover802_2ControlProtocol						=0x804b,
	SNAControlProtocol								=0x804d,
	IP6HeaderCompressionControlProtocol				=0x804f,
	StampedeBridgingControlProtocol					=0x006f,
	compressiononsinglelinkinmultilinkgroupcontrol	=0x80fb,
	CompressionControlProtocol						=0x80fd,
	LinkControlProtocol								=0xc021,
	PasswordAuthenticationProtocol					=0xc023,
	LinkQualityReport								=0xc025,
	ShivaPasswordAuthenticationProtocol				=0xc027,
	CallBackControlProtocolCBCP						=0xc029,
	ContainerControlProtocol						=0xc081,
	ChallengeHandshakeAuthenticationProtocol		=0xc223,
	ProprietaryAuthenticationProtocol				=0xc281,
	StampedeBridgingAuthorizationProtocol			=0xc26f,
	ProprietaryNodeIDAuthenticationProtocol			=0xc481
	};

}
}
}
}

#endif
