/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_iana_iph_
#define _oscl_protocol_iana_iph_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace IANA {
/** */
namespace IP {

enum{
	ICMP					= 1,
	IGMP					= 2,
	GGP						= 3,
	IP						= 4,
	ST						= 5,
	TCP						= 6,
	UCL						= 7,
	EGP						= 8,
	IGP						= 9,
	BBN_RCC_MON				= 10,
	NVP_II					= 11,
	PUP						= 12,
	ARGUS					= 13,
	EMCON					= 14,
	XNET					= 15,
	CHAOS					= 16,
	UDP						= 17,
	MUX						= 18,
	DCN_MEAS				= 19,
	HMP						= 20,
	PRM						= 21,
	XNS_IDP					= 22,
	TRUNK_1					= 23,
	TRUNK_2					= 24,
	LEAF_1					= 25,
	LEAF_2					= 26,
	RDP						= 27,
	IRTP					= 28,
	ISO_TP4					= 29,
	NETBLT					= 30,
	MFE_NSP					= 31,
	MERIT_INP				= 32,
	SEP						= 33,
	3PC						= 34,
	IDPR					= 35,
	XTP						= 36,
	DDP						= 37,
	IDPR_CMTP				= 38,
	TPpp					= 39,
	IL						= 40,
	SIP						= 41,
	SDRP					= 42,
	SIP_SR					= 43,
	SIP_FRAG				= 44,
	IDRP					= 45,
	RSVP					= 46,
	GRE						= 47,
	MHRP					= 48,
	BNA						= 49,
	SIPP_ESP				= 50,
	SIPP_AH					= 51,
	I_NLSP					= 52,
	SWIPE					= 53,
	NHRP					= 54,
	AnyHost					= 61,
	CFTP					= 62,
	AnyLocalNetwork			= 63,
	SAT_EXPAK				= 64,
	KRYPTOLAN				= 65,
	RVD						= 66,
	IPPC					= 67,
	AnyDistFileSystem		= 68,
	SAT_MON					= 69,
	VISA					= 70,
	IPCV					= 71,
	CPNX					= 72,
	CPHB					= 73,
	WSN						= 74,
	PVP						= 75,
	BR_SAT_MON				= 76,
	SUN_ND					= 77,
	WB_MON					= 78,
	WB_EXPAK				= 79,
	ISO_IP					= 80,
	VMTP					= 81,
	SECURE_VMTP				= 82,
	VINES					= 83,
	TTP						= 84,
	NSFNET_IGP				= 85,
	DGP						= 86,
	TCF						= 87,
	IGRP					= 88,
	OSPFIGP					= 89,
	Sprite_RPC				= 90,
	LARP					= 91,
	MTP						= 92,
	AX_25					= 93,
	IPIP					= 94,
	MICP					= 95,
	SCC_SP					= 96,
	ETHERIP					= 97,
	ENCAP					= 98,
	AnyPrivateEncryption	= 99,
	GMTP					= 100
	};

}
}
}
}

#endif

