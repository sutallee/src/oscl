/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_iso_std4335h_
#define _oscl_protocol_iso_std4335h_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ISO {
/** */
namespace Std4335 {

// Basic control field masks
/** */
enum{controlILsb					=0x00};
/** */
enum{controlIFieldMask				=0x01};
/** */
enum{controlIBitValueMask_IFrame	=0x00};
/** */
enum{controlIBitValueMask_SUFrame	=0x01};

// Supervisor/User field masks
/** */
enum{controlSULsb				=0x00};
/** */
enum{controlSUFieldMask			=0x03};
/** */
enum{controlSUValueMask_SFrame	=0x01};
/** */
enum{controlSUValueMask_UFrame	=0x03};

/** */
enum{controlSLsb				=0x02};
/** */
enum{controlSFieldMask			=0x0C};
/** */
enum{controlSValueMask_RR		=0x00};
/** */
enum{controlSValueMask_REJ		=0x08};
/** */
enum{controlSValueMask_RNR		=0x04};
/** */
enum{controlSValueMask_SREJ		=0x0C};

// Poll/Final field
/** */
enum{controlPFLsb				=0x04};
/** */
enum{controlPFFieldMask			=0x10};
/** */
enum{controlPFValueMask_Poll	=0x10};
/** */
enum{controlPFValueMask_Final	=0x10};

// Next Receive field
/** */
enum{controlNRLsb		=0x05};
/** */
enum{controlNRFieldMask	=0xE0};

// Next Send field
/** */
enum{controlNSLsb		=0x01};
/** */
enum{controlNSFieldMask	=0x0E};

/** */
enum{controlMLsb		=0x02};
/** */
enum{controlMFieldMask	=0xEC};

// Commands
// Mode (M) field.
/** */
enum{controlMValueMask_C_SNRM	=0x80};
/** */
enum{controlMValueMask_C_SARM	=0x0C};
/** */
enum{controlMValueMask_C_SABM	=0x2C};
/** */
enum{controlMValueMask_C_DISC	=0x40};
/** */
enum{controlMValueMask_C_SNRME	=0xCC};
/** */
enum{controlMValueMask_C_SARME	=0x4C};
/** */
enum{controlMValueMask_C_SABME	=0x3C};
/** */
enum{controlMValueMask_C_SIM	=0x04};
/** */
enum{controlMValueMask_C_UP		=0x20};
/** */
enum{controlMValueMask_C_UI		=0x00};
/** */
enum{controlMValueMask_C_XID	=0xAC};
/** */
enum{controlMValueMask_C_RSET	=0x8C};
/** */
enum{controlMValueMask_C_TEST	=0xE0};

// Responses
// Mode (M) field.
/** */
enum{controlMValueMask_R_UA		=0x60};
/** */
enum{controlMValueMask_R_FRMR	=0x84};
/** */
enum{controlMValueMask_R_DM		=0x0C};
/** */
enum{controlMValueMask_R_RD		=0x40};
/** */
enum{controlMValueMask_R_RIM	=0x04};
/** */
enum{controlMValueMask_R_UI		=0x00};
/** */
enum{controlMValueMask_R_XID	=0xAC};
/** */
enum{controlMValueMask_R_TEST	=0xE0};

#endif
