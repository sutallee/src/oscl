/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_iso_std3309h_
#define _oscl_protocol_iso_std3309h_


/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace ISO {
/** */
namespace Std3309 {

/** Frame delimiter */
enum{flag=0x7E};

/** Transparency escape */
enum{escape=0x7D};

/** Broadcast address */
enum{addressAllStations=0xFF};

/** LSB of address extension bit */
enum{addressE_Lsb=0x00};

/** Address extension bit mask */
enum{addressE_FieldMask=0x01};

/** Address extension indicator */
enum{addressE_ValueMask_Extended=0x01};

/** Station address mask */
enum{addressNoStation_FieldMask=0xFE};

/** Reserved address to be discarded by all receiving stations */
enum{addressNoStation=0x00};

/** LSB of group/multicast address indicator */
enum{addressG_Lsb=0x01};

/** Field mask for group/multicast address indicator */
enum{addressG_FieldMask=0x02};

/** Value mask indicating a group/multicast address */
enum{addressG_ValueMask_Group=0x02};

/** LSB of extended control field indicator */
enum{controlE_Lsb=0x00};

/** Extended control field indicator mask */
enum{controlE_FieldMask=0x01};

/** Value mask indicating control field is extended. */
enum{controlE_ValueMask_Extended=0x01};

}
}
}
}

#endif
