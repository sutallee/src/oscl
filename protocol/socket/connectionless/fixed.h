/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connectionless_fixedh_
#define _oscl_protocol_socket_connectionless_fixedh_
#include <string.h>
#include "peer.h"
#include "err.h"
#include "oscl/protocol/socket/connectionless/tx/api.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connectionless {

/** */
template <unsigned size>
class FixedPeer : public Peer {
	private:
		/** The "minimum" buffer size allows for 2 IPv4
			addresses (source and destination) and two
			IPv4 port numbers (source and destination).
		 */
		enum{minimumBufferSize=(2*sizeof(unsigned long))+(2*sizeof(unsigned))};

		/** */
		union {
			Oscl::Memory::AlignedBlock<size>				_appSpecific;
			Oscl::Memory::AlignedBlock<minimumBufferSize>	_minimum;
			}		_addressBuffer;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::TX::Api*				_transmitter;

		/** */
		unsigned							_length;

	public:
		/** */
		FixedPeer() noexcept;

	public:	// Peer
		/** This operation may only be invoked by an
			entity that "knows" the protocol specific 
			information about the peer. The "address"
			of the specified "length" is copied into
			the Peer implementation. If the Peer implementation
			does not have a large enough address buffer
			to contain the entier "address", then _tx
			is set to NULL indicating that the datagram
			cannot be received. In addition, the actual
			size of the address buffer (maxLength())
			is returned. Otherwise, _tx is set to the
			addres of tx, and "length" is returned.
		 */
		unsigned	set(	Oscl::Protocol::Socket::
							Connectionless::TX::Api&	tx,
							const void*					address,
							unsigned					length
							) noexcept;

		/** Copies the protocol specific peer addressing information
			supplied by the set() operation into the supplied "address"
			buffer, which must be at least "addressLength" bytes long.
			This operation returns addressLength if successful, or zero
			if the address contained in the peer is invalid or does not
			exactly match "addressLength".
		 */
		unsigned	copyOutAddress(	void*		address,
									unsigned	addressLength
									) const noexcept;

		/** This operation is invoked to send a datagram toward the
			peer. Returns zero/null if successful, otherwise returns
			a TxError 
		 */
		const	Oscl::Protocol::Socket::
				Connectionless::TxError*
						send(	const void*	datagram,
								unsigned	length
								) noexcept;
	};

template <unsigned size>
FixedPeer<size>::FixedPeer() noexcept:
		_transmitter(0),
		_length(0)
		{
	}

template <unsigned size>
unsigned	FixedPeer<size>::set(	Oscl::Protocol::Socket::
									Connectionless::TX::Api&	transmitter,
									const void*					address,
									unsigned					length
									) noexcept{
	if(length > sizeof(_addressBuffer)){
		_transmitter	= 0;
		return 0;
		}
	_transmitter	= &transmitter;
	_length			= length;
	memcpy(&_addressBuffer,address,length);
	return length;
	}

template <unsigned size>
unsigned	FixedPeer<size>::copyOutAddress(	void*		address,
												unsigned	addressLength
												) const noexcept{
	if(!_transmitter){
		return 0;
		}
	if(addressLength != _length){
		return 0;
		}
	memcpy(address,&_addressBuffer,addressLength);
	return addressLength;
	}

template <unsigned size>
const	Oscl::Protocol::Socket::
		Connectionless::TxError*
			FixedPeer<size>::send(	const void*	datagram,
									unsigned	length
									) noexcept{
	if(!_transmitter){
		return &invalidDestination;
		}
	_transmitter->send(*this,datagram,length);
	return 0;
	}

}
}
}
}

#endif
