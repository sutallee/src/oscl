/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connectionless_errorh_
#define _oscl_protocol_socket_connectionless_errorh_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connectionless {

/** */
class TxErrorHandler {
	public:
		/** Make GCC happy */
		virtual ~TxErrorHandler() {}
		/** This operation is invoked if the content
			specified destination was invalid.
		 */
		virtual void	invalidDestination() noexcept=0;
		/** This operation is invoked if the length
			of the datagram is too long due to local
			system limitations.
		 */
		virtual void	datagramTooLong() noexcept=0;

		/** This operation is invoked if the datagram
			cannot be delivered for any other reason.
		 */
		virtual void	undeliverable() noexcept=0;

		/** This operation is invoked if the socket
			is closed.
		 */
		virtual void	socketClosed() noexcept=0;
	};

/** This type is returned by a datagram transmit
	operation when a run-time error is encountered.
	The application may then invoke the query
	operation to handle the specific error.
 */
class TxError {
	public:
		/** Make GCC happy */
		virtual ~TxError() {}
		/** This operation is invoked to discern the
			specific type of error by an application
			that encountered a run-time error while
			attempting to transmit on socket.
		 */
		virtual void	query(TxErrorHandler& rh) const noexcept=0;
	};

}
}
}
}

#endif
