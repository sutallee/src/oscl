/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connectionless_rx_apih_
#define _oscl_protocol_socket_connectionless_rx_apih_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connectionless {

class Peer;

/** */
namespace RX {

/** This interface represents the socket interface
	used to receive datagrams from a peer.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** This operation copies a datagram from a "source"
			peer to the "datagram" buffer. The "length"
			parameter specifies the maximum number of bytes
			that the "datagram" buffer can hold.
			The addressing information for the source peer
			is copied into the "source".
			Zero is returned if the socket is closed.
			If the source addressing information cannot
			be copied into the "source" Peer object, then
			object will be invalidated and it will not
			be usable to respond to the datagram, however,
			the datagram will still be copied into the
			"datagram" buffer as described above.
		 */
		virtual unsigned	recv(	Peer&		source,
									void*		datagram,
									unsigned	maxLength
									) noexcept=0;
	};

}
}
}
}
}

#endif
