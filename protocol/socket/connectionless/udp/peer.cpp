/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "peer.h"

using namespace Oscl::Protocol::Socket::Connectionless::UDP;

Peer::Peer(	Oscl::Protocol::Socket::
			Connectionless::TX::Api&	tx,
			unsigned long				peerIpAddress,
			unsigned long				localIpAddress,
			unsigned					peerPortNum,
			unsigned					localPortNum
			) noexcept{
	Oscl::Protocol::IP::Port::Path	path(	peerIpAddress,
											localIpAddress,
											peerPortNum,
											localPortNum
											);
	set(tx,&path,sizeof(path));
	}

