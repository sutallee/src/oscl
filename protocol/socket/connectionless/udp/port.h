/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connectionless_udp_porth_
#define _oscl_protocol_socket_connectionless_udp_porth_

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connectionless {
/** */
namespace UDP {

/** */
class Port : public Oscl::Protocol::Socket::Connectionless::RX::Api {
	private:
		/** */
		Oscl::Protocol::IP::UDP::Port::Req::Api::SAP&	_sap;

		/** */
		unsigned	_ipListenPort;

	public:
		/** */
		Port(	Oscl::Protocol::IP::UDP::Port::Req::Api::SAP&	sap,
				unsigned	ipListenPort
				) noexcept;

	public:
		/** */
		unsigned	recv(	Peer&		source,
							const void*	datagram,
							unsigned	maxLength
							) noexcept;
	};

}
}
}
}
}

#endif
