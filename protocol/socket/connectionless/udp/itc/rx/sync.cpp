/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/mt/itc/mbox/syncrh.h"
#include "sync.h"
#include "oscl/protocol/socket/connectionless/peer.h"

using namespace Oscl::Protocol::Socket::Connectionless::UDP::Itc::RX;

Sync::Sync(	Oscl::Protocol::IP::UDP::Port::RX::RecvApi&			rxapi,
			Oscl::Protocol::Socket::Connectionless::TX::Api&	transmitter
			) noexcept:
		_rxapi(rxapi),
		_transmitter(transmitter)
		{
	}

unsigned	Sync::recv(	Oscl::Protocol::Socket::
						Connectionless::Peer&		source,
						void*						datagram,
						unsigned					maxLength
						) noexcept{
	Oscl::Protocol::IP::Port::Path	path;
	maxLength	= _rxapi.recv(path,datagram,maxLength);
	source.set(	_transmitter,
				&path,
				sizeof(path)
				);
	return maxLength;
	}
