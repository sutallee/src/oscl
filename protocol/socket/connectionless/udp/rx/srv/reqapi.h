/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connectionless_udp_rx_srv_reqapih_
#define _oscl_protocol_socket_connectionless_udp_rx_srv_reqapih_
#include "oscl/protocol/ip/port/path.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connectionless {
/** */
namespace UDP {
/** */
namespace RX {
/** */
namespace Srv {
/** */
namespace Req {

/** This payload delivers the "datagram" to the peer
	identified by the "path". The buffer passed becomes
	the "property" of the network subsystem until the
	associated request completes. That is to say that
	the buffer contents are NOT copied.
	Also, it is important (for portability) that the
	buffers/"datagram" buffer be "Cache Aligned", whatever
	that means for a particular system. This is due to the fact that
	buffers are potentially cache invalidated at the driver
	level of the system, and doing so may cause surrounding
	memory that is not a part of the buffer to be corrupted
	under varying conditions.
 */
class RecvPayload {
	public:
		/** This variable is filled out before the
			datagram is returned unless the length
			field is set to zero because the socket
			was closed.
		 */
		Oscl::Protocol::IP::Port::Path	_path;

		/** */
		void*							_datagram;

		/** This member contains the maximum size of the
			buffer when the request is sent, but is
			modified before the request is returned
			to reflect the actual number of bytes
			copied into the "datagram" buffer.
			If the socket is closed while the
			receive request is pending, the value
			of the _length member is set to zero.
		 */
		unsigned						_length;

	public:
		/** */
		RecvPayload(	void*			datagram,
						unsigned		length
						) noexcept;
	};

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,RecvPayload>	RecvReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>						SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>				ConcreteSAP;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual	void	request(RecvReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}
}


#endif
