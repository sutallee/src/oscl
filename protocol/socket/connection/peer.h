/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connection_peerh_
#define _oscl_protocol_socket_connection_peerh_
#include "oscl/protocol/socket/connection/tx/api.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connection {

/** A "Peer" contains all of the protocol specific information
	needed to uniquely identify the "far" end (peer) of a communications
	channel. Note the difference between "Peer" and "peer".

	Problem:
	o	An application in the abstract should not need to know
		the the details of the protocol that it uses to exchange
		datagrams with a peer.
	o	An application needs to have a copy of (not a reference to)
		the Peer information. This eliminates the need for complex
		memory management between the application and the communications
		sub-system.
 */
class Peer {
	public:
		
	public:
		/** */
		virtual ~Peer() {}

		/** This operation may only be invoked by an
			entity that "knows" the protocol specific 
			information about the peer. The "address"
			of the specified "length" is copied into
			the Peer implementation. If the Peer implementation
			does not have a large enough address buffer
			to contain the entier "address", then protocol
			is set to NULL indicating Peer is invalid.
			In addition, the actual size of the address
			buffer (maxLength()) is returned. Otherwise,
			the protocol is stored, the address is
			copied and "length" is returned.
		 */
		virtual unsigned	set(	Oscl::Protocol::Socket::
									Connection::TX::Api&	transmitter,
									const void*					address,
									unsigned					length
									) noexcept=0;

		/** Copies the protocol specific peer addressing information
			supplied by the set() operation into the supplied "address"
			buffer, which must be at least "addressLength" bytes long.
			This operation returns addressLength if successful, or zero
			if the address contained in the peer is invalid or does not
			exactly match "addressLength".
		 */
		virtual unsigned	copyOutAddress(	void*		address,
											unsigned	addressLength
											) const noexcept=0;

		/** This operation is invoked to send a datagram toward the
			peer. Returns zero/null if successful, otherwise returns
			a TxError 
		 */
		virtual	const	Oscl::Protocol::Socket::
						Connection::TxError*
								send(	const void*	datagram,
										unsigned	length
										) noexcept=0;
	};

}
}
}
}

#endif
