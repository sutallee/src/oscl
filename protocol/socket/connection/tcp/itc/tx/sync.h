/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_protocol_socket_connection_tcp_itc_tx_synch_
#define _oscl_protocol_socket_connection_tcp_itc_tx_synch_
#include "oscl/protocol/socket/connection/tx/api.h"
#include "oscl/protocol/socket/connection/tx/api.h"
#include "oscl/protocol/ip/tcp/port/tx/send.h"

/** */
namespace Oscl {
/** */
namespace Protocol {
/** */
namespace Socket {
/** */
namespace Connection {
/** */
namespace TCP {
/** */
namespace Itc {
/** */
namespace TX {

/** */
class Sync : public Oscl::Protocol::Socket::Connection::TX::Api {
	public:
		/** */
		Oscl::Protocol::IP::TCP::Port::TX::SendApi&			_txapi;

	public:
		/** */
		Sync(Oscl::Protocol::IP::TCP::Port::TX::SendApi&	txapi) noexcept;

	public:	// Oscl::Protocol::Socket::Connection::TX::Api
		/** Attempts to send the "datagram" of the specified
			length toward the "destination".
			Returns zero/null if successful, otherwise a
			pointer to the specific error is returned.
		 */
		const	Oscl::Protocol::Socket::
				Connection::TxError*	send(	const Peer&	destination,
													const void*	datagram,
													unsigned	length
													) noexcept;
	};
}
}
}
}
}
}
}

#endif
