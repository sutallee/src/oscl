/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/mt/itc/mbox/syncrh.h"
#include "sync.h"
#include "oscl/protocol/socket/connection/peer.h"
#include "oscl/protocol/socket/connection/err.h"

using namespace Oscl::Protocol::Socket::Connection::TCP::Itc::TX;

Sync::Sync(Oscl::Protocol::IP::TCP::Port::TX::SendApi&	txapi) noexcept:
		_txapi(txapi)
		{
	}

const Oscl::Protocol::Socket::
Connection::TxError*	Sync::send(	const Peer&	destination,
										const void*	datagram,
										unsigned	length
										) noexcept{
	Oscl::Protocol::IP::Port::Path	path;
	if(!destination.copyOutAddress(&path,sizeof(path))){
		return &invalidDestination;
		}
	unsigned
	len	= _txapi.send(path,datagram,length);
	if(len != length){
		if(len == 0){
			return &socketClosed;
			}
		if(len < length){
			return &datagramTooLong;
			}
		// FIXME: what if the length is the max value
		// of an unsigned integer and is still not "too long"?
		// Highly unlikely as the datagram would likely exceed
		// a system limitation first.
		return &undeliverable;
		}
	return 0;
	}
