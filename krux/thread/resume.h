/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_resumeh_
#define _oscl_krux_thread_resumeh_
#include "oscl/krux/port/trap.h"

/** */
namespace Oscl {
/** */
namespace Krux {

class Thread;

/** Concrete trap that implements the yield service
	which gives up processor time to other threads within
	the same priority band as the invoking thread.
 */

class TrapResume : public Trap{
	private:
		/** This variable references the thread to receive the
			resume operation.
		 */
		Thread&	_threadToResume;
	public:
		/** Constructor taking a reference to the target thread.
		 */

		TrapResume(Thread& thread) noexcept:_threadToResume(thread){};

		/** Constructor taking a pointer to the target thread.
		 */
		TrapResume(Thread* thread) noexcept:_threadToResume(*thread){};

		/** Implements Trap service interface, performing
			the actual resume operation within the processor's
			supervisor mode.
		 */

		void suService(void) noexcept;
	};

}
}

#endif
