/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdlib.h>
#include <string.h>
#include "sched.h"
#include "thread.h"

using namespace Oscl::Krux;

/*****************************************************************************
 * standard_new()
 *	This routine implements the logic of the default new handler.
 */

extern std::new_handler	__new_handler;
//extern std::new_handler	__default_new_handler;

void *standard_new(size_t sz) {
	typedef void	(*vfp)(void);
#if 0
	extern vfp		__new_handler;
#endif

#if 0
	extern vfp		__default_new_handler;
#endif
	void*			p;

#if 0
	vfp		handler = (__new_handler) ? __new_handler : __default_new_handler;
#else
	vfp		handler = (__new_handler) ? __new_handler : 0;
#endif

	/* malloc (0) is unpredictable; avoid it.  */
	if (sz == 0) sz = 1;
	p = (void *) malloc (sz);
	while (p == 0){
		(*handler) ();
		p = (void *) malloc (sz);
		}
	return p;
	}

/*****************************************************************************
 * operator new()
 *	This routine replaces the default C++ new operator with one which
 *	recognizes thread memory partitions.
 */

void *operator new(size_t size) {
	Thread*		currentThread=0;
	void*		p;

	if(Scheduler::SchedulerReady()){
		currentThread	= Scheduler::GetScheduler().getCurrentThread();
		}

	if (!size) size = 1;

	if(currentThread){
		p	= currentThread->alloc(size);

		if(!p){
			currentThread->memoryFail();
			p	= currentThread->alloc(size);
			}
		}
	else{
		p	= standard_new(size);
		}

	return p;
	}

/*****************************************************************************
 * operator delete()
 *	This routine replaces the default C++ delete operator with one which
 *	recognizes thread memory partitions.
 */

void operator delete (void *p) {
	Thread*		currentThread;

	currentThread	= Scheduler::GetScheduler().getCurrentThread();

	if(currentThread){
		currentThread->bfree(p);
		}
	else{
		free(p);
		}
	}

void operator delete(void * ptr, size_t size){
	for(;;);
	}

