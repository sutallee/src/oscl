/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_ukspschedh_
#define _oscl_krux_thread_ukspschedh_
#include "sched.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** Implements a thread scheduler for a single processor
	system.
 */
class SingleProcessorScheduler: public Scheduler{
	private:
		/** The currently scheduled thread.
		 */
		Thread*			currentThread;

		/** Internal counter used to keep track of
			the number of calls to lockScheduler()
			and unlockScheduler. When this count is zero,
			scheduling is enabled.
		 */
		unsigned		disableCount;

	public:
		/** Constructor initializes private member variables.
		 */
		SingleProcessorScheduler() noexcept;

		/** Returns the currently scheduled thread.
		 */
		virtual Thread*	getCurrentThread() noexcept;

		/** Causes the highest priority ready thread to be scheduled
			for execution.
		 */
		virtual Thread*	scheduleHighestPriortyReady() noexcept;

		/** Causes thread scheduling to be disabled such that the thread that
			is currently scheduled will not be interrupted by another thread.
			An internal count is maintained of the number of times that this function is
			called. Scheduling will not be resumed until this count is decremented
			to zero by an equal number of calls to the unlockScheduler()
			member function. This allows nesting of paired calls to lockScheduler()
			and unlockScheduler.
			The current thread is NOT protected from exception handlers, but threads
			made ready by exception handlers will not execute until scheduling
			is re-enabled.
		 */
		virtual void	lockScheduler() noexcept;

		/** Causes thread scheduling to be re-enabled after scheduling
			has been disabled by the lockScheduler() member function. Scheduling
			is only re-enabled once the internal count has been decremented
			to zero. This allows nesting of paired calls to lockScheduler()
			and unlockScheduler().
		 */
		virtual void	unlockScheduler() noexcept;

		/** Returns true if the current thread has requested that
			it not be interrupted by other threads via the lockScheduler()
			member function. Under such conditions the scheduleHighestPriortyReady()
			will always return without scheduling a different thread. Therefore,
			if this function returns true, there is no thread switching to be
			performed.
		 */
		virtual bool	schedulerIsLocked() noexcept;

		/** Makes the specified thread the current thread.
		 */
		virtual void	setCurrentThread(Thread* thread) noexcept;

		/** Begins operation of the scheduler. This member function never returns.
		 */
		virtual void	start() noexcept;
	};

}
}

#endif

