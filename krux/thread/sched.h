/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_ukschedh_
#define _oscl_krux_thread_ukschedh_
#include "thread.h"
#include "oscl/krux/port/frame.h"

extern "C"{
	/** This function is called by target specific trap
		handlers to allow for potential rescheduling of
		threads. Interrupts should always be disabled
		before calling this function.
	 */
	OsclKruxFrame*		Schedule(OsclKruxFrame *frame) noexcept;
	}

/** */
namespace Oscl {
/** */
namespace Krux {

/** Abstract class that represents a thread scheduler. Anticipated
	derived classes are schedulers for single and multi processor
	subsystems.
 */
class Scheduler{
	public:
		/** Make GCC happy */
		virtual ~Scheduler() {}

		/** Returns the currently scheduled thread. This is a pure
			virtual function which must be implemented by sub-classes.
		 */
		virtual Thread*	getCurrentThread() noexcept= 0;

		/** Searches for the highest priority thread ready for
			for execution and makes it the current thread. This is a
			pure virtual function which must be implemented by sub-classes.
		 */
		virtual Thread*	scheduleHighestPriortyReady() noexcept= 0;

		/** Causes thread scheduling to be disabled such that the thread that
			is currently scheduled will not be interrupted by another thread.
			An internal count is maintained of the number of times that this function is
			called. Scheduling will not be resumed until this count is decremented
			to zero by an equal number of calls to the unlockScheduler()
			member function. This allows nesting of paired calls to lockScheduler()
			and unlockScheduler.
			The current thread is NOT protected from exception handlers, but threads
			made ready by exception handlers will not execute until scheduling
			is re-enabled.
			In a multi-processor system, this function would cause threads
			executing on other processors to be suspended until the scheduler
			is unlocked via the unlockScheduler() member function.
			This is a pure virtual function whose implementation is dependent
			on derived classes.
		 */
		virtual void	lockScheduler() noexcept= 0;

		/** Causes thread scheduling to be re-enabled after scheduling
			has been disabled by the lockScheduler() member function. Scheduling
			is only re-enabled once the internal count has been decremented
			to zero. This allows nesting of paired calls to lockScheduler()
			and unlockScheduler().
			When the internal count is decremented to zero in a multi-processor system,
			this function would cause threads suspended by the scheduler being
			disabled to be enabled for scheduling by other processors.
			This is a pure virtual function whose implementation is dependent
			on derived classes.
		 */
		virtual void	unlockScheduler() noexcept= 0;

		/** Returns true if the current thread has requested that
			it not be interrupted by other threads via the lockScheduler()
			member function. Under such conditions the scheduleHighestPriortyReady()
			will always return without scheduling a different thread. Therefore,
			if this function returns true, there is no thread switching to be
			performed.
			This is a pure virtual function whose implementation is dependent
			on derived classes.
		 */
		virtual bool	schedulerIsLocked()	noexcept= 0;

		/** Makes the specified thread the current thread. In a mult-processor
			system, the specified thread becomes the current thread for this
			processor.
			This is a pure virtual function whose implementation is dependent
			on derived classes.
		 */
		virtual void	setCurrentThread(Thread* thread) noexcept= 0;

		/** Begins operation of the scheduler. This member function never returns.
		 */
		virtual void	start() noexcept= 0;

		/** Returns the scheduler for this processing element. This method
			must be implemented by derived class implementations. No default
			is supplied.
		 */

		/** */
		static Scheduler&	GetScheduler() noexcept;
		/** */
		static bool			SchedulerReady() noexcept;
	};

}
}

#endif
