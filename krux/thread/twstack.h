/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_twstackh_
#define _oscl_krux_thread_twstackh_
#include "thread.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** */
template <unsigned maxStackSize>
class ThreadWithStack {
	private:
		/** */
		Oscl::Memory::AlignedBlock<maxStackSize>	_stack;
	public:
		/** */
		Thread										_thread;
	public:
		/** */
		ThreadWithStack(	SchedPriBand&	priority,
							Mt::Runnable&	runnable
							) noexcept:
			_thread(	&priority,
						runnable,
						_stack._charMem,
						sizeof(_stack)
						){}

		/** */
		unsigned long	untouchedStackSpace() const noexcept{
			unsigned char*
			p	= ((unsigned char*)&_stack);
			for(unsigned long i = 0; i < sizeof(_stack);++i,++p){
				if(*p != 0xFF){
					return i;
					}
				}
			return sizeof(_stack);
			}

		/** */
		unsigned long	stackSize() const noexcept{
			return sizeof(_stack);
			}
	};

}
}

#endif
