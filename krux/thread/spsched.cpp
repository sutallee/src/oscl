/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "spsched.h"
#include "oscl/krux/plist/plist.h"
#include "oscl/krux/port/platform.h"

#ifdef NEED_DUMB_SHIT
	extern void dumb();
#endif

extern Oscl::Krux::SchedPriList	Prioritizer;

using namespace Oscl::Krux;

static bool	SchedConstructed;

static SingleProcessorScheduler	sps;

SingleProcessorScheduler::SingleProcessorScheduler() noexcept{
	currentThread	= 0;	// Perhaps should be the null task?
	disableCount	= 0;	// Scheduler is initially enabled.
	SchedConstructed	= true;
	}

Thread*	SingleProcessorScheduler::getCurrentThread() noexcept{
	return currentThread;
	}

Thread*	SingleProcessorScheduler::scheduleHighestPriortyReady() noexcept{
	if(!schedulerIsLocked()){
		Prioritizer.scheduleHighestPriortyReady();
		}
	return currentThread;
	}

void	SingleProcessorScheduler::lockScheduler() noexcept{
	disableCount++;
	}

void	SingleProcessorScheduler::unlockScheduler() noexcept{
	if(disableCount) disableCount--;
	}

bool	SingleProcessorScheduler::schedulerIsLocked() noexcept{
	return (disableCount)?true:false;
	}

void	SingleProcessorScheduler::setCurrentThread(Thread* thread) noexcept{
	currentThread	= thread;
	}

void	SingleProcessorScheduler::start() noexcept{
	OsclKruxFrame*	frame;
#ifdef NEED_DUMB_SHIT
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
#if 0
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
#endif
#endif
	scheduleHighestPriortyReady();
	frame	= currentThread->getFrame();
	/*	When this function is called, the system is operating
		in supervisor mode, under a supervisor stack pointer.
	 */
	OsclKruxStartTrap(frame);
	}

Scheduler&	Scheduler::GetScheduler() noexcept{
	return sps;
	}
bool			Scheduler::SchedulerReady() noexcept{
	return SchedConstructed;
	}
