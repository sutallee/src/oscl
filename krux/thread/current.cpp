/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sched.h"
#include "oscl/mt/thread.h"
#include "oscl/krux/timer/sematimer.h"
#include "oscl/timer/busywait.h"
#include "oscl/timer/tocks.h"

using namespace Oscl::Krux;

Oscl::Mt::Thread::Thread() noexcept{
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept{
	return *Scheduler::GetScheduler().getCurrentThread();
	}

void	Oscl::Mt::Thread::sleep(unsigned long milliseconds) noexcept {
	if(		Scheduler::SchedulerReady()
		&&	Scheduler::GetScheduler().getCurrentThread()
		){
		Oscl::Krux::Sema::Binary	sema;
		SemaTimer		timer(sema,OsclTimerMillisecondsToTocks(milliseconds));
		timer.start();
		timer.wait();
		}
	else{
		OsclTimerBusyWaitDelay(milliseconds);
		}
	}

