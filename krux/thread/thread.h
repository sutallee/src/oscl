/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_ukthreadh_
#define _oscl_krux_thread_ukthreadh_
#include "oscl/krux/port/frame.h"
#include "oscl/krux/port/entry.h"
#include "oscl/krux/spri/spri.h"
#include "oscl/mt/runnable.h"
#include "oscl/mt/thread.h"
#include "oscl/krux/mutex/sema.h"
#include "oscl/krux/sema/bsema.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** Represents a unit of processor time allocation.
	Threads are the primary unit manipulated
	by the micro-kernel scheduler.
 */
class	Thread :	public SchedPriBandEntry,
					public EntryPoint,
					public Mt::Thread
					{
	public:
		/** */
		static unsigned				nThreads;

	private:
#ifdef RUN_TIME_STACK_CHECK_ENABLED
		/** This variable represents the limit of the stack.
			If the stack pointer is ever less than this value
			a run time debug routine is executed.
		 */

		const unsigned long			_stackLimit;

		/** This variable is set to true if the run time stack
			checking is enabled and the limit of the stack is
			ever exceeded.
		 */
		bool						_outOfBounds;
#endif

		/** */
		bool						_catchBusError;

		/** */
		bool						_busErrorCaught;

		/** This member is used to perform synchronous transactions
			and eliminates the need to allocate a new semaphore
			every time such a synchronous transaction is invoked.
			It is available through the appropriate thread interfaces
			via the scheduler's currentThread operations.
		 */
		Sema::Binary				_syncSema;

		/** This static mutex is used to protect global thread
			resources, such as heap memory.
		 */
		static Mutex::Semaphore		mutex;

		/** This class is used as an element in a list of long-word
			sized variables that are saved and restored on every
			task switch of the thread.
		 */
		class Variable : public QueueItem {
			private:
				/** This variable holds the address of the long-word to
					be copied on context switches.
				 */
				void**	const	_v;

				/** This member holds the copy of the saved variable.
				 */
				void*			_saved;

			public:
				/** The constructor saves the address of the variable and
					copies the initial value into the saved member variable.
				 */
				inline Variable(void** v):_v(v),_saved(*v){}

				/** This operation is called when the thread is switched
					out in favor of a higher priority thread. The registered
					location is copied into the saved variable.
				 */
				inline void	saveContext(){
					_saved	= *_v;
					}

				/** This opertion is called when the thread is scheduled
					to execute. The registered location is copied from the
					saved variable into the address.
				 */
				inline void	restoreContext(){
					*_v	= _saved;
					}
			};

		Variable*	_contextVariableList;

		/** Non-zero when the thread has been suspended.
			Allows for nested suspend/resume pairs.
		 */
		unsigned				_suspended;

		/** Contains the run member function that implements
		 	the threads functionality. The run function is
			called when the thread is started.
		 */
		Mt::Runnable&			_runnable;

		/** Contains the saved stack context when the
			thread is not currently active.
		 */
		::OsclKruxFrame*		_frame;

		/** This flag is set to cause the thread to
			be placed last in its priority band
			the next time it is scheduled to run.
			The flag is reset after the thread
			is re-scheduled.
		 */
		bool					_yield;

		/** This flag is true if the thread is currently
			threaded into it's priority queue.
		 */
		bool					_queued;

	public:
		/** Contains pointer to the current priority band.
		 */
		class SchedPriBand		*_priority;

	public:
		/** The thread constructor requires a priority band
			and an initial stack space.
		 */
		Thread(	SchedPriBand	*priority,
				Mt::Runnable&	run,
				void			*stack,
				unsigned long	stackSize
				) noexcept;

		/** The destructor disposes of memory allocated for
			additional context variables.
		 */
		virtual ~Thread();

		/** Invoked from user mode allow other threads of the same priority to run.
		 */
		void	yield() noexcept;

		/** Invoked from supervisor mode to cause thread to be skipped for scheduling
		 	if it is the currently active thread. This is typically called by the
		 	yield trap.
		 */

		void	suYield(void) noexcept;

		/** Invoked from user mode to suspend this thread.
		 */
		void	suspend() noexcept;

		/** Invoked from user mode to resume this thread.
		 */
		void	resume() noexcept;

		/** Thread is prevented from being placed in the
			scheduler's ready queue, and thread is removed
			from the ready queue if it is already in the queue.
			An internal count of the number of calls to suspend()
			is maintained such that nested suspend() and resume()
			calls can be accommadated.
		 */
		void suSuspend(void) noexcept;

		/** Decrement the internal suspend count of this thread
			and if it reaches zero, put the thread in the ready queue
			of it's priority band.
			An internal count of the number of calls to suspend()
			is maintained such that nested suspend() and resume()
			calls can be accommadated.
		 */
		void suResume(void) noexcept;

		/** Cause the thread to put itself at the beginning of
			its priority queue if it is ready to execute
			(not suspended). This function is ONLY called by
			the scheduler when the thread is the currently executing
			thread to allow the thread to compete for scheduling.
		 */
		void suSchedule(void) noexcept;

		/** Entry implements the EntryPoint interface and is called when
			the thread is started. Its sole function is to invoke the
			polymorphic run() member function.
		 */
		void entry() noexcept;

		/** Sets the frame pointer.
		 */
		inline void setFrame(OsclKruxFrame* frame) {_frame=frame;}

		/** Returns the frame pointer.
		 */
		inline OsclKruxFrame* getFrame() const {return _frame;}

		/** Implements the pure virtual function inherited from SchedPriBandEntry
			that is used to indicate that this thread is to become the
			currently executing thread.
		 */
		void	activate(void) noexcept ;

		/** Requests thread to make additional memory resources available.
			by default, the global heap is called to allocate additional
			pages for this thread.
		 */
		virtual void memoryFail() noexcept;

		/** Allocate a block of memory of the requested size from the thread's
			local partition. The default implementation allocates memory
			from the global heap using malloc(). Returns zero pointer if
			memory is unavailable.
		 */
		virtual void *alloc(unsigned long size) noexcept;

		/** Return block of memory to thread's local partition. The default
			implementation returns the block to the global heap using
			free();
		 */
		virtual void bfree(void *m) noexcept;

		/** Test the parameter against the _stackLimit and return true
			if the stack is outside of its limits.
		 */

		bool	stackOutOfBounds(unsigned long sp) noexcept;

		/** This operation removes the first Variable from the stack
			and returns a pointer reference. If the stack is empty,
			a null pointer is returned.
		 */
		Variable*	getVar();

		/** This operation is called when a thread needs to add static
			variables to be saved and restored during thread context
			switch operations. The required memory is allocated from the
			global heap.
		 */
		bool/*success*/	addVar(void** variableAddress) noexcept;

		/** This operation is called by the scheduler to cause the
			thread to save context that is in addition to the
			stack based context, which is preserved by the scheduler.
		 */
		void	saveExtraContext();

		/** This operation is called by the scheduler to cause the
			thread to restore context that is in addition to the
			stack based context, which is restored by the scheduler.
		 */
		void	restoreExtraContext();

		/** This operation returns the signal interface for the thread
			semaphore, which is used for synchronous message transactions.
			This is the sister operation to the thread's syncWait()
			operation.
		 */
		Mt::Sema::SignalApi&	getSyncSignalApi() noexcept;

		/** This operation blocks on the thread semaphore, waiting for
			a synchronous message operation to complete. This is the
			sister operation to the thread's getSyncSignaled() operation.
		 */
		void		syncWait() noexcept;

		/** This operation returns a reference to the currently
			executing thread.
		 */
		static Mt::Thread&	getCurrent() noexcept ;

		/** */
		static Thread&	getCurrentKernelThread() noexcept ;

		/** This operation returns the name (null terminated string)
			of the current thread.
		 */
		const char* getName() noexcept;

		/** Begin catching bus errors.
		 */
		void	catchBusError() noexcept;

		/** Returns true if a bus error was caught.
			Disables bus error catching.
		 */
		bool	caughtBusError() noexcept;

		/** Returns true if thread was intentionally catching
			bus errors. This is called by the bus error exception
			handler.
			Disables bus error catching.
		 */
		bool	busError() noexcept;

	private:
		/** This operation is called to add all of the C++ exception
			handling variables to the Variable stack. Returns true
			when successful.
		 */
		bool /* success */ addExceptionVariables();
	};

}
}

#endif
