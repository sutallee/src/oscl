/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_suspendh_
#define _oscl_krux_thread_suspendh_
#include "oscl/krux/port/trap.h"

/** */
namespace Oscl {
/** */
namespace Krux {

class Thread;

/** Concrete trap that implements the yield service
	which gives up processor time to other threads within
	the same priority band as the invoking thread.
 */

class TrapSuspend : public Trap{
	private:
		/** This variable holds a reference to the target
			of the suspend operation.
		 */

		Thread&	_threadToSuspend;
	public:
		/** This constructor takes a reference to the target thread.
		 */

		TrapSuspend(Thread& thread) noexcept:_threadToSuspend(thread){};

		/** This constructor takes a pointer to the target thread.
		 */

		TrapSuspend(Thread* thread) noexcept:_threadToSuspend(*thread){};

		/** Implements Trap service interface, performing
			the actual suspend operation within the processor's
			supervisor mode.
		 */
		void suService(void) noexcept;
	};

}
}

#endif
