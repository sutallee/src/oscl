//oscl/krux/thread/thread.cpp#2 - edit change 952 (text)
/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdlib.h>
#include <string.h>
#include "thread.h"
#include "sched.h"
#include "yield.h"
#include "suspend.h"
#include "resume.h"
#include "oscl/krux/timer/sematimer.h"
#include "oscl/krux/mutex/super.h"
#include "oscl/krux/sema/bsema.h"
#include "oscl/mt/scopesync.h"

using namespace Oscl::Krux;

unsigned		Thread::nThreads;

/*****************************************************************************
 * Thread()
 */

Thread::Thread(	SchedPriBand*	priority,
				Mt::Runnable&	runnable,
				void*			stack,
				unsigned long	stackSize
				) noexcept
							:
#ifdef RUN_TIME_STACK_CHECK_ENABLED
							_stackLimit(	reinterpret_cast<unsigned long>(stack)
											+stackSize/10),
											_outOfBounds(false),
#endif
							_catchBusError(false),
							_busErrorCaught(false),
							_contextVariableList(0),
							_suspended(0),
							_runnable(runnable),
							_queued(false)
							{
	memset(stack,0xFF,stackSize);
	SupervisorMutex	mutex;
	_priority		= priority;
	_frame			= OsclKruxBuildStackFrame(*this,stack,stackSize);
	_queued			= true;
		{
		Mt::ScopeSync	ss(mutex);
		++Thread::nThreads;
		priority->put(this);
		}
	}

/*****************************************************************************
 * ~Thread()
 */

Thread::~Thread(){
	Variable*	next;
	while((next=getVar())){
		delete next;
		}
	}

/*****************************************************************************
 * yield()
 */

void	Thread::yield() noexcept {
	TrapYield	yield(this);

	yield.trap();
	}

/*****************************************************************************
 * suYield()
 */

void Thread::suYield(void) noexcept {
	_yield	= true;
	}

/*****************************************************************************
 * suspend()
 */

void	Thread::suspend() noexcept {
	TrapSuspend	suspend(this);

	suspend.trap();
	}

/*****************************************************************************
 * resume()
 */

void	Thread::resume() noexcept {
	TrapResume	resume(this);

	resume.trap();
	}

/*****************************************************************************
 * suSuspend()
 */

void Thread::suSuspend(void) noexcept {
	SupervisorMutex	mutex;
	mutex.lock();
	if(!_suspended){
		if(_queued){
			_priority->remove(this);
			_queued	= false;
			}
		}
	_suspended++;
	mutex.unlock();
	}

/*****************************************************************************
 * resume()
 */

void Thread::suResume(void) noexcept {
	SupervisorMutex	mutex;
	mutex.lock();
	if(_suspended){
		if(!(--_suspended)){
			if(!_queued){
				_queued	= true;
				_priority->put(this);
				}
			}
		}
	mutex.unlock();
	}

/*****************************************************************************
 * schedule()
 */

void Thread::suSchedule(void) noexcept {
	// Interrupts are off when this is called
	if(!_suspended){
		if(_yield){
			_yield	= false;
			if(!_queued){
				_queued	= true;
				_priority->put(this);
				}
			}
		else{
			if(!_queued){
				_queued	= true;
				_priority->push(this);
				}
			}
		}
	}

/*****************************************************************************
 * entry()
 */

void Thread::entry() noexcept {
	if(!addExceptionVariables()){
		suspend();
		}
	_runnable.run();
	}

/*****************************************************************************
 * addExceptionVariables()
 */

bool /* success */ Thread::addExceptionVariables(){
	Variable*		next;

	return true;
	while((next=getVar())){
		delete next;
		}
	return false;
	}

/*****************************************************************************
 * activate()
 */

void Thread::activate() noexcept {
	_queued	= false;
	Scheduler::GetScheduler().setCurrentThread(this);
	}

/*****************************************************************************
 * memoryFail()
 */

void Thread::memoryFail() noexcept {
	}

/*****************************************************************************
 * alloc()
 */

void*	Thread::alloc(unsigned long size) noexcept {
	Mt::ScopeSync	ss(Thread::mutex);
	return malloc(size);
	}

/*****************************************************************************
 * bfree()
 */

void	Thread::bfree(void *m) noexcept {
	Mt::ScopeSync	ss(Thread::mutex);
	free(m);
	}

/*****************************************************************************
 * getVar()
 */

Thread::Variable*	Thread::getVar(){
	Thread::Variable*	next;
	if((next=_contextVariableList)){
		_contextVariableList	= (Variable*)next->__qitemlink;
		}
	return next;
	}

/*****************************************************************************
 * addVar()
 */

bool/*success*/	Thread::addVar(void** variableAddress) noexcept {
	Variable*	v	= new Variable(variableAddress);
	if(!v) return false;
	v->__qitemlink		= _contextVariableList;
	_contextVariableList	= v;
	return true;
	}

/*****************************************************************************
 * saveExtraContext()
 */

void	Thread::saveExtraContext(){
	Variable*	v;
	for(v =_contextVariableList;v;v=(Variable*)v->__qitemlink){
		v->saveContext();
		}
	}

/*****************************************************************************
 * restoreExtraContext()
 */

void	Thread::restoreExtraContext(){
	Variable*	v;
	for(v =_contextVariableList;v;v=(Variable*)v->__qitemlink){
		v->restoreContext();
		}
	}

/*****************************************************************************
 * getSyncSignalApi()
 */

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

/*****************************************************************************
 * syncWait()
 */

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

/*****************************************************************************
 * getCurrent()
 */

Oscl::Mt::Thread&	Thread::getCurrent() noexcept{
	return *Scheduler::GetScheduler().getCurrentThread();
	}

/*****************************************************************************
 * getCurrentKernelThread()
 */

Thread&	Thread::getCurrentKernelThread() noexcept{
	return *Scheduler::GetScheduler().getCurrentThread();
	}

/*****************************************************************************
 * getName()
 */

const char* Thread::getName() noexcept{
	return "";
	}

/*****************************************************************************
  mutex
 */

Mutex::Semaphore		Thread::mutex;

#ifdef RUN_TIME_STACK_CHECK_ENABLED
/*****************************************************************************
 * stackOutOfBounds()
 */

bool	Thread::stackOutOfBounds(unsigned long sp) noexcept {
	if(sp < _stackLimit){
		_outOfBounds	= true;
		loop: goto loop;
		return true;
		}
	else{
		_outOfBounds	= false;
		return false;
		}
	}
#endif

void	Thread::catchBusError() noexcept {
	_catchBusError	= true;
	_busErrorCaught	= false;
	}

bool	Thread::caughtBusError() noexcept {
	_catchBusError		= false;
	bool	busError	= _busErrorCaught;
	_busErrorCaught		= false;
	return busError;
	}

bool	Thread::busError() noexcept {
	bool	catchBusError	= _catchBusError;
	_catchBusError	= false;
	_busErrorCaught	= true;
	return catchBusError;
	}

