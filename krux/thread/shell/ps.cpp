/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <stdio.h>
#include "ps.h"
#include "oscl/shell/line/cmdapi.h"
#include "oscl/krux/thread/thread.h"

using namespace Oscl::Ooos::Thread::Shell;

static const char	psHelp[]	= {" - Display thread statistics.\n\r"};      
static const char	cmdName[]	= {"ps"};      

PsCmd::PsCmd() noexcept
		{
	}

const char*	PsCmd::command() const noexcept{
	return cmdName;
	}

void	PsCmd::help(Oscl::Stream::Output::Api&	output) const noexcept{
	output.write(cmdName,sizeof(cmdName)-1);
	output.write(psHelp,sizeof(psHelp)-1);
	}

Oscl::Shell::Line::InputApi*
	PsCmd::execute(	Oscl::Shell::Line::StateHeader&	state,
					const char*						arguments
					) const noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"Total Threads: %u\n\r",
				Oscl::Krux::Thread::nThreads
				);
	state._output.write(buffer,strlen(buffer));
	return 0;
	}

