/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_thread_waiterh_
#define _oscl_krux_thread_waiterh_
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** Represents an abstract entity (a thread or group of threads) that
	can block pending an event.
 */
class Waiter : public QueueItem {
	public:
		/** virtual constructor to shut-up GCC.
		 */
		virtual ~Waiter() {}

		/** Called from supervisor-mode to cause the waiter to block.
		 */
		virtual void suWait(void) noexcept= 0;

		/** Called from supervisor-mode to cause the waiter to proceed.
		 */
		virtual void suProceed(void) noexcept= 0;
	};

}
}

#endif
