/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/frame.h"
#include "oscl/hw/hitachi/h83664/map.h"
#include "oscl/krux/port/trap.h"
#include "oscl/interrupt/handler.h"
#include "oscl/krux/sema/bbsema.h"

class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	typedef struct OOTrapHandlerArgs{
		Oscl::Krux::Trap	*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	}

extern "C" {
	OsclKruxFrame*	h8SystemCall(OsclKruxFrame& frame);
	OsclKruxFrame*	h8SystemTick(OsclKruxFrame& frame);
	}

OsclKruxFrame*	h8SystemCall(OsclKruxFrame& frame){
	unsigned char*	p	= (unsigned char*)&frame.r0;
	void*	trap	= &p[2];
	OOTrapHandler(trap);
	return &frame;
	}

OsclKruxFrame*	h8SystemTick(OsclKruxFrame& frame){
	extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;
	UKClockSema.suSignal();
	H83664_IRR1	= 0xBF;	// Clear timer A interrupt
	return &frame;
	}

