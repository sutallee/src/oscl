/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/frame.h"
#include "oscl/krux/port/trap.h"
#include "oscl/interrupt/handler.h"

class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	typedef struct OOTrapHandlerArgs{
		Oscl::Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	}

extern "C" {
	OsclKruxFrame*	avrSystemCall(OsclKruxFrame& frame);
	}

#if 0
/* Called from the PPC reset exception.
 */
OsclKruxFrame*	ppcReset(	bool				isMasterProcessor,
					Ppc::Oea::Srr0::Reg	nip,
					Ppc::Oea::Msr::Reg	srr1,
					Ppc::Oea::Msr::Reg	msr
					){
	// SRVABI:
	// The stack must be aligned to a 16 byte boudary.
	// The stack must point to a word location containing
	// a null frame pointer.
	// Note:
	//	The OsclKruxFrame structure type is NOT a part of the
	//	SRVABI.

	// Setup the system timing
	setupSCCR();

	// Initialize the BSS
	initializeBSS();

	// Initialize the Global Page
	initializeGlobalPage();

	// Setup external bus characteristics.
	setupSIUMCR();

	// The next three lines enable and initialize
	// the PPC decrementer. Since I've moved to using
	// the PIT for its rate monotonic characteristics,
	// I have the decrementer and timebase disabled for now.
	setupTBSCR();

	// Call pre-constructor initialization to setup
	// chip selects and that sort of stuff.
	OsclKruxPreConstructorInit();

	// Call main()
	extern int main(int argc,char *argv[]);
	main(0,0);	// this does not return

	// Should never get here.
	while(true);
	return 0;
	}

OsclKruxFrame*	ppcDecrementer(OsclKruxFrame& frame){
	// This should update the syst
//	Ppc::mtdec(1562500);	// 50MHz/(16*2) == 1s
	return &frame;
	}

OsclKruxFrame*	ppcExternalInterrupt(OsclKruxFrame& frame){
	extern Oscl::Interrupt::HandlerApi*	ExternalInterruptHandler;
	// Enable MMU to get correct cache attributes for
	// memory mapped I/O. Without doing this, things were...
	// unpredicable on the machine check front. Interrupts
	// would work a few times (PIT) and then I would miss
	// an interrupt status. I suspect I was using a cached
	// copy of the PICSR, since cache is enabled even when
	// the MMU is disabled. Changing the cache to write-through
	// helped (a little) but enable the MMU fixed things.

	// Finally, handle the interrupt.
	ExternalInterruptHandler->interrupt();

	return &frame;
	}
#endif

OsclKruxFrame*	avrSystemCall(OsclKruxFrame& frame){
	uint16_t	trap	= frame.r24;
	trap	|= (frame.r25<<8);
	OOTrapHandler((void*)trap);
	return &frame;
	}

