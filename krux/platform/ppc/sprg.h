/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/platform/ppc/frame.ahh"
#include "oscl/krux/platform/ppc/scratch.ahh"
#include "oscl/hw/powerpc/uisa.asm.h"
#include "oscl/hw/powerpc/oea.asm.h"

// This SPRG holds the SMP processor instance
// scratch area.
#define SmpScratchSprg		OsclPpcOeaSprSPRG0

// This SPRG holds the frame pointer / stack pointer
// during first level exception processing.
#define ExceptScratchSprg	OsclPpcOeaSprSPRG1

// This is used to hold the Thread/Frame pointer
// for now.
#define FrameSprg			OsclPpcOeaSprSPRG2

// This holds the SMP processor specific
// exception stack pointer base (top).
#define SmpStackPtrSprg		OsclPpcOeaSprSPRG3

// This just symbolically represents the GPR
// used as the frame/stack pointer.
#define StackPtrGpr			OsclPpcUisaGPR1

#define FramePtrGpr			OsclPpcUisaGPR3


