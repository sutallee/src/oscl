/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "syspage.h"

SystemPage::SystemPage() noexcept:
		_nFreePages(0),
		_nUsedPages(0),
		_nAllocedMmuPages(0),
		_nAllocedHeapPages(0)
		{
	extern const char		__FREE_PAGE_START__;
	extern unsigned char	SystemRamSize;	// Defined by linker script
	unsigned long		firstAligned;
	firstAligned	= (			((unsigned long)&__FREE_PAGE_START__)
							+	(OsclPageSize-1)) & ~(OsclPageSize-1)
							;
	const unsigned long		remainingRamSize =
		((unsigned long)&SystemRamSize) - firstAligned;
	const unsigned long		remainingPages	= remainingRamSize/OsclPageSize;
	OoosPage*	page	= (OoosPage*)firstAligned;
	for(unsigned i = 0;i<remainingPages;++i){
		freePages.put(&page[i]);
		}
	_nFreePages	= remainingPages;
	}

OoosPage*	SystemPage::allocHeap() noexcept{
	OoosPage*	page	= allocFreePage();
	if(page){
		++_nAllocedHeapPages;
		}
	return freePages.pop();
	}

OoosPage*	SystemPage::allocMMU() noexcept{
	OoosPage*	page=allocUsedPage();
	if(page){
		++_nAllocedMmuPages;
		return page;
		}
	page	= freePages.getLast();
	if(page){
		++_nAllocedMmuPages;
		}
	return page;
	}

void		SystemPage::freeMMU(OoosPage* page) noexcept{
	freeUsedPage(page);
	--_nAllocedMmuPages;
	}

unsigned	SystemPage::nFreePages() const noexcept{
	return _nFreePages;
	}

unsigned	SystemPage::nUsedPages() const noexcept{
	return _nUsedPages;
	}

unsigned	SystemPage::nAllocedMmuPages() const noexcept{
	return _nAllocedMmuPages;
	}

unsigned	SystemPage::nAllocedHeapPages() const noexcept{
	return _nAllocedHeapPages;
	}

OoosPage*	SystemPage::allocFreePage() noexcept{
	OoosPage*	page	= freePages.pop();
	if(page){
		--_nFreePages;
		}
	return freePages.pop();
	}

void		SystemPage::freeUsedPage(OoosPage* page) noexcept{
	freeUsedPages.put(page);
	++_nUsedPages;
	}

OoosPage*	SystemPage::allocUsedPage() noexcept{
	OoosPage*	page=freeUsedPages.get();
	if(page){
		--_nUsedPages;
		}
	return page;
	}

