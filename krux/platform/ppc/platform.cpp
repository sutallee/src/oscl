/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/trap.h"
#include "oscl/krux/port/platform.h"
#include "oscl/kernel/mmu.h"
#include "oscl/cache/operations.h"
#include "oscl/hw/powerpc/oea.h"
#include <stdint.h>
#include "mmu.h"
#include "page.h"

using namespace Oscl::Krux;
using namespace Oscl;

extern "C"{
	extern volatile unsigned long   GlobalMpLockVariable;
	extern void	StartOOOS(void);
	extern void	ThreadEntry(void);
	extern void	UnknownException(void);
	typedef struct OOTrapHandlerArgs{
		Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	uint16_t	InterruptNestLevel = ~0;
	}

volatile unsigned long   GlobalMpLockVariable;

/*****************************************************************************
 * OOTrapHandler()
 *	Called by Trap #0
 */

void	OOTrapHandler(void *usp) {
	((OOTrapHandlerArgs *)usp)->	trap->suService();
}

/*****************************************************************************
 * OsclKruxInit()
 *	Initialize platform layer.
 *	Called in supervisor mode.
 */

extern "C" void OsclKruxInit() noexcept {
	}

/*****************************************************************************
 * OsclKruxBuildStackFrame()
 *  Builds stack frame for thread.
 */

OsclKruxFrame* OsclKruxBuildStackFrame(	EntryPoint&		entry,
										void*			stack,
										unsigned long	stackSize
										) noexcept {
	OsclKruxFrame*
	frame	= (OsclKruxFrame*)(((uint32_t)((uint8_t *)stack+stackSize))-sizeof(OsclKruxFrame));

	frame->gpr[3]	= (uint32_t)&entry;
	frame->gpr[1]	= (uint32_t)frame;
	frame->nip		= (uint32_t)OsclKruxEntry;
	frame->link		= (uint32_t)0;
	frame->msr		= (uint32_t)(
							Ppc::Oea::Msr::Pow::ValueMask_Disabled
						|	Ppc::Oea::Msr::Ile::ValueMask_BigEndian
						|	Ppc::Oea::Msr::Ee::ValueMask_Enabled
						|	Ppc::Oea::Msr::Pr::ValueMask_Supervisor
						|	Ppc::Oea::Msr::Fp::ValueMask_Disabled
						|	Ppc::Oea::Msr::Me::ValueMask_Enabled
						|	Ppc::Oea::Msr::Fe0::ValueMask_Zero
						|	Ppc::Oea::Msr::Se::ValueMask_Normal
						|	Ppc::Oea::Msr::Be::ValueMask_Normal
						|	Ppc::Oea::Msr::Fe1::ValueMask_Zero
						|	Ppc::Oea::Msr::Ip::Value_X000
#ifdef KRUX_PPC_MMU_DISABLED
						|	Ppc::Oea::Msr::Ir::ValueMask_Disabled
						|	Ppc::Oea::Msr::Dr::ValueMask_Disabled
#else
						|	Ppc::Oea::Msr::Ir::ValueMask_Enabled
						|	Ppc::Oea::Msr::Dr::ValueMask_Enabled
#endif
						|	Ppc::Oea::Msr::Ri::ValueMask_Recoverable
						|	Ppc::Oea::Msr::Le::ValueMask_BigEndian
						);
	return frame;
	}

/*****************************************************************************
 * OsclKruxEntry()
 */

void OsclKruxEntry(EntryPoint *entry) noexcept{
	entry->entry();
	// If this ever returns it would be wise to
	// call currentThread->userSuspend()
	}

/*****************************************************************************
 * OsclKruxStartTrap()
 */

void OsclKruxStartTrap(OsclKruxFrame *frame) noexcept{
	asm volatile (	"	mr	3,%0\n"
					"	b	StartOOOS\n"
					: : "r" (frame)
					: "r3"
					);
}

/*****************************************************************************
 * OsclKernelVirtualToPhysical()
 */

void*	OsclKernelVirtualToPhysical(void* virtualAddress) noexcept{
	return MmuVirtualToPhysical(virtualAddress);
	}

/*****************************************************************************
 * OsclCacheInhibitRange()
 */

void	OsclCacheInhibitRange(void* virtualAddress,unsigned long size) noexcept{
	MmuCacheInhibitRange(virtualAddress,size);
	}

