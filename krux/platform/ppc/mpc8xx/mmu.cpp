/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/queue/queue.h"
#include "oscl/hw/motorola/mpc8xx/mmu/desc.h"
#include "oscl/hw/motorola/mpc8xx/mmu/epn.h"
#include "oscl/driver/motorola/mpc8xx/mmu/l1table.h"
#include "oscl/driver/motorola/mpc8xx/mmu/l2table.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mtwb.h"
#include "oscl/driver/powerpc/asm.h"
#include "oscl/krux/platform/ppc/mmu.h"
#include "oscl/driver/motorola/mpc8xx/mmu/spr.h"
#include "oscl/driver/motorola/mpc8xx/cache/iccst.h"
#include "oscl/driver/motorola/mpc8xx/cache/dccst.h"
#include "oscl/hw/est/mdp860/pci/map.h"
#include "oscl/krux/platform/ppc/syspage.h"
#include "oscl/cache/operations.h"
#include "oscl/mt/mutex/iss.h"

using namespace Oscl::Mot8xx::Mmu;

static Level2Descriptor*
mapMemoryPage(	Level1Twam1Table&	l1table,
				void*				virtAddr,
				void*				physAddr
				) noexcept{
	Level1Descriptor*	l1desc	= l1table.getDesc(virtAddr);
	if(!l1desc){
		while(true);
		}
	if(!l1desc->isValid()){
		Level2Table*	l2table	= (Level2Table*)OoosGlobalSystemPage.allocMMU();
		if(!l2table){
			while(true);
			}
		l2table	= new (l2table) Level2Table();
		l1desc->setLevel2Table(*l2table);
		l1desc->setAccessProtectionGroup(0);
		l1desc->setNonGuarded();
		l1desc->setPageSize4KByte();
		l1desc->setCacheCopyback();
		l1desc->setDescriptorValid();
		}
	Level2Table*		l2table	= l1desc->getLevel2Table();
	Level2Descriptor*	l2desc	= l2table->getDescTwam1(virtAddr);
	if(!l2desc){
		while(true);
		}
	if(l2desc->isValid()){
		while(true);
		}
	l2desc->setRealPage(physAddr);
	l2desc->setProtectionAllReadOnly();
	l2desc->setChanged();
	l2desc->set4KPageSize();
	l2desc->setSharedPage();
	l2desc->setCacheAllow();
	l2desc->setPageValid();
	return l2desc;
	}

static Level2Descriptor*
mapIoPage(	Level1Twam1Table&	l1table,
			void*				virtAddr,
			void*				physAddr
			) noexcept{
	Level1Descriptor*	l1desc	= l1table.getDesc(virtAddr);
	if(!l1desc){
		while(true);
		}
	if(!l1desc->isValid()){
		Level2Table*	l2table	= (Level2Table*)OoosGlobalSystemPage.allocMMU();
		if(!l2table){
			while(true);
			}
		l2table	= new (l2table) Level2Table();
		l1desc->setLevel2Table(*l2table);
		l1desc->setAccessProtectionGroup(0);
		l1desc->setGuarded();
		l1desc->setPageSize4KByte();
		l1desc->setCacheWritethrough();
		l1desc->setDescriptorValid();
		}
	Level2Table*		l2table	= l1desc->getLevel2Table();
	Level2Descriptor*	l2desc	= l2table->getDescTwam1(virtAddr);
	if(!l2desc){
		while(true);
		}
	if(l2desc->isValid()){
		while(true);
		}
	l2desc->setRealPage(physAddr);
	l2desc->setProtectionSuperReadWriteOnly();
	l2desc->setChanged();
	l2desc->set4KPageSize();
	l2desc->setSharedPage();
	l2desc->setCacheInhibit();
	l2desc->setPageValid();
	return l2desc;
	}

static void	mapKernelTextPage(	Level1Twam1Table&	l1table,
								void*				virtAddr,
								void*				physAddr
								) noexcept{
	Level2Descriptor*	l2desc	= mapMemoryPage(l1table,virtAddr,physAddr);
	if(!l2desc){
		while(true);
		}
	l2desc->setProtectionAllReadOnly();
	l2desc->setSharedPage();
	}

static void	mapKernelDataPage(	Level1Twam1Table&	l1table,
								void*				virtAddr,
								void*				physAddr
								) noexcept{
	Level2Descriptor*	l2desc	= mapMemoryPage(l1table,virtAddr,physAddr);
	if(!l2desc){
		while(true);
		}
	l2desc->setProtectionSuperReadWriteOnly();
	l2desc->setSharedPage();
	}

void initializeLevelOneTable(Level1Desc::Reg* table){
	for(unsigned i=0;i<1024;++i){
		table[i]	=	Level1Desc::V::Value_Invalid;
		}
	}

void initializeLevelTwoTable(Level2Desc::Reg* table){
	for(unsigned i=0;i<1024;++i){
		table[i]	=	Level2Desc::V::ValueMask_Invalid;
		}
	}

void create4kSupervisorTextPage(	Level1Desc::Reg*	pageTable,
									unsigned long		epn,
									unsigned long		rpn
									){
	unsigned long	level1Index;
	level1Index	=	(epn & Twam1::Epn::Level1Index::FieldMask)
					>>Twam1::Epn::Level1Index::Lsb;
	unsigned long	level2Index;
	level2Index	=	(epn & Twam1::Epn::Level2Index::FieldMask)
					>>Twam1::Epn::Level2Index::Lsb;
	
	const Level1Desc::Reg	settingsMask =
		 		Level1Desc::APG::FieldMask
			|	Level1Desc::G::FieldMask
			|	Level1Desc::PS::FieldMask
			|	Level1Desc::WT::FieldMask
			|	Level1Desc::V::FieldMask
			;
	const Level1Desc::Reg	desiredSettings =
		 		(0x0<<Level1Desc::APG::Lsb)
			|	Level1Desc::G::ValueMask_NonGuarded
			|	Level1Desc::PS::ValueMask_Small
			|	Level1Desc::WT::ValueMask_Writethrough
			|	Level1Desc::V::ValueMask_Valid
			;

	Level2Desc::Reg*	level2Table	= 0;
	if(		(pageTable[level1Index] & Level1Desc::V::FieldMask )
		==	Level1Desc::V::ValueMask_Valid
		){
		// Existing level 1 entry already exists
		if((pageTable[level1Index] & settingsMask) != desiredSettings) {
			// Existing level 1 attributes are incompatible
			// with text page.
			while(true);
			}
		level2Table	=	(Level2Desc::Reg*)
						(		pageTable[level1Index]
							&	Level1Desc::L2BA::FieldMask
							);
		}
	if(!level2Table){
		// Need to alocate a level 2 table entry
		level2Table	= (Level2Desc::Reg*) OoosGlobalSystemPage.allocMMU();
		if(!level2Table){
			// Can't allocate a page for the level 2 table.
			while(true);
			}
		initializeLevelTwoTable(level2Table);
		}
	if(		(level2Table[level2Index] & Level2Desc::V::FieldMask)
		==	Level2Desc::V::ValueMask_Valid
		){
		// Already allocated!
		while(true);
		}
	}

void initializePageTable(){
	// Allocate Level1 Table
	OoosPage*	page;
	page	= OoosGlobalSystemPage.allocMMU();
	if(!page){
		while(true);
		}
	Level1Twam1Table*	l1Table	= new (page) Level1Twam1Table();

	writeM_TWB(l1Table);
	}

void	InitializeMmu(){
	initializePageTable();
	}

void	EnableMmu(){
	MdCtr::Reg	mdctr	=		MdCtr::GPM::ValueMask_PowerPcMode
							|	MdCtr::PPM::ValueMask_ProtectPage
							|	MdCtr::CIDEF::ValueMask_Allow
							|	MdCtr::WTDEF::ValueMask_Copyback
							|	MdCtr::RSV4D::ValueMask_Modulo32
							|	MdCtr::TWAM::ValueMask_SubPage4K
							|	MdCtr::PPCS::ValueMask_Ignore
							|	(0<<MdCtr::DTLB_INDX::Lsb)
							;
	writeMD_CTR(mdctr);
	MiCtr::Reg	mictr	=		MiCtr::GPM::ValueMask_PowerPcMode
							|	MiCtr::PPM::ValueMask_ProtectPage
							|	MiCtr::CIDEF::ValueMask_Allow
							|	MiCtr::RSV4I::ValueMask_Modulo32
							|	MiCtr::PPCS::ValueMask_Ignore
							|	(0<<MiCtr::ITLB_INDX::Lsb)
							;
	writeMI_CTR(mictr);
	MxAp::Reg	ap	=
			MxAp::Gpm0::GP15::ValueMask_PageProtection
		|	MxAp::Gpm0::GP14::ValueMask_PageProtection
		|	MxAp::Gpm0::GP13::ValueMask_PageProtection
		|	MxAp::Gpm0::GP12::ValueMask_PageProtection
		|	MxAp::Gpm0::GP11::ValueMask_PageProtection
		|	MxAp::Gpm0::GP10::ValueMask_PageProtection
		|	MxAp::Gpm0::GP9::ValueMask_PageProtection
		|	MxAp::Gpm0::GP8::ValueMask_PageProtection
		|	MxAp::Gpm0::GP7::ValueMask_PageProtection
		|	MxAp::Gpm0::GP6::ValueMask_PageProtection
		|	MxAp::Gpm0::GP5::ValueMask_PageProtection
		|	MxAp::Gpm0::GP4::ValueMask_PageProtection
		|	MxAp::Gpm0::GP3::ValueMask_PageProtection
		|	MxAp::Gpm0::GP2::ValueMask_PageProtection
		|	MxAp::Gpm0::GP1::ValueMask_PageProtection
		|	MxAp::Gpm0::GP0::ValueMask_PageProtection
		;
	writeMI_AP(ap);
	writeMD_AP(ap);
	Oscl::Ppc::tlbia();
	Oscl::Ppc::sync();
	Oscl::Ppc::Oea::Msr::Reg	msr	= Oscl::Ppc::mfmsr();
	msr	|=		Oscl::Ppc::Oea::Msr::Ir::ValueMask_Enabled
			|	Oscl::Ppc::Oea::Msr::Dr::ValueMask_Enabled
			;
	Oscl::Ppc::mtmsr(msr);
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	Oscl::Ppc::isync();
	// Enable caches here
	Oscl::Mot8xx::Cache::InstructionCacheControl	iccst;
	Oscl::Mot8xx::Cache::DataCacheControl			dccst;
	dccst.unlockAll();
	iccst.unlockAll();
	Oscl::Ppc::eieio();
	dccst.invalidateAll();
	Oscl::Ppc::eieio();
	dccst.enableCache();
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	iccst.invalidateAll();
	Oscl::Ppc::eieio();
	iccst.enableCache();
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	Oscl::Ppc::isync();
	}

void	MapIoRange(void* address,unsigned long size) noexcept{
	Oscl::Mot8xx::Mmu::Mtwb::Reg	twb	= readM_TWB();
	twb	&= Oscl::Mot8xx::Mmu::Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	char*				mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		mapIoPage(*l1Table,mpage,mpage);
		}
	}

void	MapWriteableRamRange(void* address,unsigned long size) noexcept{
	Oscl::Mot8xx::Mmu::Mtwb::Reg	twb	= readM_TWB();
	twb	&= Oscl::Mot8xx::Mmu::Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	char*				mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		mapKernelDataPage(*l1Table,mpage,mpage);
		}
	}

void	MapReadOnlyRamRange(void* address,unsigned long size) noexcept{
	Oscl::Mot8xx::Mmu::Mtwb::Reg	twb	= readM_TWB();
	twb	&= Oscl::Mot8xx::Mmu::Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	char*				mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		mapKernelTextPage(*l1Table,mpage,mpage);
		}
	}

/** Returns zero if no translation. Note that this assumes page zero
	is not usable in VM which is true for this implementation.
 */
void*	MmuVirtualToPhysical(void* virtualAddress) noexcept{
	using namespace Oscl::Mot8xx::Mmu;
	Mtwb::Reg	twb	=	readM_TWB();
	twb				&=	Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	Level1Descriptor*	l1Desc	= l1Table->getDesc(virtualAddress);
	if(!l1Desc->isValid()){
		return 0;
		}
	Level2Table*		l2Table	= l1Desc->getLevel2Table();
	Level2Descriptor*	l2Desc	= l2Table->getDescTwam1(virtualAddress);
	if(l2Desc->isValid()){
		unsigned long	pageOffset	= (unsigned long)virtualAddress;
		pageOffset	&= l2Desc->getPageSizeMask();
		return (void*)(l2Desc->getRealPage()+pageOffset);
		}
	return 0;
	}

static void	cacheInhibitPage(	Level1Twam1Table&	l1Table,
								void*				virtAddr
								) noexcept{
	// FIXME: In SMP systems, the global
	Level1Descriptor*	l1Desc	= l1Table.getDesc(virtAddr);
	if(!l1Desc->isValid()){
		while(true);
		}
	Level2Table*		l2Table	= l1Desc->getLevel2Table();
	Level2Descriptor*	l2Desc	= l2Table->getDescTwam1(virtAddr);

		{
		Oscl::Mt::IntScopeSync	iss;

		if(!l2Desc->isValid()){
			while(true);
			}
		l2Desc->setPageInvalid();
		Oscl::Ppc::sync();
		l2Desc->setCacheInhibit();
		l2Desc->setPageValid();
		Oscl::Ppc::tlbie(virtAddr);
		Oscl::Ppc::eieio();
		Oscl::Ppc::tlbsync();
		Oscl::Ppc::sync();
		}
	}

void	MmuCacheInhibitRange(void* address,unsigned long size) noexcept{
	using namespace Oscl::Mot8xx::Mmu;
	Mtwb::Reg	twb	=	readM_TWB();
	twb				&=	Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	char*				mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		cacheInhibitPage(*l1Table,mpage);
		OsclCacheDataInvalidateRange((void*)mpage,(unsigned long)OsclPageSize);
		}
	}

