/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/hw/powerpc/uisa.h"
#include "oscl/hw/powerpc/oea.h"
#include "oscl/krux/port/frame.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/krux/thread/sched.h"
#include "oscl/krux/thread/thread.h"
#include "oscl/hw/powerpc/oea.h"
#include "oscl/krux/port/platform.h"
#include "oscl/krux/port/trap.h"
#include "oscl/driver/powerpc/asm.h"
#include "oscl/hw/est/mdp860/pci/map.h"
#include "oscl/interrupt/handler.h"
#include "oscl/krux/platform/ppc/syspage.h"
#include "oscl/driver/powerpc/asm.h"

class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	typedef struct OOTrapHandlerArgs{
		Oscl::Krux::Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	}

extern "C" {
	OsclKruxFrame*	ppcReset(	bool						isMasterProcessor,
								Oscl::Ppc::Oea::Srr0::Reg	nip,
								Oscl::Ppc::Oea::Msr::Reg	srr1,
								Oscl::Ppc::Oea::Msr::Reg	msr
								);
	OsclKruxFrame*	ppcMachineCheck(OsclKruxFrame& frame);
	OsclKruxFrame*	ppcSystemCall(OsclKruxFrame& frame);
	OsclKruxFrame*	ppcDecrementer(OsclKruxFrame& frame);
	OsclKruxFrame*	ppcExternalInterrupt(OsclKruxFrame& frame);
	OsclKruxFrame*	ppc8xxDataTlbError(OsclKruxFrame& frame);
	OsclKruxFrame*	ppc8xxInstructionTlbError(OsclKruxFrame& frame);
	}

static void initializeGlobalPage() noexcept{
	new (&OoosGlobalSystemPage) SystemPage();
	}

static void initializeBSS() noexcept{
	// Initialize the BSS
	extern char		__SBSS_START__;
	extern char		__SBSS_END__;
	char*	bssstart = &__SBSS_START__;
	for(;bssstart < &__SBSS_END__;++bssstart){
		*bssstart	= 0;
		}
	}

static void setupTimebase() noexcept{
	// The next three lines can be used to enable and
	// initialize the PPC decrementer. Currently,
	// I am using the PIT for its rate monotonic characteristics,
	// and thus have the decrementer and timebase disabled for now.
	// It seems to me that the decrementer would be useful for
	// time sharing multi-tasking, where rate monotonicity is
	// not required. I've disabled it to prevent the currently
	// unused interrupts it causes, since this is the only means
	// I can find of disabling the decrementer interrupts.
	Oscl::Ppc::mtdec(1000);
	}

static void setupSystemTiming() noexcept{
#if 0
	using namespace Oscl::Mot8xx;
	Mpc8xxQUICC.clkrstkey.sccrk	= ClkRstKey::SCCRK::Key;
	Mpc8xxQUICC.clkrst.sccr		=
			ClkRst::SCCR::COM::ValueMask_FullStrength
		|	ClkRst::SCCR::TBS::ValueMask_OscClkDiv
		|	ClkRst::SCCR::RTDIV::ValueMask_DivideBy4
		|	ClkRst::SCCR::RTSEL::ValueMask_OSCM
		|	ClkRst::SCCR::CRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::PRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::EBDF::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFSYNC::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFBRG::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFNL::ValueMask_DivideBy2
		|	ClkRst::SCCR::DFNH::ValueMask_DivideBy1
		;
#endif
	}

/* Called from the PPC reset exception.
 */
OsclKruxFrame*	ppcReset(	bool						isMasterProcessor,
							Oscl::Ppc::Oea::Srr0::Reg	nip,
							Oscl::Ppc::Oea::Msr::Reg	srr1,
							Oscl::Ppc::Oea::Msr::Reg	msr
							){
	// SRVABI:
	// The stack must be aligned to a 16 byte boudary.
	// The stack must point to a word location containing
	// a null frame pointer.
	// Note:
	//	The OsclKruxFrame structure type is NOT a part of the
	//	SRVABI.

	// Setup the system timing
	setupSystemTiming();

	// Initialize the BSS
	initializeBSS();

	// Initialize the Global Page
	initializeGlobalPage();

	// Setup external bus characteristics.
	//setupSIUMCR();

	// The next three lines enable and initialize
	// the PPC decrementer. Since I've moved to using
	// the PIT for its rate monotonic characteristics,
	// I have the decrementer and timebase disabled for now.
	setupTimebase();

	// Call pre-constructor initialization to setup
	// chip selects and that sort of stuff.
	OsclKruxPreConstructorInit();

	// Call main()
	extern int main(int argc,char *argv[]);
	main(0,0);	// this does not return

	// Should never get here.
	while(true);
	return 0;
	}

OsclKruxFrame*	ppcMachineCheck(OsclKruxFrame& frame){
	Oscl::Krux::Scheduler&	sched	= Oscl::Krux::Scheduler::GetScheduler();
	if(&sched == 0){
		while(true);
		}
	if(Oscl::Krux::Thread::getCurrentKernelThread().busError()){
		frame.nip		+= 4;
		}
	else{
		Oscl::Krux::Thread::getCurrentKernelThread().suSuspend();
		}
	return &frame;
	}

OsclKruxFrame*	ppcDecrementer(OsclKruxFrame& frame){
	// This should update the syst
//	Oscl::Ppc::mtdec(1562500);	// 50MHz/(16*2) == 1s
	return &frame;
	}

OsclKruxFrame*	ppcExternalInterrupt(OsclKruxFrame& frame){
	extern Oscl::Interrupt::HandlerApi*	ExternalInterruptHandler;
	// Enable MMU to get correct cache attributes for
	// memory mapped I/O. Without doing this, things were...
	// unpredicable on the machine check front. Interrupts
	// would work a few times (PIT) and then I would miss
	// an interrupt status. I suspect I was using a cached
	// copy of the PICSR, since cache is enabled even when
	// the MMU is disabled. Changing the cache to write-through
	// helped (a little) but enable the MMU fixed things.

	// Finally, handle the interrupt.
	ExternalInterruptHandler->interrupt();

	return &frame;
	}

OsclKruxFrame*	ppcSystemCall(OsclKruxFrame& frame){
	switch(frame.gpr[4]){
		case 0:
			if(		(frame.msr & Oscl::Ppc::Oea::Msr::Pr::FieldMask)
				==	Oscl::Ppc::Oea::Msr::Pr::ValueMask_Supervisor
				){
				OOTrapHandler((void*)&frame.gpr[3]);
				break;
				}
		default:
			Oscl::Krux::Thread::getCurrentKernelThread().suSuspend();
			};
	return &frame;
	}

OsclKruxFrame*	ppc8xxDataTlbError(OsclKruxFrame& frame){
	Oscl::Ppc::mfdsisr();
	Oscl::Ppc::mfdar();
	while(true);
	}

OsclKruxFrame*	ppc8xxInstructionTlbError(OsclKruxFrame& frame){
	while(true);
	}

