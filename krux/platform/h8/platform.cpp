/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/trap.h"
#include "oscl/krux/port/platform.h"
#include "oscl/krux/port/entry.h"
#include <stdint.h>
#include "oscl/kernel/mmu.h"

using namespace Oscl::Krux;
using namespace Oscl;

extern "C"{
	extern void	StartOOOS(void);
	extern void	ThreadEntry(void);
	extern void	UnknownException(void);
	typedef struct OOTrapHandlerArgs{
		Oscl::Krux::Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	uint16_t	InterruptNestLevel = ~0;
	}

/*****************************************************************************
 * OOTrapHandler()
 *	Called by Trap #0
 */

void	OOTrapHandler(void *usp)
{
	((OOTrapHandlerArgs *)usp)->trap->suService();
}

/*****************************************************************************
 * OsclKruxInit()
 *	Initialize platform layer.
 *	Called in supervisor mode.
 */

void OsclKruxInit() noexcept {
	}

/*****************************************************************************
 * OsclKruxBuildStackFrame()
 *  Builds stack frame for thread.
 */

OsclKruxFrame* OsclKruxBuildStackFrame(	EntryPoint&		entry,
								void*			stack,
								unsigned long	stackSize
								) noexcept {
	OsclKruxFrame*	frame;
	// Normal mode assumed
	uint16_t	stackTop	= reinterpret_cast<uint16_t>(stack);
	stackTop	+= stackSize;
	stackTop	-= sizeof(OsclKruxFrame);
	frame		= reinterpret_cast<OsclKruxFrame*>(stackTop);
	frame->ccr		= (uint8_t)0;
	frame->r7		= reinterpret_cast<uint16_t>(frame);
	frame->r6		= 0;
	frame->r5		= 0;
	frame->r4		= 0;
	frame->r3		= 0;
	frame->r2		= 0;
	frame->r1		= 0;
	frame->r0		= reinterpret_cast<uint16_t>(&entry);
	frame->pc		= (void(*)())OsclKruxEntry;
	return frame;
	}

/*****************************************************************************
 * OsclKruxEntry()
 */

void OsclKruxEntry(EntryPoint *entry) noexcept {
	entry->entry();
	// If this ever returns it would be wise to
	// call currentThread->userSuspend()
	}

/*****************************************************************************
 * OsclKruxStartTrap()
 */

void OsclKruxStartTrap(OsclKruxFrame *frame) noexcept{
#if 0
	asm volatile (	"	mr	3,%0\n"
					"	b	StartOOOS\n"
					: : "r" (frame)
					: "r3"
					);
#else
	asm volatile (	"	jmp	StartOOOS\n"
					: : "r" (frame)
					);
	for(;;);
#endif
}

/*****************************************************************************
 * OsclKernelVirtualToPhysical()
 */

void*	OsclKernelVirtualToPhysical(void* virtualAddress) noexcept{
	// Not on AVR!
	return 0;
	}

/*****************************************************************************
 * OsclCacheInhibitRange()
 */

void	OsclCacheInhibitRange(void* virtualAddress,unsigned long size) noexcept{
	// Not on AVR!
	}

/*****************************************************************************
 * OsclKernelGetPageSize()
 */

unsigned long	OsclKernelGetPageSize() noexcept{
	// Not on AVR!
	for(;;);
	return 0;
	}

/*****************************************************************************
 * OsclCacheGetLineSize()
 */

unsigned long	OsclCacheGetLineSize() noexcept{
	for(;;);
	return 0;
	}

