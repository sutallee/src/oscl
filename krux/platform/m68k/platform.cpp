/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/trap.h"
#include "oscl/krux/port/platform.h"
#include <stdint.h>
#include "handler.h"
#include "oscl/hw/motorola/68k/vectors.h"

using namespace Oscl::Krux;
using namespace Oscl;

extern "C"{
	extern void	StartOOOS(void);
	extern void	ThreadEntry(void);
	extern void	UnknownException(void);
	typedef struct OOTrapHandlerArgs{
		Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	uint16_t	InterruptNestLevel = ~0;
	}

/*****************************************************************************
 * OOTrapHandler()
 *	Called by Trap #0
 */

void	OOTrapHandler(void *usp)
{
	((OOTrapHandlerArgs *)usp)->trap->suService();
}

/*****************************************************************************
 * OsclKruxInit()
 *	Initialize platform layer.
 *	Called in supervisor mode.
 */

void OsclKruxInit() noexcept {
	extern M68xxxExceptionVectors*	vtable;
	M68xxxExceptionHandler	*handler = 0;

	for(int i=0;i<256;i++,handler++){
		*handler	= UnknownException;
		}

	vtable->byName.trapInstruction0			= Trap0Handler;
	vtable->byName.trapInstructionD			= UnknownException;
	vtable->byName.trapInstructionE			= UnknownException;
	vtable->byName.trapInstructionF			= UnknownException;
}

/*****************************************************************************
 * OsclKruxBuildStackFrame()
 *  Builds stack frame for thread.
 */

OsclKruxFrame* OsclKruxBuildStackFrame(	EntryPoint&		entry,
										void*			stack,
										unsigned long	stackSize
										) noexcept {
	OsclKruxFrame*
		frame = (OsclKruxFrame*)(((uint32_t)((uint8_t *)stack+stackSize))-sizeof(OsclKruxFrame));

	frame->a0		= (uint32_t)&entry;
	frame->sr		= 0x2000;
	frame->pc		= (void (*)(void))ThreadEntry;
	frame->format	= 0x0080;	// trap 0 vector

return frame;
}

/*****************************************************************************
 * OsclKruxEntry()
 */

void OsclKruxEntry(EntryPoint *entry) noexcept{
	entry->entry();
	/* If this ever returns it would be wise to call currentThread->userSuspend()
	 */
}

/*****************************************************************************
 * OsclKruxStartTrap()
 */

void OsclKruxStartTrap(OsclKruxFrame *frame) noexcept{
	extern OsclKruxFrame	SupervisorStackTop;
	void					*stackTop;
	

#define TRAP_0_OFFSET	0x80
#define TRAP_0_FORMAT	0x00

	stackTop	= &SupervisorStackTop;

	asm volatile (" move.l %0,%%ssp" : : "m" (stackTop));	// setup supervisor stack
	asm volatile (" move.w #0x80,%%sp@-" : :);				// push trap frame format
	asm volatile (" move.l %0,%%sp@-" : : "m" (frame));	// push user stack pointer
	asm volatile (" jmp	StartOOOS\n");
}

