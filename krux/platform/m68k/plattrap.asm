/*
*   Copyright (C) 2004 Michael Nelson Moran
*
*   This file is part of OSCL.
*
*   OSCL is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   OSCL is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
*   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
*   License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with OSCL; see the file COPYING.  If not, write to the Free
*   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
*
*   To negotiate other licensing arrangments/agreements contact
*   the copyright holder:
*
*   Michael N. Moran
*   218 Wilshire Terrace       mike@mnmoran.org
*   White, GA, USA 30184       http://mnmoran.org
*/

#include "exception.def"

        XDEF    UnknownException
UnknownException:
	bra	.


*
* Routine to start thread
*  A0 contains UKernThread pointer
*

		XREF	OsclKruxEntry
        XDEF    ThreadEntry
ThreadEntry:
		move.l		a0,-(sp)				Get USP from stack frame
		jsr			OsclKruxEntry
		bra			.						Should never return!

*
* Routine to start system traps
*

        XDEF    StartOOOS
StartOOOS:
		move.l		(sp)+,a0					Get USP from stack frame
		move		a0,usp						Update new USP
		RESTORE_CONTEXT
		rte									RETURN to highest priority task

*
* Exception Handler for Trap#0
*
        XREF    _OOTrapHandler

        XDEF    Trap0Handler
Trap0Handler:
        OO_TRAP_WRAPPER OOTrapHandler

