/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/frame.h"
#include "oscl/krux/port/trap.h"
#include "oscl/interrupt/handler.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/hw/ti/msp430/x16x/map.h"

/** */
class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	/** */
	typedef struct OOTrapHandlerArgs{
		/** */
		Oscl::Krux::Trap	*trap;
		}OOTrapHandlerArgs;

	/** */
	void	OOTrapHandler(void *usp);
	}

extern "C" {
	/** */
	OsclKruxFrame*	msp430SystemCall(OsclKruxFrame& frame);
	/** */
	OsclKruxFrame*	msp430SystemTick(OsclKruxFrame& frame);
	}

/** */
OsclKruxFrame*	msp430SystemCall(OsclKruxFrame& frame){
	void*	trap	= &frame.r15;
	OOTrapHandler(trap);
	return &frame;
	}

/** */
OsclKruxFrame*	msp430SystemTick(OsclKruxFrame& frame){
	using namespace Oscl::HW::TI::Msp430::x16x::TimerA;
	extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;

	switch(Msp430x16xSixteenBitPeripherals.TAIV){
		case TAIV::InterruptNotPending:
			return &frame;
		case TAIV::CaptureCompare1:
			for(;;);
			return &frame;
		case TAIV::CaptureCompare2:
			for(;;);
			return &frame;
		case TAIV::TimerOverflow:
			Msp430x16xSixteenBitPeripherals.timera.TACTL	&= ~(
				TACTL::TAIFG::FieldMask
				);
			UKClockSema.suSignal();
			return &frame;
		default:
			for(;;);
		}
	return &frame;
	}

