/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_platform_msp430_macroh_
#define _oscl_krux_platform_msp430_macroh_

	.macro	__saveContext
	push.w    r15
	push.w    r14
	push.w    r13
	push.w    r12
	push.w    r11
	push.w    r10
	push.w    r9
	push.w    r8
	push.w    r7
	push.w    r6
	push.w    r5
	push.w    r4
	.endm

	.macro	__restoreContext
	pop.w	r4
	pop.w	r5
	pop.w	r6
	pop.w	r7
	pop.w	r8
	pop.w	r9
	pop.w	r10
	pop.w	r11
	pop.w	r12
	pop.w	r13
	pop.w	r14
	pop.w	r15
	.endm

#endif
