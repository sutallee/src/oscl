/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_platform_xscale_syspageh_
#define _oscl_krux_platform_xscale_syspageh_
#include "page.h"
#include "oscl/queue/queue.h"

/** This class manages the physical RAM pages used
	by the kernel.
 */
class SystemPage {
	private:
		/** This is an ordered list of unallocated
			physical RAM pages. This list *may* contain
			holes. Once allocated, pages are never released
			to this list, but instead go into the freeUsedPages
			list.
		 */
		Oscl::Queue< KruxPage >	freePages;
		/** This is an unordered list of physical RAM pages
			that have been freed and may be re-used. This list
			starts as empty and is consulted first by the
			"heap" allocation operations.
		 */
		Oscl::Queue< KruxPage >	freeUsedPages;
		/** This is an unordered list of physical 1KB blocks.
			This list is used to allocate coarse Level2 page tables,
			which are 1KB in size.
		 */
		Oscl::Queue< CoarseLevel2TableMem >	coarseLevel2Mem;
		/** */
		unsigned				_nFreePages;
		/** */
		unsigned				_nUsedPages;
		/** */
		unsigned				_nAllocedMmuPages;
		/** */
		unsigned				_nAllocedHeapPages;
	public:
		/** */
		SystemPage() noexcept;
		/** This operation was added to enable the allocation of
			blocks of memory larger than a single page that must
			be the size and alignment of a power of 2. The impetus
			for this function is the need for a 16KB block for
			the MMU first level page table.
		 */
		void*	allocAlignedPow2MMU(unsigned exponent) noexcept;
		/** */
		KruxPage*	allocHeap() noexcept;
		/** */
		KruxPage*	allocMMU() noexcept;
		/** */
		CoarseLevel2TableMem*	allocCoarseLevel2MMU() noexcept;
		/** */
		void		freeMMU(KruxPage* page) noexcept;
	public:
		/** Returns the number of physical RAM pages in
			the freePages list.
		 */
		unsigned	nFreePages() const noexcept;
		/** Returns the number of physical RAM pages in
			the freeUsedPages list.
		 */
		unsigned	nUsedPages() const noexcept;
		/** Returns the number of pages currently in use
			by the users of the allocMMU,freeMMU, and allocAlignedPow2MMU
			operations.
		 */
		unsigned	nAllocedMmuPages() const noexcept;
		/** Returns the number of pages currently in use
			by the users of the allocHeap operation.
		 */
		unsigned	nAllocedHeapPages() const noexcept;
	private:
		/** */
		KruxPage*	allocFreePage() noexcept;
		/** */
		void		freeUsedPage(KruxPage* page) noexcept;
		/** */
		KruxPage*	allocUsedPage() noexcept;
	};

extern SystemPage	KruxGlobalSystemPage;	// Located in linker script

#endif
