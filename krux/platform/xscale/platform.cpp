/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/trap.h"
#include "oscl/krux/port/platform.h"
#include "oscl/kernel/mmu.h"
#include "oscl/cache/operations.h"
#include "oscl/hw/arm/psrreg.h"
#include <stdint.h>
#include "mmu.h"
#include "page.h"

#include "oscl/error/info.h"

using namespace Oscl::Krux;
using namespace Oscl;

extern "C"{
	extern volatile unsigned long   GlobalMpLockVariable;
	extern void	StartOOOS(void);
	extern void	ThreadEntry(void);
	extern void	UnknownException(void);
	typedef struct OOTrapHandlerArgs{
		Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	uint16_t	InterruptNestLevel = ~0;
	}

volatile unsigned long   GlobalMpLockVariable;

/*****************************************************************************
 * OOTrapHandler()
 *	Called by Trap #0
 */

void	OOTrapHandler(void *usp) {
#if 0
	OOTrapHandlerArgs*	args	= (OOTrapHandlerArgs*)usp;
#endif
//	Oscl::Error::Info::log("\n\rOOTrapHandler\n\r");
#if 0
	Oscl::Error::Info::hexDump((unsigned long)args);
	Oscl::Error::Info::log("\n\r");
	Oscl::Error::Info::hexDump((unsigned long)(args->trap));
	Oscl::Error::Info::log("\n\r");
	Oscl::Error::Info::hexDump(args->trap,32,(unsigned long)args->trap);
#endif
	((OOTrapHandlerArgs *)usp)->	trap->suService();
//	Oscl::Error::Info::log("\n\rOOTrapHandler exit\n\r");
}

/*****************************************************************************
 * OsclKruxInit()
 *	Initialize platform layer.
 *	Called in supervisor mode.
 */

extern "C" void OsclKruxInit() noexcept {
	}

/*****************************************************************************
 * OsclKruxBuildStackFrame()
 *  Builds stack frame for thread.
 */

OsclKruxFrame* OsclKruxBuildStackFrame(	EntryPoint&		entry,
										void*			stack,
										unsigned long	stackSize
										) noexcept {
	OsclKruxFrame*
	frame	= (OsclKruxFrame*)(((uint32_t)((uint8_t *)stack+stackSize))-sizeof(OsclKruxFrame));

	frame->r[0]		= (uint32_t)&entry;
	frame->r[1]		= (uint32_t)0;
	frame->r[2]		= (uint32_t)0;
	frame->r[3]		= (uint32_t)0;
	frame->r[4]		= (uint32_t)0;
	frame->r[5]		= (uint32_t)0;
	frame->r[6]		= (uint32_t)0;
	frame->r[7]		= (uint32_t)0;
	frame->r[8]		= (uint32_t)0;
	frame->r[9]		= (uint32_t)0;
	frame->r[10]	= (uint32_t)0;
	frame->r[11]	= (uint32_t)0;
	frame->ip		= (uint32_t)0;
	frame->sp		= (uint32_t)frame;
	frame->pc		= (uint32_t)OsclKruxEntry;
	frame->lr		= (uint32_t)0;
	frame->spsr		= (uint32_t)(
							ARM::PSR::N::ValueMask_Positive
						|	ARM::PSR::Z::ValueMask_NotZero
						|	ARM::PSR::C::ValueMask_NotCarry
						|	ARM::PSR::V::ValueMask_NotOverflow
						|	ARM::PSR::Q::Value_NotOverflow
						|	ARM::PSR::I::ValueMask_InterruptsEnabled
						|	ARM::PSR::F::ValueMask_InterruptsDisabled
						|	ARM::PSR::T::ValueMask_ArmExecution
						|	ARM::PSR::M::ValueMask_System
						);
	return frame;
	}

/*****************************************************************************
 * OsclKruxEntry()
 */

void OsclKruxEntry(EntryPoint *entry) noexcept{
	entry->entry();
	// If this ever returns it would be wise to
	// call currentThread->userSuspend()
	}

/*****************************************************************************
 * OsclKruxStartTrap()
 */

void OsclKruxStartTrap(OsclKruxFrame *frame) noexcept{
	asm volatile (	"	mov	r0,%0\n"
					"	b	StartOOOS\n"
					: : "r" (frame)
					: "r0"
					);
}

/*****************************************************************************
 * OsclKernelVirtualToPhysical()
 */

void*	OsclKernelVirtualToPhysical(void* virtualAddress) noexcept{
	return MmuVirtualToPhysical(virtualAddress);
	}

/*****************************************************************************
 * OsclCacheInhibitRange()
 */

void	OsclCacheInhibitRange(void* virtualAddress,unsigned long size) noexcept{
	MmuCacheInhibitRange(virtualAddress,size);
	}

/*****************************************************************************
 * OsclKernelGetPageSize()
 */

unsigned long	OsclKernelGetPageSize() noexcept{
	return OsclPageSize;
	}

/*****************************************************************************
 * OsclCacheGetLineSize()
 */

unsigned long	OsclCacheGetLineSize() noexcept{
	// The IXP4xx has a 32 byte cache line size.
	return 32;
	}

