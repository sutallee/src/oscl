/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/krux/port/frame.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/krux/thread/sched.h"
#include "oscl/krux/thread/thread.h"
#include "oscl/krux/port/platform.h"
#include "oscl/krux/port/trap.h"
#include "oscl/hw/arm/psrreg.h"
#if 0
#include "oscl/hw/powerpc/oea.h"
#include "oscl/driver/tundra/qspan/qspan.h"
#include "oscl/hw/powerpc/oea.h"
#include "oscl/driver/powerpc/asm.h"
#include "oscl/hw/est/mdp860/pci/map.h"
#include "oscl/hw/motorola/mpc8xx/sitkeyreg.h"
#include "oscl/driver/motorola/mpc8xx/spr.h"
#include "oscl/driver/powerpc/asm.h"
#endif
#include "oscl/interrupt/handler.h"
#include "syspage.h"
#include "oscl/hw/linksys/nslu2/map.h"
#include "oscl/hw/national/uart/pc16550.h"
#include "oscl/error/info.h"

extern "C" void	printConsoleCharacter(char	c){
	volatile Oscl::IXP4XX::UART::HighSpeed::LSR::Reg& lsr			= IntelIxp4xxHighSpeedUART.lsr;
	volatile Oscl::IXP4XX::UART::HighSpeed::RBR_THR_DLL::Reg& thr	= IntelIxp4xxHighSpeedUART.rbr_thr_dll;
	while((lsr & Oscl::HW::National::UART::PC16550::LSR::TEMT::FieldMask) == Oscl::HW::National::UART::PC16550::LSR::TEMT::ValueMask_NotEmpty);
	thr	= c;
	}

extern "C" void dumpTheFrame(OsclKruxFrame& frame) {
#if 0
	Oscl::Error::Info::log("R0: ");
	Oscl::Error::Info::hexDump(frame.r[0]);
	Oscl::Error::Info::log("\n\rR1: ");
	Oscl::Error::Info::hexDump(frame.r[1]);
	Oscl::Error::Info::log("\n\rR2: ");
	Oscl::Error::Info::hexDump(frame.r[2]);
	Oscl::Error::Info::log("\n\rR3: ");
	Oscl::Error::Info::hexDump(frame.r[3]);
	Oscl::Error::Info::log("\n\rR4: ");
	Oscl::Error::Info::hexDump(frame.r[4]);
	Oscl::Error::Info::log("\n\rR5: ");
	Oscl::Error::Info::hexDump(frame.r[5]);
	Oscl::Error::Info::log("\n\rR6: ");
	Oscl::Error::Info::hexDump(frame.r[6]);
	Oscl::Error::Info::log("\n\rR7: ");
	Oscl::Error::Info::hexDump(frame.r[7]);
	Oscl::Error::Info::log("\n\rR8: ");
	Oscl::Error::Info::hexDump(frame.r[8]);
	Oscl::Error::Info::log("\n\rR9: ");
	Oscl::Error::Info::hexDump(frame.r[9]);
	Oscl::Error::Info::log("\n\rR10: ");
	Oscl::Error::Info::hexDump(frame.r[10]);
	Oscl::Error::Info::log("\n\rR11: ");
	Oscl::Error::Info::hexDump(frame.r[11]);
	Oscl::Error::Info::log("\n\rIP: ");
	Oscl::Error::Info::hexDump(frame.ip);
	Oscl::Error::Info::log("\n\rSP: ");
	Oscl::Error::Info::hexDump(frame.sp);
	Oscl::Error::Info::log("\n\rLR: ");
	Oscl::Error::Info::hexDump(frame.lr);	// LR
	Oscl::Error::Info::log("\n\rPC: ");
	Oscl::Error::Info::hexDump(frame.pc);	// PC
	Oscl::Error::Info::log("\n\rSPSR: ");
	Oscl::Error::Info::hexDump(frame.spsr);
	Oscl::Error::Info::log("\n\rFrame: ");
	Oscl::Error::Info::hexDump((unsigned long)&frame);
	Oscl::Error::Info::log("\n\r*R0: ");
	Oscl::Error::Info::hexDump(*(unsigned long*)frame.r[0]);
	Oscl::Error::Info::log("\n\r");
//	Oscl::Error::Info::hexDump((const void*)0x008030F8,4,(unsigned long)0x008030F8);
#endif
	}

class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	typedef struct OOTrapHandlerArgs{
		Oscl::Krux::Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	}

extern "C" {
	extern "C" int _start(void);
	extern "C" int _init(void);
	OsclKruxFrame*	armReset(void);
	OsclKruxFrame*	armSystemCall(OsclKruxFrame& frame);
	OsclKruxFrame*	armInterrupt(OsclKruxFrame& frame);
	}

static void initializeGlobalPage() noexcept{
//	printConsoleCharacter('1');
	new (&KruxGlobalSystemPage) SystemPage();
	}

static void initializeBSS() noexcept{
	// Initialize the BSS
	extern char		__SBSS_START__;
	extern char		__SBSS_END__;
	char*	bssstart = &__SBSS_START__;
	for(;bssstart < &__SBSS_END__;++bssstart){
		*bssstart	= 0;
		}
	}

static void setupExternalBus() noexcept{
#if 0
	// FIXME: This routine demonstrates the
	// configuration needed to access the QSPAN
	// PCI bridge. It should be moved to a
	// platform specific location.
	// Chip selects must also be configured properly.
	// In particular the QSPAN configuration space
	// chip select must be set to enable BIH and
	// use external TA.
	// Known good value: 0x00610900
	using namespace Oscl::Mot8xx::Siu::SIUMCR;
	Mpc8xxQUICC.siu.siumcr	=
			EARB::ValueMask_InternalArbitration
		|	EARP::ValueMask_HighestPriority
		|	DSHW::ValueMask_DisableInternalDataShowCycles
		|	DBGC::ValueMask_VFLS
		|	DBPC::ValueMask_Mode00
		|	FRC::ValueMask_FRZ
		|	DLK::ValueMask_Locked
		|	OPAR::ValueMask_EvenParity
		|	PNCS::ValueMask_DisableParity
		|	DPC::ValueMask_IRQ
		|	MPRE::ValueMask_IRQ
		|	MLRC::ValueMask_KrRetry	// Required for QSPAN
		|	AEME::ValueMask_ExternalMasterDisabled
		|	SEME::ValueMask_ExternalMasterEnabled	// Required for QSPAN
		|	BSC::ValueMask_Dedicated
		|	GB5E::ValueMask_BDIP
		|	B2DD::ValueMask_CS2Only
		|	B3DD::ValueMask_CS3Only
		;
#endif
	}

static void setupSystemTimer() noexcept{
#if 0
	using namespace Oscl::Mot8xx;
	// The next three lines can be used to enable and
	// initialize the PPC decrementer. Currently,
	// I am using the PIT for its rate monotonic characteristics,
	// and thus have the decrementer and timebase disabled for now.
	// It seems to me that the decrementer would be useful for
	// time sharing multi-tasking, where rate monotonicity is
	// not required. I've disabled it to prevent the currently
	// unused interrupts it causes, since this is the only means
	// I can find of disabling the decrementer interrupts.
	Mpc8xxQUICC.sitkey.tbk		= SitKey::TBK::Key;
	Mpc8xxQUICC.sitimer.tbscr	= SiTimer::TBSCR::TBE::ValueMask_Enable;
	Oscl::Ppc::mtdec(1000);
#endif
	}

static void setupSystemTiming() noexcept{
#if 0
	using namespace Oscl::Mot8xx;
	Mpc8xxQUICC.clkrstkey.sccrk	= ClkRstKey::SCCRK::Key;
	Mpc8xxQUICC.clkrst.sccr		=
			ClkRst::SCCR::COM::ValueMask_FullStrength
		|	ClkRst::SCCR::TBS::ValueMask_OscClkDiv
		|	ClkRst::SCCR::RTDIV::ValueMask_DivideBy4
		|	ClkRst::SCCR::RTSEL::ValueMask_OSCM
		|	ClkRst::SCCR::CRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::PRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::EBDF::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFSYNC::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFBRG::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFNL::ValueMask_DivideBy2
		|	ClkRst::SCCR::DFNH::ValueMask_DivideBy1
		;
#endif
	}

extern "C" void __printWord(void);
/* Called from the ARM reset exception.
 */
OsclKruxFrame*	armReset(void){
	// SRVABI:
	// The stack must be aligned to a 16 byte boudary.
	// The stack must point to a word location containing
	// a null frame pointer.
	// Note:
	//	The OsclKruxFrame structure type is NOT a part of the
	//	SRVABI.

//	printConsoleCharacter('a');

	// Setup the system timing
	setupSystemTiming();

//	printConsoleCharacter('b');

	// Initialize the BSS
	initializeBSS();

//	printConsoleCharacter('c');
	// Initialize the Global Page
	initializeGlobalPage();

//	printConsoleCharacter('d');
	// Setup external bus characteristics.
	setupExternalBus();

//	printConsoleCharacter('e');
	// The next three lines enable and initialize
	// the PPC decrementer. Since I've moved to using
	// the PIT for its rate monotonic characteristics,
	// I have the decrementer and timebase disabled for now.
	setupSystemTimer();

//	printConsoleCharacter('f');
	// Call pre-constructor initialization to setup
	// chip selects and that sort of stuff.
	OsclKruxPreConstructorInit();

//	printConsoleCharacter('g');
	// Call main()
#if 0
	extern int main(int argc,char *argv[]);
	main(0,0);	// this does not return
#else
//	printConsoleCharacter('h');
#if 0
	_start();
#else
	_init();
	extern int main(int argc,char *argv[]);
	main(0,0);	// this does not return
#endif
#endif

//	printConsoleCharacter('z');
	// Should never get here.
	while(true);
	return 0;
	}

OsclKruxFrame*	armInterrupt(OsclKruxFrame& frame){
	extern Oscl::Interrupt::HandlerApi*	ExternalInterruptHandler;
	// Enable MMU to get correct cache attributes for
	// memory mapped I/O. Without doing this, things were...
	// unpredicable on the machine check front. Interrupts
	// would work a few times (PIT) and then I would miss
	// an interrupt status. I suspect I was using a cached
	// copy of the PICSR, since cache is enabled even when
	// the MMU is disabled. Changing the cache to write-through
	// helped (a little) but enable the MMU fixed things.

//	Oscl::Error::Info::log("\n\rarmInterrupt\n\r");
	dumpTheFrame(frame);

	// Finally, handle the interrupt.
	ExternalInterruptHandler->interrupt();
//	Oscl::Error::Info::log("exit\n\r");

	return &frame;
	}

OsclKruxFrame*	armSystemCall(OsclKruxFrame& frame){
//	Oscl::Error::Info::log("\n\rarmSystemCall\n\r");
	dumpTheFrame(frame);
	if(		(frame.spsr & Oscl::ARM::PSR::M::FieldMask)
		==	Oscl::ARM::PSR::M::ValueMask_System
		){
		OOTrapHandler((void*)&frame.r[0]);
//		Oscl::Error::Info::log("exit\n\r");
		}
	else {
//		printConsoleCharacter('Z');
		for(;;);
		}
	return &frame;
	}

