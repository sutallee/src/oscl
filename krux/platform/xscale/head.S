/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

// The cpwait macro is defined for the Intel XSCALE
// to ensure that the CP15 coprocessor has completed
// the previous CP15 update/write operation before
// continuing.
	.macro	cpwait,	freereg
	mrc	p15,0,\freereg,c2,c0,0	// arbitrary read of a cp15 register
	mov	\freereg, \freereg		// wait for it (dependency causes core to 
								// wait for co-processor)
	sub	pc, pc, #4				// branch to next instruction (effectively
								// flushes the instruction pipeline.
	.endm

	.text
.=0x0000
	.globl	__armExceptionVectors
__armExceptionVectors:
	bl	__armReset	// I'm using the LR further down
	b	__armUndefinedInstruction
	b	__armSoftwareInterrupt
	b	__armPrefetchAbort
	b	__armDataAbort
	b	__someBulshitVector
	b	__armIrq
	b	__armFiq

__armReset:
	.globl	__armReset
	// First! Let's disable the MMU!
	mrc	p15,0,r0,c1,c0,0	// Read the CP15 control register
	bic	r0,r0,#1			// Clear the M (MMU enable) bit
	mcr	p15,0,r0,c1,c0,0
	cpwait	r0
	// I expect the link register (LR) to contain the
	// address of the ARM "undefined instruction" vector
	// at this point.
	mov		r0,#0		// destination
	sub		r1,lr,#4	// source (__armExceptionVectors)
	ldr		r3,theEnd
	add		r3,r3,#3
	mov		r3,r3,lsr #2
loop:
	cmp		r3,#0
	beq		done
	ldr		r4,[r1], #+4
	str		r4,[r0], #+4
	subs	r3,r3,#1
	b		loop
done:
	ldr		sp,stackPage
	ldr		pc,absoluteArmReset

// Character in r10
// Uses registers r8,r9, and r10
__printChar:
	ldr		r9,consoleUartBase
__txemptyLoop:
	ldr		r8,[r9,#20]	// read LSR
						// test bit 6 (TEMT: Transmitter Empty)
	tst		r8,#64
	beq		__txemptyLoop
	str		r10,[r9,#0]
	bx		lr


	.globl	__printWord
__printWord:
// Word in r10
// Uses registers r7,r8,r9, r10,r11,r12
	mov		r11,lr	// save LR
	mov		r12,r10	// Get copy of R0
	mov		r12,r12,ror #28
	mov		r7,#8
__printWordLoop:
	mov		r10,r12
	and		r10,r10,#15
	add		r10,r10,#48
	cmp		r10,#57
	addgt	r10,r10,#7
	bl		__printChar
	mov		r12,r12,ror #28
	mov		r10,r12
	sub		r7,r7,#1
	cmp		r7,#0
	bne		__printWordLoop
	mov		r10,#10
	bl		__printChar
	mov		r10,#13
	bl		__printChar
	mov		lr,r11
	bx		lr

registerPtr:
	.long	register0
register0:
	.long	0
register1:
	.long	0
register2:
	.long	0
register3:
	.long	0
register4:
	.long	0
register5:
	.long	0
register6:
	.long	0
register7:
	.long	0
register8:
	.long	0
register9:
	.long	0
register10:
	.long	0
register11:
	.long	0
register12:
	.long	0
register13:
	.long	0
register14:
	.long	0
register15:
	.long	0
regCPSR:
	.long	0

	.globl	regDump
regDump:
	str	r0,register0
	ldr	r0,registerPtr
	stmib	r0,{r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,ip,sp,lr,pc}
	mov	r10,#10
	bl	__printChar
	mov	r10,#13
	bl	__printChar
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'0'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#0]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#4]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'2'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#8]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'3'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#12]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'4'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#16]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'5'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#20]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'6'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#24]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'7'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#28]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'8'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#32]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'9'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#36]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#'0'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#40]
	bl	__printWord
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#44]
	bl	__printWord
	mov	r10,#'I'
	bl	__printChar
	mov	r10,#'P'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#48]
	bl	__printWord
	mov	r10,#'S'
	bl	__printChar
	mov	r10,#'P'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#52]
	bl	__printWord
	mov	r10,#'L'
	bl	__printChar
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#56]
	bl	__printWord
	mov	r10,#'P'
	bl	__printChar
	mov	r10,#'C'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	ldr	r10,[r0,#60]
	bl	__printWord
	mov	r10,#'C'
	bl	__printChar
	mov	r10,#'P'
	bl	__printChar
	mov	r10,#'S'
	bl	__printChar
	mov	r10,#'R'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrs	r10,CPSR
	bl	__printWord

	// Main ID Register
	mov	r10,#'0'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c0,c0,0
	bl	__printWord

	// Cache Type Register
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c0,c0,1
	bl	__printWord

	// Control Register
	mov	r10,#'1'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c1,c0,0
	bl	__printWord

	// Translation base
	mov	r10,#'2'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c2,c0,0
	bl	__printWord

	// Domain Access Control
	mov	r10,#'3'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c3,c0,0
	bl	__printWord

	// Fault Status
	mov	r10,#'4'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c5,c0,0
	bl	__printWord

	// Fault Address
	mov	r10,#'5'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c6,c0,0
	bl	__printWord

	// Data TLB Lockdown
	mov	r10,#'6'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c10,c0,0
	bl	__printWord

	// Instruction TLB Lockdown
	mov	r10,#'7'
	bl	__printChar
	mov	r10,#' '
	bl	__printChar
	mrc	p15,0,r10,c10,c0,1
	bl	__printWord

	b	.

returnFromReset:
	mov	ip,sp
	mov	r10,sp
	bl	__printWord
	stmdb   sp!, {fp, ip, lr, pc}
	mov	r10,#0xAA
	bl	__printWord
	mrs		r10,SPSR
	bl		__printWord
	b	.
	.globl	idata_end
stackPage:
	.long	__SUPER_STACK_END__
	.long	__SUPER_STACK_PAGE__
theStart:
	.long	__armExceptionVectors
theEnd:
	.long	idata_end
consoleUartBase:
	.long	0xC8000000
loadBase:
	.long	0x1600000
absoluteArmReset:
	.long	armReset
absoluteReturnFromReset:
	.long	returnFromReset
__armUndefinedInstruction:
	b	.

currentFramePtr:
	.long	currentFrame

__armSoftwareInterrupt:
	ldr		sp,stackPage		// Get the stack page in the SP
	stmdb	sp!,{lr}			// Save link register
	ldr		lr,currentFramePtr	// get pointer to the frame pointer
	ldr		lr,[lr]				// get the current frame pointer
	stmia	lr,{r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,ip,sp,lr}^ // Save the user frame
	mrs		r0,SPSR
	str		r0,[lr,#64]
	ldmia	sp!,{r0}			// Get saved link register
	str		r0,[lr,#60]			// Store as PC
	mov		r0,lr				// Setup frame pointer as argument
	bl		armSystemCall
	bl		Schedule
	// I considered only saving non-volatile registers here if the
	// frame pointer returned by Schedule is different from the
	// one stored in currentFramePtr. However, due to the limited
	// addressing modes and sequential nature of the ldm/stm instructions,
	// I decided that it would be too much work/math and may require
	// that I change the layout of the Frame structure.
	ldr		lr,currentFramePtr	// get pointer to the frame pointer
	str		r0,[lr]				// save the frame pointer
	mov		lr,r0			// Move new frame pointer to link register
	ldr		r0,[lr,#64]		// Get SPSR
	msr		spsr_cxsf,r0	// Load SPSR
	ldmia	lr, {r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14}^
	ldr		lr,[lr,#60]		// Get the PC in lr
	nop						// IMPORTANT!!! Cannot use backed register
							// immediately after user mode LDM
							// See note on page A9-18
	movs	pc,lr
	
__armPrefetchAbort:
	bl		regDump
	b	.
__armDataAbort:
	b		regDump
__someBulshitVector:
	mov		r10,#'b'
	bl		__printChar
	mov		r10,#'u'
	bl		__printChar
	mov		r10,#'l'
	bl		__printChar
	mov		r10,#'l'
	bl		__printChar
	mov		r10,#'s'
	bl		__printChar
	mov		r10,#'h'
	bl		__printChar
	mov		r10,#'i'
	bl		__printChar
	mov		r10,#'t'
	bl		__printChar
	b		regDump;
__armIrq:
	ldr		sp,stackPage
	stmdb	sp!,{lr}			// Save link register
	ldr		lr,currentFramePtr	// get frame pointer
	ldr		lr,[lr]				// get the current frame pointer
	// Save the frame
	stmia	lr,{r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,ip,sp,lr}^
	mrs		r0,SPSR				// Get the user CPSR/SPSR
	str		r0,[lr,#64]			// Save the SPSR
	ldmia	sp!,{r0}			// Get saved link register
	sub		r0,r0,#4			// Adjust the return address accordingly
	str		r0,[lr,#60]			// Store as PC
	mov		r0,lr				// Setup frame pointer as argument
	bl		armInterrupt
	bl		Schedule
	ldr		lr,currentFramePtr
	str		r0,[lr]
	mov		lr,r0			// Move new frame pointer to link register
	ldr		r0,[lr,#64]		// Get SPSR
	msr		spsr_cxsf,r0	// Load SPSR
	ldmia	lr, {r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14}^
	ldr		lr,[lr,#60]		// Get the PC in lr
	nop						// IMPORTANT!!! Cannot use backed register
							// immediately after user mode LDM
							// See note on page A9-18
	movs	pc,lr

__armFiq:
	b		regDump
	.bss
	.globl	currentFrame
currentFrame:
	.long	0
