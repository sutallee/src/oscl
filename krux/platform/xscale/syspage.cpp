/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "syspage.h"
#include "oscl/error/fatal.h"

extern "C" void	printConsoleCharacter(char	c);

SystemPage::SystemPage() noexcept:
		_nFreePages(0),
		_nUsedPages(0),
		_nAllocedMmuPages(0),
		_nAllocedHeapPages(0)
		{
	extern const char		__FREE_PAGE_START__;
	extern const char		___data_start;
	extern unsigned char	SystemRamSize;
	unsigned long		firstAligned;
	firstAligned	= (			((unsigned long)&__FREE_PAGE_START__)
							+	(OsclPageSize-1)) & ~(OsclPageSize-1)
							;
	const unsigned long		remainingRamSize =
		((unsigned long)&SystemRamSize + (unsigned long)&___data_start) - firstAligned;
	const unsigned long		remainingPages	= remainingRamSize/OsclPageSize;
	KruxPage*	page	= (KruxPage*)firstAligned;
	for(unsigned i = 0;i<remainingPages;++i){
		freePages.put(&page[i]);
		}
	_nFreePages	= remainingPages;
	}

void*	SystemPage::allocAlignedPow2MMU(unsigned exponent) noexcept{
	unsigned	sizeInBytes	= 1U<<exponent;
	unsigned	sizeInPages	= sizeInBytes/OsclPageSize;
	unsigned long	alignmentMask	= sizeInBytes-1;
	Oscl::Queue< KruxPage >	allocedPages;
	Oscl::Queue< KruxPage >	triedPages;
	// The outer loop tries to find an initial suitably aligned
	// page. It is assumed that the freePages queue has been
	// filled from lowest address to highest address, such that
	// as pages are removed from the head of the queue, they will
	// be extracted in order of ascending addresses.
	for(;;){
		unsigned long	address;
		for(;;){
			KruxPage*	page	= freePages.get();
			if(!page){
				// Can't allocate block
				while((page=triedPages.pop())){
					freePages.push(page);
					}
				Oscl::ErrorFatal::logAndExit("allocAlignedPow2MMU fail 1\n\r");
				return 0;
				}
			address	= (unsigned long)page;
			if((address & alignmentMask) == 0){
				// We've found an aligned starting page
				allocedPages.put(page);
				break;
				}
			triedPages.push(page);
			}
		unsigned long		nextAddress = address+OsclPageSize;
		// At this point we have found a properly aligned starting
		// page, now all we need to do is ensure that the remaining
		// pages required are sequential pages.
		for(unsigned nPagesAllocated=1;nPagesAllocated<sizeInPages;++nPagesAllocated,nextAddress+=OsclPageSize){
			KruxPage*	page	= freePages.get();
			if(!page){
				// Can't allocate block
				while((page=allocedPages.get())){
					freePages.push(page);
					}
				while((page=triedPages.pop())){
					freePages.push(page);
					}
				Oscl::ErrorFatal::logAndExit("allocAlignedPow2MMU fail 2\n\r");
				return 0;
				}
			allocedPages.put(page);
			address	= (unsigned long)page;
			if(address != nextAddress){
				while((page=allocedPages.get())){
					triedPages.push(page);
					}
				break;
				}
			}
		KruxPage*	page	= allocedPages.first();
		if(page){
			while((page=triedPages.pop())){
				freePages.push(page);
				}
			_nAllocedMmuPages	-= sizeInPages;
			return allocedPages.first();
			}
		}
	Oscl::ErrorFatal::logAndExit("allocAlignedPow2MMU fail 3\n\r");
	return 0;
	}

KruxPage*	SystemPage::allocHeap() noexcept{
	KruxPage*	page	= allocFreePage();
	if(page){
		++_nAllocedHeapPages;
		}
	return freePages.pop();
	}

KruxPage*	SystemPage::allocMMU() noexcept{
	KruxPage*	page=allocUsedPage();
	if(page){
		++_nAllocedMmuPages;
		return page;
		}
	page	= freePages.getLast();
	if(page){
		++_nAllocedMmuPages;
		}
	return page;
	}

CoarseLevel2TableMem*	SystemPage::allocCoarseLevel2MMU() noexcept{
	CoarseLevel2TableMem*	mem = coarseLevel2Mem.get();
	if(mem){
		return mem;
		}
	KruxPage*	page=allocMMU();
	if(!page){
		return 0;
		}

	mem	= (CoarseLevel2TableMem*)page;

	// There are 4 x 1KB CoarseLevel2TableMem per page.
	// Here, we put three of them in the free list.
	for(unsigned i=0;i<3;++i,++mem){
		coarseLevel2Mem.put(mem);
		}

	return mem;
	}

void		SystemPage::freeMMU(KruxPage* page) noexcept{
	freeUsedPage(page);
	--_nAllocedMmuPages;
	}

unsigned	SystemPage::nFreePages() const noexcept{
	return _nFreePages;
	}

unsigned	SystemPage::nUsedPages() const noexcept{
	return _nUsedPages;
	}

unsigned	SystemPage::nAllocedMmuPages() const noexcept{
	return _nAllocedMmuPages;
	}

unsigned	SystemPage::nAllocedHeapPages() const noexcept{
	return _nAllocedHeapPages;
	}

KruxPage*	SystemPage::allocFreePage() noexcept{
	KruxPage*	page	= freePages.pop();
	if(page){
		--_nFreePages;
		}
	return freePages.pop();
	}

void		SystemPage::freeUsedPage(KruxPage* page) noexcept{
	freeUsedPages.put(page);
	++_nUsedPages;
	}

KruxPage*	SystemPage::allocUsedPage() noexcept{
	KruxPage*	page=freeUsedPages.get();
	if(page){
		--_nUsedPages;
		}
	return page;
	}

