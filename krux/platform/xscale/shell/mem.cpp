/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <stdio.h>
#include "mem.h"
#include "oscl/shell/line/cmdapi.h"
#include "oscl/krux/platform/xscale/syspage.h"

using namespace Oscl::Krux::Platform::Ppc::Shell;

static const char	memHelp[]   = {" - Display memory size and usage.\n\r"};
static const char	cmdName[]   = {"mem"};

MemoryCmd::MemoryCmd() noexcept
		{
	}

const char*	MemoryCmd::command() const noexcept{
	return cmdName;
	}

void	MemoryCmd::help(Oscl::Stream::Output::Api&	output) const noexcept{
	output.write(cmdName,sizeof(cmdName)-1);
	output.write(memHelp,sizeof(memHelp)-1);
	}

Oscl::Shell::Line::InputApi*
	MemoryCmd::execute(	Oscl::Shell::Line::StateHeader&	state,
						const char*						arguments
						) const noexcept{
	char	buffer[64];
	extern unsigned char	SystemRamSize;	// Defined by linker script
	sprintf(	buffer,
				"Total DRAM: %lu bytes\n\r",
				((unsigned long)&SystemRamSize)
				);
	state._output.write(buffer,strlen(buffer));
	extern const char		__FREE_PAGE_START__;
	sprintf(	buffer,
				"Heap Start Address: 0x%8.8lX\n\r",
				((unsigned long)&__FREE_PAGE_START__)
				);
	state._output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"FreeMemory: %u bytes\n\r",
				KruxGlobalSystemPage.nFreePages()*OsclPageSize
				);
	state._output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"AllocedMemory: %u bytes\n\r",
				KruxGlobalSystemPage.nAllocedMmuPages()*OsclPageSize
				);
	state._output.write(buffer,strlen(buffer));
	return 0;
	}

