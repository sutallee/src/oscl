/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/krux/port/frame.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/krux/thread/sched.h"
#include "oscl/krux/thread/thread.h"
#include "oscl/krux/port/platform.h"
#include "oscl/krux/port/trap.h"
#include "oscl/driver/mips/asm.h"
#include "oscl/driver/mips/cp0/asm.h"
#include "oscl/interrupt/handler.h"
#include "syspage.h"
#include "oscl/hw/ti/tnetv105x/gnu/map.h"
#include "oscl/hw/national/uart/pc16550.h"
#include "oscl/error/info.h"

extern "C" void	printConsoleCharacter(char	c){
	volatile Oscl::HW::TI::TNETV105X::UART::LSR::Reg& lsr			= TiTnet105x_UART.lsr;
	volatile Oscl::HW::TI::TNETV105X::UART::RBR_THR_DLL::Reg& thr	= TiTnet105x_UART.rbr_thr_dll;
	while((lsr & Oscl::HW::National::UART::PC16550::LSR::TEMT::FieldMask) == Oscl::HW::National::UART::PC16550::LSR::TEMT::ValueMask_NotEmpty);
	thr	= c;
	}

extern "C" void dumpTheFrame(OsclKruxFrame& frame) {
#if 1
	Oscl::Error::Info::log("R0,zero: ");
	Oscl::Error::Info::hexDump(frame.r[0]);
	Oscl::Error::Info::log("\n\rR1,at: ");
	Oscl::Error::Info::hexDump(frame.r[1]);
	Oscl::Error::Info::log("\n\rR2,v0: ");
	Oscl::Error::Info::hexDump(frame.r[2]);
	Oscl::Error::Info::log("\n\rR3,v1: ");
	Oscl::Error::Info::hexDump(frame.r[3]);
	Oscl::Error::Info::log("\n\rR4,a0: ");
	Oscl::Error::Info::hexDump(frame.r[4]);
	Oscl::Error::Info::log("\n\rR5,a1: ");
	Oscl::Error::Info::hexDump(frame.r[5]);
	Oscl::Error::Info::log("\n\rR6,a2: ");
	Oscl::Error::Info::hexDump(frame.r[6]);
	Oscl::Error::Info::log("\n\rR7,a3: ");
	Oscl::Error::Info::hexDump(frame.r[7]);
	Oscl::Error::Info::log("\n\rR8,t0: ");
	Oscl::Error::Info::hexDump(frame.r[8]);
	Oscl::Error::Info::log("\n\rR9,t1: ");
	Oscl::Error::Info::hexDump(frame.r[9]);
	Oscl::Error::Info::log("\n\rR10,t2: ");
	Oscl::Error::Info::hexDump(frame.r[10]);
	Oscl::Error::Info::log("\n\rR11,t3: ");
	Oscl::Error::Info::hexDump(frame.r[11]);
	Oscl::Error::Info::log("\n\rR12,t4: ");
	Oscl::Error::Info::hexDump(frame.r[12]);
	Oscl::Error::Info::log("\n\rR13,t5: ");
	Oscl::Error::Info::hexDump(frame.r[13]);
	Oscl::Error::Info::log("\n\rR14,t6: ");
	Oscl::Error::Info::hexDump(frame.r[14]);
	Oscl::Error::Info::log("\n\rR15,t7: ");
	Oscl::Error::Info::hexDump(frame.r[15]);
	Oscl::Error::Info::log("\n\rR16,s0: ");
	Oscl::Error::Info::hexDump(frame.r[16]);
	Oscl::Error::Info::log("\n\rR17,s1: ");
	Oscl::Error::Info::hexDump(frame.r[17]);
	Oscl::Error::Info::log("\n\rR18,s2: ");
	Oscl::Error::Info::hexDump(frame.r[18]);
	Oscl::Error::Info::log("\n\rR19,s3: ");
	Oscl::Error::Info::hexDump(frame.r[19]);
	Oscl::Error::Info::log("\n\rR20,s4: ");
	Oscl::Error::Info::hexDump(frame.r[20]);
	Oscl::Error::Info::log("\n\rR21,s5: ");
	Oscl::Error::Info::hexDump(frame.r[21]);
	Oscl::Error::Info::log("\n\rR22,s6: ");
	Oscl::Error::Info::hexDump(frame.r[22]);
	Oscl::Error::Info::log("\n\rR23,s7: ");
	Oscl::Error::Info::hexDump(frame.r[23]);
	Oscl::Error::Info::log("\n\rR24,t8: ");
	Oscl::Error::Info::hexDump(frame.r[24]);
	Oscl::Error::Info::log("\n\rR25,t9: ");
	Oscl::Error::Info::hexDump(frame.r[25]);
	Oscl::Error::Info::log("\n\rR26,k0: ");
	Oscl::Error::Info::hexDump(frame.r[26]);
	Oscl::Error::Info::log("\n\rR27,k1: ");
	Oscl::Error::Info::hexDump(frame.r[27]);
	Oscl::Error::Info::log("\n\rR28,gp: ");
	Oscl::Error::Info::hexDump(frame.r[28]);
	Oscl::Error::Info::log("\n\rR29,sp: ");
	Oscl::Error::Info::hexDump(frame.r[29]);
	Oscl::Error::Info::log("\n\rR30,fp: ");
	Oscl::Error::Info::hexDump(frame.r[30]);
	Oscl::Error::Info::log("\n\rR31,ra: ");
	Oscl::Error::Info::hexDump(frame.r[31]);

	Oscl::Error::Info::log("\n\rEPC: ");
	Oscl::Error::Info::hexDump(frame.epc);

	Oscl::Error::Info::log("\n\rCAUSE: ");
	Oscl::Error::Info::hexDump(frame.cause);

	Oscl::Error::Info::log("\n\rSTATUS: ");
	Oscl::Error::Info::hexDump(frame.status);

	Oscl::Error::Info::log("\n\rBadVAddr: ");
	Oscl::Error::Info::hexDump(Oscl::MIPS::CP0::badVAddr());

	Oscl::Error::Info::log("\n\rHI: ");
	Oscl::Error::Info::hexDump((uint32_t)frame.hi);

	Oscl::Error::Info::log("\n\rLO: ");
	Oscl::Error::Info::hexDump((uint32_t)frame.lo);

	Oscl::Error::Info::log("\n\rErrorPC: ");
	Oscl::Error::Info::hexDump(Oscl::MIPS::CP0::errorEPC());

	Oscl::Error::Info::log("\n\r");
#endif
	}

class TestHandler : public Oscl::Interrupt::HandlerApi {
	public:
		void interrupt() noexcept{}
	};

TestHandler	testHandler;

extern "C"{
	typedef struct OOTrapHandlerArgs{
		Oscl::Krux::Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	}

extern "C" {
	extern "C" int _start(void);
	extern "C" int _init(void);
	OsclKruxFrame*	mipsReset(void);
	OsclKruxFrame*	mipsSystemCall(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsInterrupt(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsBadException(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsLoadFetchAddressError(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsStoreAddressError(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsTlbLoadFetch(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsTlbRefillExl0(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsCacheError(OsclKruxFrame& frame);
	OsclKruxFrame*	mipsAlternateInterrupt(OsclKruxFrame& frame);
	}

static void initializeGlobalPage() noexcept{
//	printConsoleCharacter('1');
	new (&KruxGlobalSystemPage) SystemPage();
	}

static void initializeBSS() noexcept{
	// Initialize the BSS
	extern char		__SBSS_START__;
	extern char		__SBSS_END__;
	char*	bssstart = &__SBSS_START__;
	for(;bssstart < &__SBSS_END__;++bssstart){
		*bssstart	= 0;
		}
	}

static void setupExternalBus() noexcept{
#if 0
	// FIXME: This routine demonstrates the
	// configuration needed to access the QSPAN
	// PCI bridge. It should be moved to a
	// platform specific location.
	// Chip selects must also be configured properly.
	// In particular the QSPAN configuration space
	// chip select must be set to enable BIH and
	// use external TA.
	// Known good value: 0x00610900
	using namespace Oscl::Mot8xx::Siu::SIUMCR;
	Mpc8xxQUICC.siu.siumcr	=
			EARB::ValueMask_InternalArbitration
		|	EARP::ValueMask_HighestPriority
		|	DSHW::ValueMask_DisableInternalDataShowCycles
		|	DBGC::ValueMask_VFLS
		|	DBPC::ValueMask_Mode00
		|	FRC::ValueMask_FRZ
		|	DLK::ValueMask_Locked
		|	OPAR::ValueMask_EvenParity
		|	PNCS::ValueMask_DisableParity
		|	DPC::ValueMask_IRQ
		|	MPRE::ValueMask_IRQ
		|	MLRC::ValueMask_KrRetry	// Required for QSPAN
		|	AEME::ValueMask_ExternalMasterDisabled
		|	SEME::ValueMask_ExternalMasterEnabled	// Required for QSPAN
		|	BSC::ValueMask_Dedicated
		|	GB5E::ValueMask_BDIP
		|	B2DD::ValueMask_CS2Only
		|	B3DD::ValueMask_CS3Only
		;
#endif
	}

static void setupSystemTimer() noexcept{
#if 0
	using namespace Oscl::Mot8xx;
	// The next three lines can be used to enable and
	// initialize the PPC decrementer. Currently,
	// I am using the PIT for its rate monotonic characteristics,
	// and thus have the decrementer and timebase disabled for now.
	// It seems to me that the decrementer would be useful for
	// time sharing multi-tasking, where rate monotonicity is
	// not required. I've disabled it to prevent the currently
	// unused interrupts it causes, since this is the only means
	// I can find of disabling the decrementer interrupts.
	Mpc8xxQUICC.sitkey.tbk		= SitKey::TBK::Key;
	Mpc8xxQUICC.sitimer.tbscr	= SiTimer::TBSCR::TBE::ValueMask_Enable;
	Oscl::Ppc::mtdec(1000);
#endif
	}

static void setupSystemTiming() noexcept{
#if 0
	using namespace Oscl::Mot8xx;
	Mpc8xxQUICC.clkrstkey.sccrk	= ClkRstKey::SCCRK::Key;
	Mpc8xxQUICC.clkrst.sccr		=
			ClkRst::SCCR::COM::ValueMask_FullStrength
		|	ClkRst::SCCR::TBS::ValueMask_OscClkDiv
		|	ClkRst::SCCR::RTDIV::ValueMask_DivideBy4
		|	ClkRst::SCCR::RTSEL::ValueMask_OSCM
		|	ClkRst::SCCR::CRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::PRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::EBDF::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFSYNC::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFBRG::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFNL::ValueMask_DivideBy2
		|	ClkRst::SCCR::DFNH::ValueMask_DivideBy1
		;
#endif
	}

extern "C" void __printWord(unsigned long v);

void __printWord(unsigned long v){
	Oscl::Error::Info::hexDump(v);
	}

/* Called from the ARM reset exception.
 */
OsclKruxFrame*	mipsReset(void){
	// SRVABI:
	// The stack must be aligned to a 16 byte boudary.
	// The stack must point to a word location containing
	// a null frame pointer.
	// Note:
	//	The OsclKruxFrame structure type is NOT a part of the
	//	SRVABI.

	//	Writeback kseg0 cache to backing store.
	unsigned long	cacheLineSize	= Oscl::MIPS::CP0::config1();
	cacheLineSize	&= ~Oscl::HW::MIPS::CP0::Config1::DL::FieldMask;
	cacheLineSize	>>= Oscl::HW::MIPS::CP0::Config1::DL::Lsb;
	for(unsigned long address=0x80000000;address<0xA0000000;address+=cacheLineSize){
		Oscl::MIPS::cachePrimaryDataHitWriteback(address);
		Oscl::MIPS::sync();
		Oscl::MIPS::cachePrimaryDataInstructionHitInvalidate(address);
		Oscl::MIPS::sync();
		}

//	Oscl::Error::Info::log("mipsReset\n\r");
	extern void dumb();
#if 1
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
	dumb();
#endif

	// Setup the system timing
	setupSystemTiming();

	// Initialize the BSS
	initializeBSS();

	// Initialize the Global Page
	initializeGlobalPage();

	// Setup external bus characteristics.
	setupExternalBus();

	// The next three lines enable and initialize
	// the PPC decrementer. Since I've moved to using
	// the PIT for its rate monotonic characteristics,
	// I have the decrementer and timebase disabled for now.
	setupSystemTimer();

	// Call pre-constructor initialization to setup
	// chip selects and that sort of stuff.
	OsclKruxPreConstructorInit();

	// Call main()
	_init();
	extern int main(int argc,char *argv[]);
	main(0,0);	// this does not return

	// Should never get here.
	while(true);
	return 0;
	}

OsclKruxFrame*	mipsInterrupt(OsclKruxFrame& frame){
	extern Oscl::Interrupt::HandlerApi*	ExternalInterruptHandler;
#if 0
	static unsigned	count	= 0;
	++count;
	if(count >= 0x100){
		Oscl::Error::Info::log("mipsInterrupt\n\r");
		count	= 0;
	}
#endif
//	Oscl::Error::Info::log("mipsInterrupt\n\r");
//	dumpTheFrame(frame);
	// Enable MMU to get correct cache attributes for
	// memory mapped I/O. Without doing this, things were...
	// unpredicable on the machine check front. Interrupts
	// would work a few times (PIT) and then I would miss
	// an interrupt status. I suspect I was using a cached
	// copy of the PICSR, since cache is enabled even when
	// the MMU is disabled. Changing the cache to write-through
	// helped (a little) but enable the MMU fixed things.

	// Finally, handle the interrupt.
	ExternalInterruptHandler->interrupt();

	return &frame;
	}

OsclKruxFrame*	mipsSystemCall(OsclKruxFrame& frame){
//	dumpTheFrame(frame);
//	Oscl::Error::Info::log("mipsSystemCall\n\r");
	OOTrapHandler((void*)&frame.r[4]);
	frame.epc	+= 4;
	return &frame;
	}

OsclKruxFrame*	mipsBadException(OsclKruxFrame& frame){
	Oscl::Error::Info::log("mipsBadException\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsLoadFetchAddressError(OsclKruxFrame& frame){
	dumpTheFrame(frame);
	Oscl::Error::Info::log("mipsLoadFetchAddressError\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsStoreAddressError(OsclKruxFrame& frame){
	dumpTheFrame(frame);
	Oscl::Error::Info::log("mipsStoreAddressError\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsTlbLoadFetch(OsclKruxFrame& frame){
	dumpTheFrame(frame);
	Oscl::Error::Info::log("mipsTlbLoadFetch\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsTlbRefillExl0(OsclKruxFrame& frame){
	dumpTheFrame(frame);
	Oscl::Error::Info::log("mipsTlbRefillExl0\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsCacheError(OsclKruxFrame& frame){
	Oscl::Error::Info::log("mipsCacheError\n\r");
	for(;;);
	}

OsclKruxFrame*	mipsAlternateInterrupt(OsclKruxFrame& frame){
	Oscl::Error::Info::log("mipsAlternateInterrupt\n\r");
	for(;;);
	}

#if 0
extern "C" void printLong(unsigned long);

void printLong(unsigned long value){
	Oscl::Error::Info::log("\n\rprintLong: ");
	Oscl::Error::Info::hexDump(value);
	Oscl::Error::Info::log("\n\r");
	uint32_t	cause	= Oscl::MIPS::CP0::cause();
	Oscl::Error::Info::log("\n\rEx CAUSE: ");
	Oscl::Error::Info::hexDump(cause);
	Oscl::Error::Info::log("\n\r");
	uint32_t
	reg	= *(uint32_t*)0xA8612420;
	Oscl::Error::Info::log("\n\rSYS_INT.enbl_set_1: ");
	Oscl::Error::Info::hexDump(reg);
	Oscl::Error::Info::log("\n\r");
	reg	= *(uint32_t*)0xA8612424;
	Oscl::Error::Info::log("\n\rSYS_INT.enbl_set_2: ");
	Oscl::Error::Info::hexDump(reg);
	Oscl::Error::Info::log("\n\r");
	for(;;);
	}
#endif
