#
# This file contains the entry point for this application. It *must*
# be located as the first object in the application. This application
# is intended to be invoked by a boot loader with a valid stack pointer.
#
#include "asmreg.h"
#include "frame.h"

        .macro  __restoreContextAndReturn
        lw  $24, FRAME_LO(sp)
        lw  $8, FRAME_R8(sp)
        lw  $9, FRAME_R9(sp)
        mtlo    $24
        lw  $24, FRAME_HI(sp)
        lw  $10, FRAME_R10(sp)
        lw  $11, FRAME_R11(sp)
        mthi    $24
        lw  $12, FRAME_R12(sp)
        lw  $13, FRAME_R13(sp)
        lw  $14, FRAME_R14(sp)
        lw  $15, FRAME_R15(sp)
        lw  $24, FRAME_R24(sp)
//        RESTORE_STATIC
        lw  $16, FRAME_R16(sp)
        lw  $17, FRAME_R17(sp)
        lw  $18, FRAME_R18(sp)
        lw  $19, FRAME_R19(sp)
        lw  $20, FRAME_R20(sp)
        lw  $21, FRAME_R21(sp)
        lw  $22, FRAME_R22(sp)
        lw  $23, FRAME_R23(sp)
        lw  $30, FRAME_R30(sp)
 //       RESTORE_AT
        .set    push
        .set    noat
        lw  $1,  FRAME_R1(sp)
        .set    pop
  //      RESTORE_SOME
        .set    push
        .set    reorder
        .set    noat 
#//	a0 = (cp0status | 0x1F)
#//  a0	^= 0x1F	; clear the KSU, ERL, EXL, and IE bits
#//	cp0status = a0	; Disable interrupts and enter kernel mode
#//	a0	&= 0xFF00
#//	v0  = FRAME_STATUS
#//	v1	= ~(0xFF00 | 0) = 0xFFFF00FF
#//	v0	&= 0xFFFF00FF	# clear IM[7:0] field
#//  v0	|= a0 ; Merge current and saved status
        mfc0    a0, cp0status
        ori a0, 0x1f
        xori    a0, 0x1f
        mtc0    a0, cp0status
		ssnop
		ssnop
		ssnop
		ssnop
        li  v1, 0xff00
        and a0, v1
        lw  v0, FRAME_STATUS(sp)
		# v0 == 0xF000FF03
#	nop
#	lui		k0,%hi(__printWord)
#	nop
#	addiu   k1,k0,%lo(__printWord)
#	move	a0,v0
#	jr      k1
#	nop
#	b	.
	nop
#        nor v1, $0, v1
		# v1 == 0xFFFF00FF
#        and v0, v1
		# v0 == 0xF0000003
        or  v0, a0
		# v0 == 0xF0000003
        mtc0    v0, cp0status
		ssnop
		ssnop
		ssnop
		ssnop
        lw  $31, FRAME_R31(sp)
        lw  gp, FRAME_R28(sp)
        lw  $25, FRAME_R25(sp)
        lw  $7,  FRAME_R7(sp)
        lw  $6,  FRAME_R6(sp)
        lw  $5,  FRAME_R5(sp)
        lw  $4,  FRAME_R4(sp)
        lw  $3,  FRAME_R3(sp)
        lw  $2,  FRAME_R2(sp)
        .set    pop
//        RESTORE_SP_AND_RET
        .set    push
        .set    noreorder
        lw  k0, FRAME_EPC(sp)
		nop
        mtc0    k0, cp0epc
		ssnop
		ssnop
        lw  sp, FRAME_R29(sp)
//        jr  k0
		eret
//         rfe
        .set    pop
        .endm

