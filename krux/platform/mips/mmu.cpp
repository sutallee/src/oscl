/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/queue/queue.h"
#include "mmu.h"
#include "syspage.h"
#include "oscl/cache/operations.h"
#include "oscl/mt/mutex/iss.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
//#include "oscl/driver/mips/cp1/asm.h"
//#include "oscl/driver/arm/mmu/l1table.h"
//#include "oscl/hw/arm/cp15/ttb.h"

#if 0
using namespace Oscl::MIPS::Mmu;

static Level2SmallDescriptor*
mapMemoryPage(	Oscl::MIPS::Mmu::Level1Table&	l1table,
				void*							virtAddr,
				void*							physAddr
				) noexcept{
	Level1Descriptor*	l1desc	= l1table.getDesc(virtAddr);
	if(!l1desc){
		Oscl::ErrorFatal::logAndExit("mapMemoryPage: l1table.getDesc() failed\n\r");
		for(;;);
		}
	if(l1desc->getDescType() == Oscl::MIPS::Mmu::Level1Descriptor::Fault){
		CoarseLevel2TableMem*	mem	= KruxGlobalSystemPage.allocCoarseLevel2MMU();
		if(!mem){
			Oscl::ErrorFatal::logAndExit("mapMemoryPage: MMU code can't allocate Coarse Level 2 Table.\n\r");
			for(;;);
			}
		CoarseLevel2Table*	l2table	= new (mem) CoarseLevel2Table();
		l1desc->setDescType(Oscl::MIPS::Mmu::Level1Descriptor::Coarse);
		Level1CoarseDescriptor*	l1CoarseDesc	= l1desc->getCoarseDescriptor();
		if(!l1CoarseDesc){
			Oscl::ErrorFatal::logAndExit("mapMemoryPage: l1desc->getCoarseDescriptor() failed.\n\r");
			for(;;);
			}
		l1CoarseDesc->setLevel2Table(*l2table);
		l1CoarseDesc->setDomain(0);
		l1CoarseDesc->setIMP(0);
		}
	Level1CoarseDescriptor*	l1CoarseDesc	= l1desc->getCoarseDescriptor();
	CoarseLevel2Table*		l2table			= l1CoarseDesc->getLevel2Table();
	Level2Descriptor*		l2desc	= l2table->getDesc(virtAddr);
	if(!l2desc){
		Oscl::ErrorFatal::logAndExit("mapMemoryPage: MMU code find allocate Coarse Level 2 descriptor.\n\r");
		for(;;);
		}
	if(l2desc->getDescType() != Oscl::MIPS::Mmu::Level2Descriptor::Fault){
		Oscl::ErrorFatal::logAndExit("mapMemoryPage: level 2 descriptor already in use!\n\r");
		for(;;);
		}
	l2desc->setDescType(Oscl::MIPS::Mmu::Level2Descriptor::Small);
	Level2SmallDescriptor*	smallDesc	= l2desc->getSmallDescriptor();
	if(!smallDesc){
		Oscl::ErrorFatal::logAndExit("mapMemoryPage: l2desc->getSmallDescriptor() failed.\n\r");
		for(;;);
		}
	smallDesc->setPageBase(physAddr);
	smallDesc->setBufferable();
	smallDesc->setCachable();
	return smallDesc;
	}

static Level2SmallDescriptor*
mapIoPage(	Oscl::MIPS::Mmu::Level1Table&	l1table,
			void*							virtAddr,
			void*							physAddr
			) noexcept{
	Level1Descriptor*	l1desc	= l1table.getDesc(virtAddr);
	if(!l1desc){
		Oscl::ErrorFatal::logAndExit("mapIoPage: l1table.getDesc() failed\n\r");
		for(;;);
		}
	if(l1desc->getDescType() == Oscl::MIPS::Mmu::Level1Descriptor::Fault){
		CoarseLevel2TableMem*	mem	= KruxGlobalSystemPage.allocCoarseLevel2MMU();
		if(!mem){
			Oscl::ErrorFatal::logAndExit("mapIoPage: MMU code can't allocate Coarse Level 2 Table.\n\r");
			for(;;);
			}
		CoarseLevel2Table*	l2table	= new (mem) CoarseLevel2Table();
		l1desc->setDescType(Oscl::MIPS::Mmu::Level1Descriptor::Coarse);
		Level1CoarseDescriptor*	l1CoarseDesc	= l1desc->getCoarseDescriptor();
		if(!l1CoarseDesc){
			Oscl::ErrorFatal::logAndExit("mapIoPage: l1desc->getCoarseDescriptor() failed.\n\r");
			for(;;);
			}
		l1CoarseDesc->setLevel2Table(*l2table);
		l1CoarseDesc->setDomain(0);
		l1CoarseDesc->setIMP(0);
		}
	Level1CoarseDescriptor*	l1CoarseDesc	= l1desc->getCoarseDescriptor();
	CoarseLevel2Table*		l2table			= l1CoarseDesc->getLevel2Table();
	Level2Descriptor*		l2desc	= l2table->getDesc(virtAddr);
	if(!l2desc){
		Oscl::ErrorFatal::logAndExit("mapIoPage: MMU code find allocate Coarse Level 2 descriptor.\n\r");
		for(;;);
		}
	if(l2desc->getDescType() != Oscl::MIPS::Mmu::Level2Descriptor::Fault){
		Oscl::ErrorFatal::logAndExit("mapIoPage: level 2 descriptor already in use!\n\r");
		for(;;);
		}
	l2desc->setDescType(Oscl::MIPS::Mmu::Level2Descriptor::Small);
	Level2SmallDescriptor*	smallDesc	= l2desc->getSmallDescriptor();
	if(!smallDesc){
		Oscl::ErrorFatal::logAndExit("mapIoPage: l2desc->getSmallDescriptor() failed.\n\r");
		for(;;);
		}
	smallDesc->setPageBase(physAddr);
	return smallDesc;
	}

static void	mapKernelTextPage(	Oscl::MIPS::Mmu::Level1Table&	l1table,
								void*							virtAddr,
								void*							physAddr
								) noexcept{
	Oscl::MIPS::Mmu::Level2SmallDescriptor*	l2desc	= mapMemoryPage(l1table,virtAddr,physAddr);
	if(!l2desc){
		Oscl::ErrorFatal::logAndExit("mapKernelTextPage: can't map page.\n\r");
		for(;;);
		}
	l2desc->setAP0(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserReadOnly);
	l2desc->setAP1(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserReadOnly);
	l2desc->setAP2(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserReadOnly);
	l2desc->setAP3(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserReadOnly);
	}

static void	mapKernelDataPage(	Level1Table&	l1table,
								void*			virtAddr,
								void*			physAddr
								) noexcept{
	Oscl::MIPS::Mmu::Level2SmallDescriptor*	l2desc	= mapMemoryPage(l1table,virtAddr,physAddr);
	if(!l2desc){
		Oscl::ErrorFatal::logAndExit("mapKernelDataPage: can't map page.\n\r");
		for(;;);
		}
	l2desc->setAP0(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
	l2desc->setAP1(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
	l2desc->setAP2(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
	l2desc->setAP3(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
	}
#endif

#if 0
void initializeLevelOneTable(Level1Desc::Reg* table){
	for(unsigned i=0;i<1024;++i){
		table[i]	=	Level1Desc::V::Value_Invalid;
		}
	}
#endif

#if 0
void initializeLevelTwoTable(Level2Desc::Reg* table){
	for(unsigned i=0;i<1024;++i){
		table[i]	=	Level2Desc::V::ValueMask_Invalid;
		}
	}
#endif

#if 0
void create4kSupervisorTextPage(	Level1Desc::Reg*	pageTable,
									unsigned long		epn,
									unsigned long		rpn
									){
	unsigned long	level1Index;
	level1Index	=	(epn & Twam1::Epn::Level1Index::FieldMask)
					>>Twam1::Epn::Level1Index::Lsb;
	unsigned long	level2Index;
	level2Index	=	(epn & Twam1::Epn::Level2Index::FieldMask)
					>>Twam1::Epn::Level2Index::Lsb;
	
	const Level1Desc::Reg	settingsMask =
		 		Level1Desc::APG::FieldMask
			|	Level1Desc::G::FieldMask
			|	Level1Desc::PS::FieldMask
			|	Level1Desc::WT::FieldMask
			|	Level1Desc::V::FieldMask
			;
	const Level1Desc::Reg	desiredSettings =
		 		(0x0<<Level1Desc::APG::Lsb)
			|	Level1Desc::G::ValueMask_NonGuarded
			|	Level1Desc::PS::ValueMask_Small
			|	Level1Desc::WT::ValueMask_Writethrough
			|	Level1Desc::V::ValueMask_Valid
			;

	Level2Desc::Reg*	level2Table	= 0;
	if(		(pageTable[level1Index] & Level1Desc::V::FieldMask )
		==	Level1Desc::V::ValueMask_Valid
		){
		// Existing level 1 entry already exists
		if((pageTable[level1Index] & settingsMask) != desiredSettings) {
			// Existing level 1 attributes are incompatible
			// with text page.
			while(true);
			}
		level2Table	=	(Level2Desc::Reg*)
						(		pageTable[level1Index]
							&	Level1Desc::L2BA::FieldMask
							);
		}
	if(!level2Table){
		// Need to alocate a level 2 table entry
		level2Table	= (Level2Desc::Reg*) KruxGlobalSystemPage.allocMMU();
		if(!level2Table){
			// Can't allocate a page for the level 2 table.
			while(true);
			}
		initializeLevelTwoTable(level2Table);
		}
	if(		(level2Table[level2Index] & Level2Desc::V::FieldMask)
		==	Level2Desc::V::ValueMask_Valid
		){
		// Already allocated!
		while(true);
		}
	}
#endif

void initializePageTable(){
#if 0
	// Allocate Level1 Table
	void*		p;
	
	// Allocate a 16KB block of physical RAM aligned
	// to a 16KB boundary.
	p	= KruxGlobalSystemPage.allocAlignedPow2MMU(14);
	if(!p){
		Oscl::ErrorFatal::logAndExit("Cannot allocate MMU level 1 descriptor table\n\r");
		for(;;);
		}
	Oscl::MIPS::Mmu::Level1Table*	l1Table	= new (p) Oscl::MIPS::Mmu::Level1Table();

	Oscl::Arm::CP15::writeTranslationBase((unsigned long)l1Table);
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:initializePageTable not implemented\n\r");
	for(;;);
#endif
	}

void	InitializeMmu(){
	initializePageTable();
	}

void	EnableMmu(){
#if 0
	MdCtr::Reg	mdctr	=		MdCtr::GPM::ValueMask_PowerPcMode
							|	MdCtr::PPM::ValueMask_ProtectPage
							|	MdCtr::CIDEF::ValueMask_Allow
							|	MdCtr::WTDEF::ValueMask_Copyback
							|	MdCtr::RSV4D::ValueMask_Modulo32
							|	MdCtr::TWAM::ValueMask_SubPage4K
							|	MdCtr::PPCS::ValueMask_Ignore
							|	(0<<MdCtr::DTLB_INDX::Lsb)
							;
	writeMD_CTR(mdctr);
	MiCtr::Reg	mictr	=		MiCtr::GPM::ValueMask_PowerPcMode
							|	MiCtr::PPM::ValueMask_ProtectPage
							|	MiCtr::CIDEF::ValueMask_Allow
							|	MiCtr::RSV4I::ValueMask_Modulo32
							|	MiCtr::PPCS::ValueMask_Ignore
							|	(0<<MiCtr::ITLB_INDX::Lsb)
							;
	writeMI_CTR(mictr);
	MxAp::Reg	ap	=
			MxAp::Gpm0::GP15::ValueMask_PageProtection
		|	MxAp::Gpm0::GP14::ValueMask_PageProtection
		|	MxAp::Gpm0::GP13::ValueMask_PageProtection
		|	MxAp::Gpm0::GP12::ValueMask_PageProtection
		|	MxAp::Gpm0::GP11::ValueMask_PageProtection
		|	MxAp::Gpm0::GP10::ValueMask_PageProtection
		|	MxAp::Gpm0::GP9::ValueMask_PageProtection
		|	MxAp::Gpm0::GP8::ValueMask_PageProtection
		|	MxAp::Gpm0::GP7::ValueMask_PageProtection
		|	MxAp::Gpm0::GP6::ValueMask_PageProtection
		|	MxAp::Gpm0::GP5::ValueMask_PageProtection
		|	MxAp::Gpm0::GP4::ValueMask_PageProtection
		|	MxAp::Gpm0::GP3::ValueMask_PageProtection
		|	MxAp::Gpm0::GP2::ValueMask_PageProtection
		|	MxAp::Gpm0::GP1::ValueMask_PageProtection
		|	MxAp::Gpm0::GP0::ValueMask_PageProtection
		;
	writeMI_AP(ap);
	writeMD_AP(ap);
	Oscl::Ppc::tlbia();
	Oscl::Ppc::sync();
	Oscl::Ppc::Oea::Msr::Reg	msr	= Oscl::Ppc::mfmsr();
	msr	|=		Oscl::Ppc::Oea::Msr::Ir::ValueMask_Enabled
			|	Oscl::Ppc::Oea::Msr::Dr::ValueMask_Enabled
			;
	Oscl::Ppc::mtmsr(msr);
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	Oscl::Ppc::isync();
	// Enable caches here
	Oscl::Mot8xx::Cache::InstructionCacheControl	iccst;
	Oscl::Mot8xx::Cache::DataCacheControl			dccst;
	dccst.unlockAll();
	iccst.unlockAll();
	Oscl::Ppc::eieio();
	dccst.invalidateAll();
	Oscl::Ppc::eieio();
	dccst.enableCache();
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	iccst.invalidateAll();
	Oscl::Ppc::eieio();
	iccst.enableCache();
	Oscl::Ppc::eieio();
	Oscl::Ppc::sync();
	Oscl::Ppc::isync();
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:EnableMMU not implemented\n\r");
	for(;;);
#endif
	}

void	MapIoRange(void* address,unsigned long size) noexcept{
#if 0
	unsigned long	value = Oscl::Arm::CP15::readTranslationBase();
	value	&= Oscl::HW::MIPS::CP15::TransTableBase::PhysicalAddress::FieldMask;
	Oscl::MIPS::Mmu::Level1Table*	l1Table	= (Oscl::MIPS::Mmu::Level1Table*)value;
	char*	mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		Level2SmallDescriptor*
		desc	= mapIoPage(*l1Table,mpage,mpage);
		desc->setAP0(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
		desc->setAP1(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
		desc->setAP2(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
		desc->setAP3(Oscl::MIPS::Mmu::AP::privilegedReadWriteUserNoAccess);
		}
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:MapIoRange not implemented\n\r");
	for(;;);
#endif
	}

void	MapWriteableRamRange(void* address,unsigned long size) noexcept{
#if 0
	unsigned long	value = Oscl::Arm::CP15::readTranslationBase();
	value	&= Oscl::HW::MIPS::CP15::TransTableBase::PhysicalAddress::FieldMask;
	Oscl::MIPS::Mmu::Level1Table*	l1Table	= (Oscl::MIPS::Mmu::Level1Table*)value;
	char*	mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		mapKernelDataPage(*l1Table,mpage,mpage);
		}
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:MapWriteableRamRange not implemented\n\r");
	for(;;);
#endif
	}

void	MapReadOnlyRamRange(void* address,unsigned long size) noexcept{
#if 0
	unsigned long	value = Oscl::Arm::CP15::readTranslationBase();
	value	&= Oscl::HW::MIPS::CP15::TransTableBase::PhysicalAddress::FieldMask;
	Oscl::MIPS::Mmu::Level1Table*	l1Table	= (Oscl::MIPS::Mmu::Level1Table*)value;
	char*	mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		mapKernelTextPage(*l1Table,mpage,mpage);
		}
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:MapReadOnlyRamRange not implemented\n\r");
	for(;;);
#endif
	}

/** Returns zero if no translation. Note that this assumes page zero
	is not usable in VM which is true for this implementation.
 */
void*	MmuVirtualToPhysical(void* virtualAddress) noexcept{
#if 0
	using namespace Oscl::Mot8xx::Mmu;
	Mtwb::Reg	twb	=	readM_TWB();
	twb				&=	Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	Level1Descriptor*	l1Desc	= l1Table->getDesc(virtualAddress);
	if(!l1Desc->isValid()){
		return 0;
		}
	Level2Table*		l2Table	= l1Desc->getLevel2Table();
	Level2Descriptor*	l2Desc	= l2Table->getDescTwam1(virtualAddress);
	if(l2Desc->isValid()){
		unsigned long	pageOffset	= (unsigned long)virtualAddress;
		pageOffset	&= l2Desc->getPageSizeMask();
		return (void*)(l2Desc->getRealPage()+pageOffset);
		}
	return 0;
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:MmuVirtualToPhysical not implemented\n\r");
	for(;;);
#endif
	}

#if 0
static void	cacheInhibitPage(	Level1Twam1Table&	l1Table,
								void*				virtAddr
								) noexcept{
	// FIXME: In SMP systems, the global
	Level1Descriptor*	l1Desc	= l1Table.getDesc(virtAddr);
	if(!l1Desc->isValid()){
		while(true);
		}
	Level2Table*		l2Table	= l1Desc->getLevel2Table();
	Level2Descriptor*	l2Desc	= l2Table->getDescTwam1(virtAddr);

		{
		Oscl::Mt::IntScopeSync	iss;

		if(!l2Desc->isValid()){
			while(true);
			}
		l2Desc->setPageInvalid();
		Oscl::Ppc::sync();
		l2Desc->setCacheInhibit();
		l2Desc->setPageValid();
		Oscl::Ppc::tlbie(virtAddr);
		Oscl::Ppc::eieio();
		Oscl::Ppc::tlbsync();
		Oscl::Ppc::sync();
		}
	}
#endif

void	MmuCacheInhibitRange(void* address,unsigned long size) noexcept{
#if 0
	using namespace Oscl::Mot8xx::Mmu;
	Mtwb::Reg	twb	=	readM_TWB();
	twb				&=	Mtwb::L1TB::FieldMask;
	Level1Twam1Table*	l1Table	= (Level1Twam1Table*)twb;
	char*				mpage	= (char*)address;
	for(;(mpage < ((char*)(address)+size));mpage+=OsclPageSize){
		cacheInhibitPage(*l1Table,mpage);
		OsclCacheDataInvalidateRange((void*)mpage,(unsigned long)OsclPageSize);
		}
#else
	Oscl::Error::Info::log("oscl/krux/platform/xscale/mmu.cpp:MmuCacheInhibitRange not implemented\n\r");
	for(;;);
#endif
	}

