#include "asmreg.h"
#include "frame.h"

        .macro  __saveContext
        .set    push
        .set    noat
        .set    reorder
        move    k0, sp
        subu sp, k0, FRAME_SIZE
        sw  k0, FRAME_R29(sp)
        sw  k0, FRAME_R26(sp)	// unnecessary
        sw  k1, FRAME_R27(sp)	// unnecessary
        sw  $3, FRAME_R3(sp)
        sw  $0, FRAME_R0(sp)
        mfc0    v1, cp0status, 0
        sw  $2, FRAME_R2(sp)
        sw  v1, FRAME_STATUS(sp)
        sw  $4, FRAME_R4(sp)
        mfc0    v1, cp0cause
        sw  $5, FRAME_R5(sp)
        sw  v1, FRAME_CAUSE(sp)
        sw  $6, FRAME_R6(sp)
        mfc0    v1, cp0epc
        sw  $7, FRAME_R7(sp)
        sw  v1, FRAME_EPC(sp)
        sw  $25, FRAME_R25(sp)
        sw  gp, FRAME_R28(sp)
        sw  $31, FRAME_R31(sp)
        .set    pop 
# SAVE_AT
        .set    push
        .set    noat
        sw  $1, FRAME_R1(sp)
        .set    pop
# SAVE_TEMP
        mfhi    v1
        sw  $8, FRAME_R8(sp)
        sw  $9, FRAME_R9(sp)
        sw  v1, FRAME_HI(sp)
        mflo    v1
        sw  $10, FRAME_R10(sp)
        sw  $11, FRAME_R11(sp)
        sw  v1,  FRAME_LO(sp)
        sw  $12, FRAME_R12(sp)
        sw  $13, FRAME_R13(sp)
        sw  $14, FRAME_R14(sp)
        sw  $15, FRAME_R15(sp)
        sw  $24, FRAME_R24(sp)
# SAVE_STATIC
        sw  $16, FRAME_R16(sp)
        sw  $17, FRAME_R17(sp)
        sw  $18, FRAME_R18(sp)
        sw  $19, FRAME_R19(sp)
        sw  $20, FRAME_R20(sp)
        sw  $21, FRAME_R21(sp)
        sw  $22, FRAME_R22(sp)
        sw  $23, FRAME_R23(sp)
        sw  $30, FRAME_R30(sp)
        .endm   

