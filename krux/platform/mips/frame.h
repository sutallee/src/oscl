#ifndef _oscl_krux_platform_mips_frameh_
#define _oscl_krux_platform_mips_frameh_

/* This must match oscl/krux/port/platform/frame.h : OsclKruxFrame */

#define FRAME_R0		24
#define FRAME_R1		28
#define FRAME_R2		32
#define FRAME_R3		36
#define FRAME_R4		40
#define FRAME_R5		44
#define FRAME_R6		48
#define FRAME_R7		52
#define FRAME_R8		56
#define FRAME_R9		60
#define FRAME_R10		64
#define FRAME_R11		68
#define FRAME_R12		72
#define FRAME_R13		76
#define FRAME_R14		80
#define FRAME_R15		84
#define FRAME_R16		88
#define FRAME_R17		92
#define FRAME_R18		96
#define FRAME_R19		100
#define FRAME_R20		104
#define FRAME_R21		108
#define FRAME_R22		112
#define FRAME_R23		116
#define FRAME_R24		120
#define FRAME_R25		124
#define FRAME_R26		128
#define FRAME_R27		132
#define FRAME_R28		136
#define FRAME_R29		140
#define FRAME_R30		144
#define FRAME_R31		148
#define FRAME_STATUS	152
#define FRAME_HI		156
#define FRAME_LO		160
#define FRAME_BADVADDR	164
#define FRAME_CAUSE		168
#define FRAME_EPC		172

#define FRAME_SIZE	176

#endif
