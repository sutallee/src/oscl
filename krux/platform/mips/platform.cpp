/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/krux/port/trap.h"
#include "oscl/krux/port/platform.h"
#include "oscl/kernel/mmu.h"
#include "oscl/cache/operations.h"
#include "oscl/hw/mips/cp0/reg.h"
#include <stdint.h>
#include "mmu.h"
#include "page.h"

using namespace Oscl::Krux;
using namespace Oscl;

extern "C"{
	extern volatile unsigned long   GlobalMpLockVariable;
	extern void	StartOOOS(OsclKruxFrame* frame) __attribute__((noreturn));
	extern void	ThreadEntry(void);
	extern void	UnknownException(void);
	typedef struct OOTrapHandlerArgs{
		Trap		*trap;
		}OOTrapHandlerArgs;

	void	OOTrapHandler(void *usp);
	uint16_t	InterruptNestLevel = ~0;
	}

volatile unsigned long   GlobalMpLockVariable;

/*****************************************************************************
 * OOTrapHandler()
 *	Called by Trap #0
 */

void	OOTrapHandler(void *usp) {
	((OOTrapHandlerArgs *)usp)->	trap->suService();
}

/*****************************************************************************
 * OsclKruxInit()
 *	Initialize platform layer.
 *	Called in supervisor mode.
 */

extern "C" void OsclKruxInit() noexcept {
	}

/*****************************************************************************
 * OsclKruxBuildStackFrame()
 *  Builds stack frame for thread.
 */

OsclKruxFrame* OsclKruxBuildStackFrame(	EntryPoint&		entry,
										void*			stack,
										unsigned long	stackSize
										) noexcept {
	OsclKruxFrame*
	frame	= (OsclKruxFrame*)(((uint32_t)((uint8_t *)stack+stackSize))-sizeof(OsclKruxFrame));

	frame->r[0]		= (uint32_t)0;
	frame->r[1]		= (uint32_t)1;
	frame->r[2]		= (uint32_t)2;
	frame->r[3]		= (uint32_t)3;
	frame->r[4]		= (uint32_t)&entry;
	frame->r[5]		= (uint32_t)5;
	frame->r[6]		= (uint32_t)6;
	frame->r[7]		= (uint32_t)7;
	frame->r[8]		= (uint32_t)8;
	frame->r[9]		= (uint32_t)9;
	frame->r[10]	= (uint32_t)10;
	frame->r[11]	= (uint32_t)11;
	frame->r[12]	= (uint32_t)12;
	frame->r[13]	= (uint32_t)13;
	frame->r[14]	= (uint32_t)14;
	frame->r[15]	= (uint32_t)15;
	frame->r[16]	= (uint32_t)16;
	frame->r[17]	= (uint32_t)17;
	frame->r[18]	= (uint32_t)18;
	frame->r[19]	= (uint32_t)19;
	frame->r[20]	= (uint32_t)20;
	frame->r[21]	= (uint32_t)21;
	frame->r[22]	= (uint32_t)22;
	frame->r[23]	= (uint32_t)23;
	frame->r[24]	= (uint32_t)24;
	frame->r[25]	= (uint32_t)25;
	frame->r[26]	= (uint32_t)26;
	frame->r[27]	= (uint32_t)27;
	frame->r[28]	= (uint32_t)28;
	frame->r[29]	= (uint32_t)frame;
	frame->r[30]	= (uint32_t)30;
	frame->r[31]	= (uint32_t)31;
	frame->hi		= (uint32_t)0;
	frame->lo		= (uint32_t)0;
	frame->badVAddr	= (uint32_t)0;
	frame->cause	= (uint32_t)0;
	frame->epc		= (uint32_t)OsclKruxEntry;
	frame->status	= (uint32_t)(
							Oscl::HW::MIPS::CP0::Status::CU::CU3::ValueMask_AccessAllowed
						|	Oscl::HW::MIPS::CP0::Status::CU::CU2::ValueMask_AccessAllowed
						|	Oscl::HW::MIPS::CP0::Status::CU::CU1::ValueMask_AccessAllowed
						|	Oscl::HW::MIPS::CP0::Status::CU::CU0::ValueMask_AccessAllowed
						|	Oscl::HW::MIPS::CP0::Status::RP::ValueMask_NormalPowerMode
						|	Oscl::HW::MIPS::CP0::Status::RE::ValueMask_UserModeConfigEndianess
						|	Oscl::HW::MIPS::CP0::Status::BEV::ValueMask_Normal
						|	Oscl::HW::MIPS::CP0::Status::TS::ValueMask_Normal
						|	Oscl::HW::MIPS::CP0::Status::SR::ValueMask_NotSoftReset
						|	Oscl::HW::MIPS::CP0::Status::NMI::ValueMask_NotNMI
						|	Oscl::HW::MIPS::CP0::Status::IM::IM7::ValueMask_Disable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM6::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM5::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM4::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM3::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM2::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM1::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::IM::IM0::ValueMask_Enable
						|	Oscl::HW::MIPS::CP0::Status::KSU::ValueMask_KernelMode
						|	Oscl::HW::MIPS::CP0::Status::ERL::ValueMask_Normal
						|	Oscl::HW::MIPS::CP0::Status::EXL::ValueMask_Exception
						|	Oscl::HW::MIPS::CP0::Status::IE::ValueMask_InterruptsEnable
						);
	return frame;
	}

/*****************************************************************************
 * OsclKruxEntry()
 */

void OsclKruxEntry(EntryPoint *entry) noexcept{
	entry->entry();
	// If this ever returns it would be wise to
	// call currentThread->userSuspend()
	}

/*****************************************************************************
 * OsclKruxStartTrap()
 */

extern void dummy();
void OsclKruxStartTrap(OsclKruxFrame *frame) noexcept {
#if 0
	asm volatile (
					"	.set push\n"
					"	.set noreorder\n"
					"	b	StartOOOS\n"
					"	move	$4,%0\n"
//					"	nop\n"
					"	.set pop\n"
					: : "r" (frame)
					: "$4","memory"
					);
#else
	StartOOOS(frame);
#endif
}

/*****************************************************************************
 * OsclKernelVirtualToPhysical()
 */

void*	OsclKernelVirtualToPhysical(void* virtualAddress) noexcept{
	return MmuVirtualToPhysical(virtualAddress);
	}

/*****************************************************************************
 * OsclCacheInhibitRange()
 */

void	OsclCacheInhibitRange(void* virtualAddress,unsigned long size) noexcept{
	MmuCacheInhibitRange(virtualAddress,size);
	}

/*****************************************************************************
 * OsclKernelGetPageSize()
 */

unsigned long	OsclKernelGetPageSize() noexcept{
	return OsclPageSize;
	}

/*****************************************************************************
 * OsclCacheGetLineSize()
 */

unsigned long	OsclCacheGetLineSize() noexcept{
	// The IXP4xx has a 32 byte cache line size.
	return 32;
	}

