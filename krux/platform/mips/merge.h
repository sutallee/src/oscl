#
# This file contains the entry point for this application. It *must*
# be located as the first object in the application. This application
# is intended to be invoked by a boot loader with a valid stack pointer.
#
#include "asmreg.h"
#include "frame.h"

        .macro  __saveContext
        .set    push
        .set    noat
        .set    reorder
         move   k1, sp
8:      move    k0, sp
        subu sp, k1, FRAME_SIZE
        sw  k0, FRAME_R29(sp)
        sw  $3, FRAME_R3(sp)
        sw  $0, FRAME_R0(sp)
        mfc0    v1, cp0status, 0
        sw  $2, FRAME_R2(sp)
        sw  v1, FRAME_STATUS(sp)
        sw  $4, FRAME_R4(sp)
        mfc0    v1, cp0cause
        sw  $5, FRAME_R5(sp)
        sw  v1, FRAME_CAUSE(sp)
        sw  $6, FRAME_R6(sp)
        mfc0    v1, cp0epc
        sw  $7, FRAME_R7(sp)
        sw  v1, FRAME_EPC(sp)
        sw  v1, FRAME_EPC(sp)
        sw  $25, FRAME_R25(sp)
        sw  gp, FRAME_R28(sp)
        sw  $31, FRAME_R31(sp)
# SAVE_AT
        sw  $1, FRAME_R1(sp)
# SAVE_TEMP
        mfhi    v1
        sw  $8, FRAME_R8(sp)
        sw  $9, FRAME_R9(sp)
        sw  v1, FRAME_HI(sp)
        mflo    v1
        sw  $10, FRAME_R10(sp)
        sw  $11, FRAME_R11(sp)
        sw  v1,  FRAME_LO(sp)
        sw  $12, FRAME_R12(sp)
        sw  $13, FRAME_R13(sp)
        sw  $14, FRAME_R14(sp)
        sw  $15, FRAME_R15(sp)
        sw  $24, FRAME_R24(sp)
# SAVE_STATIC
        sw  $16, FRAME_R16(sp)
        sw  $17, FRAME_R17(sp)
        sw  $18, FRAME_R18(sp)
        sw  $19, FRAME_R19(sp)
        sw  $20, FRAME_R20(sp)
        sw  $21, FRAME_R21(sp)
        sw  $22, FRAME_R22(sp)
        sw  $23, FRAME_R23(sp)
        sw  $30, FRAME_R30(sp)
        .set    pop 
        .endm   

        .macro  RESTORE_TEMP
        lw  $24, FRAME_LO(sp)
        lw  $8, FRAME_R8(sp)
        lw  $9, FRAME_R9(sp)
        mtlo    $24
        lw  $24, FRAME_HI(sp)
        lw  $10, FRAME_R10(sp)
        lw  $11, FRAME_R11(sp)
        mthi    $24
        lw  $12, FRAME_R12(sp)
        lw  $13, FRAME_R13(sp)
        lw  $14, FRAME_R14(sp)
        lw  $15, FRAME_R15(sp)
        lw  $24, FRAME_R24(sp)
        .endm

        .macro  RESTORE_STATIC
        lw  $16, FRAME_R16(sp)
        lw  $17, FRAME_R17(sp)
        lw  $18, FRAME_R18(sp)
        lw  $19, FRAME_R19(sp)
        lw  $20, FRAME_R20(sp)
        lw  $21, FRAME_R21(sp)
        lw  $22, FRAME_R22(sp)
        lw  $23, FRAME_R23(sp)
        lw  $30, FRAME_R30(sp)
        .endm

        .macro  RESTORE_AT
        .set    push
        .set    noat
        lw  $1,  FRAME_R1(sp)
        .set    pop
        .endm

        .macro  RESTORE_SOME
        .set    push
        .set    reorder
        .set    noat 
        mfc0    a0, cp0status
        ori a0, 0x1f
        xori    a0, 0x1f
        mtc0    a0, cp0status
        li  v1, 0xff00
        and a0, v1
        lw  v0, FRAME_STATUS(sp)
        nor v1, $0, v1
        and v0, v1
        or  v0, a0
        mtc0    v0, cp0status
		ssnop
		ssnop
		ssnop
		ssnop
        lw  $31, FRAME_R31(sp)
        lw  gp, FRAME_R28(sp)
        lw  $25, FRAME_R25(sp)
        lw  $7,  FRAME_R7(sp)
        lw  $6,  FRAME_R6(sp)
        lw  $5,  FRAME_R5(sp)
        lw  $4,  FRAME_R4(sp)
        lw  $3,  FRAME_R3(sp)
        lw  $2,  FRAME_R2(sp)
        .set    pop
        .endm


        .macro  RESTORE_SP_AND_RET
        .set    push
        .set    noreorder
        lw  k0, FRAME_EPC(sp)
        lw  sp, FRAME_R29(sp)
        jr  k0
         rfe
        .set    pop
        .endm


        .macro  RESTORE_ALL
        RESTORE_TEMP
        RESTORE_STATIC
        RESTORE_AT
        RESTORE_SOME
        RESTORE_SP
        .endm

        .macro  RESTORE_ALL_AND_RET
        RESTORE_TEMP
        RESTORE_STATIC
        RESTORE_AT
        RESTORE_SOME
        RESTORE_SP_AND_RET
        .endm

interruptEntryPoint:
	SAVE_ALL
	jal	interruptHandler
	RESTORE_ALL_AND_RET

