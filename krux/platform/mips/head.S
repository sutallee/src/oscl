#
# This file contains the entry point for this application. It *must*
# be located as the first object in the application. This application
# is intended to be invoked by a boot loader with a valid stack pointer.

#
# For KRUX, the general exception vector is 0xBFC00180 .
# There is executable code at this location, NOT simply
# a pointer to the exception routine. A typical sequence
# is:
#
#	lui $26,%hi(generalExceptionHandler)
#	addiu	$26,$24,%lo(generalExceptionHandler)
#	jr		$26
#	nop
#
# However, there are only 128 bytes (32 words) for the general
# exception vector.
#
#	The cause of the general exception can be from one of many
#	sources including interrupts and system calls. The
#	CP0 STATUS register must be examined to determine
#	the source of the exception. In the case of an interrupt
#	cause, the SYS_INT PRIORITY_INDEX register must be consulted
#	to further determine the interrupt source.
#

#include "save.h"
#include "restore.h"

	.globl	__mipsReset
	.set	noreorder
__mipsReset:
	# Setup the stack pointer
#	lui	$16,%hi(TiTnetv105xInitialSP)
#	addiu	sp,$16,%lo(TiTnetv105xInitialSP)
	lui	$16,%hi(__SUPER_STACK_END__)
	addiu	sp,$16,%lo(__SUPER_STACK_END__)

	# Clear interrupt enables
	mtc0	$0,$12,0
	ssnop
	ssnop
	ssnop
	ssnop
	#
	cache	0x19,0(a0)
#	lui		t0,%hi(0xA8612430)
#	addiu	t1,t0,%lo(0xA8612430)
#	lui		t2,%hi(0xA8612434)
#	addiu	t3,t2,%lo(0xA8612434)
#	nor	a0,zero,zero
#	sw	a0,0(t1)
#	sw	a0,0(t3)

#	Copy General Exception code
	# Copy the general exception vector code to 0x80000180
	## load source pointer
	lui		t0,%hi(__generalExceptionVectorCode)
	nop
	addiu	t0,t0,%lo(__generalExceptionVectorCode)
	## load destination pointer
	lui		t1,%hi(0x80000180)
	nop
	addiu	t1,t1,%lo(0x80000180)
	## load destination pointer
	lui		t3,%hi(0xA0000180)
	nop
	addiu	t4,t3,%lo(0xA0000180)
	## copy the lui
	lw		t2,0(t0)
	nop
	sw		t2,0(t1)
	sw		t2,0(t4)
	## copy the addiu
	lw		t2,4(t0)
	nop
	sw		t2,4(t1)
	sw		t2,4(t4)
	## copy the jr
	lw		t2,8(t0)
	nop
	sw		t2,8(t1)
	sw		t2,8(t4)
	## copy the nop
	lw		t2,12(t0)
	nop
	sw		t2,12(t1)
	sw		t2,12(t4)
	## copy the nop
	lw		t2,16(t0)
	nop
	sw		t2,16(t1)
	sw		t2,16(t4)
	## copy the nop
	lw		t2,20(t0)
	nop
	sw		t2,20(t1)
	sw		t2,20(t4)
#### extra #####
	lw		t2,24(t0)
	nop
	sw		t2,24(t1)
	sw		t2,24(t4)

	lw		t2,28(t0)
	nop
	sw		t2,28(t1)
	sw		t2,28(t4)

	lw		t2,32(t0)
	nop
	sw		t2,32(t1)
	sw		t2,32(t4)

	lw		t2,36(t0)
	nop
	sw		t2,36(t1)
	sw		t2,36(t4)

	lw		t2,40(t0)
	nop
	sw		t2,40(t1)
	sw		t2,40(t4)
#### end extra #####


#	Copy TLB Refill, EXL=0 code to 0x80000000
	## load source pointer
	lui		t0,%hi(__tlbRefillExl0Code)
	nop
	addiu	t0,t0,%lo(__tlbRefillExl0Code)
	## load destination pointer
	lui		t1,%hi(0x80000000)
	nop
	addiu	t1,t1,%lo(0x80000000)
	## load destination pointer
	lui		t3,%hi(0xA0000000)
	nop
	addiu	t4,t3,%lo(0xA0000000)
	## copy the lui
	lw		t2,0(t0)
	nop
	sw		t2,0(t1)
	sw		t2,0(t4)
	## copy the addiu
	lw		t2,4(t0)
	nop
	sw		t2,4(t1)
	sw		t2,4(t4)
	## copy the jr
	lw		t2,8(t0)
	nop
	sw		t2,8(t1)
	sw		t2,8(t4)
	## copy the nop
	lw		t2,12(t0)
	nop
	sw		t2,12(t1)
	sw		t2,12(t4)
	## copy the nop
	lw		t2,16(t0)
	nop
	sw		t2,16(t1)
	sw		t2,16(t4)
	## copy the nop
	lw		t2,20(t0)
	nop
	sw		t2,20(t1)
	sw		t2,20(t4)

#	Copy Cache Error code to 0x80000100
	## load source pointer
	lui		t0,%hi(__cacheErrorCode)
	nop
	addiu	t0,t0,%lo(__cacheErrorCode)
	## load destination pointer
	lui		t1,%hi(0x80000100)
	nop
	addiu	t1,t1,%lo(0x80000100)
	## load destination pointer
	lui		t3,%hi(0xA0000100)
	nop
	addiu	t4,t3,%lo(0xA0000100)
	## copy the lui
	lw		t2,0(t0)
	nop
	sw		t2,0(t1)
	sw		t2,0(t4)
	## copy the addiu
	lw		t2,4(t0)
	nop
	sw		t2,4(t1)
	sw		t2,4(t4)
	## copy the jr
	lw		t2,8(t0)
	nop
	sw		t2,8(t1)
	sw		t2,8(t4)
	## copy the nop
	lw		t2,12(t0)
	nop
	sw		t2,12(t1)
	sw		t2,12(t4)
	## copy the nop
	lw		t2,16(t0)
	nop
	sw		t2,16(t1)
	sw		t2,16(t4)
	## copy the nop
	lw		t2,20(t0)
	nop
	sw		t2,20(t1)
	sw		t2,20(t4)
#	Copy Alternate Interrupt code to 0x80000200
	## load source pointer
	lui		t0,%hi(__alternateInterruptCode)
	nop
	addiu	t0,t0,%lo(__alternateInterruptCode)
	## load destination pointer
	lui		t1,%hi(0x80000200)
	nop
	addiu	t1,t1,%lo(0x80000200)
	## load destination pointer
	lui		t3,%hi(0xA0000200)
	nop
	addiu	t4,t3,%lo(0xA0000200)
	## copy the lui
	lw		t2,0(t0)
	nop
	sw		t2,0(t1)
	sw		t2,0(t4)
	## copy the addiu
	lw		t2,4(t0)
	nop
	sw		t2,4(t1)
	sw		t2,4(t4)
	## copy the jr
	lw		t2,8(t0)
	nop
	sw		t2,8(t1)
	sw		t2,8(t4)
	## copy the nop
	lw		t2,12(t0)
	nop
	sw		t2,12(t1)
	sw		t2,12(t4)
	## copy the nop
	lw		t2,16(t0)
	nop
	sw		t2,16(t1)
	sw		t2,16(t4)
	## copy the nop
	lw		t2,20(t0)
	nop
	sw		t2,20(t1)
	sw		t2,20(t4)

	sync
	lui		t0,%hi(mipsReset)
	addiu   t1,t0,%lo(mipsReset)
	jr      t1
	nop

__generalExceptionVectorCode:
	lui		k0,%hi(__generalException)
	addiu   k1,k0,%lo(__generalException)
	jr      k1
	nop

__tlbRefillExl0Code:
	lui		k0,%hi(__tlbRefillExl0)
	addiu   k1,k0,%lo(__tlbRefillExl0)
	jr      k1
	nop

__tlbRefillExl0:
	__saveContext
	jal	mipsTlbRefillExl0
	move	a0,sp
	b.
	nop

__cacheErrorCode:
	lui		k0,%hi(__cacheError)
	addiu   k1,k0,%lo(__cacheError)
	jr      k1
	nop

__cacheError:
	__saveContext
	jal	mipsCacheError
	move	a0,sp
	b.
	nop

__alternateInterruptCode:
	lui		k0,%hi(__alternateInterrupt)
	addiu   k1,k0,%lo(__alternateInterrupt)
	jr      k1
	nop

__alternateInterrupt:
	__saveContext
	jal	mipsAlternateInterrupt
	move	a0,sp
	b.
	nop

__generalException:
	# Read the cause register
	mfc0	k0, $13, 0
	nop
	sll		k0,k0,25
	lui		k1,%hi(causeTable)
	sra		k0,k0,27
	addiu	k1,k1,%lo(causeTable)
	sll		k0,k0,2
	nop
	addu	k0,k0,k1
	nop
	lw		k0,0(k0)
	nop
	jr		k0
	nop

__TlbModification:
__TlbStore:
__FetchBusError:
__DataBusError:
__Breakpoint:
__ReservedInstruction:
__CoprocessorUnusable:
__ArithmeticOverflow:
__Trap:
__FloatingPoint:
__Coprocessor2:
__MDMXUnusable:
__Watch:
__MachineCheck:
__CacheError:
__Reserved:
	__saveContext
	//Ensure that frame pointer is in a0!
	jal	mipsBadException
	move	a0,sp
	b	.
	nop

__TlbLoadFetch:
	__saveContext
	//Ensure that frame pointer is in a0!
	jal	mipsTlbLoadFetch
	move	a0,sp
	jal	Schedule
	move	a0,sp
	move	sp,v0
	__restoreContextAndReturn

__StoreAddressError:
	__saveContext
	//Ensure that frame pointer is in a0!
	jal	mipsStoreAddressError
	move	a0,sp
	jal	Schedule
	move	a0,sp
	move	sp,v0
	__restoreContextAndReturn

	
__SystemCall:
	__saveContext
	//Ensure that frame pointer is in a0!
#
#	lui		a0,0x1555
#	lui		k0,%hi(__printWord)
#	addiu   k1,k0,%lo(__printWord)
#	jalr    k1
#	nop
#	b	.
#	nop
#
	jal	mipsSystemCall
	move	a0,sp
	jal	Schedule
	move	a0,sp
	move	sp,v0
	__restoreContextAndReturn

__Interrupt:
	__saveContext
	//Ensure that frame pointer is in a0!
	jal	mipsInterrupt
	move	a0,sp
	jal	Schedule
	move	a0,sp
	move	sp,v0
	__restoreContextAndReturn

__LoadFetchAddressError:
	__saveContext
	//Ensure that frame pointer is in a0!
	jal	mipsLoadFetchAddressError
	move	a0,sp
	jal	Schedule
	move	a0,sp
	move	sp,v0
	__restoreContextAndReturn

causeTable:
	.long	__Interrupt
	.long	__TlbModification
	.long	__TlbLoadFetch
	.long	__TlbStore
	.long	__LoadFetchAddressError
	.long	__StoreAddressError
	.long	__FetchBusError
	.long	__DataBusError
	.long	__SystemCall
	.long	__Breakpoint
	.long	__ReservedInstruction
	.long	__CoprocessorUnusable
	.long	__ArithmeticOverflow
	.long	__Trap
	.long	__Reserved
	.long	__FloatingPoint
	.long	__Reserved
	.long	__Reserved
	.long	__Coprocessor2
	.long	__Reserved
	.long	__Reserved
	.long	__Reserved
	.long	__MDMXUnusable
	.long	__Watch
	.long	__MachineCheck
	.long	__Reserved
	.long	__Reserved
	.long	__Reserved
	.long	__Reserved
	.long	__Reserved
	.long	__CacheError
	.long	__Reserved
