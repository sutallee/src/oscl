/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_timer_systimerh_
#define _oscl_krux_timer_systimerh_
#include "uktimer.h"
#include "oscl/krux/sema/bqsema.h"
#include "oscl/krux/port/trap.h"
#include "oscl/mt/runnable.h"
#include "oscl/krux/mutex/sema.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** This class implements the system timer thread which is the
	operating system entity responsible for maintaining timers
	used by the rest of the system.
 */

class SystemTimerThread:	public Mt::Runnable,
							public SystemTimerNode
							{
	private:
		/** The semaphore which is signaled by the clock driver interrupt
			service routine every TOCK.
		 */

		Sema::Base&					_tockSema;

	public:
		/** This is the constructor.
		 */
		SystemTimerThread(Sema::Base& tockSema) noexcept;

		/** This function implements the Runnable interface and is
			called when the thread begins execution. The main event loop
			of the system timer is contained in this function.
		 */
		void run() noexcept;

		/** This function is invoked from thread level
		 *  to insert a timer into the SystemTimerThread list.
		 */
		void start(SystemTimer* timer) noexcept;

		/** This function is invoked from thread level
		 *  to remove a timer from the SystemTimerThread list.
		 */
		void stop(SystemTimer* timer) noexcept;

		/** This function is invoked from supervisor level
		 *  to insert a timer into the SystemTimerThread list.
		 */
		void suStart(SystemTimer* timer) noexcept;

		/** This function is invoked from supervisor level
		 *  to remove a timer from the SystemTimerThread list.
		 */
		void suStop(SystemTimer* timer) noexcept;

		/** This function is invoked from supervisor level
		 *  to process the active timers.
		 */
		void suTock() noexcept;

		/** This class function is returns the singleton reference
		 *  to the SystemTimerThread.
		 */
		static SystemTimerThread&	GetSystemTimer(void) noexcept;

	private:
		/** This function is used by the timer thread to insert the
		 *	specified timer into the timer list before the specified timer.
		 */
		void insert(	SystemTimerNode*	nextTimer,
						SystemTimerNode*	newTimer
						) noexcept;

		/** This function is used by the timer thread to insert the
		 *	specified timer at the end of the timer list.
		 */
		void insert(SystemTimerNode* newTimer) noexcept;

		/** This function is used by the timer thread to remove the
		 *	specified timer from the timer list
		 */
		void remove(SystemTimerNode* timer) noexcept;

	public:
		inline SystemTimer*	getTimer() noexcept { return 0;}
	};


/** This concrete trap class is used by clients to start
	system timers.
	*/

class StartTimerTrap : public Trap {
	private:
		/** A reference to the system timer thread.
		 */

		SystemTimerThread&	_timerThread;

		/** A pointer to the timer to start.
		 */

		SystemTimer*			_timer;
	public:
		/** The trap constructor.
		 */
		StartTimerTrap(	SystemTimerThread&	timerThread,
						SystemTimer*		timer
						) noexcept:
			_timerThread(timerThread),_timer(timer){
			}

		/** The trap service routine that is invoked to start the timer
			from the operating system trap.
		 */
		void suService(void) noexcept;
	};

/** This concrete trap class is used by clients to stop
	system timers.
 */

class StopTimerTrap : public Trap {
	private:
		/** A reference to the system timer thread.
		 */

		SystemTimerThread&	_timerThread;

		/** A pointer to the timer to start.
		 */

		SystemTimer*		_timer;
	public:
		/** The trap constructor.
		 */
		StopTimerTrap(	SystemTimerThread&	timerThread,
						SystemTimer*		timer
						) noexcept:
			_timerThread(timerThread),_timer(timer){
			}

		/** The trap service routine that is invoked to start the timer
			from the operating system trap.
		 */
		void suService(void) noexcept;
	};

/** This concrete trap class is used by the system timer thread to
	trap to the operating system and process all pending timers.
 */

class ExpireTimerTrap : public Trap {
	private:
		/** A reference to the timer thread used to envoke
			timer management routines once inside the trap.
		 */
		SystemTimerThread&	_timerThread;
	public:
		/** The timer expirator constructor.
		 */
		ExpireTimerTrap(SystemTimerThread& timerThread) noexcept:
			_timerThread(timerThread){
			}

		/** The trap service routine that is invoked to begin processing
			timers from the operating system trap.
		 */
		void suService(void) noexcept;
	};

}
}

#endif
