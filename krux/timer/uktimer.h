/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_timer_uktimerh_
#define _oscl_krux_timer_uktimerh_
#include "oscl/krux/port/trap.h"
#include "oscl/mt/timer.h"

/** */
namespace Oscl {
/** */
namespace Krux {

class SystemTimer;

/** This class defines a doubly linked list node for system timers.
 */
class SystemTimerNode {
	public:
		/** This forward link field points to the previous element in the timer list,
			or is zero if this timer is at the head of the list.
		 */
		SystemTimerNode*	_prevLink;

		/** This backward link field points to the next element in the timer list,
			or is zero if this timer is at the tail of the list.
		 */
		SystemTimerNode*	_nextLink;

	public:
		/** */
		SystemTimerNode() noexcept:_prevLink(0),_nextLink(0){}
		/** */
		SystemTimerNode(SystemTimerNode* nextLink,SystemTimerNode* prevLink) noexcept:_prevLink(prevLink),_nextLink(nextLink){}

		/** Make GCC happy */
		virtual ~SystemTimerNode() {}

	public:
		virtual SystemTimer*	getTimer() noexcept=0;
	};

/** Abstract class representing a generic timer.
 */
class SystemTimer : public Mt::Timer , public SystemTimerNode {
	private:
		/** This boolean variable indicates the state of the timer from the user's
			perspective.
		 */
		bool			_active;

	public:
		/** The number of tocks remaining before the timer expires. This public variable is
		 	only to be accessed by the SystemTimerThread.
		 */
		unsigned long	_tocksRemaining;

		/** The number of tocks in the delay when the timer is started and restarted.
		 */
		unsigned long	_tockDelay;

	public:
		/** The constructor initializes the length of the timer.
		 */
		SystemTimer(unsigned long tocks) noexcept:SystemTimerNode(),_active(false),_tockDelay(tocks){};

		/** This function starts the timer. If the timer is already active, then the timer
			is restarted. It can be overridden by sub-classes,
			but should be invoked by the subclass from the overriding implementation.
		 */

		virtual void start() noexcept;

		/** This function starts the timer with the specified value. If the timer
			is already active, then the timer is restarted with the new value.
			It can be overridden by sub-classes, but should be invoked by
			the subclass from the overriding implementation.
		 */

		virtual void start(unsigned long tocks) noexcept;

		/** This function is semantically the same as the start() function.
		 */

		virtual void restart() noexcept;

		/** This function is semantically the same as the start(unsigned long) function.
		 */

		virtual void restart(unsigned long tocks) noexcept;

		/** This function stops the timer. It can be overridden by sub-classes,
			but should be invoked by the subclass from the overriding implementation.
		 */

		virtual void stop() noexcept;

		/** This function decrements the timer and returns true
		 	if the timer has decremented to zero.
		 */
		bool suTockExpired() noexcept;

		/** This function is called from the supervisor
			level by a trap, and causes the timer to do whatever
		 	it should when it is expired. The timer has
		 	been released by the SystemTimerThread lists
		 	before this function is invoked.
		 */
		virtual void suExpire() noexcept= 0;

		/** This function returns with an indication as to whether the
			the timer has expired. This is a default implementation which
			may be overridden by sub-classes.
		 */
		virtual bool hasExpired() noexcept;

		inline SystemTimer*	getTimer() noexcept { return this;}
	};

}
}

#endif
