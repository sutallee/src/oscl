/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_timer_timerh_
#define _oscl_krux_timer_timerh_

/** */
namespace Oscl {
/** */
namespace Krux {

/** */
namespace Sema {
	class Base;
}

/** Represnts the timer management of the micro-kernel.
 */
class Timer {
	private:
		/** System dependent number of milliseconds between
			calls to the tick() member function.
		 */
		unsigned long	_milliSecondsPerTick;

	protected:
		/** The semaphore that is signaled by the system clock
			interrupt service routine every TOCK period.
		 */
		Sema::Base&		_tockSema;

	public:
		/** Constructor initializes expected tick rate and tock semaphore.
		 */
		Timer(unsigned long milliSecondsPerTick,Sema::Base& tockSema) noexcept;

		/** Destructor is virtual.
		 */
		virtual ~Timer();

		/** This is invoked for each timer interrupt.
			This function should only be called only from the
			platform interrupt service routine . It is expected that
			this function runs in an interrupt context.
		 */
		virtual void suTick() noexcept;
	};

}
}

#endif
