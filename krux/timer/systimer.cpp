/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "systimer.h"

extern Oscl::Krux::SystemTimerThread	TheSystemTimerThread;

using namespace Oscl::Krux;

SystemTimerThread::SystemTimerThread(Sema::Base& tockSema) noexcept:
		SystemTimerNode(this,this),
		_tockSema(tockSema)
		{
	}

void SystemTimerThread::start(SystemTimer* timer) noexcept{
	StartTimerTrap	start(*this,timer);
	start.trap();
	}

void SystemTimerThread::stop(SystemTimer* timer) noexcept{
	StopTimerTrap		stop(*this,timer);
	stop.trap();
	}

void SystemTimerThread::suStart(SystemTimer* newTimer) noexcept{
	SystemTimerNode*	next;
	if(newTimer->_nextLink || newTimer->_prevLink){
		// Timer is already active
		remove(newTimer);
		}
	if(!(newTimer->_tocksRemaining = newTimer->_tockDelay)){
		// timer has zero delay
		newTimer->suExpire();
		return;
		}
	for(next=_nextLink;next->getTimer();next=next->_nextLink){
		if(newTimer->_tocksRemaining < next->getTimer()->_tocksRemaining){
			insert(next,newTimer);
			next->getTimer()->_tocksRemaining	-= newTimer->_tocksRemaining;
			break;
			}
		newTimer->_tocksRemaining -= next->getTimer()->_tocksRemaining;
		}
	if(!next->getTimer()){
		insert(newTimer);
		}
	}

void SystemTimerThread::insert(	SystemTimerNode*	nextTimer,
								SystemTimerNode*	newTimer
								) noexcept{
	// Update links in new timer node
	newTimer->_nextLink				= nextTimer;
	newTimer->_prevLink				= nextTimer->_prevLink;

	// Update links in previous timer node
	nextTimer->_prevLink->_nextLink	= newTimer;

	// Update links in next timer node
	nextTimer->_prevLink			= newTimer;
	}

void SystemTimerThread::insert(SystemTimerNode* newTimer) noexcept{
	// Update links in new timer node
	newTimer->_nextLink				= this;
	newTimer->_prevLink				= _prevLink;

	_prevLink->_nextLink			= newTimer;
	_prevLink						= newTimer;
	}

void SystemTimerThread::remove(SystemTimerNode* timer) noexcept{
	if(timer->_nextLink){
		SystemTimer*	t;
		t	= timer->_nextLink->getTimer();
		if(t){
			// Its not the "null" timer
			t->_tocksRemaining += timer->getTimer()->_tocksRemaining;
			}
		}
	timer->_prevLink->_nextLink	= timer->_nextLink;
	timer->_nextLink->_prevLink	= timer->_prevLink;
	timer->_nextLink	=
	timer->_prevLink	= 0;
	}

void SystemTimerThread::suStop(SystemTimer* newTimer) noexcept{
	remove(newTimer);
	}

void SystemTimerThread::run() noexcept{
	ExpireTimerTrap	expire(*this);
	while(true){
		_tockSema.wait();
		if(_nextLink->getTimer()){
			expire.trap();
			}
		}
	}

void SystemTimerThread::suTock() noexcept{
	SystemTimer*	next;
	if(_nextLink->getTimer()){
		if(_nextLink->getTimer()->suTockExpired()){
			while(		(_nextLink->getTimer())
					&&	!(_nextLink->getTimer()->_tocksRemaining)
					){
				next	= _nextLink->getTimer();
				remove(next);
				next->suExpire();
				}
			}
		}
	}

void ExpireTimerTrap::suService(void) noexcept{
	_timerThread.suTock();
	}

void StartTimerTrap::suService(void) noexcept{
	_timerThread.suStart(_timer);
	}

void StopTimerTrap::suService(void) noexcept{
	_timerThread.suStop(_timer);
	}

SystemTimerThread&	SystemTimerThread::GetSystemTimer(void) noexcept{

	return TheSystemTimerThread;
	}

