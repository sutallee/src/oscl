/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "uktimer.h"
#include "systimer.h"

using namespace Oscl::Krux;

bool SystemTimer::suTockExpired() noexcept{
	_tocksRemaining--;
	if(_tocksRemaining > 0x80000000){
		while(true);
		}
	return !_tocksRemaining;
	}

void SystemTimer::start() noexcept{
	_active	= true;
	SystemTimerThread::GetSystemTimer().start(this);
	}

void SystemTimer::start(unsigned long tocks) noexcept{
	_tockDelay	= tocks;
	start();
	}

void SystemTimer::restart() noexcept{
	start();
	}

void SystemTimer::restart(unsigned long tocks) noexcept{
	start(tocks);
	}

void SystemTimer::stop() noexcept{
	if(_active){
		_active	= false;
		SystemTimerThread::GetSystemTimer().stop(this);
		}
	}

bool SystemTimer::hasExpired() noexcept{
	return (_active && (!_prevLink && !_nextLink));
	}

