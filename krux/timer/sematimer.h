/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_timer_sematimerh_
#define _oscl_krux_timer_sematimerh_
#include "uktimer.h"
#include "oscl/mt/sema/api.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** This class implements a timer which signals
	the supplied semaphore when it expires.
 */

class SemaTimer : public SystemTimer {
	private:
		/** Reference to the semaphore which will be signaled
			upon timer expiration.
		 */
		Mt::Sema::Api&		_sema;

	public:
		/** The constructor initializes the length of the timer.
		 */
		SemaTimer(Mt::Sema::Api& sema,unsigned long tocks) noexcept;

	public: // SystemTimer implementation
		/** This function is called from the supervisor
			level by a trap, and causes the timer semaphore
			to be signaled.
		 */
		void suExpire() noexcept;

	public:
		/** This function causes the calling thread to block and wait until
			the timer expires.
		 */
		void wait() noexcept;
	};

}
}

#endif
