/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_spri_sprih_
#define _oscl_krux_spri_sprih_
#include "oscl/queue/defpriq.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** Represents an entry in a priority band. A priority band
	is a group of threads with the same priority.
 */
class SchedPriBandEntry : public DeferedPrioritizedItem<SchedPriBandEntry>{
	public:
		/** Constructor
		 */
		SchedPriBandEntry() noexcept;

		/** Implements the pure virtual function mandated by
			PriorityQueue to order items in the queue.
			Returns true if the priority of this item is
			greater than the priority of the "priority" argument.
			Currently, this function always returns false,
			resulting in a default FIFO ordering.
			Note: not currently used.
		 */
		bool			isHigherPriority(SchedPriBandEntry *priority) noexcept;

		/** Implements the pure virtual function mandated by PriorityQueue
			to order items in the queue. Returns true if the priority
			of this item is lower than the priority of the "priority"
			argument.  Currently, this function always returns true,
			resulting in a default FIFO ordering.
			Note: not currently used.
		 */
		bool			isLowerPriority(SchedPriBandEntry *priority) noexcept;

		/** This pure virtual function is inherited from and used by
			DeferedPriorityQueue to activate the defered function.
			In this case the defered function will be to schedule the
			thread as the currently active thread.
		 */
		virtual void	activate(void) noexcept=0;
	};

/** Maintains a FIFO ordered queue of threads which are at the same
	priority level, and are ready to run. This class is built on
	a priority queue such that the OS can later be modified to have
	priorities within a priority band.
 */
class SchedPriBand : public DeferedPriorityQueue<SchedPriBandEntry>{
	public:
		/** Constructor
		 */
		SchedPriBand() noexcept;
	};

typedef QueueItem									SchedPriBandItem;
typedef Queue<SchedPriBandEntry>					SchedPriBandQ;
typedef PrioritizedItem<SchedPriBandEntry>			SchedPriBandPriItem;
typedef PriorityQueue<SchedPriBandEntry>			SchedPriBandPriQ;
typedef DeferedPrioritizedItem<SchedPriBandEntry>	SchedPriBandDefPriItem;
typedef DeferedPriorityQueue<SchedPriBandEntry>		SchedPriBandDefPriQ;

}
}

#endif
