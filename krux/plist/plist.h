/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_plist_plisth_
#define _oscl_krux_plist_plisth_
#include "oscl/queue/priorityq.h"
#include "oscl/krux/spri/spri.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** Represents an entry in a priority list. Each entry has a
	priority band in which it stores a list of threads which
	are ready to run. The priority list
	is a linked list of priority bands ordered by the priority,
	which is searched from highest to lowest priority for ready
	threads.
 */
class SchedPriListEntry : public PrioritizedItem<SchedPriListEntry>{
	private:
		/** The priority of this band.
		 */
		const unsigned	priority;

	public:
		/** The priority band implementation for this entry.
		 */
		SchedPriBand			priorityBand;

		/** The highest available priority.
		 */
		static const unsigned	highestPriority	= (unsigned)(-1);

		/** The lowest available priority.
		 */
		static const unsigned	lowestPriority	= 0;

	public:
		/** Constructor will eventually initialize the priority field
			if required. Higher nubers represent higher priority.
			Lower numbers are lower priority.
		 */
		SchedPriListEntry(unsigned pri) noexcept;

		/** Implements the pure virtual function mandated by PriorityQueue to order
			items in the queue. Returns true if this item has a higher priority than the
			"priority" argument item.
		 */
		bool			isHigherPriority(SchedPriListEntry *priority) noexcept;	// not currently used

		/** Implements the pure virtual function mandated by PriorityQueue to order
			items in the queue. Returns true if this item has a lower priority than the
			"priority" argument item. Currently, this function always returns false, resulting
			in a default FIFO ordering.
		 */
		bool			isLowerPriority(SchedPriListEntry *priority) noexcept;	// not currently used
	};


/** Maintains a prioritized list of priority bands. The list is searched from highest
	priority to lowest priority and each priority band is in turn searched for the
	first thread ready to execute.
 */
class SchedPriList : public PriorityQueue<SchedPriListEntry>{
	public:
		/** Constructor
		 */
		SchedPriList() noexcept;

		/** This member function is called to cause the prioiry band list
			to be searched for and make active the highest priority ready thread.
		 */
		void	scheduleHighestPriortyReady(void) noexcept;
	};

typedef QueueItem								SchedPriListQItem;
typedef Queue<SchedPriListEntry>				SchedPriListQ;
typedef PrioritizedItem<SchedPriListEntry>		SchedPriListEntryItem;
typedef PriorityQueue<SchedPriListEntry>		SchedPriListEntryQ;

}
}

#endif
