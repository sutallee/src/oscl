/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_mutex_semah_
#define _oscl_krux_mutex_semah_
#include "oscl/mt/mutex/simple.h"
#include "oscl/krux/sema/bqsema.h"

/** */
namespace Oscl {
/** */
namespace Krux {
/** */
namespace Mutex {

/** This class implements a mutual exclusion lock
 *	using a binary semaphore as the locking primitive.
 */
class Semaphore : public Oscl::Mt::Mutex::Simple::Api {
	private:
		/** An instance of a queued binary semaphore
			which is used to implement the locking
			mechanism.
		 */
		Krux::Sema::BinaryQueue	_sema;

	public:
		inline Semaphore(){
			_sema.suSignal();	// Default is signaled
			}
		/** The lock primitive is invoked by a thread
		 *	prior to entering a critical section,
		 *	to prevent other threads from manipulating
		 *	data while in the critical section.
		 */
		inline void	lock() noexcept{
			_sema.wait();
			}

		/** The unlock primitive is invoked by a thread
		 *	at the end of a critical section to enable
		 *	the next waiting thread to enter its critical
		 *	section.
		 */
		inline void	unlock() noexcept{
			_sema.signal();
			}
	};

}
}
}

#endif
