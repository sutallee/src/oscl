/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_mutex_recsemah_
#define _oscl_krux_mutex_recsemah_
#include "oscl/mt/mutex/recursive.h"
#include "oscl/krux/sema/bqsema.h"
#include "oscl/krux/thread/thread.h"

/** */
namespace Oscl {
/** */
namespace Krux {
/** */
namespace Mutex {

/** This class implements a recursive mutual exclusion lock
 *	using a binary semaphore as the locking primitive.
 */

class Recursive: public Oscl::Mt::Mutex::Recursive::Api {
	private:
		/** An instance of a queued binary semaphore
			which is used to implement the locking
			mechanism.
		 */
		Krux::Sema::BinaryQueue	_sema;

		/** This attribute records the thread that currently
			owns this mutex. The value is null if no thread
			currently owns the mutex.
		 */
		Krux::Thread*			_owner;

		/** This attibute is a count of the number of times
			that the owner has aquired a lock.
		 */
		unsigned				_count;

	public:
		/** */
		Recursive() noexcept;

		/** The lock primitive is invoked by a thread
		 *	prior to entering a critical section,
		 *	to prevent other threads from manipulating
		 *	data while in the critical section.
		 */
		void	lock() noexcept;

		/** The unlock primitive is invoked by a thread
		 *	at the end of a critical section to enable
		 *	the next waiting thread to enter its critical
		 *	section.
		 */
		void	unlock() noexcept;
	};

}
}
}

#endif
