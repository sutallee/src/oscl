/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_mutex_superh_
#define _oscl_krux_mutex_superh_
#include "oscl/mt/mutex/simple.h"
#include "oscl/kernel/interrupt.h"

/** */
namespace Oscl {
/** */
namespace Krux {

/** This class implements a mutual exclusion lock
 *	using supervisor mode interrupt locking primitives.
 */

class SupervisorMutex : public Oscl::Mt::Mutex::Simple::Api {
	private:
		/** Processor interrupt status mask is saved in
			this location while the lock is active.
		 */
		Oscl::Kernel::IrqMaskState	_save;

	public:
		/** The lock primitive is invoked by the a supervisor
		 *	mode program prior to entering a critical section,
		 *	to prevent other threads or interrupts from manipulating
		 *	data at the same time. The current exclusion
		 *	state is saved before being changed.
		 */
		inline void	lock() noexcept{
			OsclKernelDisableInterrupts(_save);
			}

		/** The unlock primitive is invoked by the supervisor
		 *	mode program at the end of a critical section to
		 *	restore the exclusion state using a value saved during
		 *	the lock operation.
		 */
		inline void	unlock() noexcept{
			OsclKernelRestoreInterruptState(_save);
			}
	};

}
}

#endif
