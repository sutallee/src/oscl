/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_platform_entrypointh_
#define _oscl_krux_platform_entrypointh_

/** */
namespace Oscl {
/** */
namespace Krux {

/** This abstract class represents an entry point
	for a thread. This is an abstract class used to decouple
	the micro kernel from the platform.
 */
class EntryPoint{
	public:
		/** Make GCC happy */
		virtual ~EntryPoint() {}

		/** The initial entry point for a thread.
		 */
		virtual void	entry(void) noexcept __attribute__((noreturn))= 0;
	};

}
}

#endif
