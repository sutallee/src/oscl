/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_platform_platformh_
#define _oscl_krux_platform_platformh_

/*
	This file describes platform dependent APIs which must be provided
	by the particular platform on which the OOOS runs. Most of the
	functions described in this file are used exclusively by the OOOS
	and the user initialization program in the main() routine.

	These functions are defined with "C" linkage to simplify their
	implementation when assembly language is required.
 */

#include "oscl/krux/port/frame.h"
#include "entry.h"
#include "oscl/krux/port/platform/platform.h"

extern "C"{
	/** This function must be called by the main() routine to setup
		platform specifics.
	 */

	void OsclKruxInit() noexcept;

	/** This function causes a CPU trap passing the supplied argument.
	 */
	void OsclKruxTrapWithArg(void *arg) noexcept;

	/** This function builds the platform specific stack frame at the
		location pointed to by the stack argument, such that the
		entry point is placed in the program counter location of the
		stack frame. This variant builds a supervisor mode frame.
	 */

	OsclKruxFrame* OsclKruxBuildStackFrame(	Oscl::Krux::EntryPoint&	entry,
									void*					stack,
									unsigned long			stackSize
									) noexcept;

	/** This function is called by the scheduler to start
		the OOOS. It never returns.
	 */
	void OsclKruxStartTrap(OsclKruxFrame *copyOfFrame) noexcept;

	/** This function is called by the trap used to start a thread. It invokes
		the thread's run() function.
	 */
	void OsclKruxEntry(Oscl::Krux::EntryPoint *entry) noexcept;

	/** This function must be called before constructors are invoked.
		This generally means before main() is called. It is intended
		to initialize hardware such as memory mapping peripherals
		and additional memory resources.
	 */
	void OsclKruxPreConstructorInit() noexcept;

	}

#endif
