/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "kernel.h"
#include "oscl/krux/thread/null.h"
#include "oscl/krux/timer/systimer.h"
#include "oscl/krux/thread/twstack.h"

using namespace Oscl::Krux;

Sema::BroadcastBinary	UKClockSema;
SchedPriList			Prioritizer;

SchedPriListEntry	nullPriority(SchedPriListEntry::lowestPriority);
SchedPriListEntry	highestPriority(SchedPriListEntry::highestPriority);
SchedPriListEntry	corePriority(3500);
SchedPriListEntry	lowestPriority(500);


NullThread<64>		nullThread(nullPriority.priorityBand);

SystemTimerThread	TheSystemTimerThread(UKClockSema);

static ThreadWithStack<200>	sysTimerThread(	highestPriority.priorityBand,
											TheSystemTimerThread
											);

void	StartKernel() noexcept{
	Prioritizer.insertByPriority(&highestPriority);
	Prioritizer.insertByPriority(&corePriority);
	Prioritizer.insertByPriority(&lowestPriority);
	Prioritizer.insertByPriority(&nullPriority);
	Oscl::Krux::Scheduler::GetScheduler().start();
	}


Oscl::Krux::SchedPriBand&	lowestPriorityBand() noexcept{
	return lowestPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	highestPriorityBand() noexcept{
	return highestPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	corePriorityBand() noexcept{
	return corePriority.priorityBand;
	}

