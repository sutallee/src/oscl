/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_kernel_basic_kernelh_
#define _oscl_krux_kernel_basic_kernelh_
#include "oscl/krux/plist/plist.h"
#include "oscl/krux/thread/spsched.h"
#include "oscl/krux/sema/bbsema.h"

/** */
extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;
/** */
extern Oscl::Krux::SchedPriList			Prioritizer;

/** */
extern void	StartKernel() noexcept;

/** */
extern Oscl::Krux::SchedPriBand&	highestPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	isrHighPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	isrLowPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	isrMiddlePriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	driverHighPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	driverLowPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	driverMiddlePriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	coreHighPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	coreLowPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	coreMiddlePriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	appHighPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	appLowPriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	appMiddlePriorityBand() noexcept;
/** */
extern Oscl::Krux::SchedPriBand&	lowestPriorityBand() noexcept;

#endif
