/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "kernel.h"
#include "oscl/krux/thread/null.h"
#include "oscl/krux/timer/systimer.h"
#include "oscl/krux/thread/twstack.h"

using namespace Oscl::Krux;

Sema::BroadcastBinary		UKClockSema;
SchedPriList                Prioritizer;

static SchedPriListEntry	nullPriority(SchedPriListEntry::lowestPriority);
static SchedPriListEntry	highestPriority(SchedPriListEntry::highestPriority);
static SchedPriListEntry	isrHighPriority(6500);
static SchedPriListEntry	isrMiddlePriority(6000);
static SchedPriListEntry	isrLowPriority(5500);
static SchedPriListEntry	driverHighPriority(5000);
static SchedPriListEntry	driverMiddlePriority(4500);
static SchedPriListEntry	driverLowPriority(4000);
static SchedPriListEntry	coreHighPriority(3500);
static SchedPriListEntry	coreMiddlePriority(3000);
static SchedPriListEntry	coreLowPriority(2500);
static SchedPriListEntry	appHighPriority(2000);
static SchedPriListEntry	appMiddlePriority(1500);
static SchedPriListEntry	appLowPriority(1000);
static SchedPriListEntry	lowestPriority(500);

// FIXME: We have a fundamental problem in that this stack
// size is system dependent. I have run into the problem
// where the default 512 is insufficient for the MIPS
// port... thus the 4x multiplier!
static 
NullThread<4*512>		nullThread(nullPriority.priorityBand);

SystemTimerThread	TheSystemTimerThread(UKClockSema);

// FIXME: We have a fundamental problem in that this stack
// size is system dependent. I have run into the problem
// where the default 512 is insufficient for the MIPS
// port... thus the 4x multiplier!
static ThreadWithStack<4*1024>	sysTimerThread(	highestPriority.priorityBand,
												TheSystemTimerThread
												);

void	StartKernel() noexcept{
	Prioritizer.insertByPriority(&highestPriority);
	Prioritizer.insertByPriority(&isrHighPriority);
	Prioritizer.insertByPriority(&isrMiddlePriority);
	Prioritizer.insertByPriority(&isrLowPriority);
	Prioritizer.insertByPriority(&driverHighPriority);
	Prioritizer.insertByPriority(&driverMiddlePriority);
	Prioritizer.insertByPriority(&driverLowPriority);
	Prioritizer.insertByPriority(&coreHighPriority);
	Prioritizer.insertByPriority(&coreMiddlePriority);
	Prioritizer.insertByPriority(&coreLowPriority);
	Prioritizer.insertByPriority(&appHighPriority);
	Prioritizer.insertByPriority(&appMiddlePriority);
	Prioritizer.insertByPriority(&appLowPriority);
	Prioritizer.insertByPriority(&lowestPriority);
	Prioritizer.insertByPriority(&nullPriority);
	Oscl::Krux::Scheduler::GetScheduler().start();
	}


Oscl::Krux::SchedPriBand&	lowestPriorityBand() noexcept{
	return lowestPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	highestPriorityBand() noexcept{
	return highestPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	isrHighPriorityBand() noexcept{
	return isrHighPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	isrLowPriorityBand() noexcept{
	return isrLowPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	isrMiddlePriorityBand() noexcept{
	return isrMiddlePriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	driverHighPriorityBand() noexcept{
	return driverHighPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	driverLowPriorityBand() noexcept{
	return driverLowPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	driverMiddlePriorityBand() noexcept{
	return driverMiddlePriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	coreHighPriorityBand() noexcept{
	return coreHighPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	coreLowPriorityBand() noexcept{
	return coreLowPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	coreMiddlePriorityBand() noexcept{
	return coreMiddlePriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	appHighPriorityBand() noexcept{
	return appHighPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	appLowPriorityBand() noexcept{
	return appLowPriority.priorityBand;
	}

Oscl::Krux::SchedPriBand&	appMiddlePriorityBand() noexcept{
	return appMiddlePriority.priorityBand;
	}

