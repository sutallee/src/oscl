/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_sema_binarysemah_
#define _oscl_krux_sema_binarysemah_
#include "sema.h"

/** */
namespace Oscl {
/** */
namespace Krux {
/** */
namespace Sema {

/** Represents a simple binary semaphore. This implementation
	allows a single waiter to be suspended while waiting
	for the semaphore to be set.
 */
class Binary : public Base {
	private:
		/** Flag that indicates the current state of the
			semaphore. The flag is true when the semaphore
			has been signaled and no waiter is pending
			on the semaphore.
		 */
		bool				signaled;

		/** Pointer to waiter that is currently blocked waiting
			on the semaphore, or zero otherwise.
		 */
		Waiter				*theWaiter;

	public:
		/** Constructor initializes the 'signaled' and 'waiter'
			member variables.
		 */
		Binary(void) noexcept;

		/** Called by the supervisor-mode trap handler to
			actually set the semphore.
		 */
		void suSignal(void) noexcept;	// Called from exception handler

		/** Called by the supervisor-mode trap handler to
			actually perform the wait on the semaphore.
			Called from exception handler.
		 */
		void suWait(Waiter& waiter) noexcept;
	};

}
}
}

#endif
