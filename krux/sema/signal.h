/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_sema_signalh_
#define _oscl_krux_sema_signalh_
#include "oscl/krux/port/trap.h"
#include "sema.h"

/** */
namespace Oscl {
/** */
namespace Krux {
/** */
namespace Sema {

/** Concrete trap that implements the setting
	of a semaphore as its service.
 */
class TrapSignal : public Trap {

	/** Reference to the semaphore to which the
		signal is directed.
	 */
	Base&		semaphore;

	public:

		/** Constructor initializes semaphore reference.
		 */
		TrapSignal(Base& sema) noexcept;

		/** Implements Trap service interface, performing
			the actual setting of the semaphore within the
			supervisor mode.
		 */
		void suService(void) noexcept;
	};

}
}
}

#endif
