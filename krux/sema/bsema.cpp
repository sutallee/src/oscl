/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "bsema.h"
#include "oscl/mt/mutex/iss.h"
#include "signal.h"
#include "oscl/krux/thread/sched.h"
#include "wait.h"
#include "oscl/krux/thread/twaiter.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Krux::Sema;

/*****************************************************************************
 * Binary()
 */

Binary::Binary(void) noexcept {
	signaled	= 0;
	theWaiter	= (Waiter *)0;
	}

/*****************************************************************************
 * suSignal()
 */

void Binary::suSignal(void) noexcept {
	Oscl::Mt::IntScopeSync	iss;

	if(theWaiter){
		theWaiter->suProceed();
		theWaiter	= 0;
		signaled	= 0;
		}
	else{
		signaled	= 1;
		}
	}

/*****************************************************************************
 * suWait()
 */

void Binary::suWait(Waiter& waiter) noexcept {
	if(theWaiter){ // only one waiter allowed at any given time
		Oscl::ErrorFatal::logAndExit("Oscl::Krux::Sema::Binary::suWait: too many waiters!\n\r");
		for(;;);
		}

	Oscl::Mt::IntScopeSync	iss;

	if(signaled){
		signaled	= 0;
		}
	else{
		theWaiter	= &waiter;
		waiter.suWait();
		}
	}

