/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_sema_waith_
#define _oscl_krux_sema_waith_
#include "oscl/krux/port/trap.h"
#include "sema.h"

/** */
namespace Oscl {
/** */
namespace Krux {
/** */
namespace Sema {

/** Concrete trap that implements waiting for
	a semaphore as its service.
 */
class TrapWait : public Trap{

	private:
		/** Reference to the semaphore on which to wait.
		 */
		Base& 	semaphore;

		/** Reference to the waiter to wait on the semaphore.
		 */
		Waiter&	theWaiter;

	public:

		/** Constructor initializes semaphore reference.
		 */
		TrapWait(Base& sema,Waiter& waiter) noexcept;

		/** Implements Trap service interface, performing
			the actual waiting for the semaphore within the
			supervisor mode.
		 */
		void suService(void) noexcept;
	};

}
}
}

#endif
