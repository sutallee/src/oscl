/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"

using namespace Oscl::BoolState;

void	Variable::Asserted::query(Query& query) const {
	query.asserted();
	}

void	Variable::Negated::query(Query& query) const {
	query.negated();
	}

Variable::Variable(bool asserted):
// This was the original... it "should" be fine, but
// now gives a constness warning.
//		_currentState((asserted?&_asserted:&_negated))
// This would be preferred if this were a simple
// problem with const'ness, however, it still fails.
//		_currentState(	(	asserted?
//								const_cast<Variable::State*>(&_asserted):
//								const_cast<Variable::State*>(&_negated)
//							)
//						)
		_currentState(	(	asserted?
								(Variable::State*)(&_asserted):
								(Variable::State*)(&_negated)
							)
						)
		// I added the casts when GCC-2.95 decided that
		// I was casting away constness
		{
	}

void	Variable::assert() noexcept {
	_currentState	= &_asserted;
	}

void	Variable::negate() noexcept {
	_currentState	= &_negated;
	}

bool	Variable::operator == (const Variable& other) const noexcept{
	return(_currentState == other._currentState);
	}

bool	Variable::operator != (const Variable& other) const noexcept{
	return(_currentState != other._currentState);
	}

Variable&	Variable::operator = (const Variable& other) noexcept{
	_currentState	= other._currentState;
	return *this;
	}

void	Variable::query(Query& query) const {
	_currentState->query(query);
	}

const Variable::Negated		Variable::_negated;

const Variable::Asserted	Variable::_asserted;
