/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_boolstate_node_parth_
#define _oscl_boolstate_node_parth_
#include "oscl/boolstate/dist/dist.h"
#include "oscl/boolstate/control/sync.h"

/** */
namespace Oscl {
/** */
namespace BoolState {
/** */
namespace Node {

/** */
class Part : private Oscl::BoolState::Control::Req::Api {
	private:
		/** */
		Distributer										_dist;

		/** */
		Oscl::BoolState::Control::Req::Api::ConcreteSAP	_controlSAP;

		/** */
		Oscl::BoolState::Control::Sync					_sync;

		/** */
		Oscl::BoolState::Req::Api::ConcreteSAP			_distSAP;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				bool						initialState
				) noexcept;

		/** */
		Oscl::BoolState::Req::Api::SAP&	getDistSAP() noexcept;

		/** */
		Oscl::BoolState::Control::Req::Api::SAP&	getControlSAP() noexcept;

		/** */
		Oscl::BoolState::Control::Api&	getControlSyncApi() noexcept;

	private: // Oscl::BoolState::Control::Req::Api
		/** */
		void	request(Oscl::BoolState::Control::Req::Api::AssertReq& msg) noexcept;
		/** */
		void	request(Oscl::BoolState::Control::Req::Api::NegateReq& msg) noexcept;
	};

}
}
}

#endif
