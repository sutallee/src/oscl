/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "dist.h"

using namespace Oscl::BoolState;

Distributer::Distributer(bool initialState) noexcept:
		_var(initialState),
		_lastChange(0)
		{
	}

void	Distributer::assert() noexcept{
	Variable	next(true);
	if(_var != next){
		_var	= next;
		notify();
		}
	}

void	Distributer::negate() noexcept{
	Variable	next(false);
	if(_var != next){
		_var	= next;
		notify();
		}
	}

void	Distributer::notify() noexcept{
	ChangeReq*	next;
	++_lastChange;
	while((next=_pending.get())){
		Oscl::BoolState::Variable&	v	= next->_payload;
		v	= _var;
		next->_payload._prevChange	= _lastChange;
		next->returnToSender();
		}
	}

void	Distributer::request(ChangeReq& msg) noexcept{
	if((_var != msg._payload) || (_lastChange != msg._payload._prevChange)){
		Oscl::BoolState::Variable&	v	= msg._payload;
		v							= _var;
		msg._payload._prevChange	= _lastChange;
		msg.returnToSender();
		return;
		}
	_pending.put(&msg);
	}

void	Distributer::request(CancelReq& msg) noexcept{
	ChangeReq*	next;
	for(next=_pending.first();next;next=_pending.next(next)){
		if(next == &msg._payload._reqToCancel){
			_pending.remove(next);	
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

