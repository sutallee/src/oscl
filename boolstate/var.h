/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_boolstate_boolstateh_
#define _oscl_boolstate_boolstateh_

/** */
namespace Oscl {
/** */
namespace BoolState {

/** This abstract class must be implemented by clients that
	wish to query the current state of a boolean state variable
	object. When the state is queried, the member function
	corresponding to the current state of the object is
	called.
 */
class  Query {
	public:
		/** Make GCC happy */
		virtual ~Query() {}

		/** This abstract member function is called when the state
			of the boolean state variable being queried is in the
			asserted state. Sub-classes implement this function to
			take action appropriate to their needs.
		 */
		virtual void	asserted() = 0;

		/** This abstract member function is called when the state
			of the boolean state variable being queried is in the
			negated state. Sub-classes implement this function to
			take action appropriate to their needs.
		 */
		virtual void	negated() = 0;
	};

/** This class represents the current state of a single boolean
	variable. Functions are provided to set the current state
	of the variable, as well as to query the current state of
	the variable.
 */
class Variable {
	private:	// Private implementation classes
		/** This abstract class is used to represent all of the possible
			states of a boolean state variable. All possible concrete
			boolean states are derived from this class.
		 */
		class State {
			public:
				/** Make GCC happy */
				virtual ~State() {}
				/** This abstract member function is used by clients that
					wish to take action based on the concrete state.
				 */
				virtual void	query(Query& query) const = 0;
			};

		/** This concrete class represents the asserted boolean state.
			It has no member variables, and thus is constant in its
			representation.
		 */
		class Asserted : public State {
			public:
				/** This member function realizes the corresponding abstract
					member function of the State base class by simply
					calling the Query::asserted() member function.
				 */
				void	query(Query& query) const;
			};

		/** This concrete class represents the negated boolean state.
			It has no member variables, and thus is constant in its
			representation.
		 */
		class Negated : public State {
			public:
				/** This member function realizes the corresponding abstract
					member function of the State base class by simply
					calling the Query::negated() member function.
				 */
				void	query(Query& query) const;
			};

	private:
		/** This variable holds a pointer to the current state
			of this class.
		 */
		const State*				_currentState;

	public:
		/** This class constant is used to set the current state
			to the negated state.
		 */
		static const Negated		_negated;

		/** This class constant is used to set the current state
			to the asserted state.
		 */
		static const Asserted		_asserted;

	public:
		/** This constructor takes a boolean indication for initialization.
			Due to a bug in the GNU compiler, which leaves the stack pointer
			referencing an odd memory location when the builtin bool type
			is used as an argument to a function, the initializer must be
			a non-zero integer representing a boolean value:-(
		 */
		Variable(bool asserted);
		/** */
		void	assert() noexcept;
		/** */
		void	negate() noexcept;
		/** */
		Variable&	operator = (const Variable& other) noexcept;
		/** */
		bool	operator == (const Variable& other) const noexcept;
		/** */
		bool	operator != (const Variable& other) const noexcept;
		/** */
		void	query(Query& query) const;
	};

}
}

#endif
