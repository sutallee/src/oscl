#ifndef _oscl_text_utf8_apih_
#define _oscl_text_utf8_apih_

/** */
namespace Oscl {
/** */
namespace Text {
/** */
namespace UTF8 {

/** */
class String {
	public:
		/** Be happy compiler
		 */
		virtual ~Api(){}
		/** */
		void	toUnicode(uint32_t*	string,unsigned maxLen) noexcept;
	};

}
}
}
