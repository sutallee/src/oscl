/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "unicode.h"

// Returns zero if an illegal sequence is detected, or a pointer
// to the start of the next utf8 sequence.
static const unsigned char*	decodeUTF8(	const unsigned char*	utf8Seq,
										uint32_t&				code
										) noexcept{
	unsigned char	firstOctet	= utf8Seq[0];
	unsigned char	firstHexCode	= (firstOctet >> 4) & 0x0F;

	uint32_t		v;

	if(firstHexCode < 0x08){
		// ASCII
		code	= utf8Seq[0];
		return &utf8Seq[1];
		}
	else if(firstHexCode < 0x0C){
		// Illegal initial code.
		return 0;
		}
	else if(firstHexCode < 0x0E){
		// 11-bit expressed in two octets

		if((utf8Seq[1] & 0xC0) != 0x80){
			// Illegal second octet
			return 0;
			}

		v	= (utf8Seq[0] & 0x1F);
		v	<<=	6;
		v	|=	(utf8Seq[1] & 0x3F);

		code	= v;

		if(v < 0x00000080){
			// Illegal encoding
			return 0;
			}

		if(v > 0x000007FF){
			// Illegal encoding
			return 0;
			}

		return &utf8Seq[2];
		}
	else if(firstHexCode < 0x0F){
		// 16-bits expressed in three octets

		if((utf8Seq[1] & 0xC0) != 0x80){
			// Illegal second octet
			return 0;
			}

		if((utf8Seq[2] & 0xC0) != 0x80){
			// Illegal third octet
			return 0;
			}

		v	= (utf8Seq[0] & 0x0F);
		v	<<=	6;
		v	|=	(utf8Seq[1] & 0x3F);
		v	<<=	6;
		v	|=	(utf8Seq[2] & 0x3F);

		code	= v;

		if(v < 0x00000800){
			// Illegal encoding
			return 0;
			}

		if(v > 0x0000FFFF){
			// Illegal encoding
			return 0;
			}

		return &utf8Seq[3];
		}
	else { // firstHexCode == 0x0F
		// 32-bits expressed as four octets

		if((utf8Seq[1] & 0xC0) != 0x80){
			// Illegal second octet
			return 0;
			}

		if((utf8Seq[2] & 0xC0) != 0x80){
			// Illegal third octet
			return 0;
			}

		if((utf8Seq[3] & 0xC0) != 0x80){
			// Illegal third octet
			return 0;
			}

		v	= (utf8Seq[0] & 0x07);
		v	<<=	6;
		v	|=	(utf8Seq[1] & 0x3F);
		v	<<=	6;
		v	|=	(utf8Seq[2] & 0x3F);
		v	<<=	6;
		v	|=	(utf8Seq[3] & 0x3F);

		code	= v;

		if(v < 0x00010000){
			// Illegal encoding
			return 0;
			}

		if(v > 0x00010FFFF){
			// Illegal encoding
			return 0;
			}

		return &utf8Seq[4];
		}
	}

namespace Oscl {
namespace Text {
namespace UTF8 {

void	utf8ToUnicode(
						const void*		utf8String,
						uint32_t*		unicodeStringArray,
						unsigned		unicodeStringArrayLen
						) noexcept{
	if(!unicodeStringArrayLen){
		return;
		}

	const unsigned maxNonNullChars	= unicodeStringArrayLen-1;

	for(unsigned i=0;i<maxNonNullChars;++i){
		utf8String	= decodeUTF8((const unsigned char*)utf8String,unicodeStringArray[i]);
		if(!utf8String){
			// Invalid encoding! Null terminate the array and get out.
			unicodeStringArray[i]	= 0;
			return;
			}
		if(!unicodeStringArray[i]){
			// Null terminator
			return;
			}
		}

	// We have filled the unicodeStringArray to capacity
	// without finding a null terminator,
	// so we need to terminate the array and run.
	unicodeStringArray[maxNonNullChars]	= 0;

	return;
	}

void	unicodeToUTF8(
			const uint32_t*	unicodeString,
			void*			utf8StringArray,
			unsigned		utf8StringArrayLen
			) noexcept{
	unsigned char*	utf8Array	= (unsigned char*)utf8StringArray;

	if(!utf8StringArrayLen){
		return;
		}

	const unsigned maxNonNullChars	= utf8StringArrayLen-1;

	for(unsigned i=0;i<maxNonNullChars;){
		if(!unicodeString[i]){
			utf8Array[i]	= 0;
			return;
			}

		if(unicodeString[i] < 0x00000080UL){
			utf8Array[i]	= (unsigned char)unicodeString[i];
			i				+= 1;
			}
		else if(unicodeString[i] < 0x00000800UL){
			if(i+1 < maxNonNullChars){
				// Insufficient space to encode
				// Null terminate and leave.
				utf8Array[i]	= 0;
				return;
				}
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>6) & 0x0000001F) | 0x000000C0);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]) & 0x0000003F) | 0x00000080);
			++i;
			}
		else if(unicodeString[i] < 0x00010000UL){
			if(i+2 < maxNonNullChars){
				// Insufficient space to encode
				// Null terminate and leave.
				utf8Array[i]	= 0;
				return;
				}
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>12) & 0x0000000F) | 0x000000E0);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>6) & 0x0000003F) | 0x00000080);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]) & 0x0000003F) | 0x00000080);
			++i;
			}
		else if(unicodeString[i] < 0x00110000UL){
			if(i+3 < maxNonNullChars){
				// Insufficient space to encode
				// Null terminate and leave.
				utf8Array[i]	= 0;
				return;
				}
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>18) & 0x00000007) | 0x000000F0);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>12) & 0x0000000F) | 0x000000E0);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]>>6) & 0x0000003F) | 0x00000080);
			++i;
			utf8Array[i]	= (unsigned char)(((unicodeString[i]) & 0x0000003F) | 0x00000080);
			++i;
			}
		else {
			// Illegal code!
			return;
			}
		}

	}
}
}
}


