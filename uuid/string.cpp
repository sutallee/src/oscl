/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include "oscl/uuid/string.h"

/** RETURN: true for failure.
 */
bool	Oscl::UUID::String::stringToUUID(
			uint8_t		uuid[16],
			const char*	uuidString
			) noexcept{

#ifdef CONFIG_NEWLIB_LIBC_NANO
	// Zephyr OS may be configured with newlib-nano,
	// which does NOT support C99 scanf format specifiers
	// and 'hh' in particular.
	unsigned short	u[16];

	int
	nMatched	= sscanf(
					uuidString,
					"%02hx" "%02hx" "%02hx" "%02hx"
					"-"
					"%02hx" "%02hx"
					"-"
					"%02hx" "%02hx"
					"-"
					"%02hx" "%02hx"
					"-"
					"%02hx" "%02hx" "%02hx" "%02hx" "%02hx" "%02hx"
					"",
					&u[0],
					&u[1],
					&u[2],
					&u[3],
					&u[4],
					&u[5],
					&u[6],
					&u[7],
					&u[8],
					&u[9],
					&u[10],
					&u[11],
					&u[12],
					&u[13],
					&u[14],
					&u[15]
					);

	for(unsigned i=0;i<16;++i){
		uuid[i]	= (uint8_t)u[i];
		}
#else
	int
	nMatched	= sscanf(
					uuidString,
					"%02hhx" "%02hhx" "%02hhx" "%02hhx"
					"-"
					"%02hhx" "%02hhx"
					"-"
					"%02hhx" "%02hhx"
					"-"
					"%02hhx" "%02hhx"
					"-"
					"%02hhx" "%02hhx" "%02hhx" "%02hhx" "%02hhx" "%02hhx"
					"",
					&uuid[0],
					&uuid[1],
					&uuid[2],
					&uuid[3],
					&uuid[4],
					&uuid[5],
					&uuid[6],
					&uuid[7],
					&uuid[8],
					&uuid[9],
					&uuid[10],
					&uuid[11],
					&uuid[12],
					&uuid[13],
					&uuid[14],
					&uuid[15]
					);
#endif

	return (nMatched < 16)?true:false;
	}

bool	Oscl::UUID::String::uuidToString(
			const uint8_t	uuid[16],
			char*			uuidString,
			unsigned		uuidStringLen
			) noexcept{
	if(uuidStringLen < 1){
		return true;
		}

	int
	n	= snprintf(
			uuidString,
			uuidStringLen,
			"%2.2X%2.2X%2.2X%2.2X"
			"-"
			"%2.2X%2.2X"
			"-"
			"%2.2X%2.2X"
			"-"
			"%2.2X%2.2X"
			"-"
			"%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X"
			"",
			uuid[0],
			uuid[1],
			uuid[2],
			uuid[3],
			uuid[4],
			uuid[5],
			uuid[6],
			uuid[7],
			uuid[8],
			uuid[9],
			uuid[10],
			uuid[11],
			uuid[12],
			uuid[13],
			uuid[14],
			uuid[15]
			);

	if(n >= (int)uuidStringLen){
		uuidString[uuidStringLen-1]	= '\0';
		return true;
		}

	return false;
	}
