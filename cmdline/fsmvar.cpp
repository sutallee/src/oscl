/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsmvar.h"

using namespace Oscl;

const CmdLineEditorStateVar::StateBufferWait	CmdLineEditorStateVar::_bufferWaitState;
const CmdLineEditorStateVar::StateEmpty			CmdLineEditorStateVar::_emptyState;
const CmdLineEditorStateVar::StateSomeData		CmdLineEditorStateVar::_someDataState;
const CmdLineEditorStateVar::StateFull			CmdLineEditorStateVar::_fullState;

bool		CmdLineEditorStateVar::ContextApi::isDelete(char data) const noexcept{
	return ((data == 0x7F) || (data == 0x08));
	}

bool		CmdLineEditorStateVar::ContextApi::isLineTerminator(char data) const noexcept{
	return ((data == '\n') || (data == '\r'));
	}

bool		CmdLineEditorStateVar::ContextApi::isLineKill(char data) const noexcept{
	return (data == 0x15);
	}

void	CmdLineEditorStateVar::StateBufferWait::put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept{
	context.beep();
	}

void	CmdLineEditorStateVar::StateEmpty::put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept{
	if(context.isDelete(data)){
		context.beep();
		}
	else if(context.isLineTerminator(data)){
		context.done();
		var.transitionToEmpty();
		}
	else if(context.isLineKill(data)){
		// no operation
		}
	else{
		if(context.append(data)){
			var.transitionToSomeData();
			context.echo(data);
			}
		else{
			context.beep();
			}
		}
	}

void	CmdLineEditorStateVar::StateSomeData::put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept{
	if(context.isDelete(data)){
		context.erase();
		}
	else if(context.isLineTerminator(data)){
		context.done();
		var.transitionToEmpty();
		}
	else if(context.isLineKill(data)){
		context.lineKill();
		var.transitionToEmpty();
		}
	else{
		if(!context.append(data)){
			context.beep();
			var.transitionToFull();
			}
		else{
			context.echo(data);
			}
		}
	}

void	CmdLineEditorStateVar::StateFull::put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept{
	if(context.isDelete(data)){
		context.erase();
		var.transitionToSomeData();
		}
	else if(context.isLineTerminator(data)){
		context.done();
		var.transitionToEmpty();
		}
	else if(context.isLineKill(data)){
		context.lineKill();
		var.transitionToEmpty();
		}
	else{
		context.beep();
		}
	}

CmdLineEditorStateVar::CmdLineEditorStateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&_emptyState)
		{
	}

void	CmdLineEditorStateVar::put(char data) noexcept{
	_state->put(*this,_context,data);
	}

void		CmdLineEditorStateVar::transitionToBufferWait() noexcept{
	_state	= &_bufferWaitState;
	}

void		CmdLineEditorStateVar::transitionToEmpty() noexcept{
	_state	= &_emptyState;
	}

void		CmdLineEditorStateVar::transitionToSomeData() noexcept{
	_state	= &_someDataState;
	}

void		CmdLineEditorStateVar::transitionToFull() noexcept{
	_state	= &_fullState;
	}

