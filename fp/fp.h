/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fp_fp16h_
#define _oscl_fp_fp16h_

/** */
namespace Oscl {

/** */
namespace FP {

/** */
class Frac16 {
	private:
		/** */
		int16_t	_value;
	public:
		inline Frac16(float value) noexcept{
			_value	= (int16_t)(value * 256);
			}

		inline Frac16(const Frac16& value) noexcept{
			_value	= value._value;
			}

		inline Frac16& operator *=(const Frac16& value) noexcept{
			int32_t	temp	= value._value;
			temp	*= _value;
			temp	>>= 16;
			_value	= (int16_t)temp;
			return *this;
			}

		inline Frac16& operator /=(const Frac16& value) noexcept{
			int32_t	dividend	= _value;
			int32_t	divisor		= value._value;
			dividend	<<= 16;
			dividend	/= divisor;
			_value	= (int16_t)dividend;
			return *this;
			}

		inline Frac16& operator +=(const Frac16& value) noexcept{
			_value	+= value._value;
			return *this;
			}

		inline Frac16& operator -=(const Frac16& value) noexcept{
			_value	-= value._value;
			return *this;
			}

		inline Frac16& operator *=(uint8_t value) noexcept{
			_value	*= value;
			return *this;
			}

		inline operator float() const noexcept{
			float	value	= _value;
			return (value / 256);
			}

		inline int16_t	raw() const noexcept{
			return _value;
			}
	};

}
}

#endif
