#ifndef _oscl_middleman_apph_
#define _oscl_middleman_apph_

#include "oscl/posix/select/api.h"
#include "oscl/posix/select/handler.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/runnable.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {

/** */
namespace MiddleMan {

/** */
class App : public Oscl::Mt::Runnable {
	private:
		/** */
		class Tap;

		/** */
		class TapOutputApi;

		/** */
		class TapSelect : public Oscl::Posix::Select::Handler {
			private:
				/** */
				Tap&				_context;

				/** */
				const int			_fd;

			public:
				/** */
				TapSelect(
					Tap&								context,
					Oscl::Posix::Select::FileDescSets&	fdSet,
					int									fd
					) noexcept;

			private: // Posix::Select::Handler
				/**	This  operation is invoked to allow the implementation to
					add its file descriptors to the appropriates file descriptor
					sets. This operation is only invoked when the operation
					is established.
				 */
				void	start(Oscl::Posix::Select::FileDescSets& next) noexcept;

				/**	When this  operation is invoked the implementation must
					clear the file descriptor from the next sets. This is
					usually done when this IO handler is removed from the
					select handler.
				 */
				void	stop(Oscl::Posix::Select::FileDescSets& next) noexcept;

				/** */
				bool	handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
									Oscl::Posix::Select::FileDescSets&			next,
									time_t&										timestamp
									) noexcept;
				/** */
				bool	fdIsClosed() noexcept;
			};

		/** */
		class TapOutputApi : public Oscl::QueueItem {
			public:
				/** */
				virtual ~TapOutputApi() {}

				/** */
				virtual void	write(	const void*		buffer,
										unsigned		len
										) noexcept = 0;
			};

		/** */
		class TapOut : public TapOutputApi {
			private:
				/** */
				const int		_fd;

			public:
				/** */
				TapOut( int		fd) noexcept;

			public:	// TapOutputApi
				/** */
				void	write(	const void*		buffer,
								unsigned		len
								) noexcept;
			};

		/** */
		class Tap {
			private:
				/** */
				typedef void	(App::* CloseFunc)(Tap& tap);

				/** */
				typedef void	(App::* WriteFunc)(const void* buffer, unsigned len);

			private:
				/** */
				App&		_context;

				/** */
				TapOut		_tapOut;

				/** */
				TapSelect	_tapSelect;

				/** */
				WriteFunc	_writeFunc;

				/** */
				CloseFunc	_closeFunc;

				/** */
				friend class TapSelect;

			public:
				/** */
				Tap(
					App&								context,
					Oscl::Posix::Select::FileDescSets&	fdSet,
					WriteFunc							writeFunc,
					CloseFunc							closeFunc,
					int									fd
					) noexcept;

				/** */
				Oscl::Posix::Select::Handler&	getSelectHandlerApi() noexcept;

				/** */
				TapOutputApi&	getTapOutputApi() noexcept;

			private:
				/** */
				void	close() noexcept;

				/** */
				void	write( const void* buffer, unsigned len) noexcept;

			};

		/***/
		union TapMem {
			/***/
			void*			__qitemlink;
			/***/
			Oscl::Memory::AlignedBlock<sizeof(Tap)>	_tap;
			};

		/** */
		class TapSocketListener : public Oscl::Posix::Select::Handler {
			private:
				/** */
				typedef void	(App::* Func)(TapMem& mem,int fd);

			private:
				/** */
				App&				_context;

				/** */
				Func					_startFunc;

				/** */
				const int				_fd;

			public:
				/** */
				TapSocketListener(
					App&								context,
					Func								startFunc,
					Oscl::Posix::Select::FileDescSets&	fdSet,
					int									fd
					) noexcept;

			private: // Posix::Select::Handler
				/**	This  operation is invoked to allow the implementation to
					add its file descriptors to the appropriates file descriptor
					sets. This operation is only invoked when the operation
					is established.
				 */
				void	start(Oscl::Posix::Select::FileDescSets& next) noexcept;

				/**	When this  operation is invoked the implementation must
					clear the file descriptor from the next sets. This is
					usually done when this IO handler is removed from the
					select handler.
				 */
				void	stop(Oscl::Posix::Select::FileDescSets& next) noexcept;

				/** */
				bool	handleIO(	const Oscl::Posix::Select::FileDescSets&	pending,
									Oscl::Posix::Select::FileDescSets&			next,
									time_t&										timestamp
									) noexcept;
				/** */
				bool	fdIsClosed() noexcept;
			};

		/** */
		friend class TapSocketListener;

	private:
		/** */
		Oscl::Queue<TapOutputApi>				_eastTap;

		/** */
		Oscl::Queue<TapOutputApi>				_westTap;

		/** */
		Oscl::Queue<Oscl::Posix::Select::Handler>	_iohandlers;

		/** */
		enum { maxTaps = 4 };

		/** */
		TapMem									_tapMem[maxTaps];

		/** */
		Oscl::Queue<TapMem>						_freeTapMem;

		/** */
		Oscl::Posix::Select::FileDescSets		_activeFdSet;

		/** */
		TapSocketListener						_easternListener;

		/** */
		TapSocketListener						_westernListener;

		/** */
		Tap										_easternEnd;

		/** */
		Tap										_westernEnd;

		/** */
		const int								_eastSocketFD;

		/** */
		const int								_westSocketFD;

		/** */
		const int								_eastFD;

		/** */
		const int								_westFD;

		/** */
		bool									_ioHandlerRemoved;

	public:
		/** */
		App(
			int					eastSocketFD,
			int					westSocketFD,
			int					eastFD,
			int					westFD
			) noexcept;

	private:
		/** */
		void	writeEastEnd(const void* buffer, unsigned len) noexcept;

		/** */
		void	writeWestEnd(const void* buffer, unsigned len) noexcept;

		/** */
		void	copyToEastEndTaps(const void* buffer, unsigned len) noexcept;

		/** */
		void	copyToWestEndTaps(const void* buffer, unsigned len) noexcept;

		/** */
		void	newEasternTap(TapMem& mem,int fd) noexcept;

		/** */
		void	newWesternTap(TapMem& mem,int fd) noexcept;

		/** */
		TapMem*	allocTapMem() noexcept;

		/** */
		void	free(Tap& tap) noexcept;

		/** */
		void	removeIoHandler(Posix::Select::Handler& handler) noexcept;

		/** */
		void	closeEastEnd(Tap& tap) noexcept;

		/** */
		void	closeWestEnd(Tap& tap) noexcept;

		/** */
		void	closeEastTap(Tap& tap) noexcept;

		/** */
		void	closeWestTap(Tap& tap) noexcept;

	public:	// Oscl::Mt::Runnable
		/** */
		void	run() noexcept;
	};

}
}

#endif
