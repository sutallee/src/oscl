#include "app.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <new>
#include <errno.h>
#include <time.h>
#include <stdio.h>

using namespace Oscl::MiddleMan;

App::TapSelect::TapSelect(
			App::Tap&							context,
			Oscl::Posix::Select::FileDescSets&	fdSet,
			int									fd
			) noexcept:
		_context(context),
		_fd(fd)
		{
	}

void	App::TapSelect::start(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_SET(_fd,&next._readfds);
	}

void	App::TapSelect::stop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_CLR(_fd,&next._readfds);
	}

bool	App::TapSelect::handleIO(
			const Oscl::Posix::Select::FileDescSets&	pending,
			Oscl::Posix::Select::FileDescSets&			next,
			time_t&										timestamp
			) noexcept {
	if(!FD_ISSET(_fd,&pending._readfds)) {
		return true;
		}

	char	buffer[4096];

	ssize_t	n	= read(_fd,buffer,sizeof(buffer));

	if(n < 1) {
		FD_CLR(_fd,&next._readfds);
		_context.close();
		return false;
		}

	_context.write(buffer,n);

	return true;
	}

bool	App::TapSelect::fdIsClosed() noexcept{
	// FIXME:
	return false;
	}

App::TapOut::TapOut(int fd) noexcept:
	_fd(fd) {
	}

void	App::TapOut::write(
			const void*		buffer,
			unsigned		len
			) noexcept {
	::write(	_fd,
				buffer,
				len
				);
	}

App::Tap::Tap(	App&								context,
				Oscl::Posix::Select::FileDescSets&	fdSet,
				WriteFunc							writeFunc,
				CloseFunc							closeFunc,
				int									fd
				) noexcept:
		_context(context),
		_tapOut(fd),
		_tapSelect(
			*this,
			fdSet,
			fd
			),
		_writeFunc(writeFunc),
		_closeFunc(closeFunc)
		{
	}

Oscl::Posix::Select::Handler&	App::Tap::getSelectHandlerApi() noexcept {
	return _tapSelect;
	}

App::TapOutputApi&	App::Tap::getTapOutputApi() noexcept {
	return _tapOut;
	}

void	App::Tap::close() noexcept {
	(_context.*_closeFunc)(*this);
	}

void	App::Tap::write( const void* buffer, unsigned len) noexcept {
	(_context.*_writeFunc)(buffer,len);
	}

App::TapSocketListener::TapSocketListener(
			App&								context,
			Func								startFunc,
			Oscl::Posix::Select::FileDescSets&	fdSet,
			int									fd
			) noexcept:
		_context(context),
		_startFunc(startFunc),
		_fd(fd)
		{
	FD_SET(_fd,&fdSet._readfds);
	}

void	App::TapSocketListener::start(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_SET(_fd,&next._readfds);
	}

void	App::TapSocketListener::stop(Oscl::Posix::Select::FileDescSets& next) noexcept{
	FD_CLR(_fd,&next._readfds);
	}

bool	App::TapSocketListener::handleIO(
			const Oscl::Posix::Select::FileDescSets&	pending,
			Oscl::Posix::Select::FileDescSets&		next,
			time_t&						timestamp
			) noexcept {
	if(!FD_ISSET(_fd,&pending._readfds)) {
		return true;
		}

	struct sockaddr		clientAddress;
	socklen_t			len	= sizeof(clientAddress);

	int	clientFD	= accept(	_fd,
								(struct sockaddr*)&clientAddress,
								&len
								);

	TapMem*	mem	= _context.allocTapMem();
	if(!mem) {
		close(clientFD);
		return false;
		}

	(_context.*_startFunc)(*mem,clientFD);

	return true;
	}

bool	App::TapSocketListener::fdIsClosed() noexcept{
	// FIXME:
	return false;
	}

App::App(	int					eastSocketFD,
			int					westSocketFD,
			int					eastFD,
			int					westFD
			) noexcept:
		_eastTap(),
		_westTap(),
		_iohandlers(),
		_freeTapMem(),
		_activeFdSet(),
		_easternListener(
			*this,
			&App::newEasternTap,
			_activeFdSet,
			eastSocketFD
			),
		_westernListener(
			*this,
			&App::newWesternTap,
			_activeFdSet,
			westSocketFD
			),
		_easternEnd(
			*this,
			_activeFdSet,
			&App::copyToEastEndTaps,
			&App::closeEastEnd,
			eastFD
			),
		_westernEnd(
			*this,
			_activeFdSet,
			&App::copyToWestEndTaps,
			&App::closeWestEnd,
			westFD
			),
		_eastSocketFD(eastSocketFD),
		_westSocketFD(westSocketFD),
		_eastFD(eastFD),
		_westFD(westFD),
		_ioHandlerRemoved(false)
		{
	for(unsigned i=0; i < maxTaps ; ++i) {
		_freeTapMem.put(&_tapMem[i]);
		}
	}

void	App::writeEastEnd(const void* buffer, unsigned len) noexcept {
	ssize_t	n	= ::write(	_eastFD,
							buffer,
							len
							);
	if( (n < 0) || (((unsigned)n) != len)) {
		printf("East end write failed\n");
		}
	}

void	App::writeWestEnd(const void* buffer, unsigned len) noexcept {
	ssize_t	n	= ::write(	_westFD,
							buffer,
							len
							);
	if( (n < 0) || (((unsigned)n) != len)) {
		printf("West end write failed\n");
		}
	}

void	App::copyToEastEndTaps(const void* buffer, unsigned len) noexcept {
	TapOutputApi*	p;
	for(	p	= _eastTap.first();
			p ;
			p	= _eastTap.next(p)
			) {
		p->write(buffer,len);
		}
	}

void	App::copyToWestEndTaps(const void* buffer, unsigned len) noexcept {
	TapOutputApi*	p;
	for(	p	= _westTap.first();
			p ;
			p	= _westTap.next(p)
			) {
		p->write(buffer,len);
		}
	}

void	App::newEasternTap(TapMem& mem,int fd) noexcept {
	App::Tap*	tap	= new App::Tap(	*this,
									_activeFdSet,
									&App::writeEastEnd,
									&App::closeEastTap,
									fd
									);
	_eastTap.put(&tap->getTapOutputApi());
	_iohandlers.put(&tap->getSelectHandlerApi());
	}

void	App::newWesternTap(TapMem& mem,int fd) noexcept {
	App::Tap*	tap	= new App::Tap(	*this,
									_activeFdSet,
									&App::writeWestEnd,
									&App::closeWestTap,
									fd
									);
	_westTap.put(&tap->getTapOutputApi());
	_iohandlers.put(&tap->getSelectHandlerApi());
	}

App::TapMem*	App::allocTapMem() noexcept {
	return _freeTapMem.get();
	}

void	App::free(Tap& tap) noexcept {
	tap.~Tap();
	_freeTapMem.put((TapMem*)&tap);
	}

void	App::removeIoHandler(Oscl::Posix::Select::Handler& handler) noexcept {
	_iohandlers.remove(&handler);
	_ioHandlerRemoved	= true;
	}

void	App::closeEastEnd(Tap& tap) noexcept {
	removeIoHandler(tap.getSelectHandlerApi());
	tap.~Tap();
	exit(0);
	}

void	App::closeWestEnd(Tap& tap) noexcept {
	removeIoHandler(tap.getSelectHandlerApi());
	tap.~Tap();
	exit(0);
	}

void	App::closeEastTap(Tap& tap) noexcept {
	removeIoHandler(tap.getSelectHandlerApi());
	_eastTap.remove(&tap.getTapOutputApi());
	free(tap);
	}

void	App::closeWestTap(Tap& tap) noexcept {
	removeIoHandler(tap.getSelectHandlerApi());
	_westTap.remove(&tap.getTapOutputApi());
	free(tap);
	}

void	App::run() noexcept {
	// Put the I/O handlers in the queue.
	_iohandlers.put(&_easternListener);
	_iohandlers.put(&_westernListener);
	_iohandlers.put(&_easternEnd.getSelectHandlerApi());
	_iohandlers.put(&_westernEnd.getSelectHandlerApi());

	_westTap.put(&_easternEnd.getTapOutputApi());
	_eastTap.put(&_westernEnd.getTapOutputApi());

	// This loop waits for file descriptor changes
	// and dispatches changes to the appropriate handler.
	for(;;) {
		// Holds the result of system calls.
		int	result;

		// Copy the file read file descriptor set.
		Oscl::Posix::Select::FileDescSets	pend(_activeFdSet);

		// Wait for events.
		result	= select(	FD_SETSIZE,
							&pend._readfds,
							&pend._writefds,
							&pend._exceptfds,
							(struct timeval*)0
							);
		if(result < 0) {
			if(errno == EINTR) {
				// This thread got a signal, so we
				// just do the select again.
				continue;
				}
			// Either we did something wrong, or the
			// application has been killed (e.g. with a Ctrl-C.)
			printf("select failed\n");
			break;
			}

		// At this point, select is indicating that one
		// of our I/O handler file descripitors has pending
		// I/O.

		// Create a timestamp.
		time_t	timestamp	= time(0);

		// Sequentially call all I/O handlers
		for(	Oscl::Posix::Select::Handler*	next	= _iohandlers.first();
				next ;
				next	= _iohandlers.next(next)
				) {
			// Give this I/O handler a chance to service
			// his file descriptor as required.
			next->handleIO(	pend,
							_activeFdSet,
							timestamp
							);
			if(_ioHandlerRemoved) {
				_ioHandlerRemoved	= false;
				break;
				}
			}
		}
	}

