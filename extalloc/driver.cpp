/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/error/fatal.h"

static bool
rangeFitsBefore(	const Oscl::ExtAlloc::Record&	rec,
					unsigned long					firstReqUnit,
					unsigned long					len,
					unsigned long					blockMask
					) noexcept{
	unsigned long	firstUnit		= rec.getFirstUnit();
	unsigned long	lastReqUnit		= firstReqUnit + len - 1;
	if(firstReqUnit < firstUnit){
		if(lastReqUnit >= firstUnit){
			return false;
			}
		}
	return false;
	}

Oscl::ExtAlloc::Driver::Driver(	unsigned long	firstUnit,
								unsigned long	nUnits,
								unsigned long	alignmentMask
								) noexcept:
		_firstUnit((firstUnit+alignmentMask)&(~alignmentMask)),
		_nUnits(nUnits-(firstUnit&alignmentMask)),
		_lastUnit(_firstUnit+_nUnits-1),
		_alignmentMask(alignmentMask)
		{
	}

Oscl::ExtAlloc::Driver::~Driver(){
	Oscl::ExtAlloc::Record*	next;
	for(next=_allocatedList.first();next;next=_allocatedList.next(next)){
		next->release();
		}
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Driver::alloc(	Oscl::ExtAlloc::FreeApi&	freeApi,
								Oscl::ExtAlloc::Record&		reqRec,
								unsigned long				nUnits,
								unsigned long				blockMask
								) noexcept{
	Oscl::ExtAlloc::Record*	next;
	// Ensure that the number of units actually allocated
	// is aligned according to the alignment requirements
	// of the allocator. This means the allocated block
	// size may be padded at the end here.
	nUnits	= ((nUnits+_alignmentMask)&(~_alignmentMask));

	// Calculate the first unit address according to the
	// blockMask that specifies additional alignment constraints
	// required by the client.
	unsigned long		reqUnit = (_firstUnit+(~blockMask)) & blockMask;

	// Search for the first fit free block.
	for(next=_allocatedList.first();next;next=_allocatedList.next(next)){
		if(rangeFitsBefore(*next,reqUnit,nUnits,blockMask)){
			reqRec.setRange(freeApi,reqUnit,nUnits);
			_allocatedList.insertBefore(next,&reqRec);
			return &reqRec;
			}
		// Re-calculate the first unit address according to the
		// blockMask that specifies additional alignment constraints
		// required by the client.
		reqUnit	=	((next->getFirstUnit() +	next->getNumberContiguousUnits())+~blockMask)&blockMask;
		}

	if(		(reqUnit <= _lastUnit)
		&&	((reqUnit+nUnits) < _lastUnit)
		){
		reqRec.setRange(freeApi,reqUnit,nUnits);
		_allocatedList.put(&reqRec);
		return &reqRec;
		}

	// Cannot allocate the memory.
	return (const Oscl::ExtAlloc::Record*)0;
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Driver::alloc(	Oscl::ExtAlloc::Record&	reqRec,
								unsigned long			nUnits
								) noexcept{
	return alloc(*this,reqRec,nUnits,~0);
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Driver::alloc(	Oscl::ExtAlloc::Record&	reqRec,
								unsigned long			nUnits,
								unsigned long			blockMask
								) noexcept{
	return alloc(*this,reqRec,nUnits,blockMask);
	}

void	Oscl::ExtAlloc::Driver::free(Oscl::ExtAlloc::Record& reqRec) noexcept{
	if(_allocatedList.remove(&reqRec)){
		reqRec.release();
		}
	else{
		Oscl::ErrorFatal::logAndExit("Oscl::ExtAlloc::Driver::free\n");
		}
	}

unsigned long	Oscl::ExtAlloc::Driver::baseUnit() const noexcept{
	return _firstUnit;
	}

