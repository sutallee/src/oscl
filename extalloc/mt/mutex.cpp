/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "mutex.h"
#include "oscl/mt/scopesync.h"

Oscl::ExtAlloc::Mutex::
Mutex(	Oscl::ExtAlloc::Api&			decorated,
		Oscl::Mt::Mutex::Simple::Api&	mutex
		) noexcept:
		Oscl::ExtAlloc::Decorator(decorated),
		_mutex(mutex)
		{
	}

Oscl::ExtAlloc::Mutex::~Mutex(){
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Mutex::alloc(	Oscl::ExtAlloc::Record&	reqRec,
								unsigned long			nUnits
								) noexcept{
	Oscl::Mt::ScopeSync	ss(_mutex);
	return _decorated.alloc(*this,reqRec,nUnits,~0);
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Mutex::alloc(	Oscl::ExtAlloc::Record&	reqRec,
								unsigned long			nUnits,
								unsigned long			blockMask
								) noexcept{
	Oscl::Mt::ScopeSync	ss(_mutex);
	return _decorated.alloc(*this,reqRec,nUnits,blockMask);
	}

void
Oscl::ExtAlloc::Mutex::free(Oscl::ExtAlloc::Record& reqRec) noexcept{
	Oscl::Mt::ScopeSync	ss(_mutex);
	_decorated.free(reqRec);
	}

unsigned long	Oscl::ExtAlloc::Mutex::baseUnit() const noexcept{
	return _decorated.baseUnit();
	}

const Oscl::ExtAlloc::Record*
Oscl::ExtAlloc::Mutex::alloc(	Oscl::ExtAlloc::FreeApi&	freeApi,
								Oscl::ExtAlloc::Record&		reqRec,
								unsigned long				nUnits,
								unsigned long				blockMask
								) noexcept{
	Oscl::Mt::ScopeSync	ss(_mutex);
	return _decorated.alloc(freeApi,reqRec,nUnits,blockMask);
	}

