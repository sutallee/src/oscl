/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_extalloc_mt_mutexh_
#define _oscl_extalloc_mt_mutexh_
#include "oscl/extalloc/decorator.h"
#include "oscl/mt/mutex/simple.h"

/** */
namespace Oscl {

/** */
namespace ExtAlloc {

/** */
class Mutex : public Oscl::ExtAlloc::Decorator {
	private:
		/** */
		Oscl::Mt::Mutex::Simple::Api&	_mutex;

	public:
		/** */
		Mutex(	Oscl::ExtAlloc::Api&			decorated,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

		/** */
		virtual ~Mutex();

	public:
		/** */
		const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::Record& reqRec,
					unsigned long			nUnits
					) noexcept;

		/** */
		const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::Record&	reqRec,
					unsigned long			nUnits,
					unsigned long			blockMask
					) noexcept;

		/** */
		void	free(Oscl::ExtAlloc::Record& reqRec) noexcept;

		/** */
		unsigned long	baseUnit() const noexcept;

	protected:
		/** */
		const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::FreeApi&	freeApi,
					Oscl::ExtAlloc::Record&		reqRec,
					unsigned long				nUnits,
					unsigned long				blockMask
					) noexcept;
	};

}
}

#endif
