/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_extalloc_apih_
#define _oscl_extalloc_apih_
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {

/** */
namespace ExtAlloc {

class Record;

/** */
class FreeApi {
	public:
		/** Shut-up GCC. */
		virtual ~FreeApi() {}
		/** */
		virtual void	free(Oscl::ExtAlloc::Record& reqRec) noexcept=0;
	};

/** */
class Api : public Oscl::ExtAlloc::FreeApi {
	public:
		/** */
		virtual const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::Record& reqRec,
					unsigned long			nUnits
					) noexcept=0;

		/**
			The "blockMask" represents the size and alignment
			of each "unit" as a bit mask with the least significant
			bits set. Thus alignment can only be on power-of-two
			boundaries.
		 */
		virtual const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::Record&	reqRec,
					unsigned long			nUnits,
					unsigned long			blockMask
					) noexcept=0;

		/**
			The "blockMask" represents the size and alignment
			of each "unit" as a bit mask with the least significant
			bits set. Thus alignment can only be on power-of-two
			boundaries.
		 */
		virtual const Oscl::ExtAlloc::Record*
			alloc(	Oscl::ExtAlloc::FreeApi&	freeApi,
					Oscl::ExtAlloc::Record&		reqRec,
					unsigned long				nUnits,
					unsigned long				blockMask
					) noexcept=0;

		/** This operation returns the index of the lowest
			number unit availbe for allocation. This is
			the equivalent of the base address of a contiguous
			block of memory.
		 */
		virtual unsigned long	baseUnit() const noexcept=0;

	};

/** This class has unusual behavior. The design is such that
	their may only be one copy of each record. The last copy
	maintains the responsibility to release the allocated
	resource.
 */
class Record : public Oscl::QueueItem {
	private:
		/** */
		unsigned long				_firstUnit;

		/** */
		unsigned long				_nUnits;

		/** */
		Oscl::ExtAlloc::FreeApi*	_freeApi;

	private:
		/** */
		Record(const Record&);
		/** */
		Record&	operator=(const Record& other);

	public:
		/** */
		Record() noexcept:_freeApi(0){};

		/** The behavior of this operation is to transfer
			the responsibility of the freeing operation
			to the copy.
		 */
		Record(Record& other) noexcept:
			_firstUnit(other._firstUnit),
			_nUnits(other._nUnits),
			_freeApi(other._freeApi)
				{
			other._freeApi	= 0;
			};

		/** The behavior of this operation is to transfer
			the responsibility of the freeing operation
			to the copy.
		 */
		Record&	operator=(Record& other) noexcept{
			_firstUnit	= other._firstUnit;
			_nUnits		= other._nUnits;
			_freeApi	= other._freeApi;
			other._freeApi	= 0;
			return *this;
			}

		/** */
		~Record(){if(_freeApi) _freeApi->free(*this);}
		
		/** */
		inline unsigned long	getFirstUnit() const  noexcept{
			return _firstUnit;
			}

		/** */
		inline unsigned long	getNumberContiguousUnits() const  noexcept{
			return _nUnits;
			}

	public:
		/** */
		inline void		setRange(	Oscl::ExtAlloc::FreeApi&	freeApi,
									unsigned long				firstUnit,
									unsigned long				nUnits
									) noexcept{
			_freeApi	= &freeApi;
			_firstUnit	= firstUnit;
			_nUnits		= nUnits;
			}

		/** */
		inline void		release() noexcept{_freeApi=0;}
	};

};
};

#endif

