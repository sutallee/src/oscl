/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_shell_line_cmdapih_
#define _oscl_shell_line_cmdapih_
#include "state.h"
#include "input.h"
#include "oscl/queue/queueitem.h"
#include "oscl/stream/output/api.h"

/** */
namespace Oscl {
/** */
namespace Shell {
/** */
namespace Line {

/** This interface describes a single command that
	may be registered with the line oriented shell.
	Each command sub-class of this API is *must*
	be implemented as a Fly-Weight. That is to say
	that it is stateless and re-entrant.
	The execute() operation provides a generic
	"state" object that may be used by the
	implementation to store its state if the command
	requires multiple lines of input.
 */
class CmdApi :	public	Oscl::QueueItem {
	public:
		/** Make GCC happy */
		virtual ~CmdApi() {}

		/** This operation returns the name of the command.
		 */
		virtual const char*	command() const noexcept=0;

		/**	This operation is called by the shell to have
			the syntax of the command displayed on "output".
		 */
		virtual void	help(Oscl::Stream::Output::Api&	output) const noexcept=0;

		/** This operation is called by the shell to execute the
			command. This operation is only invoked if the command()
			operation returns a string that matches the command
			received by the shell. The command itself is stripped
			from the input line and the remaining command line is
			in "arguments".

			The "state" has 2 functions:
			1. It contains a reference to the output stream to which
				command results must be sent.
			2. It may be used by the command to hold state information
				when the command is interactive.

			Interactive commands return false and store state information
			in state.stateMemory, which is preserved and passed in
			subsequent calls to the input() operation.

			Returns NULL to indicate that the command is finished.
			Returns an InputApi pointer to receive another line
			of input.
		 */
		virtual InputApi*	execute(	StateHeader&	state,
										const char*		arguments
										) const noexcept=0;
	};

}
}
}

#endif
