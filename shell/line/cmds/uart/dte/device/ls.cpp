/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "ls.h"
#include "iter.h"
#include "oscl/shell/line/cmdapi.h"

using namespace Oscl::Shell::Line::Cmds::UART::DTE::Device;

static const char	sizeErr[]   = {
	"Not enough Oscl::Shell::Line::StateMemory\n\r"
	};
static const char	memHelp[]   = {" - list UART DTE devices.\n\r"};
static const char	cmdName[]   = {"uart.dte.dev.ls"};

ListCmd::ListCmd(	Oscl::Mt::Itc::Dyn::Adv::
					Cli::SyncApi<Oscl::UART::Dyn::DTE::Device>& findApi
					) noexcept:
		_findApi(findApi)
		{
	}

const char*	ListCmd::command() const noexcept{
	return cmdName;
	}

void	ListCmd::help(Oscl::Stream::Output::Api&	output) const noexcept{
	output.write(cmdName,sizeof(cmdName)-1);
	output.write(memHelp,sizeof(memHelp)-1);
	}

Oscl::Shell::Line::InputApi*
	ListCmd::execute(	Oscl::Shell::Line::StateHeader&	state,
						const char*						arguments
						) const noexcept{
	if(state._maxStateMemorySize < sizeof(Iterate)){
		state._output.write(sizeErr,sizeof(sizeErr)-1);
		return 0;
		}
	Iterate*	iterator	= new (state._stateMemory)
								Iterate(_findApi);
	return iterator->start(state._output);
	}

