/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <stdio.h>
#include "iter.h"
#include "oscl/shell/line/cmdapi.h"

using namespace Oscl::Shell::Line::Cmds::UART::DTE::Device;

static const char	headerStr[] = {"------- UART DTE Device -----\r\n"};
static const char	oidStr[] = {" OID: "};
static const char	more[] = {"--More--"};
static const char	crlf[] = {"\r\n"};

Iterate::Iterate(	Oscl::Mt::Itc::Dyn::Adv::
					Cli::SyncApi<Oscl::UART::Dyn::DTE::Device>& findApi
					) noexcept:
		_findApi(findApi),
		_expected(0),
		_index(0)
		{
	}

Oscl::Shell::Line::InputApi*
	Iterate::start(Oscl::Stream::Output::Api& output) noexcept{
	_index		= 0;
	_expected	= 0;
	Oscl::Mt::Itc::Dyn::Handle<Oscl::UART::Dyn::DTE::Device>
	handle	= _findApi.find(*this);
	if(!handle){
		// No devices
		return 0;
		}

	printDevice(output,handle->getDynSrv());

	return this;
	}

Oscl::Shell::Line::InputApi*
	Iterate::input(	StateHeader&	state,
					const char*		inputLine
					) noexcept{

	if(*inputLine){
		return 0;
		}

	_index	= 0;
	Oscl::Mt::Itc::Dyn::Handle<Oscl::UART::Dyn::DTE::Device>
	handle	= _findApi.find(*this);
	if(!handle){
		// No devices
		return 0;
		}

	printDevice(state._output,handle->getDynSrv());

	return this;
	}

bool	Iterate::next(Oscl::UART::Dyn::DTE::Device& dev) noexcept{
	if(_index == _expected){
		++_expected;
		return true;
		}
	++_index;
	return false;
	}

void	Iterate::printDevice(	Oscl::Stream::Output::Api&			output,
								const Oscl::UART::Dyn::DTE::Device&	device
								) const noexcept{
	char	buffer[64];
	output.write(headerStr,sizeof(headerStr)-1);
	output.write(oidStr,sizeof(oidStr)-1);
	Oscl::ObjectID::RO::Iterator	it(device._location);
	for(it.first();it.more();it.next()){
		sprintf(buffer,".%u",it.current());
		output.write(buffer,strlen(buffer));
		}
	output.write(crlf,sizeof(crlf)-1);
	output.write(more,sizeof(more)-1);
	}

