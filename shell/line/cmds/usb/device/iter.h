/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_shell_line_usb_device_iterh_
#define _oscl_shell_line_usb_device_iterh_
#include "oscl/shell/line/input.h"
#include "oscl/mt/itc/dyn/adv/clisync.h"
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/driver/usb/setup/packet.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/desc/parser/parser.h"

/** */
namespace Oscl {
/** */
namespace Shell {
/** */
namespace Line {
/** */
namespace Cmds {
/** */
namespace Usb {
/** */
namespace Device {

/** */
enum{descBufferSize=256};

/** */
struct BuffMem {
	/** */
	Oscl::Memory::AlignedBlock< descBufferSize >	data;
	};

/** */
struct IterMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Oscl::Usb::Setup::Packet) >	setup;
	/** */
	BuffMem															data;
	};

/** */
class Iterate :	private	Oscl::Shell::Line::InputApi,
				private	Oscl::Mt::Itc::Dyn::Adv::Iterator<Oscl::Usb::DynDevice>
				{
		/** */
		Oscl::Memory::AlignedBlock< descBufferSize >	_buffer;

		/** */
		Oscl::Usb::Desc::Parser::Part					_parser;

		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Cli::SyncApi<Oscl::Usb::DynDevice>&				_findApi;

		/** */
		unsigned										_expected;

		/** */
		unsigned										_index;

		/** */
		IterMem&										_mem;

		/** */
		bool											_error;

	public:
		/** */
		Iterate(	Oscl::Mt::Itc::Dyn::Adv::
					Cli::SyncApi<Oscl::Usb::DynDevice>&	findApi,
					IterMem&							mem
					) noexcept;

		/** */
		InputApi*	start(Oscl::Stream::Output::Api& output) noexcept;

		/** */
		void	printDevice(	Oscl::Stream::Output::Api&	output,
								const Oscl::Usb::DynDevice& device
								) noexcept;

	private:
		/** returns true if failed */
		bool	getConfig(Oscl::Usb::Pipe::Message::SyncApi& device) noexcept;

	private:	// Oscl::Shell::Line::InputApi
		/** */
		InputApi*	input(	StateHeader&	state,
							const char*		inputLine
							) noexcept;

	private:	// Dyn::Adv::Iterator
		/** */
		bool	next(Oscl::Usb::DynDevice& dev) noexcept;
	};

}
}
}
}
}
}

#endif
