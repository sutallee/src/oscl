/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "iter.h"
#include "oscl/shell/line/cmdapi.h"
#include "oscl/driver/usb/setup/getconfigdesc.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/driver/usb/pipe/status.h"

using namespace Oscl::Shell::Line::Cmds::Usb::Device;

static const char	headerStr[] = {"------- USB Device -----\r\n"};
static const char	oidStr[] = {" OID: "};
static const char	more[] = {"--More--"};
static const char	crlf[] = {"\r\n"};

Iterate::Iterate(	Oscl::Mt::Itc::Dyn::Adv::
					Cli::SyncApi<Oscl::Usb::DynDevice>& findApi,
					IterMem&							mem
					) noexcept:
		_parser(	*(const Oscl::Usb::Desc::Config*)&_buffer,
					sizeof(_buffer)
					),
		_findApi(findApi),
		_expected(0),
		_index(0),
		_mem(mem)
		{
	}

/** */
class Visitor : public Oscl::Usb::Desc::Parser::VisitorApi {
		/** */
		Oscl::Stream::Output::Api&	_output;
	public:
		/** */
		Visitor(Oscl::Stream::Output::Api&  output) noexcept;
	private:
		/** */
		void	visit(const Oscl::Usb::Desc::Config& desc) noexcept;
		/** */
		void	visit(const Oscl::Usb::Desc::Interface& desc) noexcept;
		/** */
		void	visit(const Oscl::Usb::Desc::Endpoint& desc) noexcept;
		/** */
		void	visit(const void* unknown,unsigned length) noexcept;
	};

Oscl::Shell::Line::InputApi*
	Iterate::start(Oscl::Stream::Output::Api& output) noexcept{
	_index		= 0;
	_expected	= 0;
	Oscl::Mt::Itc::Dyn::Handle<Oscl::Usb::DynDevice>
	handle	= _findApi.find(*this);
	if(!handle){
		// No devices
		return 0;
		}

	printDevice(output,handle->getDynSrv());

	return this;
	}

Oscl::Shell::Line::InputApi*
	Iterate::input(	StateHeader&	state,
					const char*		inputLine
					) noexcept{

	if(*inputLine){
		return 0;
		}

	_index	= 0;
	Oscl::Mt::Itc::Dyn::Handle<Oscl::Usb::DynDevice>
	handle	= _findApi.find(*this);
	if(!handle){
		// No devices
		return 0;
		}

	printDevice(state._output,handle->getDynSrv());

	return this;
	}

bool	Iterate::next(Oscl::Usb::DynDevice& dev) noexcept{
	if(_index == _expected){
		_error	= getConfig(dev._ctrlPipe.getSyncApi());
		if(!_error){
			memcpy(&_buffer,&_mem.data,sizeof(_buffer));
			}
		++_expected;
		return true;
		}
	++_index;
	return false;
	}

void	Iterate::printDevice(	Oscl::Stream::Output::Api&	output,
								const Oscl::Usb::DynDevice&	device
								) noexcept{
	char	buffer[64];
	output.write(headerStr,sizeof(headerStr)-1);
	output.write(oidStr,sizeof(oidStr)-1);
	Oscl::ObjectID::RO::Iterator	it(device._location);
	for(it.first();it.more();it.next()){
		sprintf(buffer,".%u",it.current());
		output.write(buffer,strlen(buffer));
		}
	output.write(crlf,sizeof(crlf)-1);
	sprintf(	buffer,
				" USB:             V%2.2X.%2.2X\n\r",
				(unsigned)(device._bcdUSB >> 8),
				(unsigned)(device._bcdUSB && 0x00FF)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Class:           0x%2.2X\n\r",
				(unsigned)(device._bDeviceClass)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Sub Class:       0x%2.2X\n\r",
				(unsigned)(device._bDeviceSubClass)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Protocol:        0x%2.2X\n\r",
				(unsigned)(device._bDeviceProtocol)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Max Packet Size: 0x%2.2X\n\r",
				(unsigned)(device._bMaxPacketSize)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Vendor:          0x%4.4X\n\r",
				(unsigned)(device._idVendor)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Product:         0x%4.4X\n\r",
				(unsigned)(device._idProduct)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Device:          V%2.2X.%2.2X\n\r",
				(unsigned)(device._bcdDevice >> 8),
				(unsigned)(device._bcdDevice && 0x00FF)
				);
	output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" Reserved:        %u\n\r",
				(unsigned)(device._reserved)
				);
	output.write(buffer,strlen(buffer));

	if(!_error){
		Visitor	visitor(output);
		for(_parser.first();_parser.more();_parser.next()){
			_parser.current(visitor);
			}
		}

	output.write(more,sizeof(more)-1);
	}

bool	Iterate::getConfig(Oscl::Usb::Pipe::Message::SyncApi& device) noexcept{
	// The config descriptor is actually read twice.
	// The first time is to get the *actual* length
	// of the entire configuration descriptor. This actual
	// length is then used to read the entire descriptor
	// in the second read. This is necesssary since some
	// USB devices do not respond when asked to return
	// a configuration descriptor that is larger than the
	// actual descriptor.
	Oscl::Usb::Setup::GetConfigDesc*
	setup	= new(&_mem.setup)
				Oscl::Usb::Setup::
				GetConfigDesc(	0,	// descriptorIndex,
								0,	// languageID
								sizeof(Oscl::Usb::Desc::Config)
								);
	const Oscl::Usb::Pipe::Status::Result*
	failed = device.read(*setup,&_mem.data);
	if(!failed){
		const Oscl::Usb::Desc::Config*
		desc	= (Oscl::Usb::Desc::Config*)&_mem.data;
		setup	= new(&_mem.setup)
					Oscl::Usb::Setup::
					GetConfigDesc(	0,	// descriptorIndex,
									0,	// languageID
									desc->_wTotalLength
									);
		failed = device.read(*setup,&_mem.data);
		}
	return failed;
	}

Visitor::Visitor(Oscl::Stream::Output::Api&  output) noexcept:
		_output(output)
		{
	}

void	Visitor::visit(const Oscl::Usb::Desc::Config& desc) noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"Configuration Descriptor\n\r"
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" wTotalLength:         0x%4.4X\n\r",
				(unsigned)(desc._wTotalLength)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bNumInterfaces:       0x%2.2X\n\r",
				(unsigned)(desc._bNumInterfaces)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bConfigurationValue:  0x%2.2X\n\r",
				(unsigned)(desc._bConfigurationValue)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" iConfiguration:       0x%2.2X\n\r",
				(unsigned)(desc._iConfiguration)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bmAttributes:         0x%2.2X\n\r",
				(unsigned)(desc._bmAttributes)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bMaxPower:            0x%2.2X\n\r",
				(unsigned)(desc._bMaxPower)
				);
	_output.write(buffer,strlen(buffer));
	}

void	Visitor::visit(const Oscl::Usb::Desc::Interface& desc) noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"Interface Descriptor\n\r"
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bInterfaceNumber:     0x%2.2X\n\r",
				(unsigned)(desc._bInterfaceNumber)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bAlternateSetting:    0x%2.2X\n\r",
				(unsigned)(desc._bAlternateSetting)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bNumEndpoints:        0x%2.2X\n\r",
				(unsigned)(desc._bNumEndpoints)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bInterfaceClass:      0x%2.2X\n\r",
				(unsigned)(desc._bInterfaceClass)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bInterfaceSubClass:   0x%2.2X\n\r",
				(unsigned)(desc._bInterfaceSubClass)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" bInterfaceProtocol:   0x%2.2X\n\r",
				(unsigned)(desc._bInterfaceProtocol)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				" iInterface:           0x%2.2X\n\r",
				(unsigned)(desc._iInterface)
				);
	_output.write(buffer,strlen(buffer));
	}

void	Visitor::visit(const Oscl::Usb::Desc::Endpoint& desc) noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"Endpoint Descriptor\n\r"
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"  bEndpointAddress:    0x%2.2X\n\r",
				(unsigned)(desc._bEndpointAddress)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"  bmAttributes:        0x%2.2X\n\r",
				(unsigned)(desc._bmAttributes)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"  wMaxPacketSize:      0x%4.4X\n\r",
				(unsigned)(desc._wMaxPacketSize)
				);
	_output.write(buffer,strlen(buffer));
	sprintf(	buffer,
				"  bInterval:           0x%2.2X\n\r",
				(unsigned)(desc._bInterval)
				);
	_output.write(buffer,strlen(buffer));
	}

void	Visitor::visit(const void* unknown,unsigned length) noexcept{
	char	buffer[64];
	sprintf(	buffer,
				"Unknown Descriptor\n\r"
				);
	_output.write(buffer,strlen(buffer));
	}

