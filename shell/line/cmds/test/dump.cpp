/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "dump.h"
#include "oscl/shell/line/cmdapi.h"
#include "oscl/mt/thread.h"
#include "oscl/error/info.h"

using namespace Oscl::Shell::Line::Cmds::Test;

static const char	memHelp[]   = {" [delay [iterations]] - "
									"Dump ASCII block to console.\n\r"
									};
static const char	cmdName[]   = {"test.dump"};
static const char	invalidArgument[]   = {": invalid argument\n\r"};
static const char	testPattern[]   = {
	"The quick brown fox jumped over the lazy dog's back.\n\r"
	"THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK.\n\r"
	"0123456789012345678901234567890123456789"
	"0123456789012345678901234567890123456789\n\r"
	};

DumpCmd::DumpCmd() noexcept
		{
	}

const char*	DumpCmd::command() const noexcept{
	return cmdName;
	}

void	DumpCmd::help(Oscl::Stream::Output::Api& output) const noexcept{
	output.write(cmdName,sizeof(cmdName)-1);
	output.write(memHelp,sizeof(memHelp)-1);
	}

Oscl::Shell::Line::InputApi*
	DumpCmd::execute(	StateHeader&		state,
						const char*			arguments
						) const noexcept{
	unsigned long	pauseInMilliseconds	= 0;
	unsigned long	nIterations	= 1;
	if(*arguments != '\0'){
		char*	endptr;
		unsigned long
		n	= strtoul(	arguments,
						&endptr,
						0
						);
		if(endptr == arguments){
			state._output.write(cmdName,sizeof(cmdName)-1);
			state._output.write(invalidArgument,sizeof(invalidArgument)-1);
			return 0;
			}
		pauseInMilliseconds	= n * 1000;
		char*	last	= endptr;
		n = strtoul(	last,
						&endptr,
						0
						);
		if(endptr != last){
			nIterations	= n;
			}
		}
//	Oscl::Error::Info::log(testPattern);
	if(pauseInMilliseconds){
		Oscl::Mt::Thread::sleep(pauseInMilliseconds);
		}
	for(unsigned i=0;i<nIterations;++i){
		state._output.write(testPattern,sizeof(testPattern)-1);
		}

	return 0;
	}

