/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "monitor.h"
#include "oscl/mt/itc/srv/crespapi.h"
#include "oscl/mt/thread.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Shell::Line::UART::DTE;

Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::UART::Dyn::DTE::Device>&	advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::UART::
								Dyn::DTE::Device
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Shell::Line::ExecApi&				exec,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::
			Monitor<Oscl::UART::Dyn::DTE::Device>(	advSAP,
													advFind,
													myPapi,
													_match
													),
		_advSAP(advSAP),
		_myPapi(myPapi),
		_match(),
		_driverPapi(driverPapi),
		_dynSrvPapi(dynSrvPapi),
		_exec(exec),
		_echo(true)
		{
	}

void	Monitor::activate() noexcept{
	createDriver();
	}

void	Monitor::releaseResources() noexcept{
	deactivateDone();
	}

void	Monitor::closeDriver() noexcept{
#if 0
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_driver->getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_driver->getSAP().post(resp->getSrvMsg());
#endif
	}

void	Monitor::deactivate() noexcept{
	// Asynchronously issue CloseReq
//	closeDriver();
	deactivateDone();
	}

void	Monitor::createDriver() noexcept{
	Oscl::UART::Dyn::DTE::Device&	dyndev	= _handle->getDynSrv();
	_driver	= new (&_driverMem)
				Oscl::Shell::Line::
				Interpreter(	dyndev._dte.getUartApi().input(),
								dyndev._dte.getUartApi().output(),
								_exec,
								_buffer,
								bufferSize,
								&_stateMem,
								sizeof(_stateMem),
								_echo
								);
	// This Interpreter must be asynchronous such that
	// it may be opened and then closed asyncrhonously.
	_driver->run();	// FIXME:!!!!
	}

void	Monitor::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	_driver->~Interpreter();
	_driver	= 0;
	releaseResources();
	}

void	Monitor::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	}

