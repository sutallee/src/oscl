/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_shell_line_uart_dte_monitorh_
#define _oscl_shell_line_uart_dte_monitorh_
#include "match.h"
#include "oscl/uart/dyn/dte/device.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"
#include "oscl/shell/line/basic/interp.h"

/** */
namespace Oscl {
/** */
namespace Shell {
/** */
namespace Line {
/** */
namespace UART {
/** */
namespace DTE {

/** */
class Monitor :
		public Oscl::Mt::Itc::Dyn::Unit::Monitor<Oscl::UART::Dyn::DTE::Device>,
		public Oscl::Mt::Itc::Srv::Close::Resp::Api
		{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::CloseMem							_closeMem;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		Req::Api<Oscl::UART::Dyn::DTE::Device>::SAP&	_advSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		Oscl::Memory::
		AlignedBlock<512>								_stateMem;
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Shell::
								Line::Interpreter
								)
							>							_driverMem;
		/** */
		Match											_match;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_driverPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_dynSrvPapi;
		/** */
		enum{bufferSize=128};
		/** */
		char											_buffer[bufferSize];
		/** */
		Oscl::Shell::Line::ExecApi&					_exec;
		/** */
		Oscl::Shell::Line::Interpreter*				_driver;
		/** */
		const bool										_echo;

	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::UART::Dyn::DTE::Device>&	advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::UART::
								Dyn::DTE::Device
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Shell::Line::ExecApi&				exec,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
	private:
		/** */
		void	releaseResources() noexcept;
		/** */
		void	closeDriver() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;

	private: // helpers
		/** */
		void	createDriver() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept;
	};

}
}
}
}
}

#endif

