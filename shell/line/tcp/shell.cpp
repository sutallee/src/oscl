/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

//#include <string.h>
//#include <stdio.h>
//#include <ctype.h>
#include "shell.h"
#include "oscl/shell/line/basic/interp.h"
#include "oscl/error/info.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Shell::Line::TCP;

Shell::Shell(	Oscl::Shell::Line::ExecApi&		exec,
				Oscl::Protocol::IP::
				TCP::Conn::Srv::Server::Part&	connection
				) noexcept:
		_exec(exec),
		_connection(connection)
		{
	}

void	Shell::run() noexcept{
	while(true){
		_connection.open();
		Oscl::Mt::Thread::sleep(2000);
		Oscl::Error::Info::log("listening\n\r");
		_connection.getConnApi().listen();
		Oscl::Error::Info::log("openning shell\n\r");
		char	shellBuff[32];
		Oscl::Memory::AlignedBlock<512>	stateMem;

		Oscl::Shell::Line::Interpreter
			shell(	_connection.input(),
					_connection.output(),
					_exec,
					shellBuff,
					sizeof(shellBuff),
					&stateMem,
					sizeof(stateMem),
					true
					);

		shell.run();
		_connection.close();
		}
	}

