/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "exec.h"
#include "oscl/strings/strip.h"

using namespace Oscl::Shell::Line;

Exec::Exec() noexcept
		{
	}

static const char	noSuchCmd[]	= {"No such command.\n\r"};
static const char	noMatch[]	= {"No help found.\n\r"};
static const char	crlf[]		= {"\n\r"};

InputApi*	Exec::execute(	StateHeader&	state,
							const char*		cmdLine
							) const noexcept{
	CmdApi*		cmd;
	const char*	cmdLineCmd		= Oscl::Strings::stripSpace(cmdLine);
	unsigned	cmdLineCmdLen	= strlen(cmdLineCmd);
	const char*	args			= Oscl::Strings::stripNotSpace(cmdLineCmd);
	cmdLineCmdLen				-= strlen(args);
	args						= Oscl::Strings::stripSpace(args);

	if(*cmdLineCmd == '\0'){
		return 0;
		}

	if(*cmdLineCmd == '?'){
		if(*args == '\0'){
			for(cmd=_commands.first();cmd;cmd=_commands.next(cmd)){
				const char*	cmdStr	= cmd->command();
				unsigned	cmdLen	= strlen(cmdStr);
				state._output.write(cmdStr,cmdLen);
				state._output.write(crlf,sizeof(crlf)-1);
				}
			return 0;
			}
		bool	matchedAtLeastOne	= false;
		for(cmd=_commands.first();cmd;cmd=_commands.next(cmd)){
			const char*	cmdStr	= cmd->command();
			if(strstr(cmdStr,args)){
				matchedAtLeastOne	= true;
				cmd->help(state._output);
				}
			}
		if(!matchedAtLeastOne){
			state._output.write(noMatch,sizeof(noMatch)-1);
			}
		return 0;
		}

	for(cmd=_commands.first();cmd;cmd=_commands.next(cmd)){
		const char*	cmdString		= cmd->command();
		unsigned	cmdStringLen	= strlen(cmdString);
		if(cmdStringLen != cmdLineCmdLen){
			// Must match in length
			continue;
			}
		if(!strncmp(cmdString,cmdLineCmd,cmdStringLen)){
			InputApi*	inApi	= cmd->execute(state,args);
			if(inApi){
				return inApi;
				}
			break;
			}
		}

	if(!cmd){
		// No matching command found;
		state._output.write(noSuchCmd,sizeof(noSuchCmd)-1);
		}

	return 0;
	}

void	Exec::attach(CmdApi& cmd) noexcept{
	_commands.put(&cmd);
	}

