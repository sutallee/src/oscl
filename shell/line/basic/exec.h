/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_shell_line_basic_exech_
#define _oscl_shell_line_basic_exech_
#include "oscl/stream/output/api.h"
#include "oscl/queue/queue.h"
#include "oscl/shell/line/cmdapi.h"
#include "oscl/shell/line/execapi.h"

/** */
namespace Oscl {
/** */
namespace Shell {
/** */
namespace Line {

/** This class performs the job of displaying a command prompt;
	parsing the input stream; executing the appropriate
	command, or displaying an appropriate error message.
 */
class Exec : public ExecApi {
	private:
		/** */
		Oscl::Queue<CmdApi>			_commands;

	public:
		/** */
		Exec() noexcept;

		/** This operation is invoked to execute the command
			specified by "cmdLine".
			Returns a NULL pointer if the command is completed
			or a pointer to an InputApi if the command requires
			another line of input.
		 */
		InputApi*	execute(	StateHeader&	state,
								const char*		cmdLine
								) const noexcept;

		/** This operation is invoked by the context to
			add commands supported by the interpreter.
			This operation should not be invoked after
			the run() operation is invoked.
		 */
		void	attach(CmdApi& cmd) noexcept;

	};

}
}
}

#endif
