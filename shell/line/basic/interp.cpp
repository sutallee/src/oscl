/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "interp.h"

using namespace Oscl::Shell::Line;

Interpreter::Interpreter(	Oscl::Stream::Input::Api&	input,
							Oscl::Stream::Output::Api&	output,
							ExecApi&					execApi,
							char						buffer[],
							unsigned					bufferSize,
							void*			const		stateMemory,
							unsigned					stateMemorySize,
							bool						echo
							) noexcept:
		_input(input),
		_output(output),
		_execApi(execApi),
		_buffer(buffer),
		_bufferSize(bufferSize),
		_stateMemory(stateMemory),
		_stateMemorySize(stateMemorySize),
		_echo(echo)
		{
	}

static const char	prompt[]	= {"$ "};
static const char	crlf[]		= {"\n\r"};
static const char	erase[]	= {"\b \b"};

void	Interpreter::run() noexcept{
	while(true){
		_output.write(prompt,sizeof(prompt)-1);

		if(!getNextCmdLine()){
			return;
			}
		Oscl::Shell::Line::StateHeader	state(	_output,
												_stateMemory,
												_stateMemorySize
												);

		InputApi*	inApi	= _execApi.execute(state,_buffer);
		if(!inApi){
			continue;
			}
		while(true){
			if(!getNextCmdLine()){
				return;
				}
			
			inApi = inApi->input(state,_buffer);
			if(!inApi){
				break;
				}
			}
		}
	}

bool	Interpreter::getNextCmdLine() noexcept{
	char*		p	= _buffer;
	unsigned	len	= 0;
	while(true){
		// Read a character
		if(_input.read(&p[len],1) != 1){
			return false;
			}

		if(p[len] == '\0'){
			continue;
			}

		// Check for end of command line
		if(p[len] == '\n' || p[len] == '\r'){
			if(_echo){
				_output.write(crlf,sizeof(crlf)-1);
				}
			p[len]	= '\0';
			return true;
			}

		// Check for delete
		if(_echo && p[len] == '\b'){
			if(len > 0){
				// erase previous character
				_output.write(erase,sizeof(erase)-1);
				--len;
				}
			}
		else if(_echo && p[len] == 0x15){
			// Kill Line ^U
			for(;len;--len){
				_output.write(erase,sizeof(erase)-1);
				}
			}
		// If not buffer overflow advance counter
		else if(len < (_bufferSize-1)){
			if(_echo){
				_output.write(&p[len],1);
				}
			++len;
			}
		}
	}
