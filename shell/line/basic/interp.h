/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_shell_line_basic_interph_
#define _oscl_shell_line_basic_interph_
#include "oscl/stream/input/api.h"
#include "oscl/stream/output/api.h"
#include "oscl/shell/line/execapi.h"
#include "oscl/stream/input/buffer/buffer.h"

/** */
namespace Oscl {
/** */
namespace Shell {
/** */
namespace Line {

/** This class performs the job of displaying a command prompt;
	and executing the command line.
 */
class Interpreter {
	private:
		/** */
		Oscl::Stream::Input::Buffer<256>	_input;
		/** */
		Oscl::Stream::Output::Api&	_output;
		/** */
		ExecApi&					_execApi;
		/** */
		char*						_buffer;
		/** */
		const unsigned				_bufferSize;
		/** This memory must be aligned to structure alignement.
		 */
		void*			const		_stateMemory;
		/** */
		unsigned					_stateMemorySize;
		/** */
		bool						_echo;

	public:
		/** */
		Interpreter(	Oscl::Stream::Input::Api&	input,
						Oscl::Stream::Output::Api&	output,
						ExecApi&					execApi,
						char						buffer[],
						unsigned					bufferSize,
						void*			const		stateMemory,
						unsigned					stateMemorySize,
						bool						echo = false
						) noexcept;

		/** This operation is invoked by the context to
			start the shell operation. It only returns
			if there is a stream error. In the case of
			a stream error, it is assumed that the stream
			has been closed by the peer.
		 */
		void	run() noexcept;

	private:
		/** Waits on input stream until a complete
			command has been entered. The command
			line is placed in the _buffer and is
			null terminated before this operation
			returns. False is returned if there
			is an error on the input stream.
		 */
		bool	getNextCmdLine() noexcept;
	};

}
}
}

#endif
