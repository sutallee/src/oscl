/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_print_itc_itc_reqapih_
#define _oscl_print_itc_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdarg.h>

/** */
namespace Oscl {

/** */
namespace Print {

/** */
namespace ITC {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This is the host interface for attaching and detaching a
	symbiont to/from the host. It is to be implemented by the
	host and used by the context.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the PrintReq
			ITC message.
		 */
		class PrintPayload {
			public:
				/**	
				 */
				const char*	_format;

				/**	
				 */
				va_list	_ap;

			public:
				/** The PrintPayload constructor. */
				PrintPayload(

					const char*	format,

					va_list	ap
					) noexcept;

				/** The PrintPayload constructor. */
				~PrintPayload() noexcept;
			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Print::ITC::Req::Api,
					PrintPayload
					>		PrintReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Print::ITC::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Print::ITC::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a PrintReq is received.
		 */
		virtual void	request(	Oscl::Print::ITC::
									Req::Api::PrintReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
