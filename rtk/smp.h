/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_rtk_smph_
#define _oscl_rtk_smph_
#include "oscl/krux/port/frame.h"

/** */
namespace Oscl{
/** */
namespace Rtk{

enum { ExceptionStackSizeInBytes = 0x400};

typedef struct ExceptionStack {
	unsigned long	longStack[ExceptionStackSizeInBytes/sizeof(unsigned long)];
	} ExceptionStack;

enum { InterruptStackSizeInBytes = 0x800};

typedef struct InterruptStack {
	unsigned long	longStack[InterruptStackSizeInBytes/sizeof(unsigned long)];
	inline void*  getTopOfPreDecrementStack() noexcept{
		return &longStack[sizeof(InterruptStack)/sizeof(unsigned long)];
		}
	} InterruptStack;

typedef struct ThreadSystemStack {
	unsigned long	longStack[InterruptStackSizeInBytes/sizeof(unsigned long)];
	inline void*  getTopOfPreDecrementStack() noexcept{
		return &longStack[sizeof(InterruptStack)/sizeof(unsigned long)];
		}
	} ThreadSystemStack;

/** This type defines the structure of a
	memory area that is used to keep processor
	instance specific data to support SMP
	configurations.
	Each processor stores a reference to this
	data structure in SPRG0.
	To promote efficient access to this data:
	1.	The structure should be page aligned.
	2.	The structure should be cache line aligned.
	3.	The structure should occupy no more than
		4096 bytes (one page) of memory.
	These properties improve efficiency by:
	
	During first level exception handling when
	the caches are disabled, only read accesses
	should be performed upon the structure.
 */

typedef class SmpInstance {
	
	};

}
}

#endif
