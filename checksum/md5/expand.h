/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_checksum_md5_expandh_
#define _oscl_checksum_md5_expandh_
#include "calculator.h"

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace MD5 {

/** */
class ModifierApi {
	public:
		/** Shut-up GCC warning.
		 */
		virtual ~ModifierApi() {}

		/** This operation is invoked immediately after
			a new MD5 Calculator is created. It is intended
			that the implementation of this operation invoke
			the update() operation to append any input
			that it deems necessary before the expansion
			input data is appended.
		 */
		virtual void	prepend(Calculator& c) noexcept=0;

		/** This operation is invoked immediately before
			the MD5 Calculator final() operation is invoked.
			It is intended that the implementation of this
			operation invoke the update() operation to append
			any additional input that it deems necessary before
			the digest is extracted from the calculator.
		 */
		virtual void	append(Calculator& c) noexcept=0;
	};

/** This operation applies the MD5 checksum to the
	"input" material and copies the result to the
	"output". If a single application of MD5 is
	insufficient to supply "outputLength" bytes,
	then the process is repeated using the digest
	from each operation as the input to the next
	until "outputLength" bytes have been copied
	to the output. The least significant portion
	of the last MD5 iteration that is not appended
	to the output buffer is discarded.
 */
void	expand(	const void*	input,
				unsigned	inputLength,
				void*		output,
				unsigned	outputLength
				) noexcept;

/** This variant works the same as the expand
	above, but updates the MD5 checksum of
	each iteration with the "preInput" before
	appending the "input" or the result of the
	previous iteration and then updates the
	MD5 checksum of each iteration with the
	"postInput" before the MD5 checksum is
	finalized.
	The preInput and postInput parameters
	may be null/zero.
 */
void	expand(	const void*	preInput,
				unsigned	preInputLength,
				const void*	postInput,
				unsigned	postInputLength,
				const void*	input,
				unsigned	inputLength,
				void*		output,
				unsigned	outputLength
				) noexcept;

/** This variant works the same as the expand
	above, but is more flexible since multiple
	fragmented buffers of material may be
	added to each iteration during the
	prepend() and append() operations. The modifier
	prepend() and append() operations are
	invoked for each new MD5 iteration giving
	the client application the ability to
	update the calculator with constants
	during the expansion process.
 */
void	expand(	ModifierApi&	modifier,
				const void*		input,
				unsigned		inputLength,
				void*			output,
				unsigned		outputLength
				) noexcept;

}
}
}

#endif

