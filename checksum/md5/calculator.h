/*
    Derived from the RSA Data Security, Inc. MD5 Message-Digest Algorithm.
*/

/*  Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
    rights reserved.

    License to copy and use this software is granted provided that it
    is identified as the "RSA Data Security, Inc. MD5 Message-Digest
    Algorithm" in all material mentioning or referencing this software
    or this function.

    License is also granted to make and use derivative works provided
    that such works are identified as "derived from the RSA Data
    Security, Inc. MD5 Message-Digest Algorithm" in all material
    mentioning or referencing the derived work.

    RSA Data Security, Inc. makes no representations concerning either
    the merchantability of this software or the suitability of this
    software for any particular purpose. It is provided "as is"
    without express or implied warranty of any kind.
    These notices must be retained in any copies of any part of this
    documentation and/or software.
 */
#ifndef _oscl_checksum_md5_md5h_
#define _oscl_checksum_md5_md5h_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace MD5 {

enum{digestSize=16};

/* MD5 context. */
class Calculator {
	public:
		/** state (ABCD) */
		uint32_t	_state[4];
		/** number of bits, modulo 2^64 (lsb first) */
		uint32_t	_count[2];
		/** input buffer */
		unsigned char	_buffer[64];
	public:
		/** */
		Calculator() noexcept;
		/** */
		~Calculator();
		/** */
		void	update(const void* input, unsigned inputLen) noexcept;
		/** */
		void	final(unsigned char digest[digestSize]) noexcept;
		/** */
		void	reset() noexcept;
	};

}
}
}

#endif
