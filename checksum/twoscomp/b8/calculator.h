/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_checksum_onescomp_b8_calculatorh_
#define _oscl_checksum_onescomp_b8_calculatorh_

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace TwosComp {
/** */
namespace B8 {

/** This class calculates a sixteen bit ones complement
	checksum over a "stream" of bytes.
 */
class Calculator {
	private:
		/** */
		unsigned char	_sum;
		/** */
		unsigned char	_extra;
		/** */
		bool			_odd;

	public:
		/** */
		Calculator(unsigned char initialSum=0) noexcept;

		/** */
		void	reset(unsigned char initialSum=0) noexcept;

		/** */
		void	accumulate(	const void*		buffer,
							unsigned		length
							) noexcept;

		/** */
		unsigned char	final() const noexcept;
	};

}
}
}
}

#endif
