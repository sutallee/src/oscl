/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
 *  sha1.h
 *
 *  Description:
 *      This is the header file for code which implements the Secure
 *      Hashing Algorithm 1 as defined in FIPS PUB 180-1 published
 *      April 17, 1995.
 *
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *
 *      Please read the file sha1.c for more information.
 *
 */

#ifndef _oscl_checksum_sha1_calculatorh_
#define _oscl_checksum_sha1_calculatorh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace SHA1 {

enum{HashSize=20};

/** Since the maximum number of bits must be maintained
	as a 32-bit unsigned integer, the maximum message
	size is limited to (2^32)/8 bytes (536,870,912).
	An overflow, will return false upon invocation
	of result() as the last step in the calculation.
 */
class Calculator {
	private:
		/** Message Digest  */
		uint32_t	_Intermediate_Hash[HashSize/4];

		/** Message length in bits */
		uint32_t	_Length_Low;

		/** Message length in bits */
		uint32_t	_Length_High;

		/* Index into message block array   */
		signed		_Message_Block_Index;

		/** 512-bit message blocks */
		uint8_t	_Message_Block[64];

		/** Is the digest computed? */
		bool		_Computed;

		/** Is the message digest corrupted? */
		bool		_Corrupted;

	public:
		/** Resets the calculator at construction. */
		Calculator() noexcept;

		/** Reset the calcualtor */
		void	reset() noexcept;

		/** Invoked to accumulate message_array bytes
			into the calcuator.
		 */
		void		input(	const void*	message_array,
							unsigned	length
							) noexcept;

		/** Finalizes the calculator and returns the resultant
			digest unless the input() message length was too
			long.
			Returns true unless message was too long for the implementation
		 */
		bool	result(uint8_t	Message_Digest[HashSize]) noexcept;

	private:
		/** */
		void	padMessage() noexcept;
		/** */
		void	processMessageBlock() noexcept;
	};

}
}
}

#endif

