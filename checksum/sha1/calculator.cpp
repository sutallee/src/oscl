/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
 *  sha1.c
 *
 *  Description:
 *      This file implements the Secure Hashing Algorithm 1 as
 *      defined in FIPS PUB 180-1 published April 17, 1995.
 *
 *      The SHA-1, produces a 160-bit message digest for a given
 *      data stream.  It should take about 2**n steps to find a
 *      message with the same digest as a given message and
 *      2**(n/2) to find any two messages with the same digest,
 *      when n is the digest size in bits.  Therefore, this
 *      algorithm can serve as a means of providing a
 *      "fingerprint" for a message.
 *
 *  Portability Issues:
 *      SHA-1 is defined in terms of 32-bit "words".  This code
 *      uses <stdint.h> (included via "sha1.h" to define 32 and 8
 *      bit unsigned integer types.  If your C compiler does not
 *      support 32 bit unsigned integers, this code is not
 *      appropriate.
 *
 *  Caveats:
 *      SHA-1 is designed to work with messages less than 2^64 bits
 *      long.  Although SHA-1 allows a message digest to be generated
 *      for messages of any number of bits less than 2^64, this
 *      implementation only works with messages with a length that is
 *      a multiple of the size of an 8-bit character.
 *
 */

#include "calculator.h"

using namespace Oscl::Checksum::SHA1;
using namespace Oscl;

/*
 *  Define the SHA1 circular left shift macro
 */
inline uint32_t	SHA1CircularShift(	unsigned	bits,
										uint32_t	word
										){
	return (((word) << (bits)) | ((word) >> (32-(bits))));
	}

Calculator::Calculator() noexcept{
	reset();
	}

/*
 *  Calculator::reset
 *
 *  Description:
 *      This function will initialize the Calcualtor in preparation
 *      for computing a new SHA1 message digest.
 *
 */
void Calculator::reset() noexcept {
    _Length_Low             = 0;
    _Length_High            = 0;
    _Message_Block_Index    = 0;

    _Intermediate_Hash[0]   = 0x67452301;
    _Intermediate_Hash[1]   = 0xEFCDAB89;
    _Intermediate_Hash[2]   = 0x98BADCFE;
    _Intermediate_Hash[3]   = 0x10325476;
    _Intermediate_Hash[4]   = 0xC3D2E1F0;

    _Computed   = 0;
    _Corrupted  = 0;
	}

/*
 *  SHA1Result
 *
 *  Description:
 *      This function will return the 160-bit message digest into the
 *      Message_Digest array  provided by the caller.
 *      NOTE: The first octet of hash is stored in the 0th element,
 *            the last octet of hash in the 19th element.
 *
 *  Parameters:
 *      Message_Digest: [out]
 *          Where the digest is returned.
 *
 *  Returns:
 *      True for success, false if input message was too long
 *
 */
bool Calculator::result(uint8_t Message_Digest[HashSize]) noexcept {
	int i;

	if (_Corrupted) {
		return false;
		}

	if (!_Computed) {
		padMessage();
		for(i=0; i<64; ++i) {
			/* message may be sensitive, clear it out */
			_Message_Block[i] = 0;
			}
		_Length_Low = 0;    /* and clear length */
		_Length_High = 0;
		_Computed = 1;
		}

	for(i = 0; i < HashSize; ++i){
		Message_Digest[i] = _Intermediate_Hash[i>>2] >> 8 * ( 3 - ( i & 0x03 ) );
		}

    return true;
	}

/*
 *  input
 *
 *  Description:
 *      This function accepts an array of octets as the next portion
 *      of the message.
 *
 *  Parameters:
 *      message_array: [in]
 *          An array of characters representing the next portion of
 *          the message.
 *      length: [in]
 *          The length of the message in message_array
 *
 */
void Calculator::input(	const void*	message,
						unsigned	length
						) noexcept {
	const uint8_t*	message_array	= (const uint8_t*)message;
	if (!length) {
		return;
		}

	if (_Corrupted) {
		return;
		}

	while(length-- && !_Corrupted) {
		_Message_Block[_Message_Block_Index++] = (*message_array & 0xFF);
		_Length_Low += 8;
		if (_Length_Low == 0){
			_Length_High++;
			if (_Length_High == 0) {
				/* Message is too long */
				_Corrupted = 1;
				}
			}

		if (_Message_Block_Index == 64) {
			processMessageBlock();
			}

    	message_array++;
		}
	}

/*
 *  processMessageBlock
 *
 *  Description:
 *      This function will process the next 512 bits of the message
 *      stored in the Message_Block array.
 *
 *  Parameters:
 *      None.
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the
 *      names used in the publication.
 *
 *
 */
void Calculator::processMessageBlock() noexcept {
	const uint32_t K[]	= {       /* Constants defined in SHA-1   */
							0x5A827999,
							0x6ED9EBA1,
							0x8F1BBCDC,
							0xCA62C1D6
							};
	int			t;					/* Loop counter                */
	uint32_t	temp;				/* Temporary word value        */
	uint32_t	W[80];				/* Word sequence               */
	uint32_t	A, B, C, D, E;		/* Word buffers                */

	/*
	 *  Initialize the first 16 words in the array W
	 */
	for(t = 0; t < 16; t++) {
		W[t] = _Message_Block[t * 4] << 24;
		W[t] |= _Message_Block[t * 4 + 1] << 16;
		W[t] |= _Message_Block[t * 4 + 2] << 8;
		W[t] |= _Message_Block[t * 4 + 3];
		}

	for(t = 16; t < 80; t++) {
		W[t] = SHA1CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
		}

	A = _Intermediate_Hash[0];
	B = _Intermediate_Hash[1];
	C = _Intermediate_Hash[2];
	D = _Intermediate_Hash[3];
	E = _Intermediate_Hash[4];

	for(t = 0; t < 20; t++) {
		temp =  SHA1CircularShift(5,A) +
				((B & C) | ((~B) & D)) + E + W[t] + K[0];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);

		B = A;
		A = temp;
		}

	for(t = 20; t < 40; t++) {
		temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
		}

	for(t = 40; t < 60; t++) {
		temp = SHA1CircularShift(5,A) +
				((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
		}

	for(t = 60; t < 80; t++) {
		temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
		E = D;
		D = C;
		C = SHA1CircularShift(30,B);
		B = A;
		A = temp;
		}

	_Intermediate_Hash[0] += A;
	_Intermediate_Hash[1] += B;
	_Intermediate_Hash[2] += C;
	_Intermediate_Hash[3] += D;
	_Intermediate_Hash[4] += E;

	_Message_Block_Index = 0;
	}

/*
 *  padMessage
 *
 *  Description:
 *      According to the standard, the message must be padded to an even
 *      512 bits.  The first padding bit must be a '1'.  The last 64
 *      bits represent the length of the original message.  All bits in
 *      between should be 0.  This function will pad the message
 *      according to those rules by filling the Message_Block array
 *      accordingly.  It will also call the ProcessMessageBlock function
 *      provided appropriately.  When it returns, it can be assumed that
 *      the message digest has been computed.
 *
 *  Parameters:
 *      ProcessMessageBlock: [in]
 *          The appropriate SHA*ProcessMessageBlock function
 *  Returns:
 *      Nothing.
 *
 */

void Calculator::padMessage() noexcept {
	/*
	 *  Check to see if the current message block is too small to hold
	 *  the initial padding bits and length.  If so, we will pad the
	 *  block, process it, and then continue padding into a second
	 *  block.
	 */
	if (_Message_Block_Index > 55) {
		_Message_Block[_Message_Block_Index++] = 0x80;
		while(_Message_Block_Index < 64) {
			_Message_Block[_Message_Block_Index++] = 0;
			}

		processMessageBlock();

		while(_Message_Block_Index < 56) {
			_Message_Block[_Message_Block_Index++] = 0;
			}
		}
	else {
		_Message_Block[_Message_Block_Index++] = 0x80;
		while(_Message_Block_Index < 56) {
			_Message_Block[_Message_Block_Index++] = 0;
			}
		}

	/*
	 *  Store the message length as the last 8 octets
	 */
	_Message_Block[56] = _Length_High >> 24;
	_Message_Block[57] = _Length_High >> 16;
	_Message_Block[58] = _Length_High >> 8;
	_Message_Block[59] = _Length_High;
	_Message_Block[60] = _Length_Low >> 24;
	_Message_Block[61] = _Length_Low >> 16;
	_Message_Block[62] = _Length_Low >> 8;
	_Message_Block[63] = _Length_Low;

	processMessageBlock();
	}

