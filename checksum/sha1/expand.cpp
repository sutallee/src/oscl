/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "expand.h"
#include "calculator.h"

using namespace Oscl::Checksum::SHA1;

void	Oscl::Checksum::SHA1::expand(	const void*	input,
										unsigned	inputLength,
										void*		output,
										unsigned	outputLength
										) noexcept{
	expand(	0,
			0,
			0,
			0,
			input,
			inputLength,
			output,
			outputLength
			);
	}

/** */
class SHA1Modifier : public ModifierApi {
	private:
		/** */
		const void*		_preInput;
		/** */
		const unsigned	_preLength;
		/** */
		const void*		_postInput;
		/** */
		const unsigned	_postLength;

	public:
		/** */
		SHA1Modifier(	const void*	preInput,
						unsigned	preLength,
						const void*	postInput,
						unsigned	postLength
						) noexcept:
				_preInput(preInput),
				_preLength(preLength),
				_postInput(postInput),
				_postLength(postLength)
				{
			}
	private:
		void	prepend(Calculator& c) noexcept{
			c.input(_preInput,_preLength);
			}
		void	append(Calculator& c) noexcept{
			c.input(_postInput,_postLength);
			}
	};

void	Oscl::Checksum::SHA1::expand(	const void*	preInput,
										unsigned	preInputLength,
										const void*	postInput,
										unsigned	postInputLength,
										const void*	input,
										unsigned	inputLength,
										void*		output,
										unsigned	outputLength
										) noexcept{
	SHA1Modifier	modifier(	preInput,
							preInputLength,
							postInput,
							postInputLength
							);
	expand(	modifier,
			input,
			inputLength,
			output,
			outputLength
			);
	}

void	Oscl::Checksum::SHA1::expand(	ModifierApi&	modifier,
										const void*		input,
										unsigned		inputLength,
										void*			output,
										unsigned		outputLength
										) noexcept{
	unsigned char	digest[HashSize];
	unsigned		nIterations	= outputLength/HashSize;

	unsigned		remaining	= outputLength;
	unsigned char*	out			= (unsigned char*)output;
	const void*		p			= input;
	unsigned		l			= inputLength;

	for(unsigned i=0;i<nIterations;++i){
		Calculator	calc;
		modifier.prepend(calc);
		calc.input(p,l);
		modifier.append(calc);
		calc.result(digest);
		memcpy(out,digest,HashSize);
		p			= digest;
		l			= HashSize;
		remaining	-= HashSize;
		out			+= HashSize;
		}
	if(remaining){
		Calculator	calc;
		modifier.prepend(calc);
		calc.input(p,l);
		modifier.append(calc);
		calc.result(digest);
		memcpy(out,digest,remaining);
		}
	}

