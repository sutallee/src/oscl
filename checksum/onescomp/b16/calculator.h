/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_checksum_onescomp_b16_calculatorh_
#define _oscl_checksum_onescomp_b16_calculatorh_

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace OnesComp {
/** */
namespace B16 {

#if 0
/** This class calculates a sixteen bit ones complement
	checksum over a "stream" of bytes.
 */
class Calculator {
	private:
		/** */
		unsigned long	_sum;
		/** */
		unsigned char	_extra;
		/** */
		bool			_odd;

	public:
		/** */
		inline Calculator(unsigned long initialSum=0) noexcept:
				_sum(initialSum),
				_odd(false)
				{
			}

		/** */
		inline void	reset(unsigned long initialSum=0) noexcept{
			_sum	= initialSum;
			_odd	= false;
			}

		/** */
		inline void	accumulate(	const void*		buffer,
								unsigned		length
								) noexcept{
			if(!length) return;

			const unsigned char*	p	= (const unsigned char*)buffer;
			unsigned long			sum	= _sum;
			unsigned long			word;
			const unsigned			len	= length >> 1;

			if(_odd){
				word	=	_extra;
				word	<<=	8;
				word	|=	*p;
				++p;
				--length;
				sum	+= word;
				}

			for(unsigned i=0;i<len;++i,p+=2){
				word	=	p[0];
				word	<<=	8;
				word	|=	p[1];
				sum	+= word;
				}

			const bool	odd	= length & 0x0001;
			_odd	= odd;
			if(odd){
				_extra	= p[0];
				}
			_sum	= sum;
			}

		/** */
		inline unsigned	final() const noexcept{
			unsigned long	word;
			word	= _sum;
			word	+= (word >> 16);
			return (unsigned)((~word) & 0x0000FFFF);
			}
	};
#else
/** This class calculates a sixteen bit ones complement
	checksum over a "stream" of bytes.
 */
class Calculator {
	private:
		/** */
		unsigned long	_sum;
		/** */
		unsigned char	_extra;
		/** */
		bool			_odd;

	public:
		/** */
		Calculator(unsigned long initialSum=0) noexcept;

		/** */
		void	reset(unsigned long initialSum=0) noexcept;

		/** */
		void	accumulate(	const void*		buffer,
								unsigned		length
								) noexcept;

		/** */
		unsigned	final() const noexcept;
	};
#endif

}
}
}
}

#endif
