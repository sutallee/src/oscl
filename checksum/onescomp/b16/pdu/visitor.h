/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_checksum_onescomp_b16_pdu_visitorh_
#include "oscl/checksum/onescomp/b16/calculator.h"
#include "oscl/pdu/visitor.h"

/** */
namespace Oscl {
/** */
namespace Checksum {
/** */
namespace OnesComp {
/** */
namespace B16 {
/** */
namespace PDU {

/** */
class Visitor : public Oscl::Pdu::ReadOnlyVisitor {
	public:
		/** */
		Oscl::Checksum::OnesComp::B16::Calculator	_csum;

	private:
		/** */
		bool /* stop */	accept(	const Oscl::Pdu::Composite&	p,
								unsigned					position
								) noexcept;

		bool /* stop */	accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
								unsigned							position
								) noexcept;
		bool /* stop */ accept(	const Oscl::Pdu::Leaf&	p,
								unsigned				position,
								unsigned				offset,
								unsigned				length
								) noexcept;
		bool /* stop */ accept(	const Oscl::Pdu::Fragment&	p,
								unsigned					position
								) noexcept;
		bool /* stop */ accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
								unsigned							position
								) noexcept;
	};
}
}
}
}
}

#endif
