/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_memory_bush_
#define _oscl_memory_bush_

/** */
namespace Oscl {
/** */
namespace Memory {
/** */
namespace Bus {

/** This forward reference represents an identifier for a bus
	mastering device such as a processor or DMA controller.
	The assumption is that each bus master may have a unique
	view of the system address space.
 */
class Master;

/** This interface defines the memory address translation functions
	typically required by drivers in a virtual memory environment
	with multiple bus masters that may have different views of
	the address space (e.g. PCI). 
 */
static void*	virtualToPhysical(void* virt) noexcept;

/** */
static void*	physicalToVirtual(void* phys) noexcept;

/** */
static void*	busToBus(	Master&			source,
							Master&			dest,
							void*			srcAddr
							) noexcept;

}
}
}

#endif
