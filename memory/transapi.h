/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_memory_transapih_
#define _oscl_memory_transapih_

/** */
namespace Oscl {
/** */
namespace Memory {
/** */
namespace Bus {

/** This interface defines the memory address translation functions
	typically required by drivers in a virtual memory environment
	with multiple bus masters that may have different views of
	the address space (e.g. PCI). 
 */
class TranslationApi {
	public:
		/** Shut-up GCC. */
		virtual ~TranslationApi() {}

		/** This operation translates a virtual memory address
			to a physical memory address.
		 */
		virtual void*	virtualToPhysical(void* virt) noexcept=0;

		/** This operation translates a physical memory address
			to a memory address.
		 */
		virtual void*	physicalToVirtual(void* phys) noexcept=0;

		/** This operation translates an address in the current
			processor view of memory to an equivalent address
			as viewed by the peripheral bus master.
		 */
		virtual void*	toMaster(void* address) noexcept=0;

		/** This operation translates an address as viewed by
			the peripheral bus master to an equivalent address
			as viewed by the current processor.
		 */
		virtual void*	fromMaster(void* srcAddr) noexcept=0;
	};

/** This class is used where memory translation is not required.
	This is valid for systems which have both of the following
	characteristics:
	-	Either they have no virtual memory concept (MMU) or in
		which the virtual and physical memory are mapped one to
		one.
	-	All bus masters share a single view of the memory address space.
 */
class NullTranslation : public TranslationApi {
	public:
		/** */
		inline void*	virtualToPhysical(void* virt) noexcept{
			return virt;
			}
		/** */
		inline void*	physicalToVirtual(void* phys) noexcept{
			return phys;
			}
		/** */
		inline void*	toMaster(void* address) noexcept{
			return address;
			}
		/** */
		inline void*	fromMaster(void* address) noexcept{
			return address;
			}
	};
}
}
}

#endif
