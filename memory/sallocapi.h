/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_memory_sallocapih_
#define _oscl_memory_sallocapih_

/** */
namespace Oscl {
/** */
namespace Memory {

/** This interface defines the means by which a client
	with a variable static resource obtains those resources.
	The intent is that an application entity implements this
	interface in such a way that the static blocks are to
	be owned exclusively by a single client.
	The blockType must be a type that has no constructors
	and represents a unit of memory required by the client.
	The intent is that the client, when created will
	repeatedly invoke the getBlock() operation until it
	returns a null pointer. The client then saves the
	blocks in some kind of list within the client and
	uses the individual blocks for its purposes until
	it is destroyed. There is no need for the client
	to release the memory since it belongs to the client
	throughout its lifetime, and is allocated before the
	client is created and freed after the client is
	destroyed.
	This eliminates the potential problems associated 
	with passing a variable sized array to the client,
	caused by a mismatch in the actual size of the
	array and the variable passed along with the array
	representing the size of that array.
	This class also allows higher layer (application)
	configuration entities to specify the amount of
	memory available to the lower layer clients, which
	allows system designers the ability to trade
	performance for memory size and visa-versa without
	changing the lower layer client code.
 */
template <class blockType>
class StaticAllocApi {
	public:
		/** Make GCC happy */
		virtual ~StaticAllocApi() {}
		/** */
		virtual blockType*	getBlock() noexcept=0;
	};

}
}

#endif
