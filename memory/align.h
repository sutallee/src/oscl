/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_memory_alignh_
#define _oscl_memory_alignh_
#include <stdlib.h>

/** */
namespace Oscl {
/** */
namespace Memory {

inline size_t	sizeForAlignment(size_t size,unsigned alignment) noexcept{
	size	+= (alignment-1);
	size	-= (size % alignment);
	return size;
	}

inline void*	roundUpAlign(void* raw,unsigned alignment) noexcept{
	unsigned long	memory	= (unsigned long)raw;
	memory	+= (alignment-1);
	memory	-= memory % alignment;
	raw		= (void*)memory;
	return raw;
	}

/** This operation is defined to aid programs in the
	dynamic allocation of blocks of memory that need
	to be aligned to a boundary higher than that
	provided by the standard malloc implementation.
 */
inline void*	mallocAligned(	void**		raw,
								size_t		size,
								unsigned	alignment
								) noexcept{
	size	+= (alignment-1);
	void*	mem = malloc(size);
	if(!mem) return 0;
	mem	= Oscl::Memory::roundUpAlign(mem,alignment);
	*raw	= mem;
	return mem;
	}

}
}

#endif
