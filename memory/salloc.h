/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_memory_salloch_
#define _oscl_memory_salloch_
#include "sallocapi.h"

/** */
namespace Oscl {
/** */
namespace Memory {

/** This class implements the StaticAllocApi<> by
	declaring the number of allocation records as a
	member variable.
 */
template <class blockType,unsigned nBlocks>
class StaticAlloc : StaticAllocApi<blockType> {
	private:
		/** */
		union Block {
			/** */
			void*		__qitemlink;
			/** */
			blockType	_block;
			};
		/** */
		Block					_block[nBlocks];
		/** */
		Oscl::Queue<Block>		_freeList;
	public:
		/** */
		StaticAlloc() noexcept{
			for(unsigned i=0;i<nBlocks;++i){
				_freeList.put(&_block[i]);
				}
			}
	private:	// StaticAllocApi
		/** */
		blockType*	getBlock() noexcept{
			Block*	block	= _freeList.get();
			if(!block){
				return 0;
				}
			return &block->_block;
			}
	};

}
}

#endif
