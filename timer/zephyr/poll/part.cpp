/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#endif

using namespace Oscl::Timer::Zephyr::Poll;

Part::Part(
	Oscl::Zephyr::Poll::Api&	pollApi
	) noexcept:
		_pollApi(pollApi),
		_timerExpirationApi(0),
		_delayInMs(0),
		_running(false)
		{
	}

void	Part::setExpirationCallback(
			Oscl::Done::Api&	expired
			) noexcept{
	_timerExpirationApi	= &expired;
	}

void	Part::start(
			unsigned long		delayInMs
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"delay: %lu ms\n",
		delayInMs
		);
	#endif

	_delayInMs	= delayInMs;

	restartTimer();
	}

void	Part::stop() noexcept{
	_delayInMs	= 0;
	_running	= false;
	_pollApi.stop(*this);
	}

void	Part::restart(unsigned long delayInMs) noexcept{
	_delayInMs	= delayInMs;

	restartTimer();
	}

void	Part::timeout() noexcept{

	if(!_delayInMs){
		// If the timer expires when _delayInMs is
		// zero, then we notify the timer client.

		if(!_timerExpirationApi){
			// This should NEVER happen.
			// If it does, this is software
			// implementation error.
			#ifdef DEBUG_TRACE
			Oscl::ErrorFatal::logAndExit(
				"%s: software error.\n",
				__PRETTY_FUNCTION__
				);
			#endif
			return;
			}

		_running	= false;
		_timerExpirationApi->done();

		return;
		}

	// The timer has been restarted with a new
	// value. Therefore, we start it again.

	restartTimer();
	}

void	Part::restartTimer() noexcept{

	// The _pollApi.stop() operation can
	// be used even if this timer is not currently
	// running. It *must* be stopped before changing
	// thee _duration value.
	_pollApi.stop(*this);

	_durationInMs	= _delayInMs;

	_delayInMs	= 0;

	_running	= true;

	_pollApi.start(*this);
	}

bool	Part::running() noexcept{
	return _running;
	}
