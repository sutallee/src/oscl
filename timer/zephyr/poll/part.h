/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_timer_zephyr_poll_timerh_
#define _oscl_timer_zephyr_poll_timerh_

#include "oscl/timer/api.h"
#include "oscl/zephyr/poll/api.h"
#include "oscl/zephyr/poll/timer.h"

/** */
namespace Oscl {
/** */
namespace Timer {
/** */
namespace Zephyr {
/** */
namespace Poll {

/** */
class Part :
	public Oscl::Timer::Api,
	private Oscl::Zephyr::Poll::Timer
	{
	private:
		/** */
		Oscl::Zephyr::Poll::Api&	_pollApi;

	private:
		/** */
		Oscl::Done::Api*			_timerExpirationApi;

	private:
		/** If zero, the timer is INACTIVE.
		 */
		unsigned long				_delayInMs;

		/** */
		bool						_running;

	public:
		/** */
		Part(
			Oscl::Zephyr::Poll::Api&	pollApi
			) noexcept;

	public: // Oscl::Timer::Api
		/**	This operation sets the callback for the
			timer expiration. This MUST be invoked
			at least once before the start() or restart()
			operations are invoked. This MAY be
			invoked to change the callback at any time.
		 */
		void	setExpirationCallback(
					Oscl::Done::Api&	expired
					) noexcept;

		/**	When this operation is invoked, the timer is marked
			as ACTIVE if it is not already running. If the timer
			is not already running internally, it will be started
			with the delayInMs value. If the internal timer is
			already running, then the implementation makes a note
			of the new delay value and when the internal timer expires
			it is restarted with the new delay value.

			param [in] delayInMs	The minimum number of milliseconds
									before the timer expires.
		 */
		void	start(
					unsigned long		delayInMs
					) noexcept;

		/** When this operation is invoked, the timer is marked
			as INACTIVE and the client will not get a timer expired
			indication.
		 */
		void	stop() noexcept;

		/**	When this operation is invoked, the timer is marked
			as ACTIVE if it is not already running. If the timer
			is not already running internally, it will be started
			with the delayInMs value. If the internal timer is
			already running, then the implementation makes a note
			of the new delay value and when the internal timer expires
			it is restarted with the new delay value. Note that from
			the client perspective, this behaviour is identical to
			the start() operation behavior.

			param [in] delayInMs	The minimum number of milliseconds
									before the timer expires.
		 */
		void	restart(unsigned long delayInMs) noexcept;

		/** This operation returns true if the timer is
			currently running.
		 */
		bool	running() noexcept;

	private:
		/**	This  operation is invoked by the select service when the timer
			expires. The timer is removed from the select service timer
			list before this operation is invoked. Therefore, it is safe
			for the implementation to re-use this timer.
			The implementation may modify the file-descriptor set that
			will be used in the next select call.
			NOTE: This operation is *always* invoked from within the
			select service thread!
		 */
		void	timeout() noexcept;

	private:
		/** */
		void	restartTimer() noexcept;
	};

}
}
}
}

#endif

