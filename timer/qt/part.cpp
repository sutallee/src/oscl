/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"

//#define DEBUG_TRACE

using namespace Oscl::Timer::Qt;

Part::Part(
	) noexcept:
		_timer(new QTimer(this)),
		_timerExpirationApi(0),
		_delayInMs(0),
		_running(false)
		{

	if(!_timer){
		Oscl::Error::Info::log(
			"%s: Timer allocation failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	connect(
		_timer,
		&QTimer::timeout,
		this,
		&Part::timerExpired
		);
	_timer->setSingleShot(true);

	}

Part::~Part() noexcept{
	/*	Hmmm... this may not be the thing
		to do since QTimer has this as a
		parent.
	 */
	delete _timer;
	}

void	Part::setExpirationCallback(
			Oscl::Done::Api&	expired
			) noexcept{
	_timerExpirationApi	= &expired;
	}

void	Part::start(
			unsigned long		delayInMs
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"delay: %lu ms\n",
		delayInMs
		);
	#endif

	_delayInMs	= delayInMs;

	restartTimer();
	}

void	Part::stop() noexcept{
	_delayInMs	= 0;
	_running	= false;
	_timer->stop();
	}

void	Part::restart(unsigned long delayInMs) noexcept{
	_delayInMs	= delayInMs;

	restartTimer();
	}

void	Part::timerExpired() noexcept{

	if(!_delayInMs){
		// If the timer expires when _delayInMs is
		// zero, then we notify the timer client.

		if(!_timerExpirationApi){
			// This should NEVER happen.
			// If it does, this is software
			// implementation error.

			#ifdef DEBUG_TRACE
			Oscl::ErrorFatal::logAndExit(
				"%s: software error.\n",
				__PRETTY_FUNCTION__
				);
			#endif

			return;
			}

		_running	= false;
		_timerExpirationApi->done();

		return;
		}

	// The timer has been restarted with a new
	// value. Therefore, we start it again.

	restartTimer();
	}

void	Part::restartTimer() noexcept{

	_timer->stop();

	unsigned long
	delayInMs	= _delayInMs;

	_delayInMs	= 0;

	_running	= true;

	_timer->start(delayInMs);
	}

bool	Part::running() noexcept{
	return _running;
	}
