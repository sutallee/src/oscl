/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdlib.h>
#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

using namespace Oscl::Timer::Posix::Select::Factory;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Posix::Select::Api&	posixSelectApi,
	TimerMem*					timerMem,
	unsigned					nTimerMem
	) noexcept:
		_posixSelectApi(posixSelectApi),
		_nDynamicAllocations(0)
		{
	for(unsigned i=0;i<nTimerMem;++i){
		_freeTimerMem.put(&timerMem[i]);
		}
	}


Oscl::Timer::Api*	Part::allocate() noexcept{
	TimerMem*
	mem	= _freeTimerMem.get();
	if(!mem){
		// For a system that requires all memory
		// to be statically allocated, this is a
		// failure indication and the number of
		// memory allocation blocks (nTimerMem)
		// needs to be increased.
		++_nDynamicAllocations;
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: dynamic allocation required _nDynamicAllocations %u.\n",
			__PRETTY_FUNCTION__,
			_nDynamicAllocations
			);
		#endif
		mem	= (TimerMem*)malloc(sizeof(TimerMem));
		}

	if(!mem){
		Oscl::ErrorFatal::logAndExit(
			"%s: out of dynamic memory.\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	Oscl::Timer::Posix::Select::Part*
	timer	= new(&mem->factoryMem.mem)
				Oscl::Timer::Posix::Select::Part(
					_posixSelectApi
					);

	mem->factoryMem.api	= timer;

	_activeFactoryMem.put(&mem->factoryMem);

	return timer;
	}

void	Part::free(Oscl::Timer::Api& timer) noexcept{
	timer.~Api();
	for(
		FactoryMem*	factoryMem	= _activeFactoryMem.first();
		factoryMem;
		factoryMem	= _activeFactoryMem.next(factoryMem)
		){
		if(&timer == factoryMem->api){
			_activeFactoryMem.remove(factoryMem);
			_freeTimerMem.put((TimerMem*)factoryMem);
			return;
			}
		}
	// If I get here, then we have a software
	// error in the client.
	Oscl::ErrorFatal::logAndExit(
		"%s: item not active in list.\n",
		__PRETTY_FUNCTION__
		);
	}

