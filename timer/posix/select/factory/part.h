/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_timer_posix_select_factory_parth_
#define _oscl_timer_posix_select_factory_parth_

#include "oscl/timer/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/timer/posix/select/part.h"
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Timer {
/** */
namespace Posix {
/** */
namespace Select {
/** */
namespace Factory {

/** */
class Part :
	public Oscl::Timer::Factory::Api
	{
	private:
		/** */
		Oscl::Posix::Select::Api&	_posixSelectApi;

	public:
		/** */
		struct FactoryMem {
			/** */
			void*	__qitemlink;

			/** This pointer is used by the factory
				to recover the specific FactoryMem
				that is associtated with this memory
				block as created by the mem member
				of this block.
			 */
			Oscl::Timer::Api*	api;

			/** */
			Oscl::Memory::AlignedBlock<
				sizeof(Oscl::Timer::Posix::Select::Part)
				>											mem;
			};

		/** */
		union TimerMem {
			/** */
			void*	__qitemlink;

			/** */
			FactoryMem	factoryMem;
			};

		/** */
		Oscl::Queue<TimerMem>	_freeTimerMem;

		/** */
		Oscl::Queue<FactoryMem>	_activeFactoryMem;

		/** */
		unsigned				_nDynamicAllocations;

	public:
		/** */
		Part(
			Oscl::Posix::Select::Api&	posixSelectApi,
			TimerMem*					timerMem,
			unsigned					nTimerMem
			) noexcept;

	public: // Oscl::Timer::Factory::Api
		/** RETURN: zero if out of timer resources.
		 */
		Oscl::Timer::Api*	allocate() noexcept;

		/**	*/
		void	free(Oscl::Timer::Api& timer) noexcept;
	};

}
}
}
}
}

#endif

