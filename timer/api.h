/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_timer_apih_
#define _oscl_timer_apih_

#include "oscl/done/api.h"

/** */
namespace Oscl {

/** */
namespace Timer {

/** This interface is an abstraction for an asynchronous
	timer service. The timer behaviour is such that the timer
	expiration *may* be as much as twice the timer value,
	even if the timer is stopped and then started again.
	This property allows for a wide variety of possible timer
	implementations.

	The implementation may use an asynchronous timer
	that may not actually be stopped or restarted
	(by stop() or restart()), but may rather be running
	internal with the previous delay value.

	If the timer has been started by start() or restart(),
	then the timer is considered ACTIVE and an expiration
	will cause a single expiration event to be generated
	through some externally specified mechanism.

	If the timer has been stopped by stop(), the timer is
	considered as INACTIVE. If the internal implementation
	timer expires while in this state, then no expiration
	event will be generated/reported through the externally
	specified mechanism.

	In general, it is expected that the client can cope with
	a delay that is relatively constant and can accept the
	possible two delay time jitter.

	When a new delay value is specified, then the new delay
	*may* not be in effect until the internal timer expires
	with the previous delay value. At this time, the internal
	timer will be restarted with the new delay value and
	the expiration event will not happen until the new delay
	has elapsed.  Again, this allows for a variety of possible
	implementations.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}

		/**	This operation sets the callback for the
			timer expiration. This MUST be invoked
			at least once before the start() or restart()
			operations are invoked. This MAY be
			invoked to change the callback at any time.

			param [in] expired		This is invoked when the timer expires.
		 */
		virtual void	setExpirationCallback(
							Oscl::Done::Api&	expired
							) noexcept=0;

		/**	When this operation is invoked, the timer is marked
			as ACTIVE if it is not already running. If the timer
			is not already running internally, it will be started
			with the delayInMs value. If the internal timer is
			already running, then the implementation makes a note
			of the new delay value and when the internal timer expires
			it is restarted with the new delay value.

			param [in] delayInMs	The minimum number of milliseconds
									before the timer expires.
		 */
		virtual void	start(
							unsigned long		delayInMs
							) noexcept=0;

		/** When this operation is invoked, the timer is marked
			as INACTIVE and the client will not get a timer expired
			indication.
		 */
		virtual void	stop() noexcept=0;

		/**	When this operation is invoked, the timer is marked
			as ACTIVE if it is not already running. If the timer
			is not already running internally, it will be started
			with the delayInMs value. If the internal timer is
			already running, then the implementation makes a note
			of the new delay value and when the internal timer expires
			it is restarted with the new delay value. Note that from
			the client perspective, this behaviour is identical to
			the start() operation behavior.

			param [in] delayInMs	The minimum number of milliseconds
									before the timer expires.
		 */
		virtual void	restart(unsigned long delayInMs) noexcept=0;

		/** This operation returns true if the timer is
			currently running.
		 */
		virtual bool	running() noexcept=0;
	};

}
}

#endif
