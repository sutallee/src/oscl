/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_layer_state_dist_parth_
#define _oscl_ppp_layer_state_dist_parth_
#include "oscl/ppp/layer/state/reqapi.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Layer {
/** */
namespace State {
/** */
namespace Dist {

/** */
class Part : private Oscl::PPP::Layer::State::Req::Api {
	private:
		/** */
		Oscl::PPP::Layer::State::Req::Api::ConcreteSAP	_sap;
		/** */
		Oscl::Queue<	Oscl::PPP::Layer::
						State::Req::Api::ChangeReq
						>								_changeList;
		/** */
		bool											_layerIsUp;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				bool						layerIsUp=false
				) noexcept;

		/** */
		Oscl::PPP::Layer::State::Req::Api::SAP&	getSAP() noexcept;

		/** */
		void	layerUp() noexcept;
		/** */
		void	layerDown() noexcept;

	private:
		/** */
		void	notify() noexcept;

	private:
		/** */
		void	request(	Oscl::PPP::Layer::
							State::Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::PPP::Layer::
							State::Req::Api::CancelReq&	msg
							) noexcept;
	};

}
}
}
}
}

#endif
