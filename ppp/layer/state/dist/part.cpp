/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::PPP::Layer::State::Dist;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			bool						layerIsUp
			) noexcept:
		_sap(*this,myPapi),
		_layerIsUp(layerIsUp)
		{
	}

Oscl::PPP::Layer::State::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::layerUp() noexcept{
	if(_layerIsUp) return;
	_layerIsUp	= true;
	notify();
	}

void	Part::layerDown() noexcept{
	if(!_layerIsUp) return;
	_layerIsUp	= false;
	notify();
	}

void	Part::notify() noexcept{
	Oscl::PPP::Layer::State::Req::Api::ChangeReq*	next;
	while((next=_changeList.get())){
		next->_payload._layerIsUp	= _layerIsUp;
		next->returnToSender();
		}
	}

void	Part::request(	Oscl::PPP::Layer::
						State::Req::Api::ChangeReq&	msg
						) noexcept{
	if(msg._payload._layerIsUp != _layerIsUp){
		msg._payload._layerIsUp	= _layerIsUp;
		msg.returnToSender();
		return;
		}
	_changeList.put(&msg);
	}

void	Part::request(	Oscl::PPP::Layer::
						State::Req::Api::CancelReq&	msg
						) noexcept{
	Oscl::PPP::Layer::State::Req::Api::ChangeReq*
	changeReq	= _changeList.remove(&msg._payload._reqToCancel);
	if(changeReq){
		changeReq->returnToSender();
		}
	msg.returnToSender();
	}

