/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_layer_lcp_txtransh_
#define _oscl_ppp_layer_lcp_txtransh_
#include "oscl/memory/block.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rocomposite.h"
#include "oscl/pdu/rofragment.h"
//#include "oscl/protocol/ip/tx/srv/reqapi.h"
#include "oscl/pdu/tx/api.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Layer {
/** */
namespace LCP {

/** */
class Part;

/** */
class TxTrans :	private Oscl::Frame::Pdu::Tx::Resp::Api,
				private Oscl::PDU::TX::Api,
				private Oscl::FreeStore::Mgr
				{
	private:
		/** */
		enum{maxPppOptionPacketSize=256};
		/** */
		Oscl::Memory::
		AlignedBlock< maxPppOptionPacketSize >			_buffer;
		/** */
		Oscl::Memory::
		AlignedBlock< sizeof(Oscl::Pdu::Fixed) >		_fixedMem;
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Pdu::
								ReadOnlyFragment
								)
						>								_fragMem;
		/** */
		Oscl::Pdu::ReadOnlyComposite					_composite;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::TxPayload		_payload;
		/** */
		Oscl::Frame::Pdu::Tx::Resp::Api::TxResp			_resp;
		/** */
		class Part&										_context;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&			_txSAP;
		/** */
		unsigned										_length;
		/** */
		Oscl::Pdu::Fixed*								_fixed;
		/** */
		Oscl::Pdu::ReadOnlyFragment*					_fragment;

	public:
		/** */
		TxTrans(	Part&									context,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Frame::Pdu::Tx::Req::Api::SAP&	txSAP
					) noexcept;

		/** */
		void		start() noexcept;

	public:	// Oscl::PDU::TX::Api
		/** */
		unsigned	open(unsigned length) noexcept;

		/** */
		unsigned	append(const void* pdu,unsigned length) noexcept;

		/** */
		void		close() noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Tx::Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

}
}
}
}

#endif
