/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_layer_lcp_parth_
#define _oscl_ppp_layer_lcp_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/ppp/layer/state/respmem.h"
#include "oscl/ppp/layer/state/dist/part.h"
#include "oscl/ppp/fsm/opt.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/frame/pdu/rx/respmem.h"
#include "oscl/hdlc/llc/proto/tx/encoder.h"
#include "txtrans.h"
#include "rxtrans.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Layer {
/** */
namespace LCP {

/** */
class Part :	private Oscl::PPP::Layer::State::Resp::Api,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Frame::Pdu::Rx::Resp::Api,
				private Oscl::PPP::FSM::OPT::Context,
				private Oscl::PDU::TX::Api
				{
	private:
		/** */
		friend class TxTrans;
		/** */
		friend class RxTrans;
	private:
		/** */
		enum{	optionTypeMaximumReceiveUnit				= 1,
				optionTypeAsyncCtrlCharMap					= 2,
				optionTypeAuthenticationProtocol			= 3,
				optionTypeQualityProtocol					= 4,
				optionTypeMagicNumber						= 5,
				optionTypeProtocolFieldCompression			= 7,
				optionTypeAddressAndControlFieldCompression	= 8 
				};

	private:
		/** */
		union TxTransMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::
			AlignedBlock< sizeof(TxTrans) >	trans;
			};
		/** */
		union RxTransMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::
			AlignedBlock< sizeof(RxTrans) >	trans;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Frame::Pdu::Rx::Resp::CancelMem			rx;
			/** */
			Oscl::PPP::Layer::State::Resp::CancelRespMem	change;
			};
	private:
		/** */
		enum{restartCountInSeconds=3};
		/** */
		enum{timerValueInSeconds=2};
		/** */
		Oscl::PPP::Layer::
		State::Resp::Api::ChangeResp		_changeResp;
		/** */
		CancelMem							_cancelMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::PPP::Layer::State::Dist::Part	_layerStateDist;
		/** */
		Oscl::PPP::Layer::
		State::Req::Api::SAP&				_lowerLayerSAP;
		/** */
		Oscl::Frame::Pdu::
		Tx::Req::Api::SAP&					_txProtocolSAP;
		/** */
		Oscl::Frame::Pdu::
		Rx::Req::Api::SAP&					_rxProtocolSAP;
		/** */
		Oscl::HDLC::LLC::Proto::TX::Encoder	_llcProtoEncoder;
		/** */
		Oscl::PPP::FSM::
		OPT::Variable						_optFSM;
		/** */
		Oscl::Queue<TxTransMem>				_freeTxTransMem;
		/** */
		Oscl::Queue<RxTransMem>				_freeRxTransMem;
		/** */
		TxTrans*							_currentTxTrans;
		/** Function pointer to next sub-server to asynchronously close
		 */
		void			(Part::*	_nextStep)();
		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*			_openReq;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*			_closeReq;
		/** */
		bool								_open;
		/** */
		bool								_layerActive;
	private:
		unsigned			_mru;
		unsigned			_rxAsyncCharMap;
		unsigned			_txAsyncCharacterMap;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Oscl::PPP::Layer::
				State::Req::Api::SAP&		lowerLayerSAP,
				Oscl::Frame::Pdu::
				Tx::Req::Api::SAP&			txProtocolSAP,
				Oscl::Frame::Pdu::
				Rx::Req::Api::SAP&			rxProtocolSAP,
				unsigned					protocolType,
				RxTransMem					rxTransMem[],
				unsigned					nRxTransMem,
				TxTransMem					txTransMem[],
				unsigned					nTxTransMem
				) noexcept;

		/** */
		Oscl::PPP::Layer::
		State::Req::Api::SAP&	getLayerStateSAP() noexcept;

		/** This operation is to be invoked approximately
			once every second from the myPapi thread.
		 */
		void	oneSecondTick() noexcept;

	private:
		/** */
		void		sendAllRxTrans() noexcept;
		/** */
		void		free(RxTrans& trans) noexcept;
		/** */
		void		free(TxTrans& trans) noexcept;

	private:
		/** */
		void		cancelTxReqs() noexcept;
		/** */
		void		cancelRxReqs() noexcept;
		/** */
		void		cancelChangeReqs() noexcept;
		/** */
		void		closeComplete() noexcept;
	private:
		/** */
		void		done(TxTrans& trans) noexcept;
		/** */
		void		done(RxTrans& trans) noexcept;

	private:
		/** */
		void	maximumReceiveUnitOption(	const unsigned char*	option,
											unsigned				length
											) noexcept;
		/** */
		void	asyncControlCharMapOption(	const unsigned char*	option,
											unsigned				length
									) noexcept;
		/** */
		void	authenticationProtocolOption(	const unsigned char*	option,
												unsigned				length
												) noexcept;
		/** */
		void	qualityProtocolOption(	const unsigned char*	option,
										unsigned				length
										) noexcept;
		/** */
		void	magicNumberOption(	const unsigned char*	option,
									unsigned				length
									) noexcept;
		/** */
		void	protocolFieldCompressionOption(	const unsigned char*	option,
												unsigned				length
												) noexcept;
		/** */
		void	addrCtrlFieldCompOption(	const unsigned char*	option,
											unsigned				length
											) noexcept;
		/** */
		void	mruConfigNAK() noexcept;
		/** */
		void	asyncControlCharMapOptionNAK() noexcept;
		/** */
		void	authenticationProtocolConfigNAK() noexcept;

	private:	// Oscl::PDU::TX::Api
		/** */
		unsigned	open(unsigned length) noexcept;

		/** */
		unsigned	append(const void* pdu,unsigned length) noexcept;

		/** */
		void		close() noexcept;

	private:	// Oscl::PPP::FSM::OPT::Context
		/** */
		void	up() noexcept;
		/** */
		void	down() noexcept;
		/** */
		void	start() noexcept;
		/** */
		void	finished() noexcept;
		/** */
		void	optionReq(	const unsigned char*	option,
							unsigned char			type,
							unsigned				optLen
							) noexcept;
		/** */
		void	buildOptions() noexcept;
		/** */
		void	defaultOptions() noexcept;
		/** */
		void	applyOptions() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Frame::Pdu::Rx::Resp::Api
		/** */
		void	response(Oscl::Frame::Pdu::Rx::Resp::Api::RxResp& msg) noexcept;

		/** */
		void	response(	Oscl::Frame::Pdu::Rx::
							Resp::Api::CancelAllResp&	msg
							) noexcept;

	private:	// Oscl::PPP::Layer::State::Resp::Api
		/** */
		void	response(	Oscl::PPP::Layer::
							State::Resp::Api::ChangeResp& msg
							) noexcept;
		/** */
		void	response(	Oscl::PPP::Layer::
							State::Resp::Api::CancelResp& msg
							) noexcept;
	};

}
}
}
}

#endif
