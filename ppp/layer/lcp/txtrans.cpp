/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "txtrans.h"
#include "part.h"

using namespace Oscl::PPP::Layer::LCP;

TxTrans::TxTrans(	Part&							context,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Frame::Pdu::Tx::
					Req::Api::SAP&					txSAP
					) noexcept:
		_composite(*this),
		_payload(_composite),
		_resp(	txSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_context(context),
		_txSAP(txSAP),
		_length(0),
		_fixed(0),
		_fragment(0)
		{
	}

void		TxTrans::start() noexcept{
	_fixed	= new(&_fixedMem)
				Oscl::Pdu::Fixed(	*this,
									*this,
									&_buffer,
									sizeof(_buffer),
									_length
									);
	_fragment	= new (&_fragMem)
					Oscl::Pdu::ReadOnlyFragment(	*this,
													_fixed
													);
	_composite.append(_fragment);
	_txSAP.post(_resp.getSrvMsg());
	}

unsigned	TxTrans::open(unsigned length) noexcept{
	if(length > sizeof(_buffer)){
		return sizeof(_buffer);
		}
	return length;
	}

unsigned	TxTrans::append(const void* pdu,unsigned length) noexcept{
	unsigned	remaining	= sizeof(_buffer)-_length;
	if(length > remaining){
		length	= remaining;
		}
	unsigned char*	p	= (unsigned char*)&_buffer;
	memcpy(&p[_length],pdu,length);
	_length	+= length;
	return length;
	}

void		TxTrans::close() noexcept{
	// let start() actually send the PDU.
	}

void	TxTrans::response(	Oscl::Frame::Pdu::
							Tx::Resp::Api::TxResp&	msg
							) noexcept{
	_context.done(*this);
	}

void	TxTrans::free(void* fso) noexcept{
	// do nothing
	}

