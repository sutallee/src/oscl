/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/protocol/iana/ppp.h"

using namespace Oscl::PPP::Layer::LCP;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Oscl::PPP::Layer::
			State::Req::Api::SAP&		lowerLayerSAP,
			Oscl::Frame::Pdu::
			Tx::Req::Api::SAP&			txProtocolSAP,
			Oscl::Frame::Pdu::
			Rx::Req::Api::SAP&			rxProtocolSAP,
			unsigned					protocolType,
			RxTransMem					rxTransMem[],
			unsigned					nRxTransMem,
			TxTransMem					txTransMem[],
			unsigned					nTxTransMem
			) noexcept:
		_changeResp(	lowerLayerSAP.getReqApi(),
						*this,
						myPapi
						),
		_myPapi(myPapi),
		_layerStateDist(myPapi,false),
		_lowerLayerSAP(lowerLayerSAP),
		_txProtocolSAP(txProtocolSAP),
		_rxProtocolSAP(rxProtocolSAP),
		_llcProtoEncoder(	*this,
							protocolType
							),
		_optFSM(	*this,
					_llcProtoEncoder,
					restartCountInSeconds,
					timerValueInSeconds
					),
		_currentTxTrans(0),
		_openReq(0),
		_closeReq(0),
		_open(false),
		_layerActive(false)
		{
	_changeResp.getSrvMsg()._payload._layerIsUp	= false;
	}

Oscl::PPP::Layer::State::Req::Api::SAP&	Part::getLayerStateSAP() noexcept{
	return _layerStateDist.getSAP();
	}

void	Part::oneSecondTick() noexcept{
	if(_open){
		_optFSM.oneSecondTick();
		}
	}

void		Part::sendAllRxTrans() noexcept{
	RxTransMem*	next;
	while((next=_freeRxTransMem.get())){
		new (next) RxTrans(	*this,
							_myPapi,
							_rxProtocolSAP
							);
		}
	}

void		Part::free(RxTrans& trans) noexcept{
	trans.~RxTrans();
	_freeRxTransMem.put((RxTransMem*)&trans);
	}

void		Part::free(TxTrans& trans) noexcept{
	trans.~TxTrans();
	_freeTxTransMem.put((TxTransMem*)&trans);
	}

void		Part::cancelRxReqs() noexcept{
	Oscl::Frame::Pdu::Rx::Resp::Api::CancelAllResp*
	resp	= new(&_cancelMem.rx.response)
				Oscl::Frame::Pdu::Rx::
				Resp::Api::CancelAllResp(	_rxProtocolSAP.getReqApi(),
											*this,
											_myPapi
											);
	_rxProtocolSAP.post(resp->getSrvMsg());
	// FIXME: finalize order
	_nextStep	= &Oscl::PPP::Layer::LCP::Part::cancelChangeReqs;
	}

void		Part::cancelChangeReqs() noexcept{
	Oscl::PPP::Layer::State::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.change.payload)
				Oscl::PPP::Layer::State::
				Req::Api::CancelPayload(_changeResp.getSrvMsg());

	Oscl::PPP::Layer::State::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.change.resp)
				Oscl::PPP::Layer::State::
				Resp::Api::CancelResp(	_lowerLayerSAP.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_lowerLayerSAP.post(resp->getSrvMsg());
	// FIXME: finalize order
	_nextStep	= &Oscl::PPP::Layer::LCP::Part::closeComplete;
	}

void		Part::closeComplete() noexcept{
	_nextStep	= 0;
	_closeReq->returnToSender();
	_closeReq	= 0;
	_open		= false;
	}

void		Part::done(TxTrans& trans) noexcept{
	trans.~TxTrans();
	_freeTxTransMem.put((TxTransMem*)&trans);
	}

void		Part::done(RxTrans& trans) noexcept{
	if(_closeReq){
		free(trans);
		return;
		}
	if(!trans._payload._handle){
		free(trans);
		return;
		}
	// FIXME:
	// 1. send data to FSM (copy to buffer first?)
	//    using visitor copy.
	for(;;);
	// 2. free msg
	// 3. transmit all rx requests
	free(trans);
	sendAllRxTrans();
	}

unsigned	Part::open(unsigned length) noexcept{
	TxTransMem*	mem = _freeTxTransMem.get();
	if(!mem) {
		return 0;
		}
	_currentTxTrans	= new (mem)
						TxTrans(	*this,
									_myPapi,
									_txProtocolSAP
									);
	// This layer doesn't add any overhead
	// its just an allocation layer.
	return _currentTxTrans->open(length);
	}

unsigned	Part::append(const void* pdu,unsigned length) noexcept{
	if(!_currentTxTrans){
		return 0;
		}
	return _currentTxTrans->append(pdu,length);
	}

void		Part::close() noexcept{
	if(!_currentTxTrans){
		return;
		}
	_currentTxTrans->close();
	_currentTxTrans->start();
	_currentTxTrans	= 0;
	}

void	Part::up() noexcept{
	_layerStateDist.layerUp();
	}

void	Part::down() noexcept{
	_layerStateDist.layerDown();
	}

void	Part::start() noexcept{
	// FIXME:
	// This is called in response to FSM tls().
	// This is where we need to send a ChangeResp
	// and return OpenReq. However, tls() is also
	// invoked by a down event to the FSM?
	for(;;);
	_layerActive	= true;
	}

void	Part::finished() noexcept{
	// FIXME:
	// Called by opt.cpp on tlf().
	// TLF indicates that the lower layer
	// is not needed by this layer.
	// This is probably where we should cancel
	// the pending ChangeResp.
	_layerActive	= false;
	if(_closeReq){
		cancelRxReqs();
		}
	}

void	Part::maximumReceiveUnitOption(	const unsigned char*	option,
										unsigned				length
										) noexcept{
	unsigned	mru;
	if(length != 2){
		mruConfigNAK();
		return;
		}
	mru	= option[0];
	mru	<<=	8;
	mru	&=	0xFF00;
	mru	|=	option[1];
	// Not currently negotiable
	if(mru < _mru){
		mruConfigNAK();
		}
	}

void	Part::asyncControlCharMapOption(	const unsigned char*	option,
										unsigned				length
										) noexcept{
	unsigned long	map;
	if(length != 4){
		asyncControlCharMapOptionNAK();
		return;
		}
	map	=	option[0];
	map	<<= 8;
	map	|=	option[1];
	map	<<= 8;
	map	|=	option[2];
	map	<<= 8;
	map	|=	option[3];
	_rxAsyncCharMap	= map;
	}

void	Part::authenticationProtocolOption(	const unsigned char*	option,
											unsigned				length
											) noexcept{
	if(length < 4){
		authenticationProtocolConfigNAK();
		return;
		}
	unsigned	protocol;
	protocol	=	option[0];
	protocol	<<=	8;
	protocol	|=	option[1];
	if(protocol != Oscl::Protocol::IANA::PPP::ChallengeHandshakeAuthenticationProtocol){
		_optFSM.appendConfigREJ(	optionTypeAuthenticationProtocol,
									length,
									option
									);
		}
	}

void	Part::qualityProtocolOption(	const unsigned char*	option,
										unsigned				length
										) noexcept{
	_optFSM.appendConfigREJ(	optionTypeQualityProtocol,
								length,
								option
								);
	}

void	Part::magicNumberOption(	const unsigned char*	option,
									unsigned				length
									) noexcept{
	_optFSM.appendConfigREJ(	optionTypeMagicNumber,
								length,
								option
								);
	}

void	Part::protocolFieldCompressionOption(	const unsigned char*	option,
												unsigned				length
												) noexcept{
	_optFSM.appendConfigREJ(	optionTypeProtocolFieldCompression,
								length,
								option
								);
	}

void	Part::addrCtrlFieldCompOption(	const unsigned char*	option,
										unsigned				length
										) noexcept{
	_optFSM.appendConfigREJ(	optionTypeAddressAndControlFieldCompression,
								length,
								option
								);
	}

void	Part::mruConfigNAK() noexcept{
	unsigned char	buffer[2];
	buffer[0]	= (unsigned char)((_mru >> 8) & 0x000000FF);
	buffer[1]	= (unsigned char)((_mru >> 0) & 0x000000FF);
	_optFSM.appendConfigNAK(	optionTypeMaximumReceiveUnit,
								2,
								buffer
								);
	}

void	Part::asyncControlCharMapOptionNAK() noexcept{
	unsigned char	buffer[4];
	buffer[0]	= (unsigned char)((_txAsyncCharacterMap >> 24) & 0x00FF);
	buffer[1]	= (unsigned char)((_txAsyncCharacterMap >> 16) & 0x00FF);
	buffer[2]	= (unsigned char)((_txAsyncCharacterMap >>  8) & 0x00FF);
	buffer[3]	= (unsigned char)((_txAsyncCharacterMap >>  0) & 0x00FF);
	_optFSM.appendConfigNAK(	optionTypeAsyncCtrlCharMap,
								4,
								buffer
								);
	}

void	Part::authenticationProtocolConfigNAK() noexcept{
		using namespace Oscl::Protocol::IANA::PPP;
		unsigned char	buffer[2];
		buffer[0]	= (unsigned char)
					((ChallengeHandshakeAuthenticationProtocol >> 8) & 0x0FF);
		buffer[1]	= (unsigned char)
					((ChallengeHandshakeAuthenticationProtocol >> 0) & 0x0FF);
		_optFSM.appendConfigNAK(	optionTypeAuthenticationProtocol,
									2,
									buffer
									);
	}

void	Part::optionReq(	const unsigned char*	option,
							unsigned char			type,
							unsigned				optLen
							) noexcept{
	// FIXME:
	// Called for individual
	// options in a configure request

	// This function is to determine the acceptablility
	// of the option and invoke either:
	// appendConfigNAK(),appendConfigNAK(),or appendConfigREJ().
	switch(type){
		case optionTypeMaximumReceiveUnit:
			maximumReceiveUnitOption(option,optLen);
			break;
		case optionTypeAsyncCtrlCharMap:
			asyncControlCharMapOption(option,optLen);
			break;
		case optionTypeAuthenticationProtocol:
			authenticationProtocolOption(option,optLen);
			break;
		case optionTypeQualityProtocol:
			qualityProtocolOption(option,optLen);
			break;
		case optionTypeMagicNumber:
			magicNumberOption(option,optLen);
			break;
		case optionTypeProtocolFieldCompression:
			protocolFieldCompressionOption(option,optLen);
			break;
		case optionTypeAddressAndControlFieldCompression:
			addrCtrlFieldCompOption(option,optLen);
			break;
		default:
			_optFSM.appendConfigREJ(	type,
										optLen,
										option
										);
			break;
		}
	}

void	Part::buildOptions() noexcept{
	// FIXME:
	for(;;);
#if 0
	unsigned char	buffer[4];
#endif
#if 0
	// MRU field
	buffer[0]	= (unsigned char)((_mru >> 8)& 0x00FF);
	buffer[1]	= (unsigned char)((_mru >> 0)& 0x00FF);
	_optFSM.appendOption(	optionTypeMaximumReceiveUnit,
							buffer,
							2
							);
#endif
#if 0
	// Async character map
	buffer[0]	= (unsigned char)((_txAsyncCharacterMap >> 24)& 0x00FF);
	buffer[1]	= (unsigned char)((_txAsyncCharacterMap >> 16)& 0x00FF);
	buffer[2]	= (unsigned char)((_txAsyncCharacterMap >>  8)& 0x00FF);
	buffer[3]	= (unsigned char)((_txAsyncCharacterMap >>  0)& 0x00FF);
	_optFSM.appendOption(	optionTypeAsyncCtrlCharMap,
							buffer,
							4
							);
#endif
#if 0
	// CHAP Authentication
	buffer[0]	= 0xC2;
	buffer[1]	= 0x23;
	buffer[2]	= 0x05;
	_optFSM.appendOption(	optionTypeAuthenticationProtocol,
							buffer,
							3
							);
#endif
	}

void	Part::defaultOptions() noexcept{
	// FIXME:
	for(;;);
#if 0
	_txAsyncCharacterMap	= 0;
	_rxAsyncCharacterMap	= 0;
	_context.setTxAsyncCharMap(~0);
	_context.setRxAsyncCharMap(~0);
	_mru					= 1500;
#endif
	}

void	Part::applyOptions() noexcept{
	// FIXME:
	for(;;);
#if 0
	_context.setTxAsyncCharMap(_txAsyncCharacterMap);
	_context.setRxAsyncCharMap(_rxAsyncCharacterMap);
#endif
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	if(_open){
		for(;;);
		}
	_open	= true;
	sendAllRxTrans();
	_changeResp.getSrvMsg()._payload._layerIsUp	= false;
	_lowerLayerSAP.post(_changeResp.getSrvMsg());
	_optFSM.open();
	msg.returnToSender();
	_openReq	= 0;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	bool	layerActive	= _layerActive;
	_closeReq	= &msg;
	// FIXME: first need to cancel ChangeResp and Frame::ReadReq
	_optFSM.close();
	if(layerActive && !_layerActive){
		return;
		}
	}

void	Part::response(	Oscl::Frame::Pdu::Rx::
						Resp::Api::RxResp&		msg
						) noexcept{
	// This should NEVER Happen
	for(;;);
	}

void	Part::response(	Oscl::Frame::Pdu::Rx::
						Resp::Api::CancelAllResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

void	Part::response(	Oscl::PPP::Layer::
						State::Resp::Api::ChangeResp& msg
						) noexcept{
	if(_closeReq){
		return;
		}
	if(msg.getSrvMsg()._payload._layerIsUp){
		_optFSM.up();
		}
	else {
		_optFSM.down();
		}
	_lowerLayerSAP.post(msg.getSrvMsg());
	}

void	Part::response(	Oscl::PPP::Layer::
						State::Resp::Api::CancelResp&	msg
						) noexcept{
	(this->*_nextStep)();
//	_optFSM.down();
//	_closeReq->returnToSender();
//	_closeReq	= 0;
	}

