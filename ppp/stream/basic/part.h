/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_stream_basic_parth_
#define _oscl_ppp_stream_basic_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/ppp/stream/rx/basic/part.h"
#include "oscl/ppp/stream/tx/basic/part.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Stream {
/** */
namespace Basic {

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Resp::Api
				{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::Mem						_openCloseMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::PPP::Stream::RX::Part				_streamRx;
		/** */
		Oscl::PPP::Stream::TX::Part				_streamTx;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP			_closeSAP;
		/** Function pointer to next sub-server to asynchronously close
		 */
		void			(Part::*	_nextStep)();
		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*				_openReq;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			pppPapi,
				Oscl::Mt::Itc::PostMsgApi&			txFramerPapi,
				Oscl::Mt::Itc::PostMsgApi&			rxFramerPapi,
				Oscl::Stream::Input::Req::Api::SAP&	rxStreamSAP,
				Oscl::Stream::Output::Api&			txStream,
				Oscl::Stream::Framer::
				RX::Srv::ReadTransMem				readMem[],
				void*								streamBufferMem,
				unsigned							nReads,
				unsigned							streamBufferSize,
				Oscl::Frame::Stream::
				RX::PDU::CompositeMem				compositeMem[],
				unsigned							nCompositeMem,
				Oscl::Frame::Stream::
				RX::PDU::FragmentMem				fragmentMem[],
				Oscl::Frame::Stream::
				RX::PDU::FixedMem					fixedMem[],
				unsigned							nFragments,
				void*								pduBufferMem,
				unsigned							pduBufferSize
				) noexcept;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&		getCloseSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Rx::
		ProtoMux::Req::Api::SAP&	getProtoMuxSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Rx::
		ProtoMux::SyncApi&			getProtoMuxSyncApi() noexcept;
	
		/** */
		Oscl::Frame::Pdu::
		Tx::Req::Api::SAP&			getProtoTxSAP() noexcept;

	private:
		/** */
		void	openTxFramer() noexcept;
		/** */
		void	openRxFramer() noexcept;
		/** */
		void	openComplete() noexcept;
		/** */
		void	closeTxFramer() noexcept;
		/** */
		void	closeRxFramer() noexcept;
		/** */
		void	closeComplete() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	};

}
}
}
}

#endif
