/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::PPP::Stream::Basic;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			pppPapi,
			Oscl::Mt::Itc::PostMsgApi&			txFramerPapi,
			Oscl::Mt::Itc::PostMsgApi&			rxFramerPapi,
			Oscl::Stream::Input::Req::Api::SAP&	rxStreamSAP,
			Oscl::Stream::Output::Api&			txStream,
			Oscl::Stream::Framer::
			RX::Srv::ReadTransMem				readMem[],
			void*								streamBufferMem,
			unsigned							nReads,
			unsigned							streamBufferSize,
			Oscl::Frame::Stream::
			RX::PDU::CompositeMem				compositeMem[],
			unsigned							nCompositeMem,
			Oscl::Frame::Stream::
			RX::PDU::FragmentMem				fragmentMem[],
			Oscl::Frame::Stream::
			RX::PDU::FixedMem					fixedMem[],
			unsigned							nFragments,
			void*								pduBufferMem,
			unsigned							pduBufferSize
			) noexcept:
		_myPapi(pppPapi),
		_streamRx(	rxFramerPapi,
					rxStreamSAP,
					readMem,
					streamBufferMem,
					nReads,
					streamBufferSize,
					compositeMem,
					nCompositeMem,
					fragmentMem,
					fixedMem,
					nFragments,
					pduBufferMem,
					pduBufferSize
					),
		_streamTx(	txFramerPapi,
					txStream
					),
		_closeSAP(*this,pppPapi),
		_nextStep(0),
		_openReq(0),
		_closeReq(0)
		{
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Rx::ProtoMux::Req::Api::SAP&	Part::getProtoMuxSAP() noexcept{
	return _streamRx.getProtoMuxSAP();
	}

Oscl::Frame::Pdu::Rx::ProtoMux::SyncApi&	Part::getProtoMuxSyncApi() noexcept{
	return _streamRx.getProtoMuxSyncApi();
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Part::getProtoTxSAP() noexcept{
	return _streamTx.getProtoTxSAP();
	}

void	Part::openTxFramer() noexcept{
	Oscl::Mt::Itc::Srv::
	Open::Req::Api::OpenPayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();
	Oscl::Mt::Itc::Srv::
	Open::Resp::Api::OpenResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::
				OpenResp(	_streamTx.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_streamTx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::openRxFramer;
	}

void	Part::openRxFramer() noexcept{
	Oscl::Mt::Itc::Srv::
	Open::Req::Api::OpenPayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();
	Oscl::Mt::Itc::Srv::
	Open::Resp::Api::OpenResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::
				OpenResp(	_streamRx.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_streamRx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::openComplete;
	}

void	Part::openComplete() noexcept{
	_openReq->returnToSender();
	}

void	Part::closeTxFramer() noexcept{
	Oscl::Mt::Itc::Srv::
	Close::Req::Api::ClosePayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Close::Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::
	Close::Resp::Api::CloseResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Close::Resp::Api::
				CloseResp(	_streamTx.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_streamTx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::closeComplete;
	}

void	Part::closeRxFramer() noexcept{
	Oscl::Mt::Itc::Srv::
	Close::Req::Api::ClosePayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Close::Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::
	Close::Resp::Api::CloseResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Close::Resp::Api::
				CloseResp(	_streamRx.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_streamRx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::closeTxFramer;
	}

void	Part::closeComplete() noexcept{
	_closeReq->returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	openTxFramer();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	closeRxFramer();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Open::Resp::Api::OpenResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Close::Resp::Api::CloseResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

