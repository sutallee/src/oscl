/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_stream_tx_basic_parth_
#define _oscl_ppp_stream_tx_basic_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/frame/stream/tx/pdu/part.h"
#include "oscl/stream/hdlc/tx/framer.h"
#include "oscl/hdlc/mac/tx/encoder.h"
#include "oscl/hdlc/llc/cmd/ppp/tx/encoder.h"
#include "oscl/hdlc/llc/proto/tx/encoder.h"
#include "oscl/stream/hdlc/fcs/fcs16.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Stream {
/** */
namespace TX {

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Resp::Api
				{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::Mem						_openCloseMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP			_closeSAP;
		/** Function pointer to next sub-server to asynchronously close
		 */
		void			(Part::*	_nextStep)();
		/** */
		Oscl::Stream::HDLC::FCS16::Accumulator	_fcs;
		/** */
		Oscl::Stream::HDLC::TX::Framer			_framer;
		/** */
		Oscl::HDLC::MAC::TX::Encoder			_macEncoder;
		/** */
		Oscl::HDLC::LLC::CMD::PPP::TX::Encoder	_llcCmdEncoder;
		/** This will be done at the IP IF layer */
//		Oscl::HDLC::LLC::Proto::TX::Encoder		_llcProtoEncoder;
		/** */
		Oscl::Frame::Stream::TX::PDU::Part		_streamTx;
		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*				_openReq;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Stream::Output::Api&		streamOut
				) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&		getCloseSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::
		Tx::Req::Api::SAP&			getProtoTxSAP() noexcept;

	private:
		/** */
		void	openStreamTx() noexcept;
		/** */
		void	openComplete() noexcept;
		/** */
		void	closeStreamTx() noexcept;
		/** */
		void	closeComplete() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	};

}
}
}
}

#endif
