/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::PPP::Stream::RX;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
			Oscl::Stream::Framer::
			RX::Srv::ReadTransMem				readMem[],
			void*								streamBufferMem,
			unsigned							nReads,
			unsigned							streamBufferSize,
			Oscl::Frame::Stream::
			RX::PDU::CompositeMem				compositeMem[],
			unsigned							nCompositeMem,
			Oscl::Frame::Stream::
			RX::PDU::FragmentMem				fragmentMem[],
			Oscl::Frame::Stream::
			RX::PDU::FixedMem					fixedMem[],
			unsigned							nFragments,
			void*								pduBufferMem,
			unsigned							pduBufferSize
			) noexcept:
		_myPapi(myPapi),
		_closeSAP(*this,myPapi),
		_nextStep(0),
		_llcCmdDecoder(*this),
		_macLayer(),
		_macAddrEntry(	_llcCmdDecoder,
						0xFF	// PPP Mac Address
						),
		_streamRx(	myPapi,
					streamSAP,
					_macLayer.getFrameReceiver(),
					readMem,
					streamBufferMem,
					nReads,
					streamBufferSize,
					compositeMem,
					nCompositeMem,
					fragmentMem,
					fixedMem,
					nFragments,
					pduBufferMem,
					pduBufferSize
					),
		_protoMuxSAP(*this,myPapi),
		_sync(_protoMuxSAP),
		_openReq(0),
		_closeReq(0)
		{
	_macLayer.getAddressMux().attach(_macAddrEntry);
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Rx::
ProtoMux::Req::Api::SAP&	Part::getProtoMuxSAP() noexcept{
	return _protoMuxSAP;
	}

Oscl::Frame::Pdu::Rx::
ProtoMux::SyncApi&			Part::getProtoMuxSyncApi() noexcept{
	return _sync;
	}

void	Part::openStreamRx() noexcept{
	Oscl::Mt::Itc::Srv::
	Open::Req::Api::OpenPayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();
	Oscl::Mt::Itc::Srv::
	Open::Resp::Api::OpenResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::OpenResp(	_streamRx.getCloseSAP().getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_streamRx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::RX::Part::openComplete;
	}

void	Part::openComplete() noexcept{
	_openReq->returnToSender();
	}

void	Part::closeStreamRx() noexcept{
	Oscl::Mt::Itc::Srv::
	Close::Req::Api::ClosePayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Close::Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::
	Close::Resp::Api::CloseResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Close::Resp::Api::CloseResp(	_streamRx.getCloseSAP().getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_streamRx.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::RX::Part::closeComplete;
	}

void	Part::closeComplete() noexcept{
	_closeReq->returnToSender();
	}

void	Part::transfer(Oscl::Pdu::Pdu* pdu) noexcept{
	unsigned char	buffer[2];
	unsigned		protocolType;
	pdu->read(buffer,2,0);
	if(!(buffer[0] & 0x01)){
		protocolType	=	buffer[0];
		protocolType	<<=	8;
		protocolType	|=	buffer[1];
		pdu->stripHeader(2);
		}
	else{
		protocolType	=	buffer[0];
		pdu->stripHeader(1);
		}
	// At this point I have a PPP PDU with the protocol
	// field header intact ready to be de-multiplexed.
	// This is where I extract the protocol type and
	// feed it into the protomux.
	Oscl::Frame::Pdu::Rx::ProtoMux::Api*	next;
	for(	next=_protoMuxList.first();
			next;
			next=_protoMuxList.next(next)
			){
		if(next->accept(pdu,protocolType)){
			return;
			}
		}
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	openStreamRx();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	closeStreamRx();
	}

void	Part::request(	Oscl::Frame::Pdu::Rx::
						ProtoMux::Req::Api::AttachReq&	msg
						) noexcept{
	_protoMuxList.put(&msg._payload._protocolMux);
	msg.returnToSender();
	}

void	Part::request(	Oscl::Frame::Pdu::Rx::
						ProtoMux::Req::Api::DetachReq&	msg
						) noexcept{
	_protoMuxList.remove(&msg._payload._protocolMux);
	msg.returnToSender();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Open::Resp::Api::OpenResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Close::Resp::Api::CloseResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

