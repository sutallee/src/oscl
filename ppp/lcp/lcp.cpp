/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "lcp.h"
#include "oscl/protocol/iana/ppp.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace LCP {

/** */
enum{
	/** */
	optionMaximumReceiveUnit				= 1,
	/** */
	optionAsyncControlCharacterMap			= 2,
	/** */
	optionAuthenticationProtocol			= 3,
	/** */
	optionQualityProtocol					= 4,
	/** */
	optionMagicNumber						= 5,
	/** */
	optionProtocolFieldCompression			= 7,
	/** */
	optionAddressAndControlFieldCompression	= 8
	};

}
}
}

using namespace Oscl::PPP::LCP;
using namespace Oscl::Protocol::IANA::PPP;

Protocol::Protocol(	LCP::Context&		context,
					Oscl::PDU::TX::Api&	transmitter,
					unsigned			restartCountInSeconds,
					unsigned			timerValueInSeconds
					) noexcept:
		_optionFSM(	*this,
					transmitter,
					restartCountInSeconds,
					timerValueInSeconds
					),
		_context(context),
		_asyncCharMapTx(~0),
		_asyncCharMapRx(~0)
		{
	}

void	Protocol::lowerLayerUp() noexcept{
	_optionFSM.up();
	}

void	Protocol::lowerLayerDown() noexcept{
	_optionFSM.down();
	}

void	Protocol::open() noexcept{
	_optionFSM.open();
	}

void	Protocol::close() noexcept{
	_optionFSM.close();
	}

void	Protocol::oneSecondTick() noexcept{
	_optionFSM.oneSecondTick();
	}

void	Protocol::rx(	const void*		pdu,
						unsigned		length
						) noexcept{
	_optionFSM.rx(pdu,length);
	}

void	Protocol::transfer(Oscl::Pdu::Pdu* pdu) noexcept{

	unsigned
	length	= pdu->read(
				_pduCopy,
				maxPduSize, //max
				0			// offset
				);

	rx(_pduCopy,length);
	}

void	Protocol::up() noexcept{
	_context.up();
	}

void	Protocol::down() noexcept{
	_context.down();
	}

void	Protocol::start() noexcept{
	_context.start();
	}

void	Protocol::finished() noexcept{
	_context.finished();
	}

void	Protocol::optionReq(	const unsigned char*	option,
								unsigned char			type,
								unsigned				optLen
								) noexcept{
	switch(type){
		case optionMaximumReceiveUnit:
			mruOption(option,optLen);
			break;
		case optionAsyncControlCharacterMap:
			asyncCharMapOption(option,optLen);
			break;
		case optionAuthenticationProtocol:
			authProtocolOption(option,optLen);
			break;
		case optionQualityProtocol:
			qualityProtocolOption(option,optLen);
			break;
		case optionMagicNumber:
			magicNumberOption(option,optLen);
			break;
		case optionProtocolFieldCompression:
			protoFieldCompOption(option,optLen);
			break;
		case optionAddressAndControlFieldCompression:
			addrCtrlFieldCompOption(option,optLen);
			break;
		default:
			_optionFSM.appendConfigREJ(type,optLen,option);
			break;
		};
	}

void	Protocol::buildOptions() noexcept{
	unsigned char	buffer[4];

	buffer[0]	= (unsigned char)(_mru >> 8);
	buffer[1]	= (unsigned char)(_mru >> 0);

	_optionFSM.appendOption(optionMaximumReceiveUnit,buffer,2);

	buffer[0]	=	(unsigned char)(_asyncCharMapTx >> 24);
	buffer[1]	=	(unsigned char)(_asyncCharMapTx >> 16);
	buffer[2]	=	(unsigned char)(_asyncCharMapTx >>  8);
	buffer[3]	=	(unsigned char)(_asyncCharMapTx >>  0);

	_optionFSM.appendOption(optionAsyncControlCharacterMap,buffer,4);

	buffer[0]	=	(unsigned char)
						(ChallengeHandshakeAuthenticationProtocol >> 8);
	buffer[1]	=	(unsigned char)
						(ChallengeHandshakeAuthenticationProtocol >> 0);
	buffer[2]	=	(unsigned char)(5);	// MD5

	_optionFSM.appendOption(optionAuthenticationProtocol,buffer,3);
	}

void	Protocol::defaultOptions() noexcept{
	_asyncCharMapTx	= 0;
	_asyncCharMapRx	= 0;
	_mru			= 1500;
	_context.setAsyncCharMapTx(~0);
	_context.setAsyncCharMapRx(~0);
	}

void	Protocol::applyOptions() noexcept{
	_context.setAsyncCharMapTx(_asyncCharMapTx);
	_context.setAsyncCharMapRx(_asyncCharMapRx);
	}

void	Protocol::mruOption(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	unsigned	mru;
	if(length != 2){
		mruConfigNAK();
		return;
		}

	mru	=	pdu[0];
	mru	<<=	8;
	mru	|=	pdu[1];

	if(mru < _mru){
		mruConfigNAK();
		}
	}

void	Protocol::asyncCharMapOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept{
	unsigned long	asyncMap;
	if(length != 4){
		asyncCharMapConfigNAK();
		return;
		}

	asyncMap	=	pdu[0];
	asyncMap	<<=	8;
	asyncMap	|=	pdu[1];
	asyncMap	<<=	8;
	asyncMap	|=	pdu[2];
	asyncMap	<<=	8;
	asyncMap	|=	pdu[3];

	_asyncCharMapRx	= asyncMap;
	}

void	Protocol::authProtocolOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept{
	unsigned	protocol;
	if(length < 4){
		authProtocolConfigNAK();
		return;
		}
	protocol	=	pdu[0];
	protocol	<<=	8;
	protocol	|=	pdu[1];

	if(protocol != ChallengeHandshakeAuthenticationProtocol){
		_optionFSM.appendConfigREJ(	optionAuthenticationProtocol,
									length,
									pdu
									);
		}
	}

void	Protocol::qualityProtocolOption(	const unsigned char*	pdu,
											unsigned				length
											) noexcept{
	_optionFSM.appendConfigREJ(	optionQualityProtocol,
								length,
								pdu
								);
	}

void	Protocol::magicNumberOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept{
	_optionFSM.appendConfigREJ(	optionMagicNumber,
								length,
								pdu
								);
	}

void	Protocol::protoFieldCompOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept{
	_optionFSM.appendConfigREJ(	optionProtocolFieldCompression,
								length,
								pdu
								);
	}

void	Protocol::addrCtrlFieldCompOption(	const unsigned char*	pdu,
											unsigned				length
											) noexcept{
	_optionFSM.appendConfigREJ(	optionAddressAndControlFieldCompression,
								length,
								pdu
								);
	}

void	Protocol::mruConfigNAK() noexcept{
	unsigned char	buffer[2];
	buffer[0]	= (unsigned char)(_mru >> 8);
	buffer[1]	= (unsigned char)(_mru >> 0);
	_optionFSM.appendConfigNAK(	optionMaximumReceiveUnit,
								2,
								buffer
								);
	}

void	Protocol::asyncCharMapConfigNAK() noexcept{
	unsigned char	buffer[4];

	buffer[0]	= (unsigned char)(_asyncCharMapTx >> 24);
	buffer[1]	= (unsigned char)(_asyncCharMapTx >> 16);
	buffer[2]	= (unsigned char)(_asyncCharMapTx >>  8);
	buffer[3]	= (unsigned char)(_asyncCharMapTx >>  0);

	_optionFSM.appendConfigNAK(	optionAsyncControlCharacterMap,
								4,
								buffer
								);
	}

void	Protocol::authProtocolConfigNAK() noexcept{
	unsigned char	buffer[2];

	buffer[0]	= (unsigned char)
					(ChallengeHandshakeAuthenticationProtocol >>  8);
	buffer[1]	= (unsigned char)
					(ChallengeHandshakeAuthenticationProtocol >>  0);
	_optionFSM.appendConfigNAK(	optionAuthenticationProtocol,
								2,
								buffer
								);
	}

