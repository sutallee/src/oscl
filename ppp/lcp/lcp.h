/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_lcp_lcph_
#define _oscl_ppp_lcp_lcph_
#include "oscl/ppp/fsm/opt.h"
#include "oscl/pdu/fwd/api.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace LCP {

/** */
class Context {
	public:
		/** */
		virtual ~Context(){}
		/** */
		virtual void	up() noexcept=0;
		/** */
		virtual void	down() noexcept=0;
		/** */
		virtual void	start() noexcept=0;
		/** */
		virtual void	finished() noexcept=0;
		/** */
		virtual void	setAsyncCharMapTx(unsigned long map) noexcept=0;
		/** */
		virtual void	setAsyncCharMapRx(unsigned long map) noexcept=0;
	};

/** */
class Protocol :	public Oscl::PPP::FSM::OPT::Context,
					public Oscl::Pdu::FWD::Api
					{
	private:
		/** */
		Oscl::PPP::FSM::OPT::Variable	_optionFSM;
		/** */
		LCP::Context&					_context;
		/** */
		unsigned long					_asyncCharMapTx;
		/** */
		unsigned long					_asyncCharMapRx;
		/** */
		unsigned						_mru;
		/** */
		enum{maxPduSize=256};
		/** */
		unsigned char					_pduCopy[maxPduSize];

	public:
		/** */
		Protocol(	LCP::Context&		context,
					Oscl::PDU::TX::Api&	transmitter,
					unsigned			restartCountInSeconds=3,
					unsigned			timerValueInSeconds=2
					) noexcept;
		/** */
		void	lowerLayerUp() noexcept;
		/** */
		void	lowerLayerDown() noexcept;
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	oneSecondTick() noexcept;
		/** */
		void	rx(	const void*		pdu,
					unsigned		length
					) noexcept;

	private:	// Oscl::Pdu::FWD::Api
		/** This operation is to be used to transfer responsibility
			for a PDU between layer entities. The implementation
			*must* take responsibility for the PDU. The implementation
			*must* assign the PDU to a Handle if it is to keep the
			PDU beyond the lifetime of this function call, as the
			caller will release its references to the PDU after
			the transfer invocation, and thus the PDU memory resources
			will be released if its reference count goes to zero.
		 */
		void	transfer(Oscl::Pdu::Pdu* pdu) noexcept;

	private:	// Oscl::PPP::FSM::OPT::Context
		/** */
		void	up() noexcept;
		/** */
		void	down() noexcept;
		/** */
		void	start() noexcept;
		/** */
		void	finished() noexcept;
		/** */
		void	optionReq(	const unsigned char*	option,
							unsigned char			type,
							unsigned				optLen
							) noexcept;
		/** */
		void	buildOptions() noexcept;
		/** */
		void	defaultOptions() noexcept;
		/** */
		void	applyOptions() noexcept;

	private:
		/** */
		void	mruOption(	const unsigned char*	pdu,
							unsigned				length
							) noexcept;
		/** */
		void	asyncCharMapOption(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	authProtocolOption(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	qualityProtocolOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept;
		/** */
		void	magicNumberOption(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	protoFieldCompOption(	const unsigned char*	pdu,
										unsigned				length
										) noexcept;
		/** */
		void	addrCtrlFieldCompOption(	const unsigned char*	pdu,
											unsigned				length
											) noexcept;
		/** */
		void	mruConfigNAK() noexcept;
		/** */
		void	asyncCharMapConfigNAK() noexcept;
		/** */
		void	authProtocolConfigNAK() noexcept;
	};

}
}
}

#endif
