/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
#include "oscl/error/fatal.h"

namespace Oscl {
namespace PPP {
namespace FSM {

class Initial : public State {
	private:
		/** */
		void	up(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
	};

class Starting : public State {
	private:
		/** */
		void	up(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
	};

class Closed : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class Stopped : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class Closing : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class Stopping : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class ReqSent : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class AckRcvd : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class AckSent : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

class Opened : public State {
	private:
		/** */
		void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		void	rxr(Context& context,StateVar& sv) const noexcept;
	};

}
}
}

using namespace Oscl::PPP::FSM;

static const Initial	stateInitial;
static const Starting	stateStarting;
static const Closed		stateClosed;
static const Stopped	stateStopped;
static const Closing	stateClosing;
static const Stopping	stateStopping;
static const ReqSent	stateReqSent;
static const AckRcvd	stateAckRcvd;
static const AckSent	stateAckSent;
static const Opened		stateOpened;

//	State
void	State::up(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::down(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::open(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::close(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::topos(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::toneg(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rcrpos(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rcrneg(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rca(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rcn(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rtr(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rta(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::ruc(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rxjpos(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rxjneg(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

void	State::rxr(Context& context,StateVar& sv) const noexcept{
	sv.protocolError();
	}

// Initial
void	Initial::up(Context& context,StateVar& sv) const noexcept{
	sv.changeToStarting();
	}

void	Initial::open(Context& context,StateVar& sv) const noexcept{
	context.tls();
	sv.changeToStarting();
	}

void	Initial::close(Context& context,StateVar& sv) const noexcept{
//	sv.changeToInitial();
	}


// Starting
void	Starting::up(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
	sv.changeToReqSent();
	}

void	Starting::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStarting();
	}

void	Starting::close(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	sv.changeToInitial();
	}


// Closed
void	Closed::down(Context& context,StateVar& sv) const noexcept{
	sv.changeToInitial();
	}

void	Closed::open(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
	sv.changeToReqSent();
	}

void	Closed::close(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosed();
	}

void	Closed::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosed();
	}

void	Closed::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosed();
	}

void	Closed::rca(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosed();
	}

void	Closed::rcn(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosed();
	}

void	Closed::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosed();
	}

void	Closed::rta(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosed();
	}

void	Closed::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToClosed();
	}

void	Closed::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosed();
	}

void	Closed::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
//	sv.changeToClosed();
	}

void	Closed::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosed();
	}


// Stopped
void	Stopped::down(Context& context,StateVar& sv) const noexcept{
	context.tls();
	sv.changeToInitial();
	}

void	Stopped::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopped();
	}

void	Stopped::close(Context& context,StateVar& sv) const noexcept{
	sv.changeToClosed();
	}

void	Stopped::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
	context.sca();
	context.timerEnable();
	sv.changeToAckSent();
	}

void	Stopped::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
	context.scn();
	context.timerEnable();
	sv.changeToReqSent();
	}

void	Stopped::rca(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToStopped();
	}

void	Stopped::rcn(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToStopped();
	}

void	Stopped::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToStopped();
	}

void	Stopped::rta(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopped();
	}

void	Stopped::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToStopped();
	}

void	Stopped::rxjpos(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToStopped();
	}

void	Stopped::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
//	sv.changeToStopped();
	}

void	Stopped::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopped();
	}


// Closing
void	Closing::down(Context& context,StateVar& sv) const noexcept{
	sv.changeToInitial();
	}

void	Closing::open(Context& context,StateVar& sv) const noexcept{
	sv.changeToStopping();
	}

void	Closing::close(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::topos(Context& context,StateVar& sv) const noexcept{
	context.str();
//	sv.changeToClosing();
	}

void	Closing::toneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToClosed();
	}

void	Closing::rcrpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::rcrneg(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::rca(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::rcn(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToClosing();
	}

void	Closing::rta(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToClosed();
	}

void	Closing::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToClosing();
	}

void	Closing::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}

void	Closing::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToClosed();
	}

void	Closing::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToClosing();
	}


// Stopping
void	Stopping::down(Context& context,StateVar& sv) const noexcept{
	context.timerDisable();
	sv.changeToStarting();
	}

void	Stopping::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::close(Context& context,StateVar& sv) const noexcept{
	sv.changeToClosing();
	}

void	Stopping::topos(Context& context,StateVar& sv) const noexcept{
	context.str();
//	sv.changeToStopping();
	}

void	Stopping::toneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	Stopping::rcrpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::rcrneg(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::rca(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::rcn(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToStopping();
	}

void	Stopping::rta(Context& context,StateVar& sv) const noexcept{
	context.sta();
	context.timerDisable();
	sv.changeToStopped();
	}

void	Stopping::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToStopping();
	}

void	Stopping::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}

void	Stopping::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	Stopping::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToStopping();
	}


// ReqSent
void	ReqSent::down(Context& context,StateVar& sv) const noexcept{
	sv.changeToStarting();
	}

void	ReqSent::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToReqSent();
	}

void	ReqSent::close(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.str();
	sv.changeToClosing();
	}

void	ReqSent::topos(Context& context,StateVar& sv) const noexcept{
	context.scr();
//	sv.changeToReqSent();
	}

void	ReqSent::toneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	ReqSent::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.sca();
	sv.changeToAckSent();
	}

void	ReqSent::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.scn();
//	sv.changeToReqSent();
	}

void	ReqSent::rca(Context& context,StateVar& sv) const noexcept{
	context.irc();
	sv.changeToAckRcvd();
	}

void	ReqSent::rcn(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
//	sv.changeToReqSent();
	}

void	ReqSent::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
//	sv.changeToReqSent();
	}

void	ReqSent::rta(Context& context,StateVar& sv) const noexcept{
//	sv.changeToReqSent();
	}

void	ReqSent::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToReqSent();
	}

void	ReqSent::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToReqSent();
	}

void	ReqSent::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	ReqSent::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToReqSent();
	}


// AckRcvd
void	AckRcvd::down(Context& context,StateVar& sv) const noexcept{
	context.timerDisable();
	sv.changeToStarting();
	}

void	AckRcvd::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckRcvd();
	}

void	AckRcvd::close(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.str();
	sv.changeToClosing();
	}

void	AckRcvd::topos(Context& context,StateVar& sv) const noexcept{
	context.scr();
	sv.changeToReqSent();
	}

void	AckRcvd::toneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	AckRcvd::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.sca();
	context.tlu();
	context.timerDisable();
	sv.changeToOpened();
	}

void	AckRcvd::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.scn();
//	sv.changeToAckRcvd();
	}

void	AckRcvd::rca(Context& context,StateVar& sv) const noexcept{
	context.scr();
	sv.changeToReqSent();
	}

void	AckRcvd::rcn(Context& context,StateVar& sv) const noexcept{
	context.scr();
	sv.changeToReqSent();
	}

void	AckRcvd::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
	sv.changeToReqSent();
	}

void	AckRcvd::rta(Context& context,StateVar& sv) const noexcept{
	sv.changeToReqSent();
	}

void	AckRcvd::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToAckRcvd();
	}

void	AckRcvd::rxjpos(Context& context,StateVar& sv) const noexcept{
	sv.changeToReqSent();
	}

void	AckRcvd::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	AckRcvd::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckRcvd();
	}


// AckSent
void	AckSent::down(Context& context,StateVar& sv) const noexcept{
	context.timerDisable();
	sv.changeToStarting();
	}

void	AckSent::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckSent();
	}

void	AckSent::close(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.str();
	sv.changeToClosing();
	}

void	AckSent::topos(Context& context,StateVar& sv) const noexcept{
	context.scr();
//	sv.changeToAckSent();
	}

void	AckSent::toneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	AckSent::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.sca();
//	sv.changeToAckSent();
	}

void	AckSent::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.scn();
	sv.changeToReqSent();
	}

void	AckSent::rca(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.tlu();
	context.timerDisable();
	sv.changeToOpened();
	}

void	AckSent::rcn(Context& context,StateVar& sv) const noexcept{
	context.irc();
	context.scr();
//	sv.changeToAckSent();
	}

void	AckSent::rtr(Context& context,StateVar& sv) const noexcept{
	context.sta();
	sv.changeToReqSent();
	}

void	AckSent::rta(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckSent();
	}

void	AckSent::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToAckSent();
	}

void	AckSent::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckSent();
	}

void	AckSent::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tlf();
	context.timerDisable();
	sv.changeToStopped();
	}

void	AckSent::rxr(Context& context,StateVar& sv) const noexcept{
//	sv.changeToAckSent();
	}


// Opened
void	Opened::down(Context& context,StateVar& sv) const noexcept{
	context.tld();
	sv.changeToStarting();
	}

void	Opened::open(Context& context,StateVar& sv) const noexcept{
//	sv.changeToOpened();
	}

void	Opened::close(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.irc();
	context.str();
	sv.changeToClosing();
	}

void	Opened::rcrpos(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.scr();
	context.sca();
	context.timerEnable();
	sv.changeToAckSent();
	}

void	Opened::rcrneg(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.scr();
	context.scn();
	context.timerEnable();
	sv.changeToReqSent();
	}

void	Opened::rca(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.scr();
	context.timerEnable();
	sv.changeToReqSent();
	}

void	Opened::rcn(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.scr();
	context.timerEnable();
	sv.changeToReqSent();
	}

void	Opened::rtr(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.zrc();
	context.sta();
	context.timerEnable();
	sv.changeToStopping();
	}

void	Opened::rta(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.scr();
	context.timerEnable();
	sv.changeToReqSent();
	}

void	Opened::ruc(Context& context,StateVar& sv) const noexcept{
	context.scj();
//	sv.changeToOpened();
	}

void	Opened::rxjpos(Context& context,StateVar& sv) const noexcept{
//	sv.changeToOpened();
	}

void	Opened::rxjneg(Context& context,StateVar& sv) const noexcept{
	context.tld();
	context.irc();
	context.str();
	context.timerEnable();
	sv.changeToStopping();
	}

void	Opened::rxr(Context& context,StateVar& sv) const noexcept{
	context.ser();
//	sv.changeToOpened();
	}

// StateVar
StateVar::StateVar(Context& context) noexcept:
		_context(context),
		_state(&stateInitial)
		{
	}

void	StateVar::up() noexcept{
	_state->up(_context,*this);
	}

void	StateVar::down() noexcept{
	_state->down(_context,*this);
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::topos() noexcept{
	_state->topos(_context,*this);
	}

void	StateVar::toneg() noexcept{
	_state->toneg(_context,*this);
	}

void	StateVar::rcrpos() noexcept{
	_state->rcrpos(_context,*this);
	}

void	StateVar::rcrneg() noexcept{
	_state->rcrneg(_context,*this);
	}

void	StateVar::rca() noexcept{
	_state->rca(_context,*this);
	}

void	StateVar::rcn() noexcept{
	_state->rcn(_context,*this);
	}

void	StateVar::rtr() noexcept{
	_state->rtr(_context,*this);
	}

void	StateVar::rta() noexcept{
	_state->rta(_context,*this);
	}

void	StateVar::ruc() noexcept{
	_state->ruc(_context,*this);
	}

void	StateVar::rxjpos() noexcept{
	_state->rxjpos(_context,*this);
	}

void	StateVar::rxjneg() noexcept{
	_state->rxjneg(_context,*this);
	}

void	StateVar::rxr() noexcept{
	_state->rxr(_context,*this);
	}

void	StateVar::changeToInitial() noexcept{
	_state	= &stateInitial;
	}

void	StateVar::changeToStarting() noexcept{
	_state	= &stateStarting;
	}

void	StateVar::changeToClosed() noexcept{
	_state	= &stateClosed;
	}

void	StateVar::changeToStopped() noexcept{
	_state	= &stateStopped;
	}

void	StateVar::changeToClosing() noexcept{
	_state	= &stateClosing;
	}

void	StateVar::changeToStopping() noexcept{
	_state	= &stateStopping;
	}

void	StateVar::changeToReqSent() noexcept{
	_state	= &stateReqSent;
	}

void	StateVar::changeToAckRcvd() noexcept{
	_state	= &stateAckRcvd;
	}

void	StateVar::changeToAckSent() noexcept{
	_state	= &stateAckSent;
	}

void	StateVar::changeToOpened() noexcept{
	_state	= &stateOpened;
	}

void	StateVar::protocolError() noexcept{
	Oscl::ErrorFatal::logAndExit(	"Oscl::PPP::FSM::StateVar:"
									"Invalid event for state."
									);
	}

