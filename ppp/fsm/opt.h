/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_fsm_opth_
#define _oscl_ppp_fsm_opth_
#include "oscl/pdu/tx/api.h"
#include "var.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace FSM {
/** */
namespace OPT {

/** */
class Context {
	public:
		/** */
		virtual ~Context(){}
		/** */
		virtual void	up() noexcept=0;
		/** */
		virtual void	down() noexcept=0;
		/** */
		virtual void	start() noexcept=0;
		/** */
		virtual void	finished() noexcept=0;
		/** */
		virtual void	optionReq(	const unsigned char*	option,
									unsigned char			type,
									unsigned				optLen
									) noexcept=0;
		/** */
		virtual void	buildOptions() noexcept=0;
		/** */
		virtual void	defaultOptions() noexcept=0;
		/** */
		virtual void	applyOptions() noexcept=0;
	};

/** */
class Variable : private Oscl::PPP::FSM::Context {
	private:
		enum{maxOptSize=512};
		enum{maxAckRejOptSize=512};
	private:
		/** */
		Oscl::PPP::FSM::StateVar	_state;
		/** */
		OPT::Context&				_context;
		/** */
		Oscl::PDU::TX::Api&			_transmitter;
		/** */
		unsigned					_restartCounterValue;
		/** */
		unsigned					_timerValue;
		/** */
		unsigned char				_identifier;
		/** */
		unsigned char				_options[maxOptSize];
		/** */
		unsigned					_optionLen;
		/** */
		unsigned char				_ackRejOpt[maxOptSize];
		/** */
		unsigned					_ackRejOptLen;
		/** */
		bool						_reject;
		/** */
		bool						_unacceptable;
		/** */
		const unsigned char*		_pdu;
		/** */
		unsigned					_pduLength;
		/** */
		bool						_timerActive;
		/** */
		unsigned					_timer;
		/** */
		unsigned					_restartCounter;
		/** */
		bool						_invalidOptionFormat;
		/** */
		bool						_layerOpen;

	public:
		/** */
		Variable(	OPT::Context&		context,
					Oscl::PDU::TX::Api&	transmitter,
					unsigned			restartCountInSeconds=3,
					unsigned			timerValueInSeconds=2
					) noexcept;
		/** */
		void	up() noexcept;
		/** */
		void	down() noexcept;
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	oneSecondTick() noexcept;
		/** */
		void	rx(const void* pdu,unsigned length) noexcept;
		/** */
		void	appendOption(	unsigned char	type,
								const void*		option,
								unsigned char	length
								) noexcept;
		/** */
		void	appendConfigNAK(	unsigned char	type,
									unsigned		length,
									const void*		option
									) noexcept;
		/** */
		void	appendConfigREJ(	unsigned char	type,
									unsigned		length,
									const void*		option
									) noexcept;

	private:	// Oscl::PPP::FSM::Context
		/** */
		void	tlu() noexcept;
		/** */
		void	tld() noexcept;
		/** */
		void	tls() noexcept;
		/** */
		void	tlf() noexcept;
		/** */
		void	irc() noexcept;
		/** */
		void	zrc() noexcept;
		/** */
		void	scr() noexcept;
		/** */
		void	sca() noexcept;
		/** */
		void	scn() noexcept;
		/** */
		void	str() noexcept;
		/** */
		void	sta() noexcept;
		/** */
		void	scj() noexcept;
		/** */
		void	ser() noexcept;
		/** */
		void	timerEnable() noexcept;
		/** */
		void	timerDisable() noexcept;

	private:
		/** */
		void	sendProtocolReject() noexcept;
		/** */
		void	configureRequest(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	configureAck(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
		/** */
		void	configureNak(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
		/** */
		void	configureReject(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	terminateRequest(	const unsigned char*	pdu,
									unsigned				length
									) noexcept;
		/** */
		void	terminateAck(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
		/** */
		void	codeReject(	const unsigned char*	pdu,
							unsigned				length
							) noexcept;
		/** */
		void	protocolReject(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
		/** */
		void	echoRequest(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
		/** */
		void	echoReply(	const unsigned char*	pdu,
							unsigned				length
							) noexcept;
		/** */
		void	discardRequest(	const unsigned char*	pdu,
								unsigned				length
								) noexcept;
	};

}
}
}
}


#endif
