/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "opt.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace FSM {
/** */
namespace OPT {

enum{	offsetCode			= 0,
		offsetIdentifier	= 1,
		offsetLength		= 2,
		offsetData			= 4
	};

enum{	codeConfigureRequest	= 1,
		codeConfigureAck		= 2,
		codeConfigureNak		= 3,
		codeConfigureReject		= 4,
		codeTerminateRequest	= 5,
		codeTerminateAck		= 6,
		codeCodeReject			= 7,
		codeProtocolReject		= 8,
		codeEchoRequest			= 9,
		codeEchoReply			= 10,
		codeDiscardRequest		= 11
	};

enum{headerLength=4};	// Code,Identifier,Length

enum{	optionOffsetType	= 0,
		optionOffsetLength	= 1,
		optionOffsetData	= 2
		};

enum{optionHeaderLength=2};

}
}
}
}

using namespace Oscl::PPP::FSM::OPT;

Variable::Variable(	OPT::Context&		context,
					Oscl::PDU::TX::Api&	transmitter,
					unsigned			restartCountInSeconds,
					unsigned			timerValueInSeconds
					) noexcept:
		_state(*this),
		_context(context),
		_transmitter(transmitter),
		_restartCounterValue(restartCountInSeconds),
		_timerValue(timerValueInSeconds),
		_identifier(0),
		_optionLen(0),
		_ackRejOptLen(0),
		_reject(false),
		_unacceptable(false),
		_pdu(0),
		_pduLength(0),
		_timerActive(false),
		_timer(0),
		_restartCounter(restartCountInSeconds),
		_invalidOptionFormat(false),
		_layerOpen(false)
		{
	}

void	Variable::up() noexcept{
	_state.up();
	}

void	Variable::down() noexcept{
	_state.down();
	}

void	Variable::open() noexcept{
	_layerOpen	= true;
	_state.open();
	}

void	Variable::close() noexcept{
	_layerOpen	= false;
	_state.close();
	}

void	Variable::oneSecondTick() noexcept{
	if(_timerActive) return;
	if(_timer){
		_timer	-= 1;
		}
	if(_restartCounter){
		_restartCounter	-= 1;
		}
	if(_restartCounter){
		_state.topos();
		}
	else {
		_state.toneg();
		}
	}

void	Variable::rx(const void* pdu,unsigned length) noexcept{
	unsigned				len;
	const unsigned char*	p	= (const unsigned char*)pdu;

	if(!_layerOpen){
		// Discard
		return;
		}

	if(length < headerLength){
		// Discard
		return;
		}

	len	=	p[offsetLength];
	len	<<=	8;
	len	&=	0xFF00;
	len	|=	p[offsetLength+1];

	if(len > length){
		return;
		}

	_pduLength	= len;
	_pdu		= (const unsigned char*)pdu;

	len		-= headerLength;

	switch(p[offsetCode]){
		case codeConfigureRequest:
			configureRequest(&p[offsetData],len);
			break;
		case codeConfigureAck:
			configureAck(&p[offsetData],len);
			break;
		case codeConfigureNak:
			configureNak(&p[offsetData],len);
			break;
		case codeConfigureReject:
			if(_identifier != p[offsetIdentifier]){
				return;
				}
			configureReject(&p[offsetData],len);
			break;
		case codeTerminateRequest:
			terminateRequest(&p[offsetData],len);
			break;
		case codeTerminateAck:
			if(_identifier != p[offsetIdentifier]){
				return;
				}
			terminateAck(&p[offsetData],len);
			break;
		case codeCodeReject:
			codeReject(&p[offsetData],len);
			break;
		case codeProtocolReject:
			protocolReject(&p[offsetData],len);
			break;
		case codeEchoRequest:
			echoRequest(&p[offsetData],len);
			break;
		case codeEchoReply:
			if(_identifier != p[offsetIdentifier]){
				return;
				}
			echoReply(&p[offsetData],len);
			break;
		case codeDiscardRequest:
			discardRequest(&p[offsetData],len);
			break;
		default:
			// Code Reject
			scj();
		}
	_pdu	= 0;
	}

void	Variable::appendOption(	unsigned char	type,
								const void*		option,
								unsigned char	length
								) noexcept{
	unsigned		len;
	unsigned char*	p;

	if(length > (0x00FF-optionHeaderLength)){
		// Too much option data.
		// FIXME: What should we *really* do here?
		return;
		}

	len	= _optionLen + length + optionHeaderLength;
	if(len > maxOptSize){
		// Too much option data.
		// FIXME: What should we *really* do here?
		return;
		}

	p	= &_options[_optionLen];

	p[optionOffsetType]	= type;
	p[optionOffsetLength]	= length + optionHeaderLength;
	memcpy(&p[optionOffsetData],option,length);

	_optionLen	= len;
	}

void	Variable::appendConfigNAK(	unsigned char	type,
									unsigned		length,
									const void*		option
									) noexcept{
	unsigned		len;
	unsigned char*	p;
	_unacceptable	= true;
	if(_reject) return;
	len	= _ackRejOptLen + length + optionHeaderLength;
	if(len > maxAckRejOptSize){
		// Too much option data
		// FIXME: What should we *really* do?
		return;
		}
	p	= &_ackRejOpt[_ackRejOptLen];
	p[optionOffsetType]	= type;
	p[optionOffsetLength]	= length+optionHeaderLength;
	memcpy(&p[optionOffsetData],option,length);

	_ackRejOptLen	+= len;
	}

void	Variable::appendConfigREJ(	unsigned char	type,
									unsigned		length,
									const void*		option
									) noexcept{
	unsigned		len;
	unsigned char*	p;
	_unacceptable	= true;
	if(_reject){
		_ackRejOptLen	= 0;
		_reject			= true;
		}

	len	= _ackRejOptLen + length + optionHeaderLength;
	if(len > maxAckRejOptSize){
		// Too much option data
		// FIXME: What should we *really* do here?
		return;
		}
	p	= &_ackRejOpt[_ackRejOptLen];

	p[optionOffsetType]	= type;
	p[optionOffsetLength]	= length+optionHeaderLength;
	memcpy(&p[optionOffsetData],option,length);
	_ackRejOptLen	= len;
	}

void	Variable::tlu() noexcept{
	_context.applyOptions();
	_context.up();
	}

void	Variable::tld() noexcept{
	_context.down();
	}

void	Variable::tls() noexcept{
	_context.start();
	}

void	Variable::tlf() noexcept{
	_context.defaultOptions();
	_context.finished();
	}

void	Variable::irc() noexcept{
	_restartCounter	= _restartCounterValue;
	}

void	Variable::zrc() noexcept{
	_restartCounter	= 0;
	_timer			= _timerValue;
	}

void	Variable::scr() noexcept{
	unsigned		length;
	unsigned char	code	= codeConfigureRequest;
	unsigned char	buffer[2];

	_identifier	+= 1;
	_optionLen	= 0;
	_context.buildOptions();
	length	= _optionLen + headerLength;
	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);
	_transmitter.open(1+1+2+_optionLen);
	_transmitter.append(&code,1);
	_transmitter.append(&_identifier,1);
	_transmitter.append(buffer,2);
	_transmitter.append(_options,_optionLen);
	_transmitter.close();
	}

void	Variable::sca() noexcept{
	unsigned		length;
	unsigned char	code	= codeConfigureAck;
	if(!_pdu){
		// This should NEVER happen
		Oscl::ErrorFatal::logAndExit(	"Oscl::PPP::FSM::OPT::Variable:"
										"PDU is null!"
										);
		}
	length	=	_pdu[offsetLength];
	length	<<=	8;
	length	|= _pdu[offsetLength+1];

	_transmitter.open(1+1+2+length);
	_transmitter.append(&code,1);
	_transmitter.append(&_pdu[offsetIdentifier],1);
	_transmitter.append(&_pdu[offsetLength],2);
	_transmitter.append(&_pdu[offsetData],length);
	_transmitter.close();
	}

void	Variable::scn() noexcept{
	unsigned		length;
	unsigned char	buffer[2];
	unsigned char	code;

	code	= (_reject)?codeConfigureReject:codeConfigureNak;

	length	= _ackRejOptLen+headerLength;
	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);
	_transmitter.open(length);
	_transmitter.append(&code,1);
	_transmitter.append(&_pdu[offsetIdentifier],1);
	_transmitter.append(buffer,2);
	_transmitter.append(_ackRejOpt,_ackRejOptLen);
	_transmitter.close();
	}

void	Variable::str() noexcept{
	unsigned		length;
	unsigned char	buffer[2];
	unsigned char	code;

	code	= codeTerminateRequest;

	_identifier	+= 1;

	length	= headerLength;
	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);

	_transmitter.open(1+1+2);
	_transmitter.append(&code,1);
	_transmitter.append(&_identifier,1);
	_transmitter.append(buffer,2);
	_transmitter.close();
	}

void	Variable::sta() noexcept{
	unsigned		length;
	unsigned char	buffer[2];
	unsigned char	code;
	code	= codeTerminateAck;

	length	= headerLength;

	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);
	_transmitter.open(1+1+2);
	_transmitter.append(&code,1);
	_transmitter.append(&_pdu[offsetIdentifier],1);
	_transmitter.append(buffer,2);
	_transmitter.close();
	}

void	Variable::scj() noexcept{
	unsigned		length;
	unsigned char	buffer[2];
	unsigned char	code;
	length	= headerLength + _pduLength;
	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);
	_transmitter.open(1+1+2+_pduLength);
	_transmitter.append(&code,1);
	_transmitter.append(&_identifier,1);
	_transmitter.append(buffer,2);
	_transmitter.append(_pdu,_pduLength);
	_transmitter.close();
	}

void	Variable::ser() noexcept{
	// FIXME: Not implemented yet.
	}

void	Variable::timerEnable() noexcept{
	_timerActive	= true;
	}

void	Variable::timerDisable() noexcept{
	_timerActive	= false;
	}

void	Variable::sendProtocolReject() noexcept{
	unsigned		length;
	unsigned char	buffer[2];
	unsigned char	code;

	code	= codeProtocolReject;

	length	= headerLength + _pduLength;

	buffer[0]	= (unsigned char)((length>>8)&0x00FF);
	buffer[1]	= (unsigned char)((length>>0)&0x00FF);

	_transmitter.open(1+1+2+_pduLength);
	_transmitter.append(&code,1);
	_transmitter.append(&_identifier,1);
	_transmitter.append(buffer,2);
	_transmitter.append(_pdu,_pduLength);
	_transmitter.close();
	}

void	Variable::configureRequest(	const unsigned char*	pdu,
									unsigned				length
									) noexcept{
	unsigned	len;
	_unacceptable	= false;
	_reject			= false;
	_ackRejOptLen	= 0;

	for(;length;length-=len,pdu+=len){
		unsigned	optFieldLen;
		if(length < optionHeaderLength){
			// Invalid format
			// FIXME: Is this right? send a protocol reject?
			// Discard seems to be the right thing to do
			// since the RFC does not specify a PDU
			// to send for format errors.
			// RFC 1661 section 5.4 paragraph 2 seems
			// to bear this out in the last sentence.
			// Likewise, Section 6 "Data" discussion
			// seems to indicate discard is right too.
			break;
			}
		len	= pdu[optionOffsetLength];
		if(len > length){
			// Too long
			return;
			}

		optFieldLen	= len-optionHeaderLength;
		_context.optionReq(	&_pdu[optionOffsetData],
							pdu[optionOffsetType],
							optFieldLen
							);
		}
	if(_unacceptable){
		_state.rcrneg();
		}
	else{
		_state.rcrpos();
		}
	}

void	Variable::configureAck(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	_state.rca();
	}

void	Variable::configureNak(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	_state.rcn();
	}

void	Variable::configureReject(	const unsigned char*	pdu,
									unsigned				length
									) noexcept{
	_state.rcn();	// FIXME: Is this right?
	}

void	Variable::terminateRequest(	const unsigned char*	pdu,
									unsigned				length
									) noexcept{
	_state.rtr();
	}

void	Variable::terminateAck(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	_state.rta();
	}

void	Variable::codeReject(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	// FIXME: is it terminal?
	_state.rxjneg();
	}

void	Variable::protocolReject(	const unsigned char*	pdu,
									unsigned				length
									) noexcept{
	// FIXME: is it terminal?
	_state.rxjneg();
	}

void	Variable::echoRequest(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	_state.rxr();
	}

void	Variable::echoReply(	const unsigned char*	pdu,
								unsigned				length
								) noexcept{
	_state.rxr();
	}

void	Variable::discardRequest(	const unsigned char*	pdu,
									unsigned				length
									) noexcept{
	_state.rxr();
	}

