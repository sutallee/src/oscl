/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_fsm_varh_
#define _oscl_ppp_fsm_varh_

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace FSM {

/** */
class Context {
	public:
		/** */
		virtual ~Context() {}
	public:
		/** */
		virtual	void	tlu() noexcept=0;
		/** */
		virtual	void	tld() noexcept=0;
		/** */
		virtual	void	tls() noexcept=0;
		/** */
		virtual	void	tlf() noexcept=0;
		/** */
		virtual	void	irc() noexcept=0;
		/** */
		virtual	void	zrc() noexcept=0;
		/** */
		virtual	void	scr() noexcept=0;
		/** */
		virtual	void	sca() noexcept=0;
		/** */
		virtual	void	scn() noexcept=0;
		/** */
		virtual	void	str() noexcept=0;
		/** */
		virtual	void	sta() noexcept=0;
		/** */
		virtual	void	scj() noexcept=0;
		/** */
		virtual	void	ser() noexcept=0;
		/** */
		virtual	void	timerEnable() noexcept=0;
		/** */
		virtual	void	timerDisable() noexcept=0;
	};

/** */
class StateVar;

/** */
class State {
	public:
		/** */
		virtual void	up(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	down(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	open(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	close(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	topos(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	toneg(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rcrpos(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rcrneg(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rca(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rcn(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rtr(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rta(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	ruc(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rxjpos(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rxjneg(Context& context,StateVar& sv) const noexcept;
		/** */
		virtual void	rxr(Context& context,StateVar& sv) const noexcept;
	};

/** */
class Initial;
/** */
class Starting;
/** */
class Closed;
/** */
class Stopped;
/** */
class Closing;
/** */
class Stopping;
/** */
class ReqSent;
/** */
class AckRcvd;
/** */
class AckSent;
/** */
class Opened;

/** */
class StateVar {
	private:
		/** */
		Context&		_context;
		/** */
		const State*	_state;

	public:
		/** */
		StateVar(Context& context) noexcept;
		/** */
		void	up() noexcept;
		/** */
		void	down() noexcept;
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	topos() noexcept;
		/** */
		void	toneg() noexcept;
		/** */
		void	rcrpos() noexcept;
		/** */
		void	rcrneg() noexcept;
		/** */
		void	rca() noexcept;
		/** */
		void	rcn() noexcept;
		/** */
		void	rtr() noexcept;
		/** */
		void	rta() noexcept;
		/** */
		void	ruc() noexcept;
		/** */
		void	rxjpos() noexcept;
		/** */
		void	rxjneg() noexcept;
		/** */
		void	rxr() noexcept;
	private:
		/** */
		friend class State;
		/** */
		friend class Initial;
		/** */
		friend class Starting;
		/** */
		friend class Closed;
		/** */
		friend class Stopped;
		/** */
		friend class Closing;
		/** */
		friend class Stopping;
		/** */
		friend class ReqSent;
		/** */
		friend class AckRcvd;
		/** */
		friend class AckSent;
		/** */
		friend class Opened;
		/** */
		void	changeToInitial() noexcept;
		/** */
		void	changeToStarting() noexcept;
		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToStopped() noexcept;
		/** */
		void	changeToClosing() noexcept;
		/** */
		void	changeToStopping() noexcept;
		/** */
		void	changeToReqSent() noexcept;
		/** */
		void	changeToAckRcvd() noexcept;
		/** */
		void	changeToAckSent() noexcept;
		/** */
		void	changeToOpened() noexcept;
		/** */
		void	protocolError() noexcept;
	};

}
}
}

#endif
