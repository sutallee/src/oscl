/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::PPP::Basic;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	pppPapi,
			Oscl::Frame::Pdu::Rx::
			ProtoMux::Req::Api::SAP&	protoMuxSAP,
			Oscl::Frame::Pdu::Rx::
			ProtoMux::SyncApi&			protoMuxSyncApi,
			Oscl::Frame::Pdu::
			Tx::Req::Api::SAP&			protoTxSAP
			) noexcept:
		_myPapi(pppPapi),
		_closeSAP(*this,pppPapi),
		_lcp(	pppPapi,
				_layerStateDist.getSAP(),
				protoTxSAP
				),
		_layerStateDist(	pppPapi,
							false
							),
		_nextStep(0),
		_openReq(0),
		_closeReq(0)
		{
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::PPP::Layer::State::Req::Api::SAP&	Part::getIpLayerStateSAP() noexcept{
	return _layerStateDist.getSAP();
	}

void	Part::openLCP() noexcept{
#if 0
	Oscl::Mt::Itc::Srv::
	Open::Req::Api::OpenPayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();
	Oscl::Mt::Itc::Srv::
	Open::Resp::Api::OpenResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::
				OpenResp(	_lcp.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_lcp.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::openCHAP;
#else
	openComplete();
#endif
	}

void	Part::openCHAP() noexcept{
	}

void	Part::openIPCP() noexcept{
	}

void	Part::openComplete() noexcept{
	_openReq->returnToSender();
	_openReq	= 0;
	}

void	Part::closeLCP() noexcept{
	}

void	Part::closeCHAP() noexcept{
	}

void	Part::closeIPCP() noexcept{
#if 0
	Oscl::Mt::Itc::Srv::
	Close::Req::Api::ClosePayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Close::Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::
	Close::Resp::Api::CloseResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Close::Resp::Api::
				CloseResp(	_ipcp.getCloseSAP().getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_ipcp.getCloseSAP().post(resp->getSrvMsg());
	_nextStep	= &Oscl::PPP::Stream::Basic::Part::closeCHAP;
#else
	closeComplete();
#endif
	}

void	Part::closeComplete() noexcept{
	_closeReq->returnToSender();
	_closeReq	= 0;
	}


void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	openLCP();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	closeIPCP();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Open::Resp::Api::OpenResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Close::Resp::Api::CloseResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

