/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ppp_basic_parth_
#define _oscl_ppp_basic_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/frame/pdu/rx/protomux/reqapi.h"
#include "oscl/frame/pdu/rx/protomux/syncapi.h"
#include "oscl/frame/pdu/tx/reqapi.h"
#include "oscl/ppp/layer/state/reqapi.h"
#include "oscl/ppp/layer/state/dist/part.h"
#include "oscl/ppp/layer/lcp/part.h"

/** */
namespace Oscl {
/** */
namespace PPP {
/** */
namespace Basic {

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Resp::Api
				{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::Mem						_openCloseMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP			_closeSAP;
		/** */
		Oscl::PPP::Layer::LCP::Part				_lcp;
		/** */
		Oscl::PPP::Layer::State::Dist::Part		_layerStateDist;
		/** Function pointer to next sub-server to asynchronously close
		 */
		void			(Part::*	_nextStep)();
		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*				_openReq;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	pppPapi,
				Oscl::Frame::Pdu::Rx::
				ProtoMux::Req::Api::SAP&	protoMuxSAP,
				Oscl::Frame::Pdu::Rx::
				ProtoMux::SyncApi&			protoMuxSyncApi,
				Oscl::Frame::Pdu::
				Tx::Req::Api::SAP&			protoTxSAP
				) noexcept;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&		getCloseSAP() noexcept;

		/** */
		Oscl::PPP::Layer::State::Req::Api::SAP&	getIpLayerStateSAP() noexcept;

	private:
		/** */
		void	openLCP() noexcept;
		/** */
		void	openCHAP() noexcept;
		/** */
		void	openIPCP() noexcept;
		/** */
		void	openComplete() noexcept;
		/** */
		void	closeLCP() noexcept;
		/** */
		void	closeCHAP() noexcept;
		/** */
		void	closeIPCP() noexcept;
		/** */
		void	closeComplete() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	};

}
}
}

#endif
