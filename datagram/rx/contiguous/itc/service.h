/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_dgram_rx_contig_itc_serviceh_
#define _oscl_dgram_rx_contig_itc_serviceh_
#include "oscl/datagram/tx/contiguous/itc/sync.h"
#include "oscl/pdu/buffer/allocator/itc/fixed.h"
#include "oscl/pdu/fwd/api.h"

#include "oscl/frame/stream/rx/pdu/config.h"

/** */
namespace Oscl {
/** */
namespace Datagram {
/** */
namespace RX {
/** */
namespace Contiguous {

/** */
class Service :	public Oscl::Datagram::TX::Contiguous::Req::Api
				{
	private:
		/** */
		Oscl::Datagram::TX::Contiguous::Sync	_sync;

	private:
		/** */
		enum{nComposites=4};
		enum{nFragments=4};
		enum{bufferSize=128};
		/** */
		Oscl::Frame::Stream::
		RX::PDU::Config<	nComposites,
							nFragments,
							bufferSize
							>			_streamPdu;
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_myPapi;
		/** */
		Oscl::Pdu::FWD::Api&			_receiver;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&	papi,
					Oscl::Pdu::FWD::Api&		receiver
					) noexcept;

		/** */
		Oscl::Datagram::TX::Contiguous::Req::Api::SAP&	getInputSAP() noexcept;
		/** */
		Oscl::Datagram::TX::Contiguous::Api&			getSyncApi() noexcept;

	private:	// Oscl::Datagram::TX::Contiguous::Req::Api
		/** */
		void	request(	Oscl::Datagram::TX::
							Contiguous::Req::Api::TxReq&	msg
							) noexcept;
	};

}
}
}
}

#endif
