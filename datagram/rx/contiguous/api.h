/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_dgram_rx_contiguous_apih_
#define _oscl_dgram_rx_contiguous_apih_

/** */
namespace Oscl {
/** */
namespace Datagram {
/** */
namespace RX {
/** */
namespace Contiguous {

/** This interface receives the datagram represented
	into a contiguous buffer from its destination.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/**	This operation returns the next datagram from its peer.
			The datagram is copied into the "datagram" buffer.
			Up to "maxLength" bytes are copied into the "datagram" buffer.
			Zero is returned if the connection is terminated, otherwise
			the number of bytes actually copied to the "datagram" buffer
			is returned. Notice that if the returned length is greater
			than maxLength, then only the first "maxLength" bytes are
			copied to the "datagram" buffer and the remaining bytes
			of the incomming datagram are discarded.
		 */
		virtual unsigned	recv(void* datagram,unsigned maxLength) noexcept=0;
	};

}
}
}
}

#endif
