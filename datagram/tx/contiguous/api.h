/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_dgram_tx_apih_
#define _oscl_dgram_tx_apih_

/** */
namespace Oscl {
/** */
namespace Datagram {
/** */
namespace TX {
/** */
namespace Contiguous {

/** This interface transmits the datagram represented
	as a contiguous buffer towards its destination.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/**	This is a best effort transfer service. No assumptions can
			be made by the client about the success or failure of the
			delivery to the destination. The delivery semantics are
			At-Most-Once for each invocation of this interface. Each
			invocation is independent (i.e. stateless) from the
			point-of-view of the implementation.

			The implementation is responsible for transmitting the message
			toward the destinatiion as a unit with the boundaries of the
			datagram intact, contrasted with a stream oriented service.

			Each invocation of this operation is independent of all
			others and the order of reception at the peer is NOT
			ensured. However, the implementation SHALL not intentionally
			reorder datagrams, and must initiate transmission in
			FIFO order relative to this interface.

			The transmit transfer semantics are At-Most-Once for each
			invocation of this interface. However, the implementation MAY
			use Exactly-Once service to perform the transfer.

			The destination receive transfer semantics are At-Least-Once.
			That is the destination MAY receive multiple copies of the
			same datagram even though only one copy was transmitted
			at this interface.

			While this interface does NOT ensure that the destination
			will receive only one copy of the datagram, this MUST
			never be intentional in the design of the implementation.
			E.g. the implemenation of this interface MUST never
			intentionally transmit the datagram more than once.
			However, certain transport mechanisms MAY unintentionally
			copy the datagram in transit and deliver two or more copies
			of the datagram to the destination under unusual circumstances.
		 */
		virtual void	send(const void* datagram,unsigned length) noexcept=0;
	};

}
}
}
}

#endif
