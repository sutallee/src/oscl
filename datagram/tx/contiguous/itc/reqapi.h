/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_dgram_tx_contig_itc_reqapih_
#define _oscl_dgram_tx_contig_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Datagram {
/** */
namespace TX {
/** */
namespace Contiguous {
/** */
namespace Req {

/** */
class TxPayload {
	public:
		/** */
		const void*		_datagram;
		/** */
		unsigned		_length;

	public:
		/** */
		TxPayload(	const void*	datagram,
					unsigned	length
					) noexcept;
	};

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,TxPayload>	TxReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>						SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>				ConcreteSAP;

	public:
		/** */
		virtual ~Api() {}

		/**	The message is not returned to the sender until the
			implementation has finished with the datagram resources.
			Thus, the memory resources associated with the datagram
			may be reclaimed after the returnToSender() operation
			is invoked.

			This is a best effort transfer service. No assumptions can
			be made by the client about the success or failure of the
			delivery to the destination. The delivery semantics are
			At-Most-Once for each invocation of this interface. Each
			invocation is independent (i.e. stateless) from the
			point-of-view of the implementation.

			The implementation is responsible for delivering the message
			to the destinatiion as a unit with the boundaries of the
			datagram intact, unlike a stream oriented service.
			Each invocation of this operation is independent of all
			others and the implementation need not ensure ordering.

			The transmit transfer semantics are At-Most-Once for each
			invocation of this interface. However, the implementation MAY
			use Exactly-Once service to perform the transfer.

			The destination receive transfer semantics are At-Least-Once.
			That is the destination MAY receive multiple copies of the
			same datagram even though only one copy was transmitted
			at this interface.

			While this interface does NOT ensure that the destination
			will receive only one copy of the datagram, this MUST
			never be intentional in the design of the implementation.
			E.g. the implemenation of this interface MUST never
			intentionally transmit the datagram more than once.
			However, certain transport mechanisms MAY unintentionally
			copy the datagram in transit and deliver two or more copies
			of the datagram to the destination under unusual circumstances.
		 */
		virtual void	request(TxReq& msg) noexcept=0;
	};

}
}
}
}
}

#endif
