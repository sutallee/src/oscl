/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ms_fat_partitionh_
#define _oscl_ms_fat_partitionh_
#include <stdint.h>

/** */
namespace Oscl {
/** Mass Storage */
namespace MS {
/** */
namespace FAT {

/** */
class Geometry {
	private:
		/** */
		const unsigned long	_totalSectors;
		/** */
		const unsigned long	_totalHeads;
	public:
		/** */
		Geometry(	unsigned long	totalSectors,
					unsigned long	totalHeads
					) noexcept:
			_totalSectors(totalSectors),
			_totalHeads(totalHeads)
			{}
	public:
		inline uint32_t chsToLBA(	unsigned		cylinder,
									unsigned		head,
									unsigned long	sector
									) noexcept{
			return		(sector -1)
					+	(head * _totalSectors)
					+	(cylinder * (_totalHeads+1) * _totalSectors);
			}
		inline void		lbaToCHS(	uint32_t		lba,
									unsigned&		cylinder,
									unsigned&		head,
									unsigned long&	sector
									) noexcept{
			const uint32_t	cylhead	= lba/_totalSectors;
			sector		= (lba%_totalSectors)+1;
			head		= cylhead%(_totalHeads+1);
			cylinder	= cylhead/(_totalHeads+1);
			}
	};

/** */
namespace Partition {

/** */
struct CHS {
	/** */
	uint8_t		_head;
	/** */
	uint8_t		_cylinderSector[2];
	/** */
	inline	uint32_t	chs() const noexcept{
		uint32_t	value	= _head;
		value <<=	8;
		value	|= _cylinderSector[1];
		value <<=	8;
		value	|= _cylinderSector[0];
		return value;
		}
	};

/** */
struct PartitionEntry {
	/** */
	uint8_t		_state;
	/** */
	CHS				_start;
	/** */
	uint8_t		_type;
	/** */
	CHS				_end;
	/** */
	uint8_t		_startingLBA[4];
	/** */
	uint8_t		_lengthInSectors[4];
	/** */
	enum{
			/** */
			typeUnknown					= 0x00,
			/** */
			typeFat12					= 0x01,
			/** */
			typeSmallFat16				= 0x04,
			/** */
			typeExtendedPartition		= 0x05,
			/** */
			typeLargeFat16				= 0x06,
			/** */
			typeFat32					= 0x0B,
			/** */
			typeFat32LBA				= 0x0C,
			/** */
			typeLargeFat16LBA			= 0x0E,
			/** */
			typeExtendedPartitionLBA	= 0x0F
			};

	/** */
	inline	bool	isActive() const noexcept{
		return _state & 0x80;
		}

	/** */
	inline	uint32_t	startingLBA() const noexcept{
		uint32_t
		value	=	_startingLBA[3];
		value	<<=	8;
		value	|=	_startingLBA[2];
		value	<<=	8;
		value	|=	_startingLBA[1];
		value	<<=	8;
		value	|=	_startingLBA[0];
		return value;
		}
	inline	uint32_t	lengthInSectors() const noexcept{
		uint32_t
		value	=	_lengthInSectors[3];
		value	<<=	8;
		value	|=	_lengthInSectors[2];
		value	<<=	8;
		value	|=	_lengthInSectors[1];
		value	<<=	8;
		value	|=	_lengthInSectors[0];
		return value;
		}
	};

/** */
struct MBR {
	/** */
	uint8_t		_code[446];
	/** */
	PartitionEntry	_pe[4];
	/** */
	uint8_t		_marker[2];
	/** */
	inline bool	validMarker() const noexcept{
		return (_marker[0] == 0x55) && (_marker[1] == 0xAA);
		}
	};

void print(const MBR& resp) noexcept;

}
}
}
}

#endif
