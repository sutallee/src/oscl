/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "monitor.h"
#include "oscl/driver/usb/setup/getconfigdesc.h"
#include "oscl/driver/usb/setup/setconfig.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/oid/fixed.h"
#include "oscl/driver/usb/pipe/status.h"

using namespace Oscl::MS::Block;

Monitor::StopDone::StopDone(Monitor& context) noexcept:
		_context(context)
		{
	}

void	Monitor::StopDone::done(Partition& partition) noexcept{
	_context.stopDone(partition);
	}


Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<	Oscl::Block::RW::
										Dyn::Service
										>&					advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&					advRapi,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					PacketMem&								packetMem,
					PartitionMem*							partitionMem,
					unsigned								maxPartitions,
					unsigned								oidType
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::
		Monitor<Oscl::Block::RW::Dyn::Service>(	advSAP,
												advFind,
												myPapi,
												_match
												),
		_match(),
		_advCapi(advCapi),
		_advRapi(advRapi),
		_advPapi(advPapi),
		_dynSrvPapi(dynSrvPapi),
		_driverPapi(driverPapi),
		_myPapi(myPapi),
		_packetMem(packetMem),
		_stopDone(*this),
		_oidType(oidType)
		{
	for(unsigned i=0;i<maxPartitions;++i){
		_freePartitions.put(&partitionMem[i]);
		}
	}

void	Monitor::activate() noexcept{
	const void*
	firstBlock	= readFirstBlock();
	if(!firstBlock){
		// Message is issued by readFirstBlock()
		return;
		}

	Oscl::MS::FAT::Partition::MBR*
	mbr	= (Oscl::MS::FAT::Partition::MBR*)firstBlock;

	if(!mbr->validMarker()){
		Oscl::Error::Info::log("Invalid FAT MBR partition marker.");
		return;
		}

	for(unsigned i=0;i<4;++i){
		if(!mbr->_pe[i].isActive()) continue;
		switch(mbr->_pe[0]._type){
			case Oscl::MS::FAT::Partition::PartitionEntry::typeSmallFat16:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeLargeFat16:
				break;
			case Oscl::MS::FAT::Partition::PartitionEntry::typeFat12:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeExtendedPartition:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeFat32:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeFat32LBA:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeLargeFat16LBA:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeExtendedPartitionLBA:
			case Oscl::MS::FAT::Partition::PartitionEntry::typeUnknown:
			default:
				Oscl::Error::Info::log("Unsupported partition type.");
				return;
			}

		activateFat16Partition(	mbr->_pe[i].startingLBA(),
								mbr->_pe[i].lengthInSectors(),
								i
								);
		}
	}

void	Monitor::closeNextPartition() noexcept{
	// This routine should close all active
	// partitions. It is invoked by deactivate().
	// The partitions may be closed synchronously
	// if they execute in a thread other than
	// this monitor thread (which they do).
	// ??? If we allow nested partitions that
	// share the same dynamic device thread,
	// (e.g. a la Micro$oft extended partitions)
	// then we must do everything asynchronously
	// to prevent dead-lock.
	// For each partition, we need to issue
	// a DeactivateReq; when that completes
	// issue a CloseReq; deallocate/destruct
	// the partition; and finally, after this
	// is completed for each partition, invoke
	// the deactivateDone() operation, which
	// causes the monitor to begin monitoring
	// for a new block device.

	Oscl::MS::Block::Partition*
	next	=	_activePartitions.get();

	if(next){
		next->stop(_stopMem,_stopDone);
		return;
		}

	deactivateDone();
	}

void	Monitor::releaseResources() noexcept{
	}

void	Monitor::stopDone(Partition& partition) noexcept{
	partition.~Partition();
	_freePartitions.put((PartitionMem*)&partition);

	closeNextPartition();
	}

void	Monitor::deactivate() noexcept{
	closeNextPartition();
	}

const void*	Monitor::readFirstBlock() noexcept{
	Oscl::Block::RW::Dyn::Service&	dyndev	= _handle->getDynSrv();
	const Oscl::Block::Read::Status::Result*
	result	= dyndev.getSyncApi().readApi().read(	0,
													1,
													&_packetMem._blockMem
													);
	if(!result){
		return &_packetMem._blockMem;
		}
	return (const void*)0;	// FIXME
	}

void	Monitor::activateFat16Partition(	uint32_t	startingLBA,
											uint32_t	lengthInSectors,
											unsigned	index
											) noexcept{
	PartitionMem*	mem	= _freePartitions.get();
	if(!mem) return;

	Oscl::ObjectID::Fixed<32>	location;
	static_cast<Oscl::ObjectID::Api&>(location)	= _handle->getDynSrv()._location;
	location	+= _oidType;
	location	+= index;


	Oscl::MS::Block::Partition*
	partition	= new (mem)
					Oscl::MS::Block::
					Partition(	_dynSrvPapi,
								_advRapi,
								_advPapi,
								_driverPapi,
								_handle->getDynSrv().getApi().getReadSAP(),
								_handle->getDynSrv().getSyncApi().readApi(),
								_handle->getDynSrv().getApi().getWriteSAP(),
								_handle->getDynSrv().getSyncApi().writeApi(),
								_myPapi,
								startingLBA,
								lengthInSectors,
								Oscl::Block::RW::Dyn::Service::fat,
								&location
								);
	_activePartitions.put(partition);
	partition->start(_advCapi);
	}

