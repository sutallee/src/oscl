/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ms_block_configh_
#define _oscl_ms_block_configh_
#include <new>
#include "monitor.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"
#include "oscl/extalloc/api.h"

/** */
namespace Oscl {
/** */
namespace MS {
/** */
namespace Block {

/**	The purpose of this template class is to provide a configurable
	means of static allocation for USB Hub Monitor.
 */
template< unsigned maxPartitions >
class Config {
	private:
		/** */
		Monitor::PartitionMem					_partitionMem[maxPartitions];
		/** Memory for the actual monitor. The monitor is
			initialized (a la "new") in the body of the
			constructor such that "complex" memory calculations
			can be performed and dynamic memory allocated.
		 */
		Oscl::Memory::
		AlignedBlock<sizeof(Monitor)>			_monMem;

		/** */
		Oscl::ExtAlloc::Record					_dmaMemRec;
		/** This pointer is initialized during construction,
			and points to _monMem after the "new" operation
			is complete.
		 */
		Monitor*								_theMonitor;
		
	public:
		/** */
		Config(		Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<	Oscl::Block::RW::
										Dyn::Service
										>&					advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&					advRapi,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::ExtAlloc::Api&					dmaMemAllocator,
					unsigned								oidType
					) noexcept;
		/** */
		virtual ~Config(){}

		/** */
		Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	getOpenCloseSyncApi() noexcept{
			return *_theMonitor;
			}
	};

/**
 */
template<unsigned maxPartitions>
Config<	maxPartitions
		>::Config(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<	Oscl::Block::RW::
										Dyn::Service
										>&					advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&					advRapi,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::ExtAlloc::Api&					dmaMemAllocator,
					unsigned								oidType
					) noexcept
		{
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	monitorPacketMemSize		=
		(sizeof(Oscl::MS::Block::Monitor::PacketMem));

	const unsigned long	dmaMemSize		= monitorPacketMemSize;

	const unsigned long	nPages		=	(dmaMemSize+almostPageSize)/pageSize;
	const unsigned long	allocSize	=	pageSize*nPages;

	if(!dmaMemAllocator.alloc(_dmaMemRec,allocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::MS::Block::Config: cant allocate dma buffers."
			);
		}

	Oscl::MS::Block::Monitor::PacketMem*
	monitorPacketMem = (Oscl::MS::Block::Monitor::PacketMem*)_dmaMemRec.getFirstUnit();

	_theMonitor	= new(&_monMem)	Monitor(	advFind,
											advSAP,
											advCapi,
											advRapi,
											advPapi,
											dynSrvPapi,
											driverPapi,
											myPapi,
											*monitorPacketMem,
											_partitionMem,
											maxPartitions,
											oidType
											);
	}

}
}
}

#endif
