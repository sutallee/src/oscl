/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ms_block_monitorh_
#define _oscl_ms_block_monitorh_
#include "match.h"
#include "oscl/block/rw/dyn/service.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/block/read/itc/respmem.h"
#include "oscl/ms/fat/partition.h"

#include "oscl/mt/itc/dyn/core/mem.h"
#include "partition.h"

/** */
namespace Oscl {
/** */
namespace MS {
/** */
namespace Block {

/** This concrete class listens for the creation of a single
	mass storage block device. Upon claiming the device,
	the device is examined to determine the type of partitioning
	schem that is used, and then some number of block partions
	are created and registered with the appropriate advertiser.
 */
class Monitor :
		public	Oscl::Mt::Itc::
				Dyn::Unit::Monitor<Oscl::Block::RW::Dyn::Service>
		{
	public:
		/** */
		union PacketMem {
			/** */
			void*								__qitemlink;
			/** This memory is used to hold a 512 block for examining
				the partition table of the block device. It is only
				needed when bringing the device on-line as it is
				dynamically discovered.
			 */
			Oscl::Memory::
			AlignedBlock<512>					_blockMem;
			};
		/** */
		class StopDone : public Oscl::MS::Block::Partition::Done {
			private:
				/** */
				Monitor&	_context;
			public:
				/** */
				StopDone(Monitor& context) noexcept;
				/** */
				void	done(Partition& partition) noexcept;
			};
		/** */
		struct PartitionMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::
			AlignedBlock<sizeof(Oscl::MS::Block::Partition)>	part;
			};

	private:
		/** */
		Partition::StopMem						_stopMem;
		/** */
		Match									_match;

		// //////////////////////////////////////////////////////
		// The following advertiser interface components
		// exist for advertising the partitions created by
		// this monitor.
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<	Oscl::Block::RW::
							Dyn::Service
							>&					_advCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<	Oscl::Block::RW::
				Dyn::Service
				>::ReleaseApi&					_advRapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_advPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_dynSrvPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_driverPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		PacketMem&								_packetMem;
		/** */
		Oscl::Queue<PartitionMem>				_freePartitions;
		/** */
		Oscl::Queue<	Oscl::MS::Block::
						Partition
						>						_activePartitions;
		/** */
		StopDone								_stopDone;
		/** */
		const unsigned							_oidType;
		
	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<	Oscl::Block::RW::
										Dyn::Service
										>&					advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&					advRapi,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					PacketMem&								packetMem,
					PartitionMem*							partitionMem,
					unsigned								maxPartitions,
					unsigned								oidType
					) noexcept;
	private:
		/** */
		void	closeNextPartition() noexcept;
		/** */
		void	releaseResources() noexcept;

	private:
		friend class StopDone;
		/** */
		void	stopDone(Partition& partition) noexcept;


	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;

	private: // helpers
		/** */
		const void*	readFirstBlock() noexcept;
		/** */
		void	activateFat16Partition(	uint32_t	startingLBA,
										uint32_t	lengthInSectors,
										unsigned	index
										) noexcept;
	};

}
}
}

#endif

