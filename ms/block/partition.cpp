/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "partition.h"

using namespace Oscl::MS::Block;

Partition::Partition(	Oscl::Mt::Itc::PostMsgApi&		dynSrvPapi,
						Oscl::Mt::Itc::Dyn::Adv::
						Core<	Oscl::Block::RW::
								Dyn::Service
								>::ReleaseApi&			advRapi,
						Oscl::Mt::Itc::PostMsgApi&		advPapi,
						Oscl::Mt::Itc::PostMsgApi&		driverPapi,
						Oscl::Block::Read::
						ITC::Req::Api<512>::SAP&		readSAP,
						Oscl::Block::Read::Api<512>&	readApi,
						Oscl::Block::Write::
						ITC::Req::Api<512>::SAP&		writeSAP,
						Oscl::Block::Write::Api<512>&	writeApi,
						Oscl::Mt::Itc::PostMsgApi&		myPapi,
						unsigned long					lbaOffset,
						unsigned long					sizeInBlocks,
						Oscl::Block::RW::
						Dyn::Service::BlockType			blockType,
						const Oscl::ObjectID::RO::Api*	location
						) noexcept:
		_blockDriver(	driverPapi,
						readSAP,
						readApi,
						writeSAP,
						writeApi,
						lbaOffset,
						sizeInBlocks
						),
		_dynService(	dynSrvPapi,
						advRapi,
						advPapi,
						_blockDriver,
						_blockDriver,
						_blockDriver,
						_blockDriver.getSAP(),
						blockType,
						sizeInBlocks,
						location
						),
		_myPapi(myPapi)
		{
	}

void	Partition::start(	Oscl::Mt::Itc::Dyn::Adv::
							Creator::SyncApi<	Oscl::Block::RW::
												Dyn::Service
												>&        advCapi
							) noexcept{
	_dynService.syncOpen();
	advCapi.add(_dynService);
	}

void	Partition::stop(StopMem& mem,Done& done) noexcept{
	Oscl::Mt::Itc::Dyn::Creator::
	Req::Api<	Oscl::Block::RW::
                Dyn::Service
			>::DeactivatePayload*
	payload	= new(&mem.deactivate.payload)
				Oscl::Mt::Itc::Dyn::Creator::
				Req::Api<	Oscl::Block::RW::
                            Dyn::Service
							>::DeactivatePayload();

	Oscl::Mt::Itc::Dyn::Creator::
	Resp::Api<Oscl::Block::RW::
                            Dyn::Service>::DeactivateResp*
	resp	= new(&mem.deactivate.resp)
				Oscl::Mt::Itc::Dyn::Creator::
				Resp::Api<	Oscl::Block::RW::
                            Dyn::Service
							>::
				DeactivateResp(	_dynService.getCreatorSAP().getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_dynService.getCreatorSAP().post(resp->getSrvMsg());
	_mem	= &mem;
	_done	= &done;
	}

void    Partition::response(	Oscl::Mt::Itc::Srv::
								Open::Resp::Api::OpenResp&	msg
								) noexcept{
	for(;;);	// Not used, we do a synchronous open
	}

void    Partition::response(	Oscl::Mt::Itc::Srv::
								Close::Resp::Api::CloseResp&	msg
								) noexcept{
	_done->done(*this);
	}

void    Partition::response(   Oscl::Mt::Itc::Dyn::Creator::
								Resp::Api<	Oscl::Block::RW::
											Dyn::Service
											>::DeactivateResp&   msg
								) noexcept{
	Oscl::Mt::Itc::Srv::Close::
	Req::Api::ClosePayload*
	payload	= new(&_mem->close.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::
	Resp::Api::CloseResp*
	resp	= new(&_mem->close.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_dynService.getCreatorSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_dynService.getCreatorSAP().post(resp->getSrvMsg());
	}

