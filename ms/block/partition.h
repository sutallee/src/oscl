/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ms_block_partitionh_
#define _oscl_ms_block_partitionh_
#include "oscl/mt/itc/dyn/core/mem.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/block/rw/dyn/service.h"
#include "oscl/block/rw/deco/driver.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"

/** */
namespace Oscl {
/** */
namespace MS {
/** */
namespace Block {

/** This object represents a single dynamic partition.
	It is to be owned by a creator, that presumably
	monitors for new block devices.
 */
class Partition :
		private	Oscl::Mt::Itc::
				Dyn::Creator::Resp::Api<Oscl::Block::RW::Dyn::Service>,
		private Oscl::Mt::Itc::Srv::Close::Resp::Api
	{
	public:
		/** */
		class Done {
			public:
				/** */
				virtual ~Done(){}
				/** */
				virtual void	done(Partition& partition) noexcept=0;
			};

		/** */
		union StopMem {
			/** */
			Oscl::Mt::Itc::Srv::Close::Resp::CloseMem	close;
			/** */
			Oscl::Mt::Itc::Dyn::
			Creator::Resp::
			DeactivateMem<	Oscl::Block::RW::
							Dyn::Service
							>							deactivate;
			};
	public:
		/** This link is for use by the creator.
		 */
		void*								__qitemlink;

	private:
		/** */
		Oscl::Block::RW::Decorator::Driver	_blockDriver;
		/** */
		Oscl::Block::RW::Dyn::Service		_dynService;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		StopMem*							_mem;
		/** */
		Done*								_done;
		
	public:
		/** */
		virtual ~Partition() {}
		/** */
		Partition(	Oscl::Mt::Itc::PostMsgApi&		dynSrvPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::PostMsgApi&		advPapi,
					Oscl::Mt::Itc::PostMsgApi&		driverPapi,
					Oscl::Block::Read::
					ITC::Req::Api<512>::SAP&		readSAP,
					Oscl::Block::Read::Api<512>&	readApi,
					Oscl::Block::Write::
					ITC::Req::Api<512>::SAP&		writeSAP,
					Oscl::Block::Write::Api<512>&	writeApi,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					unsigned long					lbaOffset,
					unsigned long					sizeInBlocks,
					Oscl::Block::RW::
					Dyn::Service::BlockType			blockType,
					const Oscl::ObjectID::RO::Api*	location=0
					) noexcept;

		/** */
		void	start(	Oscl::Mt::Itc::Dyn::Adv::
						Creator::SyncApi<	Oscl::Block::RW::
											Dyn::Service
											>&        advCapi
						) noexcept;

		/** This operation proceeds asynchronously. It invokes the
			specified done function upon completion.
		 */
		void	stop(StopMem& mem,Done& done) noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void    response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&	msg
							) noexcept;
		/** */
		void    response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	private:    // Oscl::Mt::Itc::Dyn::Creator::Resp::Api
		/** */
		void    response(   Oscl::Mt::Itc::Dyn::Creator::
							Resp::Api<	Oscl::Block::RW::
										Dyn::Service
										>::DeactivateResp&   msg
							) noexcept;
	};

}
}
}

#endif
