/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_cache_platform_operationsh_
#define _oscl_cache_platform_operationsh_

#ifndef _oscl_cache_operationsh_
#warning "NEVER include this file directly. #include oscl/cache/operations.h instead."
#endif

//
// The operations are implemented in an assembly language
// routine in oscl/platform/mips/{specific}/oscl/cache/platform/plat.S
// Since the MIPS processors have potentially configurable or at least
// run-time disoverable cache-line sizes, we implement this as a
// general MIPS way.
//
#if 0
extern "C" {
	/** This operation flushes all dirty cache-lines 
		included in the specified range of addresses.
	 */
	void OsclCacheDataFlushRange(	void*			effectiveAddress,
									unsigned long	nBytes
									) noexcept;

	/** This operation invalidates all cache-lines 
		included in the specified range of addresses.
	 */
	void OsclCacheDataInvalidateRange(	void*			effectiveAddress,
										unsigned long	nBytes
										) noexcept;

	/** This operation returns the size of the cache-line
		for the system.
	 */
	unsigned long	OsclCacheGetLineSize() noexcept;
	}
#endif

#endif
