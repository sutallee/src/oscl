/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_kernel_platform_interrupth_
#define _oscl_kernel_platform_interrupth_

#ifndef _oscl_kernel_interrupth_
#warning "NEVER include this file directly. #include oscl/kernel/interrupt.h instead."
#endif

#include "oscl/compiler/types.h"
#include "oscl/driver/h8300l/asm.h"

/** */
namespace Oscl {
/** */
namespace Kernel {

	/** */
	typedef Oscl::Unsigned8	IrqMaskState;

}
}

extern "C"{
	/** */
	inline void	OsclKernelDisableInterrupts(Oscl::Kernel::IrqMaskState& save) noexcept{
		Oscl::Kernel::IrqMaskState	ccr = Oscl::H8300L::getCCR();
		save        = ccr;
		ccr			|= 0x80;
		Oscl::H8300L::setCCR(ccr);
		}

	/** */
	inline void	OsclKernelRestoreInterruptState(const Oscl::Kernel::IrqMaskState& savedLevel) noexcept{
		Oscl::Kernel::IrqMaskState	state	= savedLevel & 0x80;
		Oscl::Kernel::IrqMaskState	ccr	=	Oscl::H8300L::getCCR();
		ccr					|=	state;
		Oscl::H8300L::setCCR(ccr);
		}
	}

#endif
