/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_cache_platform_lineh_
#define _oscl_cache_platform_lineh_

#ifndef _oscl_cache_lineh_
#warning "NEVER include this file directly. #include oscl/cache/line.h instead."
#endif

#include "oscl/compiler/align.h"

/**
	This value is derived from the Linux configuration
	which indicates a cache line size of 1<<7 (128) bytes.
	Other internet sources indicate that it is actually
	64 bytes.
	
 */
#define OsclCacheLineSizeInBytes	64

#define OsclCacheAlignMacroPre() \
    OsclCompilerAlignMacroPre(OsclCacheLineSizeInBytes)

#define OsclCacheAlignMacroPost() \
    OsclCompilerAlignMacroPost(OsclCacheLineSizeInBytes)

#endif
