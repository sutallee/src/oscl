/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_port_platform_frameh_
#define _oscl_krux_port_platform_frameh_
#include "oscl/compiler/types.h"

typedef struct OsclKruxFrame {
	// FIXME
	// This should be the registers
	// saved by an INTERRUPT() routine
	// followd by the other registers.
	Oscl::Unsigned8	sreg;
	Oscl::Unsigned8	r31;
	Oscl::Unsigned8	r30;
	Oscl::Unsigned8	r29;
	Oscl::Unsigned8	r28;
	Oscl::Unsigned8	r27;
	Oscl::Unsigned8	r26;
	Oscl::Unsigned8	r25;
	Oscl::Unsigned8	r24;
	Oscl::Unsigned8	r23;
	Oscl::Unsigned8	r22;
	Oscl::Unsigned8	r21;
	Oscl::Unsigned8	r20;
	Oscl::Unsigned8	r19;
	Oscl::Unsigned8	r18;
	Oscl::Unsigned8	r17;
	Oscl::Unsigned8	r16;
	Oscl::Unsigned8	r15;
	Oscl::Unsigned8	r14;
	Oscl::Unsigned8	r13;
	Oscl::Unsigned8	r12;
	Oscl::Unsigned8	r11;
	Oscl::Unsigned8	r10;
	Oscl::Unsigned8	r9;
	Oscl::Unsigned8	r8;
	Oscl::Unsigned8	r7;
	Oscl::Unsigned8	r6;
	Oscl::Unsigned8	r5;
	Oscl::Unsigned8	r4;
	Oscl::Unsigned8	r3;
	Oscl::Unsigned8	r2;
	Oscl::Unsigned8	r1;
	Oscl::Unsigned8	r0;
	void		(*pc)();
	}OsclKruxFrame;

#endif

