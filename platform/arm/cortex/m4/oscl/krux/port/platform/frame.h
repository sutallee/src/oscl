/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_target_frameh_
#define _oscl_target_frameh_
#include "oscl/compiler/types.h"

/* There is not yet a Krux port to the ARM Cortex M4 */
#error "The features in this file are bogus."

/**	This describes the format of the stack frame
	that is automatically generated by the Cortex-M4
	when it takes an exception.
 */
struct OsclKruxCortexM4ExceptionFrame {
	Oscl::Unsigned32	r0;
	Oscl::Unsigned32	r1;
	Oscl::Unsigned32	r2;
	Oscl::Unsigned32	r3;
	Oscl::Unsigned32	r12;
	Oscl::Unsigned32	lr;
	Oscl::Unsigned32	pc;
	Oscl::Unsigned32	xPSR;
	};

/**
	From the ARM-ABI document:
		r0-r3 - argument/return values (volatile)
		r4-r8,r10,r11,SP - callee saved, local subroutine variables
		A subroutine must preserve the contents of registers r4-r8,r10,r11,SP
		and possbilby r9.
		r9 is platform specific (may be callee saved)
		r12 - linker scratch register, and may be used within a route
				to hold intermediate values between subroutine calls.
 */

typedef struct OsclKruxFrame {
	OsclKruxCortexM4ExceptionFrame	exFrame;
	Oscl::Unsigned32				r5;
	Oscl::Unsigned32				r6;
	Oscl::Unsigned32				r7;
	Oscl::Unsigned32				r8;
	Oscl::Unsigned32				r9;
	Oscl::Unsigned32				r10;
	Oscl::Unsigned32				r11;
	}OsclKruxFrame;

#endif /* _target_frameh_ */

