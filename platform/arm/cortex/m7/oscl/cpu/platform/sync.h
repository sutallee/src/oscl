/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_cpu_platform_synch_
#define _oscl_cpu_platform_synch_

#ifndef _oscl_cpu_synch_
#warning "NEVER include this file directly. #include oscl/cpu/sync.h instead."
#endif

#if 0
#include "oscl/driver/cortexm7/asm.h"

extern "C" {
	/** This operation ensures that the memory loads and stores 
		that appear before this operation indeed executed before
		those that come after this operation.  In short, loads
		and stores on either side of this operation cannot be
		re-ordered by either the processor or the compiler.
		(e.g. PowerPC eieio with "memory" clobber)
	 */
	inline void OsclCpuInOrderMemoryAccessBarrier() noexcept{
		/*	ARM Compared to the PowerPC eieio:
			The default rules for Device memory are generally
			sufficient, though a DMB may be required in some circumstances
			e.g. to enforce ordering between device accesses which are in
			different regions of Device memory.

			Therefore, I'm going to go ahead and use DMB here.
		 */
		Oscl::CortexM7::dmb();
		}

	/** This operation ensures that all instructions prior to
		its invocation have completed before the instructions
		subsequent to its invocation are executed.
		(e.g. PowerPC isync)
	 */
	inline void OsclCpuInstructionSynchronizeAccess() noexcept{
		Oscl::CortexM7::isb();
		}

	/** This operation ensures that all memory accesses initiated
		prior to its invocation have completed before the instructions
		subsequent to its invocation are executed.
		(e.g. PowerPC sync)
	 */
	inline void OsclCpuDataSynchronizeAccess() noexcept{
		Oscl::CortexM7::dmb();
		}

	}
#endif

#endif
