/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/



namespace Oscl {
namespace Platform {

/*
 * Returns true if the volatile global spin-lock is non-zero.
 * The inline implementation may always return false for non-SMP
 * systems.
 */
bool GlobalSpinLockIsBusy();

/*
 * User/Kernel space dependent function to prevent
 * signals/interrupts to the current thread.
 */
void  PreventAsync(AsyncState& save) noexcept;

/*
 * A mutual-exclusion primitive used by threads to
 * protect a shared resource. MUST NOT be invoked
 * from an interrupt service routine or a signal
 * handler (use suAcquireGlobalThreadLock instead.)
 */
void AcquireGlobalThreadLock(AsyncState& save) noexcept{
   for(;;){
      while(GlobalSpinLockIsBusy());
      PreventAsync(save);
      if(ReserveGlobalSpinLock()){
         if(TrySetGlobalSpinLock()){
            PostSpinLockSync();
            return;
            }
         }
      RestoreAsync(save);
      }
   }

/*
 * Equivalent of AcquireGlobalThreadLock, but to be used
 * on calls from signal/interrupt mode.
 *
 * In user-space, the suPreventAsync() and
 * suRestoreAsync() implementations are NULL, since
 * a thread there cannot be interrupted by another
 * signal. However, to allow for nested interrupts,
 * kernel-space implementations MUST disable disable-save
 * restore the processor's interrupt mask.
 *
 */
#ifdef KERNEL_SPACE
void suAcquireGlobalThreadLock(AsyncState& save) noexcept{
   for(;;){
      while(GlobalSpinLockIsBusy());
      suPreventAsync(save);
      if(ReserveGlobalSpinLock()){
         if(TrySetGlobalSpinLock()){
            PostSpinLockSync();
            return;
            }
         }
      suRestoreAsync(save);
      }
   }
#else // USER_SPACE equivalent
void suAcquireGlobalThreadLock(AsyncState& save) noexcept{
   for(;;){
      while(GlobalSpinLockIsBusy());
      if(ReserveGlobalSpinLock()){
         if(TrySetGlobalSpinLock()){
            PostSpinLockSync();
            return;
            }
         }
      }
   }
#endif

