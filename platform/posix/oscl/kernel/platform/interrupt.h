/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_kernel_platform_interrupth_
#define _oscl_kernel_platform_interrupth_

#ifndef _oscl_kernel_interrupth_
#warning "NEVER include this file directly. #include oscl/kernel/interrupt.h instead."
#endif

#if 0
#include "oscl/compiler/types.h"
#include "oscl/driver/powerpc/asm.h"
#else
//#include <semaphore.h>
#include <new>
#include <stdio.h>
#include "oscl/mt/posix/mutex.h"

#include "oscl/memory/block.h"

#endif

/** */
namespace Oscl {
/** */
namespace Kernel {

	/** */
	typedef Oscl::Unsigned16	IrqMaskState;

#if 0
	/** */
	extern sem_t	irqSema;
#endif

namespace Posix {
	// 
	void init() noexcept;
	};

}
}

#if 0
static void construct() __attribute__ ((constructor));

static void construct() {
	int
	result	= sem_init(
				&Oscl::Kernel::irqSema,
				0,	// pshared,
				0	// value
				);
	if(result){
		for(;;);
		}
	}
#endif

#if 0
extern "C"{
	/** */
	inline void	OsclKernelDisableInterrupts(Oscl::Kernel::IrqMaskState& save) noexcept{
#if 0
		int	result;
		for(;;){
			result	= sem_wait(&Oscl::Kernel::irqSema);
			if(result){
				// signal interrupt EINTR
				continue;
				}
			}
#else
		irqMutex->lock();
#endif
		}

	inline void	OsclKernelRestoreInterruptState(const Oscl::Kernel::IrqMaskState& savedLevel) noexcept{
#if 0
		int
		result	= sem_post(&Oscl::Kernel::irqSema);
		if(result){
			for(;;);
			}
#else
		irqMutex->unlock();
#endif
		}
	}
#endif

#endif
