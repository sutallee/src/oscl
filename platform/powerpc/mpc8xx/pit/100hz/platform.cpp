/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/timer/tocks.h"

/** This implementation of the platform specific function
	OsclTimerMillisecondsToTocks() assumes that the MPC8xx PIT
	has been configured for a 10ms period (100Hz frequencey).
 */ 
unsigned long	OsclTimerMillisecondsToTocks(unsigned long milliseconds) noexcept{
	// oscl/driver/motorola/mpc860p/device/device.cpp
	// The PIT is set to a period of approximately 10ms which
	// corresponds to a frequency of 100Hz.
	milliseconds	= (milliseconds)?milliseconds:1;// At least 1 millisecond
	const unsigned long	millisecondsPerTock	= 10;	// 10ms per tock
	milliseconds	+= (millisecondsPerTock-1);		// round up
	return milliseconds / millisecondsPerTock;		// 
	}

