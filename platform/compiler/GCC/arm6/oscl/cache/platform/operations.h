/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_cache_platform_operationsh_
#define _oscl_cache_platform_operationsh_

#ifndef _oscl_cache_operationsh_
#warning "NEVER include this file directly. #include oscl/cache/operations.h instead."
#endif

//
// The operations are implemented in an assembly language
// routine in oscl/platform/powerpc/{specific}/oscl/cache/platform/plat.S
//

extern "C" {
#if 0
	/** This operation flushes all dirty cache-lines 
		included in the specified range of addresses.
	 */
	inline void OsclCacheDataFlushRange(	void*			effectiveAddress,
											unsigned long	nBytes
											) noexcept{
		// Not implemented for this platform
		for(;;);
		}

	/** This operation invalidates all cache-lines 
		included in the specified range of addresses.
	 */
	inline void OsclCacheDataInvalidateRange(	void*			effectiveAddress,
												unsigned long	nBytes
												) noexcept{
		// FIXME: Not implemented for this platform
		for(;;);
		}

	/** This operation returns the size of the cache-line
		for the system.
	 */
	inline unsigned long	OsclCacheGetLineSize() noexcept{
		// I believe this is correct
		return 16;
#include <unistd.h>
		sysconf (_SC_LEVEL1_DCACHE_LINESIZE)
		}
#endif	
	};

#endif
