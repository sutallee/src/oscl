/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_atomic_platform_incdech_
#define _oscl_atomic_platform_incdech_

#ifndef _oscl_atomic_incdech_
#warning "NEVER include this file directly. #include oscl/atomic/incdec.h instead."
#endif

//#include "oscl/driver/arm/asm.h"

extern "C"{
#if 0
	/*
	 * This operation atomically increments the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicIncrement(volatile Oscl::Atomic::Integer* location) noexcept{
		while(true){
			unsigned long	value	= Oscl::Ppc::lwarx(location);
			if(Oscl::Ppc::stwcx(location,++value)){
				return value;
				}
			}
		}

	/*
	 * This operation atomically decrements the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicDecrement(volatile Oscl::Atomic::Integer* location) noexcept{
		while(true){
			unsigned long	value	= Oscl::Ppc::lwarx(location);
			if(Oscl::Ppc::stwcx(location,--value)){
				return value;
				}
			}
		}
#else
	/*
	 * This operation atomically increments the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicIncrement(volatile Oscl::Atomic::Integer* location) noexcept{
		unsigned long tmp;
		int	result;
//			"@ atomic_inc "\n"
		__asm__ __volatile__(
			"1:	ldrex	%0, [%3]\n"
			"	add 	%0, %0, #1\n"
			"	strex	%1, %0, [%3]\n"
			"	teq	%1, #0\n"
			"	bne	1b"
			: "=&r" (result), "=&r" (tmp), "+Qo" (*location)
			: "r" (location)
			: "cc"
			);
		return result;
		}

	/*
	 * This operation atomically decrements the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicDecrement(volatile Oscl::Atomic::Integer* location) noexcept{
		unsigned long tmp;
		int	result;
		__asm__ __volatile__(
			"1:	ldrex	%0, [%3]\n"
			"	sub 	%0, %0, #1\n"
			"	strex	%1, %0, [%3]\n"
			"	teq	%1, #0\n"
			"	bne	1b"
			: "=&r" (result), "=&r" (tmp), "+Qo" (*location)
			: "r" (location)
			: "cc"
			);
		return result;
	// "=&r" means :
	//	"=" Means that this operand is write-only for this instruction: the previous value is discarded and replaced by output data.
	//	"&" This operand may not lie in a regiter that i used as an input operand or as part of any memory address.
	//	"r" A register operand is allowd provided it is in a general register.
	// "Ir"
	//	"I" immediate integer operands with explicit range of values 1 to 8.
	//			ARM: Integer that is valid as an immediate operand in a data processing instruction.  That is, an integer in the range 0 to 255 rotated by a multiple of 2
	//	"r" A register operand is allowd provided it is in a general register.
	// "+Qo"
	//	"+" Means that this operand is both read and written by the instruction.
	//	"Q" A memory reference where the exact address is in a single register (''m'' is preferable for 'asm' statements)
	//	"o"  Amemory operand is allowed, but only if the address is "offsettable".  This means that adding a small integer (actually, the width in bytes of the operand, as determined by its machine mode) may be added to the address and the result is also a valid memory address.

	// A colon separates the assembler template from the first output operand and another separates the last output operand from the first input, if any.
	// Commas separate the operands within each group.
	// If there are no output operands but there are input operands, you must place two consecutive colons surrounding the place where the output operands would go.
	// Some instructions clobber specific hard registers.  To describe this, write a third colon after the input operands, followed by the names of the clobbered hard registers (given as strings).
	// If your assembler instruction can alter the condition code register, add 'cc' to the list of clobbered registers.  GCC on some machines represents the condition codes as a specific hardware register; 'cc' serves to name this register.  On other machines, the condition code is handled differently, and specifying 'cc' has no effect.  But it is valid no matter what the machine.
		}
#endif
	}

#endif
