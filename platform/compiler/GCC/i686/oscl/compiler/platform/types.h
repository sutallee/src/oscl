/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_compiler_platform_typesh_
#define _oscl_compiler_platform_typesh_

#include <stdint.h>

#ifndef _oscl_compiler_typesh_
#warning "NEVER include this file directly. #include oscl/compiler/types.h instead."
#endif

#define READONLY	volatile
#define WRITEONLY	volatile

/** */
namespace Oscl {

typedef uint8_t	Octet;
typedef int8_t			Byte;
typedef int8_t			SByte;
typedef uint8_t	UByte;
typedef int8_t			Bool;
typedef UByte			PAD;
typedef UByte			Reg8;

typedef uint8_t 	Flags;
typedef uint8_t 	Fields;
typedef unsigned 		Field;
typedef unsigned 		Flag;
typedef int8_t		Integer8;
typedef uint8_t	Unsigned8;
typedef int16_t			Integer16;
typedef uint16_t	Unsigned16;
typedef int16_t			Signed16;
typedef uint32_t	Unsigned32;
typedef int32_t			Signed32;
typedef int64_t		Signed64;
typedef uint16_t	Unsigned64;
typedef uintptr_t	UnsignedPtr;

}

#endif
