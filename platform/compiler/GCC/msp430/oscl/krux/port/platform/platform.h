/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_port_platform_platformh_
#define _oscl_krux_port_platform_platformh_

/** This file shadows the platform.h file. It implements
	inline versions of some of the functions described in that file.
 */

extern "C"{
	/** */
	inline void OsclKruxTrapWithArg(void* arg) noexcept{
		// Setup the stack in the same way that an interrupt does,
		// and then disable interrupts and jump to the system
		// call routine.
		asm volatile(
						// This code "emulates" a trap.
						// I *could* save some interrupt latency time by
						// creating the stack frame and pushing the general
						// registers first and then disabling interrupts
						// before pushing the SR... but I'm lazy now, and
						// this emulates more like an actual trap.
						//
						// I *must* disable interrupts before saving the SR!
						"	mov		%0,r15\n"	// Get the argument in R15
						"	push.w	#0f\n"	// Push the absolute return address
						"	dint\n"						// disable interrupts
						"	nop\n"						// disable interrupts
						"	mov		r2,r14\n"			// Get SR to R14
						"	bis.w	#0x0008,r14\n"		// Enable interrupts in SR copy
						"	push.w	r14\n"				// Push the SR copy
						"	br		#__msp430Trap0\n"
						"0:\n"
						:
                        : "r" (arg)
                        : "r14","r15"	// I use R14 to manipulate the SR.
                        );
		}
	}

#endif
