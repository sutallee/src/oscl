/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_atomic_platform_typesh_
#define _oscl_atomic_platform_typesh_

#ifndef _oscl_atomic_typesh_
#warning "NEVER include this file directly. #include oscl/atomic/types.h instead."
#endif

/** This file shadows the platform/platform.h file. It implements
	inline versions of some of the functions described in that file.
 */

#include "oscl/compiler/types.h"

/** */
namespace Oscl {
/** */
namespace Atomic {

	/** FIXME: I'm not sure about this yet */
	typedef Oscl::Signed16		Integer;

	/** */
	typedef Oscl::Signed16		SignedInt;

	/** */
	typedef Oscl::Unsigned16	UnsignedInt;

}
}

#endif
