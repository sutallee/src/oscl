/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_compiler_platform_typesh_
#define _oscl_compiler_platform_typesh_

#ifndef _oscl_compiler_typesh_
#warning "NEVER include this file directly. #include oscl/compiler/types.h instead."
#endif

/** This file describes primitive data types that are size dependent
	on various combinations of CPU and compiler.
 */

namespace Oscl {

typedef unsigned char	Octet;
typedef char			Byte;
typedef char			SByte;
typedef unsigned char	UByte;
typedef char			Bool;
typedef UByte			PAD;
typedef UByte			Reg8;

typedef unsigned char 	Flags;
typedef unsigned char 	Fields;
typedef unsigned 		Field;
typedef unsigned 		Flag;
typedef signed char		Integer8;
typedef unsigned char	Unsigned8;
typedef short			Integer16;
typedef unsigned short	Unsigned16;
typedef short			Signed16;
typedef unsigned long	Unsigned32;
typedef long long		Signed64;
typedef unsigned long long	Unsigned64;
typedef long			Signed32;
typedef unsigned long	UnsignedPtr;
typedef float			Float32;
typedef double			Float64;

}

#endif
