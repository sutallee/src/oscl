/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_atomic_platform_incdech_
#define _oscl_atomic_platform_incdech_

#ifndef _oscl_atomic_incdech_
#warning "NEVER include this file directly. #include oscl/atomic/incdec.h instead."
#endif

#include "oscl/driver/powerpc/asm.h"

extern "C"{
#if 0
	/*
	 * This operation atomically increments the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicIncrement(volatile Oscl::Atomic::Integer* location) noexcept{
		while(true){
			unsigned long	value	= Oscl::Ppc::lwarx(location);
			if(Oscl::Ppc::stwcx(location,++value)){
				return value;
				}
			}
		}

	/*
	 * This operation atomically decrements the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicDecrement(volatile Oscl::Atomic::Integer* location) noexcept{
		while(true){
			unsigned long	value	= Oscl::Ppc::lwarx(location);
			if(Oscl::Ppc::stwcx(location,--value)){
				return value;
				}
			}
		}
#endif
	}

#endif
