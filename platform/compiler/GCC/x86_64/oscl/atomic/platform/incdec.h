/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_atomic_platform_incdech_
#define _oscl_atomic_platform_incdech_

#ifndef _oscl_atomic_incdech_
#warning "NEVER include this file directly. #include oscl/atomic/incdec.h instead."
#endif

#if 0
static inline uint32_t atomic_inc(uint32_t volatile* ptr) {
    uint16_t value	= 1;
    __asm__("lock xadd %w0, %w1" : "+r" (value) : "m" (*ptr));
    return ++value;
	}
#endif

extern "C"{
	/*
	 * This operation atomically increments the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicIncrement(volatile Oscl::Atomic::Integer* location) noexcept{
		int	result	= 1;

            asm volatile (
				"lock xaddl %0, %1\n"
				: "+r" (result), "+m" (*(location))
				: : "memory", "cc"
				);

		return result + 1;
		}

	/*
	 * This operation atomically decrements the specified location.
	 */
	inline Oscl::Atomic::Integer OsclAtomicDecrement(volatile Oscl::Atomic::Integer* location) noexcept{
		int	result	= -1;

            asm volatile (
				"lock xaddl %0, %1\n"
				: "+r" (result), "+m" (*(location))
				: : "memory", "cc"
				);

		return result - 1;
		}
	}

#endif
