/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_target_frameh_
#define _oscl_target_frameh_
#include "oscl/compiler/types.h"

/**
	From the ARM-ABI document:
		r0-r3 - argument/return values (volatile)
		r4-r8,r10,r11,SP - callee saved, local subroutine variables
		A subroutine must preserve the contents of registers r4-r8,r10,r11,SP
		and possbilby r9.
		r9 is platform specific (may be callee saved)
		r12 - linker scratch register, and may be used within a route
				to hold intermediate values between subroutine calls.
 */

typedef struct OsclKruxFrame {
	Oscl::Unsigned32	pad[6];
	/** General Purpose Registers */
	Oscl::Unsigned32	r[32];
	/** CP0 Status */
	Oscl::Unsigned32	status;
	Oscl::Unsigned32	hi;
	Oscl::Unsigned32	lo;
	/** CP0 BadVAddr */
	Oscl::Unsigned32	badVAddr;	// Not used/saved!
	/** CP0 Cause */
	Oscl::Unsigned32	cause;
	/** CP0 EPC */
	Oscl::Unsigned32	epc;
	}OsclKruxFrame;

#endif /* _target_frameh_ */

