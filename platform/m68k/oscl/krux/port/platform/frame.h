/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_target_frameh_
#define _oscl_krux_target_frameh_
#include <stdint.h>

	/** This data structure describes the thread stack frame
		used by the Krux RTOS.
	 */
	typedef struct OsclKruxFrame{
		uint32_t	d0;
		uint32_t	d1;
		uint32_t	d2;
		uint32_t	d3;
		uint32_t	d4;
		uint32_t	d5;
		uint32_t	d6;
		uint32_t	d7;
		uint32_t	a1;
		uint32_t	a2;
		uint32_t	a3;
		uint32_t	a4;
		uint32_t	a5;
		uint32_t	a6;
		uint32_t	a0;
		uint16_t	sr;
		void		(*pc)(void);	/* Program Counter at time of KS or interrupt */
		uint16_t	format;
		}OsclKruxFrame;

#endif

