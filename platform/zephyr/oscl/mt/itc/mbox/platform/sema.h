/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_platform_semah_
#define _oscl_mt_itc_mbox_platform_semah_
#include "kernel.h"
#include "oscl/mt/sema/api.h"
#include "oscl/mt/itc/mbox/platform/sema.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
class MailboxSema : public Oscl::Mt::Sema::Api {
	private:
		/** */
		struct k_sem	_sema;

	public:
		/** */
		MailboxSema() noexcept;

		/** */
		~MailboxSema();

		/** */
		inline void	signal() noexcept{
			k_sem_give(&_sema);
			}

		/** */
		inline void	wait() noexcept{
			k_sem_take(&_sema,K_FOREVER);
			}

		/** */
		inline void	suSignal() noexcept{
			k_sem_give(&_sema);
			}
	};

}
}
}

#endif
