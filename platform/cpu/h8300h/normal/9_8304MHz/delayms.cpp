/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/timer/busywait.h"

/** This implementation of the platform specific function
	OsclTimerBusyWaitDelay() assumes that the H8/300H is operating
	at 9.8304MHz in "normal" mode.
 */ 
void OsclTimerBusyWaitDelay(unsigned long milliseconds) noexcept {
	// Assuming 1 instruction per clock cycle @ 50MHz
	milliseconds	= (milliseconds)?milliseconds:1;// At least 1 millisecond
	for(;milliseconds;--milliseconds){
		const unsigned long		delay	= 1638400/1000 + 1;
		// total time per loop = 6 states @ 9.8304MHz = 6/9.8304MHz = 610.4ns = 1638400Hz
		asm volatile (
						"0:\n"
						// States = I x Si + J x Sj + K x Sk + L x Sl + M x Sm + N x Sn
						// dec.w States = 1 x Si + 0 x Sj + 0 x Sk + 0 x Sl + 0 x Sm + 0 x Sn
						// dec.w States = 1 x Si
						// dec.w States = 1 x 2 = 2 states
						"	dec.l	#1,%r0\n"	// 2 bytes, 2 states
						// bne d:8 = 2 x Si + 0 x Sj + 0 x Sk + 0 x Sl + 0 x Sm + 0 x Sn
						// bne d:8 = 2 x Si
						// bne d:8 = 2 x 2 = 4 states
						"	bne		0b:8\n"		// 2 bytes, 4 states
						: : "r" (delay)
						);
		}
	}

