/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/timer/busywait.h"

/** This implementation of the platform specific function
	OsclTimerBusyWaitDelay() assumes that the MSP430 is operating
	at 800KHz and executing 1 instruction per 800KHz clock cycle.
 */ 
void OsclTimerBusyWaitDelay(unsigned long milliseconds) noexcept {
	// Assuming 1 instruction per clock cycle @ 800KHz
	const unsigned long	clkFreq=800000;		// Hz
	const unsigned long	loopsPerMillisecond=5500;	// determined emphirically over 1 minute
	milliseconds	= (milliseconds)?milliseconds:1;// At least 1 millisecond
	for(;milliseconds;--milliseconds){
		unsigned long		delay	= clkFreq/loopsPerMillisecond;
		for(;delay;--delay);
		}
	}

