/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_fs_file_fgetsh_
#define _oscl_zephyr_fs_file_fgetsh_

#include "oscl/fs/file/api.h"

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace FS {
/** */
namespace File {

/*
	fgets()  reads in at most one less than size characters from stream and
	stores them into the buffer pointed to by s.
	Reading stops after an EOF or a newline.
	If a newline is read, it is stored into the buffer.
	A terminating null byte ('\0') is stored after the
	last character in the buffer.
*/
char*	fgets(
			char*					s,
			int						size,
			Oscl::FS::File::Api&	file
			);
}
}
}
}

#endif

