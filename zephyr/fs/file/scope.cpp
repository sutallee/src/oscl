/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#if KERNEL_VERSION_NUMBER >= ZEPHYR_VERSION(3,1,0)
#include <zephyr/fs/fs.h>
#else
#include <fs/fs.h>
#endif
#include <errno.h>
#include <string.h>
#include "scope.h"
#include "oscl/strings/base.h"

using namespace Oscl::Zephyr::FS::File;

Scope::Scope(
	const char* fileName,
	bool		create
	) noexcept{

	if(!create){
		struct fs_dirent	entry;
		_openResult	= fs_stat(
						fileName,
						&entry
						);

		if(_openResult < 0){
			return;
			}
		if(entry.type != FS_DIR_ENTRY_FILE){
			// The fileName exists, but it is no a file.
			_openResult	= -EISDIR;
			return;
			}
		}

	_openResult	= fs_open(
					&_file,
					fileName
					#if KERNEL_VERSION_NUMBER >= ZEPHYR_VERSION(2,4,0)
					,
					FS_O_CREATE | FS_O_RDWR
					#endif
					);
	}

Scope::Scope() noexcept:
		_openResult(-ENOENT)
		{
	}

Scope::~Scope() noexcept{
	if(_openResult){
		return;
		}

	fs_close(&_file);
	}

int	Scope::openResult() noexcept{
	return _openResult;
	}

bool	Scope::mkstemp(char* templateName) noexcept{

	if(!_openResult){
		// The current is already open. Close it.
		fs_close(&_file);
		_openResult	= -ENOENT;
		}

	int
	templateNameLen	= strlen(templateName);

	if(templateNameLen < 6){
		// The last six characters of the string
		// are replaced by the new name. We must
		// have space for that and we don't.
		return true;
		}

	int
	baseNameLength	= templateNameLen-6;

	for(unsigned i=0;i<(1<<16);++i){
		Oscl::Strings::Base
			t(
				&templateName[baseNameLength],
				6+1
				);

		t	+= i;

		struct fs_dirent	entry;

		int
		result	= fs_stat(
					templateName,
					&entry
					);

		if(result < 0){
			if(result != -ENOENT){
				// Something wierd happend.
				return true;
				}
			}

		// At this point we have determined that
		// there is no entity with our chosen name.

		_openResult	= fs_open(
						&_file,
						templateName
						#if KERNEL_VERSION_NUMBER >= ZEPHYR_VERSION(2,4,0)
						,
						FS_O_CREATE | FS_O_RDWR
						#endif
						);

		return _openResult?true:false;
		}

	// Out of numbers!
	// This should never happen.
	return true;
	}

ssize_t	Scope::read(
			void*	ptr,
			size_t	size
			) noexcept{
	return fs_read(
			&_file,
			ptr,
			size
			);
	}

ssize_t	Scope::write(
			const void*	ptr,
			size_t		size
			) noexcept{
	return fs_write(
			&_file,
			ptr,
			size
			);
	}

int	Scope::seekFromBeginning(
		off_t	offset
		) noexcept{
	return fs_seek(
			&_file,
			offset,
			FS_SEEK_SET
			);
	}

int	Scope::seekFromCurrentPosition(
		off_t	offset
		) noexcept{
	return fs_seek(
			&_file,
			offset,
			FS_SEEK_CUR
			);
	}

int	Scope::seekFromEnd(
		off_t	offset
		) noexcept{
	return fs_seek(
			&_file,
			offset,
			FS_SEEK_END
			);
	}

off_t	Scope::tell() noexcept{
	return fs_tell(&_file);
	}


int	Scope::truncate(off_t length) noexcept{
	return fs_truncate(
			&_file,
			length
			);
	}

int	Scope::sync() noexcept{
	return fs_sync(&_file);
	}

