/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_fs_file_scopeh_
#define _oscl_zephyr_fs_file_scopeh_

#include "oscl/fs/file/api.h"
#include <fs/fs.h>

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace FS {
/** */
namespace File {

/**	This class is used to operate on a particular file.
	Upon construction, the file is opened.
	Upon destruction, the file is closed.
 */
class Scope : public Oscl::FS::File::Api {
	private:
		/** */
		struct fs_file_t	_file;

		/** */
		int		_openResult;

	public:
		/** The constructor will open the file
			specified by fileName.
			If the create parameter is true,
			the file will be created if it does
			not already exist.
			If the create parameter is false
			and the file does not exist, the
			file will not be opened and -ENOENT
			will be returnedd by the openResult()
			operation.
		 */
		Scope(
			const char*	fileName,
			bool		create	= false
			) noexcept;

		/** This constructor can be used to create
			a scope object, which does *not* open
			a file.
			The openResult() operation will always
			return -ENOENT until another operation
			operation (e.g. mkstemp()) is used to
			successfully (e.g. mkstemp) open a file.
		 */
		explicit Scope() noexcept;

		/** */
		~Scope() noexcept;

		/**	This operation should be used by the
			application after construction to determine
			the result of the fs_open() operation.
			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	openResult() noexcept;

		/** This operation is used to open a file based
			with a unique name based on the templateName.
			If this object is already opened, then the
			existing file will be closed and the file
			with the new name will be opened.

			RETURN:
				false:	success
				true: 	failure
		 */
		bool	mkstemp(char *templateName) noexcept;

	public: // Api
		/**	Reads items of data of size bytes long.
			RETURN:	
				Number of bytes read.
				On success, it will be equal to number of items
				requested to be read.
				Returns less than number of bytes requested if
				there are not enough bytes available in file.
				Will return -ERRNO code on error.
		 */
		ssize_t	read(
					void*	ptr,
					size_t	size
					) noexcept override;

		/**	Writes items of data of size bytes long.
			RETURN:
				Number of bytes written.
				On success, it will be equal to the number
				of bytes requested to be written.
				Any other value, indicates an error.
				Will return -ERRNO code on error.
				In the case where -ERRNO is returned, the file pointer
				will not be advanced because it couldn’t start the operation.
				In the case where it is able to write, but is not able to
				complete writing all of the requested number of bytes,
				then it is because the disk got full.
				In that case, it returns less number of bytes written
				than requested, but not a negative -ERRNO value as
				in regular error case.
		 */
		ssize_t	write(
					const void*	ptr,
					size_t		size
					) noexcept override;

		/**	Moves the file position to a new location in the file.
			The offset is added to beginning-file position.

			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	seekFromBeginning(
				off_t	offset
				) noexcept override;

		/**	Moves the file position to a new location in the file.
			The offset is added to current-file position.

			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	seekFromCurrentPosition(
				off_t	offset
				) noexcept override;

		/**	Moves the file position to a new location in the file.
			The offset is added to end-of-file position.

			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	seekFromEnd(
				off_t	offset
				) noexcept override;

		/**	Retrieves the current position in the file.
			RETURN:
				Current position in file.
				WARNING: Current revision does not validate the file object.
		 */
		off_t	tell() noexcept override;

		/**	Truncates the file to the new length if it is
			shorter than the current size of the file.
			Expands the file if the new length is greater
			than the current size of the file.
			The expanded region would be filled with zeroes.

			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	truncate(off_t length) noexcept override;

		/**	This function can be used to flush the cache
			of an open file.
			This can be called to ensure data gets written
			to the storage media immediately.
			This may be done to avoid data loss if power
			is removed unexpectedly.
			Note that closing a file will cause caches
			to be flushed correctly so it need not be
			called if the file is being closed.

			RETURN:
				0: Success
				-ERRNO: errno code if error.
		 */
		int	sync() noexcept override;
	};

}
}
}
}

#endif

