/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_zephyr_poll_timerh_
#define _oscl_zephyr_poll_timerh_

#include "kernel.h"

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace Poll {

/**	This interface is called by a select service whenever
	the specified timer expires.
 */
class Timer {
	public:
		/** For use by the select service to keep the timer in a list.
			The implementation of this interface must *never* use this field.
 		 */
		void*	__qitemlink;

		/** For use by the select service to keep the timer in a list.
			The implementation of this interface must *never* use this field.
 		 */
		void*	__prev;

	public:
		/** */
		unsigned long		_durationInMs;

	public:
		/** Make GCC happy */
		virtual ~Timer(){}

		/**	This  operation is invoked by the select service when the timer
			expires. The timer is removed from the select service timer
			list before this operation is invoked. Therefore, it is safe
			for the implementation to re-use this timer.
			The implementation may modify the file-descriptor set that
			will be used in the next select call.
			NOTE: This operation is *always* invoked from within the
			select service thread!
		 */
		virtual void	timeout() noexcept=0;
	};

/** This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name clashes.
	This template solves the problem by implementing
	the interface and then invoking member function
	pointers in the context instead.
	One instance of this object is created in the context
	for each interface of this type used in the context.
	Each instance is constructed such that it refers to different
	member functions within the context.
	The interface of this object is passed to any
	object that requires the interface. When the
	object invokes any of the operations of the API,
	the corresponding member function of the context
	will subsequently be invoked.
 */
template <class Context>
class TimerComposer : public Timer {
	public:
		/** This type defines the function signature
			for a context member operation that
			updates the value of a uint8_t variable.
		 */
		typedef void    (Context::*Timeout)();

	private:
        /** A reference to the Context, which contains
			the done callback member function.
		 */
		Context&			_context;

	private:
		/** A pointer to a member function within the 
			Context that will be invoked when the
			timeout operation of the interface
			inherited by this class is invoked.
		 */
		Timeout				_timeout;

	public:
		/** The constructor requires:
			context -
				A reference to the Context of which
				this class is a member and which contains
				the done callback member function.
			timeout-
				A pointer to a Context member function
				that is invoked when the timer expires.
		 */
		TimerComposer(
			Context&							context,
			Timeout								timeout
			) noexcept;

	private: // Oscl::Zephyr::Poll::Timer
		/** This operation is called when the timer expires.
		 */
		void	timeout() noexcept;
	};

template <class Context>
TimerComposer<Context>::
TimerComposer(
	Context&	context,
	Timeout		timeout
	) noexcept:
		_context(context),
		_timeout(timeout)
		{
	}

template <class Context>
void	TimerComposer<Context>::timeout() noexcept{
	(_context.*_timeout)();
	}
}
}
}

#endif
