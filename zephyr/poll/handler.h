/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_poll_handlerh_
#define _oscl_zephyr_poll_handlerh_

#include "kernel.h"

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace Poll {

/**	This interface is called by a select thread whenever
	the Zephyr k_poll call returns indicating a change
	in one of its file descriptors.
 */
class Handler {
	public:
		/** For use by the select thread to keep API in a list.
			The implementation of this interface should *never* use this field.
 		 */
		void*	__qitemlink;

		/** This field may used by the server while the handler
			is attached and must never be used by the client.
		 */
		void*	_serverData;

	public:
		/** Make GCC happy */
		virtual ~Handler(){}

		/**	This  operation is invoked when the handler is attached
			to allow the implementation to initialize its event.
			This operation is only invoked when the operation
			is established.
		 */
		virtual void	start(struct k_poll_event& event) noexcept=0;

		/**	When this  operation is invoked when the handler is
			detached.
		 */
		virtual void	stop() noexcept=0;

		/**	This operation is invoked whenever the select returns
			and the state of the event indicates that it is pending.
			The implementation then performs any I/O based on the
			event type and resets the state of the event.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the event list.
			If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after
			this operation returns.

			The handler returns FALSE if it wishes to remain
			attached to the event list.
		 */
		virtual bool	process(struct k_poll_event& event) noexcept = 0;
	};

/** This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name clashes.
	This template solves the problem by implementing
	the interface and then invoking member function
	pointers in the context instead.
	One instance of this object is created in the context
	for each interface of this type used in the context.
	Each instance is constructed such that it refers to different
	member functions within the context.
	The interface of this object is passed to any
	object that requires the interface. When the
	object invokes any of the operations of the API,
	the corresponding member function of the context
	will subsequently be invoked.
 */
template <class Context>
class HandlerComposer : public Handler {
	public:
		/** This type defines the function signature
			for a context member operation that
			starts the Handler.
		 */
		typedef void    (Context::*Start)(struct k_poll_event& event);

		/** This type defines the function signature
			for a context member operation that
			starts the Handler.
		 */
		typedef void    (Context::*Stop)();

		typedef bool	(Context::*Process)(
							struct k_poll_event&	next
							);

	private:
        /** A reference to the Context, which contains
			the done callback member function.
		 */
		Context&			_context;

	private:
		/** A pointer to a member function within the 
			Context that will be invoked when the
			start operation of the interface
			inherited by this class is invoked.
		 */
		Start				_start;

		/** A pointer to a member function within the 
			Context that will be invoked when the
			stop operation of the interface
			inherited by this class is invoked.
		 */
		Stop				_stop;

		/** A pointer to a member function within the 
			Context that will be invoked when the
			handlIO operation of the interface
			inherited by this class is invoked.
		 */
		Process			_process;

	public:
		/** The constructor requires:
			context -
				A reference to the Context of which
				this class is a member and which contains
				the done callback member function.
			timeout-
				A pointer to a Context member function
				that is invoked when the timer expires.
		 */
		HandlerComposer(
			Context&	context,
			Start		start,
			Stop		stop,
			Process		process
			) noexcept;

	private: // Oscl::Zephyr::Poll::Handler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	start(struct k_poll_event& event) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	stop() noexcept;

		/**	This operation is invoked whenever the select returns
			and the state of the event indicates that it is pending.
			The implementation then performs any I/O based on the
			event type and resets the state of the event.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the event list.
			If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after
			this operation returns.

			The handler returns FALSE if it wishes to remain
			attached to the event list.
		 */
		bool	process(struct k_poll_event& event) noexcept;
	};

template <class Context>
HandlerComposer<Context>::
HandlerComposer(
	Context&	context,
	Start		start,
	Stop		stop,
	Process		process
	) noexcept:
		_context(context),
		_start(start),
		_stop(stop),
		_process(process)
		{
	}

template <class Context>
void	HandlerComposer<Context>::start(struct k_poll_event& event) noexcept{
	(_context.*_start)(event);
	}

template <class Context>
void	HandlerComposer<Context>::stop() noexcept{
	(_context.*_stop)();
	}

template <class Context>
bool	HandlerComposer<Context>::process(
			struct k_poll_event&	event
			) noexcept{
	return (_context.*_process)(event);
	}

}
}
}

#endif
