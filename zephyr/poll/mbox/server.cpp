/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "oscl/mt/scopesync.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"
#include "kernel.h"

using namespace Oscl::Zephyr::Poll;

Server::Server(
	struct k_poll_event*	events,
	unsigned				nEvents
	) noexcept:
		_events(events),
		_nEvents(nEvents),
		_mboxSemaHandler(
			*this,
			&Server::mboxSemaStart,
			&Server::mboxSemaStop,
			&Server::mboxSemaProcess
			)
		{
	initializeEvents();
	k_sem_init(
		&_mboxSema,
		0,	// initial count
		1	// binary semaphore
		);
	}

Server::~Server(){
	}

#if 0
Oscl::Zephyr::Poll::Req::Api::SAP&	Server::getSAP() noexcept{
	return _sync.getSAP();
	}

Oscl::Zephyr::Poll::Api&	Server::getSyncApi() noexcept{
	return _sync;
	}

Oscl::Mt::Itc::PostMsgApi&	Server::getPostMsgApi() noexcept{
	return *this;
	}
#endif

void	Server::initializeEvents() noexcept{
	for(unsigned i=0;i<_nEvents;++i){
		k_poll_event_init(
			&_events[i],
			K_POLL_TYPE_IGNORE,	// type
			K_POLL_MODE_NOTIFY_ONLY,	// mode
			0	// obj FIXME: is zero/NULL legal for K_POLL_TYPE_IGNORE?
			);
		}
	}

struct k_poll_event*	Server::allocEvent() noexcept{
	for(unsigned i=0;i<_nEvents;++i){
		struct k_poll_event&	event	= _events[i];

		if(!event.obj && (event.type == K_POLL_TYPE_IGNORE)){
			return &event;
			}
		}

	/*	This should never happen if enough
		events were allocated by the context.
		Add more events in the context.
	 */
	Oscl::ErrorFatal::logAndExit(
		"%s: Event allocation failed!\n",
		__PRETTY_FUNCTION__
		);

	return 0;
	}

Oscl::Mt::Itc::PostMsgApi&	Server::getPostMsgApi() noexcept{
	return *this;
	}

void	Server::attach(Handler&	handler) noexcept{

	struct k_poll_event*
	event	= allocEvent();

	// NOTE: The allocEvent() generates a fatal
	// error if an event cannot be allocated.
	// Therefore, we don't for a NULL event here.

	handler._serverData	= event;

	handler.start(*event);

	_pollQ.put(&handler);
	}

void	Server::detach(Handler&	handler) noexcept{

	if(_pollQ.remove(&handler)){

		struct k_poll_event*
		event	= (struct k_poll_event*)handler._serverData;

		if(event){
			k_poll_event_init(
				event,
				K_POLL_TYPE_IGNORE,	// type
				K_POLL_MODE_NOTIFY_ONLY,	// mode
				0	// obj FIXME: is zero/NULL legal for K_POLL_TYPE_IGNORE?
				);
			}

		handler.stop();
		}
	}

void	Server::start(Timer& timer) noexcept{
	Timer*	next;
	for(
		next	= _timerList.first();
		next;
		next	= _timerList.next(next)
		){

		if(timer._durationInMs < next->_durationInMs){

			next->_durationInMs	= next->_durationInMs - timer._durationInMs;

			_timerList.insertBefore(next,&timer);

			return;
			}
		timer._durationInMs	= timer._durationInMs - next->_durationInMs;
		}

	_timerList.put(&timer);
	}

void	Server::stop(Timer& timer) noexcept{
	Timer*	next;

	for(
		next=_timerList.first();
		next;
		next	= _timerList.next(next)
		){

		if(&timer == next){

			Timer*	nt	= _timerList.next(next);

			if(nt){
				nt->_durationInMs	= nt->_durationInMs + timer._durationInMs;
				}

			_timerList.remove(&timer);

			break;
			}
		}
	}

unsigned long	Server::calculateNextTimeoutInMs() noexcept{
	Timer*	timer	= _timerList.first();

	if(!timer){
		return 0;
		}

	_timeoutInMs	= timer->_durationInMs;

	return _timeoutInMs;
	}

void	Server::updateTimeout(int64_t deductionInMs) noexcept{
	if(!deductionInMs){
		return;
		}

	Timer*
	timer	= _timerList.first();

	if(deductionInMs >= timer->_durationInMs){
		// We use one to ensure that we get a timeout.
		timer->_durationInMs	= 1;
		return;
		}

	timer->_durationInMs	-= deductionInMs;
	}

void	Server::processTimeout() noexcept{
	// The processTimeout() is called because k_poll returned -EAGAIN,
	// so we can assume that the first timer in the list has expired.
	Timer*
	timer	= _timerList.get();

	if(!timer){
		return;
		}

	Oscl::Queue<
		Oscl::Zephyr::Poll::Timer
		>	expiredList;

	expiredList.put(timer);

	// The _timerList is sorted such that the first entry
	// in the list is the first to expire. Subsequent entries
	// that expire at the same time will have a zero _duration.

	// This is tricky. I expect that the remaining
	// timer list may contain some expired items at
	// the beginning of the list which have zero durations.
	while((timer = _timerList.get())){
		if(timer->_durationInMs){
			// The timer that was at the head of the list
			// is not expired, so we push it back into the
			// queue and we're done.
			_timerList.push(timer);
			break;
			}
		else {
			// The timer that was at the head of the list
			// *has* expired, so we put it in the expired
			// list for later processing.
			expiredList.put(timer);
			}
		}

	// Finally, we notify all of the expired timers.
	while((timer = expiredList.get())){
		timer->timeout();
		}
	}

void	Server::processMsgQ() noexcept{
	for(;;){
		Oscl::Mt::Itc::Msg*	msg;
			{
			Oscl::Mt::ScopeSync	sync(_mutex);
			msg = _mboxQ.get();
			}
		if(msg){
			msg->process();
			}
		else {
			break;
			}
		}
	}

void	Server::post(Oscl::Mt::Itc::Msg& msg) noexcept{
		{
		Oscl::Mt::ScopeSync	sync(_mutex);
		_mboxQ.put(&msg);
		signal();
		}
	}

void	Server::postSync(Oscl::Mt::Itc::Msg& msg) noexcept{
	post(msg);
	Oscl::Mt::Thread::getCurrent().syncWait();
	}

void	Server::signal(void) noexcept{
	k_sem_give(&_mboxSema);
	}

void	Server::suSignal(void) noexcept{
	k_sem_give(&_mboxSema);
	}

void Server::run() noexcept{

	initialize();

	for(;;){
		int	result;

		int64_t	startTime	= k_uptime_get();

		unsigned long	timeout	= calculateNextTimeoutInMs();

		result	= k_poll(
					_events,
					_nEvents,
					timeout?K_MSEC(timeout):K_FOREVER
					);

		int64_t	duration	= k_uptime_delta(&startTime);

		updateTimeout(duration);

		if(result < 0) {
			if(result == -EAGAIN) {
				// timer expired
				processTimeout();
				continue;
				}
			if(result == -EINTR) {
				/*
					Polling has been interrupted, e.g. with k_queue_cancel_wait().
					All output events are still set and valid, cancelled event(s)
					will be set to K_POLL_STATE_CANCELLED.
					In other words, -EINTR status means that at least one of output
					events is K_POLL_STATE_CANCELLED. 
				 */
				// FIXME: process K_POLL_STATE_CANCELLED events.
				Oscl::ErrorFatal::logAndExit(
					"%s: NOT IMPLEMENTED!\n",
					__PRETTY_FUNCTION__
					);
				continue;	// not really
				}
			if(result == -ENOMEM) {
				/*
					Thread resource pool insufficient memory (user mode only).
					FIXME: How do we handle this?
				 */
				Oscl::ErrorFatal::logAndExit(
					"%s: Thread resource pool insufficient memory for k_poll().\n",
					__PRETTY_FUNCTION__
					);
				continue;	// not really
				}
			if(result == -EINVAL) {
				// Bad parameters
				Oscl::ErrorFatal::logAndExit(
					"%s: Bad parameters to k_poll().\n",
					__PRETTY_FUNCTION__
					);
				continue;	// not really
				}
			else {
				Oscl::ErrorFatal::logAndExit(
					"%s: Unknown result from k_poll(): %d.\n",
					__PRETTY_FUNCTION__,
					result
					);
				break;
				}
			}

		processIoHandlers();
		}
	}

void Server::initialize() noexcept{
	attach(_mboxSemaHandler);
	}

#if 0
void	Server::setSignaledApi(SignaledApi* sapi) noexcept{
	if(sapi && _signaledApi){
		// To help ensure that only one SignaledApi is set at any
		// given time, the protocol is for the client to set the
		// signal handler, and then set it to zero when it is no
		// longer required. Thus, if there are (by programmer error)
		// two clients that attempt to set the SignaledApi together,
		// the error will be caught at run-time here.
		Oscl::ErrorFatal::logAndExit(
			"%s\n",
			__PRETTY_FUNCTION__
			);
		return;
		}
	_signaledApi	= sapi;
	}

Oscl::Mt::Sema::SignalApi&	Server::getSignalApi() noexcept{
	return *this;
	}

Oscl::Mt::Itc::PostMsgApi&	Server::getPostMsgApi() noexcept{
	return *this;
	}
#endif

void	Server::processIoHandlers() noexcept{
//	time_t	timestamp	= time(0);

	for(
		Handler*	next	= _pollQ.first();
		next ;
		) {

		struct k_poll_event*
		event	= (struct k_poll_event*)next->_serverData;

		bool
		remove	= next->process(*event);

		event->state	= K_POLL_STATE_NOT_READY;

		if(remove){
			Handler*
			h		= next;
			next	= _pollQ.next(next);

			detach(*h);
			}
		else {
			next	= _pollQ.next(next);
			}
		}
	}

void	Server::mboxSemaStart(struct k_poll_event& event) noexcept{
	k_poll_event_init(
		&event,
		K_POLL_TYPE_SEM_AVAILABLE,	// type
		K_POLL_MODE_NOTIFY_ONLY,	// mode
		&_mboxSema
		);
	}

void	Server::mboxSemaStop() noexcept{
	// We NEVER stop the mboxSema
	}

bool	Server::mboxSemaProcess(struct k_poll_event& event) noexcept{
	if(event.state == K_POLL_STATE_SEM_AVAILABLE){
		k_sem_take(
			event.sem,
			K_NO_WAIT
			);
		processMsgQ();
		}
	return false; // We never stop handling mboxSema events.
	}
