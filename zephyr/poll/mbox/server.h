/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_poll_mbox_serverh_
#define _oscl_zephyr_poll_mbox_serverh_

#include "oscl/mt/runnable.h"
#include "oscl/zephyr/poll/api.h"
#include "oscl/mt/itc/mbox/mutex.h"
#include "oscl/zephyr/poll/timer.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/mbox/msg.h"
#include "oscl/mt/sema/sigapi.h"

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace Poll {

/** This concrete class handles the Zephyr k_poll() function for
	many registered clients.

	It allows a single thread to handle multiple events
	for clients (Parts) running in the same thread.

	It is a special mailbox in that
	it uses a special technique to cause its mailbox to be
	processed. Specifically, whenever its signal() operation
	is invoked.
 */
class Server :
	public Oscl::Mt::Runnable,
	public Oscl::Mt::Itc::PostMsgApi,
	public Oscl::Mt::Sema::SignalApi,
	public Oscl::Zephyr::Poll::Api
	{
	private:
		/** */
		struct k_poll_event* const				_events;

		/** */
		const unsigned							_nEvents;

	private:
		/** */
		Oscl::Queue<Oscl::Mt::Itc::Msg>			_mboxQ;

		/** Mailbox semaphore */
		struct k_sem							_mboxSema;

		/** */
		Oscl::Zephyr::Poll::HandlerComposer<
			Server
			>									_mboxSemaHandler;

		/** */
		Oscl::Queue<
			Oscl::Zephyr::Poll::Handler
			>									_pollQ;

		/** */
		Oscl::Queue<Oscl::Zephyr::Poll::Timer>	_timerList;

		/** */
		Oscl::Mt::Itc::MailboxMutex				_mutex;

		/** Holds the value of the next poll timeout in milliseconds.
		 */
		unsigned long							_timeoutInMs;

	public:
		/** */
		Server(
			struct k_poll_event*	events,
			unsigned				nEvents
			) noexcept;

		/** */
		virtual ~Server();

	public: // Oscl::Zephyr::Poll::Api
		/** Get the mailbox interface for the poll server.
		 */
		Oscl::Mt::Itc::PostMsgApi&	getPostMsgApi() noexcept;

		/** This operation *may* be invoked before the thread
			begins running. Afterwards, it is only invoked
			by the Server itself upon receiving an AddReq
			message.
		 */
		void	attach(Handler& handler) noexcept;

		/** */
		void	detach(Handler& handler) noexcept;

		/** */
		void	start(Timer& timer) noexcept;

		/** */
		void	stop(Timer& timer) noexcept;

	private:
		/** */
		void	initializeEvents() noexcept;

		/** */
		struct k_poll_event*	allocEvent() noexcept;

		/** */
		void	processMsgQ() noexcept;

	public:	// PostMsgApi
		/** */
		void	post(Oscl::Mt::Itc::Msg& msg) noexcept;
		/** */
		void	postSync(Oscl::Mt::Itc::Msg& msg) noexcept;

	public: // SignalApi interface
		/** */
		void signal(void) noexcept;
		/** */
		void suSignal(void) noexcept;

	protected:
		/** */
		virtual void	initialize() noexcept;

	public: // Runnable
		/** */
		virtual void	run() noexcept;

	private:
		/** */
		void	processIoHandlers() noexcept;

		/** */
		void	processTimeout() noexcept;

		/** */
		unsigned long	calculateNextTimeoutInMs() noexcept;

		/** */
		void	updateTimeout(int64_t deductionInMs) noexcept;

	private: // Oscl::Zephyr::Poll::HandlerComposer _mboxSemaHandler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	mboxSemaStart(struct k_poll_event& event) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	mboxSemaStop() noexcept;

		/**	This operation is invoked whenever the select returns
			and the state of the event indicates that it is pending.
			The implementation then performs any I/O based on the
			event type and resets the state of the event.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the event list.
			If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after
			this operation returns.

			The handler returns FALSE if it wishes to remain
			attached to the event list.
		 */
		bool	mboxSemaProcess(struct k_poll_event& event) noexcept;
	};

}
}
}

#endif
