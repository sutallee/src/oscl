/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_poll_mbox_configh_
#define _oscl_zephyr_poll_mbox_configh_

#include "server.h"

/** */
namespace Oscl {
/** */
namespace Zephyr {
/** */
namespace Poll {

/** This template class is an Oscl::Zephyr::Poll::Server
	that is used specify the maximum number of events
	that the server can handle.
 */
template <unsigned nEvents>
class Config : public Server {
	private:
		/** This array contains the number of event
			records specified by the template parameter
			nEvents plus one additional event that is
			used internally by the Server.
		 */
		struct k_poll_event	_eventMem[nEvents+1];

	public:
		/** */
		Config() noexcept:
			Server(_eventMem,nEvents+1)
			{}
	};

}
}
}

#endif
