/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_zephyr_poll_apih_
#define _oscl_zephyr_poll_apih_

#include "handler.h"
#include "timer.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {

/** */
namespace Zephyr {

/** */
namespace Poll {

/**	This interface is used to add and remove poll
	handlers and timers to the poll server.
 */
class Api {
	public:
		/** Get the mailbox interface for the poll server.
		 */
		virtual Oscl::Mt::Itc::PostMsgApi&	getPostMsgApi() noexcept=0;

		/**	This operation registers the specified handler with the
			poll server.
		 */
		virtual void	attach(Handler& handler) noexcept=0;

		/**	This operation removes the specified handler from the
			poll server.
		 */
		virtual void	detach(Handler& handler) noexcept=0;

		/** */
		virtual void	start(Timer& timer) noexcept=0;

		/** */
		virtual void	stop(Timer& timer) noexcept=0;

	};

}
}
}

#endif
