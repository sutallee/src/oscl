/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_decoder_apih_
#define _oscl_decoder_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Decoder {

/** This structure is used to save the
	state of the decoder using the
	push() operation.

	It is possible to nest multiple uses
	of State objects, since the decoder
	maintains a LIFO ordered list of
	these objects.

	The client must *NOT* modify the fields
	between the coresponding push() and pop()
	operations associated with a State structure.

	The push() and pop() operations must
	be balanced for correct operations.
 */
struct State {
	/** */
	void*		__qitemlink;

	/** */
	unsigned	_offset;

	/** */
	unsigned	_length;
	};

/** This interface is for use by clients that
	need to decode values from an inter-process
	communication message. Clients of this abstract
	interface do not need to know how the data is
	encoded.

	Each of the non-const decoding operations normally
	may advance the offset within the message when
	successful and maintain the underflow state
	of the message.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** Returns the number of octets that
			remain in the message.
		 */
		virtual unsigned	remaining() const noexcept=0;

		/** This operation allows the client to
			save the current message processing
			state and later restore it with the pop()
			operation.

			The remaining parameter specifies the
			maximum buffer length remaining while
			this state is in play. The value must
			never to be more than that returned
			by the remaining() operation before
			push() is invoked. This allows the
			user to specify a maximum remaining
			buffer space that is *LESS* than 
			currently available.
		 */
		virtual void	push(
							Oscl::Decoder::State&	r,
							unsigned				remaining
							) noexcept=0;

		/** This operation allows the client to
			restore the current message processing
			state to the last state saved by
			the push() operation (LIFO order).

			RETURN: The number of octets that were
			decoded since the state was pushed. This
			value is intended to be used by the client
			with the skip() operation if required.
		 */
		virtual unsigned	pop() noexcept=0;

		/** RETURN: true if any preceding operation
			resulted in more octets being requested
			than actually exist in the message.

			The underflow state is restored by the
			pop() operation as required.
		 */
		virtual bool	underflow() const noexcept=0;

		/** This operation advances the message state
			offset by the specified number of octets.
		 */
		virtual void	skip(unsigned n) noexcept=0;

		/** This operation may be used to force alignment
			within the buffer to the sizes specified by
			the argument.
		 */
		virtual void	forceAlign(unsigned octets) noexcept=0;

		/** Retrieves an unsigned 8-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(uint8_t& v) noexcept=0;

		/** Retrieves a signed 8-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(int8_t& v) noexcept=0;

		/** Retrieves an unsigned 16-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(uint16_t& v) noexcept=0;

		/** Retrieves a signed 16-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(int16_t& v) noexcept=0;

		/** Retrieves an unsigned 32-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(uint32_t& v) noexcept=0;

		/** Retrieves a signed 32-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(int32_t& v) noexcept=0;

		/** Retrieves an unsigned 24-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode24Bit(uint32_t& v) noexcept=0;

		/** Retrieves an unsigned 64-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(uint64_t& v) noexcept=0;

		/** Retrieves a signed 64-bit value from the
			message and either advances the message
			state or sets an underflow indication.
		 */
		virtual void	decode(int64_t& v) noexcept=0;

		/** Retrieves a double value from the
			message and either advances the message
			state or sets an underflow indication.

			WARNING: This operation is only to be
			used for interprocess communication on
			machines/processes with the exact same
			representation of a double!
		 */
		virtual void	decode(double& v) noexcept=0;

		/** Retrieves the number of octets specified
			by the length parameter from the
			message and either advances the message
			state or sets an underflow indication.
			The octets are written to the buffer.

			WARNING: The buffer allocated by the client
			*MUST* be at least the size specified by
			the length parameter.
		 */
		virtual void	copyOut(
							void*		buffer,
							unsigned	length
							) noexcept=0;
	};

}
}


#endif
