/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_decoder_linear_baseh_
#define _oscl_decoder_linear_baseh_

#include "api.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {

/** */
namespace Decoder {

/** */
namespace Linear {

/** */
class Base : public Oscl::Decoder::Linear::Api {
	private:
		/** */
		Oscl::Queue<Oscl::Decoder::State>		_stack;

	protected:
		/** */
		const uint8_t*			_buffer;

		/** */
		unsigned				_length;

		/** */
		unsigned				_offset;

		/** */
		bool					_underflow;

	public:
		/** */
		Base(
			const void*	buffer,
			unsigned	bufferLength
			) noexcept;

		/** */
		unsigned	remaining() const noexcept;

		/** */
		void	push(
					Oscl::Decoder::State&	r,
					unsigned				remaining
					) noexcept;

		/** */
		unsigned	pop() noexcept;

		/** */
		bool	underflow() const noexcept;

		/** */
		void	skip(unsigned n) noexcept;

		/** */
		void	forceAlign(unsigned octets) noexcept;

		/** */
		virtual void	decode(uint8_t& v) noexcept;

		/** */
		virtual void	decode(int8_t& v) noexcept;

		/** */
		virtual void	decode(uint16_t& v) noexcept=0;

		/** */
		virtual void	decode(int16_t& v) noexcept;

		/** */
		virtual void	decode(uint32_t& v) noexcept=0;

		/** */
		virtual void	decode24Bit(uint32_t& v) noexcept=0;

		/** */
		void	decode(int32_t& v) noexcept;

		/** */
		virtual void	decode(uint64_t& v) noexcept=0;

		/** */
		void	decode(int64_t& v) noexcept;

		/** */
		void	decode(double& v) noexcept;

		/** */
		virtual void	copyOut(void* buffer, unsigned length) noexcept;

		/** */
		virtual const char*	decodeString() noexcept;

		/** */
		virtual const void*	decodeSequence(uint32_t& length) noexcept;

		/** */
		virtual const void*	decodeSequence(uint16_t& length) noexcept;

		/** */
		virtual const void*	decodeSequence(uint8_t& length) noexcept;

	protected:
		/** */
		virtual bool	align(unsigned octets) noexcept;

	};

}
}
}

#endif
