/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"
#include "oscl/error/info.h"

using namespace Oscl::Decoder::Linear;

Base::Base(
	const void*	buffer,
	unsigned	bufferLength
	) noexcept:
		_buffer((const uint8_t*)buffer),
		_length(bufferLength),
		_offset(0),
		_underflow(false)
		{
	}

unsigned	Base::remaining() const noexcept{
	return _length-_offset;
	}

void	Base::push(
				Oscl::Decoder::State&	r,
				unsigned				remaining
				) noexcept{

	_stack.push(&r);

	r._offset	= _offset;
	r._length	= _length;
	_length		= _offset+remaining;
	}

unsigned	Base::pop() noexcept{

	Oscl::Decoder::State*
	r	= _stack.pop();

	if(!r){
		Oscl::Error::Info::log(
			"%s: pop without push.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned	len		= _offset-r->_offset;

	_offset		= r->_offset;
	_length		= r->_length;
	_underflow	= (_offset > _length)?true:false;

	return len;
	}

bool	Base::align(unsigned bytes) noexcept{
	return false;
	}

bool	Base::underflow() const noexcept{
	return _underflow;
	}

void	Base::skip(unsigned n) noexcept{

	if((_offset + n) > _length){
		_underflow	= true;
		return;
		}

	_offset	+= n;
	}

void	Base::forceAlign(unsigned octets) noexcept{
	unsigned 	offset	= _offset;

	offset	+= octets-1;

	offset	/= octets;

	offset	*= octets;

	if(offset > _length){
		_underflow	=  true;
		return;
		}

	_offset	= offset;
	}

void	Base::decode(uint8_t& v) noexcept{

	if(_offset+1 > _length){
		_underflow	= true;
		return;
		}

	_underflow	= false;

	v	= _buffer[_offset];

	++_offset;
	}

void	Base::decode(int8_t& v) noexcept{
	decode((uint8_t&)v);
	}

const char*	Base::decodeString() noexcept{

	unsigned	i;

	for(i=_offset; i<_length; ++i){

		if(!_buffer[i]){
			break;
			}

		}

	if(i < _length){

		const char*	p = (const char*)&_buffer[_offset];

		++i;

		_offset	= i;

		return p; 
		}

	return 0;
	}

void	Base::decode(int16_t& v) noexcept{
	decode((uint16_t&)v);
	}

void	Base::decode(int32_t& v) noexcept{
	decode((uint32_t&)v);
	}

void	Base::decode(int64_t& v) noexcept{
	decode((uint64_t&)v);
	}

void	Base::decode(double& v) noexcept{
	union{
		double		d;
		uint64_t	u;
		}	magicUnion;

	decode(magicUnion.u);

	if(_underflow){
		return;
		}

	v	= magicUnion.d;
	}

void	Base::copyOut(void* buffer, unsigned length) noexcept{

	if(_underflow){
		return;
		}

	unsigned	remain	= remaining();

	unsigned	len	= (length > remain)?remain:length;

	uint8_t*	p	= (uint8_t*)buffer;

	for(unsigned i=0;i<len;++i){
		p[i]	= _buffer[_offset+i];
		}

	_offset	+= len;

	if(len < length){
		_underflow	= true;
		}
	}

const void*	Base::decodeSequence(uint32_t& length) noexcept{

	uint32_t	len;
	unsigned	offset	= _offset;

	decode(len);

	if(_underflow){
		return 0;
		}

	if(_offset + len > _length){
		_underflow	= true;
		_offset		= offset;
		return 0;
		}

	const void*	p	= &_buffer[_offset];

	_offset	+= len;

	length	= len;

	return p;
	}

const void*	Base::decodeSequence(uint16_t& length) noexcept{

	uint16_t	len;
	unsigned	offset	= _offset;

	decode(len);

	if(_underflow){
		return 0;
		}

	if(_offset + len > _length){
		_underflow	= true;
		_offset		= offset;
		return 0;
		}

	const void*	p	= &_buffer[_offset];

	_offset	+= len;

	length	= len;

	return p;
	}

const void*	Base::decodeSequence(uint8_t& length) noexcept{

	uint8_t		len;
	unsigned	offset	= _offset;

	decode(len);

	if(_underflow){
		return 0;
		}

	if(_offset + len > _length){
		_underflow	= true;
		_offset		= offset;
		return 0;
		}

	const void*	p	= &_buffer[_offset];

	_offset	+= len;

	length	= len;

	return p;
	}

