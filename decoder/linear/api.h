/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_decoder_linear_apih_
#define _oscl_decoder_linear_apih_

#include "oscl/decoder/api.h"

/** */
namespace Oscl {

/** */
namespace Decoder {

/** */
namespace Linear {

/** */
class Api : public Oscl::Decoder::Api {
	public:
		/** */
		virtual ~Api(){}

		/** Get a pointer to the linear buffer
			position that contains a null terminated
			string.
			RETURN: pointer to string or zero if
			no such string exists, possibly due to an
			underflow.
		 */
		virtual const char*	decodeString() noexcept=0;

		/** Get a pointer to the linear buffer
			position that is the beginning sequence of
			octets. The sequence is expected to be
			encoded as a 32-bit length value followed
			by a sequence of octets as specified by the
			length field.

			RETURN: pointer to string or zero if
			no such sequence exists, possibly due to an
			underflow. On success, the length of the
			sequence is return in the length parameter.
		 */
		virtual const void*	decodeSequence(uint32_t& length) noexcept=0;

		/** Get a pointer to the linear buffer
			position that is the beginning sequence of
			octets. The sequence is expected to be
			encoded as a 16-bit length value followed
			by a sequence of octets as specified by the
			length field.

			RETURN: pointer to string or zero if
			no such sequence exists, possibly due to an
			underflow. On success, the length of the
			sequence is return in the length parameter.
		 */
		virtual const void*	decodeSequence(uint16_t& length) noexcept=0;

		/** Get a pointer to the linear buffer
			position that is the beginning sequence of
			octets. The sequence is expected to be
			encoded as an 8-bit length value followed
			by a sequence of octets as specified by the
			length field.

			RETURN: pointer to string or zero if
			no such sequence exists, possibly due to an
			underflow. On success, the length of the
			sequence is return in the length parameter.
		 */
		virtual const void*	decodeSequence(uint8_t& length) noexcept=0;
	};

}
}
}


#endif
