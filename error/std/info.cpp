/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "oscl/error/info.h"

using namespace Oscl::Error;

void	Info::log(const Strings::Api& errorString) noexcept{
	fprintf(stdout,"%s\n",errorString.getString());
	}

void	Info::logString(const char* string) noexcept{
	printf("%s\n",string);
	}

void	Info::log(
			const char*	format,
			...
			) noexcept{
    va_list ap;

    va_start(ap, format);

	vprintf(
		format,
		ap
		);

    va_end(ap);

	fflush(stdout);
	}

void	Info::logRaw(
			const char*	format,
			...
			) noexcept{
    va_list ap;

    va_start(ap, format);

	vprintf(
		format,
		ap
		);

    va_end(ap);

	fflush(stdout);
	}

