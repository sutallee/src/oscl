/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/info.h"

using namespace Oscl::Error;

static void	byteToAscii(char buff[3],unsigned char value){
	for(unsigned i=0;i<2;++i){
		char	v	= value >> ((8-4)-(i*4));
		v	&= 0x000F;
		v	+= '0';
		if(v > '9'){
//			v	+= 7;
			v	+= 7 + 0x20;
			}
		buff[i]	= v;
		}
	buff[2]	= '\0';
	}

static void	longToAscii(char buff[9],unsigned long value){
	for(unsigned i=0;i<8;++i){
		char	v	= value >> ((32-4)-(i*4));
		v	&= 0x000F;
		v	+= '0';
		if(v > '9'){
			v	+= 7 + 0x20;
			}
		buff[i]	= v;
		}
	buff[8]	= '\0';
	}

void	Info::hexDump(	const void*		mem,
						unsigned		n,
						unsigned		offset
						) noexcept{
	char	buff[8];
	char*	p	= (char*)mem;
	for(unsigned i=0;i<n;++i){
		if(!(i%16)){
			char buff[9];
			Oscl::Error::Info::logRaw("\n\t");
			longToAscii(buff,i+offset);
			Oscl::Error::Info::log(buff);
			Oscl::Error::Info::logRaw(": ");
			}
		byteToAscii(buff,p[i]);
		Oscl::Error::Info::logRaw(buff);
		Oscl::Error::Info::logRaw(" ");
		}
	Oscl::Error::Info::logRaw("\n");
	}

void	Info::hexDump(unsigned long	value) noexcept{
	char	buff[9];
	longToAscii(buff,value);
	Oscl::Error::Info::logRaw(buff);
	}

void	Info::hexDump(unsigned char	value) noexcept{
	char	buff[3];
	byteToAscii(buff,value);
	Oscl::Error::Info::logRaw(buff);
	}

