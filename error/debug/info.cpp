/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/strings/fixed.h"
#include "oscl/error/info.h"
#include "oscl/mt/mutex/iss.h"
#include "oscl/stream/output/api.h"
#include "oscl/error/stderr.h"

namespace Oscl {
namespace Error {

struct Record {
	void*					__qitemlink;
	Oscl::Strings::Fixed<64>	_error;
	};

typedef Oscl::Memory::AlignedBlock<sizeof(Record)>					RecordMem;
typedef Oscl::Memory::AlignedBlock<sizeof(Oscl::Queue<RecordMem>)>	RecQueueMem;
typedef Oscl::Memory::AlignedBlock<sizeof(Oscl::Queue<Record>)>		QueueMem;

enum {nRecords=10};

}
}

using namespace Oscl::Error;

static RecordMem	recordMem[nRecords];
static RecQueueMem	freeRecMem;
static QueueMem		queueMem;
static bool			initialized;
static Oscl::Stream::Output::Api*	streamOutput;

Oscl::Queue<Record>*	errQ;
Oscl::Queue<RecordMem>*	freeQ;

void	Oscl::Error::setStandardError(Oscl::Stream::Output::Api& stdError) noexcept{
	streamOutput	= &stdError;
	}

static void	initialize() noexcept{
	if(initialized) return;
	initialized	= true;

	errQ	= new(&queueMem) Oscl::Queue<Record>;

	freeQ	= new(&freeRecMem) Oscl::Queue<RecordMem>;

	for(unsigned i=0;i<nRecords;++i){
		freeQ->put(&recordMem[i]);
		}
	}

// Setting this variable to true will cause
// Informational errors to be ignored.
// The default is to loop forever for debugging
// purposes.
bool	GlobalErrorInfoDisable;

void	Info::log(const Strings::Api& errorString) noexcept{

	if(streamOutput) {
		streamOutput->write(
			errorString,
			errorString.length()
			);
		}

		{
		Oscl::Mt::IntScopeSync	iss;
		initialize();
		if(GlobalErrorInfoDisable) return;
	
		Oscl::Queue<Record>*
		errQ	= (Oscl::Queue<Record>*)&queueMem;
	
		Oscl::Queue<RecordMem>*
		freeQ	= (Oscl::Queue<RecordMem>*)&freeRecMem;
	
		RecordMem*
		recMem	= freeQ->get();
	
		if(!recMem){
			recMem	= (RecordMem*)errQ->get();
			}
		if(!recMem){
			while(true);
			}
		Record*	
		rec	= new(recMem) Record();
		rec->_error	= errorString;
		errQ->put(rec);
		}
	}

static void	logRecord(
				const char*	format,
				va_list		ap
				) noexcept{

	Oscl::Strings::Fixed<128>	error;

	error.vassign(
		format,
		ap
		);

	if(streamOutput){
		streamOutput->write(
			error.getString(),
			error.length()
			);
		if(error.truncated()){
			streamOutput->write(
				"\n",
				1
				);
			}
		}

		{
		Oscl::Mt::IntScopeSync	iss;
		initialize();
		if(GlobalErrorInfoDisable) return;

		Oscl::Queue<Record>*
		errQ	= (Oscl::Queue<Record>*)&queueMem;

		Oscl::Queue<RecordMem>*
		freeQ	= (Oscl::Queue<RecordMem>*)&freeRecMem;

		RecordMem*
		recMem	= freeQ->get();

		if(!recMem){
			recMem	= (RecordMem*)errQ->get();
			}
		if(!recMem){
			while(true);
			}
		Record*	
		rec	= new(recMem) Record();

		rec->_error	= error;

		errQ->put(rec);
		}
	}

void	Info::logRaw(
			const char*	format,
			...
			) noexcept{
    va_list ap;

    va_start(ap, format);

	logRecord(
		format,
		ap
		);

    va_end(ap);
	}

void	Info::log(
			const char*	format,
			...
			) noexcept{

    va_list ap;

    va_start(ap, format);

	logRecord(
		format,
		ap
		);

    va_end(ap);
	}

