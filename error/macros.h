/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_error_macrosh_
#define _oscl_error_macrosh_

/** The OSCL_PRETTY_FUNCTION may be overridden
	to reduce the amount of debug text in an
	application.
	This is typically done by defining it in the
	global buildCfg.h when using BENV.
 */
#ifndef OSCL_PRETTY_FUNCTION
	#define OSCL_PRETTY_FUNCTION	__PRETTY_FUNCTION__
#endif

/** The OSCL_PRETTY_FUNCTION_DETAIL may be overridden
	to reduce the amount of debug text in an
	application.
	This is typically done by defining it in the
	global buildCfg.h when using BENV.

	OSCL_PRETTY_FUNCTION_DETAIL is generally
	used where we would *NOT* use OSCL_PRETTY_FUNCTION
	under "normal" circumstances, which is generally
	in code that is conditionally compiled only when
	debugging is enabled and we want maximum detail.
	However, under memory constrained systems, we
	sometimes cannot handle the extra characters
	in __PRETTY_FUNCTION__ so we can define this
	to be either __FUNCTION__ or something else.
 */
#ifndef OSCL_PRETTY_FUNCTION_DETAIL
	#define OSCL_PRETTY_FUNCTION_DETAIL	__PRETTY_FUNCTION__
#endif

#endif
