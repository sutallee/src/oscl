/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_logic_coreh_
#define _oscl_logic_coreh_
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {

/** */
namespace Logic {

/** */
static const bool	_asserted	= true;

/** */
static const bool	_negated	= false;

/** */
class StateApi {
	public:
		/** */
		virtual ~StateApi() {};
	public:
		/** */
		virtual void	asserted() noexcept=0;
		/** */
		virtual void	negated() noexcept=0;
	};

/** */
class OutputApi {
	public:
		/** */
		class Observer : public StateApi , public QueueItem {
			// This class intentionally left blank ;-)
			};
	public:
		/** */
		virtual ~OutputApi() {}

	public:
		/** */
		virtual void		attach(Observer& o) noexcept=0;

		/** */
		virtual void		detach(Observer& o) noexcept=0;

		/** */
		virtual const bool&	getOutputRef() const noexcept=0;
	};

};
};

#endif
