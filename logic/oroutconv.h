/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_logic_oroutconvh_
#define _oscl_logic_oroutconvh_

#include "observer.h"
#include "oscl/logic/gate/or.h"

/** */
namespace Oscl {

/** */
namespace Logic {

/** This template allows the user to monitor a Logic output
	and convert output changes into other actions. This is
	a conversion activity. The conversion function may be
	inserted into the Logic "circuit" as an n-input OR gate
	in addition to the monitoring function.
 */
template <class Context>
class OrOutConv : public Gate::OrApi , private OutputApi::Observer {
	public:
		/** */
		typedef void (Context::* AssertedFunc)();
		/** */
		typedef void (Context::* NegatedFunc)();

	private:
		/** */
		Context&		_context;
		/** */
		AssertedFunc	_asserted;
		/** */
		NegatedFunc		_negated;
		/** */
		Gate::Or		_orGate;

	public:
		/** */
		OrOutConv(	Context&		context,
					AssertedFunc	asserted,
					NegatedFunc		negated
					) noexcept;

		/** */
		InputApi&		getInputApi() noexcept;

		/** */
		OutputApi&	getOutputApi() noexcept;

	private: // OutputApi::Observer
		/** */
		void	asserted() noexcept;
		/** */
		void	negated() noexcept;
	};

template <class Context>
OrOutConv<Context>::OrOutConv(	Context&		context,
								AssertedFunc	asserted,
								NegatedFunc		negated
								) noexcept:
		_context(context),
		_asserted(asserted),
		_negated(negated),
		_orGate()
		{
	_orGate.getOutputApi().attach(*this);
	}

template <class Context>
InputApi&	OrOutConv<Context>::getInputApi() noexcept{
	return _orGate.getInputApi();
	}

template <class Context>
OutputApi&	OrOutConv<Context>::getOutputApi() noexcept{
	return _orGate.getOutputApi();
	}

template <class Context>
void	OrOutConv<Context>::asserted() noexcept{
	(_context.*_asserted)();
	}

template <class Context>
void	OrOutConv<Context>::negated() noexcept{
	(_context.*_negated)();
	}

}
}

#endif
