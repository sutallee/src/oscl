/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "output.h"

using namespace Oscl;
using namespace Oscl::Logic;

Logic::Output::Output() noexcept:
		_asserted(false)
		{
	}

void	Logic::Output::attach(Observer& o) noexcept{
	_observers.put(&o);
	if(_asserted)	o.asserted();
	else			o.negated();
	}

void	Logic::Output::detach(Observer& o) noexcept{
	_observers.remove(&o);
	}

const bool&	Logic::Output::getOutputRef() const noexcept{
	return _asserted;
	}

void	Logic::Output::notify() noexcept{
	if(_asserted)	notifyAsserted();
	else			notifyNegated();
	}

void	Logic::Output::notifyAsserted() noexcept{
	Observer*	o;
	for(	o	= _observers.first();
			o;
			o	= _observers.next(o)
			){
		o->asserted();
		}
	}

void	Logic::Output::notifyNegated() noexcept{
	Observer*	o;
	for(	o	= _observers.first();
			o;
			o	= _observers.next(o)
			){
		o->negated();
		}
	}

void	Logic::Output::asserted() noexcept{
	if(!_asserted){
		_asserted	= true;
		notifyAsserted();
		}
	}

void	Logic::Output::negated() noexcept{
	if(_asserted){
		_asserted	= false;
		notifyNegated();
		}
	}

