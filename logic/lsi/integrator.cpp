/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "integrator.h"

using namespace Oscl;
using namespace Oscl::Logic;

Logic::Lsi::Integ::Integ(	const bool&	dataInput,
							unsigned	nTicks,
							unsigned	upToDownRation
							) noexcept:
		Logic::Lsi::IntegApi(dataInput),
		_maxValue(nTicks*upToDownRation),
		_currentCount(_maxValue),
		_upCountValue(upToDownRation),
		_downCountValue(1),
		_inhibitInput(*this),
		_setInput(*this),
		_clearInput(*this),
		_inhibited(_inhibitInput.getOutput()),
		_set(_setInput.getOutput()),
		_cleared(_clearInput.getOutput()),
		_integratorState(true)
		{
	_output.asserted();
	}

Seq::InputApi&	Logic::Lsi::Integ::getClockInput() noexcept{
	return *this;
	}

Logic::OutputApi&	Logic::Lsi::Integ::getOutput() noexcept{
	return _output;
	}

Logic::InputApi&		Logic::Lsi::Integ::getInhibit() noexcept{
	return _inhibitInput;
	}

Logic::InputApi&		Logic::Lsi::Integ::getSet() noexcept{
	return _setInput;
	}

Logic::InputApi&		Logic::Lsi::Integ::getClear() noexcept{
	return _clearInput;
	}

void	Logic::Lsi::Integ::attach(	Seq::SubjectApi&	output,
									Seq::Wire&		wire
									) noexcept{
	wire.setSeqApi(*this);
	output.attach(wire);
	}

void	Logic::Lsi::Integ::detach(	Seq::SubjectApi&	output,
									Seq::Wire&		wire
									) noexcept{
	output.detach(wire);
	}

void	Logic::Lsi::Integ::tick() noexcept{
	if(_inhibited || _set || _cleared){
		return;
		}
	if(_dataInput){
		if(_currentCount >= _maxValue) return;
		_currentCount	+= _upCountValue;
		if(_currentCount >= _maxValue){
			_currentCount		= _maxValue;
			_integratorState	= true;
			}
		}
	else{
		if(_currentCount <= _minValue) return;
		_currentCount	-= _downCountValue;
		if(_currentCount <= _minValue){
			_currentCount	= _minValue;
			_integratorState	= false;
			}
		}
	evaluateOutput();
	}

void	Logic::Lsi::Integ::clearAsserted() noexcept{
	_currentCount		= _minValue;
	_integratorState	= false;
	evaluateOutput();
	}

void	Logic::Lsi::Integ::clearNegated() noexcept{
	if(_set)	setAsserted();
	else		evaluateOutput();
	}

void	Logic::Lsi::Integ::setAsserted() noexcept{
	if(_cleared) return;
	_currentCount		= _maxValue;
	_integratorState	= true;
	evaluateOutput();
	}

void	Logic::Lsi::Integ::setNegated() noexcept{
	evaluateOutput();
	}

void	Logic::Lsi::Integ::evaluateOutput() noexcept {
	if(_cleared){
		_output.negated();
		return;
		}
	if(_set){
		_output.asserted();
		return;
		}
	if(_integratorState){
		_output.asserted();
		}
	else{
		_output.negated();
		}
	}

////////// Inhibit

Logic::Lsi::Integ::Inhibit::Inhibit(Logic::Lsi::Integ& context) noexcept:
		_context(context),
		_output(false),
		_assertedCount(0)
		{
	}

const bool&	Logic::Lsi::Integ::Inhibit::getOutput() noexcept{
	return _output;
	}

void	Logic::Lsi::Integ::Inhibit::attach(	Logic::OutputApi&	output,
											Logic::Wire&		wire
											) noexcept{
	wire.setStateApi(*this);
	if(!output.getOutputRef()){
		++_assertedCount;
		}
	output.attach(wire);
	}

void	Logic::Lsi::Integ::Inhibit::detach(	Logic::OutputApi&	output,
											Logic::Wire&		wire
											) noexcept{
	output.detach(wire);
	if(output.getOutputRef()) negated();
	}

void	Logic::Lsi::Integ::Inhibit::asserted() noexcept{
	++_assertedCount;
	_output	= true;
	}

void	Logic::Lsi::Integ::Inhibit::negated() noexcept{
	--_assertedCount;
	if(!_assertedCount) _output = false;
	}

////////// Clear

Logic::Lsi::Integ::Clear::Clear(Logic::Lsi::Integ& context) noexcept:
		_context(context),
		_output(false),
		_assertedCount(0)
		{
	}

const bool&	Logic::Lsi::Integ::Clear::getOutput() noexcept{
	return _output;
	}

void	Logic::Lsi::Integ::Clear::attach(	Logic::OutputApi&	output,
											Logic::Wire&			wire
											) noexcept{
	wire.setStateApi(*this);
	if(!output.getOutputRef()){
		++_assertedCount;
		}
	output.attach(wire);
	}

void	Logic::Lsi::Integ::Clear::detach(	Logic::OutputApi&	output,
											Logic::Wire&			wire
											) noexcept{
	output.detach(wire);
	if(output.getOutputRef()) negated();
	}

void	Logic::Lsi::Integ::Clear::asserted() noexcept{
	++_assertedCount;
	_output	= true;
	_context.clearAsserted();
	}

void	Logic::Lsi::Integ::Clear::negated() noexcept{
	--_assertedCount;
	if(_assertedCount) return;
	_output	= false;
	_context.clearNegated();
	}

////////// Set

Logic::Lsi::Integ::Set::Set(Logic::Lsi::Integ& context) noexcept:
		_context(context),
		_output(false),
		_assertedCount(0)
		{
	}

const bool&	Logic::Lsi::Integ::Set::getOutput() noexcept{
	return _output;
	}

void	Logic::Lsi::Integ::Set::attach(	Logic::OutputApi&	output,
										Logic::Wire&			wire
										) noexcept{
	wire.setStateApi(*this);
	if(!output.getOutputRef()){
		++_assertedCount;
		}
	output.attach(wire);
	}

void	Logic::Lsi::Integ::Set::detach(	Logic::OutputApi&	output,
										Logic::Wire&			wire
										) noexcept{
	output.detach(wire);
	if(output.getOutputRef()) negated();
	}

void	Logic::Lsi::Integ::Set::asserted() noexcept{
	++_assertedCount;
	_output	= true;
	_context.setAsserted();
	}

void	Logic::Lsi::Integ::Set::negated() noexcept{
	--_assertedCount;
	if(_assertedCount) return;
	_output	= false;
	_context.setNegated();
	}

