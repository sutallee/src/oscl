/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_logic_lsi_hintapih_
#define _oscl_logic_lsi_hintapih_
#include "hintinapi.h"
#include "hintoutapi.h"

/** */
namespace Oscl {
/** */
namespace Logic {
/** */
namespace Lsi {

/** */
class HierIntegApi :	public HierIntegInputApi,
						public HierIntegOutputApi
						{
	public:
		/** */
		virtual ~HierIntegApi(){}
	public:
		/** */
		virtual Seq::InputApi&		getIntegClockInput() noexcept=0;
		/** */
		virtual Logic::InputApi&	getIntegDataDefectInput() noexcept=0;
		/** */
		virtual Logic::InputApi&	getDefectSuppInput() noexcept=0;
		/** */
		virtual Logic::InputApi&	getFailureSuppInput() noexcept=0;
		/** */
		virtual Logic::InputApi&	getSetInput() noexcept=0;
		/** */
		virtual Logic::InputApi&	getClearInput() noexcept=0;

	public:
		/** */
		virtual Logic::OutputApi&	getDefectHierOutput() noexcept=0;
		/** */
		virtual Logic::OutputApi&	getFailureHierOutput() noexcept=0;
		/** */
		virtual Logic::OutputApi&	getDefectOutput() noexcept=0;
		/** */
		virtual Logic::OutputApi&	getFailureOutput() noexcept=0;
	};

};
};
};

#endif
