/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "hintegrator.h"

using namespace Oscl;
using namespace Oscl::Logic;

Logic::Lsi::HierInteg::HierInteg(	unsigned	nTicks,
											unsigned	upToDownRation
											) noexcept:
		_integrator(	_dataInput.getOutput().getOutputRef(),
						nTicks,
						upToDownRation)
		{
	_integrator.getInhibit().attach(			_defectSuppInput.getOutput(),
												_defectSuppInputToIntegInhibitWire
												);
	_integrator.getSet().attach(				_setInputOrGate.getOutput(),
												_setInputOrGateToIntegSetInputWire
												);
	_integrator.getClear().attach(				_clearInputOrGate.getOutput(),
												_clearInputOrGateToIntegClearInputWire
												);
	_clearInputOrGate.getInput().attach(		_failureSuppInput.getOutput(),
												_failureSuppInputToIntegSetWire
												);
	_defectHierarchyOutput.getInput().attach(	_dataInput.getOutput(),
												_dataInputToDefectOutputWire
												);
	_defectHierarchyOutput.getInput().attach(	_defectSuppInput.getOutput(),
												_defectSuppInputToDefectOutputWire
												);
	_failureHierarchyOutput.getInput().attach(	_integrator.getOutput(),
												_integOutputToFailureOutputWire
												);
	_failureHierarchyOutput.getInput().attach(	_failureSuppInput.getOutput(),
												_failureSuppToFailureOutputWire
												);
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getDefectSummaryOutput() noexcept{
	return _defectSuppInput.getOutput();
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getFailureSummaryOutput() noexcept{
	return _failureSuppInput.getOutput();
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getDefectOutput() noexcept{
	return _dataInput.getOutput();
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getFailureOutput() noexcept{
	return _integrator.getOutput();
	}

Seq::InputApi&		Logic::Lsi::HierInteg::getIntegClockInput() noexcept{
	return _integrator.getClockInput();
	}

Logic::InputApi&		Logic::Lsi::HierInteg::getIntegDataDefectInput() noexcept{
	return _dataInput.getInput();
	}

Logic::InputApi&		Logic::Lsi::HierInteg::getDefectSuppInput() noexcept{
	return _defectSuppInput.getInput();
	}

Logic::InputApi&		Logic::Lsi::HierInteg::getFailureSuppInput() noexcept{
	return _failureSuppInput.getInput();
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getDefectHierOutput() noexcept{
	return _defectHierarchyOutput.getOutput();
	}

Logic::OutputApi&	Logic::Lsi::HierInteg::getFailureHierOutput() noexcept{
	return _failureHierarchyOutput.getOutput();
	}

Logic::InputApi&	Logic::Lsi::HierInteg::getSetInput() noexcept{
	return _setInputOrGate.getInput();
	}

Logic::InputApi&	Logic::Lsi::HierInteg::getClearInput() noexcept{
	return _clearInputOrGate.getInput();
	}
