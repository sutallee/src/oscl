/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_logic_lsi_integratorh_
#define _oscl_logic_lsi_integratorh_
#include "oscl/logic/seq/api.h"
#include "intapi.h"
#include "oscl/logic/output.h"

/** */
namespace Oscl {
/** */
namespace Logic {
/** */
namespace Lsi {

/** */
class Integ :	public IntegApi,
				protected Seq::InputApi,
				protected Seq::Api
				{
	private:
		/** */
		class Inhibit :	public Logic::InputApi,
						public Logic::StateApi
						{
			private:
				/** */
				Integ&		_context;
				/** */
				bool		_output;
				/** */
				unsigned	_assertedCount;
			public:
				/** */
				explicit Inhibit(Integ& context) noexcept;
				/** */
				const bool&	getOutput() noexcept;
			public:
				/** */
				void	attach(	Logic::OutputApi&	output,
								Logic::Wire&		wire)
								noexcept;
				/** */
				void	detach(	Logic::OutputApi&	output,
								Logic::Wire&		wire
								) noexcept;
			public:
				/** */
				void	asserted() noexcept;
				/** */
				void	negated() noexcept;
			};
		/** */
		class Set :	public Logic::InputApi,
					public Logic::StateApi
					{
			private:
				/** */
				Integ&	_context;
				/** */
				bool				_output;
				/** */
				unsigned			_assertedCount;
			public:
				/** */
				explicit Set(Integ& context) noexcept;
				/** */
				const bool&	getOutput() noexcept;
			public:
				/** */
				void	attach(	Logic::OutputApi&	output,
								Logic::Wire&		wire
								) noexcept;
				/** */
				void	detach(	Logic::OutputApi&	output,
								Logic::Wire&		wire
								) noexcept;
			public:
				/** */
				void	asserted() noexcept;
				/** */
				void	negated() noexcept;
			};

		/** */
		class Clear :	public Logic::InputApi,
						public Logic::StateApi
						{
			private:
				/** */
				Integ&		_context;
				/** */
				bool		_output;
				/** */
				unsigned	_assertedCount;
			public:
				/** */
				explicit Clear(Integ& context) noexcept;
				/** */
				const bool&	getOutput() noexcept;
			public:
				/** */
				void	attach(	Logic::OutputApi&	output,
								Logic::Wire&		wire
								) noexcept;
				/** */
				void	detach(	Logic::OutputApi&	output,
								Logic::Wire&		wire
								) noexcept;
			public:
				/** */
				void	asserted() noexcept;
				/** */
				void	negated() noexcept;
			};
	private:
		/** */
		signed int					_maxValue;
		/** */
		signed int					_currentCount;
		/** */
		signed int					_upCountValue;
		/** */
		signed int					_downCountValue;
		/** */
		static const signed int		_minValue	= 0;
	private:
		/** */
		Logic::Output	_output;
		/** */
		Inhibit			_inhibitInput;
		/** */
		Set				_setInput;
		/** */
		Clear			_clearInput;
		/** */
		const bool&		_inhibited;
		/** */
		const bool&		_set;
		/** */
		const bool&		_cleared;
		/** */
		bool			_integratorState;
	public:
		/** */
		explicit Integ(	const bool&	dataInput,
						unsigned	nTicks,
						unsigned	upToDownRatio
						) noexcept;
	public:
		/** */
		Seq::InputApi&		getClockInput() noexcept;
		/** */
		Logic::OutputApi&	getOutput() noexcept;
		/** */
		Logic::InputApi&	getInhibit() noexcept;
		/** */
		Logic::InputApi&	getSet() noexcept;
		/** */
		Logic::InputApi&	getClear() noexcept;
	private:
		/** */
		void	attach(Seq::SubjectApi& output,Seq::Wire& wire) noexcept;
		/** */
		void	detach(Seq::SubjectApi& output,Seq::Wire& wire) noexcept;
	private:
		/** */
		void	tick() noexcept;
	public:
		/** */
		void	clearAsserted() noexcept;
		/** */
		void	clearNegated() noexcept;
		/** */
		void	setAsserted() noexcept;
		/** */
		void	setNegated() noexcept;
		/** */
		void	evaluateOutput() noexcept;
	};
};
};
};

#endif
