/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_logic_lsi_hintegratorh_
#define _oscl_logic_lsi_hintegratorh_
#include "hintapi.h"
#include "integrator.h"
#include "oscl/logic/gate/or.h"

/** */
namespace Oscl {
/** */
namespace Logic {
/** */
namespace Lsi {

/** */
class HierInteg : public HierIntegApi {
	private:
		/** */
		Logic::Wire			_defectSuppInputToIntegInhibitWire;
		/** */
		Logic::Wire			_failureSuppInputToIntegSetWire;
		/** */
		Logic::Wire			_dataInputToDefectOutputWire;
		/** */
		Logic::Wire			_defectSuppInputToDefectOutputWire;
		/** */
		Logic::Wire			_integOutputToFailureOutputWire;
		/** */
		Logic::Wire			_failureSuppToFailureOutputWire;
		/** */
		Logic::Wire			_setInputOrGateToIntegSetInputWire;
		/** */
		Logic::Wire			_clearInputOrGateToIntegClearInputWire;
	private:
		/** */
		Logic::Gate::Or		_dataInput;
		/** */
		Logic::Gate::Or		_defectSuppInput;
		/** */
		Logic::Gate::Or		_failureSuppInput;
		/** */
		Logic::Gate::Or		_defectHierarchyOutput;
		/** */
		Logic::Gate::Or		_failureHierarchyOutput;
		/** */
		Logic::Gate::Or		_clearInputOrGate;
		/** */
		Logic::Gate::Or		_setInputOrGate;
	private:
		/** */
		Integ	_integrator;
	public:
		/** */
		explicit HierInteg(	unsigned	nTicks,
							unsigned	upToDownRatio
							)noexcept;
		/** */
		Logic::OutputApi&		getDefectSummaryOutput() noexcept;
		/** */
		Logic::OutputApi&		getFailureSummaryOutput() noexcept;
	public:
		/** */
		Seq::InputApi&			getIntegClockInput() noexcept;
		/** */
		Logic::InputApi&		getIntegDataDefectInput() noexcept;
		/** */
		Logic::InputApi&		getDefectSuppInput() noexcept;
		/** */
		Logic::InputApi&		getFailureSuppInput() noexcept;
		/** */
		Logic::OutputApi&		getDefectHierOutput() noexcept;
		/** */
		Logic::OutputApi&		getFailureHierOutput() noexcept;
		/** */
		Logic::InputApi&		getSetInput() noexcept;
		/** */
		Logic::InputApi&		getClearInput() noexcept;
		/** */
		Logic::OutputApi&		getDefectOutput() noexcept;
		/** */
		Logic::OutputApi&		getFailureOutput() noexcept;
	};

};
};
};

#endif
