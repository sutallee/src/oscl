/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "simple.h"

using namespace Oscl::Bus;

SimpleBus::SimpleBus(	void*			cpuAperatureBase,
						unsigned long	aperatureSize,
						unsigned long	busAperatureBase
						) noexcept:
		_cpuAperatureBase((unsigned long)cpuAperatureBase),
		_aperatureSize(aperatureSize),
		_busAperatureBase(busAperatureBase),
		_cpuAperatureEnd(_cpuAperatureBase+_aperatureSize-1),
		_busAperatureEnd(_busAperatureBase+_aperatureSize-1)
		{
	}

bool	SimpleBus::busToCpu(	uintptr_t	busAddress,
								void*&		cpuAddress
								) noexcept{
	if(busAddress > _busAperatureEnd) return false;
	if(busAddress < _busAperatureBase) return false;
	unsigned long	offset	= busAddress - _busAperatureBase;
	cpuAddress	= (void*)(offset + _cpuAperatureBase);
	return true;
	}

bool	SimpleBus::cpuToBus(	void*		cpuAddress,
								uintptr_t&	busAddress
								) noexcept{
	unsigned long	cpuAddr	= (unsigned long)cpuAddress;
	if(cpuAddr > _cpuAperatureEnd) return false;
	if(cpuAddr < _cpuAperatureBase) return false;
	unsigned long	offset	= cpuAddr - _cpuAperatureBase;
	busAddress	= (offset + _busAperatureBase);
	return true;
	}
