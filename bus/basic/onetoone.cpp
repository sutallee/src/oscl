/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "onetoone.h"

using namespace Oscl::Bus;

OneToOne::OneToOne(	void*			aperatureBase,
					unsigned long	aperatureSize
					) noexcept:
		_aperatureBase((unsigned long)aperatureBase),
		_aperatureSize(aperatureSize),
		_aperatureEnd(_aperatureBase+_aperatureSize-1)
		{
	}

bool	OneToOne::busToCpu(	unsigned long	busAddress,
							void*&			cpuAddress
							) noexcept{
	if(busAddress > _aperatureEnd) return false;
	if(busAddress < _aperatureBase) return false;
	cpuAddress	= (void*)(busAddress);
	return true;
	}

bool	OneToOne::cpuToBus(	void*			cpuAddress,
							unsigned long&	busAddress
							) noexcept{
	unsigned long	cpuAddr	= (unsigned long)cpuAddress;
	if(cpuAddr > _aperatureEnd) return false;
	if(cpuAddr < _aperatureBase) return false;
	busAddress	= cpuAddr;
	return true;
	}

