/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bus_basic_onetooneh_
#define _oscl_bus_basic_onetooneh_
#include "oscl/bus/api.h"

/** */
namespace Oscl {
/** */
namespace Bus {

/** This class may be used in the case where the memory aperature has
	the same base address whether viewed from the CPU or the bus.
 */
class OneToOne : public Api {
	public:
		/** */
		using Api::busToCpu;
		using Api::cpuToBus;

	private:
		/** */
		const unsigned long	_aperatureBase;
		/** */
		const unsigned long	_aperatureSize;
		/** */
		const unsigned long	_aperatureEnd;

	public:
		/** */
		OneToOne(	void*			aperatureBase,
					unsigned long	aperatureSize
					) noexcept;

	public:	// BusApi
		/** Returns true on success if busAddress exists in the CPU
			address space.
		 */
		bool	busToCpu(	unsigned long	busAddress,
							void*&			cpuAddress
							) noexcept;

		/** Returns true on success if cpuAddress exists in the bus
			address space.
		 */
		bool	cpuToBus(	void*			cpuAddress,
							unsigned long&	busAddress
							) noexcept;
	};

}
}

#endif
