/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bus_basic_simpleh_
#define _oscl_bus_basic_simpleh_
#include "oscl/bus/api.h"

/** */
namespace Oscl {
/** */
namespace Bus {

/** */
class SimpleBus : public Api {
	private:
		/** */
		const unsigned long	_cpuAperatureBase;
		/** */
		const unsigned long	_aperatureSize;
		/** */
		const unsigned long	_busAperatureBase;
		/** */
		const unsigned long	_cpuAperatureEnd;
		/** */
		const unsigned long	_busAperatureEnd;

	public:
		/** */
		SimpleBus(	void*			cpuAperatureBase,
					unsigned long	aperatureSize,
					unsigned long	busAperatureBase
					) noexcept;

	public:	// BusApi
		/** Returns true on success if busAddress exists in the CPU
			address space.
		 */
		bool	busToCpu(	uintptr_t	busAddress,
							void*&		cpuAddress
							) noexcept;

		/** Returns true on success if cpuAddress exists in the bus
			address space.
		 */
		bool	cpuToBus(	void*		cpuAddress,
							uintptr_t&	busAddress
							) noexcept;
	};

}
}

#endif
