/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_sysid_basic_serviceh_
#define _oscl_sysid_basic_serviceh_
#include "oscl/sysid/mp/itc/reqapi.h"
#include "oscl/sysid/control/itc/sync.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/strings/fixed.h"

/** */
namespace Oscl {
/** */
namespace SysID {
/** */
namespace Basic {

/** */
class Service:
	public Oscl::Mt::Itc::Srv::CloseSync,
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	private Oscl::SysID::MP::ITC::Req::Api,
	private Oscl::SysID::Control::ITC::Req::Api
	{
	private:
		/** */
		Oscl::SysID::Control::ITC::Sync						_controlSync;
		/** */
		Oscl::SysID::MP::ITC::Req::Api::ConcreteSAP			_mpSAP;
		/** */
		Oscl::Strings::Fixed<64>							_sysID;
		/** */
		Oscl::Queue<	Oscl::SysID::MP::
						ITC::Req::Api::ChangeReq
						>									_observers;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
					const char*					initialSysID
					) noexcept;

		/** */
		Oscl::SysID::MP::ITC::Req::Api::SAP&	getMpSAP() noexcept;

		/** */
		Oscl::SysID::Control::ITC::Req::Api::SAP&	getControlSAP() noexcept;

		/** */
		Oscl::SysID::Control::Api&	getControlApi() noexcept;

	private:
		/** */
		void	notify() noexcept;

	private: //Oscl::Mt::Itc::Srv::Close::Req::Api,
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private: // Oscl::SysID::MP::ITC::Req::Api
		/** */
		void	request(	Oscl::SysID::MP::
							ITC::Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::SysID::MP::
							ITC::Req::Api::CancelReq&	msg
							) noexcept;
	private: // Oscl::SysID::Control::ITC::Req::Api
		/** */
		void	request(	Oscl::SysID::Control::
							ITC::Req::Api::SetReq&		msg
							) noexcept;
	};

}
}
}

#endif
