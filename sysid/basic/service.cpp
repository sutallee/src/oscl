/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"

using namespace Oscl::SysID::Basic;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
					const char*					initialSysID
					) noexcept:
		CloseSync(*this,myPapi),
		_controlSync(*this,myPapi),
		_mpSAP(*this,myPapi),
		_sysID(initialSysID),
		_observers()
		{
	}

Oscl::SysID::MP::ITC::Req::Api::SAP&	Service::getMpSAP() noexcept{
	return _mpSAP;
	}

Oscl::SysID::Control::ITC::Req::Api::SAP&	Service::getControlSAP() noexcept{
	return _controlSync.getSAP();
	}

Oscl::SysID::Control::Api&	Service::getControlApi() noexcept{
	return _controlSync;
	}

void	Service::notify() noexcept{
	Oscl::SysID::MP::ITC::Req::Api::ChangeReq*	next;
	while((next=_observers.get())){
		next->_payload._clientValue	= _sysID;
		next->returnToSender();
		}
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	msg.returnToSender();
	}

void	Service::request(	Oscl::SysID::MP::
							ITC::Req::Api::ChangeReq&	msg
							) noexcept{
	if(msg._payload._clientValue == _sysID){
		_observers.put(&msg);
		return;
		}
	msg._payload._clientValue	= _sysID;
	msg.returnToSender();
	}

void	Service::request(	Oscl::SysID::MP::
							ITC::Req::Api::CancelReq&	msg
							) noexcept{
	Oscl::SysID::MP::ITC::Req::Api::ChangeReq*
	req	= _observers.remove(&msg._payload._changeToCancel);
	if(req){
		req->returnToSender();
		}
	msg.returnToSender();
	}

void	Service::request(	Oscl::SysID::Control::
							ITC::Req::Api::SetReq&		msg
							) noexcept{
	_sysID	= msg._payload._newValue;
	notify();
	msg.returnToSender();
	}

