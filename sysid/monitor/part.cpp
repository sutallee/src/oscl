/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::SysID::Monitor;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
			Oscl::SysID::MP::ITC::Req::Api::SAP&	mpSAP,
			Oscl::Strings::Api&						localSysID,
			Oscl::Strings::Api&						payloadSysID
			) noexcept:
		_myPapi(myPapi),
		_mpSAP(mpSAP),
		_localSysID(localSysID),
		_payload(payloadSysID),
		_resp(	mpSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_done(0)
		{
	}

void	Part::start() noexcept{
	_mpSAP.post(_resp.getSrvMsg());
	}

void	Part::stop(	Oscl::SysID::MP::ITC::Resp::CancelMem&	mem,
					Oscl::Done::Api&						done
					) noexcept{
	_done	= &done;
	Oscl::SysID::MP::ITC::Req::Api::CancelPayload*
	payload	= new(&mem.payload)
				Oscl::SysID::MP::ITC::
				Req::Api::CancelPayload(_resp.getSrvMsg());
	Oscl::SysID::MP::ITC::Resp::Api::CancelResp*
	resp	= new(&mem.resp)
				Oscl::SysID::MP::ITC::
				Resp::Api::CancelResp(	_mpSAP.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_mpSAP.post(resp->getSrvMsg());
	}

void	Part::response(	Oscl::SysID::MP::ITC::
						Resp::Api::ChangeResp&	msg
						) noexcept{
	if(_done){
		return;
		}
	_localSysID	= msg._payload._clientValue;
	start();
	}

void	Part::response(	Oscl::SysID::MP::ITC::
						Resp::Api::CancelResp&	msg
						) noexcept{
	_done->done();
	_done	= 0;
	}

