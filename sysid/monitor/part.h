/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_sysid_monitor_parth_
#define _oscl_sysid_monitor_parth_
#include "oscl/sysid/mp/itc/respmem.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace SysID {
/** */
namespace Monitor {

/** */
class Part :
	Oscl::SysID::MP::ITC::Resp::Api
	{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		Oscl::SysID::MP::ITC::Req::Api::SAP&			_mpSAP;
		/** */
		Oscl::Strings::Api&								_localSysID;
		/** */
		Oscl::SysID::MP::ITC::Req::Api::ChangePayload	_payload;
		/** */
		Oscl::SysID::MP::ITC::Resp::Api::ChangeResp		_resp;
		/** */
		Oscl::Done::Api*								_done;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
				Oscl::SysID::MP::ITC::Req::Api::SAP&	mpSAP,
				Oscl::Strings::Api&						localSysID,
				Oscl::Strings::Api&						payloadSysID
				) noexcept;
		/** */
		void	start() noexcept;
		/** */
		void	stop(	Oscl::SysID::MP::ITC::Resp::CancelMem&	mem,
						Oscl::Done::Api&						done
						) noexcept;

	private:
		/** */
		void	response(	Oscl::SysID::MP::ITC::
							Resp::Api::ChangeResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::SysID::MP::ITC::
							Resp::Api::CancelResp&	msg
							) noexcept;
	};

}
}
}

#endif
