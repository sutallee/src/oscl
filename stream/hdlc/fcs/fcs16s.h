/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_fcs16sh_
#define _oscl_stream_hdlc_fcs16sh_
#include "api.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace FCS16S {

/** The initial FCS value */
enum{initialFCS=0xFFFF};

/** The final GOOD FCS value */
enum{goodFCS=0xF0B8};

/** The Polynomial value
	x**0 + x**5 + x**12 + x**16
	1000 0100 0000 1000 1
 */
enum{thePolynomial=0x8408};

/** This class calculates the IETF RFC 1662 CRC algoritmically
	rather than using the 512 byte table. This is done to
	save ROM storage at the cost of being slower.
 */
class Accumulator : public Oscl::Stream::HDLC::FCS::Api {
	private:
		/** */
		uint16_t	_fcs;

	public:
		/** */
		Accumulator() noexcept;

	private:
		/** This operation is invoked to initialize the
			FCS for accumulation.
		 */
		void	initialize() noexcept;

		/** This operation is invoked for each octet to
			accumulate the CRC.
		 */
		void	accumulate(unsigned char octet) noexcept;

		/** This operation is invoked by a receiver after all
			octets in the frame have been received. It returns
			true if the accumulated CRC indicates that the frame
			was received error free.
		 */
		bool	fcsGood() noexcept;

		/** Returns the number of octets in the CRC. E.g. a
			CRC16 will return 2.
		  */
		unsigned		length() noexcept;

		/** This transmitter operation prepares the accumlated
			CRC for transmission. After invoking this operation
			the transmitter will invoke the "getNextFcsOctet()"
			operation for the number of times indicated by the
			value returned from this operation.
		 */
		unsigned		prepareToGetCRC() noexcept;

		/** Returns the next octet of the CRC. This operation
			is used by a transmitter to retrieve the accumulated
			CRC in the order in which the CRC is to be append
			to the frame. This operation should be invoked
			the number of times returned by the prepareToGetCRC()
			operation.
		 */
		unsigned char	getNextFcsOctet() noexcept;
	
	};

}
}
}
}

#endif
