/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fcs16s.h"

using namespace Oscl::Stream::HDLC::FCS16S;
using namespace Oscl;

static inline uint16_t lookup(unsigned char octet) noexcept {
    uint16_t    v = octet;
    for (unsigned i = 8; i--; ){
        v = v & 1 ? (v >> 1) ^ thePolynomial : v >> 1;
        }
	return v & 0xFFFF;
    }


Accumulator::Accumulator() noexcept:
		_fcs(initialFCS)
		{
	}

void	Accumulator::initialize() noexcept{
	_fcs	= initialFCS;
	}

void	Accumulator::accumulate(unsigned char octet) noexcept{
	_fcs	=		(_fcs >> 8) ^ lookup((_fcs^(octet))&0xFF);
	}

bool	Accumulator::fcsGood() noexcept{
	return _fcs	== goodFCS;
	}

unsigned		Accumulator::length() noexcept{
	return 2;
	}

unsigned	Accumulator::prepareToGetCRC() noexcept{
	_fcs	= ~_fcs;
	return 2;
	}

unsigned char	Accumulator::getNextFcsOctet() noexcept{
	unsigned char	octet	= (unsigned char)(_fcs & 0x00FF);
	_fcs	>>= 8;
	return octet;
	}

