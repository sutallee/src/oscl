/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_tx_framerh_
#define _oscl_stream_hdlc_tx_framerh_
#include "oscl/pdu/tx/api.h"
#include "oscl/stream/output/api.h"
#include "oscl/stream/hdlc/fcs/api.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace TX {

/** */
class Framer : public Oscl::PDU::TX::Api {
	private:
		/** */
		Oscl::Stream::Output::Api&		_stream;
		/** */
		Oscl::Stream::HDLC::FCS::Api&	_fcs;

		/** */
		bool						_alwaysTxOpenningFlag;

		/** */
		bool						_flagSent;

		/** */
		unsigned long				_asyncCharMap;

	public:
		/** */
		Framer(	Oscl::Stream::Output::Api&		stream,
				Oscl::Stream::HDLC::FCS::Api&	fcs,
				unsigned long					asyncCharMap,
				bool							alwaysTxOpenningFlag
				) noexcept;

		/** */
		void	setAsyncCharMap(unsigned long asyncCharacterMap) noexcept;

	public:	// Oscl::PDU::TX::Api
		/** */
		unsigned	open(unsigned length) noexcept;
		/** */
		unsigned	append(const void* pdu,unsigned length) noexcept;
		/** */
		void		close() noexcept;

	private:
		/** Send specified octet through framer. Returns true
			when successful, or false if stream is closed.
		 */
		bool tx(unsigned char octet) noexcept;
	};
}
}
}
}

#endif
