/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "framer.h"
#include "oscl/protocol/iso/std3309.h"

using namespace Oscl::Stream::HDLC::TX;

Framer::Framer(	Oscl::Stream::Output::Api&		stream,
				Oscl::Stream::HDLC::FCS::Api&	fcs,
				unsigned long					asyncCharMap,
				bool							alwaysTxOpenningFlag
				) noexcept:
		_stream(stream),
		_fcs(fcs),
		_alwaysTxOpenningFlag(alwaysTxOpenningFlag),
		_flagSent(false),
		_asyncCharMap(asyncCharMap)
		{
	}

void	Framer::setAsyncCharMap(unsigned long asyncCharacterMap) noexcept{
	_asyncCharMap	= asyncCharacterMap;
	}

static const unsigned char	hdlcFlag	= Oscl::Protocol::ISO::Std3309::flag;
static const unsigned char	hdlcEscape	= Oscl::Protocol::ISO::Std3309::escape;

unsigned	Framer::open(unsigned length) noexcept{
	_fcs.initialize();
	if(!_flagSent){
		unsigned	sent	= _stream.write(&hdlcFlag,1);
		if(!sent){
			return 0;
			}
		}
	if(!_alwaysTxOpenningFlag){
		_flagSent	= true;
		}
	return length;
	}

unsigned	Framer::append(const void* pdu,unsigned length) noexcept{
	const unsigned char*	p	= (const unsigned char*)pdu;
	for(unsigned i=0;i<length;++i,++p){
		_fcs.accumulate(*p);
		if(!tx(*p)){
			/** Stream closed */
			return 0;
			}
		}
	return length;
	}

void		Framer::close() noexcept{
	unsigned char	octet;

	const unsigned	length	= _fcs.prepareToGetCRC();
	for(unsigned i=0;i<length;++i){
		octet	= _fcs.getNextFcsOctet();
		if(!tx(octet)){
			// Stream closed.
			return;
			}
		}

	_stream.write(&hdlcFlag,1);

	_stream.flush();
	}

bool	Framer::tx(unsigned char octet) noexcept{
	switch(octet){
		case Oscl::Protocol::ISO::Std3309::flag:
		case Oscl::Protocol::ISO::Std3309::escape:
			octet	^=0x20;
			if(!_stream.write(&hdlcEscape,1)){
				// Stream closed
				_flagSent	= false;
				return false;
				}
			break;
		default:
			if(octet < 0x20){
				if(_asyncCharMap & (1UL<<octet)){
					octet	^= 0x20;
					if(!_stream.write(&hdlcEscape,1)){
						// Stream closed
						_flagSent	= false;
						return false;
						}
					}
				}
			break;
		}
	if(!_stream.write(&octet,1)){
		// Stream closed
		_flagSent	= false;
		return false;
		}
	return true;
	}

