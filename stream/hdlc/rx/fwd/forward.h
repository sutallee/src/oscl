/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_rx_fwd_forwardh_
#define _oscl_stream_hdlc_rx_fwd_forwardh_
#include "oscl/pdu/fwd/api.h"
#include "oscl/pdu/buffer/allocator/itc/fixed.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/stream/hdlc/rx/framer.h"
#include "oscl/stream/hdlc/fcs/fcs16.h"
#include "oscl/stream/input/itc/reqapi.h"

#include "oscl/pdu/memory/itc/config.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace RX {

/** */
class Forward :	public Oscl::PDU::Buffer::Allocator::Req::Api,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Frame::Stream::RX::Api,
				public Oscl::Mt::Itc::Srv::CloseSync
				{
	private:
		/** */
		enum{bufferSize=2048};

		/** */
		enum{nBuffers=2};

		/** */
		Oscl::Pdu::Memory::ITC::Config<
			bufferSize,	// bufferSize
			nBuffers,	// nBuffers
			nBuffers,	// nFixed
			0,	// nFragment
			0	// nComposite
			>	_allocator;

	private:
#if 0
		/** */
		union FixedMem {
			void*	__qitemlink;
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::PDU::Buffer::
									Allocator::Fixed<bufferSize>
									)
							>	mem;
			};

		/** */
		FixedMem						_buffMem[nBuffers];
		/** */
		Oscl::Queue<FixedMem>			_freeBuffers;
#endif

		/** */
		Oscl::Mt::Itc::PostMsgApi&		_myPapi;
		/** */
		Oscl::PDU::Buffer::Allocator::Req::Api::ConcreteSAP	_allocSAP;
		/** */
		Oscl::Pdu::FWD::Api&				_receiver;

#if 0
		/** */
		Oscl::PDU::Buffer::Allocator::Fixed<bufferSize>*	_current;
#endif
		/** */
		Oscl::Pdu::Fixed*	_current;

		/** */
		unsigned		_length;
		/** */
		unsigned char*	_buffer;

	public:
		/** */
		Forward(	Oscl::Mt::Itc::PostMsgApi&		papi,
					Oscl::Pdu::FWD::Api&			receiver
					) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::PDU::Buffer::Allocator::Req::Api
		/** */
		void	request(	Oscl::PDU::Buffer::
							Allocator::Req::FreeReq&	msg
							) noexcept;

	private:	// Oscl::Frame::Stream::RX::Api
		/** */
		void		open() noexcept;

		/** */
		unsigned	append(unsigned char octet) noexcept;

		/** */
		unsigned	append(const void* octets,unsigned nOctets) noexcept;

		/** */
		void		close(unsigned nFcsOctets) noexcept;

		/** */
		void		abort() noexcept;

#if 0
	private:
		/** */
		void		free() noexcept;
		/** */
		FixedMem*	alloc() noexcept;
#endif
	};

}
}
}
}

#endif
