/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "forward.h"

using namespace Oscl::Stream::HDLC::RX;

Forward::Forward(	Oscl::Mt::Itc::PostMsgApi&			papi,
					Oscl::Pdu::FWD::Api&				receiver
					) noexcept:
		CloseSync(*this,papi),
		_allocator(
			papi,
			"Oscl::Stream::HDLC::RX::Forward"
			),
		_myPapi(papi),
		_allocSAP(*this,papi),
		_receiver(receiver),
		_current(0)
		{
#if 0
	for(unsigned i=0;i<nBuffers;++i){
		_freeBuffers.put(&_buffMem[i]);
		}
#endif
	}

void	Forward::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	msg.returnToSender();
	}

void	Forward::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	// FIXME: Not implemented
	// Should cancel outstanding stream read requests
	for(;;);
	}

void	Forward::request(	Oscl::PDU::Buffer::
							Allocator::Req::FreeReq&	msg
							) noexcept{
	// FIXME: Not implemented yet
	// Frame buffer is being returned from upper layer client.
#if 0
	FixedMem*	mem	= (FixedMem*)msg._user;
	msg.~FreeReq();
	_freeBuffers.put(mem);
#endif
	}

void		Forward::open() noexcept{
	unsigned	allocatedSize;
	void*
	buffer	= _allocator.allocBuffer(allocatedSize);

	if(!buffer){
		_current	= 0;
		return;
		}

	Oscl::Pdu::Fixed*
	current	= _allocator.allocFixed(
				buffer,
				allocatedSize
				);

	if(!current){
		_current	= 0;
		return;
		}

#if 0
	FixedMem*	mem	= _freeBuffers.get();
	if(!mem){
		_current	= 0;
		return;
		}
	_current	= new(mem)
					Oscl::PDU::Buffer::Allocator::
					Fixed<bufferSize>(_allocSAP);

	_buffer		= _current->getBuffer();
	_length		= 0;
#endif

//	Oscl::Error::Info::log("Opened\n\r");
	}

//static void printOctet(unsigned char octet) noexcept{
//	char	buffer[8];
//	sprintf(buffer,"0x%2.2X ",(unsigned)octet);
//	Oscl::Error::Info::log(buffer);
//	}

unsigned	Forward::append(unsigned char octet) noexcept{
	if(!_current) return 0;
#if 0
	unsigned	len	= _current->_fixedBuff.append(&octet,1);
#else
	unsigned	len	= _current->append(&octet,1);
#endif
//	if(len) printOctet(octet);
	return len;
	}

unsigned	Forward::append(const void* octets,unsigned nOctets) noexcept{
	if(!_current) return 0;
#if 0
	nOctets	= _current->_fixedBuff.append(octets,nOctets);
#else
	nOctets	= _current->append(octets,nOctets);
#endif

//	const unsigned char*	p	= (const unsigned char*)octets;
//	for(unsigned i=0;i<nOctets;++i){
//		printOctet(p[i]);
//		}

	_length	+= nOctets;
	return nOctets;
	}

void		Forward::close(unsigned nFcsOctets) noexcept{

	if(!_current){
		return;
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= _current;

	_current	= 0;

	pdu->stripTrailer(nFcsOctets);


	_receiver.transfer(pdu);

//	Oscl::Error::Info::log("\n\rClose\n\r");
	}

void		Forward::abort() noexcept{

	if(!_current) {
		return;
		}

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= _current;

	_current	= 0;

//	Oscl::Error::Info::log("\n\rAborted\n\r");
	}

