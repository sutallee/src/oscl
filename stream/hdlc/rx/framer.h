/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_rx_framerh_
#define _oscl_stream_hdlc_rx_framerh_
#include "oscl/frame/stream/rx/api.h"
#include "oscl/stream/input/api.h"
#include "oscl/stream/hdlc/fcs/api.h"
#include "oscl/stream/framer/rx/api.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace RX {

/** */
class ErrorApi {
	public:
		/** */
		virtual ~ErrorApi() {}
		/** */
		virtual void	badCRC() noexcept=0;
		/** */
		virtual void	discardDueToResourceShortage() noexcept=0;
		/** */
		virtual void	invalidFrameStructure() noexcept=0;
	};

/** */
class Framer : public Oscl::Stream::Framer::RX::Api {
	private:
		/** */
		Oscl::Frame::Stream::RX::Api&		_receiver;
		/** */
		Oscl::Stream::HDLC::FCS::Api&		_fcs;
		/** */
		Oscl::Stream::HDLC::RX::ErrorApi&	_errorApi;
		/** */
		unsigned long						_asyncCharMap;
		/** */
		bool								_escapeReceived;
		/** */
		bool								_receivingFrame;
		/** */
		bool								_dataReceived;

	public:
		/** */
		Framer(	Oscl::Frame::Stream::RX::Api&	receiver,
				Oscl::Stream::HDLC::FCS::Api&	fcs,
				ErrorApi&						errorApi,
				unsigned long					asyncCharMap
				) noexcept;

		/** */
		Framer(	Oscl::Frame::Stream::RX::Api&	receiver,
				Oscl::Stream::HDLC::FCS::Api&	fcs,
				unsigned long					asyncCharMap
				) noexcept;

		/** */
		void	setAsyncCharacterMap(unsigned long asyncCharMap) noexcept;

	public: // Oscl::Stream::Framer::RX::Api
		/** */
		void	input(const void* buffer,unsigned length) noexcept;

	};

}
}
}
}

#endif
