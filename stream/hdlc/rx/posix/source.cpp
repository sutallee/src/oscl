/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "source.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Stream::HDLC::RX;

Source::Source(	Oscl::Datagram::TX::Contiguous::Api&	dest,
				Oscl::Stream::Input::Api&				inStream
				) noexcept:
		_fcs(),
		_framer(*this,_fcs,0),
		_dest(dest),
		_inStream(inStream)
		{
	}

enum{readBufferSize=256};

void	Source::run() noexcept{
	unsigned char	buffer[readBufferSize];
	unsigned long	length;
	while(true){
		length	= _inStream.read(buffer,readBufferSize);
		if(!length){
			break;
			}
		_framer.input(buffer,(unsigned)length);
		}

	while(true){
		// FIXME: what to do in case of stream close/error.
		Oscl::Mt::Thread::sleep(60*60*1000);
		}
	}

void		Source::open() noexcept{
	_buffer.reset();
	}

unsigned	Source::append(unsigned char octet) noexcept{
	return _buffer.append(&octet,1);
	}

unsigned	Source::append(const void* octets,unsigned nOctets) noexcept{
	return _buffer.append(octets,nOctets);
	}

void		Source::close(unsigned nFcsOctets) noexcept{
	_buffer.stripTrailer(nFcsOctets);
	_dest.send(_buffer.getBuffer(),_buffer.length());
	}

void		Source::abort() noexcept{
	_buffer.reset();
	}

