/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_rx_posix_sourceh_
#define _oscl_stream_hdlc_rx_posix_sourceh_
#include "oscl/mt/runnable.h"
#include "oscl/stream/hdlc/rx/framer.h"
#include "oscl/stream/hdlc/fcs/fcs16.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/datagram/tx/contiguous/api.h"
#include "oscl/buffer/fixed.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace RX {

/** */
class Source :	public Oscl::Mt::Runnable,
				private Oscl::Frame::Stream::RX::Api
				{
	private:
		/** */
		Oscl::Stream::HDLC::FCS16::Accumulator	_fcs;
		/** */
		Oscl::Stream::HDLC::RX::Framer			_framer;
		/** */
		Oscl::Datagram::TX::Contiguous::Api&	_dest;
		/** */
		Oscl::Stream::Input::Api&				_inStream;
		/** */
		enum{maxBufferSize=2048};
		/** */
		Oscl::Buffer::Fixed<maxBufferSize>		_buffer;

	public:
		/** */
		Source(	Oscl::Datagram::TX::Contiguous::Api&	dest,
				Oscl::Stream::Input::Api&				inStream
				) noexcept;

	private:	// Oscl::Mt::Runnable
		void	run() noexcept;

	private: // Oscl::Frame::Stream::RX::Api
		/** */
		void		open() noexcept;

		/** */
		unsigned	append(unsigned char octet) noexcept;

		/** */
		unsigned	append(const void* octets,unsigned nOctets) noexcept;
		/** */
		void		close(unsigned nFcsOctets) noexcept;

		/** */
		void		abort() noexcept;
	};

}
}
}
}

#endif
