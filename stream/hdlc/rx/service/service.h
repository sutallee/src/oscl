/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_rx_service_serviceh_
#define _oscl_stream_hdlc_rx_service_serviceh_
#include "oscl/pdu/rx/api.h"
#include "oscl/stream/input/itc/respmem.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/stream/hdlc/rx/framer.h"
#include "oscl/stream/hdlc/fcs/fcs16.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace RX {

/** */
struct StreamReadBuffer {
	/** */
	void*	__qitemlink;
	};

/** */
union RxTransMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(RxTrans)>	trans;
	};

/** */
class Service :	public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Mt::Itc::Srv::CloseSync
				{
	private:
		/** */
		Oscl::Stream::Input::Resp::CancelMem	_cancelMem;
		/** */
		Oscl::Queue<RxTransMem>					_freeTransMem;
		/** */
		Oscl::Queue<StreamReadBuffer>			_freeStreamReadBuffer;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Stream::HDLC::FCS16::Accumulator	_fcs;
		/** */
		Oscl::Stream::HDLC::RX::Framer			_framer;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		_streamSAP;
		/** */
		Oscl::Queue<RxTrans>					_pendingRxTrans;
		/** */
		const unsigned							_bufferSize;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*				_closeReq;

	public:
		/** Constraints:
			o	The transMem array *must* have nTransactions
				elements.
			o	bufferMem *must* have nTransactions elements
				each element *must* be bufferSize bytes in length.
				e.g. bufferMem must be nTransactions*bufferSize
				bytes in length.
			o 	note that transMem and bufferMem are related
				by nTransactions.
			o	bufferSize must be a multiple of 4 bytes or
				greater even power of 2 alignment.
			o	bufferMem must be aligned on the same boundary
				as bufferSize.
		 */
		Service(	Oscl::Mt::Itc::PostMsgApi&			papi,
					Oscl::Frame::Stream::RX::Api&		forward,
					Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
					RxTransMem							transMem[],
					void*								bufferMem,
					unsigned							nTransactions,
					unsigned							bufferSize
					) noexcept;

		/** */
		friend class RxTrans;
		/** */
		void	done(RxTrans& trans) noexcept;
		/** */
		void	error(RxTrans& trans) noexcept;
		/** */
		void	canceled(RxTrans& trans) noexcept;
		/** */
		void	free(RxTrans& trans) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:
		/** */
		void	sendReads() noexcept;

		/** */
		void	cancelReads() noexcept;
	};

}
}
}
}

#endif
