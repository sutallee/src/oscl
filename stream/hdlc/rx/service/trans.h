/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_hdlc_rx_service_transh_
#define _oscl_stream_hdlc_rx_service_transh_
#include "oscl/pdu/rx/api.h"
#include "oscl/stream/input/itc/respmem.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace HDLC {
/** */
namespace RX {

class Service;

/** */
class RxTrans :	public Oscl::Stream::Input::Resp::Api,
				public Oscl::QueueItem
				{
	public:
		/** */
		Oscl::Stream::Input::Req::Api::ReadPayload	_payload;

	private:
		/** */
		Oscl::Stream::Input::Resp::Api::ReadResp	_resp;
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Service&									_context;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&			_streamSAP;
		/** */
		bool										_canceling;

	public:
		/** */
		RxTrans(	Service&							context,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
					void*								buffer,
					unsigned							bufferSize
					) noexcept;

		/** */
		void	cancel(Oscl::Stream::Input::Resp::CancelRespMem& mem) noexcept;

	private:	// Oscl::Stream::Input::Resp::Api
		/** */
		void	response(	Oscl::Stream::
							Input::Resp::Api::ReadResp&	msg
							) noexcept;

		/** */
		void	response(	Oscl::Stream::
							Input::Resp::Api::CancelResp&	msg
							) noexcept;

	};

}
}
}
}

#endif
