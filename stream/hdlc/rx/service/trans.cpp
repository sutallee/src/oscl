/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "trans.h"
#include "service.h"

using namespace Oscl::Stream::HDLC::RX;

RxTrans::RxTrans(	Service&							context,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
					void*								buffer,
					unsigned							bufferSize
					) noexcept:
		_payload(buffer,bufferSize),
		_resp(	streamSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_myPapi(myPapi),
		_context(context),
		_streamSAP(streamSAP),
		_canceling(false)
		{
	}

void	RxTrans::cancel(Oscl::Stream::Input::Resp::CancelRespMem& mem) noexcept{
	Oscl::Stream::Input::Req::Api::CancelPayload*
	payload	= new(&mem.payload)
				Oscl::Stream::Input::Req::Api::
				CancelPayload(_resp.getSrvMsg());
	Oscl::Stream::Input::Resp::Api::CancelResp*
	resp	= new(&mem.resp)
				Oscl::Stream::Input::Resp::Api::
				CancelResp(	_streamSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_streamSAP.post(resp->getSrvMsg());
	_canceling	= true;
	}

void	RxTrans::response(	Oscl::Stream::
							Input::Resp::Api::ReadResp&	msg
							) noexcept{
	if(_canceling) return;
	if(msg._payload._buffer.length()){
		_context.done(*this);
		}
	else {
		_context.error(*this);
		}
	}

void	RxTrans::response(	Oscl::Stream::
							Input::Resp::Api::CancelResp&	msg
							) noexcept{
	msg.~CancelResp();
	msg._payload.~CancelPayload();
	_context.canceled(*this);
	}

