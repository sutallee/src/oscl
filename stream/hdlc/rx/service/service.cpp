/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "service.h"
#include "oscl/error/info.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Stream::HDLC::RX;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&			papi,
					Oscl::Frame::Stream::RX::Api&		forward,
					Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
					RxTransMem							transMem[],
					void*								bufferMem,
					unsigned							nTransactions,
					unsigned							bufferSize
					) noexcept:
		CloseSync(*this,papi),
		_myPapi(papi),
		_fcs(),
		_framer(forward,_fcs,0),
		_streamSAP(streamSAP),
		_bufferSize(bufferSize),
		_closeReq(0)
		{
	unsigned char*	buff	= (unsigned char*)bufferMem;
	for(unsigned i=0;i<nTransactions;++i,buff += bufferSize){
		_freeTransMem.put(&transMem[i]);
		_freeStreamReadBuffer.put((StreamReadBuffer*)buff);
		}
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	sendReads();
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	cancelReads();
	}

void	Service::done(RxTrans& trans) noexcept{
	_pendingRxTrans.remove(&trans);
	if(_closeReq){
		free(trans);
		return;
		}
	const void*		buffer	= trans._payload._buffer.getBuffer();
	const unsigned	length	= trans._payload._buffer.length();
	_framer.input(buffer,length);
	free(trans);
	sendReads();
	}

void	Service::error(RxTrans& trans) noexcept{
	_pendingRxTrans.remove(&trans);
	free(trans);
	}

void	Service::canceled(RxTrans& trans) noexcept{
	free(trans);
	cancelReads();
	}

void	Service::free(RxTrans& trans) noexcept{
	StreamReadBuffer*
	buffer	= (StreamReadBuffer*)trans._payload._buffer.getBuffer();
	_freeStreamReadBuffer.put(buffer);

	trans.~RxTrans();
	_freeTransMem.put((RxTransMem*)&trans);
	}

void	Service::sendReads() noexcept{
	RxTransMem*	mem;
	while((mem=_freeTransMem.get())){
		StreamReadBuffer*
		buffer	= _freeStreamReadBuffer.get();
		RxTrans*	trans	= new (mem) RxTrans(	*this,
													_myPapi,
													_streamSAP,
													buffer,
													_bufferSize
													);
		_pendingRxTrans.put(trans);
		}
	}

void	Service::cancelReads() noexcept{
	RxTrans*	trans	= _pendingRxTrans.get();
	if(trans){
		trans->cancel(_cancelMem.cancel);
		return;
		}
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

