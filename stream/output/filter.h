/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_output_filterh_
#define _oscl_stream_output_filterh_
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Output {

/** This abstract class forms the base class from which output
	stream filters may be built. This is a decorator as described
	in the GOF Decorator pattern.
 */
class Filter : public Api {
	protected:
		/**
		 */
		Api&	_outputStream;

	public:
		/** This constructor requires a reference to the output
			stream to which the filter is applied.
		 */
		Filter(Api& outputStream) noexcept;

	public:
		/** Copies at most maxSize number of bytes to the stream
			from the array specified by dest. The implementation
			applies filtering and writes the filtered results
			to the _outputStream.
			RETURNS:
			On success, returns the number of bytes accepted by the filter
			and the current position is advanced by this number. Zero
			is returned if the end of the stream is reached.
		 */
		virtual unsigned long	read(	void*			dest,
										unsigned long	maxSize
										) noexcept=0;
	};

}
}
}

#endif
