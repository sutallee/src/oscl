/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_output_test_testh_
#define _oscl_stream_output_test_testh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/stream/output/itc/sync.h"
#include "oscl/buffer/fixed.h"
#include "oscl/queue/queue.h"
#include "oscl/stream/output/driver/api.h"

/** */
class BufferTest :	public Oscl::Stream::Output::Req::Api,
					public Oscl::Mt::Itc::Server
					{
	private:
		/** */
		Oscl::Stream::Output::Sync			_sync;

		/** */
		Oscl::Stream::Output::Driver::Api&	_driver;

		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendFlush;

		/** */
		Oscl::Queue<WriteReq>				_pending;

		/** */
		Oscl::Queue<WriteReq>				_transmitting;

		/** */
		FlushReq*							_flushReq;

	public:
		/** */
		BufferTest(Oscl::Stream::Output::Driver::Api& driver) noexcept;

		/** */
		Oscl::Stream::Output::Api&	getStreamApi() noexcept;

		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getSAP() noexcept;

	private:	// Oscl::Mt::Itc::Server
		/** */
		void	request(WriteReq& msg) noexcept;

		/** */
		void	request(FlushReq& msg) noexcept;

		/** */
		void	mboxSignaled() noexcept;

	private:
		/** */
		void	send(WriteReq& msg) noexcept;
	};

extern BufferTest	TheBufferTest;

#endif
