/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "test.h"

static Oscl::Buffer::Fixed<25>	temp;

BufferTest::BufferTest(Oscl::Stream::Output::Driver::Api& driver) noexcept:
		_sync(*this,*this),
		_driver(driver)
		{
	}

Oscl::Stream::Output::Api&	BufferTest::getStreamApi() noexcept{
	return _sync;
	}

Oscl::Stream::Output::Req::Api::SAP&	BufferTest::getSAP() noexcept{
	return _sync.getSAP();
	}

void	BufferTest::request(WriteReq& msg) noexcept{
	if(_flushReq){
		_pendFlush.put(&msg);
		return;
		}

	send(msg);

#if 0
	Oscl::Buffer::Api&			dest	= temp;
	dest								= source;
	msg.getPayload()._nWritten			= temp.length();
	msg.returnToSender();
#endif
	}

void	BufferTest::request(FlushReq& msg) noexcept{
	}

void	BufferTest::mboxSignaled() noexcept{
	WriteReq*	msg;
	while(_driver.nextCompletedBuffer()){
		msg	= _transmitting.get();
		if(!msg){
			for(;;);	// Logic error
			}
		msg->getPayload()._nWritten	= msg->getPayload()._buffer.length();
		msg->returnToSender();
		}

	Oscl::Queue<WriteReq>	pend	= _pending;
	while((msg=pend.get())){
		send(*msg);
		}
	if(_flushReq){
		if(!_transmitting.first()){
			_flushReq->returnToSender();
			Oscl::Mt::Itc::SrvMsg*	next;
			while((next=_pendFlush.get())){
				next->process();
				}
			}
		}
	}

void	BufferTest::send(WriteReq& msg) noexcept{
	const Oscl::Buffer::Base&	source	= msg.getPayload()._buffer;

	bool	sent	= _driver.sendBuffer(	source.getBuffer(),
											source.length()
											);
	if(sent){
		_transmitting.put(&msg);
		}
	else{
		_pending.put(&msg);
		}
	}

//BufferTest	TheBufferTest;
