/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/buffer/var.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Stream::Output;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&					reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

unsigned long	Sync::write(	const void*			source,
								unsigned long		count
								) noexcept{
	if(!count) return 0;
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	// This const_cast is OK since I'm only allowing for a const Buffer::Var
	const Buffer::Var					buffer(	const_cast<void*>(source),
												count,
												count
												);
	Req::Api::WritePayload				payload(buffer);
	Req::Api::WriteReq					req(	_sap.getReqApi(),
												payload,
												srh
												);
	_sap.postSync(req);
	return payload._nWritten;
	}

unsigned long	Sync::write(const Buffer::Base&	source) noexcept{
	if(!source.length()) return 0;
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::WritePayload				payload(source);
	Req::Api::WriteReq					req(	_sap.getReqApi(),
												payload,
												srh
												);
	_sap.postSync(req);
	return payload._nWritten;
	}

/** */
void	Sync::flush() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::FlushReq					req(	_sap.getReqApi(),
												srh
												);
	_sap.postSync(req);
	return;
	}
