/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_output_itc_reqapih_
#define _oscl_stream_output_itc_reqapih_
#include "oscl/buffer/base.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Output {
/** */
namespace Req {

/** */
class Api {
	public:
#if 0
		/** */
		class WritePayload {
			public:
				/** Buffer containing bytes to be written. */
				const void*	const	_source;
				/** Output: Number of bytes to be written from buffer.
					Input: Number of bytes actually written.
				 */
				unsigned long		_count;
			public:
				/** */
				WritePayload(	const void*		source,
								unsigned long	count
								) noexcept;
			};
#endif
		/** */
		class WritePayload {
			public:
				/** */
				const Oscl::Buffer::Base&	_buffer;
				/** Returned: Number of bytes actually written. */
				unsigned long				_nWritten;
			public:
				/** */
				WritePayload(const Oscl::Buffer::Base&	buffer) noexcept;
			};
		/** */
		class FlushPayload { };
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,WritePayload>	WriteReq;
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Api,FlushPayload>	FlushReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>	SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual void	request(WriteReq& msg) noexcept=0;
		/** */
		virtual void	request(FlushReq& msg) noexcept=0;
	};

}
}
}
}

#endif
