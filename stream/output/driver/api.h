/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_output_driver_apih_
#define _oscl_stream_output_driver_apih_

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Output {
/** */
namespace Driver {

/** */
class Api {
	public: // Operational
		/** This operation is used to queue a buffer for transmission.
			When the buffer has been transmitted successfully, the
			callback is invoked. The operation returns true if the
			buffer is queued for transmission, or false if the
			transmitter does not currently have the resources available
			to queue the buffer. In the latter case, the client should
			try to queue the buffer again after another buffer has
			completed transmission.
		 */
		virtual bool		sendBuffer(	const void*		buffer,
										unsigned		length
										) noexcept=0;

		/** This operation is invoked by the driver to retrieve
			buffers that have been transmitted from the driver. This
			is typically done as the result of a transmit interrupt.
			The operation should be called repeatedly until it
			returns a zero for the buffer pointer indicating no
			more completed buffers are available.  Buffers are
			always returned in the order in which they
			are given to the driver using the sendBuffer() operation.
		 */
		virtual void*		nextCompletedBuffer() noexcept=0;
	};

}
}
}
}

#endif
