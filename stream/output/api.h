/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_output_apih_
#define _oscl_stream_output_apih_
#include "oscl/buffer/base.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Output {

/** This interface represents an interface used to write
	a sequential stream of characters.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** Copies at most maxSize number of bytes to the stream
			from the array specified by source.
			RETURNS:
			On success, returns the number of bytes accepted by the stream
			and the current stream position is advanced by this number.
			If the actual number accepted by the stream is less than the
			requested count, then the end of stream has been reached.
		 */
		virtual unsigned long	write(	const void*			source,
										unsigned long		count
										) noexcept=0;

		/** Copies at most source.length() number of bytes to the
			stream from the array specified by source.
			RETURNS:
			On success, returns the number of bytes accepted by the stream
			and the current stream position is advanced by this number.
			If the actual number accepted by the stream is less than the
			requested count, then the end of stream has been reached.
		 */
		virtual unsigned long	write(const Buffer::Base& source) noexcept=0;

		/** This operation ensures that any data that has been buffered
			in the output stream is written to the stream.
		 */
		virtual void	flush() noexcept=0;
	};

}
}
}

#endif
