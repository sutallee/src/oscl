/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_write_apih_
#define _oscl_stream_write_apih_

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Write {

/** */
class Api {
	public:
		/** Returns true if a write transaction
			is in progress. Attempts by the client
			to write more data will fail.
			Rather than poll, the client can wait for
			the writeComplete callback.
		 */
		virtual bool	writeBusy() const noexcept=0;

		/** 
			Begin write of length octets from data to the
			file-descriptor.
			RETURN:
				true if busy. No data is written.
				false if the data is queued. In this case,
				the buffer must not be used by the client
				until the write completes.
		 */
		virtual bool	write(const void* data, unsigned length) noexcept=0;

	};

}
}
}

#endif
