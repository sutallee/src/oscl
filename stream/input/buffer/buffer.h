/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_input_buffer_bufferh_
#define _oscl_stream_input_buffer_bufferh_
#include "oscl/stream/input/filter.h"
#include "oscl/buffer/fixed.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Input {

/** This class buffers stream input for more efficient
	device utilization.
 */
template <unsigned buffSize>
class Buffer : public Filter {
	private:
		/** */
		Oscl::Buffer::Fixed<buffSize>	_buffer;

	public:
		/** */
		Buffer(Api& inputStream) noexcept;

	public:
		/** At most one less than maxSize characters
			are read from stream and stored into the
			destination buffer pointed to by dest.
			Reading stops after and end of stream or
			a newline. If a newline is read, it is stored into
			the buffer. The buffer is null terminated after the
			last character in the buffer.
			Returns the number of characters placed into
			the buffer, or zero if the end of stream is
			encountered.
			NOTE: maxSize MUST be large enough to hold at least
				two bytes, or a false end-of-stream will be
				indicated.
		 */
		unsigned long	read(	void*			dest,
								unsigned long	maxSize
								) noexcept;

		/** At most one less than dest.bufferSize() characters
			are read from stream and stored into the
			destination buffer pointed to by dest.
			Reading stops after and end of stream or
			a newline. If a newline is read, it is stored into
			the buffer. The buffer is null terminated after the
			last character in the buffer.
			Returns the number of characters placed into
			the buffer, or zero if the end of stream is
			encountered.
			NOTE: The buffer MUST be large enough to hold at least
				two bytes, or a false end-of-stream will be
				indicated.
		 */
		unsigned long	read(Oscl::Buffer::Base& dest) noexcept;
	};

template <unsigned buffSize>
Buffer<buffSize>::Buffer(Api& input) noexcept:
		Filter(input)
		{
	}

template <unsigned buffSize>
unsigned long	Buffer<buffSize>::read(	void*			dest,
										unsigned long	maxSize
										) noexcept{
	if(!_buffer.length()){
		_buffer.reset();
		if(!_inputStream.read(_buffer)){
			// Stream closed
			return 0;
			}
		}
	unsigned	len	= _buffer.copyOut(dest,maxSize);
	_buffer.stripHeader(len);
	return len;
	}

template <unsigned buffSize>
unsigned long	Buffer<buffSize>::read(Oscl::Buffer::Base& dest) noexcept{
	if(!_buffer.length()){
		_buffer.reset();
		if(!_inputStream.read(_buffer)){
			// Stream closed
			return 0;
			}
		}
	unsigned	len	= _buffer.copyOut(dest);
	_buffer.stripHeader(len);
	return len;
	}

}
}
}

#endif
