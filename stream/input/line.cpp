/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "line.h"

using namespace Oscl::Stream::Input;

Line::Line(Api& inputStream) noexcept:
		Filter(inputStream)
		{
	}

unsigned long	Line::read(	void*			dest,
							unsigned long	maxSize
							) noexcept{
	char*		p=(char*)dest;
	unsigned	i;
	if(!maxSize) return 0;	// Note... this falsely indicates the end of stream
	for(i=0;i<(maxSize-1);++i,++p){
		if(!_inputStream.read(p,1)) break;
		if(*p == '\n'){
			++p;
			break;
			}
		}
	*p	= '\0';
	return i; // Notice that if maxSize is 1, end-of-stream is falsely indicated
	}

unsigned long	Line::read(Buffer::Base& dest) noexcept{
	dest.reset();
	unsigned		i;
	unsigned long	n	= dest.remaining();
	if(!n) return 0;	// Note... this falsely indicates the end of stream
	for(i=0;i<(n-1);++i){
		char	c;
		if(!_inputStream.read(&c,1)) break;
		dest	= c;
		}
	dest	= '\0';
	return i; // Notice that if the buffer size is 1, end-of-stream is falsely indicated
	}

