/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_input_driver_apih_
#define _oscl_stream_input_driver_apih_

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Input {
/** */
namespace Driver {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}

	public: // Initialization
		/** */
		virtual unsigned	requiredBufferSize() const noexcept=0;

	public: // Operational
		/** This operation is used by the client to make a buffer
			available to the driver for receiving characters. If
			the buffer is accepted, true is returned as a result.
			If the driver has enough receive buffers, the operation
			returns false.
		 */
		virtual bool		freeBuffer(	void*	buffer) noexcept=0;

		/** This operation is invoked by the driver to retrieve
			buffers that are no longer empty from the driver. This
			is typically done as the result of a receiver interrupt.
			The operation should be called repeatedly until it
			returns zero length indicating that no more non-empty
			buffers are available. The return value indicates the
			number of valid octets that have been placed contiguously
			into the buffer starting at offset zero. If a non-zero
			value is returned, the buffer pointer is updated to point
			to the buffer. Otherwise, the pointer is not changed.
			Buffers are always returned in the order in which they
			are given to the driver using the freeBuffer() operation.
		 */
		virtual unsigned		nextCompletedBuffer(void** buffer) noexcept=0;
	};

}
}
}
}

#endif
