/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "fd.h"


using namespace Oscl::Stream::Posix;

FD::FD(int inputFD,int outputFD) noexcept:
		_inputFD(inputFD),
		_outputFD(outputFD)
		{
	}

Oscl::Stream::Input::Api&	FD::input() noexcept{
	return *this;
	}

Oscl::Stream::Output::Api&	FD::output() noexcept{
	return *this;
	}

unsigned long	FD::read(	void*			dest,
							unsigned long	maxSize
							) noexcept{
	unsigned long	len;
	for(;;){
		ssize_t
		result = ::read(_inputFD,dest,maxSize);

		if(result<0){
			if(errno == EINTR){
				continue;
				}
			len	= 0;
			perror("FD::read()");
			}
		else {
			len	= (unsigned long)result; 
			}
		break;
		}

	return len;
	}

unsigned long	FD::read(Buffer::Base& dest) noexcept{
	unsigned char	buffer[32];
	unsigned long	len	= read(buffer,32);
	dest.append(buffer,len);
	return len;
	}

unsigned long	FD::write(	const void*			source,
							unsigned long		count
							) noexcept{
	return ::write(_outputFD,source,count);
	}

unsigned long	FD::write(const Buffer::Base& source) noexcept{
	return write(source.startOfData(),source.remaining());
	}

void	FD::flush() noexcept{
	// FD not buffered.
	}

