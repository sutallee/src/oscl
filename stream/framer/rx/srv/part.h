/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_framer_rx_srv_parth_
#define _oscl_stream_framer_rx_srv_parth_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/frame/stream/rx/api.h"
#include "oscl/stream/framer/rx/api.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Stream {
/** */
namespace Framer {
/** */
namespace RX {
/** */
namespace Srv {

/** */
union ReadTransMem {
	/** */
	void*			__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock<sizeof (ReadTrans) >	readMem;
	};

struct BufferMem {
	/** */
	void*			__qitemlink;
	};

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api
				{
	private:
		/** */
		friend class ReadTrans;
	private:
		/** */
		Oscl::Stream::Input::Resp::CancelRespMem	_cancelMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Oscl::Stream::Framer::RX::Api&				_frameApi;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&			_streamSAP;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP				_closeSAP;
		/** */
		Oscl::Queue<ReadTransMem>					_freeReadMem;
		/** */
		Oscl::Queue<ReadTrans>						_pendingReads;
		/** */
		Oscl::Queue<BufferMem>						_freeBufferMem;
		/** */
		const unsigned								_bufferSize;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*					_closeReq;

	public:
		/** nReads is the number of elements in
			the readMem array and the number of buffers
			of size bufferSize in the bufferMem memory.
		 */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Stream::Framer::RX::Api&		frameApi,
				Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
				ReadTransMem						readMem[],
				void*								bufferMem,
				unsigned							nReads,
				unsigned							bufferSize
				) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

	private:
		/** */
		void	done(ReadTrans& trans) noexcept;
		/** */
		void	canceled(ReadTrans& trans) noexcept;
		/** */
		void	free(ReadTrans& trans) noexcept;
		/** */
		void	sendAllReadRequests() noexcept;
		/** */
		void	cancelNextReadTrans() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	};

}
}
}
}
}

#endif
