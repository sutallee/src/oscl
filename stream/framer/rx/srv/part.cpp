/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Stream::Framer::RX::Srv;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Stream::Framer::RX::Api&		frameApi,
			Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
			ReadTransMem						readMem[],
			void*								bufferMem,
			unsigned							nReads,
			unsigned							bufferSize
			) noexcept:
		_myPapi(myPapi),
		_frameApi(frameApi),
		_streamSAP(streamSAP),
		_closeSAP(*this,myPapi),
		_bufferSize(bufferSize),
		_closeReq(0)
		{
	if(bufferSize < sizeof(BufferMem)){
		Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::Stream::RX::Part:"
										"bufferSize too small"
										);
		}
	unsigned char*	p	= (unsigned char*)bufferMem;
	for(unsigned i=0;i<nReads;++i,p += bufferSize){
		_freeReadMem.put(&readMem[i]);
		_freeBufferMem.put((BufferMem*)p);
		}
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

void	Part::done(ReadTrans& trans) noexcept{
	unsigned	length	= trans._payload._buffer.length();
	if(length){
		_frameApi.input(	trans._payload._buffer.getBuffer(),
							length
							);
		}
	else {
		// FIXME: Should count errors here
		}
	free(trans);
	if(!_closeReq){
		sendAllReadRequests();
		}
	}

void	Part::canceled(ReadTrans& trans) noexcept{
	free(trans);
	cancelNextReadTrans();
	}

void	Part::free(ReadTrans& trans) noexcept{
	_pendingReads.remove(&trans);
	const void*	buffer	= trans._payload._buffer.getBuffer();
	trans.~ReadTrans();
	_freeReadMem.put((ReadTransMem*)&trans);
	_freeBufferMem.put((BufferMem*)buffer);
	}

void	Part::sendAllReadRequests() noexcept{
	ReadTransMem*	mem;
	while((mem = _freeReadMem.get())){
		BufferMem*	buffer	= _freeBufferMem.get();
		ReadTrans*
		trans	= new(mem) ReadTrans(	*this,
										_myPapi,
										_streamSAP,
										buffer,
										_bufferSize
										);
		_pendingReads.put(trans);
		}
	}

void	Part::cancelNextReadTrans() noexcept{
	ReadTrans*	trans;
	trans	= _pendingReads.first();
	if(trans){
		trans->cancel(_cancelMem);
		return;
		}
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	sendAllReadRequests();
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	cancelNextReadTrans();
	}

