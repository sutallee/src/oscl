/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "parser.h"

using namespace Oscl::Stream::Framer::Line;

Parser::Parser(
	Oscl::Stream::Line::Parse::Api&		parseApi,
	void*								buffer,
	unsigned							bufferSize
	) noexcept:
		_parseApi(parseApi),
		_buffer((char*)buffer),
		_bufferSize(bufferSize),
		_fsm(*this)
		{
	}

void	Parser::input(const void* buffer,unsigned length) noexcept{
	const char*	p	= (const char*)buffer;

	for(unsigned i=0;i<length;++i){
		_char	= p[i];
		_fsm.input();
		}
	}

void	Parser::open() noexcept{
	_offset			= 0;
	_failed	= false;
	}

void	Parser::append() noexcept{
	if(_offset < (_bufferSize-1)){
		_buffer[_offset]	= _char;
		++_offset;
		}
	else {
		_failed	= true;
		}
	}

void	Parser::fail() noexcept{
	// do nothing
	}

void	Parser::close() noexcept{
	_buffer[_offset]	= '\0';
	_parseApi.parse(_buffer);
	}

bool	Parser::eol() noexcept{
	switch(_char){
		case '\n':
		case '\r':
		case '\0':
			return true;
		default:
			return false;
		}
	}

bool	Parser::failed() noexcept{
	return _failed;
	}

bool	Parser::esc() noexcept{
	return false;
	}

