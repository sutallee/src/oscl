/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_framer_line_parth_
#define _oscl_stream_framer_line_parth_

#include "oscl/stream/framer/rx/api.h"
#include "oscl/stream/line/parse/api.h"
#include "oscl/stream/framer/line/fsm/fsm.h"

/** */
namespace Oscl {

/** */
namespace Stream {

/** */
namespace Framer {

/** */
namespace Line {

/**	*/
class Parser :
		public Oscl::Stream::Framer::RX::Api,
		private Oscl::Stream::Framer::Line::FSM::StateVar::ContextApi
		{
	private:
		/** */
		Oscl::Stream::Line::Parse::Api&			_parseApi;

		/** */
		char*	const							_buffer;

		/** */
		const unsigned							_bufferSize;

	private:
		/** */
		Oscl::Stream::Framer::
		Line::FSM::StateVar						_fsm;

	private:
		/** */
		unsigned								_offset;

		/** */
		bool									_failed;

		/** */
		char									_char;

	public:
		/** */
		Parser(
			Oscl::Stream::Line::Parse::Api&		parseApi,
			void*								buffer,
			unsigned							bufferSize
			) noexcept;

	public:	// Oscl::Stream::Framer::RX::Api
		/** */
		 void	input(const void* buffer,unsigned length) noexcept;

	private: // Oscl::Stream::Framer::Line::FSM::::StateVar::ContextApi
		/**	*/
		void	open() noexcept;

		/**	*/
		void	append() noexcept;

		/**	*/
		void	fail() noexcept;

		/**	*/
		void	close() noexcept;

		/**	*/
		bool	eol() noexcept;

		/**	*/
		bool	failed() noexcept;

		/**	*/
		bool	esc() noexcept;

	};
}
}
}
}

#endif
