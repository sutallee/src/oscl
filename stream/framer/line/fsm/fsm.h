/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_stream_framer_line_fsm_fsmh_
#define _oscl_stream_framer_line_fsm_fsmh_

/** */
namespace Oscl {

/** */
namespace Stream {

/** */
namespace Framer {

/** */
namespace Line {

/** */
namespace FSM {

/**	This StateVar class implements a Finite State Machine
	(FSM) by applying a refinement of the GOF State Pattern,
	which is relized by applying another GOF pattern named the
	Flyweight Pattern. The Flyweight Pattern is used in the
	implementation of the state subclasses allowing a single
	set of concrete state classes to be shared between any number
	of instances of this FSM.
 */
/**	
 */
class StateVar {
	public:
		/**	This interface is implemented by the context whose behavior
			is controlled by the FSM implemented by this StateVar.
		 */
		class ContextApi {
			public:
				/** Make the compiler happy. */
				virtual ~ContextApi() {}
				/**	Start new frame/line.
				 */
				virtual void	open() noexcept=0;

				/**	Append to character to line.
				 */
				virtual void	append() noexcept=0;

				/**	Current frame/line complete.
				 */
				virtual void	close() noexcept=0;

				/**	Fail current line.
				 */
				virtual void	fail() noexcept=0;

				/**	RETURN: true if current character is an
					end-of-line character.
				 */
				virtual bool	eol() noexcept=0;

				/**	RETURN: true if last action failed.
				 */
				virtual bool	failed() noexcept=0;

				/**	RETURN: true if current character is an
					"escape" character.
				 */
				virtual bool	esc() noexcept=0;

			};
	private:
		/** */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}
				/**	
				 */
				virtual void	input(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

			};
		/** */
		friend class State;

	private:
		/**	Waiting for a non end-of-line character.
		 */
		class Hunt : public State {
			public:
				/** The cosntructor.
				 */
				Hunt() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Hunt;

	private:
		/**	Waiting for input.
		 */
		class RX : public State {
			public:
				/** The cosntructor.
				 */
				RX() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class RX;

	private:
		/**	Waiting for escaped input.
		 */
		class Escape : public State {
			public:
				/** The cosntructor.
				 */
				Escape() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Escape;

	private:
		/**	Waiting for input end-of-line after failure.
		 */
		class RxFail : public State {
			public:
				/** The cosntructor.
				 */
				RxFail() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class RxFail;

	private:
		/**	Waiting for escaped character after failure.
		 */
		class EscapeFail : public State {
			public:
				/** The cosntructor.
				 */
				EscapeFail() noexcept;

			public:
				/**	
				 */
				void	input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class EscapeFail;

	private:
		/** This is the state-less shared instance (fly-weight)
			of the Hunt state.
		 */
		const static Hunt	_Hunt;

		/** This is the state-less shared instance (fly-weight)
			of the RX state.
		 */
		const static RX	_RX;

		/** This is the state-less shared instance (fly-weight)
			of the Escape state.
		 */
		const static Escape	_Escape;

		/** This is the state-less shared instance (fly-weight)
			of the RxFail state.
		 */
		const static RxFail	_RxFail;

		/** This is the state-less shared instance (fly-weight)
			of the EscapeFail state.
		 */
		const static EscapeFail	_EscapeFail;

	private:
		/** This member references the context.
		 */
		ContextApi&		_context;

		/** This member determines the current state of the FSM.
		 */
		const State*	_state;

	public:
		/** The constructor requires a reference to its context.
		 */
		StateVar(ContextApi& context) noexcept;

		/** Be happy compiler! */
		virtual ~StateVar() {}

	public:
		/**	
		 */
		void	input() noexcept;

	private:
		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Hunt state.
		 */
		void changeToHunt() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the RX state.
		 */
		void changeToRX() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Escape state.
		 */
		void changeToEscape() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the RxFail state.
		 */
		void changeToRxFail() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the EscapeFail state.
		 */
		void changeToEscapeFail() noexcept;

	private:
	private:
		/** This operation is invoked when the FSM detects
			a protocol violation.
		 */
		void	protocolError() noexcept;
	};
}
}
}
}
}
#endif
