/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

namespace Oscl {
namespace Stream {
namespace Framer {
namespace Line {
namespace FSM {

/** */
fsm StateVar {

	/** Start new frame/line.
	 */
	action	void open;

	/** Append to character to line.
	 */
	action	void append;

	/** Current frame/line complete.
	 */
	action	void close;

	/** Fail current line.
	 */
	action	void fail;

	/** RETURN: true if current character is an end-of-line character.
	 */
	action	bool eol;

	/** RETURN: true if last action failed.
	 */
	action	bool failed;

	/** RETURN: true if current character is an "escape" character.
	 */
	action	bool esc;

	/** Waiting for a non end-of-line character.
	 */
	state Hunt {
		/** */
		event input;
		}

	/** Waiting for input.
	 */
	state RX {
		/** */
		event input;
		}

	/** Waiting for escaped input.
	 */
	state Escape {
		/** */
		event input;
		}

	/** Waiting for input end-of-line after failure.
	 */
	state RxFail {
		/** */
		event input;
		}

	/** Waiting for escaped character after failure.
	 */
	state EscapeFail {
		/** */
		event input;
		}
	}
}
}
}
}
}

