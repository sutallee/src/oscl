/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
using namespace Oscl::Stream::Framer::Line::FSM;

//--------------- Procedures ----------------
//--------------- State ----------------
//--------------- Hunt ----------------
StateVar::Hunt::Hunt() noexcept
		{
	}
void	StateVar::Hunt::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	if(context.eol()){
		return;
		}

	context.open();

	if(context.esc()){
		stateVar.changeToEscape();
		return;
		}

	context.append();

	if(context.failed()){
		stateVar.changeToRxFail();
		return;
		}

	stateVar.changeToRX();
	}
//--------------- RX ----------------
StateVar::RX::RX() noexcept
		{
	}
void	StateVar::RX::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	if(context.eol()){
		context.close();
		stateVar.changeToHunt();
		return;
		}

	if(context.esc()){
		stateVar.changeToEscape();
		return;
		}

	context.append();

	if(context.failed()){
		stateVar.changeToRxFail();
		}
	}
//--------------- Escape ----------------
StateVar::Escape::Escape() noexcept
		{
	}
void	StateVar::Escape::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	context.append();

	if(context.failed()){
		stateVar.changeToRxFail();
		return;
		}

	stateVar.changeToRX();
	}
//--------------- RxFail ----------------
StateVar::RxFail::RxFail() noexcept
		{
	}
void	StateVar::RxFail::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	if(context.eol()){
		context.fail();
		stateVar.changeToHunt();
		return;
		}

	if(context.esc()){
		stateVar.changeToEscapeFail();
		return;
		}
	}
//--------------- EscapeFail ----------------
StateVar::EscapeFail::EscapeFail() noexcept
		{
	}
void	StateVar::EscapeFail::input(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{
	stateVar.changeToRxFail();
	}

