/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
using namespace Oscl::Stream::Framer::Line::FSM;

//--------------- Concrete Fly-Weight States ----------------
const StateVar::Hunt	StateVar::_Hunt;
const StateVar::RX	StateVar::_RX;
const StateVar::Escape	StateVar::_Escape;
const StateVar::RxFail	StateVar::_RxFail;
const StateVar::EscapeFail	StateVar::_EscapeFail;

//--------------- StateVar constructor ----------------

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&_Hunt)
		{
	}

//--------------- StateVar events ----------------
void	StateVar::input() noexcept{
	_state->input(_context,*this);
	}

//--------------- StateVar state-change operations ----------------
void StateVar::changeToHunt() noexcept{
	_state	= &_Hunt;
	}

void StateVar::changeToRX() noexcept{
	_state	= &_RX;
	}

void StateVar::changeToEscape() noexcept{
	_state	= &_Escape;
	}

void StateVar::changeToRxFail() noexcept{
	_state	= &_RxFail;
	}

void StateVar::changeToEscapeFail() noexcept{
	_state	= &_EscapeFail;
	}

//--------------- State ----------------
void	StateVar::State::input(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{
	stateVar.protocolError();
	}

