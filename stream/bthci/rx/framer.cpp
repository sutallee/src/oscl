/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "framer.h"
#include "oscl/protocol/iso/std3309.h"
#include "oscl/error/info.h"

using namespace Oscl::Stream::BtHci::RX;

class nullError : public ErrorApi {
	public:
		/** */
		void	badCRC() noexcept;
		/** */
		void	discardDueToResourceShortage() noexcept;
		/** */
		void	invalidFrameStructure() noexcept;
	};

void	nullError::badCRC() noexcept{
	Oscl::Error::Info::log("\n\rBad FCS\n\r");
	}

void	nullError::discardDueToResourceShortage() noexcept{
	Oscl::Error::Info::log("\n\rOut of buffers\n\r");
	}

void	nullError::invalidFrameStructure() noexcept{
	Oscl::Error::Info::log("\n\rInvalid Frame\n\r");
	}

static nullError	nullErr;

Framer::Framer(
	Oscl::Frame::Stream::RX::Api&	receiver,
	Oscl::Stream::BtHci::FCS::Api&	fcs,
	ErrorApi&						errorApi
	) noexcept:
		_receiver(receiver),
		_fcs(fcs),
		_errorApi(errorApi),
		_escapeReceived(false),
		_receivingFrame(false),
		_dataReceived(false)
		{
	}

Framer::Framer(
	Oscl::Frame::Stream::RX::Api&	receiver,
	Oscl::Stream::BtHci::FCS::Api&	fcs
	) noexcept:
		_receiver(receiver),
		_fcs(fcs),
		_errorApi(nullErr),
		_escapeReceived(false),
		_receivingFrame(false),
		_dataReceived(false)
		{
	}

void	Framer::input(
			const void*	buffer,
			unsigned	length
			) noexcept{

	const unsigned char*	p= (unsigned char*)buffer;

	for(unsigned i=0;i<length;++i,++p){
		unsigned char	octet	= *p;
		switch(octet){
			case Oscl::Protocol::ISO::Std3309::flag:
				if(_escapeReceived){
					_errorApi.invalidFrameStructure();
					_receivingFrame	= false;
					_escapeReceived	= false;
					}
				if(_receivingFrame){
					if(!_dataReceived){
						// Just another flag
						continue;
						}
					if(_fcs.fcsGood()){
						_receiver.close(_fcs.length());
						}
					else{
						_errorApi.badCRC();
						_receiver.abort();
						}
					}
				else{
					_receivingFrame	= true;
					}
				_dataReceived	= false;
				continue;

			case Oscl::Protocol::ISO::Std3309::escape:
				if(!_escapeReceived){
					_escapeReceived	= true;
					continue;
					}
				break;
			default:
				break;
			}

		if(!_receivingFrame){
			// Discard mid frame characters
			continue;
			}

		if(octet < 0x20){
			}

		if(!_dataReceived){
			// This is the first non-flag byte received
			// and thus the start of a packet.
			_fcs.initialize();
			_receiver.open();
			_dataReceived	= true;
			}

		if(_escapeReceived){
			octet	^= 0x20;
			_escapeReceived	= false;
			}

		if(!_receiver.append(octet)){
			// Resource shortage
			_errorApi.discardDueToResourceShortage();
			_receiver.abort();
			_receivingFrame	= false;
			_escapeReceived	= false;
			continue;
			}
		_fcs.accumulate(octet);
		}
	}

