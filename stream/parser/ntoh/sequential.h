#include <stdint.h>

/** The purpose of this class is to allow a packet
	buffer to be parsed sequentially. Typically,
	after receiving a buffer, the client will
	create one of objects passing a pointer to
	the beginning of the buffer and either the
	maximum length of the buffer, or even better,
	the number of valid contiguous bytes in the buffer
	(if known).
	The client then invokes the parsing operations
	as required in the order that various elements
	are expected in the buffer. This means that the
	client must know apriori the structure of the
	buffer before it reads each element.
 */
class Sequential {
	private:
		/** A pointer to the start of the buffer.
		 */
		const unsigned char* const	_buffer;

		/** The maximum number of bytes in the buffer.
		 */
		const unsigned				_bufferLen;

		/** A pointer to the next sequential location
			in the _buffer to be parsed.
		 */
		const unsigned char*		_current;

		/** The number of bytes from the _current
			buffer to the end of the buffer.
		 */
		unsigned					_remaining;

		/** This member is true if a parse error
			is encountered.
		 */
		bool						_parseError;

		/** This member is true if the last string
			parsing operation was truncated because
			the client supplied buffer was too small.
		 */
		bool						_truncated;

	public:
		/** */
		Sequential(	const void*	buffer,
					unsigned	bufferLen
					) noexcept;

		/** This operation resets the current parsing position
			to the start of the buffer, and clears any pending
			parse error state.
		 */
		void	reset() noexcept;

		/** This operation returns true if an error is encountered
			while parsing a field.
		 */
		bool	parseError() noexcept;

		/** This operation resets the internal parse error
			indication to allow another parse operation to
			start at the current buffer position.
		 */
		void	resetParseError() noexcept;

		/** This operation returns true if the value returned
			by either a getCString() or getPString() was
			truncated because it would not fit in the client's
			buffer.
		 */
		bool	truncated() noexcept;

		/** This operation parses the next byte of the buffer
			and returns the uint8_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(uint8_t& value) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns the uint16_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(uint16_t& value) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns the uint32_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(uint32_t& value) noexcept;

		/** This operation parses the next byte of the buffer
			and returns the int8_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(int8_t& value) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns the int16_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(int16_t& value) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns the int32_t from that location.

			True is returned if an error is encountered parsing
			the buffer. In this case, the referenced value
			is not changed. In addtion, the internal buffer
			position sequence is not changed.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value and the internal buffer
			position is updated to point the next element
			to be parsed.
		 */
		void	get(int32_t& value) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns a copy of the C string (null terminated)
			from that location.

			True is returned if an error is encountered parsing
			the buffer. If the string was simply truncated because
			it would not fit in the value buffer then the nCopied
			value is set to the actual number of bytes copied to
			the value string. In that case, the internal buffer
			positioin is updated to point to the next element
			after the null terminated string in the buffer.
			If there was any other kind of error encountered while
			parsing the string, the value buffer will not be
			changed and the value of nCopied will be set to zero.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value, and the value at nCopied
			is updated to reflect the number of characters
			copied into the value buffer. The internal buffer
			position is updated to point the next element
			to be parsed.

			If the value buffer is written as described above,
			the string will be null terminated.
		 */
		unsigned	getCString(	char*		value,
								unsigned	maxValueLen
								) noexcept;

		/** This operation parses the next bytes of the buffer
			and returns a copy of the P(ascal) string from that
			location as a null terminated C string.

			A Pascal (P) string is a sequence of characters
			preceded by an octet that specifies the number
			of character octets that follow it immediately
			in the buffer.

			True is returned if an error is encountered parsing
			the buffer. If the string was simply truncated because
			it would not fit in the value buffer then the nCopied
			value is set to the actual number of bytes copied to
			the value string. In that case, the internal buffer
			positioin is updated to point to the next element
			after the null terminated string in the buffer.
			If there was any other kind of error encountered while
			parsing the string, the value buffer will not be
			changed and the value of nCopied will be set to zero.

			False is returned if the value is successfully
			parsed. In this case the parsed value is written
			to the referenced value, and the value at nCopied
			is updated to reflect the number of characters
			copied into the value buffer. The internal buffer
			position is updated to point the next element
			to be parsed.

			If the value buffer is written as described above,
			the string will be null terminated.
		 */
		unsigned	getPString(	char*		value,
								unsigned	maxValueLen
								) noexcept;
	};
