#include "sequential.h"
#include <arpa/inet.h>
#include <string.h>

Sequential::Sequential(	const void*	buffer,
						unsigned	bufferLen
						) noexcept:
		_buffer((const unsigned char*)buffer),
		_bufferLen(bufferLen),
		_current(_buffer),
		_remaining(bufferLen),
		_parseError(false),
		_truncated(false)
		{
	}

void	Sequential::reset() noexcept{
	_current	= _buffer;
	_remaining	= _bufferLen;
	}

bool	Sequential::parseError() noexcept{
	return _parseError;
	}

void	Sequential::resetParseError() noexcept{
	_parseError	= false;
	}

void	Sequential::get(uint8_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 1){
		_parseError	= true;
		return;
		}

	value	= (uint8_t)_current[0];

	_current	+= 1;
	_remaining	-= 1;
	}

void	Sequential::get(uint16_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 2 ){
		_parseError	= true;
		return;
		}

	uint16_t
	v	= _current[0];
	v	<<= 8;
	v	|= (_current[1] & 0x0FF);

	value	= ntohs(v);

	_current	+= 2;
	_remaining	-= 2;
	}

void	Sequential::get(uint32_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 4 ){
		_parseError	= true;
		return;
		}

	uint32_t
	v	= _current[0];
	v	<<= 8;
	v	|= (_current[1] & 0x0FF);
	v	<<= 8;
	v	|= (_current[2] & 0x0FF);
	v	<<= 8;
	v	|= (_current[3] & 0x0FF);

	value	= ntohl(v);

	_current	+= 4;
	_remaining	-= 4;
	}

void	Sequential::get(int8_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 1){
		_parseError	= true;
		return;
		}

	value	= (int8_t)_current[0];

	_current	+= 1;
	_remaining	-= 1;
	}

void	Sequential::get(int16_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 2 ){
		_parseError	= true;
		return;
		}

	uint32_t
	v	= _current[0];
	v	<<= 8;
	v	|= (_current[1] & 0x0FF);

	v	= ntohs(v);

	value	= (int16_t)v;

	_current	+= 2;
	_remaining	-= 2;
	}

void	Sequential::get(int32_t& value) noexcept{
	if(_parseError){
		return;
		}

	if(_remaining < 4 ){
		_parseError	= true;
		return;
		}

	uint32_t
	v	= _current[0];
	v	<<= 8;
	v	|= (_current[1] & 0x0FF);
	v	<<= 8;
	v	|= (_current[2] & 0x0FF);
	v	<<= 8;
	v	|= (_current[3] & 0x0FF);

	v	= ntohl(v);

	value	= (int32_t)v;

	_current	+= 4;
	_remaining	-= 4;
	}


unsigned	Sequential::getCString(	char*		value,
									unsigned	maxValueLen
									) noexcept{
	if(_parseError){
		return 0;
		}

	_truncated	= false;

	if(_remaining < 1){
		_parseError	= true;
		return 0;
		}

	// FIXME: strnlen is a GNU extension
	size_t
	len	= strnlen((const char*)_current,_remaining);

	if(len == _remaining){
		// Null termination not found.
		_parseError	= true;
		return 0;
		}

	strncpy(value,(const char*)_current,maxValueLen-1);

	value[maxValueLen-1]	= '\0';

	_current	+= len+1;
	_remaining	-= len+1;

	if(len > (maxValueLen-1)){
		_truncated	= true;
		}

	return len;
	}


unsigned	Sequential::getPString(	char*		value,
									unsigned	maxValueLen
									) noexcept{
	if(_parseError){
		return 0;
		}

	_truncated	= false;

	if(_remaining < 1){
		_parseError	= true;
		return 0;
		}

	unsigned len	= _current[0];

	if((_remaining-1) < len){
		// pstring in buffer is invalid
		_parseError	= true;
		return 0;
		}

	unsigned	nToCopy = len;

	if(len > maxValueLen-1){
		// Truncated
		_truncated		= true;
		nToCopy			= maxValueLen-1;
		return 0;
		}

	memcpy(value,&_current[1],nToCopy);

	value[nToCopy]	= '\0';

	_current	+= len+1;
	_remaining	-= len+1;

	return nToCopy;
	}

