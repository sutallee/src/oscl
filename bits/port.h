/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_porth_
#define _oscl_bits_porth_
#include "fieldapi.h"
#include "bitfield.h"

/** */
namespace Oscl {

/** */
namespace Bits {

/** */
template <class Type>
class Port : public FieldApi<Type> {
	private:
		/** */
		BitField<Type>	_lastWrite;

		/** */
		volatile Type&	_port;

	public:
		/** Constructor
		 */
		Port(volatile Type& port, Type initialValue=0) noexcept;

		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		void	changeBits(	Type	affectedBits,
							Type	bitValues
							) noexcept;

		/** Clear the bits specified as a mask of affectedBits.
		 */
		void	clearBits(Type affectedBits) noexcept;

		/** Set the bits specified as a mask of affectedBits.
		 */
		void	setBits(Type affectedBits) noexcept;

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(Type affectedBits) noexcept;

		/** Compare the interestingBits of the port with the
			corresponding bits of the bitValues mask.
		 */
		bool	equal(Type interestingBits,Type bitValues) const noexcept;

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		Type	read() const noexcept;
	};

template <class Type>
Port<Type>::Port(volatile Type& port, Type initialValue) noexcept:
		_port(port)
		{
	_lastWrite.clearBits(~0);
	}

template <class Type>
void	Port<Type>::changeBits(	Type	affectedBits,
								Type	bitValues
								) noexcept{
	_lastWrite.changeBits(affectedBits,bitValues);
	_port	= _lastWrite.read();
	}

template <class Type>
void	Port<Type>::clearBits(Type affectedBits) noexcept{
	_lastWrite.clearBits(affectedBits);
	_port	= _lastWrite.read();
	}

template <class Type>
void	Port<Type>::setBits(Type affectedBits) noexcept{
	_lastWrite.setBits(affectedBits);
	_port	= _lastWrite.read();
	}

template <class Type>
bool	Port<Type>::testAndSet(Type affectedBits) noexcept{
	if(_lastWrite.testAndSet(affectedBits)){
		_port	= _lastWrite.read();
		return true;
		}
	return false;
	}

template <class Type>
bool	Port<Type>::equal(Type interestingBits,Type bitValues) const noexcept{
	return (_port & interestingBits) == (bitValues & interestingBits);
	}

template <class Type>
Type	Port<Type>::read() const noexcept{
	return _port;
	}

}
}

#endif
