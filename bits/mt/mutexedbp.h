/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_mutexedbph_
#define _oscl_bits_mutexedbph_
#include "oscl/bits/fieldapi.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/mt/scopesync.h"

/** */
namespace Oscl {
/** */
namespace Bits {

/** */
template <class Type>
class MutexedPort : public FieldApi<Type> {
	private:
		/** */
		FieldApi<Type>&					_bitPort;

		/** */
		Oscl::Mt::Mutex::Simple::Api&	_mutex;

	public:
		/** Constructor
		 */
		MutexedPort(FieldApi<Type>& port, Oscl::Mt::Mutex::Simple::Api& mutex) noexcept;

		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		void	changeBits(	Type	affectedBits,
							Type	bitValues
							) noexcept;

		/** Clear the bits specified as a mask of affectedBits.
		 */
		void	clearBits(Type affectedBits) noexcept;

		/** Set the bits specified as a mask of affectedBits.
		 */
		void	setBits(Type affectedBits) noexcept;

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is atomic to the degree provided by the Mutex.
		 */
		bool	testAndSet(Type affectedBits) noexcept;

		/** Compare the interestingBits of the port with the
			corresponding bits of the bitValues mask.
		 */
		bool	equal(Type interestingBits,Type bitValues) const noexcept;

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		Type	read() const noexcept;
	};

template <class Type>
MutexedPort<Type>::MutexedPort(	FieldApi<Type>&	bitPort,
										Oscl::Mt::Mutex::Simple::Api&	mutex
										) noexcept:
		_bitPort(bitPort),
		_mutex(mutex)
		{
	}

template <class Type>
void	MutexedPort<Type>::changeBits(	Type	affectedBits,
										Type	bitValues
										) noexcept{
	Mt::ScopeSync	ss(_mutex);
	_bitPort.changeBits(affectedBits,bitValues);
	}

template <class Type>
void	MutexedPort<Type>::clearBits(Type affectedBits) noexcept{
	Mt::ScopeSync	ss(_mutex);
	_bitPort.clearBits(affectedBits);
	}

template <class Type>
void	MutexedPort<Type>::setBits(Type affectedBits) noexcept{
	Mt::ScopeSync	ss(_mutex);
	_bitPort.setBits(affectedBits);
	}

template <class Type>
bool	MutexedPort<Type>::testAndSet(Type affectedBits) noexcept{
	Mt::ScopeSync	ss(_mutex);
	return _bitPort.testAndSet(affectedBits);
	}

template <class Type>
bool	MutexedPort<Type>::equal(	Type	interestingBits,
									Type	bitValues
									) const noexcept{
	return _bitPort.equal(interestingBits,bitValues);
	}

template <class Type>
Type	MutexedPort<Type>::read() const noexcept{
	return _bitPort.read();
	}

}
}

#endif
