/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_iobith_
#define _oscl_bits_iobith_
#include "fieldapi.h"
#include "ioapi.h"

/** */
namespace Oscl{

/** */
namespace Bits{

/** */
template <class Type,unsigned BitNum,bool ActiveState>
class IoBit : public InOutApi {
	private:
		/** */
		FieldApi<Type>&		_portBits;
	public:
		/** */
		IoBit(FieldApi<Type>& portBits) noexcept;
		/** */
	public:	// InOutApi
		/** */
		void	high() noexcept;
		/** */
		void	low() noexcept;
		/** */
		bool	state() noexcept;
	};

template <class Type,unsigned BitNum,bool ActiveState>
IoBit<Type,BitNum,ActiveState>:: IoBit(FieldApi<Type>& portBits) noexcept:
		_portBits(portBits)
		{
	}

template <class Type,unsigned BitNum,bool ActiveState>
void	IoBit<Type,BitNum,ActiveState> ::high() noexcept{
	_portBits.changeBits((1<<BitNum),ActiveState<<BitNum);
	}

template <class Type,unsigned BitNum,bool ActiveState>
void	IoBit<Type,BitNum,ActiveState> ::low() noexcept{
	_portBits.changeBits((1<<BitNum),(!ActiveState)<<BitNum);
	}

template <class Type,unsigned BitNum,bool ActiveState>
bool	IoBit<Type,BitNum,ActiveState> ::state() noexcept{
	return _portBits.equal((1<<BitNum),((!ActiveState)<<BitNum));
	}
}
}

#endif
