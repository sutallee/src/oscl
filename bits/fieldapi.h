/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_fieldapih_
#define _oscl_bits_fieldapih_

/** */
namespace Oscl {

/** */
namespace Bits {

/** */
template <class Type>
class FieldApi {
	public:
		/** Shut-up GCC. */
		virtual ~FieldApi() {}

		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		virtual void	changeBits(	Type	affectedBits,
									Type	bitValues
									) noexcept=0;

		/** Clear the bits specified as a mask of affectedBits.
		 */
		virtual void	clearBits(Type affectedBits) noexcept=0;

		/** Set the bits specified as a mask of affectedBits.
		 */
		virtual void	setBits(Type affectedBits) noexcept=0;

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is not necessarily atomic, although sub-classes may
			make it so.
		 */
		virtual bool	testAndSet(Type affectedBits) noexcept=0;

		/** Compare the interestingBits of the port with the
			corresponding bits of the bitValues mask.
		 */
		virtual bool	equal(	Type	interestingBits,
								Type	bitValues
								) const noexcept=0;

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		virtual Type	read() const noexcept=0;
	};

}
}

#endif
