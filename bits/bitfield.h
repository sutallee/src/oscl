/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_bitfieldh_
#define _oscl_bits_bitfieldh_

/** */
namespace Oscl {

/** The purpose of this primitive data type is to simplify bit field
	manipulation; clarify the client code that performs the manipulation;
	and to capture the code for performing the manipulations such that
	programming errors are minimized.

	This class is not derived from an abstract class since it is intended
	to be used where a primitive data type would be used, and the size
	overhead of a VTABLE would change the size relative to the primitive
	Type. This would limit the usefulness of the class in representing
	hardware register layouts.
 */
template <class Type>
class BitField {
	private:
		/** The actual data.
		 */
		Type	_field;
	public:
#if 0
		/** Constructor
		 */
		inline BitField(Type field=0) noexcept:_field(field){}
#endif

		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		inline BitField<Type>	changeBits(	Type	affectedBits,
											Type	bitValues
											) noexcept{
			_field	&= ~affectedBits;
			_field	|= (bitValues & affectedBits);
			return *this;
			}

		/** Clear the bits specified as a mask of affectedBits.
		 */
		inline BitField<Type>	clearBits(Type affectedBits) noexcept{
			_field	&= ~affectedBits;
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits.
		 */
		inline BitField<Type>	setBits(Type affectedBits) noexcept{
			_field	|= affectedBits;
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(Type affectedBits) noexcept{
			if(equal(affectedBits,0)){
				_field	|= affectedBits;
				return true;
				}
			return false;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	equal(	Type	interestingBits,
							Type	bitValues
							) const noexcept{
			return (_field & interestingBits) == (bitValues & interestingBits);
			}

		/** Extracts the value from the specified bit field.
		 */
		inline Type	extract(	Type		fieldMask,
								unsigned	lsb
								) const noexcept{
			return (_field & fieldMask) >> lsb;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	anySet(	Type interestingBits) const noexcept{
			return (_field & interestingBits);
			}

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline Type	read() const noexcept{ return _field;}

		/** Allow object to be cast to actual Type.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline operator Type() const noexcept{return _field;}

		/** */
		inline Type&	operator =(const Type& other) noexcept{
			_field	= other;
			return _field;
			}

		/** */
		inline Type&	operator =(volatile const Type& other) noexcept{
			_field	= other;
			return _field;
			}
	};

};

#endif
