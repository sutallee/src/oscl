/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bits_ioapih_
#define _oscl_bits_ioapih_
#include "inputapi.h"
#include "outputapi.h"

/** */
namespace Oscl {
/** */
namespace Bits {

/** The purpose of this interface is to hide the details
	about the parallel port interface for this control/status/data
	bit. This interface allows the address, bit number, 
	access method (Memory Map I/O or I/O instructions), and
	logical to physical state conversion to be handled by the
	implementation.
 */
class InOutApi : public InputApi, public OutputApi {
	public:
		/** */
		virtual void	high() noexcept=0;
		/** */
		virtual void	low() noexcept=0;
		/** */
		virtual bool	state() noexcept=0;
	};

}
}

#endif
