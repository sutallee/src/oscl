/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bounded_signedh_
#define _oscl_bounded_signedh_

/* NOTE!:
	This file is under development.
	This file is un-tested.
	This file is volatile and may/will change,
	and may be deleted.
 */


/** */
namespace Oscl {
/** */
namespace Bounded {

/** */
template <class intType>
class Signed {
	private:
		/** */
		intType							_int;
		/** */
		bool							_overflow;
		/** */
		static constexpr intType		_msbMask =
											(intType)1ULL<<((sizeof(intType)*8)-1);
	public:
		/** */
		static constexpr intType		_largestPositive =
											(intType)(~_msbMask);
		/** */
		static constexpr intType		_largestNegative =
											(intType)(_msbMask);
	public:
		/** */
		inline Signed() noexcept:
				_int(0),
				_overflow(false)
				{
			}
		/** */
		inline Signed(const Signed<intType>& s) noexcept{
			if((_overflow = s._overflow)) return;
			_int	= s._int;
			}
		/** */
		inline Signed(const intType s) noexcept:
				_overflow(false)
				{
			_int	= s;
			}
		/** */
		inline intType	getSigned() const noexcept { return _int; }
		/** */
		inline bool	overflow() const noexcept {return _overflow;}
		/** */
		inline Signed&	operator = (const Signed& s) noexcept{
			if(!(_overflow = s._overflow)){
				_int	= s._int;
				}
			return *this;
			}
		/** */
		inline Signed&	operator = (intType s) noexcept{
			_overflow	= false;
			_int		= s;
			return *this;
			}
		/** */
		Signed&	operator += (const Signed& s) noexcept{
			if(_overflow || s._overflow) return *this;
			long			sum	= s._int + _int;
				{
				intType	arg	= ((intType)s._int) & _msbMask;
				intType	loc	= ((intType)_int) & _msbMask;
				if(arg ^ loc){
					// Cannot overflow
					_int	= sum;
					}
				else{
					// Check for overflow
					const intType res	= (intType)sum & _msbMask;
					if(res ^ arg){
						_overflow	= true;
						}
					else{
						_int	= sum;
						}
					}
				}
			return *this;
			}

		/** */
		Signed&	operator -= (const Signed& s) noexcept{
			if(_overflow || s._overflow) return *this;
				intType			diff	= _int - s._int;
				{
				intType	arg		= ((intType)s._int) & _msbMask;
				intType	loc		= ((intType)_int) & _msbMask;
				if(arg ^ loc){
					// Check for overflow
					// operands have different signs
					const intType	res	= diff & _msbMask;
					if(res ^ loc){
						_overflow	= true;
						}
					else{
						_int	= diff;
						}
					}
				else{
					// Cannot overflow
					_int	= diff;
					}
				}
			return *this;
			}

		/** */
		Signed&	operator /= (const Signed& s) noexcept{
			if(_overflow || s._overflow) return *this;
			if(s._int == 0){
				_overflow	= true;
				}
			_int	/= s._int;
			return *this;
			}

		/** */
		Signed&	operator %= (const Signed& s) noexcept{
			if(_overflow || s._overflow) return *this;
			if(s._int == 0){
				_overflow	= true;
				}
			_int	%= s._int;
			return *this;
			}
		bool	operator < (const Signed& s) const noexcept{
			if(_overflow) return false;
			return (_int < s._int);
			}
	};
template <class intType>
inline Signed<intType>&	operator + (	const Signed<intType>&	a,
										const Signed<intType>&	b
										) noexcept{
	Signed<intType>	result(a);
	return result += b;
	}
template <class intType>
inline Signed<intType>&	operator - (	const Signed<intType>&	a,
										const Signed<intType>&	b
										) noexcept{
	Signed<intType>	result(a);
	return result -= b;
	}
template <class intType>
inline Signed<intType>&	operator / (	const Signed<intType>&	a,
										const Signed<intType>&	b
										) noexcept{
	Signed<intType>	result(a);
	return result /= b;
	}
};
};

#endif
