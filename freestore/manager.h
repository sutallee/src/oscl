/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_freestore_managerh_
#define _oscl_freestore_managerh_

/** */
namespace Oscl {
/** */
namespace FreeStore {

/** Represents an abstract storage allocation interface.
 *	The primary purpose of this mechanism is to allow for multiple
 *	instances of specialized storage management. Objects allocated from this
 *	type of free store manager are subclasses of the companion
 *	FreeStore::Object. These managed objects must override the default
 *	delete operator calling FreeStore::Object::fsmfree().
 *	Typically:<pre><code>
 *		void operator delete(void *p){
 *			((MyConcreteClass *)p)->fsmfree(p);
 *			}
 * 	</code></pre>
 *	Remember to declare any base class of MyConcreteClass as having
 *	a virtual destructor to ensure that a delete of the abstract class
 *	will result in "MyConcreteClass::operator delete" being invoked.
 *	The result is that the object is returned to the free store from which
 *	it came. The mechanism for switching between FreeStore::Mgr objects at run-
 *	time is implementation dependent. However, this interface was designed
 *	to support per-thread allocation, where the micro-kernel supports
 *	switching the default storage allocator during a context switch.
 */
class Mgr {
	public: // Constructors
		/** The constructor initializes the _observers list.
		 */
		Mgr() noexcept;

		/** Virtual destructors are just a good idea.
		 */
		virtual ~Mgr();

	public:
		/** This function is called by the FreeStore::Object to
		 *	deallocate the memory resource specified. The specified
		 *	void pointer always points to the beginning of the memory
		 *	resource. A simple implementation would be for the sub-class
		 *	to simply invoke "::free(fso);".
		 */
		virtual void	free(void* fso)	 noexcept= 0;
	};

}
}

#endif
