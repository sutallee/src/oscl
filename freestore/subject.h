/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_freestore_subjecth_
#define _oscl_freestore_subjecth_
#include "oscl/mt/mutex/simple.h"

/** */
namespace Oscl {


/** */
namespace FreeStore {

/** */
class Observer;

/** This class represents the subject interface to free store observers.
 */
class Subject {
	private:
		/** A list of observers to changes in the freestore.
		 */
		Observer*	_observers;

	public:
		/** The constructor.
		 */
		Subject() noexcept;

		/** This utility threads the observer into the freestore observer
			list.
		 */
		void	attach(Observer* observer,Oscl::Mt::Mutex::Simple::Api& lock) noexcept;

		/** This utility removes the observer from the freestore observer
			list.
		 */
		Observer*	detach(Observer* observer,Oscl::Mt::Mutex::Simple::Api& lock) noexcept;

	protected:	// utilities

		/** This interface is called when the free store goes below the
			implementation defined low water mark. The implementation of
			this function iterates the observer list and invokes each
			observer lowMemoryMark operation.
		 */
		void	notifyLowMemoryMark(Oscl::Mt::Mutex::Simple::Api& lock) noexcept;

		/** This interface is called when the free store goes above the
			implementation defined high water mark. The implementation of
			this function iterates the observer list and invokes each
			observer highMemoryMark operation.
		 */
		void	notifyHighMemoryMark(Oscl::Mt::Mutex::Simple::Api& lock) noexcept;

	};

}
}

#endif
