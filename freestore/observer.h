/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_freestore_observerh_
#define _oscl_freestore_observerh_

/** */
namespace Oscl {
/** */
namespace FreeStore {

/** This abstract class is used by FreeStore::Mgr clients/users that need
	to be informed about the amount of memory available from the manager.
	This information may be used to implement flow control type policies.
	No assumptions should be made about the thread of control which
	calls these functions. The low water and high water marks form
	a hesterisis loop.
 */

class Observer {
	private:
		/** This link field is used to by the subject to link this
			observer into its list of registered/attached observers.
		 */

		Observer*	_link;

		friend class Subject;

	public:
		/** Make GCC happy */
		virtual ~Observer() {}

		/** This function is implemented by the client and called by the
			free store manager when a low memory threshold (specified
			by a particular FreeStore::Mgr implementation) is crossed.
		 */
		virtual void	lowMemoryMark() noexcept=0;

		/** This function is implemented by the client and called by the
			free store manager when a high memory threshold (specified
			by a particular FreeStore::Mgr implementation) is crossed.
		 */
		virtual void	highMemoryMark() noexcept=0;
	};

}
}

#endif
