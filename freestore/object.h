/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_freestore_objecth_
#define _oscl_freestore_objecth_
#include <stddef.h>
#include "manager.h"

/** */
namespace Oscl {
/** */
namespace FreeStore {

/**	The purpose of this class is to cause an instance of a derived class to be
 *	deleted from the same FreeStore::Mgr that created it.
 *	This class must be inherited by all objects allocated from a FreeStore::Mgr.
 *	Generally, this class should be "private"ly inherited by the sub-class.
 */
class Object{
	protected:
		/** This is a reference to the FreeStore::Mgr that allocated an
		 *	instance of this object.
		 */
		Mgr&	_freeStoreMgr;

	protected:
		/** This constructor takes a reference to the FreeStore::Mgr
		 *	used to allocate the object.
		 */
		Object(Mgr& mgr) noexcept:_freeStoreMgr(mgr){}

		/** This virtual destructor ensures that the appropriate size
			is passed to the delete operator when the object is destroyed.
		 */
		virtual ~Object(){}

		/** This function is only called by the sub-classes' overriden
		 *	delete operator.
		 */
		inline void	fsmfree(void *p) {
			_freeStoreMgr.free(p);
			}

	public:		// Memory management operators
		/** Sub classes MUST provide their own delete operator which can do
			exactly what this delete operator does, except a cast to the
			concrete type of the sub-class.
		 */
		void operator delete(void *p,size_t size) noexcept;
	};

}
}

#endif
