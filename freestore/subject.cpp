/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "subject.h"
#include "observer.h"
#include "oscl/mt/mutex/simple.h"

using namespace Oscl::FreeStore;

Subject::Subject() noexcept:_observers(0){
	}

void	Subject::attach(Observer* observer,Oscl::Mt::Mutex::Simple::Api& lock) noexcept{
	lock.lock();
	observer->_link	= _observers;
	_observers		= observer;
	lock.unlock();
	}

Observer*	Subject::detach(Observer* observer,Oscl::Mt::Mutex::Simple::Api& lock) noexcept{
	Observer*	next;
	Observer*	prev;
	if(!_observers) return 0;
	lock.lock();
	for(next=_observers,prev=0;next;next=next->_link){
		if(next == observer){
			if(prev){
				prev->_link	= next->_link;
				}
			else{
				_observers	= next->_link;
				}
			lock.unlock();
			return next;
			}
		}
	lock.unlock();
	return 0;
	}

void	Subject::notifyLowMemoryMark(Oscl::Mt::Mutex::Simple::Api& lock) noexcept{
	Observer*	next;
	if(!_observers) return;
	lock.lock();
	for(next=_observers;next;next=next->_link){
		next->lowMemoryMark();
		}
	lock.unlock();
	}

void	Subject::notifyHighMemoryMark(Oscl::Mt::Mutex::Simple::Api& lock) noexcept{
	Observer*	next;
	if(!_observers) return;
	lock.lock();
	for(next=_observers;next;next=next->_link){
		next->highMemoryMark();
		}
	lock.unlock();
	}

