/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_freestore_composersh_
#define _oscl_freestore_composersh_

#include "oscl/freestore/manager.h"

/** */
namespace Oscl {
/** */
namespace FreeStore {

/** */
template <class Type>
class FreeStoreApi {
	public:
		/** */
		virtual void	free(Type* p) noexcept=0;
	};

/** */
template <class Context,class Type>
class Composer: public FreeStoreApi<Type> {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*_free)(Type* p);

	public:
		/** */
		Composer(
			Context&	context,
			void		(Context::*free)(Type*p)
			) noexcept;

		/** */
		void	free(Type* p) noexcept;
	};

template <class Context, class Type>
Composer<Context,Type>::Composer(
	Context&	context,
	void		(Context::*free)(Type*p)
	) noexcept:
		_context(context),
		_free(free)
		{
	}

template <class Context, class Type>
void	Composer<Context,Type>::free(Type* p) noexcept{
	(_context.*_free)(p);
	}

/** */
template <class Type>
class FreeStoreMgr : public Oscl::FreeStore::Mgr {
	private:
		/** */
		FreeStoreApi<Type>&	_api;

	public:
		/** */
		FreeStoreMgr(FreeStoreApi<Type>& api) noexcept:_api(api){}

	private: // FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept{
			_api.free((Type*)fso);
			}
	};

}
}

#endif
